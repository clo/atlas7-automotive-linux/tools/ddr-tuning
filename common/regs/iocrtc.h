/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __IOCRTC_H__
#define __IOCRTC_H__



/* IOC_RTC */
/* ==================================================================== */

/* SW_RTC_FUNC_SEL_0_REG_SET Address */
/* Sets selected bits of SW_RTC_FUNC_SEL_0_REG register */
#define SW_RTC_FUNC_SEL_0_REG_SET 0x18880000

/* SW_RTC_FUNC_SEL_0_REG_SET.rtc_gpio_0 - sets function_select bits of RTC_GPIO_0 pad */
/* sets function_select bits of RTC_GPIO_0 pad */
/* 0b000 : rtc_gpio[0] */
/* 0b001 : pwc.wakeup_src_0 */
/* 0b011 : jtag.tck */
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__SHIFT       0
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__MASK        0x00000007
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.rtc_gpio_1 - sets function_select bits of RTC_GPIO_1 pad */
/* sets function_select bits of RTC_GPIO_1 pad */
/* 0b000 : rtc_gpio[1] */
/* 0b001 : pwc.wakeup_src_1 */
/* 0b010 : c.can_trnsvr_intr */
/* 0b011 : jtag.tdi */
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__SHIFT       4
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__MASK        0x00000070
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.rtc_gpio_2 - sets function_select bits of RTC_GPIO_2 pad */
/* sets function_select bits of RTC_GPIO_2 pad */
/* 0b000 : rtc_gpio[2] */
/* 0b001 : pwc.wakeup_src_2 */
/* 0b010 : c1.can_rxd_1_mux2 */
/* 0b011 : jtag.swdiotms */
/* 0b101 : c0.can_rxd_0_mux1 */
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT       8
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK        0x00000700
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__INV_MASK    0xFFFFF8FF
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.rtc_gpio_3 - sets function_select bits of RTC_GPIO_3 pad */
/* sets function_select bits of RTC_GPIO_3 pad */
/* 0b000 : rtc_gpio[3] */
/* 0b001 : pwc.wakeup_src_3 */
/* 0b010 : c1.can_txd_1_mux2 */
/* 0b011 : jtag.tdo */
/* 0b100 : pwc.gpio3_clk */
/* 0b101 : c0.can_txd_0_mux1 */
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT       12
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK        0x00007000
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__INV_MASK    0xFFFF8FFF
#define SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.low_bat_ind_b - sets function_select bits of LOW_BAT_IND_B pad */
/* sets function_select bits of LOW_BAT_IND_B pad */
/* 0b000 : rtc_gpio[4] */
/* 0b001 : pwc.lowbatt_b_mux0 */
/* 0b010 : sp0.ext_ldo_on */
/* 0b011 : jtag.ntrst_mux0 */
#define SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__SHIFT       16
#define SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__MASK        0x00070000
#define SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__INV_MASK    0xFFF8FFFF
#define SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.on_key_b - sets function_select bits of ON_KEY_B pad */
/* sets function_select bits of ON_KEY_B pad */
/* 0b001 : pwc.on_key_b_mux0 */
#define SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__SHIFT       20
#define SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__MASK        0x00700000
#define SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__INV_MASK    0xFF8FFFFF
#define SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.ext_on - sets function_select bits of EXT_ON pad */
/* sets function_select bits of EXT_ON pad */
/* 0b001 : pwc.ext_on */
#define SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__SHIFT       24
#define SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__MASK        0x07000000
#define SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__INV_MASK    0xF8FFFFFF
#define SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_SET.mem_on - sets function_select bits of MEM_ON pad */
/* sets function_select bits of MEM_ON pad */
/* 0b001 : pwc.mem_on */
#define SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__SHIFT       28
#define SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__MASK        0x70000000
#define SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__INV_MASK    0x8FFFFFFF
#define SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR Address */
/* Clears selected bits of SW_RTC_FUNC_SEL_0_REG register */
#define SW_RTC_FUNC_SEL_0_REG_CLR 0x18880004

/* SW_RTC_FUNC_SEL_0_REG_CLR.rtc_gpio_0 - clears function_select bits of RTC_GPIO_0 pad */
/* clears function_select bits of RTC_GPIO_0 pad */
/* 0b000 : rtc_gpio[0] */
/* 0b001 : pwc.wakeup_src_0 */
/* 0b011 : jtag.tck */
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__SHIFT       0
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__MASK        0x00000007
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.rtc_gpio_1 - clears function_select bits of RTC_GPIO_1 pad */
/* clears function_select bits of RTC_GPIO_1 pad */
/* 0b000 : rtc_gpio[1] */
/* 0b001 : pwc.wakeup_src_1 */
/* 0b010 : c.can_trnsvr_intr */
/* 0b011 : jtag.tdi */
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__SHIFT       4
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__MASK        0x00000070
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.rtc_gpio_2 - clears function_select bits of RTC_GPIO_2 pad */
/* clears function_select bits of RTC_GPIO_2 pad */
/* 0b000 : rtc_gpio[2] */
/* 0b001 : pwc.wakeup_src_2 */
/* 0b010 : c1.can_rxd_1_mux2 */
/* 0b011 : jtag.swdiotms */
/* 0b101 : c0.can_rxd_0_mux1 */
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__SHIFT       8
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK        0x00000700
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__INV_MASK    0xFFFFF8FF
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.rtc_gpio_3 - clears function_select bits of RTC_GPIO_3 pad */
/* clears function_select bits of RTC_GPIO_3 pad */
/* 0b000 : rtc_gpio[3] */
/* 0b001 : pwc.wakeup_src_3 */
/* 0b010 : c1.can_txd_1_mux2 */
/* 0b011 : jtag.tdo */
/* 0b100 : pwc.gpio3_clk */
/* 0b101 : c0.can_txd_0_mux1 */
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__SHIFT       12
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK        0x00007000
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__INV_MASK    0xFFFF8FFF
#define SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.low_bat_ind_b - clears function_select bits of LOW_BAT_IND_B pad */
/* clears function_select bits of LOW_BAT_IND_B pad */
/* 0b000 : rtc_gpio[4] */
/* 0b001 : pwc.lowbatt_b_mux0 */
/* 0b010 : sp0.ext_ldo_on */
/* 0b011 : jtag.ntrst_mux0 */
#define SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__SHIFT       16
#define SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__MASK        0x00070000
#define SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__INV_MASK    0xFFF8FFFF
#define SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.on_key_b - clears function_select bits of ON_KEY_B pad */
/* clears function_select bits of ON_KEY_B pad */
/* 0b001 : pwc.on_key_b_mux0 */
#define SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__SHIFT       20
#define SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__MASK        0x00700000
#define SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__INV_MASK    0xFF8FFFFF
#define SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.ext_on - clears function_select bits of EXT_ON pad */
/* clears function_select bits of EXT_ON pad */
/* 0b001 : pwc.ext_on */
#define SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__SHIFT       24
#define SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__MASK        0x07000000
#define SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__INV_MASK    0xF8FFFFFF
#define SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_0_REG_CLR.mem_on - clears function_select bits of MEM_ON pad */
/* clears function_select bits of MEM_ON pad */
/* 0b001 : pwc.mem_on */
#define SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__SHIFT       28
#define SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__MASK        0x70000000
#define SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__INV_MASK    0x8FFFFFFF
#define SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_1_REG_SET Address */
/* Sets selected bits of SW_RTC_FUNC_SEL_1_REG register */
#define SW_RTC_FUNC_SEL_1_REG_SET 0x18880008

/* SW_RTC_FUNC_SEL_1_REG_SET.core_on - sets function_select bits of CORE_ON pad */
/* sets function_select bits of CORE_ON pad */
/* 0b001 : pwc.core_on */
#define SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__SHIFT       0
#define SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__MASK        0x00000007
#define SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_1_REG_SET.io_on - sets function_select bits of IO_ON pad */
/* sets function_select bits of IO_ON pad */
/* 0b001 : pwc.io_on */
#define SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__SHIFT       4
#define SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__MASK        0x00000070
#define SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_1_REG_SET.can0_tx - sets function_select bits of CAN0_TX pad */
/* sets function_select bits of CAN0_TX pad */
/* 0b000 : rtc_gpio[5] */
/* 0b001 : c0.can_txd_0_mux0 */
/* 0b010 : u2.txd_2 */
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__SHIFT       8
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__MASK        0x00000700
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__INV_MASK    0xFFFFF8FF
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_SET.can0_rx - sets function_select bits of CAN0_RX pad */
/* sets function_select bits of CAN0_RX pad */
/* 0b000 : rtc_gpio[6] */
/* 0b001 : c0.can_rxd_0_mux0 */
/* 0b010 : u2.rxd_2 */
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__SHIFT       12
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__MASK        0x00007000
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__INV_MASK    0xFFFF8FFF
#define SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_SET.spi0_clk - sets function_select bits of SPI0_CLK pad */
/* sets function_select bits of SPI0_CLK pad */
/* 0b000 : rtc_gpio[7] */
/* 0b001 : sp0.qspi_clk */
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__SHIFT       16
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__MASK        0x00070000
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__INV_MASK    0xFFF8FFFF
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_SET.spi0_cs_b - sets function_select bits of SPI0_CS_B pad */
/* sets function_select bits of SPI0_CS_B pad */
/* 0b000 : rtc_gpio[8] */
/* 0b001 : sp0.qspi_cs_b */
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__SHIFT       20
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__MASK        0x00700000
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__INV_MASK    0xFF8FFFFF
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_SET.spi0_io_0 - sets function_select bits of SPI0_IO_0 pad */
/* sets function_select bits of SPI0_IO_0 pad */
/* 0b000 : rtc_gpio[9] */
/* 0b001 : sp0.qspi_data_0 */
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__SHIFT       24
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__MASK        0x07000000
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__INV_MASK    0xF8FFFFFF
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_SET.spi0_io_1 - sets function_select bits of SPI0_IO_1 pad */
/* sets function_select bits of SPI0_IO_1 pad */
/* 0b000 : rtc_gpio[10] */
/* 0b001 : sp0.qspi_data_1 */
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__SHIFT       28
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__MASK        0x70000000
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__INV_MASK    0x8FFFFFFF
#define SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR Address */
/* Clears selected bits of SW_RTC_FUNC_SEL_1_REG register */
#define SW_RTC_FUNC_SEL_1_REG_CLR 0x1888000C

/* SW_RTC_FUNC_SEL_1_REG_CLR.core_on - clears function_select bits of CORE_ON pad */
/* clears function_select bits of CORE_ON pad */
/* 0b001 : pwc.core_on */
#define SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__SHIFT       0
#define SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__MASK        0x00000007
#define SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_1_REG_CLR.io_on - clears function_select bits of IO_ON pad */
/* clears function_select bits of IO_ON pad */
/* 0b001 : pwc.io_on */
#define SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__SHIFT       4
#define SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__MASK        0x00000070
#define SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_FUNC_SEL_1_REG_CLR.can0_tx - clears function_select bits of CAN0_TX pad */
/* clears function_select bits of CAN0_TX pad */
/* 0b000 : rtc_gpio[5] */
/* 0b001 : c0.can_txd_0_mux0 */
/* 0b010 : u2.txd_2 */
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__SHIFT       8
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__MASK        0x00000700
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__INV_MASK    0xFFFFF8FF
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR.can0_rx - clears function_select bits of CAN0_RX pad */
/* clears function_select bits of CAN0_RX pad */
/* 0b000 : rtc_gpio[6] */
/* 0b001 : c0.can_rxd_0_mux0 */
/* 0b010 : u2.rxd_2 */
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__SHIFT       12
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__MASK        0x00007000
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__INV_MASK    0xFFFF8FFF
#define SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR.spi0_clk - clears function_select bits of SPI0_CLK pad */
/* clears function_select bits of SPI0_CLK pad */
/* 0b000 : rtc_gpio[7] */
/* 0b001 : sp0.qspi_clk */
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__SHIFT       16
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__MASK        0x00070000
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__INV_MASK    0xFFF8FFFF
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR.spi0_cs_b - clears function_select bits of SPI0_CS_B pad */
/* clears function_select bits of SPI0_CS_B pad */
/* 0b000 : rtc_gpio[8] */
/* 0b001 : sp0.qspi_cs_b */
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__SHIFT       20
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__MASK        0x00700000
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__INV_MASK    0xFF8FFFFF
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR.spi0_io_0 - clears function_select bits of SPI0_IO_0 pad */
/* clears function_select bits of SPI0_IO_0 pad */
/* 0b000 : rtc_gpio[9] */
/* 0b001 : sp0.qspi_data_0 */
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__SHIFT       24
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__MASK        0x07000000
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__INV_MASK    0xF8FFFFFF
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_1_REG_CLR.spi0_io_1 - clears function_select bits of SPI0_IO_1 pad */
/* clears function_select bits of SPI0_IO_1 pad */
/* 0b000 : rtc_gpio[10] */
/* 0b001 : sp0.qspi_data_1 */
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__SHIFT       28
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__WIDTH       3
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__MASK        0x70000000
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__INV_MASK    0x8FFFFFFF
#define SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_2_REG_SET Address */
/* Sets selected bits of SW_RTC_FUNC_SEL_2_REG register */
#define SW_RTC_FUNC_SEL_2_REG_SET 0x18880010

/* SW_RTC_FUNC_SEL_2_REG_SET.spi0_io_2 - sets function_select bits of SPI0_IO_2 pad */
/* sets function_select bits of SPI0_IO_2 pad */
/* 0b000 : rtc_gpio[11] */
/* 0b001 : sp0.qspi_data_2 */
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__SHIFT       0
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__WIDTH       3
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__MASK        0x00000007
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_2_REG_SET.spi0_io_3 - sets function_select bits of SPI0_IO_3 pad */
/* sets function_select bits of SPI0_IO_3 pad */
/* 0b000 : rtc_gpio[12] */
/* 0b001 : sp0.qspi_data_3 */
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__SHIFT       4
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__WIDTH       3
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__MASK        0x00000070
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_2_REG_CLR Address */
/* Clears selected bits of SW_RTC_FUNC_SEL_2_REG register */
#define SW_RTC_FUNC_SEL_2_REG_CLR 0x18880014

/* SW_RTC_FUNC_SEL_2_REG_CLR.spi0_io_2 - clears function_select bits of SPI0_IO_2 pad */
/* clears function_select bits of SPI0_IO_2 pad */
/* 0b000 : rtc_gpio[11] */
/* 0b001 : sp0.qspi_data_2 */
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__SHIFT       0
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__WIDTH       3
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__MASK        0x00000007
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__INV_MASK    0xFFFFFFF8
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__HW_DEFAULT  0x0

/* SW_RTC_FUNC_SEL_2_REG_CLR.spi0_io_3 - clears function_select bits of SPI0_IO_3 pad */
/* clears function_select bits of SPI0_IO_3 pad */
/* 0b000 : rtc_gpio[12] */
/* 0b001 : sp0.qspi_data_3 */
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__SHIFT       4
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__WIDTH       3
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__MASK        0x00000070
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__INV_MASK    0xFFFFFF8F
#define SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__HW_DEFAULT  0x0

/* SW_RTC_PULL_EN_0_REG_SET Address */
/* Sets selected bits of SW_RTC_PULL_EN_0_REG register */
#define SW_RTC_PULL_EN_0_REG_SET  0x18880100

/* SW_RTC_PULL_EN_0_REG_SET.rtc_gpio_0 - sets pull_select bits of RTC_GPIO_0 pad */
/* sets pull_select bits of RTC_GPIO_0 pad */
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__SHIFT       0
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__MASK        0x00000001
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.rtc_gpio_1 - sets pull_select bits of RTC_GPIO_1 pad */
/* sets pull_select bits of RTC_GPIO_1 pad */
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__SHIFT       2
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__MASK        0x00000004
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.rtc_gpio_2 - sets pull_select bits of RTC_GPIO_2 pad */
/* sets pull_select bits of RTC_GPIO_2 pad */
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT       4
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK        0x00000010
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.rtc_gpio_3 - sets pull_select bits of RTC_GPIO_3 pad */
/* sets pull_select bits of RTC_GPIO_3 pad */
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT       6
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK        0x00000040
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.low_bat_ind_b - sets pull_select bits of LOW_BAT_IND_B pad */
/* sets pull_select bits of LOW_BAT_IND_B pad */
#define SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__SHIFT       8
#define SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__MASK        0x00000100
#define SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__INV_MASK    0xFFFFFEFF
#define SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.on_key_b - sets pull_select bits of ON_KEY_B pad */
/* sets pull_select bits of ON_KEY_B pad */
#define SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__SHIFT       10
#define SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__MASK        0x00000400
#define SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__INV_MASK    0xFFFFFBFF
#define SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.ext_on - sets pull_select bits of EXT_ON pad */
/* sets pull_select bits of EXT_ON pad */
#define SW_RTC_PULL_EN_0_REG_SET__EXT_ON__SHIFT       12
#define SW_RTC_PULL_EN_0_REG_SET__EXT_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__EXT_ON__MASK        0x00001000
#define SW_RTC_PULL_EN_0_REG_SET__EXT_ON__INV_MASK    0xFFFFEFFF
#define SW_RTC_PULL_EN_0_REG_SET__EXT_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.mem_on - sets pull_select bits of MEM_ON pad */
/* sets pull_select bits of MEM_ON pad */
#define SW_RTC_PULL_EN_0_REG_SET__MEM_ON__SHIFT       14
#define SW_RTC_PULL_EN_0_REG_SET__MEM_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__MEM_ON__MASK        0x00004000
#define SW_RTC_PULL_EN_0_REG_SET__MEM_ON__INV_MASK    0xFFFFBFFF
#define SW_RTC_PULL_EN_0_REG_SET__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.core_on - sets pull_select bits of CORE_ON pad */
/* sets pull_select bits of CORE_ON pad */
#define SW_RTC_PULL_EN_0_REG_SET__CORE_ON__SHIFT       16
#define SW_RTC_PULL_EN_0_REG_SET__CORE_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__CORE_ON__MASK        0x00010000
#define SW_RTC_PULL_EN_0_REG_SET__CORE_ON__INV_MASK    0xFFFEFFFF
#define SW_RTC_PULL_EN_0_REG_SET__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.io_on - sets pull_select bits of IO_ON pad */
/* sets pull_select bits of IO_ON pad */
#define SW_RTC_PULL_EN_0_REG_SET__IO_ON__SHIFT       18
#define SW_RTC_PULL_EN_0_REG_SET__IO_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__IO_ON__MASK        0x00040000
#define SW_RTC_PULL_EN_0_REG_SET__IO_ON__INV_MASK    0xFFFBFFFF
#define SW_RTC_PULL_EN_0_REG_SET__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.can0_tx - sets pull_select bits of CAN0_TX pad */
/* sets pull_select bits of CAN0_TX pad */
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__SHIFT       20
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__MASK        0x00100000
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__INV_MASK    0xFFEFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.can0_rx - sets pull_select bits of CAN0_RX pad */
/* sets pull_select bits of CAN0_RX pad */
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__SHIFT       22
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__MASK        0x00400000
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__INV_MASK    0xFFBFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.spi0_clk - sets pull_select bits of SPI0_CLK pad */
/* sets pull_select bits of SPI0_CLK pad */
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__SHIFT       24
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__MASK        0x01000000
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__INV_MASK    0xFEFFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.spi0_cs_b - sets pull_select bits of SPI0_CS_B pad */
/* sets pull_select bits of SPI0_CS_B pad */
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__SHIFT       26
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__MASK        0x04000000
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__INV_MASK    0xFBFFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.spi0_io_0 - sets pull_select bits of SPI0_IO_0 pad */
/* sets pull_select bits of SPI0_IO_0 pad */
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__SHIFT       28
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__MASK        0x10000000
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_SET.spi0_io_1 - sets pull_select bits of SPI0_IO_1 pad */
/* sets pull_select bits of SPI0_IO_1 pad */
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__SHIFT       30
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__MASK        0x40000000
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__INV_MASK    0xBFFFFFFF
#define SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR Address */
/* Clears selected bits of SW_RTC_PULL_EN_0_REG register */
#define SW_RTC_PULL_EN_0_REG_CLR  0x18880104

/* SW_RTC_PULL_EN_0_REG_CLR.rtc_gpio_0 - clears pull_select bits of RTC_GPIO_0 pad */
/* clears pull_select bits of RTC_GPIO_0 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__SHIFT       0
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__MASK        0x00000001
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.rtc_gpio_1 - clears pull_select bits of RTC_GPIO_1 pad */
/* clears pull_select bits of RTC_GPIO_1 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__SHIFT       2
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__MASK        0x00000004
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.rtc_gpio_2 - clears pull_select bits of RTC_GPIO_2 pad */
/* clears pull_select bits of RTC_GPIO_2 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__SHIFT       4
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK        0x00000010
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.rtc_gpio_3 - clears pull_select bits of RTC_GPIO_3 pad */
/* clears pull_select bits of RTC_GPIO_3 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__SHIFT       6
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK        0x00000040
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.low_bat_ind_b - clears pull_select bits of LOW_BAT_IND_B pad */
/* clears pull_select bits of LOW_BAT_IND_B pad */
#define SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__SHIFT       8
#define SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__MASK        0x00000100
#define SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__INV_MASK    0xFFFFFEFF
#define SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.on_key_b - clears pull_select bits of ON_KEY_B pad */
/* clears pull_select bits of ON_KEY_B pad */
#define SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__SHIFT       10
#define SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__MASK        0x00000400
#define SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__INV_MASK    0xFFFFFBFF
#define SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.ext_on - clears pull_select bits of EXT_ON pad */
/* clears pull_select bits of EXT_ON pad */
#define SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__SHIFT       12
#define SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__MASK        0x00001000
#define SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__INV_MASK    0xFFFFEFFF
#define SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.mem_on - clears pull_select bits of MEM_ON pad */
/* clears pull_select bits of MEM_ON pad */
#define SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__SHIFT       14
#define SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__MASK        0x00004000
#define SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__INV_MASK    0xFFFFBFFF
#define SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.core_on - clears pull_select bits of CORE_ON pad */
/* clears pull_select bits of CORE_ON pad */
#define SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__SHIFT       16
#define SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__MASK        0x00010000
#define SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__INV_MASK    0xFFFEFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.io_on - clears pull_select bits of IO_ON pad */
/* clears pull_select bits of IO_ON pad */
#define SW_RTC_PULL_EN_0_REG_CLR__IO_ON__SHIFT       18
#define SW_RTC_PULL_EN_0_REG_CLR__IO_ON__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__IO_ON__MASK        0x00040000
#define SW_RTC_PULL_EN_0_REG_CLR__IO_ON__INV_MASK    0xFFFBFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.can0_tx - clears pull_select bits of CAN0_TX pad */
/* clears pull_select bits of CAN0_TX pad */
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__SHIFT       20
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__MASK        0x00100000
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__INV_MASK    0xFFEFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.can0_rx - clears pull_select bits of CAN0_RX pad */
/* clears pull_select bits of CAN0_RX pad */
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__SHIFT       22
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__MASK        0x00400000
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__INV_MASK    0xFFBFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.spi0_clk - clears pull_select bits of SPI0_CLK pad */
/* clears pull_select bits of SPI0_CLK pad */
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__SHIFT       24
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__MASK        0x01000000
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__INV_MASK    0xFEFFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.spi0_cs_b - clears pull_select bits of SPI0_CS_B pad */
/* clears pull_select bits of SPI0_CS_B pad */
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__SHIFT       26
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__MASK        0x04000000
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__INV_MASK    0xFBFFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.spi0_io_0 - clears pull_select bits of SPI0_IO_0 pad */
/* clears pull_select bits of SPI0_IO_0 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__SHIFT       28
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__MASK        0x10000000
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_0_REG_CLR.spi0_io_1 - clears pull_select bits of SPI0_IO_1 pad */
/* clears pull_select bits of SPI0_IO_1 pad */
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__SHIFT       30
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__WIDTH       1
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__MASK        0x40000000
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__INV_MASK    0xBFFFFFFF
#define SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_1_REG_SET Address */
/* Sets selected bits of SW_RTC_PULL_EN_1_REG register */
#define SW_RTC_PULL_EN_1_REG_SET  0x18880108

/* SW_RTC_PULL_EN_1_REG_SET.spi0_io_2 - sets pull_select bits of SPI0_IO_2 pad */
/* sets pull_select bits of SPI0_IO_2 pad */
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__SHIFT       0
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__WIDTH       1
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__MASK        0x00000001
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__INV_MASK    0xFFFFFFFE
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_1_REG_SET.spi0_io_3 - sets pull_select bits of SPI0_IO_3 pad */
/* sets pull_select bits of SPI0_IO_3 pad */
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__SHIFT       2
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__WIDTH       1
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__MASK        0x00000004
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__INV_MASK    0xFFFFFFFB
#define SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_1_REG_CLR Address */
/* Clears selected bits of SW_RTC_PULL_EN_1_REG register */
#define SW_RTC_PULL_EN_1_REG_CLR  0x1888010C

/* SW_RTC_PULL_EN_1_REG_CLR.spi0_io_2 - clears pull_select bits of SPI0_IO_2 pad */
/* clears pull_select bits of SPI0_IO_2 pad */
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__SHIFT       0
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__WIDTH       1
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__MASK        0x00000001
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__INV_MASK    0xFFFFFFFE
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__HW_DEFAULT  0x1

/* SW_RTC_PULL_EN_1_REG_CLR.spi0_io_3 - clears pull_select bits of SPI0_IO_3 pad */
/* clears pull_select bits of SPI0_IO_3 pad */
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__SHIFT       2
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__WIDTH       1
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__MASK        0x00000004
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__INV_MASK    0xFFFFFFFB
#define SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET Address */
/* Sets selected bits of SW_RTC_STRENGTH_CNTL_0_REG register */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET 0x18880200

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.rtc_gpio_0 - sets drive_strength bits of RTC_GPIO_0 pad */
/* sets drive_strength bits of RTC_GPIO_0 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_0__SHIFT       0
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_0__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_0__MASK        0x00000001
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_0__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.rtc_gpio_1 - sets drive_strength bits of RTC_GPIO_1 pad */
/* sets drive_strength bits of RTC_GPIO_1 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_1__SHIFT       2
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_1__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_1__MASK        0x00000004
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_1__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.rtc_gpio_2 - sets drive_strength bits of RTC_GPIO_2 pad */
/* sets drive_strength bits of RTC_GPIO_2 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_2__SHIFT       4
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_2__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_2__MASK        0x00000010
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_2__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.rtc_gpio_3 - sets drive_strength bits of RTC_GPIO_3 pad */
/* sets drive_strength bits of RTC_GPIO_3 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_3__SHIFT       6
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_3__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_3__MASK        0x00000040
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__RTC_GPIO_3__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.low_bat_ind_b - sets drive_strength bits of LOW_BAT_IND_B pad */
/* sets drive_strength bits of LOW_BAT_IND_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__LOW_BAT_IND_B__SHIFT       8
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__LOW_BAT_IND_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__LOW_BAT_IND_B__MASK        0x00000100
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__LOW_BAT_IND_B__INV_MASK    0xFFFFFEFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.on_key_b - sets drive_strength bits of ON_KEY_B pad */
/* sets drive_strength bits of ON_KEY_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__ON_KEY_B__SHIFT       10
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__ON_KEY_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__ON_KEY_B__MASK        0x00000400
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__ON_KEY_B__INV_MASK    0xFFFFFBFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__ON_KEY_B__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.ext_on - sets drive_strength bits of EXT_ON pad */
/* sets drive_strength bits of EXT_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__EXT_ON__SHIFT       12
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__EXT_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__EXT_ON__MASK        0x00001000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__EXT_ON__INV_MASK    0xFFFFEFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__EXT_ON__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.mem_on - sets drive_strength bits of MEM_ON pad */
/* sets drive_strength bits of MEM_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__MEM_ON__SHIFT       14
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__MEM_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__MEM_ON__MASK        0x00004000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__MEM_ON__INV_MASK    0xFFFFBFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.core_on - sets drive_strength bits of CORE_ON pad */
/* sets drive_strength bits of CORE_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CORE_ON__SHIFT       16
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CORE_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CORE_ON__MASK        0x00010000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CORE_ON__INV_MASK    0xFFFEFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.io_on - sets drive_strength bits of IO_ON pad */
/* sets drive_strength bits of IO_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__IO_ON__SHIFT       18
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__IO_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__IO_ON__MASK        0x00040000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__IO_ON__INV_MASK    0xFFFBFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.can0_tx - sets drive_strength bits of CAN0_TX pad */
/* sets drive_strength bits of CAN0_TX pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_TX__SHIFT       20
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_TX__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_TX__MASK        0x00100000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_TX__INV_MASK    0xFFEFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_TX__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.can0_rx - sets drive_strength bits of CAN0_RX pad */
/* sets drive_strength bits of CAN0_RX pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_RX__SHIFT       22
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_RX__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_RX__MASK        0x00400000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_RX__INV_MASK    0xFFBFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__CAN0_RX__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.spi0_clk - sets drive_strength bits of SPI0_CLK pad */
/* sets drive_strength bits of SPI0_CLK pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CLK__SHIFT       24
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CLK__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CLK__MASK        0x01000000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CLK__INV_MASK    0xFEFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CLK__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.spi0_cs_b - sets drive_strength bits of SPI0_CS_B pad */
/* sets drive_strength bits of SPI0_CS_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CS_B__SHIFT       26
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CS_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CS_B__MASK        0x04000000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CS_B__INV_MASK    0xFBFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_CS_B__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.spi0_io_0 - sets drive_strength bits of SPI0_IO_0 pad */
/* sets drive_strength bits of SPI0_IO_0 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_0__SHIFT       28
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_0__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_0__MASK        0x10000000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_0__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_SET.spi0_io_1 - sets drive_strength bits of SPI0_IO_1 pad */
/* sets drive_strength bits of SPI0_IO_1 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_1__SHIFT       30
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_1__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_1__MASK        0x40000000
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_1__INV_MASK    0xBFFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_SET__SPI0_IO_1__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR Address */
/* Clears selected bits of SW_RTC_STRENGTH_CNTL_0_REG register */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR 0x18880204

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.rtc_gpio_0 - clears drive_strength bits of RTC_GPIO_0 pad */
/* clears drive_strength bits of RTC_GPIO_0 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_0__SHIFT       0
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_0__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_0__MASK        0x00000001
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_0__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.rtc_gpio_1 - clears drive_strength bits of RTC_GPIO_1 pad */
/* clears drive_strength bits of RTC_GPIO_1 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_1__SHIFT       2
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_1__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_1__MASK        0x00000004
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_1__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.rtc_gpio_2 - clears drive_strength bits of RTC_GPIO_2 pad */
/* clears drive_strength bits of RTC_GPIO_2 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_2__SHIFT       4
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_2__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_2__MASK        0x00000010
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_2__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.rtc_gpio_3 - clears drive_strength bits of RTC_GPIO_3 pad */
/* clears drive_strength bits of RTC_GPIO_3 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_3__SHIFT       6
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_3__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_3__MASK        0x00000040
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__RTC_GPIO_3__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.low_bat_ind_b - clears drive_strength bits of LOW_BAT_IND_B pad */
/* clears drive_strength bits of LOW_BAT_IND_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__LOW_BAT_IND_B__SHIFT       8
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__LOW_BAT_IND_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__LOW_BAT_IND_B__MASK        0x00000100
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__LOW_BAT_IND_B__INV_MASK    0xFFFFFEFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__LOW_BAT_IND_B__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.on_key_b - clears drive_strength bits of ON_KEY_B pad */
/* clears drive_strength bits of ON_KEY_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__ON_KEY_B__SHIFT       10
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__ON_KEY_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__ON_KEY_B__MASK        0x00000400
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__ON_KEY_B__INV_MASK    0xFFFFFBFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__ON_KEY_B__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.ext_on - clears drive_strength bits of EXT_ON pad */
/* clears drive_strength bits of EXT_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__EXT_ON__SHIFT       12
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__EXT_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__EXT_ON__MASK        0x00001000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__EXT_ON__INV_MASK    0xFFFFEFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__EXT_ON__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.mem_on - clears drive_strength bits of MEM_ON pad */
/* clears drive_strength bits of MEM_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__MEM_ON__SHIFT       14
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__MEM_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__MEM_ON__MASK        0x00004000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__MEM_ON__INV_MASK    0xFFFFBFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__MEM_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.core_on - clears drive_strength bits of CORE_ON pad */
/* clears drive_strength bits of CORE_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CORE_ON__SHIFT       16
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CORE_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CORE_ON__MASK        0x00010000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CORE_ON__INV_MASK    0xFFFEFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CORE_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.io_on - clears drive_strength bits of IO_ON pad */
/* clears drive_strength bits of IO_ON pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__IO_ON__SHIFT       18
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__IO_ON__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__IO_ON__MASK        0x00040000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__IO_ON__INV_MASK    0xFFFBFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__IO_ON__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.can0_tx - clears drive_strength bits of CAN0_TX pad */
/* clears drive_strength bits of CAN0_TX pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_TX__SHIFT       20
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_TX__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_TX__MASK        0x00100000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_TX__INV_MASK    0xFFEFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_TX__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.can0_rx - clears drive_strength bits of CAN0_RX pad */
/* clears drive_strength bits of CAN0_RX pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_RX__SHIFT       22
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_RX__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_RX__MASK        0x00400000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_RX__INV_MASK    0xFFBFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__CAN0_RX__HW_DEFAULT  0x0

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.spi0_clk - clears drive_strength bits of SPI0_CLK pad */
/* clears drive_strength bits of SPI0_CLK pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CLK__SHIFT       24
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CLK__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CLK__MASK        0x01000000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CLK__INV_MASK    0xFEFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CLK__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.spi0_cs_b - clears drive_strength bits of SPI0_CS_B pad */
/* clears drive_strength bits of SPI0_CS_B pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CS_B__SHIFT       26
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CS_B__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CS_B__MASK        0x04000000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CS_B__INV_MASK    0xFBFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_CS_B__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.spi0_io_0 - clears drive_strength bits of SPI0_IO_0 pad */
/* clears drive_strength bits of SPI0_IO_0 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_0__SHIFT       28
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_0__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_0__MASK        0x10000000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_0__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_0_REG_CLR.spi0_io_1 - clears drive_strength bits of SPI0_IO_1 pad */
/* clears drive_strength bits of SPI0_IO_1 pad */
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_1__SHIFT       30
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_1__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_1__MASK        0x40000000
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_1__INV_MASK    0xBFFFFFFF
#define SW_RTC_STRENGTH_CNTL_0_REG_CLR__SPI0_IO_1__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_1_REG_SET Address */
/* Sets selected bits of SW_RTC_STRENGTH_CNTL_1_REG register */
#define SW_RTC_STRENGTH_CNTL_1_REG_SET 0x18880208

/* SW_RTC_STRENGTH_CNTL_1_REG_SET.spi0_io_2 - sets drive_strength bits of SPI0_IO_2 pad */
/* sets drive_strength bits of SPI0_IO_2 pad */
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_2__SHIFT       0
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_2__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_2__MASK        0x00000001
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_2__INV_MASK    0xFFFFFFFE
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_2__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_1_REG_SET.spi0_io_3 - sets drive_strength bits of SPI0_IO_3 pad */
/* sets drive_strength bits of SPI0_IO_3 pad */
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_3__SHIFT       2
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_3__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_3__MASK        0x00000004
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_3__INV_MASK    0xFFFFFFFB
#define SW_RTC_STRENGTH_CNTL_1_REG_SET__SPI0_IO_3__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_1_REG_CLR Address */
/* Clears selected bits of SW_RTC_STRENGTH_CNTL_1_REG register */
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR 0x1888020C

/* SW_RTC_STRENGTH_CNTL_1_REG_CLR.spi0_io_2 - clears drive_strength bits of SPI0_IO_2 pad */
/* clears drive_strength bits of SPI0_IO_2 pad */
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_2__SHIFT       0
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_2__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_2__MASK        0x00000001
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_2__INV_MASK    0xFFFFFFFE
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_2__HW_DEFAULT  0x1

/* SW_RTC_STRENGTH_CNTL_1_REG_CLR.spi0_io_3 - clears drive_strength bits of SPI0_IO_3 pad */
/* clears drive_strength bits of SPI0_IO_3 pad */
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_3__SHIFT       2
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_3__WIDTH       1
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_3__MASK        0x00000004
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_3__INV_MASK    0xFFFFFFFB
#define SW_RTC_STRENGTH_CNTL_1_REG_CLR__SPI0_IO_3__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_0_REG_SET Address */
/* Sets selected bits of SW_RTC_IN_DISABLE_0_REG register */
#define SW_RTC_IN_DISABLE_0_REG_SET 0x18880A00

/* SW_RTC_IN_DISABLE_0_REG_SET.sd1__sd_dat_1_0 - sets in_disable bits of SD1__SD_DAT_1_0 pad */
/* sets in_disable bits of SD1__SD_DAT_1_0 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__SHIFT       0
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__MASK        0x00000001
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd1__sd_dat_1_1 - sets in_disable bits of SD1__SD_DAT_1_1 pad */
/* sets in_disable bits of SD1__SD_DAT_1_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__SHIFT       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__MASK        0x00000002
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd1__sd_dat_1_2 - sets in_disable bits of SD1__SD_DAT_1_2 pad */
/* sets in_disable bits of SD1__SD_DAT_1_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__SHIFT       2
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__MASK        0x00000004
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd1__sd_dat_1_3 - sets in_disable bits of SD1__SD_DAT_1_3 pad */
/* sets in_disable bits of SD1__SD_DAT_1_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__SHIFT       3
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__MASK        0x00000008
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.c1__can_rxd_1 - sets in_disable bits of C1__CAN_RXD_1 pad */
/* sets in_disable bits of C1__CAN_RXD_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__SHIFT       4
#define SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__MASK        0x00000010
#define SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_0_REG_SET.u3__rxd_3 - sets in_disable bits of U3__RXD_3 pad */
/* sets in_disable bits of U3__RXD_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__SHIFT       5
#define SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__MASK        0x00000020
#define SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.gn__trg_acq_clk - sets in_disable bits of GN__TRG_ACQ_CLK pad */
/* sets in_disable bits of GN__TRG_ACQ_CLK pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__SHIFT       6
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__MASK        0x00000040
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.gn__trg_acq_d0 - sets in_disable bits of GN__TRG_ACQ_D0 pad */
/* sets in_disable bits of GN__TRG_ACQ_D0 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__SHIFT       7
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__MASK        0x00000080
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.gn__trg_acq_d1 - sets in_disable bits of GN__TRG_ACQ_D1 pad */
/* sets in_disable bits of GN__TRG_ACQ_D1 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__SHIFT       8
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__MASK        0x00000100
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.gn__trg_irq_b - sets in_disable bits of GN__TRG_IRQ_B pad */
/* sets in_disable bits of GN__TRG_IRQ_B pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__SHIFT       9
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__MASK        0x00000200
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_0_REG_SET.gn__trg_spi_di - sets in_disable bits of GN__TRG_SPI_DI pad */
/* sets in_disable bits of GN__TRG_SPI_DI pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__SHIFT       10
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__MASK        0x00000400
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.au__utfs_2 - sets in_disable bits of AU__UTFS_2 pad */
/* sets in_disable bits of AU__UTFS_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__SHIFT       22
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__MASK        0x00400000
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__INV_MASK    0xFFBFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.au__usclk_2 - sets in_disable bits of AU__USCLK_2 pad */
/* sets in_disable bits of AU__USCLK_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__SHIFT       23
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__MASK        0x00800000
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__INV_MASK    0xFF7FFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.au__urxd_2 - sets in_disable bits of AU__URXD_2 pad */
/* sets in_disable bits of AU__URXD_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__SHIFT       24
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__MASK        0x01000000
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__INV_MASK    0xFEFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.au__utxd_2 - sets in_disable bits of AU__UTXD_2 pad */
/* sets in_disable bits of AU__UTXD_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__SHIFT       25
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__MASK        0x02000000
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__INV_MASK    0xFDFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_cmd_6 - sets in_disable bits of SD6__SD_CMD_6 pad */
/* sets in_disable bits of SD6__SD_CMD_6 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__SHIFT       26
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__MASK        0x04000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__INV_MASK    0xFBFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_clk_6 - sets in_disable bits of SD6__SD_CLK_6 pad */
/* sets in_disable bits of SD6__SD_CLK_6 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__SHIFT       27
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__MASK        0x08000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__INV_MASK    0xF7FFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_dat_6_0 - sets in_disable bits of SD6__SD_DAT_6_0 pad */
/* sets in_disable bits of SD6__SD_DAT_6_0 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__SHIFT       28
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__MASK        0x10000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_dat_6_1 - sets in_disable bits of SD6__SD_DAT_6_1 pad */
/* sets in_disable bits of SD6__SD_DAT_6_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__SHIFT       29
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__MASK        0x20000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__INV_MASK    0xDFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_dat_6_2 - sets in_disable bits of SD6__SD_DAT_6_2 pad */
/* sets in_disable bits of SD6__SD_DAT_6_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__SHIFT       30
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__MASK        0x40000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__INV_MASK    0xBFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_SET.sd6__sd_dat_6_3 - sets in_disable bits of SD6__SD_DAT_6_3 pad */
/* sets in_disable bits of SD6__SD_DAT_6_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__SHIFT       31
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__MASK        0x80000000
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__INV_MASK    0x7FFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR Address */
/* Clears selected bits of SW_RTC_IN_DISABLE_0_REG register */
#define SW_RTC_IN_DISABLE_0_REG_CLR 0x18880A04

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd1__sd_dat_1_0 - clears in_disable bits of SD1__SD_DAT_1_0 pad */
/* clears in_disable bits of SD1__SD_DAT_1_0 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__SHIFT       0
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__MASK        0x00000001
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd1__sd_dat_1_1 - clears in_disable bits of SD1__SD_DAT_1_1 pad */
/* clears in_disable bits of SD1__SD_DAT_1_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__SHIFT       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__MASK        0x00000002
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd1__sd_dat_1_2 - clears in_disable bits of SD1__SD_DAT_1_2 pad */
/* clears in_disable bits of SD1__SD_DAT_1_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__SHIFT       2
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__MASK        0x00000004
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd1__sd_dat_1_3 - clears in_disable bits of SD1__SD_DAT_1_3 pad */
/* clears in_disable bits of SD1__SD_DAT_1_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__SHIFT       3
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__MASK        0x00000008
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.c1__can_rxd_1 - clears in_disable bits of C1__CAN_RXD_1 pad */
/* clears in_disable bits of C1__CAN_RXD_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__SHIFT       4
#define SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__MASK        0x00000010
#define SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_0_REG_CLR.u3__rxd_3 - clears in_disable bits of U3__RXD_3 pad */
/* clears in_disable bits of U3__RXD_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__SHIFT       5
#define SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__MASK        0x00000020
#define SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.gn__trg_acq_clk - clears in_disable bits of GN__TRG_ACQ_CLK pad */
/* clears in_disable bits of GN__TRG_ACQ_CLK pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__SHIFT       6
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__MASK        0x00000040
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.gn__trg_acq_d0 - clears in_disable bits of GN__TRG_ACQ_D0 pad */
/* clears in_disable bits of GN__TRG_ACQ_D0 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__SHIFT       7
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__MASK        0x00000080
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.gn__trg_acq_d1 - clears in_disable bits of GN__TRG_ACQ_D1 pad */
/* clears in_disable bits of GN__TRG_ACQ_D1 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__SHIFT       8
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__MASK        0x00000100
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.gn__trg_irq_b - clears in_disable bits of GN__TRG_IRQ_B pad */
/* clears in_disable bits of GN__TRG_IRQ_B pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__SHIFT       9
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__MASK        0x00000200
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_0_REG_CLR.gn__trg_spi_di - clears in_disable bits of GN__TRG_SPI_DI pad */
/* clears in_disable bits of GN__TRG_SPI_DI pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__SHIFT       10
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__MASK        0x00000400
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.au__utfs_2 - clears in_disable bits of AU__UTFS_2 pad */
/* clears in_disable bits of AU__UTFS_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__SHIFT       22
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__MASK        0x00400000
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__INV_MASK    0xFFBFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.au__usclk_2 - clears in_disable bits of AU__USCLK_2 pad */
/* clears in_disable bits of AU__USCLK_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__SHIFT       23
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__MASK        0x00800000
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__INV_MASK    0xFF7FFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.au__urxd_2 - clears in_disable bits of AU__URXD_2 pad */
/* clears in_disable bits of AU__URXD_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__SHIFT       24
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__MASK        0x01000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__INV_MASK    0xFEFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.au__utxd_2 - clears in_disable bits of AU__UTXD_2 pad */
/* clears in_disable bits of AU__UTXD_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__SHIFT       25
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__MASK        0x02000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__INV_MASK    0xFDFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_cmd_6 - clears in_disable bits of SD6__SD_CMD_6 pad */
/* clears in_disable bits of SD6__SD_CMD_6 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__SHIFT       26
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__MASK        0x04000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__INV_MASK    0xFBFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_clk_6 - clears in_disable bits of SD6__SD_CLK_6 pad */
/* clears in_disable bits of SD6__SD_CLK_6 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__SHIFT       27
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__MASK        0x08000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__INV_MASK    0xF7FFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_dat_6_0 - clears in_disable bits of SD6__SD_DAT_6_0 pad */
/* clears in_disable bits of SD6__SD_DAT_6_0 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__SHIFT       28
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__MASK        0x10000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_dat_6_1 - clears in_disable bits of SD6__SD_DAT_6_1 pad */
/* clears in_disable bits of SD6__SD_DAT_6_1 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__SHIFT       29
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__MASK        0x20000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__INV_MASK    0xDFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_dat_6_2 - clears in_disable bits of SD6__SD_DAT_6_2 pad */
/* clears in_disable bits of SD6__SD_DAT_6_2 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__SHIFT       30
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__MASK        0x40000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__INV_MASK    0xBFFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_0_REG_CLR.sd6__sd_dat_6_3 - clears in_disable bits of SD6__SD_DAT_6_3 pad */
/* clears in_disable bits of SD6__SD_DAT_6_3 pad */
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__SHIFT       31
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__WIDTH       1
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__MASK        0x80000000
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__INV_MASK    0x7FFFFFFF
#define SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_SET Address */
/* Sets selected bits of SW_RTC_IN_DISABLE_1_REG register */
#define SW_RTC_IN_DISABLE_1_REG_SET 0x18880A08

/* SW_RTC_IN_DISABLE_1_REG_SET.u3__cts_3 - sets in_disable bits of U3__CTS_3 pad */
/* sets in_disable bits of U3__CTS_3 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__SHIFT       0
#define SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__MASK        0x00000001
#define SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_SET.u4__cts_4 - sets in_disable bits of U4__CTS_4 pad */
/* sets in_disable bits of U4__CTS_4 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__SHIFT       1
#define SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__MASK        0x00000002
#define SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_SET.jtag__jt_dbg_nsrst - sets in_disable bits of JTAG__JT_DBG_NSRST pad */
/* sets in_disable bits of JTAG__JT_DBG_NSRST pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__SHIFT       2
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__MASK        0x00000004
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.jtag__ntrst - sets in_disable bits of JTAG__NTRST pad */
/* sets in_disable bits of JTAG__NTRST pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__SHIFT       3
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__MASK        0x00000008
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.pwc__lowbatt_b - sets in_disable bits of PWC__LOWBATT_B pad */
/* sets in_disable bits of PWC__LOWBATT_B pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__SHIFT       4
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__MASK        0x00000010
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.pwc__on_key_b - sets in_disable bits of PWC__ON_KEY_B pad */
/* sets in_disable bits of PWC__ON_KEY_B pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__SHIFT       5
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__MASK        0x00000020
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_SET.ca__spi_func_csb - sets in_disable bits of CA__SPI_FUNC_CSB pad */
/* sets in_disable bits of CA__SPI_FUNC_CSB pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__SHIFT       6
#define SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__MASK        0x00000040
#define SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.sd2__sd_cd_b_2 - sets in_disable bits of SD2__SD_CD_B_2 pad */
/* sets in_disable bits of SD2__SD_CD_B_2 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__SHIFT       7
#define SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__MASK        0x00000080
#define SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.ks__kas_spi_cs_n - sets in_disable bits of KS__KAS_SPI_CS_N pad */
/* sets in_disable bits of KS__KAS_SPI_CS_N pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__SHIFT       8
#define SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__MASK        0x00000100
#define SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.c0__can_rxd_0 - sets in_disable bits of C0__CAN_RXD_0 pad */
/* sets in_disable bits of C0__CAN_RXD_0 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0__SHIFT       9
#define SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0__MASK        0x00000200
#define SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.gn__gnss_irq1 - sets in_disable bits of GN__GNSS_IRQ1 pad */
/* sets in_disable bits of GN__GNSS_IRQ1 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__SHIFT       10
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__MASK        0x00000400
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.gn__gnss_irq2 - sets in_disable bits of GN__GNSS_IRQ2 pad */
/* sets in_disable bits of GN__GNSS_IRQ2 pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__SHIFT       11
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__MASK        0x00000800
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__INV_MASK    0xFFFFF7FF
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.gn__gnss_m0_porst_b - sets in_disable bits of GN__GNSS_M0_PORST_B pad */
/* sets in_disable bits of GN__GNSS_M0_PORST_B pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__SHIFT       12
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__MASK        0x00001000
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__INV_MASK    0xFFFFEFFF
#define SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.rg__gmac_phy_intr_n - sets in_disable bits of RG__GMAC_PHY_INTR_N pad */
/* sets in_disable bits of RG__GMAC_PHY_INTR_N pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__SHIFT       13
#define SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__MASK        0x00002000
#define SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__INV_MASK    0xFFFFDFFF
#define SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_SET.clkc__trg_ref_clk - sets in_disable bits of CLKC__TRG_REF_CLK pad */
/* sets in_disable bits of CLKC__TRG_REF_CLK pad */
#define SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__SHIFT       14
#define SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__MASK        0x00004000
#define SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__INV_MASK    0xFFFFBFFF
#define SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_CLR Address */
/* Clears selected bits of SW_RTC_IN_DISABLE_1_REG register */
#define SW_RTC_IN_DISABLE_1_REG_CLR 0x18880A0C

/* SW_RTC_IN_DISABLE_1_REG_CLR.u3__cts_3 - clears in_disable bits of U3__CTS_3 pad */
/* clears in_disable bits of U3__CTS_3 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__SHIFT       0
#define SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__MASK        0x00000001
#define SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_CLR.u4__cts_4 - clears in_disable bits of U4__CTS_4 pad */
/* clears in_disable bits of U4__CTS_4 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__SHIFT       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__MASK        0x00000002
#define SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_CLR.jtag__jt_dbg_nsrst - clears in_disable bits of JTAG__JT_DBG_NSRST pad */
/* clears in_disable bits of JTAG__JT_DBG_NSRST pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__SHIFT       2
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__MASK        0x00000004
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.jtag__ntrst - clears in_disable bits of JTAG__NTRST pad */
/* clears in_disable bits of JTAG__NTRST pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__SHIFT       3
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__MASK        0x00000008
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.pwc__lowbatt_b - clears in_disable bits of PWC__LOWBATT_B pad */
/* clears in_disable bits of PWC__LOWBATT_B pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__SHIFT       4
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__MASK        0x00000010
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.pwc__on_key_b - clears in_disable bits of PWC__ON_KEY_B pad */
/* clears in_disable bits of PWC__ON_KEY_B pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__SHIFT       5
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__MASK        0x00000020
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_1_REG_CLR.ca__spi_func_csb - clears in_disable bits of CA__SPI_FUNC_CSB pad */
/* clears in_disable bits of CA__SPI_FUNC_CSB pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__SHIFT       6
#define SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__MASK        0x00000040
#define SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.sd2__sd_cd_b_2 - clears in_disable bits of SD2__SD_CD_B_2 pad */
/* clears in_disable bits of SD2__SD_CD_B_2 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__SHIFT       7
#define SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__MASK        0x00000080
#define SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.ks__kas_spi_cs_n - clears in_disable bits of KS__KAS_SPI_CS_N pad */
/* clears in_disable bits of KS__KAS_SPI_CS_N pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__SHIFT       8
#define SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__MASK        0x00000100
#define SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.c0__can_rxd_0 - clears in_disable bits of C0__CAN_RXD_0 pad */
/* clears in_disable bits of C0__CAN_RXD_0 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0__SHIFT       9
#define SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0__MASK        0x00000200
#define SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.gn__gnss_irq1 - clears in_disable bits of GN__GNSS_IRQ1 pad */
/* clears in_disable bits of GN__GNSS_IRQ1 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__SHIFT       10
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__MASK        0x00000400
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.gn__gnss_irq2 - clears in_disable bits of GN__GNSS_IRQ2 pad */
/* clears in_disable bits of GN__GNSS_IRQ2 pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__SHIFT       11
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__MASK        0x00000800
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__INV_MASK    0xFFFFF7FF
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.gn__gnss_m0_porst_b - clears in_disable bits of GN__GNSS_M0_PORST_B pad */
/* clears in_disable bits of GN__GNSS_M0_PORST_B pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__SHIFT       12
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__MASK        0x00001000
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__INV_MASK    0xFFFFEFFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.rg__gmac_phy_intr_n - clears in_disable bits of RG__GMAC_PHY_INTR_N pad */
/* clears in_disable bits of RG__GMAC_PHY_INTR_N pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__SHIFT       13
#define SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__MASK        0x00002000
#define SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__INV_MASK    0xFFFFDFFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_1_REG_CLR.clkc__trg_ref_clk - clears in_disable bits of CLKC__TRG_REF_CLK pad */
/* clears in_disable bits of CLKC__TRG_REF_CLK pad */
#define SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__SHIFT       14
#define SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__MASK        0x00004000
#define SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__INV_MASK    0xFFFFBFFF
#define SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET Address */
/* Sets selected bits of SW_RTC_IN_DISABLE_VAL_0_REG register */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET 0x18880A80

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd1__sd_dat_1_0 - sets in_disable_val bits of SD1__SD_DAT_1_0 pad */
/* sets in_disable_val bits of SD1__SD_DAT_1_0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__SHIFT       0
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__MASK        0x00000001
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd1__sd_dat_1_1 - sets in_disable_val bits of SD1__SD_DAT_1_1 pad */
/* sets in_disable_val bits of SD1__SD_DAT_1_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__SHIFT       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__MASK        0x00000002
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd1__sd_dat_1_2 - sets in_disable_val bits of SD1__SD_DAT_1_2 pad */
/* sets in_disable_val bits of SD1__SD_DAT_1_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__SHIFT       2
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__MASK        0x00000004
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd1__sd_dat_1_3 - sets in_disable_val bits of SD1__SD_DAT_1_3 pad */
/* sets in_disable_val bits of SD1__SD_DAT_1_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__SHIFT       3
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__MASK        0x00000008
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.c1__can_rxd_1 - sets in_disable_val bits of C1__CAN_RXD_1 pad */
/* sets in_disable_val bits of C1__CAN_RXD_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__SHIFT       4
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__MASK        0x00000010
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.u3__rxd_3 - sets in_disable_val bits of U3__RXD_3 pad */
/* sets in_disable_val bits of U3__RXD_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__SHIFT       5
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__MASK        0x00000020
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.gn__trg_acq_clk - sets in_disable_val bits of GN__TRG_ACQ_CLK pad */
/* sets in_disable_val bits of GN__TRG_ACQ_CLK pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__SHIFT       6
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__MASK        0x00000040
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.gn__trg_acq_d0 - sets in_disable_val bits of GN__TRG_ACQ_D0 pad */
/* sets in_disable_val bits of GN__TRG_ACQ_D0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__SHIFT       7
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__MASK        0x00000080
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.gn__trg_acq_d1 - sets in_disable_val bits of GN__TRG_ACQ_D1 pad */
/* sets in_disable_val bits of GN__TRG_ACQ_D1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__SHIFT       8
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__MASK        0x00000100
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.gn__trg_irq_b - sets in_disable_val bits of GN__TRG_IRQ_B pad */
/* sets in_disable_val bits of GN__TRG_IRQ_B pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__SHIFT       9
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__MASK        0x00000200
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.gn__trg_spi_di - sets in_disable_val bits of GN__TRG_SPI_DI pad */
/* sets in_disable_val bits of GN__TRG_SPI_DI pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__SHIFT       10
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__MASK        0x00000400
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.au__utfs_2 - sets in_disable_val bits of AU__UTFS_2 pad */
/* sets in_disable_val bits of AU__UTFS_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__SHIFT       22
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__MASK        0x00400000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__INV_MASK    0xFFBFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.au__usclk_2 - sets in_disable_val bits of AU__USCLK_2 pad */
/* sets in_disable_val bits of AU__USCLK_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__SHIFT       23
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__MASK        0x00800000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__INV_MASK    0xFF7FFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.au__urxd_2 - sets in_disable_val bits of AU__URXD_2 pad */
/* sets in_disable_val bits of AU__URXD_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__SHIFT       24
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__MASK        0x01000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__INV_MASK    0xFEFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.au__utxd_2 - sets in_disable_val bits of AU__UTXD_2 pad */
/* sets in_disable_val bits of AU__UTXD_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__SHIFT       25
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__MASK        0x02000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__INV_MASK    0xFDFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_cmd_6 - sets in_disable_val bits of SD6__SD_CMD_6 pad */
/* sets in_disable_val bits of SD6__SD_CMD_6 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__SHIFT       26
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__MASK        0x04000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__INV_MASK    0xFBFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_clk_6 - sets in_disable_val bits of SD6__SD_CLK_6 pad */
/* sets in_disable_val bits of SD6__SD_CLK_6 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__SHIFT       27
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__MASK        0x08000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__INV_MASK    0xF7FFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_dat_6_0 - sets in_disable_val bits of SD6__SD_DAT_6_0 pad */
/* sets in_disable_val bits of SD6__SD_DAT_6_0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__SHIFT       28
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__MASK        0x10000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_dat_6_1 - sets in_disable_val bits of SD6__SD_DAT_6_1 pad */
/* sets in_disable_val bits of SD6__SD_DAT_6_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__SHIFT       29
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__MASK        0x20000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__INV_MASK    0xDFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_dat_6_2 - sets in_disable_val bits of SD6__SD_DAT_6_2 pad */
/* sets in_disable_val bits of SD6__SD_DAT_6_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__SHIFT       30
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__MASK        0x40000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__INV_MASK    0xBFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_SET.sd6__sd_dat_6_3 - sets in_disable_val bits of SD6__SD_DAT_6_3 pad */
/* sets in_disable_val bits of SD6__SD_DAT_6_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__SHIFT       31
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__MASK        0x80000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__INV_MASK    0x7FFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR Address */
/* Clears selected bits of SW_RTC_IN_DISABLE_VAL_0_REG register */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR 0x18880A84

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd1__sd_dat_1_0 - clears in_disable_val bits of SD1__SD_DAT_1_0 pad */
/* clears in_disable_val bits of SD1__SD_DAT_1_0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__SHIFT       0
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__MASK        0x00000001
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd1__sd_dat_1_1 - clears in_disable_val bits of SD1__SD_DAT_1_1 pad */
/* clears in_disable_val bits of SD1__SD_DAT_1_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__SHIFT       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__MASK        0x00000002
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd1__sd_dat_1_2 - clears in_disable_val bits of SD1__SD_DAT_1_2 pad */
/* clears in_disable_val bits of SD1__SD_DAT_1_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__SHIFT       2
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__MASK        0x00000004
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd1__sd_dat_1_3 - clears in_disable_val bits of SD1__SD_DAT_1_3 pad */
/* clears in_disable_val bits of SD1__SD_DAT_1_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__SHIFT       3
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__MASK        0x00000008
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.c1__can_rxd_1 - clears in_disable_val bits of C1__CAN_RXD_1 pad */
/* clears in_disable_val bits of C1__CAN_RXD_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__SHIFT       4
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__MASK        0x00000010
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.u3__rxd_3 - clears in_disable_val bits of U3__RXD_3 pad */
/* clears in_disable_val bits of U3__RXD_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__SHIFT       5
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__MASK        0x00000020
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.gn__trg_acq_clk - clears in_disable_val bits of GN__TRG_ACQ_CLK pad */
/* clears in_disable_val bits of GN__TRG_ACQ_CLK pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__SHIFT       6
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__MASK        0x00000040
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.gn__trg_acq_d0 - clears in_disable_val bits of GN__TRG_ACQ_D0 pad */
/* clears in_disable_val bits of GN__TRG_ACQ_D0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__SHIFT       7
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__MASK        0x00000080
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.gn__trg_acq_d1 - clears in_disable_val bits of GN__TRG_ACQ_D1 pad */
/* clears in_disable_val bits of GN__TRG_ACQ_D1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__SHIFT       8
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__MASK        0x00000100
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.gn__trg_irq_b - clears in_disable_val bits of GN__TRG_IRQ_B pad */
/* clears in_disable_val bits of GN__TRG_IRQ_B pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__SHIFT       9
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__MASK        0x00000200
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.gn__trg_spi_di - clears in_disable_val bits of GN__TRG_SPI_DI pad */
/* clears in_disable_val bits of GN__TRG_SPI_DI pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__SHIFT       10
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__MASK        0x00000400
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.au__utfs_2 - clears in_disable_val bits of AU__UTFS_2 pad */
/* clears in_disable_val bits of AU__UTFS_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__SHIFT       22
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__MASK        0x00400000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__INV_MASK    0xFFBFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.au__usclk_2 - clears in_disable_val bits of AU__USCLK_2 pad */
/* clears in_disable_val bits of AU__USCLK_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__SHIFT       23
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__MASK        0x00800000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__INV_MASK    0xFF7FFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.au__urxd_2 - clears in_disable_val bits of AU__URXD_2 pad */
/* clears in_disable_val bits of AU__URXD_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__SHIFT       24
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__MASK        0x01000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__INV_MASK    0xFEFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.au__utxd_2 - clears in_disable_val bits of AU__UTXD_2 pad */
/* clears in_disable_val bits of AU__UTXD_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__SHIFT       25
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__MASK        0x02000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__INV_MASK    0xFDFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_cmd_6 - clears in_disable_val bits of SD6__SD_CMD_6 pad */
/* clears in_disable_val bits of SD6__SD_CMD_6 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__SHIFT       26
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__MASK        0x04000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__INV_MASK    0xFBFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_clk_6 - clears in_disable_val bits of SD6__SD_CLK_6 pad */
/* clears in_disable_val bits of SD6__SD_CLK_6 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__SHIFT       27
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__MASK        0x08000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__INV_MASK    0xF7FFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_dat_6_0 - clears in_disable_val bits of SD6__SD_DAT_6_0 pad */
/* clears in_disable_val bits of SD6__SD_DAT_6_0 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__SHIFT       28
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__MASK        0x10000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__INV_MASK    0xEFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_dat_6_1 - clears in_disable_val bits of SD6__SD_DAT_6_1 pad */
/* clears in_disable_val bits of SD6__SD_DAT_6_1 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__SHIFT       29
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__MASK        0x20000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__INV_MASK    0xDFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_dat_6_2 - clears in_disable_val bits of SD6__SD_DAT_6_2 pad */
/* clears in_disable_val bits of SD6__SD_DAT_6_2 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__SHIFT       30
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__MASK        0x40000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__INV_MASK    0xBFFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_0_REG_CLR.sd6__sd_dat_6_3 - clears in_disable_val bits of SD6__SD_DAT_6_3 pad */
/* clears in_disable_val bits of SD6__SD_DAT_6_3 pad */
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__SHIFT       31
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__MASK        0x80000000
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__INV_MASK    0x7FFFFFFF
#define SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET Address */
/* Sets selected bits of SW_RTC_IN_DISABLE_VAL_1_REG register */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET 0x18880A88

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.u3__cts_3 - sets in_disable_val bits of U3__CTS_3 pad */
/* sets in_disable_val bits of U3__CTS_3 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__SHIFT       0
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__MASK        0x00000001
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.u4__cts_4 - sets in_disable_val bits of U4__CTS_4 pad */
/* sets in_disable_val bits of U4__CTS_4 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__SHIFT       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__MASK        0x00000002
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.jtag__jt_dbg_nsrst - sets in_disable_val bits of JTAG__JT_DBG_NSRST pad */
/* sets in_disable_val bits of JTAG__JT_DBG_NSRST pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__SHIFT       2
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__MASK        0x00000004
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.jtag__ntrst - sets in_disable_val bits of JTAG__NTRST pad */
/* sets in_disable_val bits of JTAG__NTRST pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__SHIFT       3
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__MASK        0x00000008
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.pwc__lowbatt_b - sets in_disable_val bits of PWC__LOWBATT_B pad */
/* sets in_disable_val bits of PWC__LOWBATT_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__SHIFT       4
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__MASK        0x00000010
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.pwc__on_key_b - sets in_disable_val bits of PWC__ON_KEY_B pad */
/* sets in_disable_val bits of PWC__ON_KEY_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__SHIFT       5
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__MASK        0x00000020
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.ca__spi_func_csb - sets in_disable_val bits of CA__SPI_FUNC_CSB pad */
/* sets in_disable_val bits of CA__SPI_FUNC_CSB pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__SHIFT       6
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__MASK        0x00000040
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.sd2__sd_cd_b_2 - sets in_disable_val bits of SD2__SD_CD_B_2 pad */
/* sets in_disable_val bits of SD2__SD_CD_B_2 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__SHIFT       7
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__MASK        0x00000080
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.ks__kas_spi_cs_n - sets in_disable_val bits of KS__KAS_SPI_CS_N pad */
/* sets in_disable_val bits of KS__KAS_SPI_CS_N pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__SHIFT       8
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__MASK        0x00000100
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.c0__can_rxd_0 - sets in_disable_val bits of C0__CAN_RXD_0 pad */
/* sets in_disable_val bits of C0__CAN_RXD_0 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0__SHIFT       9
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0__MASK        0x00000200
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.gn__gnss_irq1 - sets in_disable_val bits of GN__GNSS_IRQ1 pad */
/* sets in_disable_val bits of GN__GNSS_IRQ1 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__SHIFT       10
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__MASK        0x00000400
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.gn__gnss_irq2 - sets in_disable_val bits of GN__GNSS_IRQ2 pad */
/* sets in_disable_val bits of GN__GNSS_IRQ2 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__SHIFT       11
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__MASK        0x00000800
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__INV_MASK    0xFFFFF7FF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.gn__gnss_m0_porst_b - sets in_disable_val bits of GN__GNSS_M0_PORST_B pad */
/* sets in_disable_val bits of GN__GNSS_M0_PORST_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__SHIFT       12
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__MASK        0x00001000
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__INV_MASK    0xFFFFEFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.rg__gmac_phy_intr_n - sets in_disable_val bits of RG__GMAC_PHY_INTR_N pad */
/* sets in_disable_val bits of RG__GMAC_PHY_INTR_N pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__SHIFT       13
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__MASK        0x00002000
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__INV_MASK    0xFFFFDFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_SET.clkc__trg_ref_clk - sets in_disable_val bits of CLKC__TRG_REF_CLK pad */
/* sets in_disable_val bits of CLKC__TRG_REF_CLK pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__SHIFT       14
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__MASK        0x00004000
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__INV_MASK    0xFFFFBFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR Address */
/* Clears selected bits of SW_RTC_IN_DISABLE_VAL_1_REG register */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR 0x18880A8C

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.u3__cts_3 - clears in_disable_val bits of U3__CTS_3 pad */
/* clears in_disable_val bits of U3__CTS_3 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__SHIFT       0
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__MASK        0x00000001
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__INV_MASK    0xFFFFFFFE
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.u4__cts_4 - clears in_disable_val bits of U4__CTS_4 pad */
/* clears in_disable_val bits of U4__CTS_4 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__SHIFT       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__MASK        0x00000002
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__INV_MASK    0xFFFFFFFD
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__HW_DEFAULT  0x0

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.jtag__jt_dbg_nsrst - clears in_disable_val bits of JTAG__JT_DBG_NSRST pad */
/* clears in_disable_val bits of JTAG__JT_DBG_NSRST pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__SHIFT       2
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__MASK        0x00000004
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__INV_MASK    0xFFFFFFFB
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.jtag__ntrst - clears in_disable_val bits of JTAG__NTRST pad */
/* clears in_disable_val bits of JTAG__NTRST pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__SHIFT       3
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__MASK        0x00000008
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__INV_MASK    0xFFFFFFF7
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.pwc__lowbatt_b - clears in_disable_val bits of PWC__LOWBATT_B pad */
/* clears in_disable_val bits of PWC__LOWBATT_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__SHIFT       4
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__MASK        0x00000010
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__INV_MASK    0xFFFFFFEF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.pwc__on_key_b - clears in_disable_val bits of PWC__ON_KEY_B pad */
/* clears in_disable_val bits of PWC__ON_KEY_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__SHIFT       5
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__MASK        0x00000020
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__INV_MASK    0xFFFFFFDF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.ca__spi_func_csb - clears in_disable_val bits of CA__SPI_FUNC_CSB pad */
/* clears in_disable_val bits of CA__SPI_FUNC_CSB pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__SHIFT       6
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__MASK        0x00000040
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__INV_MASK    0xFFFFFFBF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.sd2__sd_cd_b_2 - clears in_disable_val bits of SD2__SD_CD_B_2 pad */
/* clears in_disable_val bits of SD2__SD_CD_B_2 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__SHIFT       7
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__MASK        0x00000080
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__INV_MASK    0xFFFFFF7F
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.ks__kas_spi_cs_n - clears in_disable_val bits of KS__KAS_SPI_CS_N pad */
/* clears in_disable_val bits of KS__KAS_SPI_CS_N pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__SHIFT       8
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__MASK        0x00000100
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__INV_MASK    0xFFFFFEFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.c0__can_rxd_0 - clears in_disable_val bits of C0__CAN_RXD_0 pad */
/* clears in_disable_val bits of C0__CAN_RXD_0 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0__SHIFT       9
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0__MASK        0x00000200
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0__INV_MASK    0xFFFFFDFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.gn__gnss_irq1 - clears in_disable_val bits of GN__GNSS_IRQ1 pad */
/* clears in_disable_val bits of GN__GNSS_IRQ1 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__SHIFT       10
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__MASK        0x00000400
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__INV_MASK    0xFFFFFBFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.gn__gnss_irq2 - clears in_disable_val bits of GN__GNSS_IRQ2 pad */
/* clears in_disable_val bits of GN__GNSS_IRQ2 pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__SHIFT       11
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__MASK        0x00000800
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__INV_MASK    0xFFFFF7FF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.gn__gnss_m0_porst_b - clears in_disable_val bits of GN__GNSS_M0_PORST_B pad */
/* clears in_disable_val bits of GN__GNSS_M0_PORST_B pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__SHIFT       12
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__MASK        0x00001000
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__INV_MASK    0xFFFFEFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.rg__gmac_phy_intr_n - clears in_disable_val bits of RG__GMAC_PHY_INTR_N pad */
/* clears in_disable_val bits of RG__GMAC_PHY_INTR_N pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__SHIFT       13
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__MASK        0x00002000
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__INV_MASK    0xFFFFDFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__HW_DEFAULT  0x1

/* SW_RTC_IN_DISABLE_VAL_1_REG_CLR.clkc__trg_ref_clk - clears in_disable_val bits of CLKC__TRG_REF_CLK pad */
/* clears in_disable_val bits of CLKC__TRG_REF_CLK pad */
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__SHIFT       14
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__WIDTH       1
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__MASK        0x00004000
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__INV_MASK    0xFFFFBFFF
#define SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__HW_DEFAULT  0x0


#endif  // __IOCRTC_H__
