/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __CPURTCIO_H__
#define __CPURTCIO_H__



/* CPURTC_IO */
/* ==================================================================== */

/*  */
#define CPURTCIOBG_CTRL           0x18840000

/* CPURTCIOBG_CTRL.CPURTCIOBG_CTRL -  */
#define CPURTCIOBG_CTRL__CPURTCIOBG_CTRL__SHIFT       0
#define CPURTCIOBG_CTRL__CPURTCIOBG_CTRL__WIDTH       1
#define CPURTCIOBG_CTRL__CPURTCIOBG_CTRL__MASK        0x00000001
#define CPURTCIOBG_CTRL__CPURTCIOBG_CTRL__INV_MASK    0xFFFFFFFE
#define CPURTCIOBG_CTRL__CPURTCIOBG_CTRL__HW_DEFAULT  0x0

/*  */
#define CPURTCIOBG_WRBE           0x18840004

/* CPURTCIOBG_WRBE.CPURTCIOBG_WR -  */
#define CPURTCIOBG_WRBE__CPURTCIOBG_WR__SHIFT       0
#define CPURTCIOBG_WRBE__CPURTCIOBG_WR__WIDTH       1
#define CPURTCIOBG_WRBE__CPURTCIOBG_WR__MASK        0x00000001
#define CPURTCIOBG_WRBE__CPURTCIOBG_WR__INV_MASK    0xFFFFFFFE
#define CPURTCIOBG_WRBE__CPURTCIOBG_WR__HW_DEFAULT  0x0

/* CPURTCIOBG_WRBE.CPURTCIOBG_BE -  */
#define CPURTCIOBG_WRBE__CPURTCIOBG_BE__SHIFT       4
#define CPURTCIOBG_WRBE__CPURTCIOBG_BE__WIDTH       4
#define CPURTCIOBG_WRBE__CPURTCIOBG_BE__MASK        0x000000F0
#define CPURTCIOBG_WRBE__CPURTCIOBG_BE__INV_MASK    0xFFFFFF0F
#define CPURTCIOBG_WRBE__CPURTCIOBG_BE__HW_DEFAULT  0x0

/*  */
#define CPURTCIOBG_ADDR           0x18840008

/* CPURTCIOBG_ADDR.CPURTCIOBG_ADDR -  */
#define CPURTCIOBG_ADDR__CPURTCIOBG_ADDR__SHIFT       0
#define CPURTCIOBG_ADDR__CPURTCIOBG_ADDR__WIDTH       32
#define CPURTCIOBG_ADDR__CPURTCIOBG_ADDR__MASK        0xFFFFFFFF
#define CPURTCIOBG_ADDR__CPURTCIOBG_ADDR__INV_MASK    0x00000000
#define CPURTCIOBG_ADDR__CPURTCIOBG_ADDR__HW_DEFAULT  0x0

/*  */
#define CPURTCIOBG_DATA           0x1884000C

/* CPURTCIOBG_DATA.CPURTCIOBG_DATA -  */
#define CPURTCIOBG_DATA__CPURTCIOBG_DATA__SHIFT       0
#define CPURTCIOBG_DATA__CPURTCIOBG_DATA__WIDTH       32
#define CPURTCIOBG_DATA__CPURTCIOBG_DATA__MASK        0xFFFFFFFF
#define CPURTCIOBG_DATA__CPURTCIOBG_DATA__INV_MASK    0x00000000
#define CPURTCIOBG_DATA__CPURTCIOBG_DATA__HW_DEFAULT  0x0


#endif  // __CPURTCIO_H__
