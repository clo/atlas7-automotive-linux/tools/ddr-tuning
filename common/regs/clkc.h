/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __CLKC_H__
#define __CLKC_H__



/* CLKC */
/* ==================================================================== */

/* MEM VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See MEMPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_MEMPLL_AB_FREQ       0x18620000

/* CLKC_MEMPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1. 

In Spread Spectrum mode this field controls the SSADDR[7:0] outputs and hence the modulation frequency */
#define CLKC_MEMPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_MEMPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_MEMPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_MEMPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_MEMPLL_AB_FREQ__DIVF__HW_DEFAULT  0x3C

/* CLKC_MEMPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_MEMPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_MEMPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_MEMPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_MEMPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_MEMPLL_AB_FREQ__DIVR__HW_DEFAULT  0x0

/* MEMP Spread Spectrum Fractional-N */
#define CLKC_MEMPLL_AB_SSC        0x18620004

/* CLKC_MEMPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if MEMPLL_AB_CTRL.sse is asserted */
#define CLKC_MEMPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_MEMPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_MEMPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_MEMPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_MEMPLL_AB_SSC__SSMOD__HW_DEFAULT  0x0

/* CLKC_MEMPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if MEMPLL_AB_CTRL.sse is asserted */
#define CLKC_MEMPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_MEMPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_MEMPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_MEMPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_MEMPLL_AB_SSC__SSDIV__HW_DEFAULT  0x0

/* CLKC_MEMPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_MEMPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_MEMPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_MEMPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_MEMPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_MEMPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x0

/* MEM PLL Control */
#define CLKC_MEMPLL_AB_CTRL0      0x18620008

/* CLKC_MEMPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_MEMPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_MEMPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_MEMPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_MEMPLL_AB_CTRL0__RSB__HW_DEFAULT  0x0

/* CLKC_MEMPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_MEMPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_MEMPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_MEMPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_MEMPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x1

/* CLKC_MEMPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_MEMPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_MEMPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_MEMPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_MEMPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_MEMPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_MEMPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_MEMPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_MEMPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_MEMPLL_AB_CTRL0__SSE__HW_DEFAULT  0x0

/* CLKC_MEMPLL_AB_CTRL0.ssram - Spread Spectrum RAM */
/* For Spread Spectrum modulation, both SSE and SSRAM must be asserted. When SSRAM is asserted, the SSRAM address is driven by the PLL from SSADR[7:0], and the SSMOD[7:0] input to the PLL is driven by the SSRAM data output.
 When SSRAM is de-asserted, the SSMOD[7:0] input to the PLL is driven by MEMPLL_AB_SSC,ssmod. Also, when SSRAM is de-asserted the SW can load the modulation pattern into the spread spectrum RAM */
/* 0 : SS RAM disabled */
/* 1 : SS RAM enabled */
#define CLKC_MEMPLL_AB_CTRL0__SSRAM__SHIFT       16
#define CLKC_MEMPLL_AB_CTRL0__SSRAM__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL0__SSRAM__MASK        0x00010000
#define CLKC_MEMPLL_AB_CTRL0__SSRAM__INV_MASK    0xFFFEFFFF
#define CLKC_MEMPLL_AB_CTRL0__SSRAM__HW_DEFAULT  0x0

/* MEM PLL Additional Control */
#define CLKC_MEMPLL_AB_CTRL1      0x1862000C

/* CLKC_MEMPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* Used for MEM clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_MEMPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_MEMPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_MEMPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_MEMPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_MEMPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x3

/* CLKC_MEMPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_MEMPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_MEMPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_MEMPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_MEMPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_MEMPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x5

/* CLKC_MEMPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_MEMPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_MEMPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_MEMPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_MEMPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_MEMPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_MEMPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* Used for MEM clock. */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_MEMPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_MEMPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_MEMPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_MEMPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x1

/* CLKC_MEMPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_MEMPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_MEMPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_MEMPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_MEMPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_MEMPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_MEMPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_MEMPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_MEMPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_MEMPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_MEMPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* MEM Status */
#define CLKC_MEMPLL_AB_STATUS     0x18620010

/* CLKC_MEMPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_MEMPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_MEMPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_MEMPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_MEMPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_MEMPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* MEM Spread Spectrum RAM Address */
#define CLKC_MEMPLL_AB_SSRAM_ADDR 0x18620014

/* CLKC_MEMPLL_AB_SSRAM_ADDR.addr - Spread Spectrum RAM address */
/* When written, sets the address for the next read or write operation via MEMPLL_AB_SSRAM_DATA.
 When read, returns the current contents of the spread spectrum RAM address.

 Access only when MEMPLL_AB_CTRL0.ssram is de-asserted ('0') */
#define CLKC_MEMPLL_AB_SSRAM_ADDR__ADDR__SHIFT       0
#define CLKC_MEMPLL_AB_SSRAM_ADDR__ADDR__WIDTH       8
#define CLKC_MEMPLL_AB_SSRAM_ADDR__ADDR__MASK        0x000000FF
#define CLKC_MEMPLL_AB_SSRAM_ADDR__ADDR__INV_MASK    0xFFFFFF00
#define CLKC_MEMPLL_AB_SSRAM_ADDR__ADDR__HW_DEFAULT  0x0

/* MEM Spread Spectrum RAM Data */
#define CLKC_MEMPLL_AB_SSRAM_DATA 0x18620018

/* CLKC_MEMPLL_AB_SSRAM_DATA.data - Spread Spectrum RAM address */
/* When written, writes the value into the spread spectrum RAM at the address indicated by <a href='CLKC_MEMPLL_AB_SSRAM_ADDR.html' target='_top'>CLKC_MEMPLL_AB_SSRAM_ADDR</a>.
 When read, reads the data from the spread spectrum RAM at the address indicated by <a href='CLKC_MEMPLL_AB_SSRAM_ADDR.html' target='_top'>CLKC_MEMPLL_AB_SSRAM_ADDR</a>.

 Access only when MEMPLL_AB_CTRL0.ssram is de-asserted ('0') */
#define CLKC_MEMPLL_AB_SSRAM_DATA__DATA__SHIFT       0
#define CLKC_MEMPLL_AB_SSRAM_DATA__DATA__WIDTH       8
#define CLKC_MEMPLL_AB_SSRAM_DATA__DATA__MASK        0x000000FF
#define CLKC_MEMPLL_AB_SSRAM_DATA__DATA__INV_MASK    0xFFFFFF00
#define CLKC_MEMPLL_AB_SSRAM_DATA__DATA__HW_DEFAULT  undefined

/* CPU VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See CPUPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_CPUPLL_AB_FREQ       0x1862001C

/* CLKC_CPUPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1 */
#define CLKC_CPUPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_CPUPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_CPUPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_CPUPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_CPUPLL_AB_FREQ__DIVF__HW_DEFAULT  0x3C

/* CLKC_CPUPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_CPUPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_CPUPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_CPUPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_CPUPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_CPUPLL_AB_FREQ__DIVR__HW_DEFAULT  0x0

/* CPU Spread Spectrum Fractional-N */
#define CLKC_CPUPLL_AB_SSC        0x18620020

/* CLKC_CPUPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if CPUPLL_AB_CTRL.sse is asserted */
#define CLKC_CPUPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_CPUPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_CPUPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_CPUPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_CPUPLL_AB_SSC__SSMOD__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if CPUPLL_AB_CTRL.sse is asserted */
#define CLKC_CPUPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_CPUPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_CPUPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_CPUPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_CPUPLL_AB_SSC__SSDIV__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_CPUPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_CPUPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_CPUPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_CPUPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_CPUPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x0

/* MEM PLL Control */
#define CLKC_CPUPLL_AB_CTRL0      0x18620024

/* CLKC_CPUPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_CPUPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_CPUPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_CPUPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_CPUPLL_AB_CTRL0__RSB__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_CPUPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_CPUPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_CPUPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_CPUPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_CPUPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_CPUPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_CPUPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_CPUPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_CPUPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_CPUPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_CPUPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_CPUPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_CPUPLL_AB_CTRL0__SSE__HW_DEFAULT  0x0

/* MEM PLL Additional Control */
#define CLKC_CPUPLL_AB_CTRL1      0x18620028

/* CLKC_CPUPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* Used for CPU clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_CPUPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_CPUPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_CPUPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_CPUPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_CPUPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x2

/* CLKC_CPUPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_CPUPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_CPUPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_CPUPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_CPUPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_CPUPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x5

/* CLKC_CPUPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_CPUPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_CPUPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_CPUPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_CPUPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_CPUPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_CPUPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* Used for CPU clock. */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_CPUPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_CPUPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_CPUPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_CPUPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_CPUPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_CPUPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_CPUPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_CPUPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_CPUPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_CPUPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_CPUPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_CPUPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_CPUPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_CPUPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* MEM Status */
#define CLKC_CPUPLL_AB_STATUS     0x1862002C

/* CLKC_CPUPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_CPUPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_CPUPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_CPUPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_CPUPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_CPUPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* SYS0_BT VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See SYS0_BTPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_SYS0_BTPLL_AB_FREQ   0x18620030

/* CLKC_SYS0_BTPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1 */
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVF__HW_DEFAULT  0x27

/* CLKC_SYS0_BTPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_SYS0_BTPLL_AB_FREQ__DIVR__HW_DEFAULT  0x0

/* SYS0_BTPLL Spread Spectrum Fractional-N */
#define CLKC_SYS0_BTPLL_AB_SSC    0x18620034

/* CLKC_SYS0_BTPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS0_BTPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS0_BTPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_SYS0_BTPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_SYS0_BTPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_SYS0_BTPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_SYS0_BTPLL_AB_SSC__SSMOD__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS0_BTPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS0_BTPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_SYS0_BTPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_SYS0_BTPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_SYS0_BTPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_SYS0_BTPLL_AB_SSC__SSDIV__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_SYS0_BTPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_SYS0_BTPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_SYS0_BTPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_SYS0_BTPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_SYS0_BTPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x0

/* MEM PLL Control */
#define CLKC_SYS0_BTPLL_AB_CTRL0  0x18620038

/* CLKC_SYS0_BTPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_SYS0_BTPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_SYS0_BTPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_SYS0_BTPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_SYS0_BTPLL_AB_CTRL0__RSB__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_SYS0_BTPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_SYS0_BTPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_SYS0_BTPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_SYS0_BTPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_SYS0_BTPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_SYS0_BTPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_SYS0_BTPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_SYS0_BTPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_SYS0_BTPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_SYS0_BTPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_SYS0_BTPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_SYS0_BTPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_SYS0_BTPLL_AB_CTRL0__SSE__HW_DEFAULT  0x0

/* MEM PLL Additional Control */
#define CLKC_SYS0_BTPLL_AB_CTRL1  0x1862003C

/* CLKC_SYS0_BTPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* used for DTO input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x1

/* CLKC_SYS0_BTPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* used for PWM input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x1

/* CLKC_SYS0_BTPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_SYS0_BTPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_SYS0_BTPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* used for DTO input clock */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* used for PWM input clock. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_SYS0_BTPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_SYS0_BTPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* MEM Status */
#define CLKC_SYS0_BTPLL_AB_STATUS 0x18620040

/* CLKC_SYS0_BTPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_SYS0_BTPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_SYS0_BTPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_SYS0_BTPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_SYS0_BTPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_SYS0_BTPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* SYS1_USB VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See SYS1_USBPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_SYS1_USBPLL_AB_FREQ  0x18620044

/* CLKC_SYS1_USBPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1 */
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVF__HW_DEFAULT  0x2D

/* CLKC_SYS1_USBPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_SYS1_USBPLL_AB_FREQ__DIVR__HW_DEFAULT  0x0

/* SYS1_USBP Spread Spectrum Fractional-N */
#define CLKC_SYS1_USBPLL_AB_SSC   0x18620048

/* CLKC_SYS1_USBPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS1_USBPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS1_USBPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_SYS1_USBPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_SYS1_USBPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_SYS1_USBPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_SYS1_USBPLL_AB_SSC__SSMOD__HW_DEFAULT  0xF9

/* CLKC_SYS1_USBPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS1_USBPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS1_USBPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_SYS1_USBPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_SYS1_USBPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_SYS1_USBPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_SYS1_USBPLL_AB_SSC__SSDIV__HW_DEFAULT  0x2C5

/* CLKC_SYS1_USBPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_SYS1_USBPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_SYS1_USBPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_SYS1_USBPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_SYS1_USBPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_SYS1_USBPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x0

/* SYS1_USB PLL Control */
#define CLKC_SYS1_USBPLL_AB_CTRL0 0x1862004C

/* CLKC_SYS1_USBPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_SYS1_USBPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_SYS1_USBPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_SYS1_USBPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_SYS1_USBPLL_AB_CTRL0__RSB__HW_DEFAULT  0x1

/* CLKC_SYS1_USBPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_SYS1_USBPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_SYS1_USBPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_SYS1_USBPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_SYS1_USBPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x0

/* CLKC_SYS1_USBPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_SYS1_USBPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_SYS1_USBPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_SYS1_USBPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_SYS1_USBPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_SYS1_USBPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_SYS1_USBPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_SYS1_USBPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_SYS1_USBPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_SYS1_USBPLL_AB_CTRL0__SSE__HW_DEFAULT  0x1

/* SYS1_USB PLL Additional Control */
#define CLKC_SYS1_USBPLL_AB_CTRL1 0x18620050

/* CLKC_SYS1_USBPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* used for DTO input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x1

/* CLKC_SYS1_USBPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* used for PWM input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x1

/* CLKC_SYS1_USBPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_SYS1_USBPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_SYS1_USBPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* used for DTO input clock. */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x1

/* CLKC_SYS1_USBPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* used for PWM input clock. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_SYS1_USBPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_SYS1_USBPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* SYS1_USB Status */
#define CLKC_SYS1_USBPLL_AB_STATUS 0x18620054

/* CLKC_SYS1_USBPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_SYS1_USBPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_SYS1_USBPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_SYS1_USBPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_SYS1_USBPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_SYS1_USBPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* SYS2_ETH VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See SYS2_ETHPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_SYS2_ETHPLL_AB_FREQ  0x18620058

/* CLKC_SYS2_ETHPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1 */
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVF__HW_DEFAULT  0x7C

/* CLKC_SYS2_ETHPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_SYS2_ETHPLL_AB_FREQ__DIVR__HW_DEFAULT  0x1

/* SYS2_ETHP Spread Spectrum Fractional-N */
#define CLKC_SYS2_ETHPLL_AB_SSC   0x1862005C

/* CLKC_SYS2_ETHPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS2_ETHPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS2_ETHPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_SYS2_ETHPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_SYS2_ETHPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_SYS2_ETHPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_SYS2_ETHPLL_AB_SSC__SSMOD__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS2_ETHPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDIV__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_SYS2_ETHPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x0

/* SYS2_ETH PLL Control */
#define CLKC_SYS2_ETHPLL_AB_CTRL0 0x18620060

/* CLKC_SYS2_ETHPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RSB__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_SYS2_ETHPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_SYS2_ETHPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_SYS2_ETHPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_SYS2_ETHPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_SYS2_ETHPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_SYS2_ETHPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_SYS2_ETHPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_SYS2_ETHPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_SYS2_ETHPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_SYS2_ETHPLL_AB_CTRL0__SSE__HW_DEFAULT  0x0

/* SYS2_ETH PLL Additional Control */
#define CLKC_SYS2_ETHPLL_AB_CTRL1 0x18620064

/* CLKC_SYS2_ETHPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x5

/* CLKC_SYS2_ETHPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x5

/* CLKC_SYS2_ETHPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_SYS2_ETHPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_SYS2_ETHPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_SYS2_ETHPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_SYS2_ETHPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* SYS2_ETH Status */
#define CLKC_SYS2_ETHPLL_AB_STATUS 0x18620068

/* CLKC_SYS2_ETHPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_SYS2_ETHPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_SYS2_ETHPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_SYS2_ETHPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_SYS2_ETHPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_SYS2_ETHPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* SYS3_SSC VCO Frequency */
/* AnalogBits PLL VCO frequency is given by Fvco = Fref * 2 * NF / NR in integer-N mode, or by Fvco = Fref * SSN / NR in Spread Spectrum (fractional-N) mode. See SYS3_SSCPLL_AB_SSC for definition of SSN.
 Output dividers are defined in TBD
 The PLL VCO must be within the range 1.8 GHz - 3.6 GHz. */
#define CLKC_SYS3_SSCPLL_AB_FREQ  0x1862006C

/* CLKC_SYS3_SSCPLL_AB_FREQ.divf - Feedback clock divider */
/* Actual divider value NF = divf + 1. 

In Spread Spectrum mode this field controls the SSADDR[7:0] outputs and hence the modulation frequency */
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVF__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVF__WIDTH       9
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVF__MASK        0x000001FF
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVF__INV_MASK    0xFFFFFE00
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVF__HW_DEFAULT  0x2D

/* CLKC_SYS3_SSCPLL_AB_FREQ.divr - Reference clock divider */
/* Actual divider value NR = divr + 1. 

 The divided reference clock must be in the range 5 MHz - 30 MHz. */
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVR__SHIFT       16
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVR__WIDTH       3
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVR__MASK        0x00070000
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVR__INV_MASK    0xFFF8FFFF
#define CLKC_SYS3_SSCPLL_AB_FREQ__DIVR__HW_DEFAULT  0x0

/* SYS3_SSCP Spread Spectrum Fractional-N */
#define CLKC_SYS3_SSCPLL_AB_SSC   0x18620070

/* CLKC_SYS3_SSCPLL_AB_SSC.ssmod - LSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS3_SSCPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS3_SSCPLL_AB_SSC__SSMOD__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_SSC__SSMOD__WIDTH       8
#define CLKC_SYS3_SSCPLL_AB_SSC__SSMOD__MASK        0x000000FF
#define CLKC_SYS3_SSCPLL_AB_SSC__SSMOD__INV_MASK    0xFFFFFF00
#define CLKC_SYS3_SSCPLL_AB_SSC__SSMOD__HW_DEFAULT  0x80

/* CLKC_SYS3_SSCPLL_AB_SSC.ssdiv - MSB of the fractional divider */
/*  Unless modified by 'ssdepth', the fractional divider value is 
 SSN = 2^24/(ssdiv[11:0]||ssmod[7:0])
 See also 'ssdepth' description.
 This is relevant only if SYS3_SSCPLL_AB_CTRL.sse is asserted */
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDIV__SHIFT       8
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDIV__WIDTH       12
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDIV__MASK        0x000FFF00
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDIV__INV_MASK    0xFFF000FF
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDIV__HW_DEFAULT  0x2C6

/* CLKC_SYS3_SSCPLL_AB_SSC.ssdepth - Modulation depth control */
/* Shifts ssmod[7:0] over the LSBs of ssdiv[11:0]. The LSBs of ssmod[7:0] are replaced by zeros */
/* 0 : No shift */
/* 1 : Shift left 1 bit */
/* 2 : Shift left 2 bits */
/* 3 : Shift left 3 bits */
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDEPTH__SHIFT       20
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDEPTH__WIDTH       2
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDEPTH__MASK        0x00300000
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDEPTH__INV_MASK    0xFFCFFFFF
#define CLKC_SYS3_SSCPLL_AB_SSC__SSDEPTH__HW_DEFAULT  0x1

/* SYS3_SSC PLL Control */
#define CLKC_SYS3_SSCPLL_AB_CTRL0 0x18620074

/* CLKC_SYS3_SSCPLL_AB_CTRL0.rsb - Reset */
/* An active-low RESET-BAR control is provided to power down the PLL and reset it to a known state. RSB resets all of the divider blocks in the PLL macro, as well as in the separate 20-divider block. RSB should be asserted, and BYPASS should be de-asserted, for power-down IDDQ testing. */
/* 0 : Reset */
/* 1 : Normal operation */
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RSB__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RSB__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RSB__MASK        0x00000001
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RSB__INV_MASK    0xFFFFFFFE
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RSB__HW_DEFAULT  0x0

/* CLKC_SYS3_SSCPLL_AB_CTRL0.bypass - Bypass */
/* BYPASS both powers-down the PLL and bypasses it such that all PLL ouptus track the reference clock input. BYPASS has precedence over RSB.  */
/* 0 : Normal operation */
/* 1 : Bypass */
#define CLKC_SYS3_SSCPLL_AB_CTRL0__BYPASS__SHIFT       4
#define CLKC_SYS3_SSCPLL_AB_CTRL0__BYPASS__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL0__BYPASS__MASK        0x00000010
#define CLKC_SYS3_SSCPLL_AB_CTRL0__BYPASS__INV_MASK    0xFFFFFFEF
#define CLKC_SYS3_SSCPLL_AB_CTRL0__BYPASS__HW_DEFAULT  0x0

/* CLKC_SYS3_SSCPLL_AB_CTRL0.range - PLL loop filter range */
/* Sets the PLL loop filter to work with the post-reference divider frequency. */
/* 0 : 5 - 13 MHz */
/* 1 : 13 - 30 MHz */
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RANGE__SHIFT       8
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RANGE__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RANGE__MASK        0x00000100
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RANGE__INV_MASK    0xFFFFFEFF
#define CLKC_SYS3_SSCPLL_AB_CTRL0__RANGE__HW_DEFAULT  0x1

/* CLKC_SYS3_SSCPLL_AB_CTRL0.sse - Fractional-N mode */
/* Enables fractional-N mode operation. */
/* 0 : Fractional-N disabled */
/* 1 : Fractional-N enabled */
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSE__SHIFT       12
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSE__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSE__MASK        0x00001000
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSE__INV_MASK    0xFFFFEFFF
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSE__HW_DEFAULT  0x1

/* CLKC_SYS3_SSCPLL_AB_CTRL0.ssram - Spread Spectrum RAM */
/* For Spread Spectrum modulation, both SSE and SSRAM must be asserted. When SSRAM is asserted, the SSRAM address is driven by the PLL from SSADR[7:0], and the SSMOD[7:0] input to the PLL is driven by the SSRAM data output.
 When SSRAM is de-asserted, the SSMOD[7:0] input to the PLL is driven by SYS3_SSCPLL_AB_SSC,ssmod. Also, when SSRAM is de-asserted the SW can load the modulation pattern into the spread spectrum RAM */
/* 0 : SS RAM disabled */
/* 1 : SS RAM enabled */
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSRAM__SHIFT       16
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSRAM__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSRAM__MASK        0x00010000
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSRAM__INV_MASK    0xFFFEFFFF
#define CLKC_SYS3_SSCPLL_AB_CTRL0__SSRAM__HW_DEFAULT  0x0

/* SYS3_SSC PLL Additional Control */
#define CLKC_SYS3_SSCPLL_AB_CTRL1 0x18620078

/* CLKC_SYS3_SSCPLL_AB_CTRL1.DIVQ1 - Divider for PLLOUT1 */
/* used for DTO input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ1__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ1__WIDTH       3
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ1__MASK        0x00000007
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ1__INV_MASK    0xFFFFFFF8
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ1__HW_DEFAULT  0x1

/* CLKC_SYS3_SSCPLL_AB_CTRL1.DIVQ2 - Divider for PLLOUT2 */
/* used for PWM input clock. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ2__SHIFT       4
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ2__WIDTH       3
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ2__MASK        0x00000070
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ2__INV_MASK    0xFFFFFF8F
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ2__HW_DEFAULT  0x1

/* CLKC_SYS3_SSCPLL_AB_CTRL1.DIVQ3 - Divider for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 1 : Divide by 2 */
/* 2 : Divide by 4 */
/* 3 : Divide by 8 */
/* 4 : Divide by 16 */
/* 5 : Divide by 32 */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ3__SHIFT       8
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ3__WIDTH       3
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ3__MASK        0x00000700
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ3__INV_MASK    0xFFFFF8FF
#define CLKC_SYS3_SSCPLL_AB_CTRL1__DIVQ3__HW_DEFAULT  0x5

/* CLKC_SYS3_SSCPLL_AB_CTRL1.ENABLE1 - Enable for PLLOUT1 */
/* used for DTO input clock. */
/* 0 : PLLOUT1 disabled */
/* 1 : PLLOUT1 enabled */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE1__SHIFT       12
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE1__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE1__MASK        0x00001000
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE1__INV_MASK    0xFFFFEFFF
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE1__HW_DEFAULT  0x0

/* CLKC_SYS3_SSCPLL_AB_CTRL1.ENABLE2 - Enable for PLLOUT2 */
/* used for PWM input clock. */
/* 0 : PLLOUT2 disabled */
/* 1 : PLLOUT2 enabled */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE2__SHIFT       13
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE2__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE2__MASK        0x00002000
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE2__INV_MASK    0xFFFFDFFF
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE2__HW_DEFAULT  0x0

/* CLKC_SYS3_SSCPLL_AB_CTRL1.ENABLE3 - Enable for PLLOUT3 */
/* The inputs controlled by this register are not used and should be left at their default values. */
/* 0 : PLLOUT3 disabled */
/* 1 : PLLOUT3 enabled */
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE3__SHIFT       14
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE3__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE3__MASK        0x00004000
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE3__INV_MASK    0xFFFFBFFF
#define CLKC_SYS3_SSCPLL_AB_CTRL1__ENABLE3__HW_DEFAULT  0x0

/* SYS3_SSC Status */
#define CLKC_SYS3_SSCPLL_AB_STATUS 0x1862007C

/* CLKC_SYS3_SSCPLL_AB_STATUS.lock - PLL lock status */
/* 0 : PLL not locked */
/* 1 : PLL locked */
#define CLKC_SYS3_SSCPLL_AB_STATUS__LOCK__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_STATUS__LOCK__WIDTH       1
#define CLKC_SYS3_SSCPLL_AB_STATUS__LOCK__MASK        0x00000001
#define CLKC_SYS3_SSCPLL_AB_STATUS__LOCK__INV_MASK    0xFFFFFFFE
#define CLKC_SYS3_SSCPLL_AB_STATUS__LOCK__HW_DEFAULT  0x0

/* SYS3_SSC Spread Spectrum RAM Address */
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR 0x18620080

/* CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR.addr - Spread Spectrum RAM address */
/* When written, sets the address for the next read or write operation via SYS3_SSCPLL_AB_SSRAM_DATA.
 When read, returns the current contents of the spread spectrum RAM address.

 Access only when SYS3_SSCPLL_AB_CTRL0.ssram is de-asserted ('0') */
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR__ADDR__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR__ADDR__WIDTH       8
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR__ADDR__MASK        0x000000FF
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR__ADDR__INV_MASK    0xFFFFFF00
#define CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR__ADDR__HW_DEFAULT  0x0

/* SYS3_SSC Spread Spectrum RAM Data */
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA 0x18620084

/* CLKC_SYS3_SSCPLL_AB_SSRAM_DATA.data - Spread Spectrum RAM address */
/* When written, writes the value into the spread spectrum RAM at the address indicated by <a href='CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR.html' target='_top'>CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR</a>.
 When read, reads the data from the spread spectrum RAM at the address indicated by <a href='CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR.html' target='_top'>CLKC_SYS3_SSCPLL_AB_SSRAM_ADDR</a>.

 Access only when SYS3_SSCPLL_AB_CTRL0.ssram is de-asserted ('0') */
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA__DATA__SHIFT       0
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA__DATA__WIDTH       8
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA__DATA__MASK        0x000000FF
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA__DATA__INV_MASK    0xFFFFFF00
#define CLKC_SYS3_SSCPLL_AB_SSRAM_DATA__DATA__HW_DEFAULT  undefined

/* AUDIO DTO Increment */
#define CLKC_AUDIO_DTO_INC        0x18620088

/* CLKC_AUDIO_DTO_INC.incr - Increment Value */
/* The increment value determines the output frequency as a function of the input frequency: 
 
Fout = incr * Fin / 2^29 
 
The default value will genrate from 1200Mhz, to get 49.152Mhz */
#define CLKC_AUDIO_DTO_INC__INCR__SHIFT       0
#define CLKC_AUDIO_DTO_INC__INCR__WIDTH       28
#define CLKC_AUDIO_DTO_INC__INCR__MASK        0x0FFFFFFF
#define CLKC_AUDIO_DTO_INC__INCR__INV_MASK    0xF0000000
#define CLKC_AUDIO_DTO_INC__INCR__HW_DEFAULT  0x14F8B59

/* DISP0 DTO Increment */
#define CLKC_DISP0_DTO_INC        0x1862008C

/* CLKC_DISP0_DTO_INC.incr - Increment Value */
/* The increment value determines the output frequency as a function of the input frequency: 
 
Fout = incr * Fin / 2^29 
 
The default value will genrate Fout = Fin / 16 */
#define CLKC_DISP0_DTO_INC__INCR__SHIFT       0
#define CLKC_DISP0_DTO_INC__INCR__WIDTH       28
#define CLKC_DISP0_DTO_INC__INCR__MASK        0x0FFFFFFF
#define CLKC_DISP0_DTO_INC__INCR__INV_MASK    0xF0000000
#define CLKC_DISP0_DTO_INC__INCR__HW_DEFAULT  0x7FFFFFE

/* DISP1 DTO Increment */
#define CLKC_DISP1_DTO_INC        0x18620090

/* CLKC_DISP1_DTO_INC.incr - Increment Value */
/* The increment value determines the output frequency as a function of the input frequency: 
 
Fout = incr * Fin / 2^29 
 
The default value will genrate Fout = Fin / 16 */
#define CLKC_DISP1_DTO_INC__INCR__SHIFT       0
#define CLKC_DISP1_DTO_INC__INCR__WIDTH       28
#define CLKC_DISP1_DTO_INC__INCR__MASK        0x0FFFFFFF
#define CLKC_DISP1_DTO_INC__INCR__INV_MASK    0xF0000000
#define CLKC_DISP1_DTO_INC__INCR__HW_DEFAULT  0x7FFFFFE

/* AUDIO DTO Source Clock Selector */
/* Write only when <a href='CLKC_AUDIO_DTO_ENA.html' target='_top'>CLKC_AUDIO_DTO_ENA</a>.enable = 0 */
#define CLKC_AUDIO_DTO_SRC        0x18620094

/* CLKC_AUDIO_DTO_SRC.src - Select input clock source for DTO */
/* 0 : SYS0_BTPLL/CLK1 */
/* 1 : SYS1_USBPLL/CLK1 */
/* 2 : SYS3_SSCPLL/CLK1 */
#define CLKC_AUDIO_DTO_SRC__SRC__SHIFT       0
#define CLKC_AUDIO_DTO_SRC__SRC__WIDTH       2
#define CLKC_AUDIO_DTO_SRC__SRC__MASK        0x00000003
#define CLKC_AUDIO_DTO_SRC__SRC__INV_MASK    0xFFFFFFFC
#define CLKC_AUDIO_DTO_SRC__SRC__HW_DEFAULT  0x1

/* AUDIO DTO Enable */
#define CLKC_AUDIO_DTO_ENA        0x18620098

/* CLKC_AUDIO_DTO_ENA.enable - DTO Enable/disable */
/* 0 : DTO Disabled */
/* 1 : DTO Enabled */
#define CLKC_AUDIO_DTO_ENA__ENABLE__SHIFT       0
#define CLKC_AUDIO_DTO_ENA__ENABLE__WIDTH       1
#define CLKC_AUDIO_DTO_ENA__ENABLE__MASK        0x00000001
#define CLKC_AUDIO_DTO_ENA__ENABLE__INV_MASK    0xFFFFFFFE
#define CLKC_AUDIO_DTO_ENA__ENABLE__HW_DEFAULT  0x0

/* AUDIO Double Resolution */
/* Write only when <a href='CLKC_AUDIO_DTO_ENA.html' target='_top'>CLKC_AUDIO_DTO_ENA</a>.enable = 0 */
#define CLKC_AUDIO_DTO_DROFF      0x1862009C

/* CLKC_AUDIO_DTO_DROFF.droff - Double resolution control */
/* Normal resolution is Fin / 2^28; Double resolution is Fin / 2^29 */
/* 0 : Double resolution on */
/* 1 : Double resolution off */
#define CLKC_AUDIO_DTO_DROFF__DROFF__SHIFT       0
#define CLKC_AUDIO_DTO_DROFF__DROFF__WIDTH       1
#define CLKC_AUDIO_DTO_DROFF__DROFF__MASK        0x00000001
#define CLKC_AUDIO_DTO_DROFF__DROFF__INV_MASK    0xFFFFFFFE
#define CLKC_AUDIO_DTO_DROFF__DROFF__HW_DEFAULT  0x0

/* DISP0 DTO Source Clock Selector */
/* Write only when <a href='CLKC_DISP0_DTO_ENA.html' target='_top'>CLKC_DISP0_DTO_ENA</a>.enable = 0 */
#define CLKC_DISP0_DTO_SRC        0x186200A0

/* CLKC_DISP0_DTO_SRC.src - Select input clock source for DTO */
/* 0 : SYS0_BTPLL/CLK1 */
/* 1 : SYS1_USBPLL/CLK1 */
/* 2 : SYS3_SSCPLL/CLK1 */
#define CLKC_DISP0_DTO_SRC__SRC__SHIFT       0
#define CLKC_DISP0_DTO_SRC__SRC__WIDTH       2
#define CLKC_DISP0_DTO_SRC__SRC__MASK        0x00000003
#define CLKC_DISP0_DTO_SRC__SRC__INV_MASK    0xFFFFFFFC
#define CLKC_DISP0_DTO_SRC__SRC__HW_DEFAULT  0x1

/* DISP0 DTO Enable */
#define CLKC_DISP0_DTO_ENA        0x186200A4

/* CLKC_DISP0_DTO_ENA.enable - DTO Enable/disable */
/* 0 : DTO Disabled */
/* 1 : DTO Enabled */
#define CLKC_DISP0_DTO_ENA__ENABLE__SHIFT       0
#define CLKC_DISP0_DTO_ENA__ENABLE__WIDTH       1
#define CLKC_DISP0_DTO_ENA__ENABLE__MASK        0x00000001
#define CLKC_DISP0_DTO_ENA__ENABLE__INV_MASK    0xFFFFFFFE
#define CLKC_DISP0_DTO_ENA__ENABLE__HW_DEFAULT  0x0

/* DISP0 Double Resolution */
/* Write only when <a href='CLKC_DISP0_DTO_ENA.html' target='_top'>CLKC_DISP0_DTO_ENA</a>.enable = 0 */
#define CLKC_DISP0_DTO_DROFF      0x186200A8

/* CLKC_DISP0_DTO_DROFF.droff - Double resolution control */
/* Normal resolution is Fin / 2^28; Double resolution is Fin / 2^29 */
/* 0 : Double resolution on */
/* 1 : Double resolution off */
#define CLKC_DISP0_DTO_DROFF__DROFF__SHIFT       0
#define CLKC_DISP0_DTO_DROFF__DROFF__WIDTH       1
#define CLKC_DISP0_DTO_DROFF__DROFF__MASK        0x00000001
#define CLKC_DISP0_DTO_DROFF__DROFF__INV_MASK    0xFFFFFFFE
#define CLKC_DISP0_DTO_DROFF__DROFF__HW_DEFAULT  0x0

/* DISP1 DTO Source Clock Selector */
/* Write only when <a href='CLKC_DISP1_DTO_ENA.html' target='_top'>CLKC_DISP1_DTO_ENA</a>.enable = 0 */
#define CLKC_DISP1_DTO_SRC        0x186200AC

/* CLKC_DISP1_DTO_SRC.src - Select input clock source for DTO */
/* 0 : SYS0_BTPLL/CLK1 */
/* 1 : SYS1_USBPLL/CLK1 */
/* 2 : SYS3_SSCPLL/CLK1 */
#define CLKC_DISP1_DTO_SRC__SRC__SHIFT       0
#define CLKC_DISP1_DTO_SRC__SRC__WIDTH       2
#define CLKC_DISP1_DTO_SRC__SRC__MASK        0x00000003
#define CLKC_DISP1_DTO_SRC__SRC__INV_MASK    0xFFFFFFFC
#define CLKC_DISP1_DTO_SRC__SRC__HW_DEFAULT  0x1

/* DISP1 DTO Enable */
#define CLKC_DISP1_DTO_ENA        0x186200B0

/* CLKC_DISP1_DTO_ENA.enable - DTO Enable/disable */
/* 0 : DTO Disabled */
/* 1 : DTO Enabled */
#define CLKC_DISP1_DTO_ENA__ENABLE__SHIFT       0
#define CLKC_DISP1_DTO_ENA__ENABLE__WIDTH       1
#define CLKC_DISP1_DTO_ENA__ENABLE__MASK        0x00000001
#define CLKC_DISP1_DTO_ENA__ENABLE__INV_MASK    0xFFFFFFFE
#define CLKC_DISP1_DTO_ENA__ENABLE__HW_DEFAULT  0x0

/* DISP1 Double Resolution */
/* Write only when <a href='CLKC_DISP1_DTO_ENA.html' target='_top'>CLKC_DISP1_DTO_ENA</a>.enable = 0 */
#define CLKC_DISP1_DTO_DROFF      0x186200B4

/* CLKC_DISP1_DTO_DROFF.droff - Double resolution control */
/* Normal resolution is Fin / 2^28; Double resolution is Fin / 2^29 */
/* 0 : Double resolution on */
/* 1 : Double resolution off */
#define CLKC_DISP1_DTO_DROFF__DROFF__SHIFT       0
#define CLKC_DISP1_DTO_DROFF__DROFF__WIDTH       1
#define CLKC_DISP1_DTO_DROFF__DROFF__MASK        0x00000001
#define CLKC_DISP1_DTO_DROFF__DROFF__INV_MASK    0xFFFFFFFE
#define CLKC_DISP1_DTO_DROFF__DROFF__HW_DEFAULT  0x0

/* I2S_CLK selector */
/* Write to this register only if I2S_CLK_SEL_STATUS.status is '1'. */
#define CLKC_I2S_CLK_SEL          0x186200B8

/* CLKC_I2S_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : DTO output divider */
/* 3 : pwm_i2s01_clk */
#define CLKC_I2S_CLK_SEL__SEL__SHIFT       0
#define CLKC_I2S_CLK_SEL__SEL__WIDTH       2
#define CLKC_I2S_CLK_SEL__SEL__MASK        0x00000003
#define CLKC_I2S_CLK_SEL__SEL__INV_MASK    0xFFFFFFFC
#define CLKC_I2S_CLK_SEL__SEL__HW_DEFAULT  0x0

/* I2S_CLK Selector Status */
#define CLKC_I2S_CLK_SEL_STATUS   0x186200BC

/* CLKC_I2S_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_I2S_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_I2S_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_I2S_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_I2S_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_I2S_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* USBPHY_CLK Dividers Configuration */
/* Configure the dividers for USBPHY_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_USBPHY_CLK_CFG       0x186200C0

/* CLKC_USBPHY_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 1 of SYS0_BTPLL */
#define CLKC_USBPHY_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_USBPHY_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_USBPHY_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_USBPHY_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_USBPHY_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_USBPHY_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 1 of SYS1_USBPLL */
#define CLKC_USBPHY_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_USBPHY_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_USBPHY_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_USBPHY_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_USBPHY_CLK_CFG__SYS1_QA__HW_DEFAULT  0x31

/* CLKC_USBPHY_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 1 of SYS2_ETHPLL */
#define CLKC_USBPHY_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_USBPHY_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_USBPHY_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_USBPHY_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_USBPHY_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_USBPHY_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 1 of SYS3_SSCPLL */
#define CLKC_USBPHY_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_USBPHY_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_USBPHY_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_USBPHY_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_USBPHY_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* USBPHY_CLK Dividers Enable */
#define CLKC_USBPHY_CLK_DIVENA    0x186200C4

/* CLKC_USBPHY_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 1 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_USBPHY_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_USBPHY_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_USBPHY_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_USBPHY_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_USBPHY_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_USBPHY_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA1 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_USBPHY_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_USBPHY_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_USBPHY_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_USBPHY_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_USBPHY_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_USBPHY_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA1 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_USBPHY_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_USBPHY_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_USBPHY_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_USBPHY_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_USBPHY_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_USBPHY_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA1 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_USBPHY_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_USBPHY_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_USBPHY_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_USBPHY_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_USBPHY_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* USBPHY_CLK Selector */
/* Write to this register only if USBPHY_CLK_SEL_STATUS.status is '1'. */
#define CLKC_USBPHY_CLK_SEL       0x186200C8

/* CLKC_USBPHY_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_USBPHY_CLK_SEL__SEL__SHIFT       0
#define CLKC_USBPHY_CLK_SEL__SEL__WIDTH       3
#define CLKC_USBPHY_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_USBPHY_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_USBPHY_CLK_SEL__SEL__HW_DEFAULT  0x0

/* USBPHY_CLK Selector Status */
#define CLKC_USBPHY_CLK_SEL_STATUS 0x186200CC

/* CLKC_USBPHY_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_USBPHY_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_USBPHY_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_USBPHY_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_USBPHY_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_USBPHY_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* BTSS_CLK Dividers Configuration */
/* Configure the dividers for BTSS_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_BTSS_CLK_CFG         0x186200D0

/* CLKC_BTSS_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 2 of SYS0_BTPLL */
#define CLKC_BTSS_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_BTSS_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_BTSS_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_BTSS_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_BTSS_CLK_CFG__SYS0_QA__HW_DEFAULT  0xC

/* CLKC_BTSS_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 2 of SYS1_USBPLL */
#define CLKC_BTSS_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_BTSS_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_BTSS_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_BTSS_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_BTSS_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_BTSS_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 2 of SYS2_ETHPLL */
#define CLKC_BTSS_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_BTSS_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_BTSS_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_BTSS_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_BTSS_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_BTSS_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 2 of SYS3_SSCPLL */
#define CLKC_BTSS_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_BTSS_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_BTSS_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_BTSS_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_BTSS_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* BTSS_CLK Dividers Enable */
#define CLKC_BTSS_CLK_DIVENA      0x186200D4

/* CLKC_BTSS_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 2 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_BTSS_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_BTSS_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_BTSS_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_BTSS_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_BTSS_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_BTSS_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA2 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_BTSS_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_BTSS_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_BTSS_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_BTSS_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_BTSS_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_BTSS_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA2 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_BTSS_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_BTSS_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_BTSS_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_BTSS_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_BTSS_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_BTSS_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA2 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_BTSS_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_BTSS_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_BTSS_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_BTSS_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_BTSS_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* BTSS_CLK Selector */
/* Write to this register only if BTSS_CLK_SEL_STATUS.status is '1'. */
#define CLKC_BTSS_CLK_SEL         0x186200D8

/* CLKC_BTSS_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_BTSS_CLK_SEL__SEL__SHIFT       0
#define CLKC_BTSS_CLK_SEL__SEL__WIDTH       3
#define CLKC_BTSS_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_BTSS_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_BTSS_CLK_SEL__SEL__HW_DEFAULT  0x0

/* BTSS_CLK Selector Status */
#define CLKC_BTSS_CLK_SEL_STATUS  0x186200DC

/* CLKC_BTSS_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_BTSS_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_BTSS_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_BTSS_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_BTSS_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_BTSS_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* RGMII_CLK Dividers Configuration */
/* Configure the dividers for RGMII_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_RGMII_CLK_CFG        0x186200E0

/* CLKC_RGMII_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 3 of SYS0_BTPLL */
#define CLKC_RGMII_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_RGMII_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_RGMII_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_RGMII_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_RGMII_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_RGMII_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 3 of SYS1_USBPLL */
#define CLKC_RGMII_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_RGMII_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_RGMII_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_RGMII_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_RGMII_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_RGMII_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 3 of SYS2_ETHPLL */
#define CLKC_RGMII_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_RGMII_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_RGMII_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_RGMII_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_RGMII_CLK_CFG__SYS2_QA__HW_DEFAULT  0xC

/* CLKC_RGMII_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 3 of SYS3_SSCPLL */
#define CLKC_RGMII_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_RGMII_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_RGMII_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_RGMII_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_RGMII_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* RGMII_CLK Dividers Enable */
#define CLKC_RGMII_CLK_DIVENA     0x186200E4

/* CLKC_RGMII_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 3 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_RGMII_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_RGMII_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_RGMII_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_RGMII_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_RGMII_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_RGMII_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA3 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_RGMII_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_RGMII_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_RGMII_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_RGMII_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_RGMII_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_RGMII_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA3 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_RGMII_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_RGMII_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_RGMII_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_RGMII_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_RGMII_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_RGMII_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA3 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_RGMII_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_RGMII_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_RGMII_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_RGMII_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_RGMII_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* RGMII_CLK Selector */
/* Write to this register only if RGMII_CLK_SEL_STATUS.status is '1'. */
#define CLKC_RGMII_CLK_SEL        0x186200E8

/* CLKC_RGMII_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_RGMII_CLK_SEL__SEL__SHIFT       0
#define CLKC_RGMII_CLK_SEL__SEL__WIDTH       3
#define CLKC_RGMII_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_RGMII_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_RGMII_CLK_SEL__SEL__HW_DEFAULT  0x0

/* RGMII_CLK Selector Status */
#define CLKC_RGMII_CLK_SEL_STATUS 0x186200EC

/* CLKC_RGMII_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_RGMII_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_RGMII_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_RGMII_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_RGMII_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_RGMII_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* CPU_CLK Dividers Configuration */
/* Configure the dividers for CPU_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_CPU_CLK_CFG          0x186200F0

/* CLKC_CPU_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 4 of SYS0_BTPLL */
#define CLKC_CPU_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_CPU_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_CPU_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_CPU_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_CPU_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_CPU_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 4 of SYS1_USBPLL */
#define CLKC_CPU_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_CPU_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_CPU_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_CPU_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_CPU_CLK_CFG__SYS1_QA__HW_DEFAULT  0x1

/* CPU_CLK Dividers Enable */
#define CLKC_CPU_CLK_DIVENA       0x186200F4

/* CLKC_CPU_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 4 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CPU_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_CPU_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_CPU_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_CPU_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_CPU_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_CPU_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA4 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CPU_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_CPU_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_CPU_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_CPU_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_CPU_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x1

/* CPU_CLK Selector */
/* Write to this register only if CPU_CLK_SEL_STATUS.status is '1'. */
#define CLKC_CPU_CLK_SEL          0x186200F8

/* CLKC_CPU_CLK_SEL.sel - Glitch-free selctor control */
/* 4 : CPUPLL output */
/* 3 : SYS1_USBPLL divider */
/* 2 : SYS0_BTPLL divider */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_CPU_CLK_SEL__SEL__SHIFT       0
#define CLKC_CPU_CLK_SEL__SEL__WIDTH       3
#define CLKC_CPU_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_CPU_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_CPU_CLK_SEL__SEL__HW_DEFAULT  0x0

/* CPU_CLK Selector Status */
#define CLKC_CPU_CLK_SEL_STATUS   0x186200FC

/* CLKC_CPU_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_CPU_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_CPU_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_CPU_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_CPU_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_CPU_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SDPHY01_CLK Dividers Configuration */
/* Configure the dividers for SDPHY01_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_SDPHY01_CLK_CFG      0x18620100

/* CLKC_SDPHY01_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 5 of SYS0_BTPLL */
#define CLKC_SDPHY01_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_SDPHY01_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_SDPHY01_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_SDPHY01_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_SDPHY01_CLK_CFG__SYS0_QA__HW_DEFAULT  0x9

/* CLKC_SDPHY01_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 5 of SYS1_USBPLL */
#define CLKC_SDPHY01_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_SDPHY01_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_SDPHY01_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_SDPHY01_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_SDPHY01_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY01_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 5 of SYS2_ETHPLL */
#define CLKC_SDPHY01_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_SDPHY01_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_SDPHY01_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_SDPHY01_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_SDPHY01_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY01_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 5 of SYS3_SSCPLL */
#define CLKC_SDPHY01_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_SDPHY01_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_SDPHY01_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_SDPHY01_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_SDPHY01_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* SDPHY01_CLK Dividers Enable */
#define CLKC_SDPHY01_CLK_DIVENA   0x18620104

/* CLKC_SDPHY01_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 5 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY01_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_SDPHY01_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_SDPHY01_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_SDPHY01_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY01_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY01_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA5 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY01_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_SDPHY01_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_SDPHY01_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_SDPHY01_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_SDPHY01_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY01_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA5 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY01_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_SDPHY01_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_SDPHY01_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_SDPHY01_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_SDPHY01_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY01_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA5 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY01_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_SDPHY01_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_SDPHY01_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_SDPHY01_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_SDPHY01_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* SDPHY01_CLK Selector */
/* Write to this register only if SDPHY01_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SDPHY01_CLK_SEL      0x18620108

/* CLKC_SDPHY01_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_SDPHY01_CLK_SEL__SEL__SHIFT       0
#define CLKC_SDPHY01_CLK_SEL__SEL__WIDTH       3
#define CLKC_SDPHY01_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SDPHY01_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SDPHY01_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SDPHY01_CLK Selector Status */
#define CLKC_SDPHY01_CLK_SEL_STATUS 0x1862010C

/* CLKC_SDPHY01_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SDPHY01_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SDPHY01_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SDPHY01_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SDPHY01_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY01_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SDPHY23_CLK Dividers Configuration */
/* Configure the dividers for SDPHY23_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_SDPHY23_CLK_CFG      0x18620110

/* CLKC_SDPHY23_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 6 of SYS0_BTPLL */
#define CLKC_SDPHY23_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_SDPHY23_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_SDPHY23_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_SDPHY23_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_SDPHY23_CLK_CFG__SYS0_QA__HW_DEFAULT  0x9

/* CLKC_SDPHY23_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 6 of SYS1_USBPLL */
#define CLKC_SDPHY23_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_SDPHY23_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_SDPHY23_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_SDPHY23_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_SDPHY23_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY23_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 6 of SYS2_ETHPLL */
#define CLKC_SDPHY23_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_SDPHY23_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_SDPHY23_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_SDPHY23_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_SDPHY23_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY23_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 6 of SYS3_SSCPLL */
#define CLKC_SDPHY23_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_SDPHY23_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_SDPHY23_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_SDPHY23_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_SDPHY23_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* SDPHY23_CLK Dividers Enable */
#define CLKC_SDPHY23_CLK_DIVENA   0x18620114

/* CLKC_SDPHY23_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 6 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY23_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_SDPHY23_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_SDPHY23_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_SDPHY23_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY23_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY23_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA6 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY23_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_SDPHY23_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_SDPHY23_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_SDPHY23_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_SDPHY23_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY23_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA6 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY23_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_SDPHY23_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_SDPHY23_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_SDPHY23_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_SDPHY23_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY23_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA6 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY23_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_SDPHY23_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_SDPHY23_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_SDPHY23_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_SDPHY23_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* SDPHY23_CLK Selector */
/* Write to this register only if SDPHY23_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SDPHY23_CLK_SEL      0x18620118

/* CLKC_SDPHY23_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_SDPHY23_CLK_SEL__SEL__SHIFT       0
#define CLKC_SDPHY23_CLK_SEL__SEL__WIDTH       3
#define CLKC_SDPHY23_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SDPHY23_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SDPHY23_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SDPHY23_CLK Selector Status */
#define CLKC_SDPHY23_CLK_SEL_STATUS 0x1862011C

/* CLKC_SDPHY23_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SDPHY23_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SDPHY23_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SDPHY23_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SDPHY23_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY23_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SDPHY45_CLK Dividers Configuration */
/* Configure the dividers for SDPHY45_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_SDPHY45_CLK_CFG      0x18620120

/* CLKC_SDPHY45_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 7 of SYS0_BTPLL */
#define CLKC_SDPHY45_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_SDPHY45_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_SDPHY45_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_SDPHY45_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_SDPHY45_CLK_CFG__SYS0_QA__HW_DEFAULT  0x9

/* CLKC_SDPHY45_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 7 of SYS1_USBPLL */
#define CLKC_SDPHY45_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_SDPHY45_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_SDPHY45_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_SDPHY45_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_SDPHY45_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY45_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 7 of SYS2_ETHPLL */
#define CLKC_SDPHY45_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_SDPHY45_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_SDPHY45_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_SDPHY45_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_SDPHY45_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY45_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 7 of SYS3_SSCPLL */
#define CLKC_SDPHY45_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_SDPHY45_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_SDPHY45_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_SDPHY45_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_SDPHY45_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* SDPHY45_CLK Dividers Enable */
#define CLKC_SDPHY45_CLK_DIVENA   0x18620124

/* CLKC_SDPHY45_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 7 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY45_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_SDPHY45_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_SDPHY45_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_SDPHY45_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY45_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY45_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA7 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY45_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_SDPHY45_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_SDPHY45_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_SDPHY45_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_SDPHY45_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY45_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA7 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY45_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_SDPHY45_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_SDPHY45_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_SDPHY45_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_SDPHY45_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY45_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA7 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY45_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_SDPHY45_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_SDPHY45_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_SDPHY45_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_SDPHY45_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* SDPHY45_CLK Selector */
/* Write to this register only if SDPHY45_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SDPHY45_CLK_SEL      0x18620128

/* CLKC_SDPHY45_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_SDPHY45_CLK_SEL__SEL__SHIFT       0
#define CLKC_SDPHY45_CLK_SEL__SEL__WIDTH       3
#define CLKC_SDPHY45_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SDPHY45_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SDPHY45_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SDPHY45_CLK Selector Status */
#define CLKC_SDPHY45_CLK_SEL_STATUS 0x1862012C

/* CLKC_SDPHY45_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SDPHY45_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SDPHY45_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SDPHY45_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SDPHY45_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY45_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SDPHY67_CLK Dividers Configuration */
/* Configure the dividers for SDPHY67_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_SDPHY67_CLK_CFG      0x18620130

/* CLKC_SDPHY67_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 8 of SYS0_BTPLL */
#define CLKC_SDPHY67_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_SDPHY67_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_SDPHY67_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_SDPHY67_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_SDPHY67_CLK_CFG__SYS0_QA__HW_DEFAULT  0x9

/* CLKC_SDPHY67_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 8 of SYS1_USBPLL */
#define CLKC_SDPHY67_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_SDPHY67_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_SDPHY67_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_SDPHY67_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_SDPHY67_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY67_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 8 of SYS2_ETHPLL */
#define CLKC_SDPHY67_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_SDPHY67_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_SDPHY67_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_SDPHY67_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_SDPHY67_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_SDPHY67_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 8 of SYS3_SSCPLL */
#define CLKC_SDPHY67_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_SDPHY67_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_SDPHY67_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_SDPHY67_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_SDPHY67_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* SDPHY67_CLK Dividers Enable */
#define CLKC_SDPHY67_CLK_DIVENA   0x18620134

/* CLKC_SDPHY67_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 8 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY67_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_SDPHY67_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_SDPHY67_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_SDPHY67_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY67_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY67_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA8 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY67_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_SDPHY67_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_SDPHY67_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_SDPHY67_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_SDPHY67_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY67_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA8 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY67_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_SDPHY67_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_SDPHY67_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_SDPHY67_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_SDPHY67_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_SDPHY67_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA8 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SDPHY67_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_SDPHY67_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_SDPHY67_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_SDPHY67_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_SDPHY67_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* SDPHY67_CLK Selector */
/* Write to this register only if SDPHY67_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SDPHY67_CLK_SEL      0x18620138

/* CLKC_SDPHY67_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_SDPHY67_CLK_SEL__SEL__SHIFT       0
#define CLKC_SDPHY67_CLK_SEL__SEL__WIDTH       3
#define CLKC_SDPHY67_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SDPHY67_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SDPHY67_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SDPHY67_CLK Selector Status */
#define CLKC_SDPHY67_CLK_SEL_STATUS 0x1862013C

/* CLKC_SDPHY67_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SDPHY67_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SDPHY67_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SDPHY67_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SDPHY67_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SDPHY67_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* CAN_CLK Dividers Configuration */
/* Configure the dividers for CAN_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_CAN_CLK_CFG          0x18620140

/* CLKC_CAN_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 9 of SYS0_BTPLL */
#define CLKC_CAN_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_CAN_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_CAN_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_CAN_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_CAN_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_CAN_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 9 of SYS1_USBPLL */
#define CLKC_CAN_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_CAN_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_CAN_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_CAN_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_CAN_CLK_CFG__SYS1_QA__HW_DEFAULT  0x18

/* CLKC_CAN_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 9 of SYS2_ETHPLL */
#define CLKC_CAN_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_CAN_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_CAN_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_CAN_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_CAN_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_CAN_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 9 of SYS3_SSCPLL */
#define CLKC_CAN_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_CAN_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_CAN_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_CAN_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_CAN_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* CAN_CLK Dividers Enable */
#define CLKC_CAN_CLK_DIVENA       0x18620144

/* CLKC_CAN_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 9 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CAN_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_CAN_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_CAN_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_CAN_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_CAN_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_CAN_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA9 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CAN_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_CAN_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_CAN_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_CAN_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_CAN_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_CAN_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA9 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CAN_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_CAN_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_CAN_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_CAN_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_CAN_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_CAN_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA9 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_CAN_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_CAN_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_CAN_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_CAN_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_CAN_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* CAN_CLK Selector */
/* Write to this register only if CAN_CLK_SEL_STATUS.status is '1'. */
#define CLKC_CAN_CLK_SEL          0x18620148

/* CLKC_CAN_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_CAN_CLK_SEL__SEL__SHIFT       0
#define CLKC_CAN_CLK_SEL__SEL__WIDTH       3
#define CLKC_CAN_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_CAN_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_CAN_CLK_SEL__SEL__HW_DEFAULT  0x0

/* CAN_CLK Selector Status */
#define CLKC_CAN_CLK_SEL_STATUS   0x1862014C

/* CLKC_CAN_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_CAN_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_CAN_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_CAN_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_CAN_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_CAN_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* DEINT_CLK Dividers Configuration */
/* Configure the dividers for DEINT_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_DEINT_CLK_CFG        0x18620150

/* CLKC_DEINT_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 10 of SYS0_BTPLL */
#define CLKC_DEINT_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_DEINT_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_DEINT_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_DEINT_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_DEINT_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_DEINT_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 10 of SYS1_USBPLL */
#define CLKC_DEINT_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_DEINT_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_DEINT_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_DEINT_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_DEINT_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3F

/* CLKC_DEINT_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 10 of SYS2_ETHPLL */
#define CLKC_DEINT_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_DEINT_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_DEINT_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_DEINT_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_DEINT_CLK_CFG__SYS2_QA__HW_DEFAULT  0x8

/* CLKC_DEINT_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 10 of SYS3_SSCPLL */
#define CLKC_DEINT_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_DEINT_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_DEINT_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_DEINT_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_DEINT_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* DEINT_CLK Dividers Enable */
#define CLKC_DEINT_CLK_DIVENA     0x18620154

/* CLKC_DEINT_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 10 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DEINT_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_DEINT_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_DEINT_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_DEINT_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_DEINT_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DEINT_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA10 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DEINT_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_DEINT_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_DEINT_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_DEINT_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_DEINT_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DEINT_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA10 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DEINT_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_DEINT_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_DEINT_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_DEINT_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_DEINT_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DEINT_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA10 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DEINT_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_DEINT_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_DEINT_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_DEINT_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_DEINT_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* DEINT_CLK Selector */
/* Write to this register only if DEINT_CLK_SEL_STATUS.status is '1'. */
#define CLKC_DEINT_CLK_SEL        0x18620158

/* CLKC_DEINT_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_DEINT_CLK_SEL__SEL__SHIFT       0
#define CLKC_DEINT_CLK_SEL__SEL__WIDTH       3
#define CLKC_DEINT_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_DEINT_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_DEINT_CLK_SEL__SEL__HW_DEFAULT  0x0

/* DEINT_CLK Selector Status */
#define CLKC_DEINT_CLK_SEL_STATUS 0x1862015C

/* CLKC_DEINT_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_DEINT_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_DEINT_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_DEINT_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_DEINT_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_DEINT_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* NAND_CLK Dividers Configuration */
/* Configure the dividers for NAND_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_NAND_CLK_CFG         0x18620160

/* CLKC_NAND_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 11 of SYS0_BTPLL */
#define CLKC_NAND_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_NAND_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_NAND_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_NAND_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_NAND_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_NAND_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 11 of SYS1_USBPLL */
#define CLKC_NAND_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_NAND_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_NAND_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_NAND_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_NAND_CLK_CFG__SYS1_QA__HW_DEFAULT  0x1

/* CLKC_NAND_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 11 of SYS2_ETHPLL */
#define CLKC_NAND_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_NAND_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_NAND_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_NAND_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_NAND_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_NAND_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 11 of SYS3_SSCPLL */
#define CLKC_NAND_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_NAND_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_NAND_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_NAND_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_NAND_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* NAND_CLK Dividers Enable */
#define CLKC_NAND_CLK_DIVENA      0x18620164

/* CLKC_NAND_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 11 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_NAND_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_NAND_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_NAND_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_NAND_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_NAND_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_NAND_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA11 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_NAND_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_NAND_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_NAND_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_NAND_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_NAND_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_NAND_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA11 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_NAND_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_NAND_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_NAND_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_NAND_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_NAND_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_NAND_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA11 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_NAND_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_NAND_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_NAND_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_NAND_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_NAND_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* NAND_CLK Selector */
/* Write to this register only if NAND_CLK_SEL_STATUS.status is '1'. */
#define CLKC_NAND_CLK_SEL         0x18620168

/* CLKC_NAND_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_NAND_CLK_SEL__SEL__SHIFT       0
#define CLKC_NAND_CLK_SEL__SEL__WIDTH       3
#define CLKC_NAND_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_NAND_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_NAND_CLK_SEL__SEL__HW_DEFAULT  0x0

/* NAND_CLK Selector Status */
#define CLKC_NAND_CLK_SEL_STATUS  0x1862016C

/* CLKC_NAND_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_NAND_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_NAND_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_NAND_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_NAND_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_NAND_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* DISP0_CLK Dividers Configuration */
/* Configure the dividers for DISP0_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_DISP0_CLK_CFG        0x18620170

/* CLKC_DISP0_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 12 of SYS0_BTPLL */
#define CLKC_DISP0_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_DISP0_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_DISP0_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_DISP0_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_DISP0_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_DISP0_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 12 of SYS1_USBPLL */
#define CLKC_DISP0_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_DISP0_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_DISP0_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_DISP0_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_DISP0_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3

/* CLKC_DISP0_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 12 of SYS2_ETHPLL */
#define CLKC_DISP0_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_DISP0_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_DISP0_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_DISP0_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_DISP0_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_DISP0_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 12 of SYS3_SSCPLL */
#define CLKC_DISP0_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_DISP0_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_DISP0_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_DISP0_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_DISP0_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* DISP0_CLK Dividers Enable */
#define CLKC_DISP0_CLK_DIVENA     0x18620174

/* CLKC_DISP0_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 12 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP0_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_DISP0_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_DISP0_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_DISP0_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_DISP0_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP0_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA12 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP0_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_DISP0_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_DISP0_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_DISP0_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_DISP0_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP0_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA12 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP0_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_DISP0_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_DISP0_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_DISP0_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_DISP0_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP0_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA12 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP0_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_DISP0_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_DISP0_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_DISP0_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_DISP0_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* DISP0_CLK Selector */
/* Write to this register only if DISP0_CLK_SEL_STATUS.status is '1'. */
#define CLKC_DISP0_CLK_SEL        0x18620178

/* CLKC_DISP0_CLK_SEL.sel - Glitch-free selctor control */
/* 6 : DISP0_DTO */
/* 5 : SYS3_SSCPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 2 : SYS0_BTPLL divider */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_DISP0_CLK_SEL__SEL__SHIFT       0
#define CLKC_DISP0_CLK_SEL__SEL__WIDTH       3
#define CLKC_DISP0_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_DISP0_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_DISP0_CLK_SEL__SEL__HW_DEFAULT  0x0

/* DISP0_CLK Selector Status */
#define CLKC_DISP0_CLK_SEL_STATUS 0x1862017C

/* CLKC_DISP0_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_DISP0_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_DISP0_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_DISP0_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_DISP0_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_DISP0_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* DISP1_CLK Dividers Configuration */
/* Configure the dividers for DISP1_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_DISP1_CLK_CFG        0x18620180

/* CLKC_DISP1_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 13 of SYS0_BTPLL */
#define CLKC_DISP1_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_DISP1_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_DISP1_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_DISP1_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_DISP1_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_DISP1_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 13 of SYS1_USBPLL */
#define CLKC_DISP1_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_DISP1_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_DISP1_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_DISP1_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_DISP1_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3

/* CLKC_DISP1_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 13 of SYS2_ETHPLL */
#define CLKC_DISP1_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_DISP1_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_DISP1_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_DISP1_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_DISP1_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_DISP1_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 13 of SYS3_SSCPLL */
#define CLKC_DISP1_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_DISP1_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_DISP1_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_DISP1_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_DISP1_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* DISP1_CLK Dividers Enable */
#define CLKC_DISP1_CLK_DIVENA     0x18620184

/* CLKC_DISP1_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 13 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP1_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_DISP1_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_DISP1_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_DISP1_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_DISP1_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP1_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA13 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP1_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_DISP1_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_DISP1_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_DISP1_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_DISP1_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP1_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA13 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP1_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_DISP1_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_DISP1_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_DISP1_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_DISP1_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_DISP1_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA13 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_DISP1_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_DISP1_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_DISP1_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_DISP1_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_DISP1_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* DISP1_CLK Selector */
/* Write to this register only if DISP1_CLK_SEL_STATUS.status is '1'. */
#define CLKC_DISP1_CLK_SEL        0x18620188

/* CLKC_DISP1_CLK_SEL.sel - Glitch-free selctor control */
/* 6 : DISP1_DTO */
/* 5 : SYS3_SSCPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 2 : SYS0_BTPLL divider */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_DISP1_CLK_SEL__SEL__SHIFT       0
#define CLKC_DISP1_CLK_SEL__SEL__WIDTH       3
#define CLKC_DISP1_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_DISP1_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_DISP1_CLK_SEL__SEL__HW_DEFAULT  0x0

/* DISP1_CLK Selector Status */
#define CLKC_DISP1_CLK_SEL_STATUS 0x1862018C

/* CLKC_DISP1_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_DISP1_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_DISP1_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_DISP1_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_DISP1_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_DISP1_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* GPU_CLK Dividers Configuration */
/* Configure the dividers for GPU_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_GPU_CLK_CFG          0x18620190

/* CLKC_GPU_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 14 of SYS0_BTPLL */
#define CLKC_GPU_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_GPU_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_GPU_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_GPU_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_GPU_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_GPU_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 14 of SYS1_USBPLL */
#define CLKC_GPU_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_GPU_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_GPU_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_GPU_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_GPU_CLK_CFG__SYS1_QA__HW_DEFAULT  0x3

/* CLKC_GPU_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 14 of SYS2_ETHPLL */
#define CLKC_GPU_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_GPU_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_GPU_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_GPU_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_GPU_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_GPU_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 14 of SYS3_SSCPLL */
#define CLKC_GPU_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_GPU_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_GPU_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_GPU_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_GPU_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* GPU_CLK Dividers Enable */
#define CLKC_GPU_CLK_DIVENA       0x18620194

/* CLKC_GPU_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 14 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GPU_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_GPU_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_GPU_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_GPU_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_GPU_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_GPU_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA14 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GPU_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_GPU_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_GPU_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_GPU_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_GPU_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x0

/* CLKC_GPU_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA14 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GPU_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_GPU_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_GPU_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_GPU_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_GPU_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_GPU_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA14 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GPU_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_GPU_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_GPU_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_GPU_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_GPU_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* GPU_CLK Selector */
/* Write to this register only if GPU_CLK_SEL_STATUS.status is '1'. */
#define CLKC_GPU_CLK_SEL          0x18620198

/* CLKC_GPU_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_GPU_CLK_SEL__SEL__SHIFT       0
#define CLKC_GPU_CLK_SEL__SEL__WIDTH       3
#define CLKC_GPU_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_GPU_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_GPU_CLK_SEL__SEL__HW_DEFAULT  0x0

/* GPU_CLK Selector Status */
#define CLKC_GPU_CLK_SEL_STATUS   0x1862019C

/* CLKC_GPU_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_GPU_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_GPU_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_GPU_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_GPU_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_GPU_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* GNSS_CLK Dividers Configuration */
/* Configure the dividers for GNSS_CLK

 When the divider ratio is even (QA is odd) the duty cycle is 50% When the divider ratio is odd (QA is even) the duty cycle is given by:
 High (VCO cycles): integer((QA-1)/2)
 Low (VCO cycles): integer((QA+1)/2)
 */
#define CLKC_GNSS_CLK_CFG         0x186201A0

/* CLKC_GNSS_CLK_CFG.sys0_QA - SYS0_BTPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 15 of SYS0_BTPLL */
#define CLKC_GNSS_CLK_CFG__SYS0_QA__SHIFT       0
#define CLKC_GNSS_CLK_CFG__SYS0_QA__WIDTH       6
#define CLKC_GNSS_CLK_CFG__SYS0_QA__MASK        0x0000003F
#define CLKC_GNSS_CLK_CFG__SYS0_QA__INV_MASK    0xFFFFFFC0
#define CLKC_GNSS_CLK_CFG__SYS0_QA__HW_DEFAULT  0x3F

/* CLKC_GNSS_CLK_CFG.sys1_QA - SYS1_USBPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 15 of SYS1_USBPLL */
#define CLKC_GNSS_CLK_CFG__SYS1_QA__SHIFT       8
#define CLKC_GNSS_CLK_CFG__SYS1_QA__WIDTH       6
#define CLKC_GNSS_CLK_CFG__SYS1_QA__MASK        0x00003F00
#define CLKC_GNSS_CLK_CFG__SYS1_QA__INV_MASK    0xFFFFC0FF
#define CLKC_GNSS_CLK_CFG__SYS1_QA__HW_DEFAULT  0x8

/* CLKC_GNSS_CLK_CFG.sys2_QA - SYS2_ETHPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 15 of SYS2_ETHPLL */
#define CLKC_GNSS_CLK_CFG__SYS2_QA__SHIFT       16
#define CLKC_GNSS_CLK_CFG__SYS2_QA__WIDTH       6
#define CLKC_GNSS_CLK_CFG__SYS2_QA__MASK        0x003F0000
#define CLKC_GNSS_CLK_CFG__SYS2_QA__INV_MASK    0xFFC0FFFF
#define CLKC_GNSS_CLK_CFG__SYS2_QA__HW_DEFAULT  0x3F

/* CLKC_GNSS_CLK_CFG.sys3_QA - SYS3_SSCPLL divider */
/* Actual divider value is 'sys0_QA' + 1.
 Refers to QA 15 of SYS3_SSCPLL */
#define CLKC_GNSS_CLK_CFG__SYS3_QA__SHIFT       24
#define CLKC_GNSS_CLK_CFG__SYS3_QA__WIDTH       6
#define CLKC_GNSS_CLK_CFG__SYS3_QA__MASK        0x3F000000
#define CLKC_GNSS_CLK_CFG__SYS3_QA__INV_MASK    0xC0FFFFFF
#define CLKC_GNSS_CLK_CFG__SYS3_QA__HW_DEFAULT  0x3F

/* GNSS_CLK Dividers Enable */
#define CLKC_GNSS_CLK_DIVENA      0x186201A4

/* CLKC_GNSS_CLK_DIVENA.sys0_QA_ena - SYS0_BTPLL divider output enable */
/* Refers to QA 15 of SYS0_BTPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GNSS_CLK_DIVENA__SYS0_QA_ENA__SHIFT       0
#define CLKC_GNSS_CLK_DIVENA__SYS0_QA_ENA__WIDTH       1
#define CLKC_GNSS_CLK_DIVENA__SYS0_QA_ENA__MASK        0x00000001
#define CLKC_GNSS_CLK_DIVENA__SYS0_QA_ENA__INV_MASK    0xFFFFFFFE
#define CLKC_GNSS_CLK_DIVENA__SYS0_QA_ENA__HW_DEFAULT  0x0

/* CLKC_GNSS_CLK_DIVENA.sys1_QA_ena - SYS1_USBPLL divider output enable */
/* Refers to QA15 of SYS1_USBPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GNSS_CLK_DIVENA__SYS1_QA_ENA__SHIFT       4
#define CLKC_GNSS_CLK_DIVENA__SYS1_QA_ENA__WIDTH       1
#define CLKC_GNSS_CLK_DIVENA__SYS1_QA_ENA__MASK        0x00000010
#define CLKC_GNSS_CLK_DIVENA__SYS1_QA_ENA__INV_MASK    0xFFFFFFEF
#define CLKC_GNSS_CLK_DIVENA__SYS1_QA_ENA__HW_DEFAULT  0x1

/* CLKC_GNSS_CLK_DIVENA.sys2_QA_ena - SYS2_ETHPLL divider output enable */
/* Refers to QA15 of SYS2_ETHPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GNSS_CLK_DIVENA__SYS2_QA_ENA__SHIFT       8
#define CLKC_GNSS_CLK_DIVENA__SYS2_QA_ENA__WIDTH       1
#define CLKC_GNSS_CLK_DIVENA__SYS2_QA_ENA__MASK        0x00000100
#define CLKC_GNSS_CLK_DIVENA__SYS2_QA_ENA__INV_MASK    0xFFFFFEFF
#define CLKC_GNSS_CLK_DIVENA__SYS2_QA_ENA__HW_DEFAULT  0x0

/* CLKC_GNSS_CLK_DIVENA.sys3_QA_ena - SYS3_SSCPLL divider output enable */
/* Refers to QA15 of SYS3_SSCPLL */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_GNSS_CLK_DIVENA__SYS3_QA_ENA__SHIFT       12
#define CLKC_GNSS_CLK_DIVENA__SYS3_QA_ENA__WIDTH       1
#define CLKC_GNSS_CLK_DIVENA__SYS3_QA_ENA__MASK        0x00001000
#define CLKC_GNSS_CLK_DIVENA__SYS3_QA_ENA__INV_MASK    0xFFFFEFFF
#define CLKC_GNSS_CLK_DIVENA__SYS3_QA_ENA__HW_DEFAULT  0x0

/* GNSS_CLK Selector */
/* Write to this register only if GNSS_CLK_SEL_STATUS.status is '1'. */
#define CLKC_GNSS_CLK_SEL         0x186201A8

/* CLKC_GNSS_CLK_SEL.sel - Glitch-free selctor control */
/* 0 : XIN main crystal */
/* 1 : XINW */
/* 2 : SYS0_BTPLL divider */
/* 3 : SYS1_USBPLL divider */
/* 4 : SYS2_ETHPLL divider */
/* 5 : SYS3_SSCPLL divider */
#define CLKC_GNSS_CLK_SEL__SEL__SHIFT       0
#define CLKC_GNSS_CLK_SEL__SEL__WIDTH       3
#define CLKC_GNSS_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_GNSS_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_GNSS_CLK_SEL__SEL__HW_DEFAULT  0x0

/* GNSS_CLK Selector Status */
#define CLKC_GNSS_CLK_SEL_STATUS  0x186201AC

/* CLKC_GNSS_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_GNSS_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_GNSS_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_GNSS_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_GNSS_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_GNSS_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* Shared Dividers Configuration */
/* Configuration for dividers 17,18,19,20 on SYS0_BTPLL, SYS1_USBPLL and SYS2_ETH that can be used for limited flexibility clocks */
#define CLKC_SHARED_DIVIDERS_CFG0 0x186201B0

/* CLKC_SHARED_DIVIDERS_CFG0.sys2_div20 - sys2_div20 value */
/* Actual divider value is 'sys2_div20' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG0__SYS2_DIV20__SHIFT       0
#define CLKC_SHARED_DIVIDERS_CFG0__SYS2_DIV20__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG0__SYS2_DIV20__MASK        0x0000003F
#define CLKC_SHARED_DIVIDERS_CFG0__SYS2_DIV20__INV_MASK    0xFFFFFFC0
#define CLKC_SHARED_DIVIDERS_CFG0__SYS2_DIV20__HW_DEFAULT  0xF

/* CLKC_SHARED_DIVIDERS_CFG0.sys1_div20 - sys1_div20 value */
/* Actual divider value is 'sys1_div20' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV20__SHIFT       8
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV20__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV20__MASK        0x00003F00
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV20__INV_MASK    0xFFFFC0FF
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV20__HW_DEFAULT  0x7

/* CLKC_SHARED_DIVIDERS_CFG0.sys1_div19 - sys1_div19 value */
/* Actual divider value is 'sys1_div19' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV19__SHIFT       16
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV19__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV19__MASK        0x003F0000
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV19__INV_MASK    0xFFC0FFFF
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV19__HW_DEFAULT  0x5

/* CLKC_SHARED_DIVIDERS_CFG0.sys1_div18 - sys1_div18 value */
/* Actual divider value is 'sys1_div18' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV18__SHIFT       24
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV18__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV18__MASK        0x3F000000
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV18__INV_MASK    0xC0FFFFFF
#define CLKC_SHARED_DIVIDERS_CFG0__SYS1_DIV18__HW_DEFAULT  0x4

/* Shared Dividers Configuration */
/* Configuration for dividers 18 and 19 on SYS2_ETHPLL and SYS3_SSCPLL that can be used for limited flexibility clocks */
#define CLKC_SHARED_DIVIDERS_CFG1 0x186201B4

/* CLKC_SHARED_DIVIDERS_CFG1.sys0_div20 - sys0_div20 value */
/* Actual divider value is 'sys0_div20' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG1__SYS0_DIV20__SHIFT       0
#define CLKC_SHARED_DIVIDERS_CFG1__SYS0_DIV20__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG1__SYS0_DIV20__MASK        0x0000003F
#define CLKC_SHARED_DIVIDERS_CFG1__SYS0_DIV20__INV_MASK    0xFFFFFFC0
#define CLKC_SHARED_DIVIDERS_CFG1__SYS0_DIV20__HW_DEFAULT  0x3

/* CLKC_SHARED_DIVIDERS_CFG1.sys1_div17 - sys1_div17 value */
/* Actual divider value is 'sys1_div17' + 1 */
#define CLKC_SHARED_DIVIDERS_CFG1__SYS1_DIV17__SHIFT       8
#define CLKC_SHARED_DIVIDERS_CFG1__SYS1_DIV17__WIDTH       6
#define CLKC_SHARED_DIVIDERS_CFG1__SYS1_DIV17__MASK        0x00003F00
#define CLKC_SHARED_DIVIDERS_CFG1__SYS1_DIV17__INV_MASK    0xFFFFC0FF
#define CLKC_SHARED_DIVIDERS_CFG1__SYS1_DIV17__HW_DEFAULT  0x3

/* Shared Dividers Enable */
/* Enable for output of dividers 18 and 19 on all SYS PLLs that can be used for limited flexibility clocks */
#define CLKC_SHARED_DIVIDERS_ENA  0x186201B8

/* CLKC_SHARED_DIVIDERS_ENA.sys2_div20 - sys2_div20 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS2_DIV20__SHIFT       0
#define CLKC_SHARED_DIVIDERS_ENA__SYS2_DIV20__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS2_DIV20__MASK        0x00000001
#define CLKC_SHARED_DIVIDERS_ENA__SYS2_DIV20__INV_MASK    0xFFFFFFFE
#define CLKC_SHARED_DIVIDERS_ENA__SYS2_DIV20__HW_DEFAULT  0x0

/* CLKC_SHARED_DIVIDERS_ENA.sys1_div20 - sys1_div20 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV20__SHIFT       4
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV20__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV20__MASK        0x00000010
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV20__INV_MASK    0xFFFFFFEF
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV20__HW_DEFAULT  0x0

/* CLKC_SHARED_DIVIDERS_ENA.sys1_div19 - sys1_div19 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV19__SHIFT       8
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV19__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV19__MASK        0x00000100
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV19__INV_MASK    0xFFFFFEFF
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV19__HW_DEFAULT  0x0

/* CLKC_SHARED_DIVIDERS_ENA.sys1_div18 - sys1_div18 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV18__SHIFT       12
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV18__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV18__MASK        0x00001000
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV18__INV_MASK    0xFFFFEFFF
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV18__HW_DEFAULT  0x0

/* CLKC_SHARED_DIVIDERS_ENA.sys0_div20 - sys0_div20 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS0_DIV20__SHIFT       16
#define CLKC_SHARED_DIVIDERS_ENA__SYS0_DIV20__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS0_DIV20__MASK        0x00010000
#define CLKC_SHARED_DIVIDERS_ENA__SYS0_DIV20__INV_MASK    0xFFFEFFFF
#define CLKC_SHARED_DIVIDERS_ENA__SYS0_DIV20__HW_DEFAULT  0x0

/* CLKC_SHARED_DIVIDERS_ENA.sys1_div17 - SYS1_USBPLL DIV20 output enable */
/* 0 : Disabled */
/* 1 : Enabled */
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV17__SHIFT       20
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV17__WIDTH       1
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV17__MASK        0x00100000
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV17__INV_MASK    0xFFEFFFFF
#define CLKC_SHARED_DIVIDERS_ENA__SYS1_DIV17__HW_DEFAULT  0x0

/* SYS_CLK Selector */
/* Write to this register only if SYS_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SYS_CLK_SEL          0x186201BC

/* CLKC_SYS_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_SYS_CLK_SEL__SEL__SHIFT       0
#define CLKC_SYS_CLK_SEL__SEL__WIDTH       3
#define CLKC_SYS_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SYS_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SYS_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SYS_CLK Selector Status */
#define CLKC_SYS_CLK_SEL_STATUS   0x186201C0

/* CLKC_SYS_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SYS_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SYS_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SYS_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SYS_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SYS_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* IO_CLK Selector */
/* Write to this register only if IO_CLK_SEL_STATUS.status is '1'. */
#define CLKC_IO_CLK_SEL           0x186201C4

/* CLKC_IO_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_IO_CLK_SEL__SEL__SHIFT       0
#define CLKC_IO_CLK_SEL__SEL__WIDTH       3
#define CLKC_IO_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_IO_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_IO_CLK_SEL__SEL__HW_DEFAULT  0x0

/* IO_CLK Selector Status */
#define CLKC_IO_CLK_SEL_STATUS    0x186201C8

/* CLKC_IO_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_IO_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_IO_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_IO_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_IO_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_IO_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* G2D_CLK Selector */
/* Write to this register only if G2D_CLK_SEL_STATUS.status is '1'. */
#define CLKC_G2D_CLK_SEL          0x186201CC

/* CLKC_G2D_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_G2D_CLK_SEL__SEL__SHIFT       0
#define CLKC_G2D_CLK_SEL__SEL__WIDTH       3
#define CLKC_G2D_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_G2D_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_G2D_CLK_SEL__SEL__HW_DEFAULT  0x0

/* G2D_CLK Selector Status */
#define CLKC_G2D_CLK_SEL_STATUS   0x186201D0

/* CLKC_G2D_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_G2D_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_G2D_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_G2D_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_G2D_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_G2D_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* JPENC_CLK Selector */
/* Write to this register only if JPENC_CLK_SEL_STATUS.status is '1'. */
#define CLKC_JPENC_CLK_SEL        0x186201D4

/* CLKC_JPENC_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_JPENC_CLK_SEL__SEL__SHIFT       0
#define CLKC_JPENC_CLK_SEL__SEL__WIDTH       3
#define CLKC_JPENC_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_JPENC_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_JPENC_CLK_SEL__SEL__HW_DEFAULT  0x0

/* JPENC_CLK Selector Status */
#define CLKC_JPENC_CLK_SEL_STATUS 0x186201D8

/* CLKC_JPENC_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_JPENC_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_JPENC_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_JPENC_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_JPENC_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_JPENC_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* VDEC_CLK Selector */
/* Write to this register only if VDEC_CLK_SEL_STATUS.status is '1'. */
#define CLKC_VDEC_CLK_SEL         0x186201DC

/* CLKC_VDEC_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_VDEC_CLK_SEL__SEL__SHIFT       0
#define CLKC_VDEC_CLK_SEL__SEL__WIDTH       3
#define CLKC_VDEC_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_VDEC_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_VDEC_CLK_SEL__SEL__HW_DEFAULT  0x0

/* VDEC_CLK Selector Status */
#define CLKC_VDEC_CLK_SEL_STATUS  0x186201E0

/* CLKC_VDEC_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_VDEC_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_VDEC_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_VDEC_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_VDEC_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_VDEC_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* GMAC_CLK Selector */
/* Write to this register only if GMAC_CLK_SEL_STATUS.status is '1'. */
#define CLKC_GMAC_CLK_SEL         0x186201E4

/* CLKC_GMAC_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_GMAC_CLK_SEL__SEL__SHIFT       0
#define CLKC_GMAC_CLK_SEL__SEL__WIDTH       3
#define CLKC_GMAC_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_GMAC_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_GMAC_CLK_SEL__SEL__HW_DEFAULT  0x0

/* GMAC_CLK Selector Status */
#define CLKC_GMAC_CLK_SEL_STATUS  0x186201E8

/* CLKC_GMAC_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_GMAC_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_GMAC_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_GMAC_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_GMAC_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_GMAC_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* USB_CLK Selector */
/* Write to this register only if USB_CLK_SEL_STATUS.status is '1'. */
#define CLKC_USB_CLK_SEL          0x186201EC

/* CLKC_USB_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_USB_CLK_SEL__SEL__SHIFT       0
#define CLKC_USB_CLK_SEL__SEL__WIDTH       3
#define CLKC_USB_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_USB_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_USB_CLK_SEL__SEL__HW_DEFAULT  0x0

/* USB_CLK Selector Status */
#define CLKC_USB_CLK_SEL_STATUS   0x186201F0

/* CLKC_USB_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_USB_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_USB_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_USB_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_USB_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_USB_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* KAS_CLK Selector */
/* Write to this register only if KAS_CLK_SEL_STATUS.status is '1'. */
#define CLKC_KAS_CLK_SEL          0x186201F4

/* CLKC_KAS_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_KAS_CLK_SEL__SEL__SHIFT       0
#define CLKC_KAS_CLK_SEL__SEL__WIDTH       3
#define CLKC_KAS_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_KAS_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_KAS_CLK_SEL__SEL__HW_DEFAULT  0x0

/* KAS_CLK Selector Status */
#define CLKC_KAS_CLK_SEL_STATUS   0x186201F8

/* CLKC_KAS_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_KAS_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_KAS_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_KAS_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_KAS_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_KAS_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SEC_CLK Selector */
/* Write to this register only if SEC_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SEC_CLK_SEL          0x186201FC

/* CLKC_SEC_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_SEC_CLK_SEL__SEL__SHIFT       0
#define CLKC_SEC_CLK_SEL__SEL__WIDTH       3
#define CLKC_SEC_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SEC_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SEC_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SEC_CLK Selector Status */
#define CLKC_SEC_CLK_SEL_STATUS   0x18620200

/* CLKC_SEC_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SEC_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SEC_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SEC_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SEC_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SEC_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* SDR_CLK Selector */
/* Write to this register only if SDR_CLK_SEL_STATUS.status is '1'. */
#define CLKC_SDR_CLK_SEL          0x18620204

/* CLKC_SDR_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_SDR_CLK_SEL__SEL__SHIFT       0
#define CLKC_SDR_CLK_SEL__SEL__WIDTH       3
#define CLKC_SDR_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_SDR_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_SDR_CLK_SEL__SEL__HW_DEFAULT  0x0

/* SDR_CLK Selector Status */
#define CLKC_SDR_CLK_SEL_STATUS   0x18620208

/* CLKC_SDR_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_SDR_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_SDR_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_SDR_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_SDR_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_SDR_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* VIP_CLK Selector */
/* Write to this register only if VIP_CLK_SEL_STATUS.status is '1'. */
#define CLKC_VIP_CLK_SEL          0x1862020C

/* CLKC_VIP_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_VIP_CLK_SEL__SEL__SHIFT       0
#define CLKC_VIP_CLK_SEL__SEL__WIDTH       3
#define CLKC_VIP_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_VIP_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_VIP_CLK_SEL__SEL__HW_DEFAULT  0x0

/* VIP_CLK Selector Status */
#define CLKC_VIP_CLK_SEL_STATUS   0x18620210

/* CLKC_VIP_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_VIP_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_VIP_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_VIP_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_VIP_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_VIP_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* NOCD_CLK Selector */
/* Write to this register only if NOCD_CLK_SEL_STATUS.status is '1'. */
#define CLKC_NOCD_CLK_SEL         0x18620214

/* CLKC_NOCD_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_NOCD_CLK_SEL__SEL__SHIFT       0
#define CLKC_NOCD_CLK_SEL__SEL__WIDTH       3
#define CLKC_NOCD_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_NOCD_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_NOCD_CLK_SEL__SEL__HW_DEFAULT  0x0

/* NOCD_CLK Selector Status */
#define CLKC_NOCD_CLK_SEL_STATUS  0x18620218

/* CLKC_NOCD_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_NOCD_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_NOCD_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_NOCD_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_NOCD_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_NOCD_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* NOCR_CLK Selector */
/* Write to this register only if NOCR_CLK_SEL_STATUS.status is '1'. */
#define CLKC_NOCR_CLK_SEL         0x1862021C

/* CLKC_NOCR_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_NOCR_CLK_SEL__SEL__SHIFT       0
#define CLKC_NOCR_CLK_SEL__SEL__WIDTH       3
#define CLKC_NOCR_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_NOCR_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_NOCR_CLK_SEL__SEL__HW_DEFAULT  0x0

/* NOCR_CLK Selector Status */
#define CLKC_NOCR_CLK_SEL_STATUS  0x18620220

/* CLKC_NOCR_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_NOCR_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_NOCR_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_NOCR_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_NOCR_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_NOCR_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* TPIU_CLK Selector */
/* Write to this register only if TPIU_CLK_SEL_STATUS.status is '1'. */
#define CLKC_TPIU_CLK_SEL         0x18620224

/* CLKC_TPIU_CLK_SEL.sel - Glitch-free selctor control */
/* 7 : SYS1_USBPLL DIV17 */
/* 6 : SYS0_BTPLL DIV20 */
/* 5 : SYS1_USBPLL DIV18 */
/* 4 : SYS1_USBPLL DIV19 */
/* 3 : SYS1_USBPLL DIV20 */
/* 2 : SYS2_ETHPLL DIV20 */
/* 1 : XINW */
/* 0 : XIN main crystal */
#define CLKC_TPIU_CLK_SEL__SEL__SHIFT       0
#define CLKC_TPIU_CLK_SEL__SEL__WIDTH       3
#define CLKC_TPIU_CLK_SEL__SEL__MASK        0x00000007
#define CLKC_TPIU_CLK_SEL__SEL__INV_MASK    0xFFFFFFF8
#define CLKC_TPIU_CLK_SEL__SEL__HW_DEFAULT  0x0

/* TPIU_CLK Selector Status */
#define CLKC_TPIU_CLK_SEL_STATUS  0x18620228

/* CLKC_TPIU_CLK_SEL_STATUS.status - Clock selector status */
/* 1 : Selector stable, switch not in progress */
/* 0 : Selector switch in progress */
#define CLKC_TPIU_CLK_SEL_STATUS__STATUS__SHIFT       0
#define CLKC_TPIU_CLK_SEL_STATUS__STATUS__WIDTH       1
#define CLKC_TPIU_CLK_SEL_STATUS__STATUS__MASK        0x00000001
#define CLKC_TPIU_CLK_SEL_STATUS__STATUS__INV_MASK    0xFFFFFFFE
#define CLKC_TPIU_CLK_SEL_STATUS__STATUS__HW_DEFAULT  0x1

/* Root Clock Enable Set */
/* Writing 1 to a bit enables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the ROOT_CLK_EN status.
 This register controls clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_SET/CLR registers to control clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN0_SET     0x1862022C

/* CLKC_ROOT_CLK_EN0_SET.AUDMSCM_KAS - AUDMSCM_KAS clock enable */
/* 0 : No effect */
/* 1 : Enable AUDMSCM_KAS clock */
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_KAS__SHIFT       0
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_KAS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_KAS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.GNSS - GNSS clock enable */
/* 0 : No effect */
/* 1 : Enable GNSS clock */
#define CLKC_ROOT_CLK_EN0_SET__GNSS__SHIFT       1
#define CLKC_ROOT_CLK_EN0_SET__GNSS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__GNSS__MASK        0x00000002
#define CLKC_ROOT_CLK_EN0_SET__GNSS__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN0_SET__GNSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.GPU - GPU clock enable */
/* 0 : No effect */
/* 1 : Enable GPU clock */
#define CLKC_ROOT_CLK_EN0_SET__GPU__SHIFT       2
#define CLKC_ROOT_CLK_EN0_SET__GPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__GPU__MASK        0x00000004
#define CLKC_ROOT_CLK_EN0_SET__GPU__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN0_SET__GPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.G2D - G2D clock enable */
/* 0 : No effect */
/* 1 : Enable G2D clock */
#define CLKC_ROOT_CLK_EN0_SET__G2D__SHIFT       3
#define CLKC_ROOT_CLK_EN0_SET__G2D__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__G2D__MASK        0x00000008
#define CLKC_ROOT_CLK_EN0_SET__G2D__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN0_SET__G2D__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.JPENC - JPENC clock enable */
/* 0 : No effect */
/* 1 : Enable JPENC clock */
#define CLKC_ROOT_CLK_EN0_SET__JPENC__SHIFT       4
#define CLKC_ROOT_CLK_EN0_SET__JPENC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__JPENC__MASK        0x00000010
#define CLKC_ROOT_CLK_EN0_SET__JPENC__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN0_SET__JPENC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.DISP0 - DISP0 clock enable */
/* 0 : No effect */
/* 1 : Enable DISP0 clock */
#define CLKC_ROOT_CLK_EN0_SET__DISP0__SHIFT       5
#define CLKC_ROOT_CLK_EN0_SET__DISP0__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__DISP0__MASK        0x00000020
#define CLKC_ROOT_CLK_EN0_SET__DISP0__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN0_SET__DISP0__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.DISP1 - DISP1 clock enable */
/* 0 : No effect */
/* 1 : Enable DISP1 clock */
#define CLKC_ROOT_CLK_EN0_SET__DISP1__SHIFT       6
#define CLKC_ROOT_CLK_EN0_SET__DISP1__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__DISP1__MASK        0x00000040
#define CLKC_ROOT_CLK_EN0_SET__DISP1__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN0_SET__DISP1__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.I2S - I2S clock enable */
/* 0 : No effect */
/* 1 : Enable I2S clock */
#define CLKC_ROOT_CLK_EN0_SET__I2S__SHIFT       8
#define CLKC_ROOT_CLK_EN0_SET__I2S__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__I2S__MASK        0x00000100
#define CLKC_ROOT_CLK_EN0_SET__I2S__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN0_SET__I2S__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.AUDMSCM_IO - AUDMSCM_IO clock enable */
/* 0 : No effect */
/* 1 : Enable AUDMSCM_IO clock */
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__SHIFT       11
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__MASK        0x00000800
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.VDIFM_IO - VDIFM_IO clock enable */
/* 0 : No effect */
/* 1 : Enable VDIFM_IO clock */
#define CLKC_ROOT_CLK_EN0_SET__VDIFM_IO__SHIFT       12
#define CLKC_ROOT_CLK_EN0_SET__VDIFM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__VDIFM_IO__MASK        0x00001000
#define CLKC_ROOT_CLK_EN0_SET__VDIFM_IO__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN0_SET__VDIFM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.GNSSM_IO - GNSSM_IO clock enable */
/* 0 : No effect */
/* 1 : Enable GNSSM_IO clock */
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__SHIFT       13
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__MASK        0x00002000
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.MEDIAM_IO - MEDIAM_IO clock enable */
/* 0 : No effect */
/* 1 : Enable MEDIAM_IO clock */
#define CLKC_ROOT_CLK_EN0_SET__MEDIAM_IO__SHIFT       14
#define CLKC_ROOT_CLK_EN0_SET__MEDIAM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__MEDIAM_IO__MASK        0x00004000
#define CLKC_ROOT_CLK_EN0_SET__MEDIAM_IO__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN0_SET__MEDIAM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.BTM_IO - BTM_IO clock enable */
/* 0 : No effect */
/* 1 : Enable BTM_IO clock */
#define CLKC_ROOT_CLK_EN0_SET__BTM_IO__SHIFT       17
#define CLKC_ROOT_CLK_EN0_SET__BTM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__BTM_IO__MASK        0x00020000
#define CLKC_ROOT_CLK_EN0_SET__BTM_IO__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN0_SET__BTM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.SDPHY01 - SDPHY01 clock enable */
/* 0 : No effect */
/* 1 : Enable SDPHY01 clock */
#define CLKC_ROOT_CLK_EN0_SET__SDPHY01__SHIFT       18
#define CLKC_ROOT_CLK_EN0_SET__SDPHY01__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__SDPHY01__MASK        0x00040000
#define CLKC_ROOT_CLK_EN0_SET__SDPHY01__INV_MASK    0xFFFBFFFF
#define CLKC_ROOT_CLK_EN0_SET__SDPHY01__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.SDPHY23 - SDPHY23 clock enable */
/* 0 : No effect */
/* 1 : Enable SDPHY23 clock */
#define CLKC_ROOT_CLK_EN0_SET__SDPHY23__SHIFT       19
#define CLKC_ROOT_CLK_EN0_SET__SDPHY23__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__SDPHY23__MASK        0x00080000
#define CLKC_ROOT_CLK_EN0_SET__SDPHY23__INV_MASK    0xFFF7FFFF
#define CLKC_ROOT_CLK_EN0_SET__SDPHY23__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.SDPHY45 - SDPHY45 clock enable */
/* 0 : No effect */
/* 1 : Enable SDPHY45 clock */
#define CLKC_ROOT_CLK_EN0_SET__SDPHY45__SHIFT       20
#define CLKC_ROOT_CLK_EN0_SET__SDPHY45__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__SDPHY45__MASK        0x00100000
#define CLKC_ROOT_CLK_EN0_SET__SDPHY45__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN0_SET__SDPHY45__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.SDPHY67 - SDPHY67 clock enable */
/* 0 : No effect */
/* 1 : Enable SDPHY67 clock */
#define CLKC_ROOT_CLK_EN0_SET__SDPHY67__SHIFT       21
#define CLKC_ROOT_CLK_EN0_SET__SDPHY67__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__SDPHY67__MASK        0x00200000
#define CLKC_ROOT_CLK_EN0_SET__SDPHY67__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN0_SET__SDPHY67__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.AUDMSCM_XIN - AUDMSCM_XIN clock enable */
/* 0 : No effect */
/* 1 : Enable AUDMSCM_XIN clock */
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_XIN__SHIFT       22
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_XIN__MASK        0x00400000
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_XIN__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN0_SET__AUDMSCM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.NAND - NAND clock enable */
/* 0 : No effect */
/* 1 : Enable NAND clock */
#define CLKC_ROOT_CLK_EN0_SET__NAND__SHIFT       27
#define CLKC_ROOT_CLK_EN0_SET__NAND__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__NAND__MASK        0x08000000
#define CLKC_ROOT_CLK_EN0_SET__NAND__INV_MASK    0xF7FFFFFF
#define CLKC_ROOT_CLK_EN0_SET__NAND__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.GNSSM_RTCM_SEC - GNSSM_RTCM_SEC clock enable */
/* 0 : No effect */
/* 1 : Enable GNSSM_RTCM_SEC clock */
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_RTCM_SEC__SHIFT       28
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_RTCM_SEC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_RTCM_SEC__MASK        0x10000000
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_RTCM_SEC__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_RTCM_SEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.CPUM_CPU - CPUM_CPU clock enable */
/* 0 : No effect */
/* 1 : Enable CPUM_CPU clock */
#define CLKC_ROOT_CLK_EN0_SET__CPUM_CPU__SHIFT       29
#define CLKC_ROOT_CLK_EN0_SET__CPUM_CPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__CPUM_CPU__MASK        0x20000000
#define CLKC_ROOT_CLK_EN0_SET__CPUM_CPU__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN0_SET__CPUM_CPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.GNSSM_XIN - GNSSM_XIN clock enable */
/* 0 : No effect */
/* 1 : Enable GNSSM_XIN clock */
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_XIN__SHIFT       30
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_XIN__MASK        0x40000000
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_XIN__INV_MASK    0xBFFFFFFF
#define CLKC_ROOT_CLK_EN0_SET__GNSSM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_SET.VIP - VIP clock enable */
/* 0 : No effect */
/* 1 : Enable VIP clock */
#define CLKC_ROOT_CLK_EN0_SET__VIP__SHIFT       31
#define CLKC_ROOT_CLK_EN0_SET__VIP__WIDTH       1
#define CLKC_ROOT_CLK_EN0_SET__VIP__MASK        0x80000000
#define CLKC_ROOT_CLK_EN0_SET__VIP__INV_MASK    0x7FFFFFFF
#define CLKC_ROOT_CLK_EN0_SET__VIP__HW_DEFAULT  0x1

/* Root Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the ROOT_CLK_EN status.
This register controls clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_SET/CLR registers to control clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN0_CLR     0x18620230

/* CLKC_ROOT_CLK_EN0_CLR.AUDMSCM_KAS - AUDMSCM_KAS clock disable */
/* 0 : No effect */
/* 1 : Disable AUDMSCM_KAS clock */
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_KAS__SHIFT       0
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_KAS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_KAS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.GNSS - GNSS clock disable */
/* 0 : No effect */
/* 1 : Disable GNSS clock */
#define CLKC_ROOT_CLK_EN0_CLR__GNSS__SHIFT       1
#define CLKC_ROOT_CLK_EN0_CLR__GNSS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__GNSS__MASK        0x00000002
#define CLKC_ROOT_CLK_EN0_CLR__GNSS__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN0_CLR__GNSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.GPU - GPU clock disable */
/* 0 : No effect */
/* 1 : Disable GPU clock */
#define CLKC_ROOT_CLK_EN0_CLR__GPU__SHIFT       2
#define CLKC_ROOT_CLK_EN0_CLR__GPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__GPU__MASK        0x00000004
#define CLKC_ROOT_CLK_EN0_CLR__GPU__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN0_CLR__GPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.G2D - G2D clock disable */
/* 0 : No effect */
/* 1 : Disable G2D clock */
#define CLKC_ROOT_CLK_EN0_CLR__G2D__SHIFT       3
#define CLKC_ROOT_CLK_EN0_CLR__G2D__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__G2D__MASK        0x00000008
#define CLKC_ROOT_CLK_EN0_CLR__G2D__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN0_CLR__G2D__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.JPENC - JPENC clock disable */
/* 0 : No effect */
/* 1 : Disable JPENC clock */
#define CLKC_ROOT_CLK_EN0_CLR__JPENC__SHIFT       4
#define CLKC_ROOT_CLK_EN0_CLR__JPENC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__JPENC__MASK        0x00000010
#define CLKC_ROOT_CLK_EN0_CLR__JPENC__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN0_CLR__JPENC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.DISP0 - DISP0 clock disable */
/* 0 : No effect */
/* 1 : Disable DISP0 clock */
#define CLKC_ROOT_CLK_EN0_CLR__DISP0__SHIFT       5
#define CLKC_ROOT_CLK_EN0_CLR__DISP0__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__DISP0__MASK        0x00000020
#define CLKC_ROOT_CLK_EN0_CLR__DISP0__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN0_CLR__DISP0__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.DISP1 - DISP1 clock disable */
/* 0 : No effect */
/* 1 : Disable DISP1 clock */
#define CLKC_ROOT_CLK_EN0_CLR__DISP1__SHIFT       6
#define CLKC_ROOT_CLK_EN0_CLR__DISP1__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__DISP1__MASK        0x00000040
#define CLKC_ROOT_CLK_EN0_CLR__DISP1__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN0_CLR__DISP1__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.I2S - I2S clock disable */
/* 0 : No effect */
/* 1 : Disable I2S clock */
#define CLKC_ROOT_CLK_EN0_CLR__I2S__SHIFT       8
#define CLKC_ROOT_CLK_EN0_CLR__I2S__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__I2S__MASK        0x00000100
#define CLKC_ROOT_CLK_EN0_CLR__I2S__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN0_CLR__I2S__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.AUDMSCM_IO - AUDMSCM_IO clock disable */
/* 0 : No effect */
/* 1 : Disable AUDMSCM_IO clock */
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_IO__SHIFT       11
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_IO__MASK        0x00000800
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_IO__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.VDIFM_IO - VDIFM_IO clock disable */
/* 0 : No effect */
/* 1 : Disable VDIFM_IO clock */
#define CLKC_ROOT_CLK_EN0_CLR__VDIFM_IO__SHIFT       12
#define CLKC_ROOT_CLK_EN0_CLR__VDIFM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__VDIFM_IO__MASK        0x00001000
#define CLKC_ROOT_CLK_EN0_CLR__VDIFM_IO__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN0_CLR__VDIFM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.GNSSM_IO - GNSSM_IO clock disable */
/* 0 : No effect */
/* 1 : Disable GNSSM_IO clock */
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_IO__SHIFT       13
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_IO__MASK        0x00002000
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_IO__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.MEDIAM_IO - MEDIAM_IO clock disable */
/* 0 : No effect */
/* 1 : Disable MEDIAM_IO clock */
#define CLKC_ROOT_CLK_EN0_CLR__MEDIAM_IO__SHIFT       14
#define CLKC_ROOT_CLK_EN0_CLR__MEDIAM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__MEDIAM_IO__MASK        0x00004000
#define CLKC_ROOT_CLK_EN0_CLR__MEDIAM_IO__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN0_CLR__MEDIAM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.BTM_IO - BTM_IO clock disable */
/* 0 : No effect */
/* 1 : Disable BTM_IO clock */
#define CLKC_ROOT_CLK_EN0_CLR__BTM_IO__SHIFT       17
#define CLKC_ROOT_CLK_EN0_CLR__BTM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__BTM_IO__MASK        0x00020000
#define CLKC_ROOT_CLK_EN0_CLR__BTM_IO__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN0_CLR__BTM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.SDPHY01 - SDPHY01 clock disable */
/* 0 : No effect */
/* 1 : Disable SDPHY01 clock */
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY01__SHIFT       18
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY01__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY01__MASK        0x00040000
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY01__INV_MASK    0xFFFBFFFF
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY01__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.SDPHY23 - SDPHY23 clock disable */
/* 0 : No effect */
/* 1 : Disable SDPHY23 clock */
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY23__SHIFT       19
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY23__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY23__MASK        0x00080000
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY23__INV_MASK    0xFFF7FFFF
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY23__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.SDPHY45 - SDPHY45 clock disable */
/* 0 : No effect */
/* 1 : Disable SDPHY45 clock */
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY45__SHIFT       20
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY45__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY45__MASK        0x00100000
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY45__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY45__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.SDPHY67 - SDPHY67 clock disable */
/* 0 : No effect */
/* 1 : Disable SDPHY67 clock */
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY67__SHIFT       21
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY67__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY67__MASK        0x00200000
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY67__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__SDPHY67__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.AUDMSCM_XIN - AUDMSCM_XIN clock disable */
/* 0 : No effect */
/* 1 : Disable AUDMSCM_XIN clock */
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_XIN__SHIFT       22
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_XIN__MASK        0x00400000
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_XIN__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__AUDMSCM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.NAND - NAND clock disable */
/* 0 : No effect */
/* 1 : Disable NAND clock */
#define CLKC_ROOT_CLK_EN0_CLR__NAND__SHIFT       27
#define CLKC_ROOT_CLK_EN0_CLR__NAND__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__NAND__MASK        0x08000000
#define CLKC_ROOT_CLK_EN0_CLR__NAND__INV_MASK    0xF7FFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__NAND__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.GNSSM_RTCM_SEC - GNSSM_RTCM_SEC clock disable */
/* 0 : No effect */
/* 1 : Disable GNSSM_RTCM_SEC clock */
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_RTCM_SEC__SHIFT       28
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_RTCM_SEC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_RTCM_SEC__MASK        0x10000000
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_RTCM_SEC__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_RTCM_SEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.CPUM_CPU - CPUM_CPU clock disable */
/* 0 : No effect */
/* 1 : Disable CPUM_CPU clock */
#define CLKC_ROOT_CLK_EN0_CLR__CPUM_CPU__SHIFT       29
#define CLKC_ROOT_CLK_EN0_CLR__CPUM_CPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__CPUM_CPU__MASK        0x20000000
#define CLKC_ROOT_CLK_EN0_CLR__CPUM_CPU__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__CPUM_CPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.GNSSM_XIN - GNSSM_XIN clock disable */
/* 0 : No effect */
/* 1 : Disable GNSSM_XIN clock */
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_XIN__SHIFT       30
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_XIN__MASK        0x40000000
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_XIN__INV_MASK    0xBFFFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__GNSSM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_CLR.VIP - VIP clock disable */
/* 0 : No effect */
/* 1 : Disable VIP clock */
#define CLKC_ROOT_CLK_EN0_CLR__VIP__SHIFT       31
#define CLKC_ROOT_CLK_EN0_CLR__VIP__WIDTH       1
#define CLKC_ROOT_CLK_EN0_CLR__VIP__MASK        0x80000000
#define CLKC_ROOT_CLK_EN0_CLR__VIP__INV_MASK    0x7FFFFFFF
#define CLKC_ROOT_CLK_EN0_CLR__VIP__HW_DEFAULT  0x1

/* Root Clock Enable Status */
/* Status of clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_STATUS registers to observe status of clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN0_STATUS  0x18620234

/* CLKC_ROOT_CLK_EN0_STATUS.AUDMSCM_KAS - AUDMSCM_KAS clock enable status */
/* 0 : AUDMSCM_KAS clock disabled */
/* 1 : AUDMSCM_KAS clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_KAS__SHIFT       0
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_KAS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_KAS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.GNSS - GNSS clock enable status */
/* 0 : GNSS clock disabled */
/* 1 : GNSS clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__GNSS__SHIFT       1
#define CLKC_ROOT_CLK_EN0_STATUS__GNSS__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__GNSS__MASK        0x00000002
#define CLKC_ROOT_CLK_EN0_STATUS__GNSS__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN0_STATUS__GNSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.GPU - GPU clock enable status */
/* 0 : GPU clock disabled */
/* 1 : GPU clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__GPU__SHIFT       2
#define CLKC_ROOT_CLK_EN0_STATUS__GPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__GPU__MASK        0x00000004
#define CLKC_ROOT_CLK_EN0_STATUS__GPU__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN0_STATUS__GPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.G2D - G2D clock enable status */
/* 0 : G2D clock disabled */
/* 1 : G2D clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__G2D__SHIFT       3
#define CLKC_ROOT_CLK_EN0_STATUS__G2D__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__G2D__MASK        0x00000008
#define CLKC_ROOT_CLK_EN0_STATUS__G2D__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN0_STATUS__G2D__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.JPENC - JPENC clock enable status */
/* 0 : JPENC clock disabled */
/* 1 : JPENC clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__JPENC__SHIFT       4
#define CLKC_ROOT_CLK_EN0_STATUS__JPENC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__JPENC__MASK        0x00000010
#define CLKC_ROOT_CLK_EN0_STATUS__JPENC__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN0_STATUS__JPENC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.DISP0 - DISP0 clock enable status */
/* 0 : DISP0 clock disabled */
/* 1 : DISP0 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__DISP0__SHIFT       5
#define CLKC_ROOT_CLK_EN0_STATUS__DISP0__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__DISP0__MASK        0x00000020
#define CLKC_ROOT_CLK_EN0_STATUS__DISP0__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN0_STATUS__DISP0__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.DISP1 - DISP1 clock enable status */
/* 0 : DISP1 clock disabled */
/* 1 : DISP1 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__DISP1__SHIFT       6
#define CLKC_ROOT_CLK_EN0_STATUS__DISP1__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__DISP1__MASK        0x00000040
#define CLKC_ROOT_CLK_EN0_STATUS__DISP1__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN0_STATUS__DISP1__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.I2S - I2S clock enable status */
/* 0 : I2S clock disabled */
/* 1 : I2S clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__I2S__SHIFT       8
#define CLKC_ROOT_CLK_EN0_STATUS__I2S__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__I2S__MASK        0x00000100
#define CLKC_ROOT_CLK_EN0_STATUS__I2S__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN0_STATUS__I2S__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.AUDMSCM_IO - AUDMSCM_IO clock enable status */
/* 0 : AUDMSCM_IO clock disabled */
/* 1 : AUDMSCM_IO clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_IO__SHIFT       11
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_IO__MASK        0x00000800
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_IO__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.VDIFM_IO - VDIFM_IO clock enable status */
/* 0 : VDIFM_IO clock disabled */
/* 1 : VDIFM_IO clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__VDIFM_IO__SHIFT       12
#define CLKC_ROOT_CLK_EN0_STATUS__VDIFM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__VDIFM_IO__MASK        0x00001000
#define CLKC_ROOT_CLK_EN0_STATUS__VDIFM_IO__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN0_STATUS__VDIFM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.GNSSM_IO - GNSSM_IO clock enable status */
/* 0 : GNSSM_IO clock disabled */
/* 1 : GNSSM_IO clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_IO__SHIFT       13
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_IO__MASK        0x00002000
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_IO__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.MEDIAM_IO - MEDIAM_IO clock enable status */
/* 0 : MEDIAM_IO clock disabled */
/* 1 : MEDIAM_IO clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__MEDIAM_IO__SHIFT       14
#define CLKC_ROOT_CLK_EN0_STATUS__MEDIAM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__MEDIAM_IO__MASK        0x00004000
#define CLKC_ROOT_CLK_EN0_STATUS__MEDIAM_IO__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN0_STATUS__MEDIAM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.BTM_IO - BTM_IO clock enable status */
/* 0 : BTM_IO clock disabled */
/* 1 : BTM_IO clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__BTM_IO__SHIFT       17
#define CLKC_ROOT_CLK_EN0_STATUS__BTM_IO__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__BTM_IO__MASK        0x00020000
#define CLKC_ROOT_CLK_EN0_STATUS__BTM_IO__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__BTM_IO__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.SDPHY01 - SDPHY01 clock enable status */
/* 0 : SDPHY01 clock disabled */
/* 1 : SDPHY01 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY01__SHIFT       18
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY01__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY01__MASK        0x00040000
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY01__INV_MASK    0xFFFBFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY01__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.SDPHY23 - SDPHY23 clock enable status */
/* 0 : SDPHY23 clock disabled */
/* 1 : SDPHY23 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY23__SHIFT       19
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY23__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY23__MASK        0x00080000
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY23__INV_MASK    0xFFF7FFFF
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY23__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.SDPHY45 - SDPHY45 clock enable status */
/* 0 : SDPHY45 clock disabled */
/* 1 : SDPHY45 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY45__SHIFT       20
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY45__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY45__MASK        0x00100000
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY45__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY45__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.SDPHY67 - SDPHY67 clock enable status */
/* 0 : SDPHY67 clock disabled */
/* 1 : SDPHY67 clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY67__SHIFT       21
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY67__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY67__MASK        0x00200000
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY67__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__SDPHY67__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.AUDMSCM_XIN - AUDMSCM_XIN clock enable status */
/* 0 : AUDMSCM_XIN clock disabled */
/* 1 : AUDMSCM_XIN clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_XIN__SHIFT       22
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_XIN__MASK        0x00400000
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_XIN__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__AUDMSCM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.NAND - NAND clock enable status */
/* 0 : NAND clock disabled */
/* 1 : NAND clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__NAND__SHIFT       27
#define CLKC_ROOT_CLK_EN0_STATUS__NAND__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__NAND__MASK        0x08000000
#define CLKC_ROOT_CLK_EN0_STATUS__NAND__INV_MASK    0xF7FFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__NAND__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.GNSSM_RTCM_SEC - GNSSM_RTCM_SEC clock enable status */
/* 0 : GNSSM_RTCM_SEC clock disabled */
/* 1 : GNSSM_RTCM_SEC clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_RTCM_SEC__SHIFT       28
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_RTCM_SEC__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_RTCM_SEC__MASK        0x10000000
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_RTCM_SEC__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_RTCM_SEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.CPUM_CPU - CPUM_CPU clock enable status */
/* 0 : CPUM_CPU clock disabled */
/* 1 : CPUM_CPU clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__CPUM_CPU__SHIFT       29
#define CLKC_ROOT_CLK_EN0_STATUS__CPUM_CPU__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__CPUM_CPU__MASK        0x20000000
#define CLKC_ROOT_CLK_EN0_STATUS__CPUM_CPU__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__CPUM_CPU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.GNSSM_XIN - GNSSM_XIN clock enable status */
/* 0 : GNSSM_XIN clock disabled */
/* 1 : GNSSM_XIN clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_XIN__SHIFT       30
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_XIN__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_XIN__MASK        0x40000000
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_XIN__INV_MASK    0xBFFFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__GNSSM_XIN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN0_STATUS.VIP - VIP clock enable status */
/* 0 : VIP clock disabled */
/* 1 : VIP clock enabled */
#define CLKC_ROOT_CLK_EN0_STATUS__VIP__SHIFT       31
#define CLKC_ROOT_CLK_EN0_STATUS__VIP__WIDTH       1
#define CLKC_ROOT_CLK_EN0_STATUS__VIP__MASK        0x80000000
#define CLKC_ROOT_CLK_EN0_STATUS__VIP__INV_MASK    0x7FFFFFFF
#define CLKC_ROOT_CLK_EN0_STATUS__VIP__HW_DEFAULT  0x1

/* Root Clock Enable Set */
/* Writing 1 to a bit enables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the ROOT_CLK_EN status.
 This register controls clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_SET/CLR registers to control clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN1_SET     0x18620238

/* CLKC_ROOT_CLK_EN1_SET.BTSS - BTSS clock enable */
/* 0 : No effect */
/* 1 : Enable BTSS clock */
#define CLKC_ROOT_CLK_EN1_SET__BTSS__SHIFT       0
#define CLKC_ROOT_CLK_EN1_SET__BTSS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__BTSS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN1_SET__BTSS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN1_SET__BTSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.USBPHY - USBPHY clock enable */
/* 0 : No effect */
/* 1 : Enable USBPHY clock */
#define CLKC_ROOT_CLK_EN1_SET__USBPHY__SHIFT       1
#define CLKC_ROOT_CLK_EN1_SET__USBPHY__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__USBPHY__MASK        0x00000002
#define CLKC_ROOT_CLK_EN1_SET__USBPHY__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN1_SET__USBPHY__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.RTCM_KAS - RTCM_KAS clock enable */
/* 0 : No effect */
/* 1 : Enable RTCM_KAS clock */
#define CLKC_ROOT_CLK_EN1_SET__RTCM_KAS__SHIFT       2
#define CLKC_ROOT_CLK_EN1_SET__RTCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__RTCM_KAS__MASK        0x00000004
#define CLKC_ROOT_CLK_EN1_SET__RTCM_KAS__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN1_SET__RTCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.AUDMSCM_NOCD - AUDMSCM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable AUDMSCM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCD__SHIFT       3
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCD__MASK        0x00000008
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCD__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.VDIFM_NOCD - VDIFM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable VDIFM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCD__SHIFT       4
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCD__MASK        0x00000010
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCD__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.GNSSM_NOCD - GNSSM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable GNSSM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCD__SHIFT       5
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCD__MASK        0x00000020
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCD__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.MEDIAM_NOCD - MEDIAM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable MEDIAM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCD__SHIFT       6
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCD__MASK        0x00000040
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCD__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.CPUM_NOCD - CPUM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable CPUM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__CPUM_NOCD__SHIFT       8
#define CLKC_ROOT_CLK_EN1_SET__CPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__CPUM_NOCD__MASK        0x00000100
#define CLKC_ROOT_CLK_EN1_SET__CPUM_NOCD__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN1_SET__CPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.GPUM_NOCD - GPUM_NOCD clock enable */
/* 0 : No effect */
/* 1 : Enable GPUM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCD__SHIFT       9
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCD__MASK        0x00000200
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCD__INV_MASK    0xFFFFFDFF
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.AUDMSCM_NOCR - AUDMSCM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable AUDMSCM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCR__SHIFT       11
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCR__MASK        0x00000800
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCR__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN1_SET__AUDMSCM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.VDIFM_NOCR - VDIFM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable VDIFM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCR__SHIFT       12
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCR__MASK        0x00001000
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCR__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN1_SET__VDIFM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.GNSSM_NOCR - GNSSM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable GNSSM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCR__SHIFT       13
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCR__MASK        0x00002000
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCR__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN1_SET__GNSSM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.MEDIAM_NOCR - MEDIAM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable MEDIAM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCR__SHIFT       14
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCR__MASK        0x00004000
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCR__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN1_SET__MEDIAM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.DDRM_NOCR - DDRM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable DDRM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__DDRM_NOCR__SHIFT       15
#define CLKC_ROOT_CLK_EN1_SET__DDRM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__DDRM_NOCR__MASK        0x00008000
#define CLKC_ROOT_CLK_EN1_SET__DDRM_NOCR__INV_MASK    0xFFFF7FFF
#define CLKC_ROOT_CLK_EN1_SET__DDRM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.TPIU - TPIU clock enable */
/* 0 : No effect */
/* 1 : Enable TPIU clock */
#define CLKC_ROOT_CLK_EN1_SET__TPIU__SHIFT       16
#define CLKC_ROOT_CLK_EN1_SET__TPIU__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__TPIU__MASK        0x00010000
#define CLKC_ROOT_CLK_EN1_SET__TPIU__INV_MASK    0xFFFEFFFF
#define CLKC_ROOT_CLK_EN1_SET__TPIU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.GPUM_NOCR - GPUM_NOCR clock enable */
/* 0 : No effect */
/* 1 : Enable GPUM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCR__SHIFT       17
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCR__MASK        0x00020000
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCR__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN1_SET__GPUM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.RGMII - RGMII clock enable */
/* 0 : No effect */
/* 1 : Enable RGMII clock */
#define CLKC_ROOT_CLK_EN1_SET__RGMII__SHIFT       20
#define CLKC_ROOT_CLK_EN1_SET__RGMII__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__RGMII__MASK        0x00100000
#define CLKC_ROOT_CLK_EN1_SET__RGMII__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN1_SET__RGMII__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.VDEC - VDEC clock enable */
/* 0 : No effect */
/* 1 : Enable VDEC clock */
#define CLKC_ROOT_CLK_EN1_SET__VDEC__SHIFT       21
#define CLKC_ROOT_CLK_EN1_SET__VDEC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__VDEC__MASK        0x00200000
#define CLKC_ROOT_CLK_EN1_SET__VDEC__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN1_SET__VDEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.SDR - SDR clock enable */
/* 0 : No effect */
/* 1 : Enable SDR clock */
#define CLKC_ROOT_CLK_EN1_SET__SDR__SHIFT       22
#define CLKC_ROOT_CLK_EN1_SET__SDR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__SDR__MASK        0x00400000
#define CLKC_ROOT_CLK_EN1_SET__SDR__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN1_SET__SDR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.DEINT - DEINT clock enable */
/* 0 : No effect */
/* 1 : Enable DEINT clock */
#define CLKC_ROOT_CLK_EN1_SET__DEINT__SHIFT       23
#define CLKC_ROOT_CLK_EN1_SET__DEINT__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__DEINT__MASK        0x00800000
#define CLKC_ROOT_CLK_EN1_SET__DEINT__INV_MASK    0xFF7FFFFF
#define CLKC_ROOT_CLK_EN1_SET__DEINT__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.BTSLOW - BTSLOW clock enable */
/* 0 : No effect */
/* 1 : Enable BTSLOW clock */
#define CLKC_ROOT_CLK_EN1_SET__BTSLOW__SHIFT       25
#define CLKC_ROOT_CLK_EN1_SET__BTSLOW__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__BTSLOW__MASK        0x02000000
#define CLKC_ROOT_CLK_EN1_SET__BTSLOW__INV_MASK    0xFDFFFFFF
#define CLKC_ROOT_CLK_EN1_SET__BTSLOW__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.CAN - CAN clock enable */
/* 0 : No effect */
/* 1 : Enable CAN clock */
#define CLKC_ROOT_CLK_EN1_SET__CAN__SHIFT       26
#define CLKC_ROOT_CLK_EN1_SET__CAN__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__CAN__MASK        0x04000000
#define CLKC_ROOT_CLK_EN1_SET__CAN__INV_MASK    0xFBFFFFFF
#define CLKC_ROOT_CLK_EN1_SET__CAN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.USB - USB clock enable */
/* 0 : No effect */
/* 1 : Enable USB clock */
#define CLKC_ROOT_CLK_EN1_SET__USB__SHIFT       28
#define CLKC_ROOT_CLK_EN1_SET__USB__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__USB__MASK        0x10000000
#define CLKC_ROOT_CLK_EN1_SET__USB__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN1_SET__USB__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_SET.GMAC - GMAC clock enable */
/* 0 : No effect */
/* 1 : Enable GMAC clock */
#define CLKC_ROOT_CLK_EN1_SET__GMAC__SHIFT       29
#define CLKC_ROOT_CLK_EN1_SET__GMAC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_SET__GMAC__MASK        0x20000000
#define CLKC_ROOT_CLK_EN1_SET__GMAC__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN1_SET__GMAC__HW_DEFAULT  0x1

/* Root Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the ROOT_CLK_EN status.
This register controls clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_SET/CLR registers to control clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN1_CLR     0x1862023C

/* CLKC_ROOT_CLK_EN1_CLR.BTSS - BTSS clock disable */
/* 0 : No effect */
/* 1 : Disable BTSS clock */
#define CLKC_ROOT_CLK_EN1_CLR__BTSS__SHIFT       0
#define CLKC_ROOT_CLK_EN1_CLR__BTSS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__BTSS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN1_CLR__BTSS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN1_CLR__BTSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.USBPHY - USBPHY clock disable */
/* 0 : No effect */
/* 1 : Disable USBPHY clock */
#define CLKC_ROOT_CLK_EN1_CLR__USBPHY__SHIFT       1
#define CLKC_ROOT_CLK_EN1_CLR__USBPHY__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__USBPHY__MASK        0x00000002
#define CLKC_ROOT_CLK_EN1_CLR__USBPHY__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN1_CLR__USBPHY__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.RTCM_KAS - RTCM_KAS clock disable */
/* 0 : No effect */
/* 1 : Disable RTCM_KAS clock */
#define CLKC_ROOT_CLK_EN1_CLR__RTCM_KAS__SHIFT       2
#define CLKC_ROOT_CLK_EN1_CLR__RTCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__RTCM_KAS__MASK        0x00000004
#define CLKC_ROOT_CLK_EN1_CLR__RTCM_KAS__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN1_CLR__RTCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.AUDMSCM_NOCD - AUDMSCM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable AUDMSCM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCD__SHIFT       3
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCD__MASK        0x00000008
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCD__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.VDIFM_NOCD - VDIFM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable VDIFM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCD__SHIFT       4
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCD__MASK        0x00000010
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCD__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.GNSSM_NOCD - GNSSM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable GNSSM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCD__SHIFT       5
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCD__MASK        0x00000020
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCD__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.MEDIAM_NOCD - MEDIAM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable MEDIAM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCD__SHIFT       6
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCD__MASK        0x00000040
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCD__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.CPUM_NOCD - CPUM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable CPUM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__CPUM_NOCD__SHIFT       8
#define CLKC_ROOT_CLK_EN1_CLR__CPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__CPUM_NOCD__MASK        0x00000100
#define CLKC_ROOT_CLK_EN1_CLR__CPUM_NOCD__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN1_CLR__CPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.GPUM_NOCD - GPUM_NOCD clock disable */
/* 0 : No effect */
/* 1 : Disable GPUM_NOCD clock */
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCD__SHIFT       9
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCD__MASK        0x00000200
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCD__INV_MASK    0xFFFFFDFF
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.AUDMSCM_NOCR - AUDMSCM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable AUDMSCM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCR__SHIFT       11
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCR__MASK        0x00000800
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCR__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN1_CLR__AUDMSCM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.VDIFM_NOCR - VDIFM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable VDIFM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCR__SHIFT       12
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCR__MASK        0x00001000
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCR__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN1_CLR__VDIFM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.GNSSM_NOCR - GNSSM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable GNSSM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCR__SHIFT       13
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCR__MASK        0x00002000
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCR__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN1_CLR__GNSSM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.MEDIAM_NOCR - MEDIAM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable MEDIAM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCR__SHIFT       14
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCR__MASK        0x00004000
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCR__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN1_CLR__MEDIAM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.DDRM_NOCR - DDRM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable DDRM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__DDRM_NOCR__SHIFT       15
#define CLKC_ROOT_CLK_EN1_CLR__DDRM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__DDRM_NOCR__MASK        0x00008000
#define CLKC_ROOT_CLK_EN1_CLR__DDRM_NOCR__INV_MASK    0xFFFF7FFF
#define CLKC_ROOT_CLK_EN1_CLR__DDRM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.TPIU - TPIU clock disable */
/* 0 : No effect */
/* 1 : Disable TPIU clock */
#define CLKC_ROOT_CLK_EN1_CLR__TPIU__SHIFT       16
#define CLKC_ROOT_CLK_EN1_CLR__TPIU__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__TPIU__MASK        0x00010000
#define CLKC_ROOT_CLK_EN1_CLR__TPIU__INV_MASK    0xFFFEFFFF
#define CLKC_ROOT_CLK_EN1_CLR__TPIU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.GPUM_NOCR - GPUM_NOCR clock disable */
/* 0 : No effect */
/* 1 : Disable GPUM_NOCR clock */
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCR__SHIFT       17
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCR__MASK        0x00020000
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCR__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN1_CLR__GPUM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.RGMII - RGMII clock disable */
/* 0 : No effect */
/* 1 : Disable RGMII clock */
#define CLKC_ROOT_CLK_EN1_CLR__RGMII__SHIFT       20
#define CLKC_ROOT_CLK_EN1_CLR__RGMII__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__RGMII__MASK        0x00100000
#define CLKC_ROOT_CLK_EN1_CLR__RGMII__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__RGMII__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.VDEC - VDEC clock disable */
/* 0 : No effect */
/* 1 : Disable VDEC clock */
#define CLKC_ROOT_CLK_EN1_CLR__VDEC__SHIFT       21
#define CLKC_ROOT_CLK_EN1_CLR__VDEC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__VDEC__MASK        0x00200000
#define CLKC_ROOT_CLK_EN1_CLR__VDEC__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__VDEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.SDR - SDR clock disable */
/* 0 : No effect */
/* 1 : Disable SDR clock */
#define CLKC_ROOT_CLK_EN1_CLR__SDR__SHIFT       22
#define CLKC_ROOT_CLK_EN1_CLR__SDR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__SDR__MASK        0x00400000
#define CLKC_ROOT_CLK_EN1_CLR__SDR__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__SDR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.DEINT - DEINT clock disable */
/* 0 : No effect */
/* 1 : Disable DEINT clock */
#define CLKC_ROOT_CLK_EN1_CLR__DEINT__SHIFT       23
#define CLKC_ROOT_CLK_EN1_CLR__DEINT__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__DEINT__MASK        0x00800000
#define CLKC_ROOT_CLK_EN1_CLR__DEINT__INV_MASK    0xFF7FFFFF
#define CLKC_ROOT_CLK_EN1_CLR__DEINT__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.BTSLOW - BTSLOW clock disable */
/* 0 : No effect */
/* 1 : Disable BTSLOW clock */
#define CLKC_ROOT_CLK_EN1_CLR__BTSLOW__SHIFT       25
#define CLKC_ROOT_CLK_EN1_CLR__BTSLOW__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__BTSLOW__MASK        0x02000000
#define CLKC_ROOT_CLK_EN1_CLR__BTSLOW__INV_MASK    0xFDFFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__BTSLOW__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.CAN - CAN clock disable */
/* 0 : No effect */
/* 1 : Disable CAN clock */
#define CLKC_ROOT_CLK_EN1_CLR__CAN__SHIFT       26
#define CLKC_ROOT_CLK_EN1_CLR__CAN__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__CAN__MASK        0x04000000
#define CLKC_ROOT_CLK_EN1_CLR__CAN__INV_MASK    0xFBFFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__CAN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.USB - USB clock disable */
/* 0 : No effect */
/* 1 : Disable USB clock */
#define CLKC_ROOT_CLK_EN1_CLR__USB__SHIFT       28
#define CLKC_ROOT_CLK_EN1_CLR__USB__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__USB__MASK        0x10000000
#define CLKC_ROOT_CLK_EN1_CLR__USB__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__USB__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_CLR.GMAC - GMAC clock disable */
/* 0 : No effect */
/* 1 : Disable GMAC clock */
#define CLKC_ROOT_CLK_EN1_CLR__GMAC__SHIFT       29
#define CLKC_ROOT_CLK_EN1_CLR__GMAC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_CLR__GMAC__MASK        0x20000000
#define CLKC_ROOT_CLK_EN1_CLR__GMAC__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN1_CLR__GMAC__HW_DEFAULT  0x1

/* Root Clock Enable Status */
/* Status of clock gating at the root of the clock tree in the Clock Controller unit
 Use LEAF_CLK*_STATUS registers to observe status of clocks at the receiving unit. */
#define CLKC_ROOT_CLK_EN1_STATUS  0x18620240

/* CLKC_ROOT_CLK_EN1_STATUS.BTSS - BTSS clock enable status */
/* 0 : BTSS clock disabled */
/* 1 : BTSS clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__BTSS__SHIFT       0
#define CLKC_ROOT_CLK_EN1_STATUS__BTSS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__BTSS__MASK        0x00000001
#define CLKC_ROOT_CLK_EN1_STATUS__BTSS__INV_MASK    0xFFFFFFFE
#define CLKC_ROOT_CLK_EN1_STATUS__BTSS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.USBPHY - USBPHY clock enable status */
/* 0 : USBPHY clock disabled */
/* 1 : USBPHY clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__USBPHY__SHIFT       1
#define CLKC_ROOT_CLK_EN1_STATUS__USBPHY__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__USBPHY__MASK        0x00000002
#define CLKC_ROOT_CLK_EN1_STATUS__USBPHY__INV_MASK    0xFFFFFFFD
#define CLKC_ROOT_CLK_EN1_STATUS__USBPHY__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.RTCM_KAS - RTCM_KAS clock enable status */
/* 0 : RTCM_KAS clock disabled */
/* 1 : RTCM_KAS clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__RTCM_KAS__SHIFT       2
#define CLKC_ROOT_CLK_EN1_STATUS__RTCM_KAS__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__RTCM_KAS__MASK        0x00000004
#define CLKC_ROOT_CLK_EN1_STATUS__RTCM_KAS__INV_MASK    0xFFFFFFFB
#define CLKC_ROOT_CLK_EN1_STATUS__RTCM_KAS__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.AUDMSCM_NOCD - AUDMSCM_NOCD clock enable status */
/* 0 : AUDMSCM_NOCD clock disabled */
/* 1 : AUDMSCM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCD__SHIFT       3
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCD__MASK        0x00000008
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCD__INV_MASK    0xFFFFFFF7
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.VDIFM_NOCD - VDIFM_NOCD clock enable status */
/* 0 : VDIFM_NOCD clock disabled */
/* 1 : VDIFM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCD__SHIFT       4
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCD__MASK        0x00000010
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCD__INV_MASK    0xFFFFFFEF
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.GNSSM_NOCD - GNSSM_NOCD clock enable status */
/* 0 : GNSSM_NOCD clock disabled */
/* 1 : GNSSM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCD__SHIFT       5
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCD__MASK        0x00000020
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCD__INV_MASK    0xFFFFFFDF
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.MEDIAM_NOCD - MEDIAM_NOCD clock enable status */
/* 0 : MEDIAM_NOCD clock disabled */
/* 1 : MEDIAM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCD__SHIFT       6
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCD__MASK        0x00000040
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCD__INV_MASK    0xFFFFFFBF
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.CPUM_NOCD - CPUM_NOCD clock enable status */
/* 0 : CPUM_NOCD clock disabled */
/* 1 : CPUM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__CPUM_NOCD__SHIFT       8
#define CLKC_ROOT_CLK_EN1_STATUS__CPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__CPUM_NOCD__MASK        0x00000100
#define CLKC_ROOT_CLK_EN1_STATUS__CPUM_NOCD__INV_MASK    0xFFFFFEFF
#define CLKC_ROOT_CLK_EN1_STATUS__CPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.GPUM_NOCD - GPUM_NOCD clock enable status */
/* 0 : GPUM_NOCD clock disabled */
/* 1 : GPUM_NOCD clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCD__SHIFT       9
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCD__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCD__MASK        0x00000200
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCD__INV_MASK    0xFFFFFDFF
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCD__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.AUDMSCM_NOCR - AUDMSCM_NOCR clock enable status */
/* 0 : AUDMSCM_NOCR clock disabled */
/* 1 : AUDMSCM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCR__SHIFT       11
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCR__MASK        0x00000800
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCR__INV_MASK    0xFFFFF7FF
#define CLKC_ROOT_CLK_EN1_STATUS__AUDMSCM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.VDIFM_NOCR - VDIFM_NOCR clock enable status */
/* 0 : VDIFM_NOCR clock disabled */
/* 1 : VDIFM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCR__SHIFT       12
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCR__MASK        0x00001000
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCR__INV_MASK    0xFFFFEFFF
#define CLKC_ROOT_CLK_EN1_STATUS__VDIFM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.GNSSM_NOCR - GNSSM_NOCR clock enable status */
/* 0 : GNSSM_NOCR clock disabled */
/* 1 : GNSSM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCR__SHIFT       13
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCR__MASK        0x00002000
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCR__INV_MASK    0xFFFFDFFF
#define CLKC_ROOT_CLK_EN1_STATUS__GNSSM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.MEDIAM_NOCR - MEDIAM_NOCR clock enable status */
/* 0 : MEDIAM_NOCR clock disabled */
/* 1 : MEDIAM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCR__SHIFT       14
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCR__MASK        0x00004000
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCR__INV_MASK    0xFFFFBFFF
#define CLKC_ROOT_CLK_EN1_STATUS__MEDIAM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.DDRM_NOCR - DDRM_NOCR clock enable status */
/* 0 : DDRM_NOCR clock disabled */
/* 1 : DDRM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__DDRM_NOCR__SHIFT       15
#define CLKC_ROOT_CLK_EN1_STATUS__DDRM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__DDRM_NOCR__MASK        0x00008000
#define CLKC_ROOT_CLK_EN1_STATUS__DDRM_NOCR__INV_MASK    0xFFFF7FFF
#define CLKC_ROOT_CLK_EN1_STATUS__DDRM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.TPIU - TPIU clock enable status */
/* 0 : TPIU clock disabled */
/* 1 : TPIU clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__TPIU__SHIFT       16
#define CLKC_ROOT_CLK_EN1_STATUS__TPIU__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__TPIU__MASK        0x00010000
#define CLKC_ROOT_CLK_EN1_STATUS__TPIU__INV_MASK    0xFFFEFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__TPIU__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.GPUM_NOCR - GPUM_NOCR clock enable status */
/* 0 : GPUM_NOCR clock disabled */
/* 1 : GPUM_NOCR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCR__SHIFT       17
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCR__MASK        0x00020000
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCR__INV_MASK    0xFFFDFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__GPUM_NOCR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.RGMII - RGMII clock enable status */
/* 0 : RGMII clock disabled */
/* 1 : RGMII clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__RGMII__SHIFT       20
#define CLKC_ROOT_CLK_EN1_STATUS__RGMII__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__RGMII__MASK        0x00100000
#define CLKC_ROOT_CLK_EN1_STATUS__RGMII__INV_MASK    0xFFEFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__RGMII__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.VDEC - VDEC clock enable status */
/* 0 : VDEC clock disabled */
/* 1 : VDEC clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__VDEC__SHIFT       21
#define CLKC_ROOT_CLK_EN1_STATUS__VDEC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__VDEC__MASK        0x00200000
#define CLKC_ROOT_CLK_EN1_STATUS__VDEC__INV_MASK    0xFFDFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__VDEC__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.SDR - SDR clock enable status */
/* 0 : SDR clock disabled */
/* 1 : SDR clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__SDR__SHIFT       22
#define CLKC_ROOT_CLK_EN1_STATUS__SDR__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__SDR__MASK        0x00400000
#define CLKC_ROOT_CLK_EN1_STATUS__SDR__INV_MASK    0xFFBFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__SDR__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.DEINT - DEINT clock enable status */
/* 0 : DEINT clock disabled */
/* 1 : DEINT clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__DEINT__SHIFT       23
#define CLKC_ROOT_CLK_EN1_STATUS__DEINT__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__DEINT__MASK        0x00800000
#define CLKC_ROOT_CLK_EN1_STATUS__DEINT__INV_MASK    0xFF7FFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__DEINT__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.BTSLOW - BTSLOW clock enable status */
/* 0 : BTSLOW clock disabled */
/* 1 : BTSLOW clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__BTSLOW__SHIFT       25
#define CLKC_ROOT_CLK_EN1_STATUS__BTSLOW__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__BTSLOW__MASK        0x02000000
#define CLKC_ROOT_CLK_EN1_STATUS__BTSLOW__INV_MASK    0xFDFFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__BTSLOW__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.CAN - CAN clock enable status */
/* 0 : CAN clock disabled */
/* 1 : CAN clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__CAN__SHIFT       26
#define CLKC_ROOT_CLK_EN1_STATUS__CAN__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__CAN__MASK        0x04000000
#define CLKC_ROOT_CLK_EN1_STATUS__CAN__INV_MASK    0xFBFFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__CAN__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.USB - USB clock enable status */
/* 0 : USB clock disabled */
/* 1 : USB clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__USB__SHIFT       28
#define CLKC_ROOT_CLK_EN1_STATUS__USB__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__USB__MASK        0x10000000
#define CLKC_ROOT_CLK_EN1_STATUS__USB__INV_MASK    0xEFFFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__USB__HW_DEFAULT  0x1

/* CLKC_ROOT_CLK_EN1_STATUS.GMAC - GMAC clock enable status */
/* 0 : GMAC clock disabled */
/* 1 : GMAC clock enabled */
#define CLKC_ROOT_CLK_EN1_STATUS__GMAC__SHIFT       29
#define CLKC_ROOT_CLK_EN1_STATUS__GMAC__WIDTH       1
#define CLKC_ROOT_CLK_EN1_STATUS__GMAC__MASK        0x20000000
#define CLKC_ROOT_CLK_EN1_STATUS__GMAC__INV_MASK    0xDFFFFFFF
#define CLKC_ROOT_CLK_EN1_STATUS__GMAC__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN0_SET     0x18620244

/* CLKC_LEAF_CLK_EN0_SET.pwm_io_clken - pwm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pwm_io_clk clock */
#define CLKC_LEAF_CLK_EN0_SET__PWM_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN0_SET__PWM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_SET__PWM_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN0_SET__PWM_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN0_SET__PWM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_SET.pwm_xin_clken - pwm_xin_clk clock enable */
/* Root clock XIN_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pwm_xin_clk clock */
#define CLKC_LEAF_CLK_EN0_SET__PWM_XIN_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN0_SET__PWM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_SET__PWM_XIN_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN0_SET__PWM_XIN_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN0_SET__PWM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_SET.pwm_xinw_clken - pwm_xinw_clk clock enable */
/* Root clock XINW_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pwm_xinw_clk clock */
#define CLKC_LEAF_CLK_EN0_SET__PWM_XINW_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN0_SET__PWM_XINW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_SET__PWM_XINW_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN0_SET__PWM_XINW_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN0_SET__PWM_XINW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_SET.thcgum_sys_clken - thcgum_sys_clk clock enable */
/* Root clock SYS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thcgum_sys_clk clock */
#define CLKC_LEAF_CLK_EN0_SET__THCGUM_SYS_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN0_SET__THCGUM_SYS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_SET__THCGUM_SYS_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN0_SET__THCGUM_SYS_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN0_SET__THCGUM_SYS_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN0_CLR     0x18620248

/* CLKC_LEAF_CLK_EN0_CLR.pwm_io_clken - pwm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pwm_io_clk clock */
#define CLKC_LEAF_CLK_EN0_CLR__PWM_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN0_CLR__PWM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_CLR__PWM_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN0_CLR__PWM_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN0_CLR__PWM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_CLR.pwm_xin_clken - pwm_xin_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pwm_xin_clk clock */
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XIN_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XIN_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XIN_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_CLR.pwm_xinw_clken - pwm_xinw_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pwm_xinw_clk clock */
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XINW_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XINW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XINW_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XINW_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN0_CLR__PWM_XINW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_CLR.thcgum_sys_clken - thcgum_sys_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thcgum_sys_clk clock */
#define CLKC_LEAF_CLK_EN0_CLR__THCGUM_SYS_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN0_CLR__THCGUM_SYS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_CLR__THCGUM_SYS_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN0_CLR__THCGUM_SYS_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN0_CLR__THCGUM_SYS_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN0_STATUS  0x1862024C

/* CLKC_LEAF_CLK_EN0_STATUS.pwm_io_clken - pwm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of pwm_io_clk leaf clock. */
/* 0 : pwm_io_clk clock disabled */
/* 1 : pwm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_STATUS.pwm_xin_clken - pwm_xin_clk clock enable status */
/* If root clock XIN_CLK is disabled this status bit does not reflect the actual status of pwm_xin_clk leaf clock. */
/* 0 : pwm_xin_clk clock disabled */
/* 1 : pwm_xin_clk clock enabled */
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XIN_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XIN_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XIN_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_STATUS.pwm_xinw_clken - pwm_xinw_clk clock enable status */
/* If root clock XINW_CLK is disabled this status bit does not reflect the actual status of pwm_xinw_clk leaf clock. */
/* 0 : pwm_xinw_clk clock disabled */
/* 1 : pwm_xinw_clk clock enabled */
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XINW_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XINW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XINW_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XINW_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN0_STATUS__PWM_XINW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN0_STATUS.thcgum_sys_clken - thcgum_sys_clk clock enable status */
/* If root clock SYS_CLK is disabled this status bit does not reflect the actual status of thcgum_sys_clk leaf clock. */
/* 0 : thcgum_sys_clk clock disabled */
/* 1 : thcgum_sys_clk clock enabled */
#define CLKC_LEAF_CLK_EN0_STATUS__THCGUM_SYS_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN0_STATUS__THCGUM_SYS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN0_STATUS__THCGUM_SYS_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN0_STATUS__THCGUM_SYS_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN0_STATUS__THCGUM_SYS_CLKEN__HW_DEFAULT  0x1

/* text */
#define CLKC_TEST_CLK_SEL         0x186202B0

/* CLKC_TEST_CLK_SEL.sel - test clock selector for clock visibility */
/* 0 : Disabled Output 1'b0  */
/* 1 : SYS0_BTPLL/Q3 Direct output from PLL  */
/* 2 : SYS1_USBPLL/Q3 Direct output from PLL  */
/* 3 : SYS2_ETHPLL/Q3 Direct output from PLL  */
/* 4 : SYS3_SSCPLL/Q3 Direct output from PLL  */
/* 5 : USBPHY_CLK Before clock-gate  */
/* 6 : RGMII_CLK Before clock-gate  */
/* 7 : BTSS_CLK Before clock-gate  */
/* 8 : CPU_CLK Feedback from CPU (CPU/16) */
/* 9 : MEM_CLK Feedback from MEM (MEM/4)  */
/* 10 : SDPHY01 Before clock-gate  */
/* 11 : SDPHY23 Before clock-gate  */
/* 12 : SDPHY45 Before clock-gate  */
/* 13 : SDPHY67 Before clock-gate  */
/* 14 : CAN Before clock-gate  */
/* 15 : DEINT Before clock-gate  */
/* 16 : NAND Before clock-gate  */
/* 17 : DISP0/VPP0 Before clock-gate  */
/* 18 : DISP1/VPP1 Before clock-gate  */
/* 19 : GPU Before clock-gate  */
/* 20 : GNSS Before clock-gate  */
/* 21 : SYS_CLK Before clock-gate  */
/* 22 : IO_CLK Before clock gate  */
/* 23 : NOCD_CLK Before clock gate  */
/* 24 : NOCR_CLK Before clock gate  */
/* 25 : pre_sel2_clk  */
/* 26 : pre_sel3_clk  */
/* 27 : pre_sel4_clk  */
/* 28 : pre_sel5_clk  */
/* 29 : pre_sel6_clk  */
/* 30 : pre_sel7_clk  */
/* 31 : I2S_CLK Before clock gate  */
#define CLKC_TEST_CLK_SEL__SEL__SHIFT       0
#define CLKC_TEST_CLK_SEL__SEL__WIDTH       5
#define CLKC_TEST_CLK_SEL__SEL__MASK        0x0000001F
#define CLKC_TEST_CLK_SEL__SEL__INV_MASK    0xFFFFFFE0
#define CLKC_TEST_CLK_SEL__SEL__HW_DEFAULT  0x0

/* CLKC_TEST_CLK_SEL.div - control test clock divider */
/* 0 : selected test clock is not divided by 16 */
/* 1 : selected test clock is divided by 16 */
#define CLKC_TEST_CLK_SEL__DIV__SHIFT       5
#define CLKC_TEST_CLK_SEL__DIV__WIDTH       1
#define CLKC_TEST_CLK_SEL__DIV__MASK        0x00000020
#define CLKC_TEST_CLK_SEL__DIV__INV_MASK    0xFFFFFFDF
#define CLKC_TEST_CLK_SEL__DIV__HW_DEFAULT  0x0

/* select gnssm_xin_ref_clk source */
#define CLKC_TRG_REFCLK_SEL       0x186202B4

/* CLKC_TRG_REFCLK_SEL.trg_refclk_sel - test clock selector for clock visibility */
/* 0 : gnss_xin_clk = xin */
/* 1 : gnss_xin_clk = trg_ref_clk_in */
#define CLKC_TRG_REFCLK_SEL__TRG_REFCLK_SEL__SHIFT       0
#define CLKC_TRG_REFCLK_SEL__TRG_REFCLK_SEL__WIDTH       1
#define CLKC_TRG_REFCLK_SEL__TRG_REFCLK_SEL__MASK        0x00000001
#define CLKC_TRG_REFCLK_SEL__TRG_REFCLK_SEL__INV_MASK    0xFFFFFFFE
#define CLKC_TRG_REFCLK_SEL__TRG_REFCLK_SEL__HW_DEFAULT  0x0

/* rw A7 dual core flag. Verify if the dual core wakeup is valid by magic number. Should only be accessed by A7. */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG0         0x186202B8

/* rw A7 dual core jump address. When dual core wakeup valid, jump to this address. Should only be accessed by A7. */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG1         0x186202BC

/* rw M3 flag. Verify if A7 wakeup of M3 is valid by magic number. */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG2         0x186202C0

/* rw M3 jump address. When A7 wakeup of M3 is valid, jump to this address. */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG3         0x186202C4

/* rw SW_REG - spare */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG4         0x186202C8

/* rw SW_REG - spare */
/* <b>Documentation for this register is still missing!</b> */
#define CLKC_CLKC_SW_REG5         0x186202CC

/* rw noc_clk_idlereq_set */
/* Writing 1 to a bit enables the associated unit clock disconnect request clk_xxx_idle_req.
 Writing 0 to a bit has no effect.
Reading this registers returns the NOC_CLK_IDLEREQ status.
 */
#define CLKC_NOC_CLK_IDLEREQ_SET  0x186202D0

/* CLKC_NOC_CLK_IDLEREQ_SET.GPU - set GPU idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_gpu_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__GPU__SHIFT       0
#define CLKC_NOC_CLK_IDLEREQ_SET__GPU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__GPU__MASK        0x00000001
#define CLKC_NOC_CLK_IDLEREQ_SET__GPU__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_IDLEREQ_SET__GPU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.JPENC - set JPENC idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_jpenc_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__JPENC__SHIFT       1
#define CLKC_NOC_CLK_IDLEREQ_SET__JPENC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__JPENC__MASK        0x00000002
#define CLKC_NOC_CLK_IDLEREQ_SET__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_IDLEREQ_SET__JPENC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.KAS - set KAS idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_kas_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__KAS__SHIFT       2
#define CLKC_NOC_CLK_IDLEREQ_SET__KAS__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__KAS__MASK        0x00000004
#define CLKC_NOC_CLK_IDLEREQ_SET__KAS__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_IDLEREQ_SET__KAS__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.VDEC - set VDEC idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_vdec_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__VDEC__SHIFT       3
#define CLKC_NOC_CLK_IDLEREQ_SET__VDEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__VDEC__MASK        0x00000008
#define CLKC_NOC_CLK_IDLEREQ_SET__VDEC__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_IDLEREQ_SET__VDEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.AFE_CVD - set AFE_CVD idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_afe_cvd_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__AFE_CVD__SHIFT       4
#define CLKC_NOC_CLK_IDLEREQ_SET__AFE_CVD__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__AFE_CVD__MASK        0x00000010
#define CLKC_NOC_CLK_IDLEREQ_SET__AFE_CVD__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_IDLEREQ_SET__AFE_CVD__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.IO - set IO idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_io_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__IO__SHIFT       5
#define CLKC_NOC_CLK_IDLEREQ_SET__IO__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__IO__MASK        0x00000020
#define CLKC_NOC_CLK_IDLEREQ_SET__IO__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_IDLEREQ_SET__IO__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.CSSI - set CSSI idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_cssi_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__CSSI__SHIFT       6
#define CLKC_NOC_CLK_IDLEREQ_SET__CSSI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__CSSI__MASK        0x00000040
#define CLKC_NOC_CLK_IDLEREQ_SET__CSSI__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_IDLEREQ_SET__CSSI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.CAN - set CAN idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_can1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__CAN__SHIFT       7
#define CLKC_NOC_CLK_IDLEREQ_SET__CAN__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__CAN__MASK        0x00000080
#define CLKC_NOC_CLK_IDLEREQ_SET__CAN__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_IDLEREQ_SET__CAN__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.CC_PUB - set CC_PUB idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_cc_pub_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_PUB__SHIFT       8
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_PUB__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_PUB__MASK        0x00000100
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_PUB__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_PUB__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.CC_SEC - set CC_SEC idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_cc_sec_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_SEC__SHIFT       9
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_SEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_SEC__MASK        0x00000200
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_SEC__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_IDLEREQ_SET__CC_SEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.ETH - set ETH idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_eth_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__ETH__SHIFT       10
#define CLKC_NOC_CLK_IDLEREQ_SET__ETH__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__ETH__MASK        0x00000400
#define CLKC_NOC_CLK_IDLEREQ_SET__ETH__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_IDLEREQ_SET__ETH__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.SDR - set SDR idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_sdr_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__SDR__SHIFT       11
#define CLKC_NOC_CLK_IDLEREQ_SET__SDR__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__SDR__MASK        0x00000800
#define CLKC_NOC_CLK_IDLEREQ_SET__SDR__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_IDLEREQ_SET__SDR__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.G2D - set G2D idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_g2d_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__G2D__SHIFT       12
#define CLKC_NOC_CLK_IDLEREQ_SET__G2D__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__G2D__MASK        0x00001000
#define CLKC_NOC_CLK_IDLEREQ_SET__G2D__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__G2D__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.MEDIAM_PCI - set MEDIAM_PCI idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_mediam_pci_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__MEDIAM_PCI__SHIFT       13
#define CLKC_NOC_CLK_IDLEREQ_SET__MEDIAM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__MEDIAM_PCI__MASK        0x00002000
#define CLKC_NOC_CLK_IDLEREQ_SET__MEDIAM_PCI__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__MEDIAM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.NAND - set NAND idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_nand_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__NAND__SHIFT       14
#define CLKC_NOC_CLK_IDLEREQ_SET__NAND__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__NAND__MASK        0x00004000
#define CLKC_NOC_CLK_IDLEREQ_SET__NAND__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__NAND__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.USB0 - set USB0 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_usb0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__USB0__SHIFT       15
#define CLKC_NOC_CLK_IDLEREQ_SET__USB0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__USB0__MASK        0x00008000
#define CLKC_NOC_CLK_IDLEREQ_SET__USB0__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_IDLEREQ_SET__USB0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.USB1 - set USB1 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_usb1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__USB1__SHIFT       16
#define CLKC_NOC_CLK_IDLEREQ_SET__USB1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__USB1__MASK        0x00010000
#define CLKC_NOC_CLK_IDLEREQ_SET__USB1__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__USB1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.DCU - set DCU idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_dcu_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__DCU__SHIFT       17
#define CLKC_NOC_CLK_IDLEREQ_SET__DCU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__DCU__MASK        0x00020000
#define CLKC_NOC_CLK_IDLEREQ_SET__DCU__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__DCU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.LCD0 - set LCD0 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_lcd0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD0__SHIFT       18
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD0__MASK        0x00040000
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD0__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.LCD1 - set LCD1 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_lcd1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD1__SHIFT       19
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD1__MASK        0x00080000
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD1__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__LCD1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.VDIFM_PCI - set VDIFM_PCI idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_vdifm_pci_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__VDIFM_PCI__SHIFT       20
#define CLKC_NOC_CLK_IDLEREQ_SET__VDIFM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__VDIFM_PCI__MASK        0x00100000
#define CLKC_NOC_CLK_IDLEREQ_SET__VDIFM_PCI__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__VDIFM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.VIP1 - set VIP1 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_vip1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__VIP1__SHIFT       21
#define CLKC_NOC_CLK_IDLEREQ_SET__VIP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__VIP1__MASK        0x00200000
#define CLKC_NOC_CLK_IDLEREQ_SET__VIP1__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__VIP1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.VPP0 - set VPP0 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_vpp0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP0__SHIFT       22
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP0__MASK        0x00400000
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP0__INV_MASK    0xFFBFFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_SET.VPP1 - set VPP1 idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_vpp1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP1__SHIFT       23
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP1__MASK        0x00800000
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP1__INV_MASK    0xFF7FFFFF
#define CLKC_NOC_CLK_IDLEREQ_SET__VPP1__HW_DEFAULT  0x0

/* rw noc_clk_idlereq_clear */
/* Writing 1 to a field disables the associated unit clock disconnect request clk_xxx_idle_req.
 Writing 0 to a bit has no effect.
Reading this registers returns the NOC_CLK_IDLEREQ status.
 */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR 0x186202D4

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.GPU - clear GPU idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_gpu_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__GPU__SHIFT       0
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__GPU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__GPU__MASK        0x00000001
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__GPU__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__GPU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.JPENC - clear JPENC idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_jpenc_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__JPENC__SHIFT       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__JPENC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__JPENC__MASK        0x00000002
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__JPENC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.KAS - clear KAS idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_kas_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__KAS__SHIFT       2
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__KAS__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__KAS__MASK        0x00000004
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__KAS__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__KAS__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.VDEC - clear VDEC idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_vdec_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDEC__SHIFT       3
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDEC__MASK        0x00000008
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDEC__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.AFE_CVD - clear AFE_CVD idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_afe_cvd_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__AFE_CVD__SHIFT       4
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__AFE_CVD__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__AFE_CVD__MASK        0x00000010
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__AFE_CVD__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__AFE_CVD__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.IO - clear IO idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_io_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__IO__SHIFT       5
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__IO__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__IO__MASK        0x00000020
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__IO__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__IO__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.CSSI - clear CSSI idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_cssi_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CSSI__SHIFT       6
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CSSI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CSSI__MASK        0x00000040
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CSSI__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CSSI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.CAN - clear CAN idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_can1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CAN__SHIFT       7
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CAN__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CAN__MASK        0x00000080
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CAN__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CAN__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.CC_PUB - clear CC_PUB idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_cc_pub_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_PUB__SHIFT       8
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_PUB__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_PUB__MASK        0x00000100
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_PUB__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_PUB__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.CC_SEC - clear CC_SEC idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_cc_sec_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_SEC__SHIFT       9
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_SEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_SEC__MASK        0x00000200
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_SEC__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__CC_SEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.ETH - clear ETH idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_eth_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__ETH__SHIFT       10
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__ETH__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__ETH__MASK        0x00000400
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__ETH__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__ETH__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.SDR - clear SDR idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_sdr_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__SDR__SHIFT       11
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__SDR__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__SDR__MASK        0x00000800
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__SDR__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__SDR__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.G2D - clear G2D idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_g2d_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__G2D__SHIFT       12
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__G2D__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__G2D__MASK        0x00001000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__G2D__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__G2D__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.MEDIAM_PCI - clear MEDIAM_PCI idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_mediam_pci_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__MEDIAM_PCI__SHIFT       13
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__MEDIAM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__MEDIAM_PCI__MASK        0x00002000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__MEDIAM_PCI__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__MEDIAM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.NAND - clear NAND idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_nand_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__NAND__SHIFT       14
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__NAND__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__NAND__MASK        0x00004000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__NAND__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__NAND__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.USB0 - clear USB0 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_usb0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB0__SHIFT       15
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB0__MASK        0x00008000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB0__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.USB1 - clear USB1 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_usb1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB1__SHIFT       16
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB1__MASK        0x00010000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB1__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__USB1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.DCU - clear DCU idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_dcu_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__DCU__SHIFT       17
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__DCU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__DCU__MASK        0x00020000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__DCU__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__DCU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.LCD0 - clear LCD0 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_lcd0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD0__SHIFT       18
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD0__MASK        0x00040000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD0__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.LCD1 - clear LCD1 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_lcd1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD1__SHIFT       19
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD1__MASK        0x00080000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD1__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__LCD1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.VDIFM_PCI - clear VDIFM_PCI idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_vdifm_pci_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDIFM_PCI__SHIFT       20
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDIFM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDIFM_PCI__MASK        0x00100000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDIFM_PCI__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VDIFM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.VIP1 - clear VIP1 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_vip1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VIP1__SHIFT       21
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VIP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VIP1__MASK        0x00200000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VIP1__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VIP1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.VPP0 - clear VPP0 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_vpp0_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP0__SHIFT       22
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP0__MASK        0x00400000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP0__INV_MASK    0xFFBFFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_CLEAR.VPP1 - clear VPP1 idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_vpp1_IdleReq */
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP1__SHIFT       23
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP1__MASK        0x00800000
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP1__INV_MASK    0xFF7FFFFF
#define CLKC_NOC_CLK_IDLEREQ_CLEAR__VPP1__HW_DEFAULT  0x0

/* ro noc_clk_idlereq status */
#define CLKC_NOC_CLK_IDLEREQ_STATUS 0x186202D8

/* CLKC_NOC_CLK_IDLEREQ_STATUS.GPU - GPU idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__GPU__SHIFT       0
#define CLKC_NOC_CLK_IDLEREQ_STATUS__GPU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__GPU__MASK        0x00000001
#define CLKC_NOC_CLK_IDLEREQ_STATUS__GPU__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_IDLEREQ_STATUS__GPU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.JPENC - JPENC idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__JPENC__SHIFT       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__JPENC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__JPENC__MASK        0x00000002
#define CLKC_NOC_CLK_IDLEREQ_STATUS__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_IDLEREQ_STATUS__JPENC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.KAS - KAS idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__KAS__SHIFT       2
#define CLKC_NOC_CLK_IDLEREQ_STATUS__KAS__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__KAS__MASK        0x00000004
#define CLKC_NOC_CLK_IDLEREQ_STATUS__KAS__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_IDLEREQ_STATUS__KAS__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.VDEC - VDEC idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDEC__SHIFT       3
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDEC__MASK        0x00000008
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDEC__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.AFE_CVD - AFE_CVD idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__AFE_CVD__SHIFT       4
#define CLKC_NOC_CLK_IDLEREQ_STATUS__AFE_CVD__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__AFE_CVD__MASK        0x00000010
#define CLKC_NOC_CLK_IDLEREQ_STATUS__AFE_CVD__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__AFE_CVD__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.IO - IO idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__IO__SHIFT       5
#define CLKC_NOC_CLK_IDLEREQ_STATUS__IO__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__IO__MASK        0x00000020
#define CLKC_NOC_CLK_IDLEREQ_STATUS__IO__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__IO__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.CSSI - CSSI idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CSSI__SHIFT       6
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CSSI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CSSI__MASK        0x00000040
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CSSI__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CSSI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.CAN - CAN idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CAN__SHIFT       7
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CAN__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CAN__MASK        0x00000080
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CAN__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CAN__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.CC_PUB - CC_PUB idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_PUB__SHIFT       8
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_PUB__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_PUB__MASK        0x00000100
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_PUB__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_PUB__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.CC_SEC - CC_SEC idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_SEC__SHIFT       9
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_SEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_SEC__MASK        0x00000200
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_SEC__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__CC_SEC__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.ETH - ETH idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__ETH__SHIFT       10
#define CLKC_NOC_CLK_IDLEREQ_STATUS__ETH__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__ETH__MASK        0x00000400
#define CLKC_NOC_CLK_IDLEREQ_STATUS__ETH__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__ETH__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.SDR - SDR idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__SDR__SHIFT       11
#define CLKC_NOC_CLK_IDLEREQ_STATUS__SDR__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__SDR__MASK        0x00000800
#define CLKC_NOC_CLK_IDLEREQ_STATUS__SDR__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__SDR__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.G2D - G2D idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__G2D__SHIFT       12
#define CLKC_NOC_CLK_IDLEREQ_STATUS__G2D__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__G2D__MASK        0x00001000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__G2D__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__G2D__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.MEDIAM_PCI - MEDIAM_PCI idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__MEDIAM_PCI__SHIFT       13
#define CLKC_NOC_CLK_IDLEREQ_STATUS__MEDIAM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__MEDIAM_PCI__MASK        0x00002000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__MEDIAM_PCI__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__MEDIAM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.NAND - NAND idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__NAND__SHIFT       14
#define CLKC_NOC_CLK_IDLEREQ_STATUS__NAND__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__NAND__MASK        0x00004000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__NAND__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__NAND__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.USB0 - USB0 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB0__SHIFT       15
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB0__MASK        0x00008000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB0__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.USB1 - USB1 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB1__SHIFT       16
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB1__MASK        0x00010000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB1__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__USB1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.DCU - DCU idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__DCU__SHIFT       17
#define CLKC_NOC_CLK_IDLEREQ_STATUS__DCU__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__DCU__MASK        0x00020000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__DCU__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__DCU__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.LCD0 - LCD0 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD0__SHIFT       18
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD0__MASK        0x00040000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD0__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.LCD1 - LCD1 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD1__SHIFT       19
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD1__MASK        0x00080000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD1__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__LCD1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.VDIFM_PCI - VDIFM_PCI idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDIFM_PCI__SHIFT       20
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDIFM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDIFM_PCI__MASK        0x00100000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDIFM_PCI__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VDIFM_PCI__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.VIP1 - VIP1 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VIP1__SHIFT       21
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VIP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VIP1__MASK        0x00200000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VIP1__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VIP1__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.VPP0 - VPP0 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP0__SHIFT       22
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP0__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP0__MASK        0x00400000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP0__INV_MASK    0xFFBFFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP0__HW_DEFAULT  0x0

/* CLKC_NOC_CLK_IDLEREQ_STATUS.VPP1 - VPP1 idle request status.
 */
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP1__SHIFT       23
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP1__MASK        0x00800000
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP1__INV_MASK    0xFF7FFFFF
#define CLKC_NOC_CLK_IDLEREQ_STATUS__VPP1__HW_DEFAULT  0x0

/* rw plug_clk_idlereq set */
/* Writing 1 to a bit enables the associated unit clock disconnect request plug_clk_xxx_idle_req.
 Writing 0 to a bit has no effect.
Reading this registers returns the PLUG_CLK_IDLEREQ status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_SET 0x186202DC

/* CLKC_PLUG_CLK_IDLEREQ_SET.BTM_AUDMSCM_D_BTM - set BTM_AUDMSCM_D_BTM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_btm_audmscm_d_btm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__BTM_AUDMSCM_D_BTM__SHIFT       0
#define CLKC_PLUG_CLK_IDLEREQ_SET__BTM_AUDMSCM_D_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__BTM_AUDMSCM_D_BTM__MASK        0x00000001
#define CLKC_PLUG_CLK_IDLEREQ_SET__BTM_AUDMSCM_D_BTM__INV_MASK    0xFFFFFFFE
#define CLKC_PLUG_CLK_IDLEREQ_SET__BTM_AUDMSCM_D_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.AUDMSCM_BTM_R_BTM - set AUDMSCM_BTM_R_BTM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_audmscm_btm_r_btm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__AUDMSCM_BTM_R_BTM__SHIFT       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__AUDMSCM_BTM_R_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__AUDMSCM_BTM_R_BTM__MASK        0x00000002
#define CLKC_PLUG_CLK_IDLEREQ_SET__AUDMSCM_BTM_R_BTM__INV_MASK    0xFFFFFFFD
#define CLKC_PLUG_CLK_IDLEREQ_SET__AUDMSCM_BTM_R_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.CPUM_DDRM1_D_DDRM - set CPUM_DDRM1_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_cpum_ddrm1_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPUM_DDRM1_D_DDRM__SHIFT       2
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPUM_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPUM_DDRM1_D_DDRM__MASK        0x00000004
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPUM_DDRM1_D_DDRM__INV_MASK    0xFFFFFFFB
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPUM_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.CPU_DDRM1_D_DDRM - set CPU_DDRM1_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_cpu_ddrm1_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPU_DDRM1_D_DDRM__SHIFT       3
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPU_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPU_DDRM1_D_DDRM__MASK        0x00000008
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPU_DDRM1_D_DDRM__INV_MASK    0xFFFFFFF7
#define CLKC_PLUG_CLK_IDLEREQ_SET__CPU_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.GPUM_DDRM3_D_DDRM - set GPUM_DDRM3_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_gpum_ddrm3_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM3_D_DDRM__SHIFT       4
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM3_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM3_D_DDRM__MASK        0x00000010
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM3_D_DDRM__INV_MASK    0xFFFFFFEF
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM3_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.GPUM_DDRM2_D_DDRM - set GPUM_DDRM2_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_gpum_ddrm2_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM2_D_DDRM__SHIFT       5
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM2_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM2_D_DDRM__MASK        0x00000020
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM2_D_DDRM__INV_MASK    0xFFFFFFDF
#define CLKC_PLUG_CLK_IDLEREQ_SET__GPUM_DDRM2_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_SET.MEDIAM_DDRM4_D_DDRM - set MEDIAM_DDRM4_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : set pwr_mediam_ddrm4_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_SET__MEDIAM_DDRM4_D_DDRM__SHIFT       6
#define CLKC_PLUG_CLK_IDLEREQ_SET__MEDIAM_DDRM4_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_SET__MEDIAM_DDRM4_D_DDRM__MASK        0x00000040
#define CLKC_PLUG_CLK_IDLEREQ_SET__MEDIAM_DDRM4_D_DDRM__INV_MASK    0xFFFFFFBF
#define CLKC_PLUG_CLK_IDLEREQ_SET__MEDIAM_DDRM4_D_DDRM__HW_DEFAULT  0x0

/* rw plug_clk_idlereq clear */
/* Writing 1 to a field disables the associated unit clock disconnect requestplug_clk_xxx_idle_req.
 Writing 0 to a bit has no effect.
Reading this registers returns the PLUG_CLK_IDLEREQ status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR 0x186202E0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.BTM_AUDMSCM_D_BTM - clear BTM_AUDMSCM_D_BTM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_btm_audmscm_d_btm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__BTM_AUDMSCM_D_BTM__SHIFT       0
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__BTM_AUDMSCM_D_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__BTM_AUDMSCM_D_BTM__MASK        0x00000001
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__BTM_AUDMSCM_D_BTM__INV_MASK    0xFFFFFFFE
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__BTM_AUDMSCM_D_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.AUDMSCM_BTM_R_BTM - clear AUDMSCM_BTM_R_BTM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_audmscm_btm_r_btm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__AUDMSCM_BTM_R_BTM__SHIFT       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__AUDMSCM_BTM_R_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__AUDMSCM_BTM_R_BTM__MASK        0x00000002
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__AUDMSCM_BTM_R_BTM__INV_MASK    0xFFFFFFFD
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__AUDMSCM_BTM_R_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.CPUM_DDRM1_D_DDRM - clear CPUM_DDRM1_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_cpum_ddrm1_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPUM_DDRM1_D_DDRM__SHIFT       2
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPUM_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPUM_DDRM1_D_DDRM__MASK        0x00000004
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPUM_DDRM1_D_DDRM__INV_MASK    0xFFFFFFFB
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPUM_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.CPU_DDRM1_D_DDRM - clear CPU_DDRM1_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_cpu_ddrm1_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPU_DDRM1_D_DDRM__SHIFT       3
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPU_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPU_DDRM1_D_DDRM__MASK        0x00000008
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPU_DDRM1_D_DDRM__INV_MASK    0xFFFFFFF7
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__CPU_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.GPUM_DDRM3_D_DDRM - clear GPUM_DDRM3_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_gpum_ddrm3_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM3_D_DDRM__SHIFT       4
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM3_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM3_D_DDRM__MASK        0x00000010
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM3_D_DDRM__INV_MASK    0xFFFFFFEF
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM3_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.GPUM_DDRM2_D_DDRM - clear GPUM_DDRM2_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_gpum_ddrm2_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM2_D_DDRM__SHIFT       5
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM2_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM2_D_DDRM__MASK        0x00000020
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM2_D_DDRM__INV_MASK    0xFFFFFFDF
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__GPUM_DDRM2_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_CLEAR.MEDIAM_DDRM4_D_DDRM - clear MEDIAM_DDRM4_D_DDRM idle request.
 */
/* 0 : no effect */
/* 1 : clear pwr_mediam_ddrm4_d_ddrm_IdleReq */
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__MEDIAM_DDRM4_D_DDRM__SHIFT       6
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__MEDIAM_DDRM4_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__MEDIAM_DDRM4_D_DDRM__MASK        0x00000040
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__MEDIAM_DDRM4_D_DDRM__INV_MASK    0xFFFFFFBF
#define CLKC_PLUG_CLK_IDLEREQ_CLEAR__MEDIAM_DDRM4_D_DDRM__HW_DEFAULT  0x0

/* ro plug_clk_idlereq status */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS 0x186202E4

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.BTM_AUDMSCM_D_BTM - BTM_AUDMSCM_D_BTM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__BTM_AUDMSCM_D_BTM__SHIFT       0
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__BTM_AUDMSCM_D_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__BTM_AUDMSCM_D_BTM__MASK        0x00000001
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__BTM_AUDMSCM_D_BTM__INV_MASK    0xFFFFFFFE
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__BTM_AUDMSCM_D_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.AUDMSCM_BTM_R_BTM - AUDMSCM_BTM_R_BTM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__AUDMSCM_BTM_R_BTM__SHIFT       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__AUDMSCM_BTM_R_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__AUDMSCM_BTM_R_BTM__MASK        0x00000002
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__AUDMSCM_BTM_R_BTM__INV_MASK    0xFFFFFFFD
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__AUDMSCM_BTM_R_BTM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.CPUM_DDRM1_D_DDRM - CPUM_DDRM1_D_DDRM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPUM_DDRM1_D_DDRM__SHIFT       2
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPUM_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPUM_DDRM1_D_DDRM__MASK        0x00000004
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPUM_DDRM1_D_DDRM__INV_MASK    0xFFFFFFFB
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPUM_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.CPU_DDRM1_D_DDRM - CPU_DDRM1_D_DDRM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPU_DDRM1_D_DDRM__SHIFT       3
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPU_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPU_DDRM1_D_DDRM__MASK        0x00000008
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPU_DDRM1_D_DDRM__INV_MASK    0xFFFFFFF7
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__CPU_DDRM1_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.GPUM_DDRM3_D_DDRM - GPUM_DDRM3_D_DDRM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM3_D_DDRM__SHIFT       4
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM3_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM3_D_DDRM__MASK        0x00000010
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM3_D_DDRM__INV_MASK    0xFFFFFFEF
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM3_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.GPUM_DDRM2_D_DDRM - GPUM_DDRM2_D_DDRM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM2_D_DDRM__SHIFT       5
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM2_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM2_D_DDRM__MASK        0x00000020
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM2_D_DDRM__INV_MASK    0xFFFFFFDF
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__GPUM_DDRM2_D_DDRM__HW_DEFAULT  0x0

/* CLKC_PLUG_CLK_IDLEREQ_STATUS.MEDIAM_DDRM4_D_DDRM - MEDIAM_DDRM4_D_DDRM idle request status.
 */
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__MEDIAM_DDRM4_D_DDRM__SHIFT       6
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__MEDIAM_DDRM4_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__MEDIAM_DDRM4_D_DDRM__MASK        0x00000040
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__MEDIAM_DDRM4_D_DDRM__INV_MASK    0xFFFFFFBF
#define CLKC_PLUG_CLK_IDLEREQ_STATUS__MEDIAM_DDRM4_D_DDRM__HW_DEFAULT  0x0

/* rw Set socket disconnect  */
/* Writing 1 to a bit enables the associated unit socket disconnect request.
 Writing 0 to a bit has no effect.
Reading this registers returns the NOC_CLK_SLVRDY status.
Writing 1 to a bit clears the associated unit socket disconnect request.
 Writing 0 to a bit has no effect.
Reading this registers returns the NOC_CLK_SLVRDY status.
 */
#define CLKC_NOC_CLK_SLVRDY_SET   0x186202E8

/* CLKC_NOC_CLK_SLVRDY_SET.AUDIO_IF - Set AUDIO_IF socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_audio_if_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__AUDIO_IF__SHIFT       0
#define CLKC_NOC_CLK_SLVRDY_SET__AUDIO_IF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__AUDIO_IF__MASK        0x00000001
#define CLKC_NOC_CLK_SLVRDY_SET__AUDIO_IF__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_SLVRDY_SET__AUDIO_IF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.DMAC2 - Set DMAC2 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_dmac2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC2__SHIFT       1
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC2__MASK        0x00000002
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC2__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.DMAC3 - Set DMAC3 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_dmac3_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC3__SHIFT       2
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC3__MASK        0x00000004
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC3__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.SPDIF - Set SPDIF socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_spdif_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__SPDIF__SHIFT       3
#define CLKC_NOC_CLK_SLVRDY_SET__SPDIF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__SPDIF__MASK        0x00000008
#define CLKC_NOC_CLK_SLVRDY_SET__SPDIF__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_SLVRDY_SET__SPDIF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.USP0 - Set USP0 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_usp0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__USP0__SHIFT       4
#define CLKC_NOC_CLK_SLVRDY_SET__USP0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__USP0__MASK        0x00000010
#define CLKC_NOC_CLK_SLVRDY_SET__USP0__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_SLVRDY_SET__USP0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.USP1 - Set USP1 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_usp1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__USP1__SHIFT       5
#define CLKC_NOC_CLK_SLVRDY_SET__USP1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__USP1__MASK        0x00000020
#define CLKC_NOC_CLK_SLVRDY_SET__USP1__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_SLVRDY_SET__USP1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.USP2 - Set USP2 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_kas_reg_hub_usp2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__USP2__SHIFT       6
#define CLKC_NOC_CLK_SLVRDY_SET__USP2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__USP2__MASK        0x00000040
#define CLKC_NOC_CLK_SLVRDY_SET__USP2__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_SLVRDY_SET__USP2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.DVM - Set DVM socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_xin_reg_hub_dvm_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__DVM__SHIFT       7
#define CLKC_NOC_CLK_SLVRDY_SET__DVM__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__DVM__MASK        0x00000080
#define CLKC_NOC_CLK_SLVRDY_SET__DVM__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_SLVRDY_SET__DVM__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.LVDS - Set LVDS socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set audmscm_xin_reg_hub_lvds_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__LVDS__SHIFT       8
#define CLKC_NOC_CLK_SLVRDY_SET__LVDS__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__LVDS__MASK        0x00000100
#define CLKC_NOC_CLK_SLVRDY_SET__LVDS__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_SLVRDY_SET__LVDS__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.A7_SPRAM_1 - Set A7_SPRAM_1 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set a7_spram_1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_1__SHIFT       9
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_1__MASK        0x00000200
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_1__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.A7_SPRAM_2 - Set A7_SPRAM_2 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set a7_spram_2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_2__SHIFT       10
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_2__MASK        0x00000400
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_2__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_SLVRDY_SET__A7_SPRAM_2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.DMAC0 - Set DMAC0 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_dmac0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC0__SHIFT       11
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC0__MASK        0x00000800
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC0__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_SLVRDY_SET__DMAC0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.SPI1 - Set SPI1 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_spi1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__SPI1__SHIFT       12
#define CLKC_NOC_CLK_SLVRDY_SET__SPI1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__SPI1__MASK        0x00001000
#define CLKC_NOC_CLK_SLVRDY_SET__SPI1__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_SLVRDY_SET__SPI1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART0 - Set UART0 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART0__SHIFT       13
#define CLKC_NOC_CLK_SLVRDY_SET__UART0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART0__MASK        0x00002000
#define CLKC_NOC_CLK_SLVRDY_SET__UART0__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART1 - Set UART1 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART1__SHIFT       14
#define CLKC_NOC_CLK_SLVRDY_SET__UART1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART1__MASK        0x00004000
#define CLKC_NOC_CLK_SLVRDY_SET__UART1__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART2 - Set UART2 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART2__SHIFT       15
#define CLKC_NOC_CLK_SLVRDY_SET__UART2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART2__MASK        0x00008000
#define CLKC_NOC_CLK_SLVRDY_SET__UART2__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART3 - Set UART3 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart3_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART3__SHIFT       16
#define CLKC_NOC_CLK_SLVRDY_SET__UART3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART3__MASK        0x00010000
#define CLKC_NOC_CLK_SLVRDY_SET__UART3__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART4 - Set UART4 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart4_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART4__SHIFT       17
#define CLKC_NOC_CLK_SLVRDY_SET__UART4__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART4__MASK        0x00020000
#define CLKC_NOC_CLK_SLVRDY_SET__UART4__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART4__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.UART5 - Set UART5 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set gnssm_io_reg_hub_uart5_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__UART5__SHIFT       18
#define CLKC_NOC_CLK_SLVRDY_SET__UART5__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__UART5__MASK        0x00040000
#define CLKC_NOC_CLK_SLVRDY_SET__UART5__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_SLVRDY_SET__UART5__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.GPIO0 - Set GPIO0 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set mediam_io_reg_hub_gpio0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__GPIO0__SHIFT       19
#define CLKC_NOC_CLK_SLVRDY_SET__GPIO0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__GPIO0__MASK        0x00080000
#define CLKC_NOC_CLK_SLVRDY_SET__GPIO0__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_SLVRDY_SET__GPIO0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.I2C1 - Set I2C1 socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set mediam_io_reg_hub_i2c1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__I2C1__SHIFT       20
#define CLKC_NOC_CLK_SLVRDY_SET__I2C1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__I2C1__MASK        0x00100000
#define CLKC_NOC_CLK_SLVRDY_SET__I2C1__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_SLVRDY_SET__I2C1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_SET.I2C - Set I2C socket disconnect.
  */
/* 0 : no effect */
/* 1 : Set mediam_io_reg_hub_i2c_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_SET__I2C__SHIFT       21
#define CLKC_NOC_CLK_SLVRDY_SET__I2C__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_SET__I2C__MASK        0x00200000
#define CLKC_NOC_CLK_SLVRDY_SET__I2C__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_SLVRDY_SET__I2C__HW_DEFAULT  0x1

/* rw Clear socket disconnect  */
#define CLKC_NOC_CLK_SLVRDY_CLEAR 0x186202EC

/* CLKC_NOC_CLK_SLVRDY_CLEAR.AUDIO_IF - Clear AUDIO_IF socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_audio_if_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__AUDIO_IF__SHIFT       0
#define CLKC_NOC_CLK_SLVRDY_CLEAR__AUDIO_IF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__AUDIO_IF__MASK        0x00000001
#define CLKC_NOC_CLK_SLVRDY_CLEAR__AUDIO_IF__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_SLVRDY_CLEAR__AUDIO_IF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.DMAC2 - Clear DMAC2 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_dmac2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC2__SHIFT       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC2__MASK        0x00000002
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC2__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.DMAC3 - Clear DMAC3 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_dmac3_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC3__SHIFT       2
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC3__MASK        0x00000004
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC3__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.SPDIF - Clear SPDIF socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_spdif_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPDIF__SHIFT       3
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPDIF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPDIF__MASK        0x00000008
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPDIF__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPDIF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.USP0 - Clear USP0 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_usp0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP0__SHIFT       4
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP0__MASK        0x00000010
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP0__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.USP1 - Clear USP1 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_usp1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP1__SHIFT       5
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP1__MASK        0x00000020
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP1__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.USP2 - Clear USP2 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_kas_reg_hub_usp2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP2__SHIFT       6
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP2__MASK        0x00000040
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP2__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__USP2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.DVM - Clear DVM socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_xin_reg_hub_dvm_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DVM__SHIFT       7
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DVM__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DVM__MASK        0x00000080
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DVM__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DVM__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.LVDS - Clear LVDS socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear audmscm_xin_reg_hub_lvds_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__LVDS__SHIFT       8
#define CLKC_NOC_CLK_SLVRDY_CLEAR__LVDS__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__LVDS__MASK        0x00000100
#define CLKC_NOC_CLK_SLVRDY_CLEAR__LVDS__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__LVDS__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.A7_SPRAM_1 - Clear A7_SPRAM_1 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear a7_spram_1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_1__SHIFT       9
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_1__MASK        0x00000200
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_1__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.A7_SPRAM_2 - Clear A7_SPRAM_2 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear a7_spram_2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_2__SHIFT       10
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_2__MASK        0x00000400
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_2__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__A7_SPRAM_2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.DMAC0 - Clear DMAC0 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_dmac0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC0__SHIFT       11
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC0__MASK        0x00000800
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC0__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__DMAC0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.SPI1 - Clear SPI1 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_spi1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPI1__SHIFT       12
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPI1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPI1__MASK        0x00001000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPI1__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__SPI1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART0 - Clear UART0 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART0__SHIFT       13
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART0__MASK        0x00002000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART0__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART1 - Clear UART1 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART1__SHIFT       14
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART1__MASK        0x00004000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART1__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART2 - Clear UART2 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart2_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART2__SHIFT       15
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART2__MASK        0x00008000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART2__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART3 - Clear UART3 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart3_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART3__SHIFT       16
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART3__MASK        0x00010000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART3__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART4 - Clear UART4 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart4_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART4__SHIFT       17
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART4__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART4__MASK        0x00020000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART4__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART4__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.UART5 - Clear UART5 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear gnssm_io_reg_hub_uart5_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART5__SHIFT       18
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART5__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART5__MASK        0x00040000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART5__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__UART5__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.GPIO0 - Clear GPIO0 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear mediam_io_reg_hub_gpio0_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__GPIO0__SHIFT       19
#define CLKC_NOC_CLK_SLVRDY_CLEAR__GPIO0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__GPIO0__MASK        0x00080000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__GPIO0__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__GPIO0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.I2C1 - Clear I2C1 socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear mediam_io_reg_hub_i2c1_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C1__SHIFT       20
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C1__MASK        0x00100000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C1__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_CLEAR.I2C - Clear I2C socket disconnect.
 */
/* 0 : no effect */
/* 1 : Clear mediam_io_reg_hub_i2c_PwrDisc_SlvRdy */
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C__SHIFT       21
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C__MASK        0x00200000
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_SLVRDY_CLEAR__I2C__HW_DEFAULT  0x1

/* ro Socket disconnect status */
#define CLKC_NOC_CLK_SLVRDY_STATUS 0x186202F0

/* CLKC_NOC_CLK_SLVRDY_STATUS.AUDIO_IF - AUDIO_IF socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__AUDIO_IF__SHIFT       0
#define CLKC_NOC_CLK_SLVRDY_STATUS__AUDIO_IF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__AUDIO_IF__MASK        0x00000001
#define CLKC_NOC_CLK_SLVRDY_STATUS__AUDIO_IF__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_SLVRDY_STATUS__AUDIO_IF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.DMAC2 - DMAC2 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC2__SHIFT       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC2__MASK        0x00000002
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC2__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.DMAC3 - DMAC3 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC3__SHIFT       2
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC3__MASK        0x00000004
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC3__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.SPDIF - SPDIF socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPDIF__SHIFT       3
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPDIF__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPDIF__MASK        0x00000008
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPDIF__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPDIF__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.USP0 - USP0 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP0__SHIFT       4
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP0__MASK        0x00000010
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP0__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.USP1 - USP1 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP1__SHIFT       5
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP1__MASK        0x00000020
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP1__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.USP2 - USP2 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP2__SHIFT       6
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP2__MASK        0x00000040
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP2__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_SLVRDY_STATUS__USP2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.DVM - DVM socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__DVM__SHIFT       7
#define CLKC_NOC_CLK_SLVRDY_STATUS__DVM__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__DVM__MASK        0x00000080
#define CLKC_NOC_CLK_SLVRDY_STATUS__DVM__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_SLVRDY_STATUS__DVM__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.LVDS - LVDS socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__LVDS__SHIFT       8
#define CLKC_NOC_CLK_SLVRDY_STATUS__LVDS__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__LVDS__MASK        0x00000100
#define CLKC_NOC_CLK_SLVRDY_STATUS__LVDS__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__LVDS__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.A7_SPRAM_1 - A7_SPRAM_1 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_1__SHIFT       9
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_1__MASK        0x00000200
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_1__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.A7_SPRAM_2 - A7_SPRAM_2 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_2__SHIFT       10
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_2__MASK        0x00000400
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_2__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__A7_SPRAM_2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.DMAC0 - DMAC0 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC0__SHIFT       11
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC0__MASK        0x00000800
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC0__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_SLVRDY_STATUS__DMAC0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.SPI1 - SPI1 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPI1__SHIFT       12
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPI1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPI1__MASK        0x00001000
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPI1__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__SPI1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART0 - UART0 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART0__SHIFT       13
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART0__MASK        0x00002000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART0__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART1 - UART1 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART1__SHIFT       14
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART1__MASK        0x00004000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART1__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART2 - UART2 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART2__SHIFT       15
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART2__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART2__MASK        0x00008000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART2__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART2__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART3 - UART3 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART3__SHIFT       16
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART3__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART3__MASK        0x00010000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART3__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART3__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART4 - UART4 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART4__SHIFT       17
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART4__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART4__MASK        0x00020000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART4__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART4__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.UART5 - UART5 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART5__SHIFT       18
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART5__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART5__MASK        0x00040000
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART5__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__UART5__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.GPIO0 - GPIO0 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__GPIO0__SHIFT       19
#define CLKC_NOC_CLK_SLVRDY_STATUS__GPIO0__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__GPIO0__MASK        0x00080000
#define CLKC_NOC_CLK_SLVRDY_STATUS__GPIO0__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__GPIO0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.I2C1 - I2C1 socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C1__SHIFT       20
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C1__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C1__MASK        0x00100000
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C1__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_SLVRDY_STATUS.I2C - I2C socket disconnect status.
 */
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C__SHIFT       21
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C__WIDTH       1
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C__MASK        0x00200000
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_SLVRDY_STATUS__I2C__HW_DEFAULT  0x1

/* ro noc_clk_idle_status     */
/* Status of clock disconnect enable.
 When asserted, this field indicates that the associated clock can be disabled. It relects the value of the clk_xxx_idle signal */
#define CLKC_NOC_CLK_IDLE_STATUS  0x186202F4

/* CLKC_NOC_CLK_IDLE_STATUS.GPU - GPU clock status */
/* 0 : GPU clock cannot be disabled */
/* 1 : GPU clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__GPU__SHIFT       0
#define CLKC_NOC_CLK_IDLE_STATUS__GPU__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__GPU__MASK        0x00000001
#define CLKC_NOC_CLK_IDLE_STATUS__GPU__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_IDLE_STATUS__GPU__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.JPENC - JPENC clock status */
/* 0 : JPENC clock cannot be disabled */
/* 1 : JPENC clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__JPENC__SHIFT       1
#define CLKC_NOC_CLK_IDLE_STATUS__JPENC__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__JPENC__MASK        0x00000002
#define CLKC_NOC_CLK_IDLE_STATUS__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_IDLE_STATUS__JPENC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.KAS - KAS clock status */
/* 0 : KAS clock cannot be disabled */
/* 1 : KAS clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__KAS__SHIFT       2
#define CLKC_NOC_CLK_IDLE_STATUS__KAS__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__KAS__MASK        0x00000004
#define CLKC_NOC_CLK_IDLE_STATUS__KAS__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_IDLE_STATUS__KAS__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.VDEC - VDEC clock status */
/* 0 : VDEC clock cannot be disabled */
/* 1 : VDEC clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__VDEC__SHIFT       3
#define CLKC_NOC_CLK_IDLE_STATUS__VDEC__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__VDEC__MASK        0x00000008
#define CLKC_NOC_CLK_IDLE_STATUS__VDEC__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_IDLE_STATUS__VDEC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.AFE_CVD - AFE_CVD clock status */
/* 0 : AFE_CVD clock cannot be disabled */
/* 1 : AFE_CVD clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__AFE_CVD__SHIFT       4
#define CLKC_NOC_CLK_IDLE_STATUS__AFE_CVD__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__AFE_CVD__MASK        0x00000010
#define CLKC_NOC_CLK_IDLE_STATUS__AFE_CVD__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_IDLE_STATUS__AFE_CVD__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.IO - IO clock status */
/* 0 : IO clock cannot be disabled */
/* 1 : IO clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__IO__SHIFT       5
#define CLKC_NOC_CLK_IDLE_STATUS__IO__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__IO__MASK        0x00000020
#define CLKC_NOC_CLK_IDLE_STATUS__IO__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_IDLE_STATUS__IO__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.CSSI - CSSI clock status */
/* 0 : CSSI clock cannot be disabled */
/* 1 : CSSI clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__CSSI__SHIFT       6
#define CLKC_NOC_CLK_IDLE_STATUS__CSSI__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__CSSI__MASK        0x00000040
#define CLKC_NOC_CLK_IDLE_STATUS__CSSI__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_IDLE_STATUS__CSSI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.CAN - CAN clock status */
/* 0 : CAN clock cannot be disabled */
/* 1 : CAN clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__CAN__SHIFT       7
#define CLKC_NOC_CLK_IDLE_STATUS__CAN__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__CAN__MASK        0x00000080
#define CLKC_NOC_CLK_IDLE_STATUS__CAN__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_IDLE_STATUS__CAN__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.CC_PUB - CC_PUB clock status */
/* 0 : CC_PUB clock cannot be disabled */
/* 1 : CC_PUB clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__CC_PUB__SHIFT       8
#define CLKC_NOC_CLK_IDLE_STATUS__CC_PUB__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__CC_PUB__MASK        0x00000100
#define CLKC_NOC_CLK_IDLE_STATUS__CC_PUB__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_IDLE_STATUS__CC_PUB__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.CC_SEC - CC_SEC clock status */
/* 0 : CC_SEC clock cannot be disabled */
/* 1 : CC_SEC clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__CC_SEC__SHIFT       9
#define CLKC_NOC_CLK_IDLE_STATUS__CC_SEC__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__CC_SEC__MASK        0x00000200
#define CLKC_NOC_CLK_IDLE_STATUS__CC_SEC__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_IDLE_STATUS__CC_SEC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.ETH - ETH clock status */
/* 0 : ETH clock cannot be disabled */
/* 1 : ETH clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__ETH__SHIFT       10
#define CLKC_NOC_CLK_IDLE_STATUS__ETH__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__ETH__MASK        0x00000400
#define CLKC_NOC_CLK_IDLE_STATUS__ETH__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_IDLE_STATUS__ETH__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.SDR - SDR clock status */
/* 0 : SDR clock cannot be disabled */
/* 1 : SDR clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__SDR__SHIFT       11
#define CLKC_NOC_CLK_IDLE_STATUS__SDR__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__SDR__MASK        0x00000800
#define CLKC_NOC_CLK_IDLE_STATUS__SDR__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_IDLE_STATUS__SDR__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.G2D - G2D clock status */
/* 0 : G2D clock cannot be disabled */
/* 1 : G2D clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__G2D__SHIFT       12
#define CLKC_NOC_CLK_IDLE_STATUS__G2D__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__G2D__MASK        0x00001000
#define CLKC_NOC_CLK_IDLE_STATUS__G2D__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_IDLE_STATUS__G2D__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.MEDIAM_PCI - MEDIAM_PCI clock status */
/* 0 : MEDIAM_PCI clock cannot be disabled */
/* 1 : MEDIAM_PCI clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__MEDIAM_PCI__SHIFT       13
#define CLKC_NOC_CLK_IDLE_STATUS__MEDIAM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__MEDIAM_PCI__MASK        0x00002000
#define CLKC_NOC_CLK_IDLE_STATUS__MEDIAM_PCI__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_IDLE_STATUS__MEDIAM_PCI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.NAND - NAND clock status */
/* 0 : NAND clock cannot be disabled */
/* 1 : NAND clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__NAND__SHIFT       14
#define CLKC_NOC_CLK_IDLE_STATUS__NAND__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__NAND__MASK        0x00004000
#define CLKC_NOC_CLK_IDLE_STATUS__NAND__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_IDLE_STATUS__NAND__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.USB0 - USB0 clock status */
/* 0 : USB0 clock cannot be disabled */
/* 1 : USB0 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__USB0__SHIFT       15
#define CLKC_NOC_CLK_IDLE_STATUS__USB0__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__USB0__MASK        0x00008000
#define CLKC_NOC_CLK_IDLE_STATUS__USB0__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_IDLE_STATUS__USB0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.USB1 - USB1 clock status */
/* 0 : USB1 clock cannot be disabled */
/* 1 : USB1 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__USB1__SHIFT       16
#define CLKC_NOC_CLK_IDLE_STATUS__USB1__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__USB1__MASK        0x00010000
#define CLKC_NOC_CLK_IDLE_STATUS__USB1__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__USB1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.DCU - DCU clock status */
/* 0 : DCU clock cannot be disabled */
/* 1 : DCU clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__DCU__SHIFT       17
#define CLKC_NOC_CLK_IDLE_STATUS__DCU__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__DCU__MASK        0x00020000
#define CLKC_NOC_CLK_IDLE_STATUS__DCU__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__DCU__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.LCD0 - LCD0 clock status */
/* 0 : LCD0 clock cannot be disabled */
/* 1 : LCD0 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__LCD0__SHIFT       18
#define CLKC_NOC_CLK_IDLE_STATUS__LCD0__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__LCD0__MASK        0x00040000
#define CLKC_NOC_CLK_IDLE_STATUS__LCD0__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__LCD0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.LCD1 - LCD1 clock status */
/* 0 : LCD1 clock cannot be disabled */
/* 1 : LCD1 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__LCD1__SHIFT       19
#define CLKC_NOC_CLK_IDLE_STATUS__LCD1__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__LCD1__MASK        0x00080000
#define CLKC_NOC_CLK_IDLE_STATUS__LCD1__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_IDLE_STATUS__LCD1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.VDIFM_PCI - VDIFM_PCI clock status */
/* 0 : VDIFM_PCI clock cannot be disabled */
/* 1 : VDIFM_PCI clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__VDIFM_PCI__SHIFT       20
#define CLKC_NOC_CLK_IDLE_STATUS__VDIFM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__VDIFM_PCI__MASK        0x00100000
#define CLKC_NOC_CLK_IDLE_STATUS__VDIFM_PCI__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__VDIFM_PCI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.VIP1 - VIP1 clock status */
/* 0 : VIP1 clock cannot be disabled */
/* 1 : VIP1 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__VIP1__SHIFT       21
#define CLKC_NOC_CLK_IDLE_STATUS__VIP1__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__VIP1__MASK        0x00200000
#define CLKC_NOC_CLK_IDLE_STATUS__VIP1__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__VIP1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.VPP0 - VPP0 clock status */
/* 0 : VPP0 clock cannot be disabled */
/* 1 : VPP0 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__VPP0__SHIFT       22
#define CLKC_NOC_CLK_IDLE_STATUS__VPP0__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__VPP0__MASK        0x00400000
#define CLKC_NOC_CLK_IDLE_STATUS__VPP0__INV_MASK    0xFFBFFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__VPP0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLE_STATUS.VPP1 - VPP1 clock status */
/* 0 : VPP1 clock cannot be disabled */
/* 1 : VPP1 clock can be disabled */
#define CLKC_NOC_CLK_IDLE_STATUS__VPP1__SHIFT       23
#define CLKC_NOC_CLK_IDLE_STATUS__VPP1__WIDTH       1
#define CLKC_NOC_CLK_IDLE_STATUS__VPP1__MASK        0x00800000
#define CLKC_NOC_CLK_IDLE_STATUS__VPP1__INV_MASK    0xFF7FFFFF
#define CLKC_NOC_CLK_IDLE_STATUS__VPP1__HW_DEFAULT  0x1

/* ro plug_clk_idle_status    */
/* Status of clock disconnect enable.
 When asserted, this field indicates that the associated clock can be disabled. It relects the value of the clk_xxx_idle signal */
#define CLKC_PLUG_CLK_IDLE_STATUS 0x186202F8

/* CLKC_PLUG_CLK_IDLE_STATUS.BTM_AUDMSCM_D_BTM - BTM_AUDMSCM_D_BTM clock status */
/* 0 : BTM_AUDMSCM_D_BTM clock cannot be disabled */
/* 1 : BTM_AUDMSCM_D_BTM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__BTM_AUDMSCM_D_BTM__SHIFT       0
#define CLKC_PLUG_CLK_IDLE_STATUS__BTM_AUDMSCM_D_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__BTM_AUDMSCM_D_BTM__MASK        0x00000001
#define CLKC_PLUG_CLK_IDLE_STATUS__BTM_AUDMSCM_D_BTM__INV_MASK    0xFFFFFFFE
#define CLKC_PLUG_CLK_IDLE_STATUS__BTM_AUDMSCM_D_BTM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.AUDMSCM_BTM_R_BTM - AUDMSCM_BTM_R_BTM clock status */
/* 0 : AUDMSCM_BTM_R_BTM clock cannot be disabled */
/* 1 : AUDMSCM_BTM_R_BTM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__AUDMSCM_BTM_R_BTM__SHIFT       1
#define CLKC_PLUG_CLK_IDLE_STATUS__AUDMSCM_BTM_R_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__AUDMSCM_BTM_R_BTM__MASK        0x00000002
#define CLKC_PLUG_CLK_IDLE_STATUS__AUDMSCM_BTM_R_BTM__INV_MASK    0xFFFFFFFD
#define CLKC_PLUG_CLK_IDLE_STATUS__AUDMSCM_BTM_R_BTM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.CPUM_DDRM1_D_DDRM - CPUM_DDRM1_D_DDRM clock status */
/* 0 : CPUM_DDRM1_D_DDRM clock cannot be disabled */
/* 1 : CPUM_DDRM1_D_DDRM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__CPUM_DDRM1_D_DDRM__SHIFT       2
#define CLKC_PLUG_CLK_IDLE_STATUS__CPUM_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__CPUM_DDRM1_D_DDRM__MASK        0x00000004
#define CLKC_PLUG_CLK_IDLE_STATUS__CPUM_DDRM1_D_DDRM__INV_MASK    0xFFFFFFFB
#define CLKC_PLUG_CLK_IDLE_STATUS__CPUM_DDRM1_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.CPU_DDRM1_D_DDRM - CPU_DDRM1_D_DDRM clock status */
/* 0 : CPU_DDRM1_D_DDRM clock cannot be disabled */
/* 1 : CPU_DDRM1_D_DDRM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__CPU_DDRM1_D_DDRM__SHIFT       3
#define CLKC_PLUG_CLK_IDLE_STATUS__CPU_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__CPU_DDRM1_D_DDRM__MASK        0x00000008
#define CLKC_PLUG_CLK_IDLE_STATUS__CPU_DDRM1_D_DDRM__INV_MASK    0xFFFFFFF7
#define CLKC_PLUG_CLK_IDLE_STATUS__CPU_DDRM1_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.GPUM_DDRM3_D_DDRM - GPUM_DDRM3_D_DDRM clock status */
/* 0 : GPUM_DDRM3_D_DDRM clock cannot be disabled */
/* 1 : GPUM_DDRM3_D_DDRM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM3_D_DDRM__SHIFT       4
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM3_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM3_D_DDRM__MASK        0x00000010
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM3_D_DDRM__INV_MASK    0xFFFFFFEF
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM3_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.GPUM_DDRM2_D_DDRM - GPUM_DDRM2_D_DDRM clock status */
/* 0 : GPUM_DDRM2_D_DDRM clock cannot be disabled */
/* 1 : GPUM_DDRM2_D_DDRM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM2_D_DDRM__SHIFT       5
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM2_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM2_D_DDRM__MASK        0x00000020
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM2_D_DDRM__INV_MASK    0xFFFFFFDF
#define CLKC_PLUG_CLK_IDLE_STATUS__GPUM_DDRM2_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLE_STATUS.MEDIAM_DDRM4_D_DDRM - MEDIAM_DDRM4_D_DDRM clock status */
/* 0 : MEDIAM_DDRM4_D_DDRM clock cannot be disabled */
/* 1 : MEDIAM_DDRM4_D_DDRM clock can be disabled */
#define CLKC_PLUG_CLK_IDLE_STATUS__MEDIAM_DDRM4_D_DDRM__SHIFT       6
#define CLKC_PLUG_CLK_IDLE_STATUS__MEDIAM_DDRM4_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLE_STATUS__MEDIAM_DDRM4_D_DDRM__MASK        0x00000040
#define CLKC_PLUG_CLK_IDLE_STATUS__MEDIAM_DDRM4_D_DDRM__INV_MASK    0xFFFFFFBF
#define CLKC_PLUG_CLK_IDLE_STATUS__MEDIAM_DDRM4_D_DDRM__HW_DEFAULT  0x1

/* ro noc_clk_idleack_status  */
/* Status of clock disconnect request.
 This field only acknowledges the idle request, reflecting the value of the clk_xxx_idle_ack signal.
 IT DOES NOT INDICATE THAT THE ASSOCIATED CLOCK CAN BE DISABLED. */
#define CLKC_NOC_CLK_IDLEACK_STATUS 0x186202FC

/* CLKC_NOC_CLK_IDLEACK_STATUS.GPU - GPU idleack */
/* 0 : NOC did not acknowledged the GPU request */
/* 1 : NOC acknowledged the GPU request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__GPU__SHIFT       0
#define CLKC_NOC_CLK_IDLEACK_STATUS__GPU__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__GPU__MASK        0x00000001
#define CLKC_NOC_CLK_IDLEACK_STATUS__GPU__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_CLK_IDLEACK_STATUS__GPU__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.JPENC - JPENC idleack */
/* 0 : NOC did not acknowledged the JPENC request */
/* 1 : NOC acknowledged the JPENC request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__JPENC__SHIFT       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__JPENC__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__JPENC__MASK        0x00000002
#define CLKC_NOC_CLK_IDLEACK_STATUS__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_CLK_IDLEACK_STATUS__JPENC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.KAS - KAS idleack */
/* 0 : NOC did not acknowledged the KAS request */
/* 1 : NOC acknowledged the KAS request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__KAS__SHIFT       2
#define CLKC_NOC_CLK_IDLEACK_STATUS__KAS__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__KAS__MASK        0x00000004
#define CLKC_NOC_CLK_IDLEACK_STATUS__KAS__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_CLK_IDLEACK_STATUS__KAS__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.VDEC - VDEC idleack */
/* 0 : NOC did not acknowledged the VDEC request */
/* 1 : NOC acknowledged the VDEC request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDEC__SHIFT       3
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDEC__MASK        0x00000008
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDEC__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDEC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.IO - IO idleack */
/* 0 : NOC did not acknowledged the IO request */
/* 1 : NOC acknowledged the IO request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__IO__SHIFT       4
#define CLKC_NOC_CLK_IDLEACK_STATUS__IO__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__IO__MASK        0x00000010
#define CLKC_NOC_CLK_IDLEACK_STATUS__IO__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_CLK_IDLEACK_STATUS__IO__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.AFE_CVD - AFE_CVD idleack */
/* 0 : NOC did not acknowledged the AFE_CVD request */
/* 1 : NOC acknowledged the AFE_CVD request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__AFE_CVD__SHIFT       5
#define CLKC_NOC_CLK_IDLEACK_STATUS__AFE_CVD__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__AFE_CVD__MASK        0x00000020
#define CLKC_NOC_CLK_IDLEACK_STATUS__AFE_CVD__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_CLK_IDLEACK_STATUS__AFE_CVD__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.CSSI - CSSI idleack */
/* 0 : NOC did not acknowledged the CSSI request */
/* 1 : NOC acknowledged the CSSI request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__CSSI__SHIFT       6
#define CLKC_NOC_CLK_IDLEACK_STATUS__CSSI__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__CSSI__MASK        0x00000040
#define CLKC_NOC_CLK_IDLEACK_STATUS__CSSI__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_CLK_IDLEACK_STATUS__CSSI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.CAN - CAN idleack */
/* 0 : NOC did not acknowledged the CAN request */
/* 1 : NOC acknowledged the CAN request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__CAN__SHIFT       7
#define CLKC_NOC_CLK_IDLEACK_STATUS__CAN__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__CAN__MASK        0x00000080
#define CLKC_NOC_CLK_IDLEACK_STATUS__CAN__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_CLK_IDLEACK_STATUS__CAN__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.CC_PUB - CC_PUB idleack */
/* 0 : NOC did not acknowledged the CC_PUB request */
/* 1 : NOC acknowledged the CC_PUB request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_PUB__SHIFT       8
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_PUB__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_PUB__MASK        0x00000100
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_PUB__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_PUB__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.CC_SEC - CC_SEC idleack */
/* 0 : NOC did not acknowledged the CC_SEC request */
/* 1 : NOC acknowledged the CC_SEC request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_SEC__SHIFT       9
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_SEC__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_SEC__MASK        0x00000200
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_SEC__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__CC_SEC__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.ETH - ETH idleack */
/* 0 : NOC did not acknowledged the ETH request */
/* 1 : NOC acknowledged the ETH request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__ETH__SHIFT       10
#define CLKC_NOC_CLK_IDLEACK_STATUS__ETH__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__ETH__MASK        0x00000400
#define CLKC_NOC_CLK_IDLEACK_STATUS__ETH__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__ETH__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.SDR - SDR idleack */
/* 0 : NOC did not acknowledged the SDR request */
/* 1 : NOC acknowledged the SDR request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__SDR__SHIFT       11
#define CLKC_NOC_CLK_IDLEACK_STATUS__SDR__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__SDR__MASK        0x00000800
#define CLKC_NOC_CLK_IDLEACK_STATUS__SDR__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_CLK_IDLEACK_STATUS__SDR__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.G2D - G2D idleack */
/* 0 : NOC did not acknowledged the G2D request */
/* 1 : NOC acknowledged the G2D request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__G2D__SHIFT       12
#define CLKC_NOC_CLK_IDLEACK_STATUS__G2D__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__G2D__MASK        0x00001000
#define CLKC_NOC_CLK_IDLEACK_STATUS__G2D__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__G2D__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.MEDIAM_PCI - MEDIAM_PCI idleack */
/* 0 : NOC did not acknowledged the MEDIAM_PCI request */
/* 1 : NOC acknowledged the MEDIAM_PCI request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__MEDIAM_PCI__SHIFT       13
#define CLKC_NOC_CLK_IDLEACK_STATUS__MEDIAM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__MEDIAM_PCI__MASK        0x00002000
#define CLKC_NOC_CLK_IDLEACK_STATUS__MEDIAM_PCI__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__MEDIAM_PCI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.NAND - NAND idleack */
/* 0 : NOC did not acknowledged the NAND request */
/* 1 : NOC acknowledged the NAND request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__NAND__SHIFT       14
#define CLKC_NOC_CLK_IDLEACK_STATUS__NAND__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__NAND__MASK        0x00004000
#define CLKC_NOC_CLK_IDLEACK_STATUS__NAND__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__NAND__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.USB0 - USB0 idleack */
/* 0 : NOC did not acknowledged the USB0 request */
/* 1 : NOC acknowledged the USB0 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB0__SHIFT       15
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB0__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB0__MASK        0x00008000
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB0__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.USB1 - USB1 idleack */
/* 0 : NOC did not acknowledged the USB1 request */
/* 1 : NOC acknowledged the USB1 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB1__SHIFT       16
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB1__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB1__MASK        0x00010000
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB1__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__USB1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.DCU - DCU idleack */
/* 0 : NOC did not acknowledged the DCU request */
/* 1 : NOC acknowledged the DCU request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__DCU__SHIFT       17
#define CLKC_NOC_CLK_IDLEACK_STATUS__DCU__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__DCU__MASK        0x00020000
#define CLKC_NOC_CLK_IDLEACK_STATUS__DCU__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__DCU__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.LCD0 - LCD0 idleack */
/* 0 : NOC did not acknowledged the LCD0 request */
/* 1 : NOC acknowledged the LCD0 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD0__SHIFT       18
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD0__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD0__MASK        0x00040000
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD0__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.LCD1 - LCD1 idleack */
/* 0 : NOC did not acknowledged the LCD1 request */
/* 1 : NOC acknowledged the LCD1 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD1__SHIFT       19
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD1__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD1__MASK        0x00080000
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD1__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__LCD1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.VDIFM_PCI - VDIFM_PCI idleack */
/* 0 : NOC did not acknowledged the VDIFM_PCI request */
/* 1 : NOC acknowledged the VDIFM_PCI request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDIFM_PCI__SHIFT       20
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDIFM_PCI__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDIFM_PCI__MASK        0x00100000
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDIFM_PCI__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__VDIFM_PCI__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.VIP1 - VIP1 idleack */
/* 0 : NOC did not acknowledged the VIP1 request */
/* 1 : NOC acknowledged the VIP1 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__VIP1__SHIFT       21
#define CLKC_NOC_CLK_IDLEACK_STATUS__VIP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__VIP1__MASK        0x00200000
#define CLKC_NOC_CLK_IDLEACK_STATUS__VIP1__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__VIP1__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.VPP0 - VPP0 idleack */
/* 0 : NOC did not acknowledged the VPP0 request */
/* 1 : NOC acknowledged the VPP0 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP0__SHIFT       22
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP0__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP0__MASK        0x00400000
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP0__INV_MASK    0xFFBFFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP0__HW_DEFAULT  0x1

/* CLKC_NOC_CLK_IDLEACK_STATUS.VPP1 - VPP1 idleack */
/* 0 : NOC did not acknowledged the VPP1 request */
/* 1 : NOC acknowledged the VPP1 request */
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP1__SHIFT       23
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP1__WIDTH       1
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP1__MASK        0x00800000
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP1__INV_MASK    0xFF7FFFFF
#define CLKC_NOC_CLK_IDLEACK_STATUS__VPP1__HW_DEFAULT  0x1

/* ro plug_clk_idleack_status */
/* Status of clock disconnect request.
 This field only acknowledges the idle request, reflecting the value of the clk_xxx_idle_ack signal.
 IT DOES NOT INDICATE THAT THE ASSOCIATED CLOCK CAN BE DISABLED. */
#define CLKC_PLUG_CLK_IDLEACK_STATUS 0x18620300

/* CLKC_PLUG_CLK_IDLEACK_STATUS.BTM_AUDMSCM_D_BTM - BTM_AUDMSCM_D_BTM idleack */
/* 0 : NOC did not acknowledged the BTM_AUDMSCM_D_BTM request */
/* 1 : NOC acknowledged the BTM_AUDMSCM_D_BTM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__BTM_AUDMSCM_D_BTM__SHIFT       0
#define CLKC_PLUG_CLK_IDLEACK_STATUS__BTM_AUDMSCM_D_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__BTM_AUDMSCM_D_BTM__MASK        0x00000001
#define CLKC_PLUG_CLK_IDLEACK_STATUS__BTM_AUDMSCM_D_BTM__INV_MASK    0xFFFFFFFE
#define CLKC_PLUG_CLK_IDLEACK_STATUS__BTM_AUDMSCM_D_BTM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.AUDMSCM_BTM_R_BTM - AUDMSCM_BTM_R_BTM idleack */
/* 0 : NOC did not acknowledged the AUDMSCM_BTM_R_BTM request */
/* 1 : NOC acknowledged the AUDMSCM_BTM_R_BTM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__AUDMSCM_BTM_R_BTM__SHIFT       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__AUDMSCM_BTM_R_BTM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__AUDMSCM_BTM_R_BTM__MASK        0x00000002
#define CLKC_PLUG_CLK_IDLEACK_STATUS__AUDMSCM_BTM_R_BTM__INV_MASK    0xFFFFFFFD
#define CLKC_PLUG_CLK_IDLEACK_STATUS__AUDMSCM_BTM_R_BTM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.CPUM_DDRM1_D_DDRM - CPUM_DDRM1_D_DDRM idleack */
/* 0 : NOC did not acknowledged the CPUM_DDRM1_D_DDRM request */
/* 1 : NOC acknowledged the CPUM_DDRM1_D_DDRM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPUM_DDRM1_D_DDRM__SHIFT       2
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPUM_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPUM_DDRM1_D_DDRM__MASK        0x00000004
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPUM_DDRM1_D_DDRM__INV_MASK    0xFFFFFFFB
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPUM_DDRM1_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.CPU_DDRM1_D_DDRM - CPU_DDRM1_D_DDRM idleack */
/* 0 : NOC did not acknowledged the CPU_DDRM1_D_DDRM request */
/* 1 : NOC acknowledged the CPU_DDRM1_D_DDRM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPU_DDRM1_D_DDRM__SHIFT       3
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPU_DDRM1_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPU_DDRM1_D_DDRM__MASK        0x00000008
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPU_DDRM1_D_DDRM__INV_MASK    0xFFFFFFF7
#define CLKC_PLUG_CLK_IDLEACK_STATUS__CPU_DDRM1_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.GPUM_DDRM3_D_DDRM - GPUM_DDRM3_D_DDRM idleack */
/* 0 : NOC did not acknowledged the GPUM_DDRM3_D_DDRM request */
/* 1 : NOC acknowledged the GPUM_DDRM3_D_DDRM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM3_D_DDRM__SHIFT       4
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM3_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM3_D_DDRM__MASK        0x00000010
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM3_D_DDRM__INV_MASK    0xFFFFFFEF
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM3_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.GPUM_DDRM2_D_DDRM - GPUM_DDRM2_D_DDRM idleack */
/* 0 : NOC did not acknowledged the GPUM_DDRM2_D_DDRM request */
/* 1 : NOC acknowledged the GPUM_DDRM2_D_DDRM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM2_D_DDRM__SHIFT       5
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM2_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM2_D_DDRM__MASK        0x00000020
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM2_D_DDRM__INV_MASK    0xFFFFFFDF
#define CLKC_PLUG_CLK_IDLEACK_STATUS__GPUM_DDRM2_D_DDRM__HW_DEFAULT  0x1

/* CLKC_PLUG_CLK_IDLEACK_STATUS.MEDIAM_DDRM4_D_DDRM - MEDIAM_DDRM4_D_DDRM idleack */
/* 0 : NOC did not acknowledged the MEDIAM_DDRM4_D_DDRM request */
/* 1 : NOC acknowledged the MEDIAM_DDRM4_D_DDRM request */
#define CLKC_PLUG_CLK_IDLEACK_STATUS__MEDIAM_DDRM4_D_DDRM__SHIFT       6
#define CLKC_PLUG_CLK_IDLEACK_STATUS__MEDIAM_DDRM4_D_DDRM__WIDTH       1
#define CLKC_PLUG_CLK_IDLEACK_STATUS__MEDIAM_DDRM4_D_DDRM__MASK        0x00000040
#define CLKC_PLUG_CLK_IDLEACK_STATUS__MEDIAM_DDRM4_D_DDRM__INV_MASK    0xFFFFFFBF
#define CLKC_PLUG_CLK_IDLEACK_STATUS__MEDIAM_DDRM4_D_DDRM__HW_DEFAULT  0x1

/* ro noc_socketconn_status */
#define CLKC_NOC_SOCKETCONN_STATUS 0x18620304

/* CLKC_NOC_SOCKETCONN_STATUS.AUDIO_IF - AUDIO_IF socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__AUDIO_IF__SHIFT       0
#define CLKC_NOC_SOCKETCONN_STATUS__AUDIO_IF__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__AUDIO_IF__MASK        0x00000001
#define CLKC_NOC_SOCKETCONN_STATUS__AUDIO_IF__INV_MASK    0xFFFFFFFE
#define CLKC_NOC_SOCKETCONN_STATUS__AUDIO_IF__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.DMAC2 - DMAC2 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC2__SHIFT       1
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC2__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC2__MASK        0x00000002
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC2__INV_MASK    0xFFFFFFFD
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC2__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.DMAC3 - DMAC3 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC3__SHIFT       2
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC3__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC3__MASK        0x00000004
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC3__INV_MASK    0xFFFFFFFB
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC3__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.SPDIF - SPDIF socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__SPDIF__SHIFT       3
#define CLKC_NOC_SOCKETCONN_STATUS__SPDIF__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__SPDIF__MASK        0x00000008
#define CLKC_NOC_SOCKETCONN_STATUS__SPDIF__INV_MASK    0xFFFFFFF7
#define CLKC_NOC_SOCKETCONN_STATUS__SPDIF__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.USP0 - USP0 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__USP0__SHIFT       4
#define CLKC_NOC_SOCKETCONN_STATUS__USP0__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__USP0__MASK        0x00000010
#define CLKC_NOC_SOCKETCONN_STATUS__USP0__INV_MASK    0xFFFFFFEF
#define CLKC_NOC_SOCKETCONN_STATUS__USP0__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.USP1 - USP1 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__USP1__SHIFT       5
#define CLKC_NOC_SOCKETCONN_STATUS__USP1__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__USP1__MASK        0x00000020
#define CLKC_NOC_SOCKETCONN_STATUS__USP1__INV_MASK    0xFFFFFFDF
#define CLKC_NOC_SOCKETCONN_STATUS__USP1__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.USP2 - USP2 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__USP2__SHIFT       6
#define CLKC_NOC_SOCKETCONN_STATUS__USP2__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__USP2__MASK        0x00000040
#define CLKC_NOC_SOCKETCONN_STATUS__USP2__INV_MASK    0xFFFFFFBF
#define CLKC_NOC_SOCKETCONN_STATUS__USP2__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.DVM - DVM socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__DVM__SHIFT       7
#define CLKC_NOC_SOCKETCONN_STATUS__DVM__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__DVM__MASK        0x00000080
#define CLKC_NOC_SOCKETCONN_STATUS__DVM__INV_MASK    0xFFFFFF7F
#define CLKC_NOC_SOCKETCONN_STATUS__DVM__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.LVDS - LVDS socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__LVDS__SHIFT       8
#define CLKC_NOC_SOCKETCONN_STATUS__LVDS__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__LVDS__MASK        0x00000100
#define CLKC_NOC_SOCKETCONN_STATUS__LVDS__INV_MASK    0xFFFFFEFF
#define CLKC_NOC_SOCKETCONN_STATUS__LVDS__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.A7_SPRAM_1 - A7_SPRAM_1 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_1__SHIFT       9
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_1__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_1__MASK        0x00000200
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_1__INV_MASK    0xFFFFFDFF
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_1__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.A7_SPRAM_2 - A7_SPRAM_2 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_2__SHIFT       10
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_2__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_2__MASK        0x00000400
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_2__INV_MASK    0xFFFFFBFF
#define CLKC_NOC_SOCKETCONN_STATUS__A7_SPRAM_2__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.DMAC0 - DMAC0 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC0__SHIFT       11
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC0__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC0__MASK        0x00000800
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC0__INV_MASK    0xFFFFF7FF
#define CLKC_NOC_SOCKETCONN_STATUS__DMAC0__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.SPI1 - SPI1 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__SPI1__SHIFT       12
#define CLKC_NOC_SOCKETCONN_STATUS__SPI1__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__SPI1__MASK        0x00001000
#define CLKC_NOC_SOCKETCONN_STATUS__SPI1__INV_MASK    0xFFFFEFFF
#define CLKC_NOC_SOCKETCONN_STATUS__SPI1__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART0 - UART0 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART0__SHIFT       13
#define CLKC_NOC_SOCKETCONN_STATUS__UART0__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART0__MASK        0x00002000
#define CLKC_NOC_SOCKETCONN_STATUS__UART0__INV_MASK    0xFFFFDFFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART0__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART1 - UART1 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART1__SHIFT       14
#define CLKC_NOC_SOCKETCONN_STATUS__UART1__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART1__MASK        0x00004000
#define CLKC_NOC_SOCKETCONN_STATUS__UART1__INV_MASK    0xFFFFBFFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART1__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART2 - UART2 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART2__SHIFT       15
#define CLKC_NOC_SOCKETCONN_STATUS__UART2__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART2__MASK        0x00008000
#define CLKC_NOC_SOCKETCONN_STATUS__UART2__INV_MASK    0xFFFF7FFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART2__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART3 - UART3 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART3__SHIFT       16
#define CLKC_NOC_SOCKETCONN_STATUS__UART3__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART3__MASK        0x00010000
#define CLKC_NOC_SOCKETCONN_STATUS__UART3__INV_MASK    0xFFFEFFFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART3__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART4 - UART4 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART4__SHIFT       17
#define CLKC_NOC_SOCKETCONN_STATUS__UART4__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART4__MASK        0x00020000
#define CLKC_NOC_SOCKETCONN_STATUS__UART4__INV_MASK    0xFFFDFFFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART4__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.UART5 - UART5 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__UART5__SHIFT       18
#define CLKC_NOC_SOCKETCONN_STATUS__UART5__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__UART5__MASK        0x00040000
#define CLKC_NOC_SOCKETCONN_STATUS__UART5__INV_MASK    0xFFFBFFFF
#define CLKC_NOC_SOCKETCONN_STATUS__UART5__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.GPIO0 - GPIO0 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__GPIO0__SHIFT       19
#define CLKC_NOC_SOCKETCONN_STATUS__GPIO0__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__GPIO0__MASK        0x00080000
#define CLKC_NOC_SOCKETCONN_STATUS__GPIO0__INV_MASK    0xFFF7FFFF
#define CLKC_NOC_SOCKETCONN_STATUS__GPIO0__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.I2C1 - I2C1 socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__I2C1__SHIFT       20
#define CLKC_NOC_SOCKETCONN_STATUS__I2C1__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__I2C1__MASK        0x00100000
#define CLKC_NOC_SOCKETCONN_STATUS__I2C1__INV_MASK    0xFFEFFFFF
#define CLKC_NOC_SOCKETCONN_STATUS__I2C1__HW_DEFAULT  0x1

/* CLKC_NOC_SOCKETCONN_STATUS.I2C - I2C socketconn_status */
/* 0 : socket can be disconnected */
/* 1 : socket cannot be disconnected */
#define CLKC_NOC_SOCKETCONN_STATUS__I2C__SHIFT       21
#define CLKC_NOC_SOCKETCONN_STATUS__I2C__WIDTH       1
#define CLKC_NOC_SOCKETCONN_STATUS__I2C__MASK        0x00200000
#define CLKC_NOC_SOCKETCONN_STATUS__I2C__INV_MASK    0xFFDFFFFF
#define CLKC_NOC_SOCKETCONN_STATUS__I2C__HW_DEFAULT  0x1

/* RSTC A7 SW RESET */
/* Activated on the rising edge. Reset is enabled by writing 0 and then 1 to this register 
 or by a7_warm_reset_b from PWRC. It also executes self refresh (or timeout) sequence */
#define CLKC_RSTC_A7_SW_RST       0x18620308

/* CLKC_RSTC_A7_SW_RST.a7_sw_reset_b - A7 SW reset */
#define CLKC_RSTC_A7_SW_RST__A7_SW_RESET_B__SHIFT       0
#define CLKC_RSTC_A7_SW_RST__A7_SW_RESET_B__WIDTH       1
#define CLKC_RSTC_A7_SW_RST__A7_SW_RESET_B__MASK        0x00000001
#define CLKC_RSTC_A7_SW_RST__A7_SW_RESET_B__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_A7_SW_RST__A7_SW_RESET_B__HW_DEFAULT  0x1

/* RSTC A7 DEBUG reset */
#define CLKC_RSTC_A7_DBG_RST      0x1862030C

/* CLKC_RSTC_A7_DBG_RST.a7_dbg_rst_b - A7 DEBUG reset */
#define CLKC_RSTC_A7_DBG_RST__A7_DBG_RST_B__SHIFT       0
#define CLKC_RSTC_A7_DBG_RST__A7_DBG_RST_B__WIDTH       1
#define CLKC_RSTC_A7_DBG_RST__A7_DBG_RST_B__MASK        0x00000001
#define CLKC_RSTC_A7_DBG_RST__A7_DBG_RST_B__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_A7_DBG_RST__A7_DBG_RST_B__HW_DEFAULT  0x1

/* RSTC DRAM self refresh enable */
#define CLKC_RSTC_SREFRESN_EN     0x18620310

/* CLKC_RSTC_SREFRESN_EN.enable - DRAM self refresh enable */
#define CLKC_RSTC_SREFRESN_EN__ENABLE__SHIFT       0
#define CLKC_RSTC_SREFRESN_EN__ENABLE__WIDTH       1
#define CLKC_RSTC_SREFRESN_EN__ENABLE__MASK        0x00000001
#define CLKC_RSTC_SREFRESN_EN__ENABLE__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_SREFRESN_EN__ENABLE__HW_DEFAULT  0x1

/* RSTC status register */
/* Indicates the staus of reset controller 
 0 - wasn't event, 1 - was event 
 bits [9:0] can be cleared to any value by writing to RSTC_STATUS and triggered by writing to RSTC_STATUS_CLR */
#define CLKC_RSTC_STATUS          0x18620314

/* CLKC_RSTC_STATUS.EARLY_RST -  */
#define CLKC_RSTC_STATUS__EARLY_RST__SHIFT       0
#define CLKC_RSTC_STATUS__EARLY_RST__WIDTH       1
#define CLKC_RSTC_STATUS__EARLY_RST__MASK        0x00000001
#define CLKC_RSTC_STATUS__EARLY_RST__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_STATUS__EARLY_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.CORE_RST -  */
#define CLKC_RSTC_STATUS__CORE_RST__SHIFT       1
#define CLKC_RSTC_STATUS__CORE_RST__WIDTH       1
#define CLKC_RSTC_STATUS__CORE_RST__MASK        0x00000002
#define CLKC_RSTC_STATUS__CORE_RST__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_STATUS__CORE_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.CPU_RST -  */
#define CLKC_RSTC_STATUS__CPU_RST__SHIFT       2
#define CLKC_RSTC_STATUS__CPU_RST__WIDTH       1
#define CLKC_RSTC_STATUS__CPU_RST__MASK        0x00000004
#define CLKC_RSTC_STATUS__CPU_RST__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_STATUS__CPU_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.A7_SW_RST -  */
#define CLKC_RSTC_STATUS__A7_SW_RST__SHIFT       3
#define CLKC_RSTC_STATUS__A7_SW_RST__WIDTH       1
#define CLKC_RSTC_STATUS__A7_SW_RST__MASK        0x00000008
#define CLKC_RSTC_STATUS__A7_SW_RST__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_STATUS__A7_SW_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.A7_WARM_RST -  */
#define CLKC_RSTC_STATUS__A7_WARM_RST__SHIFT       4
#define CLKC_RSTC_STATUS__A7_WARM_RST__WIDTH       1
#define CLKC_RSTC_STATUS__A7_WARM_RST__MASK        0x00000010
#define CLKC_RSTC_STATUS__A7_WARM_RST__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_STATUS__A7_WARM_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.A7_WD_RST -  */
#define CLKC_RSTC_STATUS__A7_WD_RST__SHIFT       5
#define CLKC_RSTC_STATUS__A7_WD_RST__WIDTH       1
#define CLKC_RSTC_STATUS__A7_WD_RST__MASK        0x00000020
#define CLKC_RSTC_STATUS__A7_WD_RST__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_STATUS__A7_WD_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.nSRST -  */
#define CLKC_RSTC_STATUS__N_SRST__SHIFT      6
#define CLKC_RSTC_STATUS__N_SRST__WIDTH      1
#define CLKC_RSTC_STATUS__N_SRST__MASK       0x00000040
#define CLKC_RSTC_STATUS__N_SRST__INV_MASK   0xFFFFFFBF
#define CLKC_RSTC_STATUS__N_SRST__HW_DEFAULT 0x0

/* CLKC_RSTC_STATUS.A7_DBG_RST -  */
#define CLKC_RSTC_STATUS__A7_DBG_RST__SHIFT       7
#define CLKC_RSTC_STATUS__A7_DBG_RST__WIDTH       1
#define CLKC_RSTC_STATUS__A7_DBG_RST__MASK        0x00000080
#define CLKC_RSTC_STATUS__A7_DBG_RST__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_STATUS__A7_DBG_RST__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.SREFRESH -  */
#define CLKC_RSTC_STATUS__SREFRESH__SHIFT       8
#define CLKC_RSTC_STATUS__SREFRESH__WIDTH       1
#define CLKC_RSTC_STATUS__SREFRESH__MASK        0x00000100
#define CLKC_RSTC_STATUS__SREFRESH__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_STATUS__SREFRESH__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.TIMEOUT -  */
#define CLKC_RSTC_STATUS__TIMEOUT__SHIFT       9
#define CLKC_RSTC_STATUS__TIMEOUT__WIDTH       1
#define CLKC_RSTC_STATUS__TIMEOUT__MASK        0x00000200
#define CLKC_RSTC_STATUS__TIMEOUT__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_STATUS__TIMEOUT__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.PWRC_DRAM_HOLD -  */
#define CLKC_RSTC_STATUS__PWRC_DRAM_HOLD__SHIFT       10
#define CLKC_RSTC_STATUS__PWRC_DRAM_HOLD__WIDTH       1
#define CLKC_RSTC_STATUS__PWRC_DRAM_HOLD__MASK        0x00000400
#define CLKC_RSTC_STATUS__PWRC_DRAM_HOLD__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_STATUS__PWRC_DRAM_HOLD__HW_DEFAULT  0x0

/* CLKC_RSTC_STATUS.PWRC_PON_STATUS -  */
#define CLKC_RSTC_STATUS__PWRC_PON_STATUS__SHIFT       12
#define CLKC_RSTC_STATUS__PWRC_PON_STATUS__WIDTH       17
#define CLKC_RSTC_STATUS__PWRC_PON_STATUS__MASK        0x1FFFF000
#define CLKC_RSTC_STATUS__PWRC_PON_STATUS__INV_MASK    0xE0000FFF
#define CLKC_RSTC_STATUS__PWRC_PON_STATUS__HW_DEFAULT  0x0

/* RSTC staus clear register */
/* This register clears the status of RSTC_STATUS */
#define CLKC_RSTC_STATUS_CLR      0x18620318

/* CLKC_RSTC_STATUS_CLR.VALUE -  */
#define CLKC_RSTC_STATUS_CLR__VALUE__SHIFT       0
#define CLKC_RSTC_STATUS_CLR__VALUE__WIDTH       1
#define CLKC_RSTC_STATUS_CLR__VALUE__MASK        0x00000001
#define CLKC_RSTC_STATUS_CLR__VALUE__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_STATUS_CLR__VALUE__HW_DEFAULT  0x0

/* RSTC timer configuration */
/* This register controls the timer mechanism. 
 It protects the DRAM self-refresh mechanism if memory controller do not return acknowledge indication */
#define CLKC_RSTC_TIMER           0x1862031C

/* CLKC_RSTC_TIMER.VALUE -  */
#define CLKC_RSTC_TIMER__VALUE__SHIFT       0
#define CLKC_RSTC_TIMER__VALUE__WIDTH       31
#define CLKC_RSTC_TIMER__VALUE__MASK        0x7FFFFFFF
#define CLKC_RSTC_TIMER__VALUE__INV_MASK    0x80000000
#define CLKC_RSTC_TIMER__VALUE__HW_DEFAULT  0x0

/* CLKC_RSTC_TIMER.ENABLE -  */
#define CLKC_RSTC_TIMER__ENABLE__SHIFT       31
#define CLKC_RSTC_TIMER__ENABLE__WIDTH       1
#define CLKC_RSTC_TIMER__ENABLE__MASK        0x80000000
#define CLKC_RSTC_TIMER__ENABLE__INV_MASK    0x7FFFFFFF
#define CLKC_RSTC_TIMER__ENABLE__HW_DEFAULT  0x0

/* Set RSTC  CGUM     SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST0 status.
 */
#define CLKC_RSTC_UNIT_SW_RST0_SET 0x18620320

/* CLKC_RSTC_UNIT_SW_RST0_SET.PWM - PWM SW reset */
/* 0 : clear PWM SW reset */
/* 1 : set PWM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_SET__PWM__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST0_SET__PWM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_SET__PWM__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST0_SET__PWM__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST0_SET__PWM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST0_SET.THCGUM - THCGUM SW reset */
/* 0 : clear THCGUM SW reset */
/* 1 : set THCGUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_SET__THCGUM__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST0_SET__THCGUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_SET__THCGUM__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST0_SET__THCGUM__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST0_SET__THCGUM__HW_DEFAULT  0x1

/* Clear RSTC  CGUM     SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST0 status.
 */
#define CLKC_RSTC_UNIT_SW_RST0_CLR 0x18620324

/* CLKC_RSTC_UNIT_SW_RST0_CLR.PWM - PWM SW reset */
/* 0 : clear PWM SW reset */
/* 1 : set PWM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_CLR__PWM__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST0_CLR__PWM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_CLR__PWM__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST0_CLR__PWM__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST0_CLR__PWM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST0_CLR.THCGUM - THCGUM SW reset */
/* 0 : clear THCGUM SW reset */
/* 1 : set THCGUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_CLR__THCGUM__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST0_CLR__THCGUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_CLR__THCGUM__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST0_CLR__THCGUM__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST0_CLR__THCGUM__HW_DEFAULT  0x1

/* RSTC  CGUM     SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST0_STATUS 0x18620328

/* CLKC_RSTC_UNIT_SW_RST0_STATUS.PWM - PWM SW reset */
/* 0 : clear PWM SW reset */
/* 1 : set PWM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__PWM__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__PWM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__PWM__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__PWM__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__PWM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST0_STATUS.THCGUM - THCGUM SW reset */
/* 0 : clear THCGUM SW reset */
/* 1 : set THCGUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__THCGUM__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__THCGUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__THCGUM__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__THCGUM__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST0_STATUS__THCGUM__HW_DEFAULT  0x1

/* Set RSTC  AUDMSCM  SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST1 status.
 */
#define CLKC_RSTC_UNIT_SW_RST1_SET 0x1862032C

/* CLKC_RSTC_UNIT_SW_RST1_SET.CVD - CVD SW reset */
/* 0 : clear CVD SW reset */
/* 1 : set CVD SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__CVD__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST1_SET__CVD__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__CVD__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST1_SET__CVD__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST1_SET__CVD__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.TIMER - TIMER SW reset */
/* 0 : clear TIMER SW reset */
/* 1 : set TIMER SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__TIMER__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__TIMER__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__TIMER__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST1_SET__TIMER__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST1_SET__TIMER__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.PULSEC - PULSEC SW reset */
/* 0 : clear PULSEC SW reset */
/* 1 : set PULSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__PULSEC__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST1_SET__PULSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__PULSEC__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST1_SET__PULSEC__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST1_SET__PULSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.TSC - TSC SW reset */
/* 0 : clear TSC SW reset */
/* 1 : set TSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__TSC__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST1_SET__TSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__TSC__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST1_SET__TSC__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST1_SET__TSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.IOCTOP - IOCTOP SW reset */
/* 0 : clear IOCTOP SW reset */
/* 1 : set IOCTOP SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__IOCTOP__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST1_SET__IOCTOP__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__IOCTOP__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST1_SET__IOCTOP__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST1_SET__IOCTOP__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.RSC - RSC SW reset */
/* 0 : clear RSC SW reset */
/* 1 : set RSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__RSC__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST1_SET__RSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__RSC__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST1_SET__RSC__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST1_SET__RSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.DVM - DVM SW reset */
/* 0 : clear DVM SW reset */
/* 1 : set DVM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__DVM__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST1_SET__DVM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__DVM__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST1_SET__DVM__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST1_SET__DVM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.LVDS - LVDS SW reset */
/* 0 : clear LVDS SW reset */
/* 1 : set LVDS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__LVDS__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST1_SET__LVDS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__LVDS__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST1_SET__LVDS__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST1_SET__LVDS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.KAS - KAS SW reset */
/* 0 : clear KAS SW reset */
/* 1 : set KAS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__KAS__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST1_SET__KAS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__KAS__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST1_SET__KAS__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__KAS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.AC97 - AC97 SW reset */
/* 0 : clear AC97 SW reset */
/* 1 : set AC97 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__AC97__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST1_SET__AC97__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__AC97__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST1_SET__AC97__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__AC97__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.USP0 - USP0 SW reset */
/* 0 : clear USP0 SW reset */
/* 1 : set USP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.USP1 - USP1 SW reset */
/* 0 : clear USP1 SW reset */
/* 1 : set USP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.USP2 - USP2 SW reset */
/* 0 : clear USP2 SW reset */
/* 1 : set USP2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP2__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP2__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP2__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__USP2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.DMAC2 - DMAC2 SW reset */
/* 0 : clear DMAC2 SW reset */
/* 1 : set DMAC2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC2__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC2__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC2__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.DMAC3 - DMAC3 SW reset */
/* 0 : clear DMAC3 SW reset */
/* 1 : set DMAC3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC3__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC3__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC3__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__DMAC3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.AUDIO - AUDIO SW reset */
/* 0 : clear AUDIO SW reset */
/* 1 : set AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__AUDIO__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST1_SET__AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__AUDIO__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST1_SET__AUDIO__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.I2S1 - I2S1 SW reset */
/* 0 : clear I2S1 SW reset */
/* 1 : set I2S1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__I2S1__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST1_SET__I2S1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__I2S1__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST1_SET__I2S1__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__I2S1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.PMU_AUDIO - PMU_AUDIO SW reset */
/* 0 : clear PMU_AUDIO SW reset */
/* 1 : set PMU_AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__PMU_AUDIO__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST1_SET__PMU_AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__PMU_AUDIO__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST1_SET__PMU_AUDIO__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__PMU_AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_SET.THAUDMSCM - THAUDMSCM SW reset */
/* 0 : clear THAUDMSCM SW reset */
/* 1 : set THAUDMSCM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_SET__THAUDMSCM__SHIFT       18
#define CLKC_RSTC_UNIT_SW_RST1_SET__THAUDMSCM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_SET__THAUDMSCM__MASK        0x00040000
#define CLKC_RSTC_UNIT_SW_RST1_SET__THAUDMSCM__INV_MASK    0xFFFBFFFF
#define CLKC_RSTC_UNIT_SW_RST1_SET__THAUDMSCM__HW_DEFAULT  0x1

/* Clear RSTC  AUDMSCM  SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST1 status.
 */
#define CLKC_RSTC_UNIT_SW_RST1_CLR 0x18620330

/* CLKC_RSTC_UNIT_SW_RST1_CLR.CVD - CVD SW reset */
/* 0 : clear CVD SW reset */
/* 1 : set CVD SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__CVD__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST1_CLR__CVD__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__CVD__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST1_CLR__CVD__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST1_CLR__CVD__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.TIMER - TIMER SW reset */
/* 0 : clear TIMER SW reset */
/* 1 : set TIMER SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TIMER__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TIMER__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TIMER__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TIMER__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TIMER__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.PULSEC - PULSEC SW reset */
/* 0 : clear PULSEC SW reset */
/* 1 : set PULSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PULSEC__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PULSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PULSEC__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PULSEC__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PULSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.TSC - TSC SW reset */
/* 0 : clear TSC SW reset */
/* 1 : set TSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TSC__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TSC__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TSC__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST1_CLR__TSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.IOCTOP - IOCTOP SW reset */
/* 0 : clear IOCTOP SW reset */
/* 1 : set IOCTOP SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__IOCTOP__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST1_CLR__IOCTOP__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__IOCTOP__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST1_CLR__IOCTOP__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__IOCTOP__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.RSC - RSC SW reset */
/* 0 : clear RSC SW reset */
/* 1 : set RSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__RSC__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST1_CLR__RSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__RSC__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST1_CLR__RSC__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__RSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.DVM - DVM SW reset */
/* 0 : clear DVM SW reset */
/* 1 : set DVM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DVM__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DVM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DVM__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DVM__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DVM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.LVDS - LVDS SW reset */
/* 0 : clear LVDS SW reset */
/* 1 : set LVDS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__LVDS__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST1_CLR__LVDS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__LVDS__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST1_CLR__LVDS__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST1_CLR__LVDS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.KAS - KAS SW reset */
/* 0 : clear KAS SW reset */
/* 1 : set KAS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__KAS__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST1_CLR__KAS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__KAS__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST1_CLR__KAS__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__KAS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.AC97 - AC97 SW reset */
/* 0 : clear AC97 SW reset */
/* 1 : set AC97 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AC97__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AC97__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AC97__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AC97__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AC97__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.USP0 - USP0 SW reset */
/* 0 : clear USP0 SW reset */
/* 1 : set USP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.USP1 - USP1 SW reset */
/* 0 : clear USP1 SW reset */
/* 1 : set USP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.USP2 - USP2 SW reset */
/* 0 : clear USP2 SW reset */
/* 1 : set USP2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP2__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP2__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP2__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__USP2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.DMAC2 - DMAC2 SW reset */
/* 0 : clear DMAC2 SW reset */
/* 1 : set DMAC2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC2__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC2__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC2__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.DMAC3 - DMAC3 SW reset */
/* 0 : clear DMAC3 SW reset */
/* 1 : set DMAC3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC3__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC3__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC3__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__DMAC3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.AUDIO - AUDIO SW reset */
/* 0 : clear AUDIO SW reset */
/* 1 : set AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AUDIO__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AUDIO__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AUDIO__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.I2S1 - I2S1 SW reset */
/* 0 : clear I2S1 SW reset */
/* 1 : set I2S1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__I2S1__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST1_CLR__I2S1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__I2S1__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__I2S1__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__I2S1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.PMU_AUDIO - PMU_AUDIO SW reset */
/* 0 : clear PMU_AUDIO SW reset */
/* 1 : set PMU_AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PMU_AUDIO__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PMU_AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PMU_AUDIO__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PMU_AUDIO__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__PMU_AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_CLR.THAUDMSCM - THAUDMSCM SW reset */
/* 0 : clear THAUDMSCM SW reset */
/* 1 : set THAUDMSCM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_CLR__THAUDMSCM__SHIFT       18
#define CLKC_RSTC_UNIT_SW_RST1_CLR__THAUDMSCM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_CLR__THAUDMSCM__MASK        0x00040000
#define CLKC_RSTC_UNIT_SW_RST1_CLR__THAUDMSCM__INV_MASK    0xFFFBFFFF
#define CLKC_RSTC_UNIT_SW_RST1_CLR__THAUDMSCM__HW_DEFAULT  0x1

/* RSTC  AUDMSCM  SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS 0x18620334

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.CVD - CVD SW reset */
/* 0 : clear CVD SW reset */
/* 1 : set CVD SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__CVD__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__CVD__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__CVD__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__CVD__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__CVD__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.TIMER - TIMER SW reset */
/* 0 : clear TIMER SW reset */
/* 1 : set TIMER SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TIMER__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TIMER__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TIMER__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TIMER__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TIMER__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.PULSEC - PULSEC SW reset */
/* 0 : clear PULSEC SW reset */
/* 1 : set PULSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PULSEC__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PULSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PULSEC__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PULSEC__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PULSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.TSC - TSC SW reset */
/* 0 : clear TSC SW reset */
/* 1 : set TSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TSC__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TSC__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TSC__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__TSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.IOCTOP - IOCTOP SW reset */
/* 0 : clear IOCTOP SW reset */
/* 1 : set IOCTOP SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__IOCTOP__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__IOCTOP__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__IOCTOP__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__IOCTOP__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__IOCTOP__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.RSC - RSC SW reset */
/* 0 : clear RSC SW reset */
/* 1 : set RSC SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__RSC__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__RSC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__RSC__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__RSC__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__RSC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.DVM - DVM SW reset */
/* 0 : clear DVM SW reset */
/* 1 : set DVM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DVM__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DVM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DVM__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DVM__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DVM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.LVDS - LVDS SW reset */
/* 0 : clear LVDS SW reset */
/* 1 : set LVDS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__LVDS__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__LVDS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__LVDS__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__LVDS__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__LVDS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.KAS - KAS SW reset */
/* 0 : clear KAS SW reset */
/* 1 : set KAS SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__KAS__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__KAS__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__KAS__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__KAS__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__KAS__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.AC97 - AC97 SW reset */
/* 0 : clear AC97 SW reset */
/* 1 : set AC97 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AC97__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AC97__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AC97__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AC97__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AC97__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.USP0 - USP0 SW reset */
/* 0 : clear USP0 SW reset */
/* 1 : set USP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.USP1 - USP1 SW reset */
/* 0 : clear USP1 SW reset */
/* 1 : set USP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.USP2 - USP2 SW reset */
/* 0 : clear USP2 SW reset */
/* 1 : set USP2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP2__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP2__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP2__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__USP2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.DMAC2 - DMAC2 SW reset */
/* 0 : clear DMAC2 SW reset */
/* 1 : set DMAC2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC2__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC2__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC2__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.DMAC3 - DMAC3 SW reset */
/* 0 : clear DMAC3 SW reset */
/* 1 : set DMAC3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC3__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC3__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC3__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__DMAC3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.AUDIO - AUDIO SW reset */
/* 0 : clear AUDIO SW reset */
/* 1 : set AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AUDIO__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AUDIO__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AUDIO__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.I2S1 - I2S1 SW reset */
/* 0 : clear I2S1 SW reset */
/* 1 : set I2S1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__I2S1__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__I2S1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__I2S1__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__I2S1__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__I2S1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.PMU_AUDIO - PMU_AUDIO SW reset */
/* 0 : clear PMU_AUDIO SW reset */
/* 1 : set PMU_AUDIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PMU_AUDIO__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PMU_AUDIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PMU_AUDIO__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PMU_AUDIO__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__PMU_AUDIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST1_STATUS.THAUDMSCM - THAUDMSCM SW reset */
/* 0 : clear THAUDMSCM SW reset */
/* 1 : set THAUDMSCM SW reset */
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__THAUDMSCM__SHIFT       18
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__THAUDMSCM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__THAUDMSCM__MASK        0x00040000
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__THAUDMSCM__INV_MASK    0xFFFBFFFF
#define CLKC_RSTC_UNIT_SW_RST1_STATUS__THAUDMSCM__HW_DEFAULT  0x1

/* Set RSTC  VDIFM    SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST2 status.
 */
#define CLKC_RSTC_UNIT_SW_RST2_SET 0x18620338

/* CLKC_RSTC_UNIT_SW_RST2_SET.SYS2PCI - SYS2PCI SW reset */
/* 0 : clear SYS2PCI SW reset */
/* 1 : set SYS2PCI SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__SYS2PCI__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST2_SET__SYS2PCI__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__SYS2PCI__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST2_SET__SYS2PCI__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST2_SET__SYS2PCI__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.PCIARB - PCIARB SW reset */
/* 0 : clear PCIARB SW reset */
/* 1 : set PCIARB SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCIARB__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCIARB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCIARB__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCIARB__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCIARB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.PCICOPY - PCICOPY SW reset */
/* 0 : clear PCICOPY SW reset */
/* 1 : set PCICOPY SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCICOPY__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCICOPY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCICOPY__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCICOPY__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST2_SET__PCICOPY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.ROM - ROM SW reset */
/* 0 : clear ROM SW reset */
/* 1 : set ROM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__ROM__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST2_SET__ROM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__ROM__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST2_SET__ROM__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST2_SET__ROM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.SDIO23 - SDIO23 SW reset */
/* 0 : clear SDIO23 SW reset */
/* 1 : set SDIO23 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO23__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO23__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO23__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO23__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO23__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.SDIO45 - SDIO45 SW reset */
/* 0 : clear SDIO45 SW reset */
/* 1 : set SDIO45 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO45__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO45__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO45__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO45__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO45__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.SDIO67 - SDIO67 SW reset */
/* 0 : clear SDIO67 SW reset */
/* 1 : set SDIO67 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO67__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO67__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO67__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO67__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST2_SET__SDIO67__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.VIP1 - VIP1 SW reset */
/* 0 : clear VIP1 SW reset */
/* 1 : set VIP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__VIP1__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST2_SET__VIP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__VIP1__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST2_SET__VIP1__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST2_SET__VIP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.VPP0 - VPP0 SW reset */
/* 0 : clear VPP0 SW reset */
/* 1 : set VPP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP0__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP0__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP0__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.LCD0 - LCD0 SW reset */
/* 0 : clear LCD0 SW reset */
/* 1 : set LCD0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.VPP1 - VPP1 SW reset */
/* 0 : clear VPP1 SW reset */
/* 1 : set VPP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__VPP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.LCD1 - LCD1 SW reset */
/* 0 : clear LCD1 SW reset */
/* 1 : set LCD1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST2_SET__LCD1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.DCU - DCU SW reset */
/* 0 : clear DCU SW reset */
/* 1 : set DCU SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__DCU__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST2_SET__DCU__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__DCU__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST2_SET__DCU__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__DCU__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.GPIO - GPIO SW reset */
/* 0 : clear GPIO SW reset */
/* 1 : set GPIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__GPIO__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST2_SET__GPIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__GPIO__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST2_SET__GPIO__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__GPIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.IPC - IPC SW reset */
/* 0 : clear IPC SW reset */
/* 1 : set IPC SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__IPC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST2_SET__IPC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__IPC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST2_SET__IPC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__IPC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.DAPA_VDIFM - DAPA_VDIFM SW reset */
/* 0 : clear DAPA_VDIFM SW reset */
/* 1 : set DAPA_VDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__DAPA_VDIFM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST2_SET__DAPA_VDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__DAPA_VDIFM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST2_SET__DAPA_VDIFM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__DAPA_VDIFM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_SET.THVDIFM - THVDIFM SW reset */
/* 0 : clear THVDIFM SW reset */
/* 1 : set THVDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_SET__THVDIFM__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST2_SET__THVDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_SET__THVDIFM__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST2_SET__THVDIFM__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST2_SET__THVDIFM__HW_DEFAULT  0x1

/* Clear RSTC  VDIFM    SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST2 status.
 */
#define CLKC_RSTC_UNIT_SW_RST2_CLR 0x1862033C

/* CLKC_RSTC_UNIT_SW_RST2_CLR.SYS2PCI - SYS2PCI SW reset */
/* 0 : clear SYS2PCI SW reset */
/* 1 : set SYS2PCI SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SYS2PCI__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SYS2PCI__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SYS2PCI__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SYS2PCI__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SYS2PCI__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.PCIARB - PCIARB SW reset */
/* 0 : clear PCIARB SW reset */
/* 1 : set PCIARB SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCIARB__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCIARB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCIARB__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCIARB__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCIARB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.PCICOPY - PCICOPY SW reset */
/* 0 : clear PCICOPY SW reset */
/* 1 : set PCICOPY SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCICOPY__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCICOPY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCICOPY__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCICOPY__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST2_CLR__PCICOPY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.ROM - ROM SW reset */
/* 0 : clear ROM SW reset */
/* 1 : set ROM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__ROM__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST2_CLR__ROM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__ROM__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST2_CLR__ROM__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST2_CLR__ROM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.SDIO23 - SDIO23 SW reset */
/* 0 : clear SDIO23 SW reset */
/* 1 : set SDIO23 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO23__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO23__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO23__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO23__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO23__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.SDIO45 - SDIO45 SW reset */
/* 0 : clear SDIO45 SW reset */
/* 1 : set SDIO45 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO45__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO45__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO45__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO45__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO45__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.SDIO67 - SDIO67 SW reset */
/* 0 : clear SDIO67 SW reset */
/* 1 : set SDIO67 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO67__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO67__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO67__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO67__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__SDIO67__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.VIP1 - VIP1 SW reset */
/* 0 : clear VIP1 SW reset */
/* 1 : set VIP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VIP1__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VIP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VIP1__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VIP1__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VIP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.VPP0 - VPP0 SW reset */
/* 0 : clear VPP0 SW reset */
/* 1 : set VPP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP0__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP0__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP0__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.LCD0 - LCD0 SW reset */
/* 0 : clear LCD0 SW reset */
/* 1 : set LCD0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.VPP1 - VPP1 SW reset */
/* 0 : clear VPP1 SW reset */
/* 1 : set VPP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__VPP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.LCD1 - LCD1 SW reset */
/* 0 : clear LCD1 SW reset */
/* 1 : set LCD1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__LCD1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.DCU - DCU SW reset */
/* 0 : clear DCU SW reset */
/* 1 : set DCU SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DCU__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DCU__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DCU__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DCU__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DCU__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.GPIO - GPIO SW reset */
/* 0 : clear GPIO SW reset */
/* 1 : set GPIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__GPIO__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST2_CLR__GPIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__GPIO__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST2_CLR__GPIO__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__GPIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.IPC - IPC SW reset */
/* 0 : clear IPC SW reset */
/* 1 : set IPC SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__IPC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST2_CLR__IPC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__IPC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST2_CLR__IPC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__IPC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.DAPA_VDIFM - DAPA_VDIFM SW reset */
/* 0 : clear DAPA_VDIFM SW reset */
/* 1 : set DAPA_VDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DAPA_VDIFM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DAPA_VDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DAPA_VDIFM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DAPA_VDIFM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__DAPA_VDIFM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_CLR.THVDIFM - THVDIFM SW reset */
/* 0 : clear THVDIFM SW reset */
/* 1 : set THVDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_CLR__THVDIFM__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST2_CLR__THVDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_CLR__THVDIFM__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST2_CLR__THVDIFM__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST2_CLR__THVDIFM__HW_DEFAULT  0x1

/* RSTC  VDIFM    SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS 0x18620340

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.SYS2PCI - SYS2PCI SW reset */
/* 0 : clear SYS2PCI SW reset */
/* 1 : set SYS2PCI SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SYS2PCI__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SYS2PCI__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SYS2PCI__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SYS2PCI__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SYS2PCI__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.PCIARB - PCIARB SW reset */
/* 0 : clear PCIARB SW reset */
/* 1 : set PCIARB SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCIARB__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCIARB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCIARB__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCIARB__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCIARB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.PCICOPY - PCICOPY SW reset */
/* 0 : clear PCICOPY SW reset */
/* 1 : set PCICOPY SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCICOPY__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCICOPY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCICOPY__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCICOPY__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__PCICOPY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.ROM - ROM SW reset */
/* 0 : clear ROM SW reset */
/* 1 : set ROM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__ROM__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__ROM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__ROM__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__ROM__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__ROM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.SDIO23 - SDIO23 SW reset */
/* 0 : clear SDIO23 SW reset */
/* 1 : set SDIO23 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO23__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO23__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO23__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO23__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO23__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.SDIO45 - SDIO45 SW reset */
/* 0 : clear SDIO45 SW reset */
/* 1 : set SDIO45 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO45__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO45__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO45__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO45__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO45__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.SDIO67 - SDIO67 SW reset */
/* 0 : clear SDIO67 SW reset */
/* 1 : set SDIO67 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO67__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO67__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO67__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO67__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__SDIO67__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.VIP1 - VIP1 SW reset */
/* 0 : clear VIP1 SW reset */
/* 1 : set VIP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VIP1__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VIP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VIP1__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VIP1__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VIP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.VPP0 - VPP0 SW reset */
/* 0 : clear VPP0 SW reset */
/* 1 : set VPP0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP0__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP0__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP0__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.LCD0 - LCD0 SW reset */
/* 0 : clear LCD0 SW reset */
/* 1 : set LCD0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.VPP1 - VPP1 SW reset */
/* 0 : clear VPP1 SW reset */
/* 1 : set VPP1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__VPP1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.LCD1 - LCD1 SW reset */
/* 0 : clear LCD1 SW reset */
/* 1 : set LCD1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__LCD1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.DCU - DCU SW reset */
/* 0 : clear DCU SW reset */
/* 1 : set DCU SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DCU__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DCU__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DCU__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DCU__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DCU__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.GPIO - GPIO SW reset */
/* 0 : clear GPIO SW reset */
/* 1 : set GPIO SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__GPIO__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__GPIO__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__GPIO__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__GPIO__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__GPIO__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.IPC - IPC SW reset */
/* 0 : clear IPC SW reset */
/* 1 : set IPC SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__IPC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__IPC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__IPC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__IPC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__IPC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.DAPA_VDIFM - DAPA_VDIFM SW reset */
/* 0 : clear DAPA_VDIFM SW reset */
/* 1 : set DAPA_VDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DAPA_VDIFM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DAPA_VDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DAPA_VDIFM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DAPA_VDIFM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__DAPA_VDIFM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST2_STATUS.THVDIFM - THVDIFM SW reset */
/* 0 : clear THVDIFM SW reset */
/* 1 : set THVDIFM SW reset */
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__THVDIFM__SHIFT       16
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__THVDIFM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__THVDIFM__MASK        0x00010000
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__THVDIFM__INV_MASK    0xFFFEFFFF
#define CLKC_RSTC_UNIT_SW_RST2_STATUS__THVDIFM__HW_DEFAULT  0x1

/* Set RSTC  GNSSM    SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST3 status.
 */
#define CLKC_RSTC_UNIT_SW_RST3_SET 0x18620344

/* CLKC_RSTC_UNIT_SW_RST3_SET.RGMII - RGMII SW reset */
/* 0 : clear RGMII SW reset */
/* 1 : set RGMII SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__RGMII__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST3_SET__RGMII__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__RGMII__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST3_SET__RGMII__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST3_SET__RGMII__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.GMAC - GMAC SW reset */
/* 0 : clear GMAC SW reset */
/* 1 : set GMAC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__GMAC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__GMAC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__GMAC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST3_SET__GMAC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST3_SET__GMAC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART1 - UART1 SW reset */
/* 0 : clear UART1 SW reset */
/* 1 : set UART1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART1__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART1__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART1__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.DMAC0 - DMAC0 SW reset */
/* 0 : clear DMAC0 SW reset */
/* 1 : set DMAC0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__DMAC0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST3_SET__DMAC0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__DMAC0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST3_SET__DMAC0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST3_SET__DMAC0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART0 - UART0 SW reset */
/* 0 : clear UART0 SW reset */
/* 1 : set UART0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART0__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART0__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART0__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART2 - UART2 SW reset */
/* 0 : clear UART2 SW reset */
/* 1 : set UART2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART2__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART2__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART2__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART3 - UART3 SW reset */
/* 0 : clear UART3 SW reset */
/* 1 : set UART3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART3__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART3__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART3__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART4 - UART4 SW reset */
/* 0 : clear UART4 SW reset */
/* 1 : set UART4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART4__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART4__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART4__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.UART5 - UART5 SW reset */
/* 0 : clear UART5 SW reset */
/* 1 : set UART5 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART5__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART5__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART5__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART5__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__UART5__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.SPI1 - SPI1 SW reset */
/* 0 : clear SPI1 SW reset */
/* 1 : set SPI1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__SPI1__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST3_SET__SPI1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__SPI1__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST3_SET__SPI1__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__SPI1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.GNSS_SYS_M0 - GNSS_SYS_M0 SW reset */
/* 0 : clear GNSS_SYS_M0 SW reset */
/* 1 : set GNSS_SYS_M0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__GNSS_SYS_M0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST3_SET__GNSS_SYS_M0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__GNSS_SYS_M0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST3_SET__GNSS_SYS_M0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__GNSS_SYS_M0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.CANBUS1 - CANBUS1 SW reset */
/* 0 : clear CANBUS1 SW reset */
/* 1 : set CANBUS1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__CANBUS1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST3_SET__CANBUS1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__CANBUS1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST3_SET__CANBUS1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST3_SET__CANBUS1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.CCSEC - CCSEC SW reset */
/* 0 : clear CCSEC SW reset */
/* 1 : set CCSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCSEC__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCSEC__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCSEC__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.CCPUB - CCPUB SW reset */
/* 0 : clear CCPUB SW reset */
/* 1 : set CCPUB SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCPUB__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCPUB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCPUB__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCPUB__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__CCPUB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.DAPA_GNSSM - DAPA_GNSSM SW reset */
/* 0 : clear DAPA_GNSSM SW reset */
/* 1 : set DAPA_GNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__DAPA_GNSSM__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST3_SET__DAPA_GNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__DAPA_GNSSM__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST3_SET__DAPA_GNSSM__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__DAPA_GNSSM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_SET.THGNSSM - THGNSSM SW reset */
/* 0 : clear THGNSSM SW reset */
/* 1 : set THGNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_SET__THGNSSM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST3_SET__THGNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_SET__THGNSSM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST3_SET__THGNSSM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST3_SET__THGNSSM__HW_DEFAULT  0x1

/* Clear RSTC  GNSSM    SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST3 status.
 */
#define CLKC_RSTC_UNIT_SW_RST3_CLR 0x18620348

/* CLKC_RSTC_UNIT_SW_RST3_CLR.RGMII - RGMII SW reset */
/* 0 : clear RGMII SW reset */
/* 1 : set RGMII SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__RGMII__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST3_CLR__RGMII__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__RGMII__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST3_CLR__RGMII__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST3_CLR__RGMII__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.GMAC - GMAC SW reset */
/* 0 : clear GMAC SW reset */
/* 1 : set GMAC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GMAC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GMAC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GMAC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GMAC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GMAC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART1 - UART1 SW reset */
/* 0 : clear UART1 SW reset */
/* 1 : set UART1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART1__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART1__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART1__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.DMAC0 - DMAC0 SW reset */
/* 0 : clear DMAC0 SW reset */
/* 1 : set DMAC0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DMAC0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DMAC0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DMAC0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DMAC0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DMAC0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART0 - UART0 SW reset */
/* 0 : clear UART0 SW reset */
/* 1 : set UART0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART0__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART0__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART0__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART2 - UART2 SW reset */
/* 0 : clear UART2 SW reset */
/* 1 : set UART2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART2__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART2__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART2__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART3 - UART3 SW reset */
/* 0 : clear UART3 SW reset */
/* 1 : set UART3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART3__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART3__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART3__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART4 - UART4 SW reset */
/* 0 : clear UART4 SW reset */
/* 1 : set UART4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART4__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART4__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART4__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.UART5 - UART5 SW reset */
/* 0 : clear UART5 SW reset */
/* 1 : set UART5 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART5__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART5__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART5__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART5__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__UART5__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.SPI1 - SPI1 SW reset */
/* 0 : clear SPI1 SW reset */
/* 1 : set SPI1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__SPI1__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST3_CLR__SPI1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__SPI1__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST3_CLR__SPI1__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__SPI1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.GNSS_SYS_M0 - GNSS_SYS_M0 SW reset */
/* 0 : clear GNSS_SYS_M0 SW reset */
/* 1 : set GNSS_SYS_M0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GNSS_SYS_M0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GNSS_SYS_M0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GNSS_SYS_M0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GNSS_SYS_M0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__GNSS_SYS_M0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.CANBUS1 - CANBUS1 SW reset */
/* 0 : clear CANBUS1 SW reset */
/* 1 : set CANBUS1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CANBUS1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CANBUS1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CANBUS1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CANBUS1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CANBUS1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.CCSEC - CCSEC SW reset */
/* 0 : clear CCSEC SW reset */
/* 1 : set CCSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCSEC__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCSEC__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCSEC__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.CCPUB - CCPUB SW reset */
/* 0 : clear CCPUB SW reset */
/* 1 : set CCPUB SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCPUB__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCPUB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCPUB__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCPUB__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__CCPUB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.DAPA_GNSSM - DAPA_GNSSM SW reset */
/* 0 : clear DAPA_GNSSM SW reset */
/* 1 : set DAPA_GNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DAPA_GNSSM__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DAPA_GNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DAPA_GNSSM__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DAPA_GNSSM__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__DAPA_GNSSM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_CLR.THGNSSM - THGNSSM SW reset */
/* 0 : clear THGNSSM SW reset */
/* 1 : set THGNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_CLR__THGNSSM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST3_CLR__THGNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_CLR__THGNSSM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST3_CLR__THGNSSM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST3_CLR__THGNSSM__HW_DEFAULT  0x1

/* RSTC  GNSSM    SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS 0x1862034C

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.RGMII - RGMII SW reset */
/* 0 : clear RGMII SW reset */
/* 1 : set RGMII SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__RGMII__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__RGMII__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__RGMII__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__RGMII__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__RGMII__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.GMAC - GMAC SW reset */
/* 0 : clear GMAC SW reset */
/* 1 : set GMAC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GMAC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GMAC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GMAC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GMAC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GMAC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART1 - UART1 SW reset */
/* 0 : clear UART1 SW reset */
/* 1 : set UART1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART1__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART1__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART1__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.DMAC0 - DMAC0 SW reset */
/* 0 : clear DMAC0 SW reset */
/* 1 : set DMAC0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DMAC0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DMAC0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DMAC0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DMAC0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DMAC0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART0 - UART0 SW reset */
/* 0 : clear UART0 SW reset */
/* 1 : set UART0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART0__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART0__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART0__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART2 - UART2 SW reset */
/* 0 : clear UART2 SW reset */
/* 1 : set UART2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART2__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART2__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART2__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART3 - UART3 SW reset */
/* 0 : clear UART3 SW reset */
/* 1 : set UART3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART3__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART3__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART3__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART4 - UART4 SW reset */
/* 0 : clear UART4 SW reset */
/* 1 : set UART4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART4__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART4__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART4__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.UART5 - UART5 SW reset */
/* 0 : clear UART5 SW reset */
/* 1 : set UART5 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART5__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART5__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART5__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART5__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__UART5__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.SPI1 - SPI1 SW reset */
/* 0 : clear SPI1 SW reset */
/* 1 : set SPI1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__SPI1__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__SPI1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__SPI1__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__SPI1__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__SPI1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.GNSS_SYS_M0 - GNSS_SYS_M0 SW reset */
/* 0 : clear GNSS_SYS_M0 SW reset */
/* 1 : set GNSS_SYS_M0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GNSS_SYS_M0__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GNSS_SYS_M0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GNSS_SYS_M0__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GNSS_SYS_M0__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__GNSS_SYS_M0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.CANBUS1 - CANBUS1 SW reset */
/* 0 : clear CANBUS1 SW reset */
/* 1 : set CANBUS1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CANBUS1__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CANBUS1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CANBUS1__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CANBUS1__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CANBUS1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.CCSEC - CCSEC SW reset */
/* 0 : clear CCSEC SW reset */
/* 1 : set CCSEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCSEC__SHIFT       12
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCSEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCSEC__MASK        0x00001000
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCSEC__INV_MASK    0xFFFFEFFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCSEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.CCPUB - CCPUB SW reset */
/* 0 : clear CCPUB SW reset */
/* 1 : set CCPUB SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCPUB__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCPUB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCPUB__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCPUB__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__CCPUB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.DAPA_GNSSM - DAPA_GNSSM SW reset */
/* 0 : clear DAPA_GNSSM SW reset */
/* 1 : set DAPA_GNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DAPA_GNSSM__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DAPA_GNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DAPA_GNSSM__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DAPA_GNSSM__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__DAPA_GNSSM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST3_STATUS.THGNSSM - THGNSSM SW reset */
/* 0 : clear THGNSSM SW reset */
/* 1 : set THGNSSM SW reset */
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__THGNSSM__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__THGNSSM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__THGNSSM__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__THGNSSM__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST3_STATUS__THGNSSM__HW_DEFAULT  0x1

/* Set RSTC  MEDIAM   SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST4 status.
 */
#define CLKC_RSTC_UNIT_SW_RST4_SET 0x18620350

/* CLKC_RSTC_UNIT_SW_RST4_SET.VDEC - VDEC SW reset */
/* 0 : clear VDEC SW reset */
/* 1 : set VDEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__VDEC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST4_SET__VDEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__VDEC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST4_SET__VDEC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST4_SET__VDEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.JPENC - JPENC SW reset */
/* 0 : clear JPENC SW reset */
/* 1 : set JPENC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__JPENC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__JPENC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__JPENC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST4_SET__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST4_SET__JPENC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.G2D - G2D SW reset */
/* 0 : clear G2D SW reset */
/* 1 : set G2D SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__G2D__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST4_SET__G2D__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__G2D__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST4_SET__G2D__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST4_SET__G2D__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.I2C0 - I2C0 SW reset */
/* 0 : clear I2C0 SW reset */
/* 1 : set I2C0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.I2C1 - I2C1 SW reset */
/* 0 : clear I2C1 SW reset */
/* 1 : set I2C1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C1__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C1__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C1__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST4_SET__I2C1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.GPIO0 - GPIO0 SW reset */
/* 0 : clear GPIO0 SW reset */
/* 1 : set GPIO0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__GPIO0__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST4_SET__GPIO0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__GPIO0__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST4_SET__GPIO0__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST4_SET__GPIO0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.NAND - NAND SW reset */
/* 0 : clear NAND SW reset */
/* 1 : set NAND SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__NAND__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST4_SET__NAND__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__NAND__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST4_SET__NAND__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST4_SET__NAND__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.SDIO01 - SDIO01 SW reset */
/* 0 : clear SDIO01 SW reset */
/* 1 : set SDIO01 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__SDIO01__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST4_SET__SDIO01__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__SDIO01__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST4_SET__SDIO01__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST4_SET__SDIO01__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.SYS2PCI2 - SYS2PCI2 SW reset */
/* 0 : clear SYS2PCI2 SW reset */
/* 1 : set SYS2PCI2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__SYS2PCI2__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST4_SET__SYS2PCI2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__SYS2PCI2__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST4_SET__SYS2PCI2__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST4_SET__SYS2PCI2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.USB0 - USB0 SW reset */
/* 0 : clear USB0 SW reset */
/* 1 : set USB0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.USB1 - USB1 SW reset */
/* 0 : clear USB1 SW reset */
/* 1 : set USB1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST4_SET__USB1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_SET.THMEDIAM - THMEDIAM SW reset */
/* 0 : clear THMEDIAM SW reset */
/* 1 : set THMEDIAM SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_SET__THMEDIAM__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST4_SET__THMEDIAM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_SET__THMEDIAM__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST4_SET__THMEDIAM__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST4_SET__THMEDIAM__HW_DEFAULT  0x1

/* Clear RSTC  MEDIAM   SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST4 status.
 */
#define CLKC_RSTC_UNIT_SW_RST4_CLR 0x18620354

/* CLKC_RSTC_UNIT_SW_RST4_CLR.VDEC - VDEC SW reset */
/* 0 : clear VDEC SW reset */
/* 1 : set VDEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__VDEC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST4_CLR__VDEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__VDEC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST4_CLR__VDEC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST4_CLR__VDEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.JPENC - JPENC SW reset */
/* 0 : clear JPENC SW reset */
/* 1 : set JPENC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__JPENC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__JPENC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__JPENC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST4_CLR__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST4_CLR__JPENC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.G2D - G2D SW reset */
/* 0 : clear G2D SW reset */
/* 1 : set G2D SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__G2D__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST4_CLR__G2D__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__G2D__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST4_CLR__G2D__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST4_CLR__G2D__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.I2C0 - I2C0 SW reset */
/* 0 : clear I2C0 SW reset */
/* 1 : set I2C0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.I2C1 - I2C1 SW reset */
/* 0 : clear I2C1 SW reset */
/* 1 : set I2C1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C1__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C1__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C1__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__I2C1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.GPIO0 - GPIO0 SW reset */
/* 0 : clear GPIO0 SW reset */
/* 1 : set GPIO0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__GPIO0__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST4_CLR__GPIO0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__GPIO0__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST4_CLR__GPIO0__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__GPIO0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.NAND - NAND SW reset */
/* 0 : clear NAND SW reset */
/* 1 : set NAND SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__NAND__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST4_CLR__NAND__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__NAND__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST4_CLR__NAND__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__NAND__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.SDIO01 - SDIO01 SW reset */
/* 0 : clear SDIO01 SW reset */
/* 1 : set SDIO01 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SDIO01__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SDIO01__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SDIO01__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SDIO01__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SDIO01__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.SYS2PCI2 - SYS2PCI2 SW reset */
/* 0 : clear SYS2PCI2 SW reset */
/* 1 : set SYS2PCI2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SYS2PCI2__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SYS2PCI2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SYS2PCI2__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SYS2PCI2__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__SYS2PCI2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.USB0 - USB0 SW reset */
/* 0 : clear USB0 SW reset */
/* 1 : set USB0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.USB1 - USB1 SW reset */
/* 0 : clear USB1 SW reset */
/* 1 : set USB1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__USB1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_CLR.THMEDIAM - THMEDIAM SW reset */
/* 0 : clear THMEDIAM SW reset */
/* 1 : set THMEDIAM SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_CLR__THMEDIAM__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST4_CLR__THMEDIAM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_CLR__THMEDIAM__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST4_CLR__THMEDIAM__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST4_CLR__THMEDIAM__HW_DEFAULT  0x1

/* RSTC  MEDIAM   SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS 0x18620358

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.VDEC - VDEC SW reset */
/* 0 : clear VDEC SW reset */
/* 1 : set VDEC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__VDEC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__VDEC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__VDEC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__VDEC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__VDEC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.JPENC - JPENC SW reset */
/* 0 : clear JPENC SW reset */
/* 1 : set JPENC SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__JPENC__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__JPENC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__JPENC__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__JPENC__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__JPENC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.G2D - G2D SW reset */
/* 0 : clear G2D SW reset */
/* 1 : set G2D SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__G2D__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__G2D__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__G2D__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__G2D__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__G2D__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.I2C0 - I2C0 SW reset */
/* 0 : clear I2C0 SW reset */
/* 1 : set I2C0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C0__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C0__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C0__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.I2C1 - I2C1 SW reset */
/* 0 : clear I2C1 SW reset */
/* 1 : set I2C1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C1__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C1__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C1__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__I2C1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.GPIO0 - GPIO0 SW reset */
/* 0 : clear GPIO0 SW reset */
/* 1 : set GPIO0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__GPIO0__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__GPIO0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__GPIO0__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__GPIO0__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__GPIO0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.NAND - NAND SW reset */
/* 0 : clear NAND SW reset */
/* 1 : set NAND SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__NAND__SHIFT       6
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__NAND__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__NAND__MASK        0x00000040
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__NAND__INV_MASK    0xFFFFFFBF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__NAND__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.SDIO01 - SDIO01 SW reset */
/* 0 : clear SDIO01 SW reset */
/* 1 : set SDIO01 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SDIO01__SHIFT       7
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SDIO01__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SDIO01__MASK        0x00000080
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SDIO01__INV_MASK    0xFFFFFF7F
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SDIO01__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.SYS2PCI2 - SYS2PCI2 SW reset */
/* 0 : clear SYS2PCI2 SW reset */
/* 1 : set SYS2PCI2 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SYS2PCI2__SHIFT       8
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SYS2PCI2__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SYS2PCI2__MASK        0x00000100
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SYS2PCI2__INV_MASK    0xFFFFFEFF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__SYS2PCI2__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.USB0 - USB0 SW reset */
/* 0 : clear USB0 SW reset */
/* 1 : set USB0 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB0__SHIFT       9
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB0__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB0__MASK        0x00000200
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB0__INV_MASK    0xFFFFFDFF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB0__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.USB1 - USB1 SW reset */
/* 0 : clear USB1 SW reset */
/* 1 : set USB1 SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB1__SHIFT       10
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB1__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB1__MASK        0x00000400
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB1__INV_MASK    0xFFFFFBFF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__USB1__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST4_STATUS.THMEDIAM - THMEDIAM SW reset */
/* 0 : clear THMEDIAM SW reset */
/* 1 : set THMEDIAM SW reset */
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__THMEDIAM__SHIFT       11
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__THMEDIAM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__THMEDIAM__MASK        0x00000800
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__THMEDIAM__INV_MASK    0xFFFFF7FF
#define CLKC_RSTC_UNIT_SW_RST4_STATUS__THMEDIAM__HW_DEFAULT  0x1

/* Set RSTC  DDRM     SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST5 status.
 */
#define CLKC_RSTC_UNIT_SW_RST5_SET 0x1862035C

/* CLKC_RSTC_UNIT_SW_RST5_SET.MEMC_DDRPHY - MEMC_DDRPHY SW reset */
/* 0 : clear MEMC_DDRPHY SW reset */
/* 1 : set MEMC_DDRPHY SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_DDRPHY__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_DDRPHY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_DDRPHY__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_DDRPHY__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_DDRPHY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_SET.MEMC_UPCTL - MEMC_UPCTL SW reset */
/* 0 : clear MEMC_UPCTL SW reset */
/* 1 : set MEMC_UPCTL SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_UPCTL__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_UPCTL__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_UPCTL__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_UPCTL__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_UPCTL__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_SET.DAPA_MEM - DAPA_MEM SW reset */
/* 0 : clear DAPA_MEM SW reset */
/* 1 : set DAPA_MEM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_SET__DAPA_MEM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST5_SET__DAPA_MEM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__DAPA_MEM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST5_SET__DAPA_MEM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST5_SET__DAPA_MEM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_SET.MEMC_MEMDIV - MEMC_MEMDIV SW reset */
/* 0 : clear MEMC_MEMDIV SW reset */
/* 1 : set MEMC_MEMDIV SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_MEMDIV__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_MEMDIV__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_MEMDIV__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_MEMDIV__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST5_SET__MEMC_MEMDIV__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_SET.THDDRM - THDDRM SW reset */
/* 0 : clear THDDRM SW reset */
/* 1 : set THDDRM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_SET__THDDRM__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST5_SET__THDDRM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_SET__THDDRM__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST5_SET__THDDRM__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST5_SET__THDDRM__HW_DEFAULT  0x1

/* Clear RSTC  DDRM     SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST5 status.
 */
#define CLKC_RSTC_UNIT_SW_RST5_CLR 0x18620360

/* CLKC_RSTC_UNIT_SW_RST5_CLR.MEMC_DDRPHY - MEMC_DDRPHY SW reset */
/* 0 : clear MEMC_DDRPHY SW reset */
/* 1 : set MEMC_DDRPHY SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_DDRPHY__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_DDRPHY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_DDRPHY__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_DDRPHY__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_DDRPHY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_CLR.MEMC_UPCTL - MEMC_UPCTL SW reset */
/* 0 : clear MEMC_UPCTL SW reset */
/* 1 : set MEMC_UPCTL SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_UPCTL__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_UPCTL__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_UPCTL__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_UPCTL__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_UPCTL__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_CLR.DAPA_MEM - DAPA_MEM SW reset */
/* 0 : clear DAPA_MEM SW reset */
/* 1 : set DAPA_MEM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_CLR__DAPA_MEM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST5_CLR__DAPA_MEM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__DAPA_MEM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST5_CLR__DAPA_MEM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST5_CLR__DAPA_MEM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_CLR.MEMC_MEMDIV - MEMC_MEMDIV SW reset */
/* 0 : clear MEMC_MEMDIV SW reset */
/* 1 : set MEMC_MEMDIV SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_MEMDIV__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_MEMDIV__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_MEMDIV__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_MEMDIV__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST5_CLR__MEMC_MEMDIV__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_CLR.THDDRM - THDDRM SW reset */
/* 0 : clear THDDRM SW reset */
/* 1 : set THDDRM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_CLR__THDDRM__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST5_CLR__THDDRM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_CLR__THDDRM__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST5_CLR__THDDRM__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST5_CLR__THDDRM__HW_DEFAULT  0x1

/* RSTC  DDRM     SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS 0x18620364

/* CLKC_RSTC_UNIT_SW_RST5_STATUS.MEMC_DDRPHY - MEMC_DDRPHY SW reset */
/* 0 : clear MEMC_DDRPHY SW reset */
/* 1 : set MEMC_DDRPHY SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_DDRPHY__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_DDRPHY__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_DDRPHY__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_DDRPHY__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_DDRPHY__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_STATUS.MEMC_UPCTL - MEMC_UPCTL SW reset */
/* 0 : clear MEMC_UPCTL SW reset */
/* 1 : set MEMC_UPCTL SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_UPCTL__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_UPCTL__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_UPCTL__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_UPCTL__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_UPCTL__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_STATUS.DAPA_MEM - DAPA_MEM SW reset */
/* 0 : clear DAPA_MEM SW reset */
/* 1 : set DAPA_MEM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__DAPA_MEM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__DAPA_MEM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__DAPA_MEM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__DAPA_MEM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__DAPA_MEM__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_STATUS.MEMC_MEMDIV - MEMC_MEMDIV SW reset */
/* 0 : clear MEMC_MEMDIV SW reset */
/* 1 : set MEMC_MEMDIV SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_MEMDIV__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_MEMDIV__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_MEMDIV__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_MEMDIV__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__MEMC_MEMDIV__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST5_STATUS.THDDRM - THDDRM SW reset */
/* 0 : clear THDDRM SW reset */
/* 1 : set THDDRM SW reset */
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__THDDRM__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__THDDRM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__THDDRM__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__THDDRM__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST5_STATUS__THDDRM__HW_DEFAULT  0x1

/* Set RSTC  CPUM     SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST6 status.
 */
#define CLKC_RSTC_UNIT_SW_RST6_SET 0x18620368

/* CLKC_RSTC_UNIT_SW_RST6_SET.CORESIGHT - CORESIGHT SW reset */
/* 0 : clear CORESIGHT SW reset */
/* 1 : set CORESIGHT SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_SET__CORESIGHT__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST6_SET__CORESIGHT__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_SET__CORESIGHT__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST6_SET__CORESIGHT__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST6_SET__CORESIGHT__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_SET.INTC - INTC SW reset */
/* 0 : clear INTC SW reset */
/* 1 : set INTC SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_SET__INTC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST6_SET__INTC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_SET__INTC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST6_SET__INTC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST6_SET__INTC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_SET.CPUIF - CPUIF SW reset */
/* 0 : clear CPUIF SW reset */
/* 1 : set CPUIF SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_SET__CPUIF__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST6_SET__CPUIF__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_SET__CPUIF__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST6_SET__CPUIF__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST6_SET__CPUIF__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_SET.THCPUM - THCPUM SW reset */
/* 0 : clear THCPUM SW reset */
/* 1 : set THCPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_SET__THCPUM__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST6_SET__THCPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_SET__THCPUM__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST6_SET__THCPUM__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST6_SET__THCPUM__HW_DEFAULT  0x1

/* Clear RSTC  CPUM     SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST6 status.
 */
#define CLKC_RSTC_UNIT_SW_RST6_CLR 0x1862036C

/* CLKC_RSTC_UNIT_SW_RST6_CLR.CORESIGHT - CORESIGHT SW reset */
/* 0 : clear CORESIGHT SW reset */
/* 1 : set CORESIGHT SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CORESIGHT__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CORESIGHT__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CORESIGHT__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CORESIGHT__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CORESIGHT__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_CLR.INTC - INTC SW reset */
/* 0 : clear INTC SW reset */
/* 1 : set INTC SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_CLR__INTC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST6_CLR__INTC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_CLR__INTC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST6_CLR__INTC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST6_CLR__INTC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_CLR.CPUIF - CPUIF SW reset */
/* 0 : clear CPUIF SW reset */
/* 1 : set CPUIF SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CPUIF__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CPUIF__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CPUIF__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CPUIF__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST6_CLR__CPUIF__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_CLR.THCPUM - THCPUM SW reset */
/* 0 : clear THCPUM SW reset */
/* 1 : set THCPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_CLR__THCPUM__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST6_CLR__THCPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_CLR__THCPUM__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST6_CLR__THCPUM__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST6_CLR__THCPUM__HW_DEFAULT  0x1

/* RSTC  CPUM     SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST6_STATUS 0x18620370

/* CLKC_RSTC_UNIT_SW_RST6_STATUS.CORESIGHT - CORESIGHT SW reset */
/* 0 : clear CORESIGHT SW reset */
/* 1 : set CORESIGHT SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CORESIGHT__SHIFT       13
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CORESIGHT__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CORESIGHT__MASK        0x00002000
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CORESIGHT__INV_MASK    0xFFFFDFFF
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CORESIGHT__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_STATUS.INTC - INTC SW reset */
/* 0 : clear INTC SW reset */
/* 1 : set INTC SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__INTC__SHIFT       14
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__INTC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__INTC__MASK        0x00004000
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__INTC__INV_MASK    0xFFFFBFFF
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__INTC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_STATUS.CPUIF - CPUIF SW reset */
/* 0 : clear CPUIF SW reset */
/* 1 : set CPUIF SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CPUIF__SHIFT       15
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CPUIF__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CPUIF__MASK        0x00008000
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CPUIF__INV_MASK    0xFFFF7FFF
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__CPUIF__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST6_STATUS.THCPUM - THCPUM SW reset */
/* 0 : clear THCPUM SW reset */
/* 1 : set THCPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__THCPUM__SHIFT       17
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__THCPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__THCPUM__MASK        0x00020000
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__THCPUM__INV_MASK    0xFFFDFFFF
#define CLKC_RSTC_UNIT_SW_RST6_STATUS__THCPUM__HW_DEFAULT  0x1

/* Set RSTC  GPUM     SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST7 status.
 */
#define CLKC_RSTC_UNIT_SW_RST7_SET 0x18620374

/* CLKC_RSTC_UNIT_SW_RST7_SET.GRAPHIC - GRAPHIC SW reset */
/* 0 : clear GRAPHIC SW reset */
/* 1 : set GRAPHIC SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_SET__GRAPHIC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST7_SET__GRAPHIC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_SET__GRAPHIC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST7_SET__GRAPHIC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST7_SET__GRAPHIC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_SET.VSS_SDR - VSS_SDR SW reset */
/* 0 : clear VSS_SDR SW reset */
/* 1 : set VSS_SDR SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_SET__VSS_SDR__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST7_SET__VSS_SDR__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_SET__VSS_SDR__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST7_SET__VSS_SDR__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST7_SET__VSS_SDR__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_SET.THGPUM - THGPUM SW reset */
/* 0 : clear THGPUM SW reset */
/* 1 : set THGPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_SET__THGPUM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST7_SET__THGPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_SET__THGPUM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST7_SET__THGPUM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST7_SET__THGPUM__HW_DEFAULT  0x1

/* Clear RSTC  GPUM     SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST7 status.
 */
#define CLKC_RSTC_UNIT_SW_RST7_CLR 0x18620378

/* CLKC_RSTC_UNIT_SW_RST7_CLR.GRAPHIC - GRAPHIC SW reset */
/* 0 : clear GRAPHIC SW reset */
/* 1 : set GRAPHIC SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_CLR__GRAPHIC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST7_CLR__GRAPHIC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_CLR__GRAPHIC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST7_CLR__GRAPHIC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST7_CLR__GRAPHIC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_CLR.VSS_SDR - VSS_SDR SW reset */
/* 0 : clear VSS_SDR SW reset */
/* 1 : set VSS_SDR SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_CLR__VSS_SDR__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST7_CLR__VSS_SDR__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_CLR__VSS_SDR__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST7_CLR__VSS_SDR__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST7_CLR__VSS_SDR__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_CLR.THGPUM - THGPUM SW reset */
/* 0 : clear THGPUM SW reset */
/* 1 : set THGPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_CLR__THGPUM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST7_CLR__THGPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_CLR__THGPUM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST7_CLR__THGPUM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST7_CLR__THGPUM__HW_DEFAULT  0x1

/* RSTC  GPUM     SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST7_STATUS 0x1862037C

/* CLKC_RSTC_UNIT_SW_RST7_STATUS.GRAPHIC - GRAPHIC SW reset */
/* 0 : clear GRAPHIC SW reset */
/* 1 : set GRAPHIC SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__GRAPHIC__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__GRAPHIC__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__GRAPHIC__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__GRAPHIC__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__GRAPHIC__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_STATUS.VSS_SDR - VSS_SDR SW reset */
/* 0 : clear VSS_SDR SW reset */
/* 1 : set VSS_SDR SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__VSS_SDR__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__VSS_SDR__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__VSS_SDR__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__VSS_SDR__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__VSS_SDR__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST7_STATUS.THGPUM - THGPUM SW reset */
/* 0 : clear THGPUM SW reset */
/* 1 : set THGPUM SW reset */
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__THGPUM__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__THGPUM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__THGPUM__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__THGPUM__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST7_STATUS__THGPUM__HW_DEFAULT  0x1

/* Set RSTC  BTM      SW reset unit (de-assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST8 status.
 */
#define CLKC_RSTC_UNIT_SW_RST8_SET 0x18620380

/* CLKC_RSTC_UNIT_SW_RST8_SET.A7CA - A7CA SW reset */
/* 0 : clear A7CA SW reset */
/* 1 : set A7CA SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_SET.DMAC4 - DMAC4 SW reset */
/* 0 : clear DMAC4 SW reset */
/* 1 : set DMAC4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__DMAC4__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__DMAC4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__DMAC4__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST8_SET__DMAC4__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST8_SET__DMAC4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_SET.UART6 - UART6 SW reset */
/* 0 : clear UART6 SW reset */
/* 1 : set UART6 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__UART6__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST8_SET__UART6__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__UART6__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST8_SET__UART6__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST8_SET__UART6__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_SET.USP3 - USP3 SW reset */
/* 0 : clear USP3 SW reset */
/* 1 : set USP3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__USP3__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST8_SET__USP3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__USP3__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST8_SET__USP3__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST8_SET__USP3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_SET.A7CA_APB - A7CA_APB SW reset */
/* 0 : clear A7CA_APB SW reset */
/* 1 : set A7CA_APB SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA_APB__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA_APB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA_APB__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA_APB__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST8_SET__A7CA_APB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_SET.THBTM - THBTM SW reset */
/* 0 : clear THBTM SW reset */
/* 1 : set THBTM SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_SET__THBTM__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST8_SET__THBTM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_SET__THBTM__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST8_SET__THBTM__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST8_SET__THBTM__HW_DEFAULT  0x1

/* Clear RSTC  BTM      SW reset unit (assert reset) */
/* Reading this registers returns the RSTC_UNIT_SW_RST8 status.
 */
#define CLKC_RSTC_UNIT_SW_RST8_CLR 0x18620384

/* CLKC_RSTC_UNIT_SW_RST8_CLR.A7CA - A7CA SW reset */
/* 0 : clear A7CA SW reset */
/* 1 : set A7CA SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_CLR.DMAC4 - DMAC4 SW reset */
/* 0 : clear DMAC4 SW reset */
/* 1 : set DMAC4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__DMAC4__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__DMAC4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__DMAC4__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST8_CLR__DMAC4__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST8_CLR__DMAC4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_CLR.UART6 - UART6 SW reset */
/* 0 : clear UART6 SW reset */
/* 1 : set UART6 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__UART6__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST8_CLR__UART6__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__UART6__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST8_CLR__UART6__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST8_CLR__UART6__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_CLR.USP3 - USP3 SW reset */
/* 0 : clear USP3 SW reset */
/* 1 : set USP3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__USP3__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST8_CLR__USP3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__USP3__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST8_CLR__USP3__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST8_CLR__USP3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_CLR.A7CA_APB - A7CA_APB SW reset */
/* 0 : clear A7CA_APB SW reset */
/* 1 : set A7CA_APB SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA_APB__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA_APB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA_APB__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA_APB__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST8_CLR__A7CA_APB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_CLR.THBTM - THBTM SW reset */
/* 0 : clear THBTM SW reset */
/* 1 : set THBTM SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_CLR__THBTM__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST8_CLR__THBTM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_CLR__THBTM__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST8_CLR__THBTM__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST8_CLR__THBTM__HW_DEFAULT  0x1

/* RSTC  BTM      SW reset status unit */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS 0x18620388

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.A7CA - A7CA SW reset */
/* 0 : clear A7CA SW reset */
/* 1 : set A7CA SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA__SHIFT       0
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA__MASK        0x00000001
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA__INV_MASK    0xFFFFFFFE
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.DMAC4 - DMAC4 SW reset */
/* 0 : clear DMAC4 SW reset */
/* 1 : set DMAC4 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__DMAC4__SHIFT       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__DMAC4__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__DMAC4__MASK        0x00000002
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__DMAC4__INV_MASK    0xFFFFFFFD
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__DMAC4__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.UART6 - UART6 SW reset */
/* 0 : clear UART6 SW reset */
/* 1 : set UART6 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__UART6__SHIFT       2
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__UART6__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__UART6__MASK        0x00000004
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__UART6__INV_MASK    0xFFFFFFFB
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__UART6__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.USP3 - USP3 SW reset */
/* 0 : clear USP3 SW reset */
/* 1 : set USP3 SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__USP3__SHIFT       3
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__USP3__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__USP3__MASK        0x00000008
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__USP3__INV_MASK    0xFFFFFFF7
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__USP3__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.A7CA_APB - A7CA_APB SW reset */
/* 0 : clear A7CA_APB SW reset */
/* 1 : set A7CA_APB SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA_APB__SHIFT       4
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA_APB__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA_APB__MASK        0x00000010
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA_APB__INV_MASK    0xFFFFFFEF
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__A7CA_APB__HW_DEFAULT  0x1

/* CLKC_RSTC_UNIT_SW_RST8_STATUS.THBTM - THBTM SW reset */
/* 0 : clear THBTM SW reset */
/* 1 : set THBTM SW reset */
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__THBTM__SHIFT       5
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__THBTM__WIDTH       1
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__THBTM__MASK        0x00000020
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__THBTM__INV_MASK    0xFFFFFFDF
#define CLKC_RSTC_UNIT_SW_RST8_STATUS__THBTM__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN1_SET     0x186204A0

/* CLKC_LEAF_CLK_EN1_SET.cvd_io_clken - cvd_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable cvd_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__CVD_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN1_SET__CVD_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__CVD_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN1_SET__CVD_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN1_SET__CVD_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.timer_io_clken - timer_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable timer_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.pulsec_io_clken - pulsec_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pulsec_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__PULSEC_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN1_SET__PULSEC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__PULSEC_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN1_SET__PULSEC_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN1_SET__PULSEC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.tsc_io_clken - tsc_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable tsc_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__TSC_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN1_SET__TSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__TSC_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN1_SET__TSC_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN1_SET__TSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.ioctop_io_clken - ioctop_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable ioctop_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__IOCTOP_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN1_SET__IOCTOP_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__IOCTOP_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN1_SET__IOCTOP_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN1_SET__IOCTOP_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.rsc_io_clken - rsc_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable rsc_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__RSC_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN1_SET__RSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__RSC_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN1_SET__RSC_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN1_SET__RSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.dvm_xin_clken - dvm_xin_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dvm_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__DVM_XIN_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN1_SET__DVM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__DVM_XIN_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN1_SET__DVM_XIN_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN1_SET__DVM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.lvds_xin_clken - lvds_xin_clk clock enable */
/* Root clock XIN_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable lvds_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__LVDS_XIN_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN1_SET__LVDS_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__LVDS_XIN_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN1_SET__LVDS_XIN_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN1_SET__LVDS_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.kas_kas_clken - kas_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable kas_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__KAS_KAS_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN1_SET__KAS_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__KAS_KAS_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN1_SET__KAS_KAS_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN1_SET__KAS_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.ac97_kas_clken - ac97_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable ac97_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__AC97_KAS_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN1_SET__AC97_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__AC97_KAS_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN1_SET__AC97_KAS_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN1_SET__AC97_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.usp0_kas_clken - usp0_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usp0_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__USP0_KAS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN1_SET__USP0_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__USP0_KAS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN1_SET__USP0_KAS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN1_SET__USP0_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.usp1_kas_clken - usp1_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usp1_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__USP1_KAS_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN1_SET__USP1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__USP1_KAS_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN1_SET__USP1_KAS_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN1_SET__USP1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.usp2_kas_clken - usp2_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usp2_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__USP2_KAS_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN1_SET__USP2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__USP2_KAS_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN1_SET__USP2_KAS_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN1_SET__USP2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.dmac2_kas_clken - dmac2_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dmac2_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__DMAC2_KAS_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN1_SET__DMAC2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__DMAC2_KAS_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN1_SET__DMAC2_KAS_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN1_SET__DMAC2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.dmac3_kas_clken - dmac3_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dmac3_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__DMAC3_KAS_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN1_SET__DMAC3_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__DMAC3_KAS_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN1_SET__DMAC3_KAS_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN1_SET__DMAC3_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.audio_if_kas_clken - audio_if_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable audio_if_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__AUDIO_IF_KAS_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN1_SET__AUDIO_IF_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__AUDIO_IF_KAS_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN1_SET__AUDIO_IF_KAS_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN1_SET__AUDIO_IF_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.i2s1_kas_clken - i2s1_kas_clk clock enable */
/* Root clock KAS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable i2s1_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__I2S1_KAS_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN1_SET__I2S1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__I2S1_KAS_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN1_SET__I2S1_KAS_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN1_SET__I2S1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.tsc_xin_clken - tsc_xin_clk clock enable */
/* Root clock XIN_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable tsc_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__TSC_XIN_CLKEN__SHIFT       21
#define CLKC_LEAF_CLK_EN1_SET__TSC_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__TSC_XIN_CLKEN__MASK        0x00200000
#define CLKC_LEAF_CLK_EN1_SET__TSC_XIN_CLKEN__INV_MASK    0xFFDFFFFF
#define CLKC_LEAF_CLK_EN1_SET__TSC_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_SET.thaudmscm_io_clken - thaudmscm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thaudmscm_io_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__THAUDMSCM_IO_CLKEN__SHIFT       22
#define CLKC_LEAF_CLK_EN1_SET__THAUDMSCM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__THAUDMSCM_IO_CLKEN__MASK        0x00400000
#define CLKC_LEAF_CLK_EN1_SET__THAUDMSCM_IO_CLKEN__INV_MASK    0xFFBFFFFF
#define CLKC_LEAF_CLK_EN1_SET__THAUDMSCM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_SET.analogtest_xin_clken - analogtest_xin_clk clock enable */
/* Root clock XIN_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable analogtest_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_SET__ANALOGTEST_XIN_CLKEN__SHIFT       23
#define CLKC_LEAF_CLK_EN1_SET__ANALOGTEST_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_SET__ANALOGTEST_XIN_CLKEN__MASK        0x00800000
#define CLKC_LEAF_CLK_EN1_SET__ANALOGTEST_XIN_CLKEN__INV_MASK    0xFF7FFFFF
#define CLKC_LEAF_CLK_EN1_SET__ANALOGTEST_XIN_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN1_CLR     0x186204A4

/* CLKC_LEAF_CLK_EN1_CLR.cvd_io_clken - cvd_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable cvd_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__CVD_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN1_CLR__CVD_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__CVD_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN1_CLR__CVD_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN1_CLR__CVD_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.timer_io_clken - timer_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable timer_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__TIMER_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN1_CLR__TIMER_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__TIMER_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN1_CLR__TIMER_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN1_CLR__TIMER_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.pulsec_io_clken - pulsec_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pulsec_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__PULSEC_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN1_CLR__PULSEC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__PULSEC_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN1_CLR__PULSEC_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN1_CLR__PULSEC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.tsc_io_clken - tsc_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable tsc_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__TSC_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN1_CLR__TSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__TSC_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN1_CLR__TSC_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN1_CLR__TSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.ioctop_io_clken - ioctop_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable ioctop_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__IOCTOP_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN1_CLR__IOCTOP_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__IOCTOP_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN1_CLR__IOCTOP_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN1_CLR__IOCTOP_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.rsc_io_clken - rsc_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable rsc_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__RSC_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN1_CLR__RSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__RSC_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN1_CLR__RSC_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN1_CLR__RSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.dvm_xin_clken - dvm_xin_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dvm_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__DVM_XIN_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN1_CLR__DVM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__DVM_XIN_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN1_CLR__DVM_XIN_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN1_CLR__DVM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.lvds_xin_clken - lvds_xin_clk clock disable */
/* 0 : No effect */
/* 1 : Disable lvds_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__LVDS_XIN_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN1_CLR__LVDS_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__LVDS_XIN_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN1_CLR__LVDS_XIN_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN1_CLR__LVDS_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.kas_kas_clken - kas_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable kas_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__KAS_KAS_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN1_CLR__KAS_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__KAS_KAS_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN1_CLR__KAS_KAS_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN1_CLR__KAS_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.ac97_kas_clken - ac97_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable ac97_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__AC97_KAS_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN1_CLR__AC97_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__AC97_KAS_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN1_CLR__AC97_KAS_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN1_CLR__AC97_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.usp0_kas_clken - usp0_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usp0_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__USP0_KAS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN1_CLR__USP0_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__USP0_KAS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN1_CLR__USP0_KAS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN1_CLR__USP0_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.usp1_kas_clken - usp1_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usp1_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__USP1_KAS_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN1_CLR__USP1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__USP1_KAS_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN1_CLR__USP1_KAS_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN1_CLR__USP1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.usp2_kas_clken - usp2_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usp2_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__USP2_KAS_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN1_CLR__USP2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__USP2_KAS_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN1_CLR__USP2_KAS_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN1_CLR__USP2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.dmac2_kas_clken - dmac2_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dmac2_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__DMAC2_KAS_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN1_CLR__DMAC2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__DMAC2_KAS_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN1_CLR__DMAC2_KAS_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN1_CLR__DMAC2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.dmac3_kas_clken - dmac3_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dmac3_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__DMAC3_KAS_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN1_CLR__DMAC3_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__DMAC3_KAS_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN1_CLR__DMAC3_KAS_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN1_CLR__DMAC3_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.audio_if_kas_clken - audio_if_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable audio_if_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__AUDIO_IF_KAS_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN1_CLR__AUDIO_IF_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__AUDIO_IF_KAS_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN1_CLR__AUDIO_IF_KAS_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN1_CLR__AUDIO_IF_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.i2s1_kas_clken - i2s1_kas_clk clock disable */
/* 0 : No effect */
/* 1 : Disable i2s1_kas_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__I2S1_KAS_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN1_CLR__I2S1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__I2S1_KAS_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN1_CLR__I2S1_KAS_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN1_CLR__I2S1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.tsc_xin_clken - tsc_xin_clk clock disable */
/* 0 : No effect */
/* 1 : Disable tsc_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__TSC_XIN_CLKEN__SHIFT       21
#define CLKC_LEAF_CLK_EN1_CLR__TSC_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__TSC_XIN_CLKEN__MASK        0x00200000
#define CLKC_LEAF_CLK_EN1_CLR__TSC_XIN_CLKEN__INV_MASK    0xFFDFFFFF
#define CLKC_LEAF_CLK_EN1_CLR__TSC_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_CLR.thaudmscm_io_clken - thaudmscm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thaudmscm_io_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__THAUDMSCM_IO_CLKEN__SHIFT       22
#define CLKC_LEAF_CLK_EN1_CLR__THAUDMSCM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__THAUDMSCM_IO_CLKEN__MASK        0x00400000
#define CLKC_LEAF_CLK_EN1_CLR__THAUDMSCM_IO_CLKEN__INV_MASK    0xFFBFFFFF
#define CLKC_LEAF_CLK_EN1_CLR__THAUDMSCM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_CLR.analogtest_xin_clken - analogtest_xin_clk clock disable */
/* 0 : No effect */
/* 1 : Disable analogtest_xin_clk clock */
#define CLKC_LEAF_CLK_EN1_CLR__ANALOGTEST_XIN_CLKEN__SHIFT       23
#define CLKC_LEAF_CLK_EN1_CLR__ANALOGTEST_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_CLR__ANALOGTEST_XIN_CLKEN__MASK        0x00800000
#define CLKC_LEAF_CLK_EN1_CLR__ANALOGTEST_XIN_CLKEN__INV_MASK    0xFF7FFFFF
#define CLKC_LEAF_CLK_EN1_CLR__ANALOGTEST_XIN_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN1_STATUS  0x186204A8

/* CLKC_LEAF_CLK_EN1_STATUS.cvd_io_clken - cvd_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of cvd_io_clk leaf clock. */
/* 0 : cvd_io_clk clock disabled */
/* 1 : cvd_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__CVD_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN1_STATUS__CVD_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__CVD_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN1_STATUS__CVD_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN1_STATUS__CVD_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.timer_io_clken - timer_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of timer_io_clk leaf clock. */
/* 0 : timer_io_clk clock disabled */
/* 1 : timer_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__TIMER_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN1_STATUS__TIMER_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__TIMER_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN1_STATUS__TIMER_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN1_STATUS__TIMER_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.pulsec_io_clken - pulsec_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of pulsec_io_clk leaf clock. */
/* 0 : pulsec_io_clk clock disabled */
/* 1 : pulsec_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__PULSEC_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN1_STATUS__PULSEC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__PULSEC_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN1_STATUS__PULSEC_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN1_STATUS__PULSEC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.tsc_io_clken - tsc_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of tsc_io_clk leaf clock. */
/* 0 : tsc_io_clk clock disabled */
/* 1 : tsc_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.ioctop_io_clken - ioctop_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of ioctop_io_clk leaf clock. */
/* 0 : ioctop_io_clk clock disabled */
/* 1 : ioctop_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__IOCTOP_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN1_STATUS__IOCTOP_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__IOCTOP_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN1_STATUS__IOCTOP_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN1_STATUS__IOCTOP_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.rsc_io_clken - rsc_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of rsc_io_clk leaf clock. */
/* 0 : rsc_io_clk clock disabled */
/* 1 : rsc_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__RSC_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN1_STATUS__RSC_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__RSC_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN1_STATUS__RSC_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN1_STATUS__RSC_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.dvm_xin_clken - dvm_xin_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of dvm_xin_clk leaf clock. */
/* 0 : dvm_xin_clk clock disabled */
/* 1 : dvm_xin_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__DVM_XIN_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN1_STATUS__DVM_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__DVM_XIN_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN1_STATUS__DVM_XIN_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN1_STATUS__DVM_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.lvds_xin_clken - lvds_xin_clk clock enable status */
/* If root clock XIN_CLK is disabled this status bit does not reflect the actual status of lvds_xin_clk leaf clock. */
/* 0 : lvds_xin_clk clock disabled */
/* 1 : lvds_xin_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__LVDS_XIN_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN1_STATUS__LVDS_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__LVDS_XIN_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN1_STATUS__LVDS_XIN_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN1_STATUS__LVDS_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.kas_kas_clken - kas_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of kas_kas_clk leaf clock. */
/* 0 : kas_kas_clk clock disabled */
/* 1 : kas_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__KAS_KAS_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN1_STATUS__KAS_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__KAS_KAS_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN1_STATUS__KAS_KAS_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN1_STATUS__KAS_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.ac97_kas_clken - ac97_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of ac97_kas_clk leaf clock. */
/* 0 : ac97_kas_clk clock disabled */
/* 1 : ac97_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__AC97_KAS_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN1_STATUS__AC97_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__AC97_KAS_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN1_STATUS__AC97_KAS_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN1_STATUS__AC97_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.usp0_kas_clken - usp0_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of usp0_kas_clk leaf clock. */
/* 0 : usp0_kas_clk clock disabled */
/* 1 : usp0_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__USP0_KAS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN1_STATUS__USP0_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__USP0_KAS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN1_STATUS__USP0_KAS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN1_STATUS__USP0_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.usp1_kas_clken - usp1_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of usp1_kas_clk leaf clock. */
/* 0 : usp1_kas_clk clock disabled */
/* 1 : usp1_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__USP1_KAS_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN1_STATUS__USP1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__USP1_KAS_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN1_STATUS__USP1_KAS_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN1_STATUS__USP1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.usp2_kas_clken - usp2_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of usp2_kas_clk leaf clock. */
/* 0 : usp2_kas_clk clock disabled */
/* 1 : usp2_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__USP2_KAS_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN1_STATUS__USP2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__USP2_KAS_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN1_STATUS__USP2_KAS_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN1_STATUS__USP2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.dmac2_kas_clken - dmac2_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of dmac2_kas_clk leaf clock. */
/* 0 : dmac2_kas_clk clock disabled */
/* 1 : dmac2_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC2_KAS_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC2_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC2_KAS_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC2_KAS_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC2_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.dmac3_kas_clken - dmac3_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of dmac3_kas_clk leaf clock. */
/* 0 : dmac3_kas_clk clock disabled */
/* 1 : dmac3_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC3_KAS_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC3_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC3_KAS_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC3_KAS_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN1_STATUS__DMAC3_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.audio_if_kas_clken - audio_if_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of audio_if_kas_clk leaf clock. */
/* 0 : audio_if_kas_clk clock disabled */
/* 1 : audio_if_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__AUDIO_IF_KAS_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN1_STATUS__AUDIO_IF_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__AUDIO_IF_KAS_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN1_STATUS__AUDIO_IF_KAS_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN1_STATUS__AUDIO_IF_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.i2s1_kas_clken - i2s1_kas_clk clock enable status */
/* If root clock KAS_CLK is disabled this status bit does not reflect the actual status of i2s1_kas_clk leaf clock. */
/* 0 : i2s1_kas_clk clock disabled */
/* 1 : i2s1_kas_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__I2S1_KAS_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN1_STATUS__I2S1_KAS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__I2S1_KAS_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN1_STATUS__I2S1_KAS_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN1_STATUS__I2S1_KAS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.tsc_xin_clken - tsc_xin_clk clock enable status */
/* If root clock XIN_CLK is disabled this status bit does not reflect the actual status of tsc_xin_clk leaf clock. */
/* 0 : tsc_xin_clk clock disabled */
/* 1 : tsc_xin_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_XIN_CLKEN__SHIFT       21
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_XIN_CLKEN__MASK        0x00200000
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_XIN_CLKEN__INV_MASK    0xFFDFFFFF
#define CLKC_LEAF_CLK_EN1_STATUS__TSC_XIN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN1_STATUS.thaudmscm_io_clken - thaudmscm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of thaudmscm_io_clk leaf clock. */
/* 0 : thaudmscm_io_clk clock disabled */
/* 1 : thaudmscm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__THAUDMSCM_IO_CLKEN__SHIFT       22
#define CLKC_LEAF_CLK_EN1_STATUS__THAUDMSCM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__THAUDMSCM_IO_CLKEN__MASK        0x00400000
#define CLKC_LEAF_CLK_EN1_STATUS__THAUDMSCM_IO_CLKEN__INV_MASK    0xFFBFFFFF
#define CLKC_LEAF_CLK_EN1_STATUS__THAUDMSCM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN1_STATUS.analogtest_xin_clken - analogtest_xin_clk clock enable status */
/* If root clock XIN_CLK is disabled this status bit does not reflect the actual status of analogtest_xin_clk leaf clock. */
/* 0 : analogtest_xin_clk clock disabled */
/* 1 : analogtest_xin_clk clock enabled */
#define CLKC_LEAF_CLK_EN1_STATUS__ANALOGTEST_XIN_CLKEN__SHIFT       23
#define CLKC_LEAF_CLK_EN1_STATUS__ANALOGTEST_XIN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN1_STATUS__ANALOGTEST_XIN_CLKEN__MASK        0x00800000
#define CLKC_LEAF_CLK_EN1_STATUS__ANALOGTEST_XIN_CLKEN__INV_MASK    0xFF7FFFFF
#define CLKC_LEAF_CLK_EN1_STATUS__ANALOGTEST_XIN_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN2_SET     0x186204B8

/* CLKC_LEAF_CLK_EN2_SET.sys2pci_io_clken - sys2pci_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sys2pci_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SYS2PCI_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN2_SET__SYS2PCI_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SYS2PCI_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN2_SET__SYS2PCI_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN2_SET__SYS2PCI_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.pciarb_io_clken - pciarb_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pciarb_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__PCIARB_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN2_SET__PCIARB_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__PCIARB_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN2_SET__PCIARB_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN2_SET__PCIARB_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.pcicopy_io_clken - pcicopy_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable pcicopy_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__PCICOPY_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN2_SET__PCICOPY_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__PCICOPY_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN2_SET__PCICOPY_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN2_SET__PCICOPY_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.rom_io_clken - rom_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable rom_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__ROM_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN2_SET__ROM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__ROM_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN2_SET__ROM_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN2_SET__ROM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio23_io_clken - sdio23_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio23_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio45_io_clken - sdio45_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio45_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio67_io_clken - sdio67_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio67_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.vip1_io_clken - vip1_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vip1_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__VIP1_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN2_SET__VIP1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__VIP1_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN2_SET__VIP1_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN2_SET__VIP1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio23_sdphy23_clken - sdio23_sdphy23_clk clock enable */
/* Root clock SDPHY23_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio23_sdphy23_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_SDPHY23_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_SDPHY23_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_SDPHY23_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_SDPHY23_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN2_SET__SDIO23_SDPHY23_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio45_sdphy45_clken - sdio45_sdphy45_clk clock enable */
/* Root clock SDPHY45_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio45_sdphy45_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_SDPHY45_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_SDPHY45_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_SDPHY45_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_SDPHY45_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN2_SET__SDIO45_SDPHY45_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.sdio67_sdphy67_clken - sdio67_sdphy67_clk clock enable */
/* Root clock SDPHY67_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio67_sdphy67_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_SDPHY67_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_SDPHY67_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_SDPHY67_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_SDPHY67_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN2_SET__SDIO67_SDPHY67_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.vpp0_disp0_clken - vpp0_disp0_clk clock enable */
/* Root clock DISP0_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vpp0_disp0_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__VPP0_DISP0_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN2_SET__VPP0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__VPP0_DISP0_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN2_SET__VPP0_DISP0_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN2_SET__VPP0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.lcd0_disp0_clken - lcd0_disp0_clk clock enable */
/* Root clock DISP0_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable lcd0_disp0_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__LCD0_DISP0_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN2_SET__LCD0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__LCD0_DISP0_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN2_SET__LCD0_DISP0_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN2_SET__LCD0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.vpp1_disp1_clken - vpp1_disp1_clk clock enable */
/* Root clock DISP1_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vpp1_disp1_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__VPP1_DISP1_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN2_SET__VPP1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__VPP1_DISP1_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN2_SET__VPP1_DISP1_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN2_SET__VPP1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.lcd1_disp1_clken - lcd1_disp1_clk clock enable */
/* Root clock DISP1_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable lcd1_disp1_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__LCD1_DISP1_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN2_SET__LCD1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__LCD1_DISP1_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN2_SET__LCD1_DISP1_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN2_SET__LCD1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.dcu_deint_clken - dcu_deint_clk clock enable */
/* Root clock DEINT_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dcu_deint_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__DCU_DEINT_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN2_SET__DCU_DEINT_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__DCU_DEINT_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN2_SET__DCU_DEINT_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN2_SET__DCU_DEINT_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.vip1_vip_clken - vip1_vip_clk clock enable */
/* Root clock VIP_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vip1_vip_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__VIP1_VIP_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN2_SET__VIP1_VIP_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__VIP1_VIP_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN2_SET__VIP1_VIP_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN2_SET__VIP1_VIP_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_SET.vdifm_dapa_r_noc_clken - vdifm_dapa_r_noc_clk clock enable */
/* Root clock NOCR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vdifm_dapa_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__VDIFM_DAPA_R_NOC_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN2_SET__VDIFM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__VDIFM_DAPA_R_NOC_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN2_SET__VDIFM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN2_SET__VDIFM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_SET.gpio_io_clken - gpio_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gpio_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__GPIO_IO_CLKEN__SHIFT       18
#define CLKC_LEAF_CLK_EN2_SET__GPIO_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__GPIO_IO_CLKEN__MASK        0x00040000
#define CLKC_LEAF_CLK_EN2_SET__GPIO_IO_CLKEN__INV_MASK    0xFFFBFFFF
#define CLKC_LEAF_CLK_EN2_SET__GPIO_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_SET.thvdifm_io_clken - thvdifm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thvdifm_io_clk clock */
#define CLKC_LEAF_CLK_EN2_SET__THVDIFM_IO_CLKEN__SHIFT       19
#define CLKC_LEAF_CLK_EN2_SET__THVDIFM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_SET__THVDIFM_IO_CLKEN__MASK        0x00080000
#define CLKC_LEAF_CLK_EN2_SET__THVDIFM_IO_CLKEN__INV_MASK    0xFFF7FFFF
#define CLKC_LEAF_CLK_EN2_SET__THVDIFM_IO_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN2_CLR     0x186204BC

/* CLKC_LEAF_CLK_EN2_CLR.sys2pci_io_clken - sys2pci_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sys2pci_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SYS2PCI_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN2_CLR__SYS2PCI_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SYS2PCI_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN2_CLR__SYS2PCI_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN2_CLR__SYS2PCI_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.pciarb_io_clken - pciarb_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pciarb_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__PCIARB_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN2_CLR__PCIARB_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__PCIARB_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN2_CLR__PCIARB_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN2_CLR__PCIARB_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.pcicopy_io_clken - pcicopy_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable pcicopy_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__PCICOPY_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN2_CLR__PCICOPY_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__PCICOPY_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN2_CLR__PCICOPY_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN2_CLR__PCICOPY_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.rom_io_clken - rom_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable rom_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__ROM_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN2_CLR__ROM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__ROM_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN2_CLR__ROM_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN2_CLR__ROM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio23_io_clken - sdio23_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio23_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio45_io_clken - sdio45_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio45_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio67_io_clken - sdio67_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio67_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.vip1_io_clken - vip1_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vip1_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio23_sdphy23_clken - sdio23_sdphy23_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio23_sdphy23_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_SDPHY23_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_SDPHY23_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_SDPHY23_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_SDPHY23_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO23_SDPHY23_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio45_sdphy45_clken - sdio45_sdphy45_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio45_sdphy45_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_SDPHY45_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_SDPHY45_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_SDPHY45_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_SDPHY45_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO45_SDPHY45_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.sdio67_sdphy67_clken - sdio67_sdphy67_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio67_sdphy67_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_SDPHY67_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_SDPHY67_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_SDPHY67_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_SDPHY67_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN2_CLR__SDIO67_SDPHY67_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.vpp0_disp0_clken - vpp0_disp0_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vpp0_disp0_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__VPP0_DISP0_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN2_CLR__VPP0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__VPP0_DISP0_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN2_CLR__VPP0_DISP0_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN2_CLR__VPP0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.lcd0_disp0_clken - lcd0_disp0_clk clock disable */
/* 0 : No effect */
/* 1 : Disable lcd0_disp0_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__LCD0_DISP0_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN2_CLR__LCD0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__LCD0_DISP0_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN2_CLR__LCD0_DISP0_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN2_CLR__LCD0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.vpp1_disp1_clken - vpp1_disp1_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vpp1_disp1_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__VPP1_DISP1_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN2_CLR__VPP1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__VPP1_DISP1_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN2_CLR__VPP1_DISP1_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN2_CLR__VPP1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.lcd1_disp1_clken - lcd1_disp1_clk clock disable */
/* 0 : No effect */
/* 1 : Disable lcd1_disp1_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__LCD1_DISP1_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN2_CLR__LCD1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__LCD1_DISP1_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN2_CLR__LCD1_DISP1_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN2_CLR__LCD1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.dcu_deint_clken - dcu_deint_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dcu_deint_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__DCU_DEINT_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN2_CLR__DCU_DEINT_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__DCU_DEINT_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN2_CLR__DCU_DEINT_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN2_CLR__DCU_DEINT_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.vip1_vip_clken - vip1_vip_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vip1_vip_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_VIP_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_VIP_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_VIP_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_VIP_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN2_CLR__VIP1_VIP_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_CLR.vdifm_dapa_r_noc_clken - vdifm_dapa_r_noc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vdifm_dapa_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__VDIFM_DAPA_R_NOC_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN2_CLR__VDIFM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__VDIFM_DAPA_R_NOC_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN2_CLR__VDIFM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN2_CLR__VDIFM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_CLR.gpio_io_clken - gpio_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gpio_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__GPIO_IO_CLKEN__SHIFT       18
#define CLKC_LEAF_CLK_EN2_CLR__GPIO_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__GPIO_IO_CLKEN__MASK        0x00040000
#define CLKC_LEAF_CLK_EN2_CLR__GPIO_IO_CLKEN__INV_MASK    0xFFFBFFFF
#define CLKC_LEAF_CLK_EN2_CLR__GPIO_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_CLR.thvdifm_io_clken - thvdifm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thvdifm_io_clk clock */
#define CLKC_LEAF_CLK_EN2_CLR__THVDIFM_IO_CLKEN__SHIFT       19
#define CLKC_LEAF_CLK_EN2_CLR__THVDIFM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_CLR__THVDIFM_IO_CLKEN__MASK        0x00080000
#define CLKC_LEAF_CLK_EN2_CLR__THVDIFM_IO_CLKEN__INV_MASK    0xFFF7FFFF
#define CLKC_LEAF_CLK_EN2_CLR__THVDIFM_IO_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN2_STATUS  0x186204C0

/* CLKC_LEAF_CLK_EN2_STATUS.sys2pci_io_clken - sys2pci_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sys2pci_io_clk leaf clock. */
/* 0 : sys2pci_io_clk clock disabled */
/* 1 : sys2pci_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SYS2PCI_IO_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN2_STATUS__SYS2PCI_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SYS2PCI_IO_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN2_STATUS__SYS2PCI_IO_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN2_STATUS__SYS2PCI_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.pciarb_io_clken - pciarb_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of pciarb_io_clk leaf clock. */
/* 0 : pciarb_io_clk clock disabled */
/* 1 : pciarb_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__PCIARB_IO_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN2_STATUS__PCIARB_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__PCIARB_IO_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN2_STATUS__PCIARB_IO_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN2_STATUS__PCIARB_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.pcicopy_io_clken - pcicopy_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of pcicopy_io_clk leaf clock. */
/* 0 : pcicopy_io_clk clock disabled */
/* 1 : pcicopy_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__PCICOPY_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN2_STATUS__PCICOPY_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__PCICOPY_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN2_STATUS__PCICOPY_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN2_STATUS__PCICOPY_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.rom_io_clken - rom_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of rom_io_clk leaf clock. */
/* 0 : rom_io_clk clock disabled */
/* 1 : rom_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__ROM_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN2_STATUS__ROM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__ROM_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN2_STATUS__ROM_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN2_STATUS__ROM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio23_io_clken - sdio23_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sdio23_io_clk leaf clock. */
/* 0 : sdio23_io_clk clock disabled */
/* 1 : sdio23_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio45_io_clken - sdio45_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sdio45_io_clk leaf clock. */
/* 0 : sdio45_io_clk clock disabled */
/* 1 : sdio45_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio67_io_clken - sdio67_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sdio67_io_clk leaf clock. */
/* 0 : sdio67_io_clk clock disabled */
/* 1 : sdio67_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.vip1_io_clken - vip1_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of vip1_io_clk leaf clock. */
/* 0 : vip1_io_clk clock disabled */
/* 1 : vip1_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio23_sdphy23_clken - sdio23_sdphy23_clk clock enable status */
/* If root clock SDPHY23_CLK is disabled this status bit does not reflect the actual status of sdio23_sdphy23_clk leaf clock. */
/* 0 : sdio23_sdphy23_clk clock disabled */
/* 1 : sdio23_sdphy23_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_SDPHY23_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_SDPHY23_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_SDPHY23_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_SDPHY23_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO23_SDPHY23_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio45_sdphy45_clken - sdio45_sdphy45_clk clock enable status */
/* If root clock SDPHY45_CLK is disabled this status bit does not reflect the actual status of sdio45_sdphy45_clk leaf clock. */
/* 0 : sdio45_sdphy45_clk clock disabled */
/* 1 : sdio45_sdphy45_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_SDPHY45_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_SDPHY45_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_SDPHY45_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_SDPHY45_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO45_SDPHY45_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.sdio67_sdphy67_clken - sdio67_sdphy67_clk clock enable status */
/* If root clock SDPHY67_CLK is disabled this status bit does not reflect the actual status of sdio67_sdphy67_clk leaf clock. */
/* 0 : sdio67_sdphy67_clk clock disabled */
/* 1 : sdio67_sdphy67_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_SDPHY67_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_SDPHY67_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_SDPHY67_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_SDPHY67_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN2_STATUS__SDIO67_SDPHY67_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.vpp0_disp0_clken - vpp0_disp0_clk clock enable status */
/* If root clock DISP0_CLK is disabled this status bit does not reflect the actual status of vpp0_disp0_clk leaf clock. */
/* 0 : vpp0_disp0_clk clock disabled */
/* 1 : vpp0_disp0_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__VPP0_DISP0_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN2_STATUS__VPP0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__VPP0_DISP0_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN2_STATUS__VPP0_DISP0_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN2_STATUS__VPP0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.lcd0_disp0_clken - lcd0_disp0_clk clock enable status */
/* If root clock DISP0_CLK is disabled this status bit does not reflect the actual status of lcd0_disp0_clk leaf clock. */
/* 0 : lcd0_disp0_clk clock disabled */
/* 1 : lcd0_disp0_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__LCD0_DISP0_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN2_STATUS__LCD0_DISP0_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__LCD0_DISP0_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN2_STATUS__LCD0_DISP0_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN2_STATUS__LCD0_DISP0_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.vpp1_disp1_clken - vpp1_disp1_clk clock enable status */
/* If root clock DISP1_CLK is disabled this status bit does not reflect the actual status of vpp1_disp1_clk leaf clock. */
/* 0 : vpp1_disp1_clk clock disabled */
/* 1 : vpp1_disp1_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__VPP1_DISP1_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN2_STATUS__VPP1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__VPP1_DISP1_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN2_STATUS__VPP1_DISP1_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN2_STATUS__VPP1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.lcd1_disp1_clken - lcd1_disp1_clk clock enable status */
/* If root clock DISP1_CLK is disabled this status bit does not reflect the actual status of lcd1_disp1_clk leaf clock. */
/* 0 : lcd1_disp1_clk clock disabled */
/* 1 : lcd1_disp1_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__LCD1_DISP1_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN2_STATUS__LCD1_DISP1_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__LCD1_DISP1_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN2_STATUS__LCD1_DISP1_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN2_STATUS__LCD1_DISP1_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.dcu_deint_clken - dcu_deint_clk clock enable status */
/* If root clock DEINT_CLK is disabled this status bit does not reflect the actual status of dcu_deint_clk leaf clock. */
/* 0 : dcu_deint_clk clock disabled */
/* 1 : dcu_deint_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__DCU_DEINT_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN2_STATUS__DCU_DEINT_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__DCU_DEINT_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN2_STATUS__DCU_DEINT_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN2_STATUS__DCU_DEINT_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.vip1_vip_clken - vip1_vip_clk clock enable status */
/* If root clock VIP_CLK is disabled this status bit does not reflect the actual status of vip1_vip_clk leaf clock. */
/* 0 : vip1_vip_clk clock disabled */
/* 1 : vip1_vip_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_VIP_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_VIP_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_VIP_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_VIP_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN2_STATUS__VIP1_VIP_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN2_STATUS.vdifm_dapa_r_noc_clken - vdifm_dapa_r_noc_clk clock enable status */
/* If root clock NOCR_CLK is disabled this status bit does not reflect the actual status of vdifm_dapa_r_noc_clk leaf clock. */
/* 0 : vdifm_dapa_r_noc_clk clock disabled */
/* 1 : vdifm_dapa_r_noc_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__VDIFM_DAPA_R_NOC_CLKEN__SHIFT       17
#define CLKC_LEAF_CLK_EN2_STATUS__VDIFM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__VDIFM_DAPA_R_NOC_CLKEN__MASK        0x00020000
#define CLKC_LEAF_CLK_EN2_STATUS__VDIFM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFDFFFF
#define CLKC_LEAF_CLK_EN2_STATUS__VDIFM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_STATUS.gpio_io_clken - gpio_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of gpio_io_clk leaf clock. */
/* 0 : gpio_io_clk clock disabled */
/* 1 : gpio_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__GPIO_IO_CLKEN__SHIFT       18
#define CLKC_LEAF_CLK_EN2_STATUS__GPIO_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__GPIO_IO_CLKEN__MASK        0x00040000
#define CLKC_LEAF_CLK_EN2_STATUS__GPIO_IO_CLKEN__INV_MASK    0xFFFBFFFF
#define CLKC_LEAF_CLK_EN2_STATUS__GPIO_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN2_STATUS.thvdifm_io_clken - thvdifm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of thvdifm_io_clk leaf clock. */
/* 0 : thvdifm_io_clk clock disabled */
/* 1 : thvdifm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN2_STATUS__THVDIFM_IO_CLKEN__SHIFT       19
#define CLKC_LEAF_CLK_EN2_STATUS__THVDIFM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN2_STATUS__THVDIFM_IO_CLKEN__MASK        0x00080000
#define CLKC_LEAF_CLK_EN2_STATUS__THVDIFM_IO_CLKEN__INV_MASK    0xFFF7FFFF
#define CLKC_LEAF_CLK_EN2_STATUS__THVDIFM_IO_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN3_SET     0x186204D0

/* CLKC_LEAF_CLK_EN3_SET.gmac_rgmii_clken - gmac_rgmii_clk clock enable */
/* Root clock RGMII_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gmac_rgmii_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__GMAC_RGMII_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN3_SET__GMAC_RGMII_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__GMAC_RGMII_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN3_SET__GMAC_RGMII_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN3_SET__GMAC_RGMII_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.gmac_gmac_clken - gmac_gmac_clk clock enable */
/* Root clock GMAC_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gmac_gmac_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__GMAC_GMAC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN3_SET__GMAC_GMAC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__GMAC_GMAC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN3_SET__GMAC_GMAC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN3_SET__GMAC_GMAC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart1_io_clken - uart1_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart1_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.dmac0_io_clken - dmac0_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dmac0_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__DMAC0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN3_SET__DMAC0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__DMAC0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN3_SET__DMAC0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN3_SET__DMAC0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart0_io_clken - uart0_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart0_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART0_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN3_SET__UART0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART0_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN3_SET__UART0_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN3_SET__UART0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart2_io_clken - uart2_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart2_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART2_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN3_SET__UART2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART2_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN3_SET__UART2_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN3_SET__UART2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart3_io_clken - uart3_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart3_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART3_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN3_SET__UART3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART3_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN3_SET__UART3_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN3_SET__UART3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart4_io_clken - uart4_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart4_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART4_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN3_SET__UART4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART4_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN3_SET__UART4_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN3_SET__UART4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.uart5_io_clken - uart5_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart5_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__UART5_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN3_SET__UART5_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__UART5_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN3_SET__UART5_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN3_SET__UART5_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.spi1_io_clken - spi1_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable spi1_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__SPI1_IO_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN3_SET__SPI1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__SPI1_IO_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN3_SET__SPI1_IO_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN3_SET__SPI1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.gnssm_gnss_clken - gnssm_gnss_clk clock enable */
/* Root clock GNSS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gnssm_gnss_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_GNSS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_GNSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_GNSS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_GNSS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_GNSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.canbus1_can_clken - canbus1_can_clk clock enable */
/* Root clock CAN_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable canbus1_can_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__CANBUS1_CAN_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN3_SET__CANBUS1_CAN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__CANBUS1_CAN_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN3_SET__CANBUS1_CAN_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN3_SET__CANBUS1_CAN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_SET.gnssm_dapa_r_noc_clken - gnssm_dapa_r_noc_clk clock enable */
/* Root clock NOCR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gnssm_dapa_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_DAPA_R_NOC_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_DAPA_R_NOC_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN3_SET__GNSSM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_SET.thgnssm_io_clken - thgnssm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thgnssm_io_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__THGNSSM_IO_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN3_SET__THGNSSM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__THGNSSM_IO_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN3_SET__THGNSSM_IO_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN3_SET__THGNSSM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_SET.ccsec_sec_clken - ccsec_sec_clk clock enable */
/* Root clock SEC_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable ccsec_sec_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__CCSEC_SEC_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN3_SET__CCSEC_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__CCSEC_SEC_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN3_SET__CCSEC_SEC_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN3_SET__CCSEC_SEC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_SET.ccpub_sec_clken - ccpub_sec_clk clock enable */
/* Root clock SEC_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable ccpub_sec_clk clock */
#define CLKC_LEAF_CLK_EN3_SET__CCPUB_SEC_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN3_SET__CCPUB_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_SET__CCPUB_SEC_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN3_SET__CCPUB_SEC_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN3_SET__CCPUB_SEC_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN3_CLR     0x186204D4

/* CLKC_LEAF_CLK_EN3_CLR.gmac_rgmii_clken - gmac_rgmii_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gmac_rgmii_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_RGMII_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_RGMII_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_RGMII_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_RGMII_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_RGMII_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.gmac_gmac_clken - gmac_gmac_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gmac_gmac_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_GMAC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_GMAC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_GMAC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_GMAC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN3_CLR__GMAC_GMAC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart1_io_clken - uart1_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart1_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART1_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN3_CLR__UART1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART1_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN3_CLR__UART1_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN3_CLR__UART1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.dmac0_io_clken - dmac0_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dmac0_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__DMAC0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN3_CLR__DMAC0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__DMAC0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN3_CLR__DMAC0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN3_CLR__DMAC0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart0_io_clken - uart0_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart0_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART0_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN3_CLR__UART0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART0_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN3_CLR__UART0_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN3_CLR__UART0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart2_io_clken - uart2_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart2_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART2_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN3_CLR__UART2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART2_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN3_CLR__UART2_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN3_CLR__UART2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart3_io_clken - uart3_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart3_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART3_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN3_CLR__UART3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART3_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN3_CLR__UART3_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN3_CLR__UART3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart4_io_clken - uart4_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart4_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART4_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN3_CLR__UART4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART4_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN3_CLR__UART4_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN3_CLR__UART4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.uart5_io_clken - uart5_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart5_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__UART5_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN3_CLR__UART5_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__UART5_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN3_CLR__UART5_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN3_CLR__UART5_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.spi1_io_clken - spi1_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable spi1_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__SPI1_IO_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN3_CLR__SPI1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__SPI1_IO_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN3_CLR__SPI1_IO_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN3_CLR__SPI1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.gnssm_gnss_clken - gnssm_gnss_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gnssm_gnss_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_GNSS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_GNSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_GNSS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_GNSS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_GNSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.canbus1_can_clken - canbus1_can_clk clock disable */
/* 0 : No effect */
/* 1 : Disable canbus1_can_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__CANBUS1_CAN_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN3_CLR__CANBUS1_CAN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__CANBUS1_CAN_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN3_CLR__CANBUS1_CAN_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN3_CLR__CANBUS1_CAN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_CLR.gnssm_dapa_r_noc_clken - gnssm_dapa_r_noc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gnssm_dapa_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_DAPA_R_NOC_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_DAPA_R_NOC_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN3_CLR__GNSSM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_CLR.thgnssm_io_clken - thgnssm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thgnssm_io_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__THGNSSM_IO_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN3_CLR__THGNSSM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__THGNSSM_IO_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN3_CLR__THGNSSM_IO_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN3_CLR__THGNSSM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_CLR.ccsec_sec_clken - ccsec_sec_clk clock disable */
/* 0 : No effect */
/* 1 : Disable ccsec_sec_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__CCSEC_SEC_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN3_CLR__CCSEC_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__CCSEC_SEC_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN3_CLR__CCSEC_SEC_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN3_CLR__CCSEC_SEC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_CLR.ccpub_sec_clken - ccpub_sec_clk clock disable */
/* 0 : No effect */
/* 1 : Disable ccpub_sec_clk clock */
#define CLKC_LEAF_CLK_EN3_CLR__CCPUB_SEC_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN3_CLR__CCPUB_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_CLR__CCPUB_SEC_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN3_CLR__CCPUB_SEC_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN3_CLR__CCPUB_SEC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN3_STATUS  0x186204D8

/* CLKC_LEAF_CLK_EN3_STATUS.gmac_rgmii_clken - gmac_rgmii_clk clock enable status */
/* If root clock RGMII_CLK is disabled this status bit does not reflect the actual status of gmac_rgmii_clk leaf clock. */
/* 0 : gmac_rgmii_clk clock disabled */
/* 1 : gmac_rgmii_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_RGMII_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_RGMII_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_RGMII_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_RGMII_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_RGMII_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.gmac_gmac_clken - gmac_gmac_clk clock enable status */
/* If root clock GMAC_CLK is disabled this status bit does not reflect the actual status of gmac_gmac_clk leaf clock. */
/* 0 : gmac_gmac_clk clock disabled */
/* 1 : gmac_gmac_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_GMAC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_GMAC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_GMAC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_GMAC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN3_STATUS__GMAC_GMAC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart1_io_clken - uart1_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart1_io_clk leaf clock. */
/* 0 : uart1_io_clk clock disabled */
/* 1 : uart1_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART1_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN3_STATUS__UART1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART1_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN3_STATUS__UART1_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN3_STATUS__UART1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.dmac0_io_clken - dmac0_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of dmac0_io_clk leaf clock. */
/* 0 : dmac0_io_clk clock disabled */
/* 1 : dmac0_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__DMAC0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN3_STATUS__DMAC0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__DMAC0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN3_STATUS__DMAC0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN3_STATUS__DMAC0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart0_io_clken - uart0_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart0_io_clk leaf clock. */
/* 0 : uart0_io_clk clock disabled */
/* 1 : uart0_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART0_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN3_STATUS__UART0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART0_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN3_STATUS__UART0_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN3_STATUS__UART0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart2_io_clken - uart2_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart2_io_clk leaf clock. */
/* 0 : uart2_io_clk clock disabled */
/* 1 : uart2_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART2_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN3_STATUS__UART2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART2_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN3_STATUS__UART2_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN3_STATUS__UART2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart3_io_clken - uart3_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart3_io_clk leaf clock. */
/* 0 : uart3_io_clk clock disabled */
/* 1 : uart3_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART3_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN3_STATUS__UART3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART3_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN3_STATUS__UART3_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN3_STATUS__UART3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart4_io_clken - uart4_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart4_io_clk leaf clock. */
/* 0 : uart4_io_clk clock disabled */
/* 1 : uart4_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART4_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN3_STATUS__UART4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART4_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN3_STATUS__UART4_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN3_STATUS__UART4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.uart5_io_clken - uart5_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart5_io_clk leaf clock. */
/* 0 : uart5_io_clk clock disabled */
/* 1 : uart5_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__UART5_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN3_STATUS__UART5_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__UART5_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN3_STATUS__UART5_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN3_STATUS__UART5_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.spi1_io_clken - spi1_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of spi1_io_clk leaf clock. */
/* 0 : spi1_io_clk clock disabled */
/* 1 : spi1_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__SPI1_IO_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN3_STATUS__SPI1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__SPI1_IO_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN3_STATUS__SPI1_IO_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN3_STATUS__SPI1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.gnssm_gnss_clken - gnssm_gnss_clk clock enable status */
/* If root clock GNSS_CLK is disabled this status bit does not reflect the actual status of gnssm_gnss_clk leaf clock. */
/* 0 : gnssm_gnss_clk clock disabled */
/* 1 : gnssm_gnss_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_GNSS_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_GNSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_GNSS_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_GNSS_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_GNSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.canbus1_can_clken - canbus1_can_clk clock enable status */
/* If root clock CAN_CLK is disabled this status bit does not reflect the actual status of canbus1_can_clk leaf clock. */
/* 0 : canbus1_can_clk clock disabled */
/* 1 : canbus1_can_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__CANBUS1_CAN_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN3_STATUS__CANBUS1_CAN_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__CANBUS1_CAN_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN3_STATUS__CANBUS1_CAN_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN3_STATUS__CANBUS1_CAN_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN3_STATUS.gnssm_dapa_r_noc_clken - gnssm_dapa_r_noc_clk clock enable status */
/* If root clock NOCR_CLK is disabled this status bit does not reflect the actual status of gnssm_dapa_r_noc_clk leaf clock. */
/* 0 : gnssm_dapa_r_noc_clk clock disabled */
/* 1 : gnssm_dapa_r_noc_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_DAPA_R_NOC_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_DAPA_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_DAPA_R_NOC_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_DAPA_R_NOC_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN3_STATUS__GNSSM_DAPA_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_STATUS.thgnssm_io_clken - thgnssm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of thgnssm_io_clk leaf clock. */
/* 0 : thgnssm_io_clk clock disabled */
/* 1 : thgnssm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__THGNSSM_IO_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN3_STATUS__THGNSSM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__THGNSSM_IO_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN3_STATUS__THGNSSM_IO_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN3_STATUS__THGNSSM_IO_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_STATUS.ccsec_sec_clken - ccsec_sec_clk clock enable status */
/* If root clock SEC_CLK is disabled this status bit does not reflect the actual status of ccsec_sec_clk leaf clock. */
/* 0 : ccsec_sec_clk clock disabled */
/* 1 : ccsec_sec_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__CCSEC_SEC_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN3_STATUS__CCSEC_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__CCSEC_SEC_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN3_STATUS__CCSEC_SEC_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN3_STATUS__CCSEC_SEC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN3_STATUS.ccpub_sec_clken - ccpub_sec_clk clock enable status */
/* If root clock SEC_CLK is disabled this status bit does not reflect the actual status of ccpub_sec_clk leaf clock. */
/* 0 : ccpub_sec_clk clock disabled */
/* 1 : ccpub_sec_clk clock enabled */
#define CLKC_LEAF_CLK_EN3_STATUS__CCPUB_SEC_CLKEN__SHIFT       16
#define CLKC_LEAF_CLK_EN3_STATUS__CCPUB_SEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN3_STATUS__CCPUB_SEC_CLKEN__MASK        0x00010000
#define CLKC_LEAF_CLK_EN3_STATUS__CCPUB_SEC_CLKEN__INV_MASK    0xFFFEFFFF
#define CLKC_LEAF_CLK_EN3_STATUS__CCPUB_SEC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN4_SET     0x186204E8

/* CLKC_LEAF_CLK_EN4_SET.media_vdec_clken - media_vdec_clk clock enable */
/* Root clock VDEC_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable media_vdec_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__MEDIA_VDEC_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN4_SET__MEDIA_VDEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__MEDIA_VDEC_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN4_SET__MEDIA_VDEC_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN4_SET__MEDIA_VDEC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.mediae_jpenc_clken - mediae_jpenc_clk clock enable */
/* Root clock JPENC_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable mediae_jpenc_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__MEDIAE_JPENC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN4_SET__MEDIAE_JPENC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__MEDIAE_JPENC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN4_SET__MEDIAE_JPENC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN4_SET__MEDIAE_JPENC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.g2d_g2d_clken - g2d_g2d_clk clock enable */
/* Root clock G2D_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable g2d_g2d_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__G2D_G2D_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN4_SET__G2D_G2D_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__G2D_G2D_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN4_SET__G2D_G2D_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN4_SET__G2D_G2D_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.i2c0_io_clken - i2c0_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable i2c0_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__I2C0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN4_SET__I2C0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__I2C0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN4_SET__I2C0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN4_SET__I2C0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.i2c1_io_clken - i2c1_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable i2c1_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__I2C1_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN4_SET__I2C1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__I2C1_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN4_SET__I2C1_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN4_SET__I2C1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.gpio0_io_clken - gpio0_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable gpio0_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__GPIO0_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN4_SET__GPIO0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__GPIO0_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN4_SET__GPIO0_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN4_SET__GPIO0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.nand_io_clken - nand_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable nand_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__NAND_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN4_SET__NAND_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__NAND_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN4_SET__NAND_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN4_SET__NAND_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.sdio01_io_clken - sdio01_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio01_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.sys2pci2_io_clken - sys2pci2_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sys2pci2_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__SYS2PCI2_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN4_SET__SYS2PCI2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__SYS2PCI2_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN4_SET__SYS2PCI2_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN4_SET__SYS2PCI2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.sdio01_sdphy01_clken - sdio01_sdphy01_clk clock enable */
/* Root clock SDPHY01_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable sdio01_sdphy01_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_SDPHY01_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_SDPHY01_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_SDPHY01_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_SDPHY01_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN4_SET__SDIO01_SDPHY01_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.mediam_nand_clken - mediam_nand_clk clock enable */
/* Root clock NAND_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable mediam_nand_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__MEDIAM_NAND_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN4_SET__MEDIAM_NAND_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__MEDIAM_NAND_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN4_SET__MEDIAM_NAND_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN4_SET__MEDIAM_NAND_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN4_SET.usb0_usb_clken - usb0_usb_clk clock enable */
/* Root clock USB_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usb0_usb_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__USB0_USB_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN4_SET__USB0_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__USB0_USB_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN4_SET__USB0_USB_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN4_SET__USB0_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.usb1_usb_clken - usb1_usb_clk clock enable */
/* Root clock USB_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usb1_usb_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__USB1_USB_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN4_SET__USB1_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__USB1_USB_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN4_SET__USB1_USB_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN4_SET__USB1_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.usbphy0_usbphy_clken - usbphy0_usbphy_clk clock enable */
/* Root clock USBPHY_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usbphy0_usbphy_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__USBPHY0_USBPHY_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN4_SET__USBPHY0_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__USBPHY0_USBPHY_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN4_SET__USBPHY0_USBPHY_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN4_SET__USBPHY0_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.usbphy1_usbphy_clken - usbphy1_usbphy_clk clock enable */
/* Root clock USBPHY_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usbphy1_usbphy_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__USBPHY1_USBPHY_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN4_SET__USBPHY1_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__USBPHY1_USBPHY_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN4_SET__USBPHY1_USBPHY_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN4_SET__USBPHY1_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_SET.thmediam_io_clken - thmediam_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thmediam_io_clk clock */
#define CLKC_LEAF_CLK_EN4_SET__THMEDIAM_IO_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN4_SET__THMEDIAM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_SET__THMEDIAM_IO_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN4_SET__THMEDIAM_IO_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN4_SET__THMEDIAM_IO_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN4_CLR     0x186204EC

/* CLKC_LEAF_CLK_EN4_CLR.media_vdec_clken - media_vdec_clk clock disable */
/* 0 : No effect */
/* 1 : Disable media_vdec_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__MEDIA_VDEC_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN4_CLR__MEDIA_VDEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__MEDIA_VDEC_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN4_CLR__MEDIA_VDEC_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN4_CLR__MEDIA_VDEC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.mediae_jpenc_clken - mediae_jpenc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable mediae_jpenc_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAE_JPENC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAE_JPENC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAE_JPENC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAE_JPENC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAE_JPENC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.g2d_g2d_clken - g2d_g2d_clk clock disable */
/* 0 : No effect */
/* 1 : Disable g2d_g2d_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__G2D_G2D_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN4_CLR__G2D_G2D_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__G2D_G2D_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN4_CLR__G2D_G2D_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN4_CLR__G2D_G2D_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.i2c0_io_clken - i2c0_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable i2c0_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__I2C0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN4_CLR__I2C0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__I2C0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN4_CLR__I2C0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN4_CLR__I2C0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.i2c1_io_clken - i2c1_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable i2c1_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__I2C1_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN4_CLR__I2C1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__I2C1_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN4_CLR__I2C1_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN4_CLR__I2C1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.gpio0_io_clken - gpio0_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable gpio0_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__GPIO0_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN4_CLR__GPIO0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__GPIO0_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN4_CLR__GPIO0_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN4_CLR__GPIO0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.nand_io_clken - nand_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable nand_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__NAND_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN4_CLR__NAND_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__NAND_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN4_CLR__NAND_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN4_CLR__NAND_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.sdio01_io_clken - sdio01_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio01_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.sys2pci2_io_clken - sys2pci2_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sys2pci2_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__SYS2PCI2_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN4_CLR__SYS2PCI2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__SYS2PCI2_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN4_CLR__SYS2PCI2_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN4_CLR__SYS2PCI2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.sdio01_sdphy01_clken - sdio01_sdphy01_clk clock disable */
/* 0 : No effect */
/* 1 : Disable sdio01_sdphy01_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_SDPHY01_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_SDPHY01_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_SDPHY01_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_SDPHY01_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN4_CLR__SDIO01_SDPHY01_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.mediam_nand_clken - mediam_nand_clk clock disable */
/* 0 : No effect */
/* 1 : Disable mediam_nand_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAM_NAND_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAM_NAND_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAM_NAND_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAM_NAND_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN4_CLR__MEDIAM_NAND_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN4_CLR.usb0_usb_clken - usb0_usb_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usb0_usb_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__USB0_USB_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN4_CLR__USB0_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__USB0_USB_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN4_CLR__USB0_USB_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN4_CLR__USB0_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.usb1_usb_clken - usb1_usb_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usb1_usb_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__USB1_USB_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN4_CLR__USB1_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__USB1_USB_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN4_CLR__USB1_USB_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN4_CLR__USB1_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.usbphy0_usbphy_clken - usbphy0_usbphy_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usbphy0_usbphy_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY0_USBPHY_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY0_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY0_USBPHY_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY0_USBPHY_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY0_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.usbphy1_usbphy_clken - usbphy1_usbphy_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usbphy1_usbphy_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY1_USBPHY_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY1_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY1_USBPHY_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY1_USBPHY_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN4_CLR__USBPHY1_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_CLR.thmediam_io_clken - thmediam_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thmediam_io_clk clock */
#define CLKC_LEAF_CLK_EN4_CLR__THMEDIAM_IO_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN4_CLR__THMEDIAM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_CLR__THMEDIAM_IO_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN4_CLR__THMEDIAM_IO_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN4_CLR__THMEDIAM_IO_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN4_STATUS  0x186204F0

/* CLKC_LEAF_CLK_EN4_STATUS.media_vdec_clken - media_vdec_clk clock enable status */
/* If root clock VDEC_CLK is disabled this status bit does not reflect the actual status of media_vdec_clk leaf clock. */
/* 0 : media_vdec_clk clock disabled */
/* 1 : media_vdec_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIA_VDEC_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIA_VDEC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIA_VDEC_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIA_VDEC_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIA_VDEC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.mediae_jpenc_clken - mediae_jpenc_clk clock enable status */
/* If root clock JPENC_CLK is disabled this status bit does not reflect the actual status of mediae_jpenc_clk leaf clock. */
/* 0 : mediae_jpenc_clk clock disabled */
/* 1 : mediae_jpenc_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAE_JPENC_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAE_JPENC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAE_JPENC_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAE_JPENC_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAE_JPENC_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.g2d_g2d_clken - g2d_g2d_clk clock enable status */
/* If root clock G2D_CLK is disabled this status bit does not reflect the actual status of g2d_g2d_clk leaf clock. */
/* 0 : g2d_g2d_clk clock disabled */
/* 1 : g2d_g2d_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__G2D_G2D_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN4_STATUS__G2D_G2D_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__G2D_G2D_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN4_STATUS__G2D_G2D_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN4_STATUS__G2D_G2D_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.i2c0_io_clken - i2c0_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of i2c0_io_clk leaf clock. */
/* 0 : i2c0_io_clk clock disabled */
/* 1 : i2c0_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__I2C0_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN4_STATUS__I2C0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__I2C0_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN4_STATUS__I2C0_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN4_STATUS__I2C0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.i2c1_io_clken - i2c1_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of i2c1_io_clk leaf clock. */
/* 0 : i2c1_io_clk clock disabled */
/* 1 : i2c1_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__I2C1_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN4_STATUS__I2C1_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__I2C1_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN4_STATUS__I2C1_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN4_STATUS__I2C1_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.gpio0_io_clken - gpio0_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of gpio0_io_clk leaf clock. */
/* 0 : gpio0_io_clk clock disabled */
/* 1 : gpio0_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__GPIO0_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN4_STATUS__GPIO0_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__GPIO0_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN4_STATUS__GPIO0_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN4_STATUS__GPIO0_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.nand_io_clken - nand_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of nand_io_clk leaf clock. */
/* 0 : nand_io_clk clock disabled */
/* 1 : nand_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__NAND_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN4_STATUS__NAND_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__NAND_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN4_STATUS__NAND_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN4_STATUS__NAND_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.sdio01_io_clken - sdio01_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sdio01_io_clk leaf clock. */
/* 0 : sdio01_io_clk clock disabled */
/* 1 : sdio01_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.sys2pci2_io_clken - sys2pci2_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of sys2pci2_io_clk leaf clock. */
/* 0 : sys2pci2_io_clk clock disabled */
/* 1 : sys2pci2_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__SYS2PCI2_IO_CLKEN__SHIFT       8
#define CLKC_LEAF_CLK_EN4_STATUS__SYS2PCI2_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__SYS2PCI2_IO_CLKEN__MASK        0x00000100
#define CLKC_LEAF_CLK_EN4_STATUS__SYS2PCI2_IO_CLKEN__INV_MASK    0xFFFFFEFF
#define CLKC_LEAF_CLK_EN4_STATUS__SYS2PCI2_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.sdio01_sdphy01_clken - sdio01_sdphy01_clk clock enable status */
/* If root clock SDPHY01_CLK is disabled this status bit does not reflect the actual status of sdio01_sdphy01_clk leaf clock. */
/* 0 : sdio01_sdphy01_clk clock disabled */
/* 1 : sdio01_sdphy01_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_SDPHY01_CLKEN__SHIFT       9
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_SDPHY01_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_SDPHY01_CLKEN__MASK        0x00000200
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_SDPHY01_CLKEN__INV_MASK    0xFFFFFDFF
#define CLKC_LEAF_CLK_EN4_STATUS__SDIO01_SDPHY01_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.mediam_nand_clken - mediam_nand_clk clock enable status */
/* If root clock NAND_CLK is disabled this status bit does not reflect the actual status of mediam_nand_clk leaf clock. */
/* 0 : mediam_nand_clk clock disabled */
/* 1 : mediam_nand_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAM_NAND_CLKEN__SHIFT       10
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAM_NAND_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAM_NAND_CLKEN__MASK        0x00000400
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAM_NAND_CLKEN__INV_MASK    0xFFFFFBFF
#define CLKC_LEAF_CLK_EN4_STATUS__MEDIAM_NAND_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN4_STATUS.usb0_usb_clken - usb0_usb_clk clock enable status */
/* If root clock USB_CLK is disabled this status bit does not reflect the actual status of usb0_usb_clk leaf clock. */
/* 0 : usb0_usb_clk clock disabled */
/* 1 : usb0_usb_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__USB0_USB_CLKEN__SHIFT       11
#define CLKC_LEAF_CLK_EN4_STATUS__USB0_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__USB0_USB_CLKEN__MASK        0x00000800
#define CLKC_LEAF_CLK_EN4_STATUS__USB0_USB_CLKEN__INV_MASK    0xFFFFF7FF
#define CLKC_LEAF_CLK_EN4_STATUS__USB0_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.usb1_usb_clken - usb1_usb_clk clock enable status */
/* If root clock USB_CLK is disabled this status bit does not reflect the actual status of usb1_usb_clk leaf clock. */
/* 0 : usb1_usb_clk clock disabled */
/* 1 : usb1_usb_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__USB1_USB_CLKEN__SHIFT       12
#define CLKC_LEAF_CLK_EN4_STATUS__USB1_USB_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__USB1_USB_CLKEN__MASK        0x00001000
#define CLKC_LEAF_CLK_EN4_STATUS__USB1_USB_CLKEN__INV_MASK    0xFFFFEFFF
#define CLKC_LEAF_CLK_EN4_STATUS__USB1_USB_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.usbphy0_usbphy_clken - usbphy0_usbphy_clk clock enable status */
/* If root clock USBPHY_CLK is disabled this status bit does not reflect the actual status of usbphy0_usbphy_clk leaf clock. */
/* 0 : usbphy0_usbphy_clk clock disabled */
/* 1 : usbphy0_usbphy_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY0_USBPHY_CLKEN__SHIFT       13
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY0_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY0_USBPHY_CLKEN__MASK        0x00002000
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY0_USBPHY_CLKEN__INV_MASK    0xFFFFDFFF
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY0_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.usbphy1_usbphy_clken - usbphy1_usbphy_clk clock enable status */
/* If root clock USBPHY_CLK is disabled this status bit does not reflect the actual status of usbphy1_usbphy_clk leaf clock. */
/* 0 : usbphy1_usbphy_clk clock disabled */
/* 1 : usbphy1_usbphy_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY1_USBPHY_CLKEN__SHIFT       14
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY1_USBPHY_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY1_USBPHY_CLKEN__MASK        0x00004000
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY1_USBPHY_CLKEN__INV_MASK    0xFFFFBFFF
#define CLKC_LEAF_CLK_EN4_STATUS__USBPHY1_USBPHY_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN4_STATUS.thmediam_io_clken - thmediam_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of thmediam_io_clk leaf clock. */
/* 0 : thmediam_io_clk clock disabled */
/* 1 : thmediam_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN4_STATUS__THMEDIAM_IO_CLKEN__SHIFT       15
#define CLKC_LEAF_CLK_EN4_STATUS__THMEDIAM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN4_STATUS__THMEDIAM_IO_CLKEN__MASK        0x00008000
#define CLKC_LEAF_CLK_EN4_STATUS__THMEDIAM_IO_CLKEN__INV_MASK    0xFFFF7FFF
#define CLKC_LEAF_CLK_EN4_STATUS__THMEDIAM_IO_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN5_SET     0x18620500

/* CLKC_LEAF_CLK_EN5_SET.memc_mem_clken - memc_mem_clk clock enable */
/* Root clock MEM_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable memc_mem_clk clock */
#define CLKC_LEAF_CLK_EN5_SET__MEMC_MEM_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN5_SET__MEMC_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_SET__MEMC_MEM_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN5_SET__MEMC_MEM_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN5_SET__MEMC_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_SET.dapa_mem_clken - dapa_mem_clk clock enable */
/* Root clock MEM_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dapa_mem_clk clock */
#define CLKC_LEAF_CLK_EN5_SET__DAPA_MEM_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN5_SET__DAPA_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_SET__DAPA_MEM_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN5_SET__DAPA_MEM_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN5_SET__DAPA_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_SET.noc_ddrm_r_noc_clken - noc_ddrm_r_noc_clk clock enable */
/* Root clock NOCR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable noc_ddrm_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN5_SET__NOC_DDRM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN5_SET__NOC_DDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_SET__NOC_DDRM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN5_SET__NOC_DDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN5_SET__NOC_DDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_SET.thddrm_r_noc_clken - thddrm_r_noc_clk clock enable */
/* Root clock NOCR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thddrm_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN5_SET__THDDRM_R_NOC_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN5_SET__THDDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_SET__THDDRM_R_NOC_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN5_SET__THDDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN5_SET__THDDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN5_CLR     0x18620504

/* CLKC_LEAF_CLK_EN5_CLR.memc_mem_clken - memc_mem_clk clock disable */
/* 0 : No effect */
/* 1 : Disable memc_mem_clk clock */
#define CLKC_LEAF_CLK_EN5_CLR__MEMC_MEM_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN5_CLR__MEMC_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_CLR__MEMC_MEM_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN5_CLR__MEMC_MEM_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN5_CLR__MEMC_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_CLR.dapa_mem_clken - dapa_mem_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dapa_mem_clk clock */
#define CLKC_LEAF_CLK_EN5_CLR__DAPA_MEM_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN5_CLR__DAPA_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_CLR__DAPA_MEM_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN5_CLR__DAPA_MEM_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN5_CLR__DAPA_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_CLR.noc_ddrm_r_noc_clken - noc_ddrm_r_noc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable noc_ddrm_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN5_CLR__NOC_DDRM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN5_CLR__NOC_DDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_CLR__NOC_DDRM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN5_CLR__NOC_DDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN5_CLR__NOC_DDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_CLR.thddrm_r_noc_clken - thddrm_r_noc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thddrm_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN5_CLR__THDDRM_R_NOC_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN5_CLR__THDDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_CLR__THDDRM_R_NOC_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN5_CLR__THDDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN5_CLR__THDDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN5_STATUS  0x18620508

/* CLKC_LEAF_CLK_EN5_STATUS.memc_mem_clken - memc_mem_clk clock enable status */
/* If root clock MEM_CLK is disabled this status bit does not reflect the actual status of memc_mem_clk leaf clock. */
/* 0 : memc_mem_clk clock disabled */
/* 1 : memc_mem_clk clock enabled */
#define CLKC_LEAF_CLK_EN5_STATUS__MEMC_MEM_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN5_STATUS__MEMC_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_STATUS__MEMC_MEM_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN5_STATUS__MEMC_MEM_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN5_STATUS__MEMC_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_STATUS.dapa_mem_clken - dapa_mem_clk clock enable status */
/* If root clock MEM_CLK is disabled this status bit does not reflect the actual status of dapa_mem_clk leaf clock. */
/* 0 : dapa_mem_clk clock disabled */
/* 1 : dapa_mem_clk clock enabled */
#define CLKC_LEAF_CLK_EN5_STATUS__DAPA_MEM_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN5_STATUS__DAPA_MEM_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_STATUS__DAPA_MEM_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN5_STATUS__DAPA_MEM_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN5_STATUS__DAPA_MEM_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_STATUS.noc_ddrm_r_noc_clken - noc_ddrm_r_noc_clk clock enable status */
/* If root clock NOCR_CLK is disabled this status bit does not reflect the actual status of noc_ddrm_r_noc_clk leaf clock. */
/* 0 : noc_ddrm_r_noc_clk clock disabled */
/* 1 : noc_ddrm_r_noc_clk clock enabled */
#define CLKC_LEAF_CLK_EN5_STATUS__NOC_DDRM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN5_STATUS__NOC_DDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_STATUS__NOC_DDRM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN5_STATUS__NOC_DDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN5_STATUS__NOC_DDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN5_STATUS.thddrm_r_noc_clken - thddrm_r_noc_clk clock enable status */
/* If root clock NOCR_CLK is disabled this status bit does not reflect the actual status of thddrm_r_noc_clk leaf clock. */
/* 0 : thddrm_r_noc_clk clock disabled */
/* 1 : thddrm_r_noc_clk clock enabled */
#define CLKC_LEAF_CLK_EN5_STATUS__THDDRM_R_NOC_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN5_STATUS__THDDRM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN5_STATUS__THDDRM_R_NOC_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN5_STATUS__THDDRM_R_NOC_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN5_STATUS__THDDRM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN6_SET     0x18620518

/* CLKC_LEAF_CLK_EN6_SET.spram1_cpudiv2_clken - spram1_cpudiv2_clk clock enable */
/* Root clock CPU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable spram1_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_SET__SPRAM1_CPUDIV2_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN6_SET__SPRAM1_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_SET__SPRAM1_CPUDIV2_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN6_SET__SPRAM1_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN6_SET__SPRAM1_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_SET.spram2_cpudiv2_clken - spram2_cpudiv2_clk clock enable */
/* Root clock CPU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable spram2_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_SET__SPRAM2_CPUDIV2_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN6_SET__SPRAM2_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_SET__SPRAM2_CPUDIV2_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN6_SET__SPRAM2_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN6_SET__SPRAM2_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_SET.coresight_cpudiv2_clken - coresight_cpudiv2_clk clock enable */
/* Root clock CPU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable coresight_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_CPUDIV2_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_CPUDIV2_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_CPUDIV2_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN6_SET.coresight_tpiu_clken - coresight_tpiu_clk clock enable */
/* Root clock TPIU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable coresight_tpiu_clk clock */
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_TPIU_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_TPIU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_TPIU_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_TPIU_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN6_SET__CORESIGHT_TPIU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_SET.thcpum_cpudiv4_clken - thcpum_cpudiv4_clk clock enable */
/* Root clock CPU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thcpum_cpudiv4_clk clock */
#define CLKC_LEAF_CLK_EN6_SET__THCPUM_CPUDIV4_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN6_SET__THCPUM_CPUDIV4_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_SET__THCPUM_CPUDIV4_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN6_SET__THCPUM_CPUDIV4_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN6_SET__THCPUM_CPUDIV4_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN6_CLR     0x1862051C

/* CLKC_LEAF_CLK_EN6_CLR.spram1_cpudiv2_clken - spram1_cpudiv2_clk clock disable */
/* 0 : No effect */
/* 1 : Disable spram1_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM1_CPUDIV2_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM1_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM1_CPUDIV2_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM1_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM1_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_CLR.spram2_cpudiv2_clken - spram2_cpudiv2_clk clock disable */
/* 0 : No effect */
/* 1 : Disable spram2_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM2_CPUDIV2_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM2_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM2_CPUDIV2_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM2_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN6_CLR__SPRAM2_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_CLR.coresight_cpudiv2_clken - coresight_cpudiv2_clk clock disable */
/* 0 : No effect */
/* 1 : Disable coresight_cpudiv2_clk clock */
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_CPUDIV2_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_CPUDIV2_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_CPUDIV2_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN6_CLR.coresight_tpiu_clken - coresight_tpiu_clk clock disable */
/* 0 : No effect */
/* 1 : Disable coresight_tpiu_clk clock */
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_TPIU_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_TPIU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_TPIU_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_TPIU_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN6_CLR__CORESIGHT_TPIU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_CLR.thcpum_cpudiv4_clken - thcpum_cpudiv4_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thcpum_cpudiv4_clk clock */
#define CLKC_LEAF_CLK_EN6_CLR__THCPUM_CPUDIV4_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN6_CLR__THCPUM_CPUDIV4_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_CLR__THCPUM_CPUDIV4_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN6_CLR__THCPUM_CPUDIV4_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN6_CLR__THCPUM_CPUDIV4_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN6_STATUS  0x18620520

/* CLKC_LEAF_CLK_EN6_STATUS.spram1_cpudiv2_clken - spram1_cpudiv2_clk clock enable status */
/* If root clock CPU_CLK is disabled this status bit does not reflect the actual status of spram1_cpudiv2_clk leaf clock. */
/* 0 : spram1_cpudiv2_clk clock disabled */
/* 1 : spram1_cpudiv2_clk clock enabled */
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM1_CPUDIV2_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM1_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM1_CPUDIV2_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM1_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM1_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_STATUS.spram2_cpudiv2_clken - spram2_cpudiv2_clk clock enable status */
/* If root clock CPU_CLK is disabled this status bit does not reflect the actual status of spram2_cpudiv2_clk leaf clock. */
/* 0 : spram2_cpudiv2_clk clock disabled */
/* 1 : spram2_cpudiv2_clk clock enabled */
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM2_CPUDIV2_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM2_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM2_CPUDIV2_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM2_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN6_STATUS__SPRAM2_CPUDIV2_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_STATUS.coresight_cpudiv2_clken - coresight_cpudiv2_clk clock enable status */
/* If root clock CPU_CLK is disabled this status bit does not reflect the actual status of coresight_cpudiv2_clk leaf clock. */
/* 0 : coresight_cpudiv2_clk clock disabled */
/* 1 : coresight_cpudiv2_clk clock enabled */
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_CPUDIV2_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_CPUDIV2_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_CPUDIV2_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_CPUDIV2_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_CPUDIV2_CLKEN__HW_DEFAULT  0x1

/* CLKC_LEAF_CLK_EN6_STATUS.coresight_tpiu_clken - coresight_tpiu_clk clock enable status */
/* If root clock TPIU_CLK is disabled this status bit does not reflect the actual status of coresight_tpiu_clk leaf clock. */
/* 0 : coresight_tpiu_clk clock disabled */
/* 1 : coresight_tpiu_clk clock enabled */
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_TPIU_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_TPIU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_TPIU_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_TPIU_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN6_STATUS__CORESIGHT_TPIU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN6_STATUS.thcpum_cpudiv4_clken - thcpum_cpudiv4_clk clock enable status */
/* If root clock CPU_CLK is disabled this status bit does not reflect the actual status of thcpum_cpudiv4_clk leaf clock. */
/* 0 : thcpum_cpudiv4_clk clock disabled */
/* 1 : thcpum_cpudiv4_clk clock enabled */
#define CLKC_LEAF_CLK_EN6_STATUS__THCPUM_CPUDIV4_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN6_STATUS__THCPUM_CPUDIV4_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN6_STATUS__THCPUM_CPUDIV4_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN6_STATUS__THCPUM_CPUDIV4_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN6_STATUS__THCPUM_CPUDIV4_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN7_SET     0x18620530

/* CLKC_LEAF_CLK_EN7_SET.graphic_gpu_clken - graphic_gpu_clk clock enable */
/* Root clock GPU_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable graphic_gpu_clk clock */
#define CLKC_LEAF_CLK_EN7_SET__GRAPHIC_GPU_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN7_SET__GRAPHIC_GPU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_SET__GRAPHIC_GPU_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN7_SET__GRAPHIC_GPU_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN7_SET__GRAPHIC_GPU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_SET.vss_sdr_clken - vss_sdr_clk clock enable */
/* Root clock SDR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable vss_sdr_clk clock */
#define CLKC_LEAF_CLK_EN7_SET__VSS_SDR_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN7_SET__VSS_SDR_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_SET__VSS_SDR_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN7_SET__VSS_SDR_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN7_SET__VSS_SDR_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_SET.thgpum_r_noc_clken - thgpum_r_noc_clk clock enable */
/* Root clock NOCR_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thgpum_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN7_SET__THGPUM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN7_SET__THGPUM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_SET__THGPUM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN7_SET__THGPUM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN7_SET__THGPUM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN7_CLR     0x18620534

/* CLKC_LEAF_CLK_EN7_CLR.graphic_gpu_clken - graphic_gpu_clk clock disable */
/* 0 : No effect */
/* 1 : Disable graphic_gpu_clk clock */
#define CLKC_LEAF_CLK_EN7_CLR__GRAPHIC_GPU_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN7_CLR__GRAPHIC_GPU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_CLR__GRAPHIC_GPU_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN7_CLR__GRAPHIC_GPU_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN7_CLR__GRAPHIC_GPU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_CLR.vss_sdr_clken - vss_sdr_clk clock disable */
/* 0 : No effect */
/* 1 : Disable vss_sdr_clk clock */
#define CLKC_LEAF_CLK_EN7_CLR__VSS_SDR_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN7_CLR__VSS_SDR_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_CLR__VSS_SDR_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN7_CLR__VSS_SDR_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN7_CLR__VSS_SDR_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_CLR.thgpum_r_noc_clken - thgpum_r_noc_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thgpum_r_noc_clk clock */
#define CLKC_LEAF_CLK_EN7_CLR__THGPUM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN7_CLR__THGPUM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_CLR__THGPUM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN7_CLR__THGPUM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN7_CLR__THGPUM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN7_STATUS  0x18620538

/* CLKC_LEAF_CLK_EN7_STATUS.graphic_gpu_clken - graphic_gpu_clk clock enable status */
/* If root clock GPU_CLK is disabled this status bit does not reflect the actual status of graphic_gpu_clk leaf clock. */
/* 0 : graphic_gpu_clk clock disabled */
/* 1 : graphic_gpu_clk clock enabled */
#define CLKC_LEAF_CLK_EN7_STATUS__GRAPHIC_GPU_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN7_STATUS__GRAPHIC_GPU_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_STATUS__GRAPHIC_GPU_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN7_STATUS__GRAPHIC_GPU_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN7_STATUS__GRAPHIC_GPU_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_STATUS.vss_sdr_clken - vss_sdr_clk clock enable status */
/* If root clock SDR_CLK is disabled this status bit does not reflect the actual status of vss_sdr_clk leaf clock. */
/* 0 : vss_sdr_clk clock disabled */
/* 1 : vss_sdr_clk clock enabled */
#define CLKC_LEAF_CLK_EN7_STATUS__VSS_SDR_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN7_STATUS__VSS_SDR_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_STATUS__VSS_SDR_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN7_STATUS__VSS_SDR_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN7_STATUS__VSS_SDR_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN7_STATUS.thgpum_r_noc_clken - thgpum_r_noc_clk clock enable status */
/* If root clock NOCR_CLK is disabled this status bit does not reflect the actual status of thgpum_r_noc_clk leaf clock. */
/* 0 : thgpum_r_noc_clk clock disabled */
/* 1 : thgpum_r_noc_clk clock enabled */
#define CLKC_LEAF_CLK_EN7_STATUS__THGPUM_R_NOC_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN7_STATUS__THGPUM_R_NOC_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN7_STATUS__THGPUM_R_NOC_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN7_STATUS__THGPUM_R_NOC_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN7_STATUS__THGPUM_R_NOC_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Set */
/* Writing 1 to a bit enables the associated unit clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN8_SET     0x18620548

/* CLKC_LEAF_CLK_EN8_SET.a7ca_btslow_clken - a7ca_btslow_clk clock enable */
/* Root clock BTSLOW_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable a7ca_btslow_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSLOW_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSLOW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSLOW_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSLOW_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSLOW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.a7ca_btss_clken - a7ca_btss_clk clock enable */
/* Root clock BTSS_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable a7ca_btss_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSS_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSS_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSS_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN8_SET__A7CA_BTSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.dmac4_io_clken - dmac4_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable dmac4_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__DMAC4_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN8_SET__DMAC4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__DMAC4_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN8_SET__DMAC4_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN8_SET__DMAC4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.uart6_io_clken - uart6_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable uart6_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__UART6_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN8_SET__UART6_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__UART6_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN8_SET__UART6_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN8_SET__UART6_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.usp3_io_clken - usp3_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable usp3_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__USP3_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN8_SET__USP3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__USP3_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN8_SET__USP3_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN8_SET__USP3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.a7ca_io_clken - a7ca_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable a7ca_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__A7CA_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN8_SET__A7CA_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__A7CA_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN8_SET__A7CA_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN8_SET__A7CA_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.noc_btm_io_clken - noc_btm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable noc_btm_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__NOC_BTM_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN8_SET__NOC_BTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__NOC_BTM_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN8_SET__NOC_BTM_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN8_SET__NOC_BTM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_SET.thbtm_io_clken - thbtm_io_clk clock enable */
/* Root clock IO_CLK must be enabled for this clock to be enabled */
/* 0 : No effect */
/* 1 : Enable thbtm_io_clk clock */
#define CLKC_LEAF_CLK_EN8_SET__THBTM_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN8_SET__THBTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_SET__THBTM_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN8_SET__THBTM_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN8_SET__THBTM_IO_CLKEN__HW_DEFAULT  0x1

/* UNit Clock Enable Clear */
/* Writing 1 to a bit disables the associated root clock.
 Writing 0 to a bit has no effect.
Reading this registers returns the LEAF_CLK_EN status.
 This register controls clock gating at the receiving unit.
 Use ROOT_CLK_EN*_SET/CLR registers to control clocks at the root of the clock tree in the Clock Controller . */
#define CLKC_LEAF_CLK_EN8_CLR     0x1862054C

/* CLKC_LEAF_CLK_EN8_CLR.a7ca_btslow_clken - a7ca_btslow_clk clock disable */
/* 0 : No effect */
/* 1 : Disable a7ca_btslow_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSLOW_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSLOW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSLOW_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSLOW_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSLOW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.a7ca_btss_clken - a7ca_btss_clk clock disable */
/* 0 : No effect */
/* 1 : Disable a7ca_btss_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSS_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSS_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSS_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_BTSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.dmac4_io_clken - dmac4_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable dmac4_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__DMAC4_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN8_CLR__DMAC4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__DMAC4_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN8_CLR__DMAC4_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN8_CLR__DMAC4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.uart6_io_clken - uart6_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable uart6_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__UART6_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN8_CLR__UART6_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__UART6_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN8_CLR__UART6_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN8_CLR__UART6_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.usp3_io_clken - usp3_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable usp3_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__USP3_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN8_CLR__USP3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__USP3_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN8_CLR__USP3_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN8_CLR__USP3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.a7ca_io_clken - a7ca_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable a7ca_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN8_CLR__A7CA_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.noc_btm_io_clken - noc_btm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable noc_btm_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__NOC_BTM_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN8_CLR__NOC_BTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__NOC_BTM_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN8_CLR__NOC_BTM_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN8_CLR__NOC_BTM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_CLR.thbtm_io_clken - thbtm_io_clk clock disable */
/* 0 : No effect */
/* 1 : Disable thbtm_io_clk clock */
#define CLKC_LEAF_CLK_EN8_CLR__THBTM_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN8_CLR__THBTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_CLR__THBTM_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN8_CLR__THBTM_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN8_CLR__THBTM_IO_CLKEN__HW_DEFAULT  0x1

/* Unit Clock Enable Status */
/* Status of clock gating at the receiving unit.
 Use ROOT_CLK_EN*_STATUS registers to observe status of clocks at the clock tree root in the Clock Controller. */
#define CLKC_LEAF_CLK_EN8_STATUS  0x18620550

/* CLKC_LEAF_CLK_EN8_STATUS.a7ca_btslow_clken - a7ca_btslow_clk clock enable status */
/* If root clock BTSLOW_CLK is disabled this status bit does not reflect the actual status of a7ca_btslow_clk leaf clock. */
/* 0 : a7ca_btslow_clk clock disabled */
/* 1 : a7ca_btslow_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSLOW_CLKEN__SHIFT       0
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSLOW_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSLOW_CLKEN__MASK        0x00000001
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSLOW_CLKEN__INV_MASK    0xFFFFFFFE
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSLOW_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.a7ca_btss_clken - a7ca_btss_clk clock enable status */
/* If root clock BTSS_CLK is disabled this status bit does not reflect the actual status of a7ca_btss_clk leaf clock. */
/* 0 : a7ca_btss_clk clock disabled */
/* 1 : a7ca_btss_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSS_CLKEN__SHIFT       1
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSS_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSS_CLKEN__MASK        0x00000002
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSS_CLKEN__INV_MASK    0xFFFFFFFD
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_BTSS_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.dmac4_io_clken - dmac4_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of dmac4_io_clk leaf clock. */
/* 0 : dmac4_io_clk clock disabled */
/* 1 : dmac4_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__DMAC4_IO_CLKEN__SHIFT       2
#define CLKC_LEAF_CLK_EN8_STATUS__DMAC4_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__DMAC4_IO_CLKEN__MASK        0x00000004
#define CLKC_LEAF_CLK_EN8_STATUS__DMAC4_IO_CLKEN__INV_MASK    0xFFFFFFFB
#define CLKC_LEAF_CLK_EN8_STATUS__DMAC4_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.uart6_io_clken - uart6_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of uart6_io_clk leaf clock. */
/* 0 : uart6_io_clk clock disabled */
/* 1 : uart6_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__UART6_IO_CLKEN__SHIFT       3
#define CLKC_LEAF_CLK_EN8_STATUS__UART6_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__UART6_IO_CLKEN__MASK        0x00000008
#define CLKC_LEAF_CLK_EN8_STATUS__UART6_IO_CLKEN__INV_MASK    0xFFFFFFF7
#define CLKC_LEAF_CLK_EN8_STATUS__UART6_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.usp3_io_clken - usp3_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of usp3_io_clk leaf clock. */
/* 0 : usp3_io_clk clock disabled */
/* 1 : usp3_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__USP3_IO_CLKEN__SHIFT       4
#define CLKC_LEAF_CLK_EN8_STATUS__USP3_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__USP3_IO_CLKEN__MASK        0x00000010
#define CLKC_LEAF_CLK_EN8_STATUS__USP3_IO_CLKEN__INV_MASK    0xFFFFFFEF
#define CLKC_LEAF_CLK_EN8_STATUS__USP3_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.a7ca_io_clken - a7ca_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of a7ca_io_clk leaf clock. */
/* 0 : a7ca_io_clk clock disabled */
/* 1 : a7ca_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_IO_CLKEN__SHIFT       5
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_IO_CLKEN__MASK        0x00000020
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_IO_CLKEN__INV_MASK    0xFFFFFFDF
#define CLKC_LEAF_CLK_EN8_STATUS__A7CA_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.noc_btm_io_clken - noc_btm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of noc_btm_io_clk leaf clock. */
/* 0 : noc_btm_io_clk clock disabled */
/* 1 : noc_btm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__NOC_BTM_IO_CLKEN__SHIFT       6
#define CLKC_LEAF_CLK_EN8_STATUS__NOC_BTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__NOC_BTM_IO_CLKEN__MASK        0x00000040
#define CLKC_LEAF_CLK_EN8_STATUS__NOC_BTM_IO_CLKEN__INV_MASK    0xFFFFFFBF
#define CLKC_LEAF_CLK_EN8_STATUS__NOC_BTM_IO_CLKEN__HW_DEFAULT  0x0

/* CLKC_LEAF_CLK_EN8_STATUS.thbtm_io_clken - thbtm_io_clk clock enable status */
/* If root clock IO_CLK is disabled this status bit does not reflect the actual status of thbtm_io_clk leaf clock. */
/* 0 : thbtm_io_clk clock disabled */
/* 1 : thbtm_io_clk clock enabled */
#define CLKC_LEAF_CLK_EN8_STATUS__THBTM_IO_CLKEN__SHIFT       7
#define CLKC_LEAF_CLK_EN8_STATUS__THBTM_IO_CLKEN__WIDTH       1
#define CLKC_LEAF_CLK_EN8_STATUS__THBTM_IO_CLKEN__MASK        0x00000080
#define CLKC_LEAF_CLK_EN8_STATUS__THBTM_IO_CLKEN__INV_MASK    0xFFFFFF7F
#define CLKC_LEAF_CLK_EN8_STATUS__THBTM_IO_CLKEN__HW_DEFAULT  0x1


#endif  // __CLKC_H__
