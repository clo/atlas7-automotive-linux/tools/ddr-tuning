/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
 
#ifndef _SOC_FC__G2D
#define _SOC_FC__G2D

#define _G2D_MODULE_BASE                         0x17010000

#define rG2D_FB_BASE                              (*(volatile unsigned *) 0x17010000U)
#define rG2D_ENG_STATUS                           (*(volatile unsigned *) 0x17010004U)
#define rG2D_ENG_CTRL                             (*(volatile unsigned *) 0x17010008U)
#define rG2D_RB_OFFSET                            (*(volatile unsigned *) 0x1701000CU)
#define rG2D_RB_LENGTH                            (*(volatile unsigned *) 0x17010010U)
#define rG2D_RB_RD_PTR                            (*(volatile unsigned *) 0x17010014U)
#define rG2D_RB_WR_PTR                            (*(volatile unsigned *) 0x17010018U)
#define rG2D_DST_OFFSET                           (*(volatile unsigned *) 0x1701001CU)
#define rG2D_DST_FORMAT                           (*(volatile unsigned *) 0x17010020U)
#define rG2D_DST_LT                               (*(volatile unsigned *) 0x17010024U)
#define rG2D_DST_RB                               (*(volatile unsigned *) 0x17010028U)
#define rG2D_CLIP_LT                              (*(volatile unsigned *) 0x1701002CU)
#define rG2D_CLIP_RB                              (*(volatile unsigned *) 0x17010030U)
#define rG2D_SRC_OFFSET                           (*(volatile unsigned *) 0x17010034U)
#define rG2D_SRC_FORMAT                           (*(volatile unsigned *) 0x17010038U)
#define rG2D_SRC_LT                               (*(volatile unsigned *) 0x1701003CU)
#define rG2D_SRC_RB                               (*(volatile unsigned *) 0x17010040U)
#define rG2D_PAT_OFFSET                           (*(volatile unsigned *) 0x17010044U)
#define rG2D_FILLCOLOR                            (*(volatile unsigned *) 0x17010048U)
#define rG2D_COLOR_KEY                            (*(volatile unsigned *) 0x1701004CU)
#define rG2D_GBL_ALPHA                            (*(volatile unsigned *) 0x17010050U)
#define rG2D_DRAW_CTL                             (*(volatile unsigned *) 0x17010054U)
#define rG2D_BLT_TIME                             (*(volatile unsigned *) 0x17010058U)
#define rG2D_DEBUGREGISTER0                       (*(volatile unsigned *) 0x1701005CU)
#define rG2D_DEBUGREGISTER1                       (*(volatile unsigned *) 0x17010060U)
#define rG2D_DEBUGREGISTER2                       (*(volatile unsigned *) 0x17010064U)
#define rG2D_INTERRUPT_ENABLE                     (*(volatile unsigned *) 0x17010080U)
#define rG2D_INTERRUPT_CLEAR                      (*(volatile unsigned *) 0x17010084U)
#define rG2D_INTERRUPT_STATUS                     (*(volatile unsigned *) 0x17010088U)
#define rG2D_HWRESERVED                           (*(volatile unsigned *) 0x1701008CU)
#endif // _SOC_FC__G2D 

