/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __IPC_H__
#define __IPC_H__



/* IPC */
/* ==================================================================== */

/* IPC_TRGT1_INIT0_1 */
/* IPC Target1 Initiator0 Interrupt 1 Register */
#define IPC_TRGT1_INIT0_1         0x13240000

/* IPC_TRGT1_INIT0_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target1, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT0_1__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT0_1__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT0_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT0_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT0_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT1_INIT0_2 */
/* IPC Target1 Initiator0 Interrupt 2 Register */
#define IPC_TRGT1_INIT0_2         0x13240004

/* IPC_TRGT1_INIT0_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target1, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT0_2__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT0_2__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT0_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT0_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT0_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT0_1 */
/* IPC Target2 Initiator0 Interrupt 1 Register */
#define IPC_TRGT2_INIT0_1         0x13240008

/* IPC_TRGT2_INIT0_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target2, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT0_1__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT0_1__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT0_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT0_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT0_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT0_2 */
/* IPC Target2 Initiator0 Interrupt 2 Register */
#define IPC_TRGT2_INIT0_2         0x1324000C

/* IPC_TRGT2_INIT0_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target2, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT0_2__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT0_2__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT0_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT0_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT0_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT0_1 */
/* IPC Target3 Initiator0 Interrupt 1 Register */
#define IPC_TRGT3_INIT0_1         0x13240010

/* IPC_TRGT3_INIT0_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target3, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT0_1__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT0_1__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT0_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT0_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT0_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT0_2 */
/* IPC Target3 Initiator0 Interrupt 2 Register */
#define IPC_TRGT3_INIT0_2         0x13240014

/* IPC_TRGT3_INIT0_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator0 to target3, and permitter by initiator0 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT0_2__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT0_2__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT0_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT0_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT0_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT1_1 */
/* IPC Target0 Initiator1 Interrupt 1 Register */
#define IPC_TRGT0_INIT1_1         0x13240100

/* IPC_TRGT0_INIT1_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target0, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT1_1__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT1_1__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT1_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT1_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT1_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT1_2 */
/* IPC Target0 Initiator1 Interrupt 2 Register */
#define IPC_TRGT0_INIT1_2         0x13240104

/* IPC_TRGT0_INIT1_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target0, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT1_2__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT1_2__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT1_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT1_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT1_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT1_1 */
/* IPC Target2 Initiator1 Interrupt 1 Register */
#define IPC_TRGT2_INIT1_1         0x13240108

/* IPC_TRGT2_INIT1_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target2, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT1_1__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT1_1__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT1_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT1_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT1_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT1_2 */
/* IPC Target2 Initiator1 Interrupt 2 Register */
#define IPC_TRGT2_INIT1_2         0x1324010C

/* IPC_TRGT2_INIT1_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target2, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT1_2__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT1_2__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT1_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT1_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT1_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT1_1 */
/* IPC Target3 Initiator1 Interrupt 1 Register */
#define IPC_TRGT3_INIT1_1         0x13240110

/* IPC_TRGT3_INIT1_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target3, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT1_1__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT1_1__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT1_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT1_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT1_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT1_2 */
/* IPC Target3 Initiator1 Interrupt 2 Register */
#define IPC_TRGT3_INIT1_2         0x13240114

/* IPC_TRGT3_INIT1_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator1 to target3, and permitter by initiator1 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT1_2__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT1_2__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT1_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT1_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT1_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT2_1 */
/* IPC Target0 Initiator2 Interrupt 1 Register */
#define IPC_TRGT0_INIT2_1         0x13240200

/* IPC_TRGT0_INIT2_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target0, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT2_1__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT2_1__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT2_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT2_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT2_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT2_2 */
/* IPC Target0 Initiator2 Interrupt 2 Register */
#define IPC_TRGT0_INIT2_2         0x13240204

/* IPC_TRGT0_INIT2_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target0, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT2_2__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT2_2__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT2_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT2_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT2_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT1_INIT2_1 */
/* IPC Target1 Initiator2 Interrupt 1 Register */
#define IPC_TRGT1_INIT2_1         0x13240208

/* IPC_TRGT1_INIT2_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target1, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT2_1__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT2_1__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT2_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT2_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT2_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT1_INIT2_2 */
/* IPC Target1 Initiator2 Interrupt 2 Register */
#define IPC_TRGT1_INIT2_2         0x1324020C

/* IPC_TRGT1_INIT2_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target1, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT2_2__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT2_2__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT2_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT2_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT2_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT2_1 */
/* IPC Target3 Initiator2 Interrupt 1 Register */
#define IPC_TRGT3_INIT2_1         0x13240210

/* IPC_TRGT3_INIT2_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target3, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT2_1__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT2_1__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT2_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT2_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT2_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT3_INIT2_2 */
/* IPC Target3 Initiator2 Interrupt 2 Register */
#define IPC_TRGT3_INIT2_2         0x13240214

/* IPC_TRGT3_INIT2_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator2 to target3, and permitter by initiator2 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT3_INIT2_2__INTERRUPT__SHIFT       0
#define IPC_TRGT3_INIT2_2__INTERRUPT__WIDTH       1
#define IPC_TRGT3_INIT2_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT3_INIT2_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT3_INIT2_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT3_1 */
/* IPC Target0 Initiator3 Interrupt 1 Register */
#define IPC_TRGT0_INIT3_1         0x13240300

/* IPC_TRGT0_INIT3_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target0, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT3_1__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT3_1__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT3_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT3_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT3_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT0_INIT3_2 */
/* IPC Target0 Initiator3 Interrupt 2 Register */
#define IPC_TRGT0_INIT3_2         0x13240304

/* IPC_TRGT0_INIT3_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target0, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT0_INIT3_2__INTERRUPT__SHIFT       0
#define IPC_TRGT0_INIT3_2__INTERRUPT__WIDTH       1
#define IPC_TRGT0_INIT3_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT0_INIT3_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT0_INIT3_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT1_INIT3_1 */
/* IPC Target1 Initiator3 Interrupt 1 Register */
#define IPC_TRGT1_INIT3_1         0x13240308

/* IPC_TRGT1_INIT3_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target1, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT3_1__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT3_1__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT3_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT3_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT3_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT1_INIT3_2 */
/* IPC Target1 Initiator3 Interrupt 2 Register */
#define IPC_TRGT1_INIT3_2         0x1324030C

/* IPC_TRGT1_INIT3_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target1, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT1_INIT3_2__INTERRUPT__SHIFT       0
#define IPC_TRGT1_INIT3_2__INTERRUPT__WIDTH       1
#define IPC_TRGT1_INIT3_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT1_INIT3_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT1_INIT3_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT3_1 */
/* IPC Target2 Initiator3 Interrupt 1 Register */
#define IPC_TRGT2_INIT3_1         0x13240310

/* IPC_TRGT2_INIT3_1.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target2, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT3_1__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT3_1__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT3_1__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT3_1__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT3_1__INTERRUPT__HW_DEFAULT  0x0

/* IPC_TRGT2_INIT3_2 */
/* IPC Target2 Initiator3 Interrupt 2 Register */
#define IPC_TRGT2_INIT3_2         0x13240314

/* IPC_TRGT2_INIT3_2.INTERRUPT - Writing for this register will cause a level interrupt assertion from initiator3 to target2, and permitter by initiator3 only. Reading from this register will clear the interrupt, and permitted by all. */
#define IPC_TRGT2_INIT3_2__INTERRUPT__SHIFT       0
#define IPC_TRGT2_INIT3_2__INTERRUPT__WIDTH       1
#define IPC_TRGT2_INIT3_2__INTERRUPT__MASK        0x00000001
#define IPC_TRGT2_INIT3_2__INTERRUPT__INV_MASK    0xFFFFFFFE
#define IPC_TRGT2_INIT3_2__INTERRUPT__HW_DEFAULT  0x0

/* IPC_SPINLOCK_RD_DEBUG */
/* IPC Spinlock Read Debug Register */
#define IPC_SPINLOCK_RD_DEBUG     0x13240400

/* IPC_SPINLOCK_RD_DEBUG.DEBUG_MODE - 1'b0 : Operational mode, reading from a TestAndSet tries to lock it. The returned read data is 0 if it was already locked and 1 if it was successfully locked.1'b1 : Debug mode, reading from a TestAndSet will simply return its value without changing its state. */
#define IPC_SPINLOCK_RD_DEBUG__DEBUG_MODE__SHIFT       0
#define IPC_SPINLOCK_RD_DEBUG__DEBUG_MODE__WIDTH       1
#define IPC_SPINLOCK_RD_DEBUG__DEBUG_MODE__MASK        0x00000001
#define IPC_SPINLOCK_RD_DEBUG__DEBUG_MODE__INV_MASK    0xFFFFFFFE
#define IPC_SPINLOCK_RD_DEBUG__DEBUG_MODE__HW_DEFAULT  0x0

/* IPC_TESTANDSET_0 */
/* IPC TEST_AND_SET_0 Register */
#define IPC_TESTANDSET_0          0x13240404

/* IPC_TESTANDSET_0.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_0__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_0__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_0__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_0__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_0__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_1 */
/* IPC TEST_AND_SET_1 Register */
#define IPC_TESTANDSET_1          0x13240408

/* IPC_TESTANDSET_1.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_1__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_1__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_1__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_1__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_1__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_2 */
/* IPC TEST_AND_SET_2 Register */
#define IPC_TESTANDSET_2          0x1324040C

/* IPC_TESTANDSET_2.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_2__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_2__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_2__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_2__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_2__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_3 */
/* IPC TEST_AND_SET_3 Register */
#define IPC_TESTANDSET_3          0x13240410

/* IPC_TESTANDSET_3.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_3__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_3__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_3__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_3__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_3__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_4 */
/* IPC TEST_AND_SET_4 Register */
#define IPC_TESTANDSET_4          0x13240414

/* IPC_TESTANDSET_4.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_4__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_4__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_4__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_4__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_4__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_5 */
/* IPC TEST_AND_SET_5 Register */
#define IPC_TESTANDSET_5          0x13240418

/* IPC_TESTANDSET_5.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_5__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_5__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_5__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_5__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_5__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_6 */
/* IPC TEST_AND_SET_6 Register */
#define IPC_TESTANDSET_6          0x1324041C

/* IPC_TESTANDSET_6.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_6__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_6__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_6__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_6__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_6__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_7 */
/* IPC TEST_AND_SET_7 Register */
#define IPC_TESTANDSET_7          0x13240420

/* IPC_TESTANDSET_7.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_7__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_7__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_7__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_7__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_7__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_8 */
/* IPC TEST_AND_SET_8 Register */
#define IPC_TESTANDSET_8          0x13240424

/* IPC_TESTANDSET_8.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_8__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_8__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_8__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_8__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_8__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_9 */
/* IPC TEST_AND_SET_9 Register */
#define IPC_TESTANDSET_9          0x13240428

/* IPC_TESTANDSET_9.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_9__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_9__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_9__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_9__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_9__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_10 */
/* IPC TEST_AND_SET_10 Register */
#define IPC_TESTANDSET_10         0x1324042C

/* IPC_TESTANDSET_10.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_10__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_10__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_10__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_10__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_10__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_11 */
/* IPC TEST_AND_SET_11 Register */
#define IPC_TESTANDSET_11         0x13240430

/* IPC_TESTANDSET_11.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_11__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_11__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_11__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_11__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_11__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_12 */
/* IPC TEST_AND_SET_12 Register */
#define IPC_TESTANDSET_12         0x13240434

/* IPC_TESTANDSET_12.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_12__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_12__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_12__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_12__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_12__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_13 */
/* IPC TEST_AND_SET_13 Register */
#define IPC_TESTANDSET_13         0x13240438

/* IPC_TESTANDSET_13.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_13__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_13__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_13__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_13__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_13__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_14 */
/* IPC TEST_AND_SET_14 Register */
#define IPC_TESTANDSET_14         0x1324043C

/* IPC_TESTANDSET_14.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_14__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_14__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_14__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_14__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_14__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_15 */
/* IPC TEST_AND_SET_15 Register */
#define IPC_TESTANDSET_15         0x13240440

/* IPC_TESTANDSET_15.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_15__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_15__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_15__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_15__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_15__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_16 */
/* IPC TEST_AND_SET_16 Register */
#define IPC_TESTANDSET_16         0x13240444

/* IPC_TESTANDSET_16.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_16__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_16__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_16__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_16__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_16__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_17 */
/* IPC TEST_AND_SET_17 Register */
#define IPC_TESTANDSET_17         0x13240448

/* IPC_TESTANDSET_17.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_17__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_17__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_17__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_17__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_17__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_18 */
/* IPC TEST_AND_SET_18 Register */
#define IPC_TESTANDSET_18         0x1324044C

/* IPC_TESTANDSET_18.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_18__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_18__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_18__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_18__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_18__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_19 */
/* IPC TEST_AND_SET_19 Register */
#define IPC_TESTANDSET_19         0x13240450

/* IPC_TESTANDSET_19.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_19__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_19__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_19__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_19__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_19__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_20 */
/* IPC TEST_AND_SET_20 Register */
#define IPC_TESTANDSET_20         0x13240454

/* IPC_TESTANDSET_20.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_20__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_20__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_20__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_20__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_20__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_21 */
/* IPC TEST_AND_SET_21 Register */
#define IPC_TESTANDSET_21         0x13240458

/* IPC_TESTANDSET_21.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_21__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_21__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_21__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_21__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_21__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_22 */
/* IPC TEST_AND_SET_22 Register */
#define IPC_TESTANDSET_22         0x1324045C

/* IPC_TESTANDSET_22.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_22__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_22__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_22__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_22__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_22__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_23 */
/* IPC TEST_AND_SET_23 Register */
#define IPC_TESTANDSET_23         0x13240460

/* IPC_TESTANDSET_23.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_23__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_23__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_23__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_23__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_23__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_24 */
/* IPC TEST_AND_SET_24 Register */
#define IPC_TESTANDSET_24         0x13240464

/* IPC_TESTANDSET_24.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_24__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_24__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_24__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_24__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_24__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_25 */
/* IPC TEST_AND_SET_25 Register */
#define IPC_TESTANDSET_25         0x13240468

/* IPC_TESTANDSET_25.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_25__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_25__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_25__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_25__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_25__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_26 */
/* IPC TEST_AND_SET_26 Register */
#define IPC_TESTANDSET_26         0x1324046C

/* IPC_TESTANDSET_26.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_26__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_26__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_26__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_26__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_26__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_27 */
/* IPC TEST_AND_SET_27 Register */
#define IPC_TESTANDSET_27         0x13240470

/* IPC_TESTANDSET_27.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_27__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_27__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_27__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_27__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_27__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_28 */
/* IPC TEST_AND_SET_28 Register */
#define IPC_TESTANDSET_28         0x13240474

/* IPC_TESTANDSET_28.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_28__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_28__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_28__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_28__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_28__SPINLOCK__HW_DEFAULT  0x0

/* IPC_TESTANDSET_29 */
/* IPC TEST_AND_SET_29 Register */
#define IPC_TESTANDSET_29         0x13240478

/* IPC_TESTANDSET_29.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. Reading will TEST, i.e. try to lock. */
#define IPC_TESTANDSET_29__SPINLOCK__SHIFT       0
#define IPC_TESTANDSET_29__SPINLOCK__WIDTH       1
#define IPC_TESTANDSET_29__SPINLOCK__MASK        0x00000001
#define IPC_TESTANDSET_29__SPINLOCK__INV_MASK    0xFFFFFFFE
#define IPC_TESTANDSET_29__SPINLOCK__HW_DEFAULT  0x0


#endif  // __IPC_H__
