/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __RETAINREGS_H__
#define __RETAINREGS_H__



/* RETAIN_REGS */
/* ==================================================================== */

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD1         0x188D0000

/* PWRC_SCRATCH_PAD1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD1__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD2         0x188D0004

/* PWRC_SCRATCH_PAD2.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD2__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD2__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD2__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD2__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD2__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD3         0x188D0008

/* PWRC_SCRATCH_PAD3.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD3__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD3__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD3__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD3__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD3__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD4         0x188D000C

/* PWRC_SCRATCH_PAD4.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD4__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD4__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD4__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD4__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD4__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD5         0x188D0010

/* PWRC_SCRATCH_PAD5.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD5__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD5__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD5__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD5__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD5__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD6         0x188D0014

/* PWRC_SCRATCH_PAD6.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD6__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD6__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD6__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD6__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD6__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD7         0x188D0018

/* PWRC_SCRATCH_PAD7.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD7__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD7__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD7__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD7__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD7__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD8         0x188D001C

/* PWRC_SCRATCH_PAD8.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD8__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD8__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD8__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD8__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD8__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD9         0x188D0020

/* PWRC_SCRATCH_PAD9.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD9__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD9__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD9__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD9__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD9__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD10        0x188D0024

/* PWRC_SCRATCH_PAD10.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD10__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD10__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD10__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD10__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD10__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD11        0x188D0028

/* PWRC_SCRATCH_PAD11.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD11__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD11__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD11__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD11__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD11__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad Register */
#define PWRC_SCRATCH_PAD12        0x188D002C

/* PWRC_SCRATCH_PAD12.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD12__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD12__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD12__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD12__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD12__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC RTC Bootstrap0 Register */
/* This register is W1, i.e. can be written by SW only once after Coldboot. */
#define PWRC_RTC_BOOTSTRAP_0      0x188D0030

/* PWRC_RTC_BOOTSTRAP_0.PWRC_RTC_BOOTSTRAP0 - Bootstrap bits (0-31) contain security definitions for NoC, etc. 
 Some of these bits may directly affect HW, so must be implemented as registers. 
 See OTP mapping CS-309596-DD for more descriptions. 
 Loaded before M3 reset is released. */
#define PWRC_RTC_BOOTSTRAP_0__PWRC_RTC_BOOTSTRAP0__SHIFT       0
#define PWRC_RTC_BOOTSTRAP_0__PWRC_RTC_BOOTSTRAP0__WIDTH       32
#define PWRC_RTC_BOOTSTRAP_0__PWRC_RTC_BOOTSTRAP0__MASK        0xFFFFFFFF
#define PWRC_RTC_BOOTSTRAP_0__PWRC_RTC_BOOTSTRAP0__INV_MASK    0x00000000
#define PWRC_RTC_BOOTSTRAP_0__PWRC_RTC_BOOTSTRAP0__HW_DEFAULT  0x0

/* PWRC RTC Bootstrap0 Register */
/* This register is W1, i.e. can be written by SW only once after Coldboot. */
#define PWRC_RTC_BOOTSTRAP_1      0x188D0034

/* PWRC_RTC_BOOTSTRAP_1.PWRC_RTC_BOOTSTRAP1 - Bootstrap bits (32-63) contain security definitions for NoC, etc. 
 Some of these bits may directly affect HW, so must be implemented as registers. 
 See OTP mapping CS-309596-DD for more descriptions. 
 Loaded before M3 reset is released. */
#define PWRC_RTC_BOOTSTRAP_1__PWRC_RTC_BOOTSTRAP1__SHIFT       0
#define PWRC_RTC_BOOTSTRAP_1__PWRC_RTC_BOOTSTRAP1__WIDTH       32
#define PWRC_RTC_BOOTSTRAP_1__PWRC_RTC_BOOTSTRAP1__MASK        0xFFFFFFFF
#define PWRC_RTC_BOOTSTRAP_1__PWRC_RTC_BOOTSTRAP1__INV_MASK    0x00000000
#define PWRC_RTC_BOOTSTRAP_1__PWRC_RTC_BOOTSTRAP1__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_0            0x188D0038

/* PWRC_M3_HASH_0.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_0__HASH__SHIFT       0
#define PWRC_M3_HASH_0__HASH__WIDTH       32
#define PWRC_M3_HASH_0__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_0__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_0__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_1            0x188D003C

/* PWRC_M3_HASH_1.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_1__HASH__SHIFT       0
#define PWRC_M3_HASH_1__HASH__WIDTH       32
#define PWRC_M3_HASH_1__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_1__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_1__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_2            0x188D0040

/* PWRC_M3_HASH_2.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_2__HASH__SHIFT       0
#define PWRC_M3_HASH_2__HASH__WIDTH       32
#define PWRC_M3_HASH_2__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_2__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_2__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_3            0x188D0044

/* PWRC_M3_HASH_3.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_3__HASH__SHIFT       0
#define PWRC_M3_HASH_3__HASH__WIDTH       32
#define PWRC_M3_HASH_3__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_3__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_3__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_4            0x188D0048

/* PWRC_M3_HASH_4.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_4__HASH__SHIFT       0
#define PWRC_M3_HASH_4__HASH__WIDTH       32
#define PWRC_M3_HASH_4__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_4__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_4__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_5            0x188D004C

/* PWRC_M3_HASH_5.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_5__HASH__SHIFT       0
#define PWRC_M3_HASH_5__HASH__WIDTH       32
#define PWRC_M3_HASH_5__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_5__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_5__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_6            0x188D0050

/* PWRC_M3_HASH_6.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_6__HASH__SHIFT       0
#define PWRC_M3_HASH_6__HASH__WIDTH       32
#define PWRC_M3_HASH_6__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_6__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_6__HASH__HW_DEFAULT  0x0

/* PWRC M3 HASH Register */
/* HASH result, write-once upon cold-boot. 
 HASH0-HASH3 are WBS, i.e. once SW has been written, HW modification is blocked. 
 <b>Note:</b> In order to ensure consistency, PWRC_M3_HASH_0 through PWRC_M3_HASH_7 registers must be updated atomically, the same as for HW update from bootstrap. 
 The values written by SW into the registers should only be updated to HW (and the write-once and/or WBS status updated) when the last register PWRC_M3_HASH_7 is written by SW. */
#define PWRC_M3_HASH_7            0x188D0054

/* PWRC_M3_HASH_7.HASH - HASH result for M3 usage (independent secure-boot). 
 Data can be written only during cold-boot, and can always be read. */
#define PWRC_M3_HASH_7__HASH__SHIFT       0
#define PWRC_M3_HASH_7__HASH__WIDTH       32
#define PWRC_M3_HASH_7__HASH__MASK        0xFFFFFFFF
#define PWRC_M3_HASH_7__HASH__INV_MASK    0x00000000
#define PWRC_M3_HASH_7__HASH__HW_DEFAULT  0x0

/* Default parameters for QSPI Register */
/* Software should store here the non-volatile parameters of the SPI flash-device. */
#define PWRC_QSPI_PARAMS          0x188D0058

/* PWRC_QSPI_PARAMS.JEDEC_ID_MAN - JEDEC Manufacturer ID */
#define PWRC_QSPI_PARAMS__JEDEC_ID_MAN__SHIFT       0
#define PWRC_QSPI_PARAMS__JEDEC_ID_MAN__WIDTH       8
#define PWRC_QSPI_PARAMS__JEDEC_ID_MAN__MASK        0x000000FF
#define PWRC_QSPI_PARAMS__JEDEC_ID_MAN__INV_MASK    0xFFFFFF00
#define PWRC_QSPI_PARAMS__JEDEC_ID_MAN__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.JEDEC_ID_TYPE - JEDEC Type ID */
#define PWRC_QSPI_PARAMS__JEDEC_ID_TYPE__SHIFT       8
#define PWRC_QSPI_PARAMS__JEDEC_ID_TYPE__WIDTH       8
#define PWRC_QSPI_PARAMS__JEDEC_ID_TYPE__MASK        0x0000FF00
#define PWRC_QSPI_PARAMS__JEDEC_ID_TYPE__INV_MASK    0xFFFF00FF
#define PWRC_QSPI_PARAMS__JEDEC_ID_TYPE__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.JEDEC_ID_DEN - JEDEC Density ID */
#define PWRC_QSPI_PARAMS__JEDEC_ID_DEN__SHIFT       16
#define PWRC_QSPI_PARAMS__JEDEC_ID_DEN__WIDTH       9
#define PWRC_QSPI_PARAMS__JEDEC_ID_DEN__MASK        0x01FF0000
#define PWRC_QSPI_PARAMS__JEDEC_ID_DEN__INV_MASK    0xFE00FFFF
#define PWRC_QSPI_PARAMS__JEDEC_ID_DEN__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.ATTR_DPM - Deep-Powerdown-Mode attribute */
/* 0 : Does not support Deep-Powerdown-Mode (DPM) */
/* 1 : Supports Deep-Powerdown-Mode (DPM) */
#define PWRC_QSPI_PARAMS__ATTR_DPM__SHIFT       25
#define PWRC_QSPI_PARAMS__ATTR_DPM__WIDTH       1
#define PWRC_QSPI_PARAMS__ATTR_DPM__MASK        0x02000000
#define PWRC_QSPI_PARAMS__ATTR_DPM__INV_MASK    0xFDFFFFFF
#define PWRC_QSPI_PARAMS__ATTR_DPM__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.ATTR_32 - 24/32-bits mode attribute (extended addressing) */
/* 0 : 24-bits address */
/* 1 : 32-bits address */
#define PWRC_QSPI_PARAMS__ATTR_32__SHIFT       26
#define PWRC_QSPI_PARAMS__ATTR_32__WIDTH       1
#define PWRC_QSPI_PARAMS__ATTR_32__MASK        0x04000000
#define PWRC_QSPI_PARAMS__ATTR_32__INV_MASK    0xFBFFFFFF
#define PWRC_QSPI_PARAMS__ATTR_32__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.READ_OP - Supported read opcodes */
/* 0 : FAST_READ (opcode 0Bh) */
/* 1 : READ2O (opcode 3Bh) */
/* 2 : READ2IO (opcode BBh) */
/* 3 : READ4O (opcode 6Bh) */
/* 4 : READ4IO (opcode EBh) */
#define PWRC_QSPI_PARAMS__READ_OP__SHIFT       27
#define PWRC_QSPI_PARAMS__READ_OP__WIDTH       3
#define PWRC_QSPI_PARAMS__READ_OP__MASK        0x38000000
#define PWRC_QSPI_PARAMS__READ_OP__INV_MASK    0xC7FFFFFF
#define PWRC_QSPI_PARAMS__READ_OP__HW_DEFAULT  0x0

/* PWRC_QSPI_PARAMS.WRITE_OP - Supported write opcodes */
/* 0 : PP (opcode 02h) */
/* 1 : PP2O (opcode A2h) */
/* 2 : PP4O (opcode 32h) */
/* 3 : PP4IO (opcode 38h) */
#define PWRC_QSPI_PARAMS__WRITE_OP__SHIFT       30
#define PWRC_QSPI_PARAMS__WRITE_OP__WIDTH       2
#define PWRC_QSPI_PARAMS__WRITE_OP__MASK        0xC0000000
#define PWRC_QSPI_PARAMS__WRITE_OP__INV_MASK    0x3FFFFFFF
#define PWRC_QSPI_PARAMS__WRITE_OP__HW_DEFAULT  0x0

/* PWRC CSSI LPI Control Register */
/* PWRC_CSSI_LPI_CTRL register controls the Low Power Interface (LPI) of Coresight, which allows SW an option to request an AXi stall. See description in Coresight spec. */
#define PWRC_CSSI_LPI_CTRL        0x188D005C

/* PWRC_CSSI_LPI_CTRL.CSYSREQ - Clock power-down request (active-low), i.e. requests an AXI stall when asserted. */
#define PWRC_CSSI_LPI_CTRL__CSYSREQ__SHIFT       0
#define PWRC_CSSI_LPI_CTRL__CSYSREQ__WIDTH       1
#define PWRC_CSSI_LPI_CTRL__CSYSREQ__MASK        0x00000001
#define PWRC_CSSI_LPI_CTRL__CSYSREQ__INV_MASK    0xFFFFFFFE
#define PWRC_CSSI_LPI_CTRL__CSYSREQ__HW_DEFAULT  0x1

/* PWRC_CSSI_LPI_CTRL.CSYSACK - Clock power-down acknowledge (active low). 
 It indicates that the AXI master interface is stalled when asserted. */
#define PWRC_CSSI_LPI_CTRL__CSYSACK__SHIFT       1
#define PWRC_CSSI_LPI_CTRL__CSYSACK__WIDTH       1
#define PWRC_CSSI_LPI_CTRL__CSYSACK__MASK        0x00000002
#define PWRC_CSSI_LPI_CTRL__CSYSACK__INV_MASK    0xFFFFFFFD
#define PWRC_CSSI_LPI_CTRL__CSYSACK__HW_DEFAULT  0x1

/* PWRC_CSSI_LPI_CTRL.CACTIVE - It indicates that a low-power request is accepted (active-low). */
#define PWRC_CSSI_LPI_CTRL__CACTIVE__SHIFT       2
#define PWRC_CSSI_LPI_CTRL__CACTIVE__WIDTH       1
#define PWRC_CSSI_LPI_CTRL__CACTIVE__MASK        0x00000004
#define PWRC_CSSI_LPI_CTRL__CACTIVE__INV_MASK    0xFFFFFFFB
#define PWRC_CSSI_LPI_CTRL__CACTIVE__HW_DEFAULT  0x1

/* PWRC Boot-Config Reset-Latches values Register */
/* PWRC_BCRL_VAL should be latched according to the following flow: 
 1. early_reset_b asserts. 
 2. bcrl_gsync_dir deasserts 2 XINW cycles later. 
 Note that early_reset_b_once is an internal signal which acts as a write-once version of early_reset_b. 
 In other words, it only asserts upon COLDBOOT-->NORMAL path, and not upon PSAVING-->NORMAL. */
#define PWRC_BCRL_VAL             0x188D0060

/* PWRC_BCRL_VAL.CA7_BOOT_SRC - Boot-source selection for Cortex-A7 reset-latch */
/* 0 : NAND SDR */
/* 1 : USB (USB1) */
/* 2 : SD Card (SDIO2) */
/* 3 : eMMC Data partition (SDIO0) */
/* 4 : Reserved */
/* 5 : SPI (QSPI, a.k.a. SPI0) */
/* 6 : NAND Toggle-Mode DDR */
/* 7 : eMMC boot partition (SDIO0) */
#define PWRC_BCRL_VAL__CA7_BOOT_SRC__SHIFT       0
#define PWRC_BCRL_VAL__CA7_BOOT_SRC__WIDTH       3
#define PWRC_BCRL_VAL__CA7_BOOT_SRC__MASK        0x00000007
#define PWRC_BCRL_VAL__CA7_BOOT_SRC__INV_MASK    0xFFFFFFF8
#define PWRC_BCRL_VAL__CA7_BOOT_SRC__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.JTAG_MODE - JTAG Mode reset-latch (formerly called 'JTAG_DIS'). */
/* 0 : JTAG IO option is selected according to JTAG_SEL reset-latch. */
/* 1 : JTAG IO option is selected according to SW IO configurations. */
#define PWRC_BCRL_VAL__JTAG_MODE__SHIFT       3
#define PWRC_BCRL_VAL__JTAG_MODE__WIDTH       1
#define PWRC_BCRL_VAL__JTAG_MODE__MASK        0x00000008
#define PWRC_BCRL_VAL__JTAG_MODE__INV_MASK    0xFFFFFFF7
#define PWRC_BCRL_VAL__JTAG_MODE__HW_DEFAULT  0x1

/* PWRC_BCRL_VAL.CM3_BOOT_TYPE - Boot-type for Cortex-M3 reset-latch */
/* 0 : Dependent */
/* 1 : Independent */
#define PWRC_BCRL_VAL__CM3_BOOT_TYPE__SHIFT       4
#define PWRC_BCRL_VAL__CM3_BOOT_TYPE__WIDTH       1
#define PWRC_BCRL_VAL__CM3_BOOT_TYPE__MASK        0x00000010
#define PWRC_BCRL_VAL__CM3_BOOT_TYPE__INV_MASK    0xFFFFFFEF
#define PWRC_BCRL_VAL__CM3_BOOT_TYPE__HW_DEFAULT  0x1

/* PWRC_BCRL_VAL.PLL_BYPASS - PLL bypass reset-latch. 
 Note that RTCPLL is affected according to PWRC_RTC_PLL_CTRL.RL_BYPASS */
/* 0 : PLL Bypass */
/* 1 : PLL Normal operation */
#define PWRC_BCRL_VAL__PLL_BYPASS__SHIFT       5
#define PWRC_BCRL_VAL__PLL_BYPASS__WIDTH       1
#define PWRC_BCRL_VAL__PLL_BYPASS__MASK        0x00000020
#define PWRC_BCRL_VAL__PLL_BYPASS__INV_MASK    0xFFFFFFDF
#define PWRC_BCRL_VAL__PLL_BYPASS__HW_DEFAULT  0x1

/* PWRC_BCRL_VAL.XTAL_FREQ - XIN crystal frequency selection reset-latch */
/* 0 : 26MHz */
/* 1 : 24MHz */
#define PWRC_BCRL_VAL__XTAL_FREQ__SHIFT       6
#define PWRC_BCRL_VAL__XTAL_FREQ__WIDTH       1
#define PWRC_BCRL_VAL__XTAL_FREQ__MASK        0x00000040
#define PWRC_BCRL_VAL__XTAL_FREQ__INV_MASK    0xFFFFFFBF
#define PWRC_BCRL_VAL__XTAL_FREQ__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.CLK_CTRL_OBS - CLKC observability reset-latch */
/* 0 : Clock controller observable */
/* 1 : Clock controller not observable */
#define PWRC_BCRL_VAL__CLK_CTRL_OBS__SHIFT       7
#define PWRC_BCRL_VAL__CLK_CTRL_OBS__WIDTH       1
#define PWRC_BCRL_VAL__CLK_CTRL_OBS__MASK        0x00000080
#define PWRC_BCRL_VAL__CLK_CTRL_OBS__INV_MASK    0xFFFFFF7F
#define PWRC_BCRL_VAL__CLK_CTRL_OBS__HW_DEFAULT  0x1

/* PWRC_BCRL_VAL.RMA_MODE - RMA mode */
/* 0 : Not Active */
/* 1 : Active */
#define PWRC_BCRL_VAL__RMA_MODE__SHIFT       8
#define PWRC_BCRL_VAL__RMA_MODE__WIDTH       1
#define PWRC_BCRL_VAL__RMA_MODE__MASK        0x00000100
#define PWRC_BCRL_VAL__RMA_MODE__INV_MASK    0xFFFFFEFF
#define PWRC_BCRL_VAL__RMA_MODE__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.ATE_MODE - ATE mode */
/* 0 : Not Active */
/* 1 : Active */
#define PWRC_BCRL_VAL__ATE_MODE__SHIFT       9
#define PWRC_BCRL_VAL__ATE_MODE__WIDTH       1
#define PWRC_BCRL_VAL__ATE_MODE__MASK        0x00000200
#define PWRC_BCRL_VAL__ATE_MODE__INV_MASK    0xFFFFFDFF
#define PWRC_BCRL_VAL__ATE_MODE__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.TEST_MODE - Test mode reset-latch */
/* 0 : Not Active */
/* 1 : Active */
#define PWRC_BCRL_VAL__TEST_MODE__SHIFT       10
#define PWRC_BCRL_VAL__TEST_MODE__WIDTH       1
#define PWRC_BCRL_VAL__TEST_MODE__MASK        0x00000400
#define PWRC_BCRL_VAL__TEST_MODE__INV_MASK    0xFFFFFBFF
#define PWRC_BCRL_VAL__TEST_MODE__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.JTAG_SEL - Jtag selection reset-latch, actually implemented as bs_jtag_tdo pin. 
 Both JTAG IO options are connected to same RTC-Coresight JTAG controller. 
 JTAG_SEL is only relevant in case JTAG_MODE reset-latch is 0x0. */
/* 0 : RTC JTAG interface (primary) */
/* 1 : Core JTAG interface (secondary) */
#define PWRC_BCRL_VAL__JTAG_SEL__SHIFT       11
#define PWRC_BCRL_VAL__JTAG_SEL__WIDTH       1
#define PWRC_BCRL_VAL__JTAG_SEL__MASK        0x00000800
#define PWRC_BCRL_VAL__JTAG_SEL__INV_MASK    0xFFFFF7FF
#define PWRC_BCRL_VAL__JTAG_SEL__HW_DEFAULT  0x0

/* PWRC_BCRL_VAL.FIB_DET - FIB DET */
/* 0 : Not detected (default) */
/* 1 : Detected */
#define PWRC_BCRL_VAL__FIB_DET__SHIFT       12
#define PWRC_BCRL_VAL__FIB_DET__WIDTH       1
#define PWRC_BCRL_VAL__FIB_DET__MASK        0x00001000
#define PWRC_BCRL_VAL__FIB_DET__INV_MASK    0xFFFFEFFF
#define PWRC_BCRL_VAL__FIB_DET__HW_DEFAULT  0x0

/* PWRC RTCM Amp. Adjust Register */
/* The Virage RAMs that are spread all over the chip and require a configurable option. 
 Therefore a register is required and since some of the RAMs are located in the RTCM, the register should be added there (for giving the CM3 an option to control them when only it is powered up). 
 A single register is used for all memories and it should look as above. */
#define PWRC_RTCM_AMP_ADJUST      0x188D0064

/* PWRC_RTCM_AMP_ADJUST.RTCM_RAM_RM - SRAM RM configuration for RTCM */
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RM__SHIFT       0
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RM__WIDTH       4
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RM__MASK        0x0000000F
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RM__INV_MASK    0xFFFFFFF0
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RM__HW_DEFAULT  0x0

/* PWRC_RTCM_AMP_ADJUST.RTCM_RAM_RME - SRAM RME configuration for RTCM */
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RME__SHIFT       4
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RME__WIDTH       1
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RME__MASK        0x00000010
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RME__INV_MASK    0xFFFFFFEF
#define PWRC_RTCM_AMP_ADJUST__RTCM_RAM_RME__HW_DEFAULT  0x0

/* PWRC_RTCM_AMP_ADJUST.CORE_RAM_RM - SRAM RM configuration for CORE */
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RM__SHIFT       8
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RM__WIDTH       4
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RM__MASK        0x00000F00
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RM__INV_MASK    0xFFFFF0FF
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RM__HW_DEFAULT  0x0

/* PWRC_RTCM_AMP_ADJUST.CORE_RAM_RME - SRAM RME configuration for CORE */
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RME__SHIFT       12
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RME__WIDTH       1
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RME__MASK        0x00001000
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RME__INV_MASK    0xFFFFEFFF
#define PWRC_RTCM_AMP_ADJUST__CORE_RAM_RME__HW_DEFAULT  0x0

/* PWRC KAS SW Configuration Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. */
#define PWRC_KAS_SW_CONFIG        0x188D0068

/* PWRC_KAS_SW_CONFIG.KAS_SW_CONFIG - KAS SW Configuration from OTP. */
#define PWRC_KAS_SW_CONFIG__KAS_SW_CONFIG__SHIFT       0
#define PWRC_KAS_SW_CONFIG__KAS_SW_CONFIG__WIDTH       24
#define PWRC_KAS_SW_CONFIG__KAS_SW_CONFIG__MASK        0x00FFFFFF
#define PWRC_KAS_SW_CONFIG__KAS_SW_CONFIG__INV_MASK    0xFF000000
#define PWRC_KAS_SW_CONFIG__KAS_SW_CONFIG__HW_DEFAULT  0x0

/* PWRC RTC GNSS Shutdown Register */
/* TRG_SHUTDOWN_B_OUT = FORCE_TRIG_SD[0] || (GNSS_RTC_SD_CTRL &amp; FORCE_TRIG_SD[1]) 

 Note: GNSS_RTC_SD_CTRL is an internal register of RTC_GNSS. */
#define PWRC_RTC_GNSS_SD          0x188D006C

/* PWRC_RTC_GNSS_SD.FORCE_TRIG_SD - RTC GNSS Force TriG Shut-Down */
#define PWRC_RTC_GNSS_SD__FORCE_TRIG_SD__SHIFT       0
#define PWRC_RTC_GNSS_SD__FORCE_TRIG_SD__WIDTH       2
#define PWRC_RTC_GNSS_SD__FORCE_TRIG_SD__MASK        0x00000003
#define PWRC_RTC_GNSS_SD__FORCE_TRIG_SD__INV_MASK    0xFFFFFFFC
#define PWRC_RTC_GNSS_SD__FORCE_TRIG_SD__HW_DEFAULT  0x3

/* PWRC RTC IO-Bridge Control Register */
/* This register allows fine-tuning of the RTCM IO-Bridge service performance. */
#define PWRC_RTC_IOB_CTRL         0x188D0070

/* PWRC_RTC_IOB_CTRL.RD_DELAY - Delay (in XINW cycles) for locking period after a Read transaction. */
#define PWRC_RTC_IOB_CTRL__RD_DELAY__SHIFT       0
#define PWRC_RTC_IOB_CTRL__RD_DELAY__WIDTH       2
#define PWRC_RTC_IOB_CTRL__RD_DELAY__MASK        0x00000003
#define PWRC_RTC_IOB_CTRL__RD_DELAY__INV_MASK    0xFFFFFFFC
#define PWRC_RTC_IOB_CTRL__RD_DELAY__HW_DEFAULT  0x3

/* PWRC_RTC_IOB_CTRL.WR_DELAY - Delay (in XINW cycles) for locking period after a Write transaction. */
#define PWRC_RTC_IOB_CTRL__WR_DELAY__SHIFT       2
#define PWRC_RTC_IOB_CTRL__WR_DELAY__WIDTH       2
#define PWRC_RTC_IOB_CTRL__WR_DELAY__MASK        0x0000000C
#define PWRC_RTC_IOB_CTRL__WR_DELAY__INV_MASK    0xFFFFFFF3
#define PWRC_RTC_IOB_CTRL__WR_DELAY__HW_DEFAULT  0x3

/* PWRC RTCM Spinlock Debug Register */
/* This register allows reading the spinlocks status without trying to lock it. */
#define PWRC_SPINLOCK_RD_DEBUG    0x188D0074

/* PWRC_SPINLOCK_RD_DEBUG.DEBUG_MODE - Debug mode enable, shared by all RTCM spinlocks. */
/* 0 : Operational mode (default), reading from a spin lock tries to lock it. 
 The returned read data is 0 if it was already locked and 1 if it was successfully locked. */
/* 1 : Debug mode, reading from a spin lock will simply return the spin-lock value. */
#define PWRC_SPINLOCK_RD_DEBUG__DEBUG_MODE__SHIFT       0
#define PWRC_SPINLOCK_RD_DEBUG__DEBUG_MODE__WIDTH       1
#define PWRC_SPINLOCK_RD_DEBUG__DEBUG_MODE__MASK        0x00000001
#define PWRC_SPINLOCK_RD_DEBUG__DEBUG_MODE__INV_MASK    0xFFFFFFFE
#define PWRC_SPINLOCK_RD_DEBUG__DEBUG_MODE__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_0         0x188D0078

/* PWRC_TESTANDSET_0.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_0__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_0__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_0__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_0__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_0__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_1         0x188D007C

/* PWRC_TESTANDSET_1.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_1__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_1__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_1__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_1__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_1__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_2         0x188D0080

/* PWRC_TESTANDSET_2.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_2__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_2__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_2__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_2__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_2__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_3         0x188D0084

/* PWRC_TESTANDSET_3.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_3__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_3__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_3__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_3__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_3__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_4         0x188D0088

/* PWRC_TESTANDSET_4.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_4__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_4__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_4__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_4__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_4__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_5         0x188D008C

/* PWRC_TESTANDSET_5.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_5__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_5__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_5__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_5__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_5__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_6         0x188D0090

/* PWRC_TESTANDSET_6.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_6__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_6__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_6__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_6__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_6__SPINLOCK__HW_DEFAULT  0x0

/* PWRC RTCM Spinlock Register */
/* All of the RTCM spinlock registers share the same DEBUG_MODE parameter (PWRC_SPINLOCK_RD_DEBUG). 
 Software should use these registers appropriately, e.g. hardware does not protect spinlock from begin stole. */
#define PWRC_TESTANDSET_7         0x188D0094

/* PWRC_TESTANDSET_7.SPINLOCK - TestAndSet spinlock module. Writing will SET, i.e. release. 
 Reading will TEST, i.e. try to lock. */
#define PWRC_TESTANDSET_7__SPINLOCK__SHIFT       0
#define PWRC_TESTANDSET_7__SPINLOCK__WIDTH       1
#define PWRC_TESTANDSET_7__SPINLOCK__MASK        0x00000001
#define PWRC_TESTANDSET_7__SPINLOCK__INV_MASK    0xFFFFFFFE
#define PWRC_TESTANDSET_7__SPINLOCK__HW_DEFAULT  0x0

/* PWRC Mode Type Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. */
#define PWRC_MODE_TYPE            0x188D0098

/* PWRC_MODE_TYPE.SR - Operational mode type for SW usage, please refer to 'System Security Spec' */
#define PWRC_MODE_TYPE__SR__SHIFT       0
#define PWRC_MODE_TYPE__SR__WIDTH       32
#define PWRC_MODE_TYPE__SR__MASK        0xFFFFFFFF
#define PWRC_MODE_TYPE__SR__INV_MASK    0x00000000
#define PWRC_MODE_TYPE__SR__HW_DEFAULT  0x0

/* PWRC UUID Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_UUID_VAL0 through PWRC_UUID_VAL3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_UUID_VAL3 is written by SW. */
#define PWRC_UUID_VAL0            0x188D009C

/* PWRC_UUID_VAL0.UUID - UUID Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_UUID_VAL0__UUID__SHIFT       0
#define PWRC_UUID_VAL0__UUID__WIDTH       32
#define PWRC_UUID_VAL0__UUID__MASK        0xFFFFFFFF
#define PWRC_UUID_VAL0__UUID__INV_MASK    0x00000000
#define PWRC_UUID_VAL0__UUID__HW_DEFAULT  0x0

/* PWRC UUID Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_UUID_VAL0 through PWRC_UUID_VAL3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_UUID_VAL3 is written by SW. */
#define PWRC_UUID_VAL1            0x188D00A0

/* PWRC_UUID_VAL1.UUID - UUID Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_UUID_VAL1__UUID__SHIFT       0
#define PWRC_UUID_VAL1__UUID__WIDTH       32
#define PWRC_UUID_VAL1__UUID__MASK        0xFFFFFFFF
#define PWRC_UUID_VAL1__UUID__INV_MASK    0x00000000
#define PWRC_UUID_VAL1__UUID__HW_DEFAULT  0x0

/* PWRC UUID Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_UUID_VAL0 through PWRC_UUID_VAL3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_UUID_VAL3 is written by SW. */
#define PWRC_UUID_VAL2            0x188D00A4

/* PWRC_UUID_VAL2.UUID - UUID Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_UUID_VAL2__UUID__SHIFT       0
#define PWRC_UUID_VAL2__UUID__WIDTH       32
#define PWRC_UUID_VAL2__UUID__MASK        0xFFFFFFFF
#define PWRC_UUID_VAL2__UUID__INV_MASK    0x00000000
#define PWRC_UUID_VAL2__UUID__HW_DEFAULT  0x0

/* PWRC UUID Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_UUID_VAL0 through PWRC_UUID_VAL3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_UUID_VAL3 is written by SW. */
#define PWRC_UUID_VAL3            0x188D00A8

/* PWRC_UUID_VAL3.UUID - UUID Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_UUID_VAL3__UUID__SHIFT       0
#define PWRC_UUID_VAL3__UUID__WIDTH       32
#define PWRC_UUID_VAL3__UUID__MASK        0xFFFFFFFF
#define PWRC_UUID_VAL3__UUID__INV_MASK    0x00000000
#define PWRC_UUID_VAL3__UUID__HW_DEFAULT  0x0

/* PWRC HBK Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_HBK0 through PWRC_HBK3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_HBK3 is written by SW. */
#define PWRC_HBK0                 0x188D00AC

/* PWRC_HBK0.HBK - HBK Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_HBK0__HBK__SHIFT       0
#define PWRC_HBK0__HBK__WIDTH       32
#define PWRC_HBK0__HBK__MASK        0xFFFFFFFF
#define PWRC_HBK0__HBK__INV_MASK    0x00000000
#define PWRC_HBK0__HBK__HW_DEFAULT  0x0

/* PWRC HBK Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_HBK0 through PWRC_HBK3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_HBK3 is written by SW. */
#define PWRC_HBK1                 0x188D00B0

/* PWRC_HBK1.HBK - HBK Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_HBK1__HBK__SHIFT       0
#define PWRC_HBK1__HBK__WIDTH       32
#define PWRC_HBK1__HBK__MASK        0xFFFFFFFF
#define PWRC_HBK1__HBK__INV_MASK    0x00000000
#define PWRC_HBK1__HBK__HW_DEFAULT  0x0

/* PWRC HBK Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_HBK0 through PWRC_HBK3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_HBK3 is written by SW. */
#define PWRC_HBK2                 0x188D00B4

/* PWRC_HBK2.HBK - HBK Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_HBK2__HBK__SHIFT       0
#define PWRC_HBK2__HBK__WIDTH       32
#define PWRC_HBK2__HBK__MASK        0xFFFFFFFF
#define PWRC_HBK2__HBK__INV_MASK    0x00000000
#define PWRC_HBK2__HBK__HW_DEFAULT  0x0

/* PWRC HBK Value Register */
/* This register is write-once (W1), i.e. can be written by SW only once after Coldboot. 
 For more information, particularly referring to functional usage, please refer to 'System Security Spec'. 

 <b>Note:</b> In order to ensure consistency, PWRC_HBK0 through PWRC_HBK3 registers must be updated atomically. 
 The values written by SW into the registers should only be updated to HW when the last register PWRC_HBK3 is written by SW. */
#define PWRC_HBK3                 0x188D00B8

/* PWRC_HBK3.HBK - HBK Value for SW usage, please refer to 'System Security Spec' */
#define PWRC_HBK3__HBK__SHIFT       0
#define PWRC_HBK3__HBK__WIDTH       32
#define PWRC_HBK3__HBK__MASK        0xFFFFFFFF
#define PWRC_HBK3__HBK__INV_MASK    0x00000000
#define PWRC_HBK3__HBK__HW_DEFAULT  0x0

/* PWRC Reset for CPU (M3 -> A7) Register */
/* M3 can control the CPU (A7) reset through this RST, which is active-low. */
#define PWRC_M3_CPU_RESET         0x188D00BC

/* PWRC_M3_CPU_RESET.RST_B - Reset (active low) */
#define PWRC_M3_CPU_RESET__RST_B__SHIFT       0
#define PWRC_M3_CPU_RESET__RST_B__WIDTH       1
#define PWRC_M3_CPU_RESET__RST_B__MASK        0x00000001
#define PWRC_M3_CPU_RESET__RST_B__INV_MASK    0xFFFFFFFE
#define PWRC_M3_CPU_RESET__RST_B__HW_DEFAULT  0x1

/* PWRC Scratch-pad W1 Register */
/* Write-Once (write is enabled again only after cold boot) */
#define PWRC_SCRATCH_PAD1_W1      0x188D00C0

/* PWRC_SCRATCH_PAD1_W1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD1_W1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD1_W1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD1_W1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD1_W1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD1_W1__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad W1 Register */
/* Write-Once (write is enabled again only after cold boot) */
#define PWRC_SCRATCH_PAD2_W1      0x188D00C4

/* PWRC_SCRATCH_PAD2_W1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD2_W1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD2_W1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD2_W1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD2_W1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD2_W1__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad W1 Register */
/* Write-Once (write is enabled again only after cold boot) */
#define PWRC_SCRATCH_PAD3_W1      0x188D00C8

/* PWRC_SCRATCH_PAD3_W1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD3_W1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD3_W1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD3_W1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD3_W1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD3_W1__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad W1 Register */
/* Write-Once (write is enabled again only after cold boot) */
#define PWRC_SCRATCH_PAD4_W1      0x188D00CC

/* PWRC_SCRATCH_PAD4_W1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD4_W1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD4_W1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD4_W1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD4_W1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD4_W1__SCRATCH_PAD__HW_DEFAULT  0x0

/* PWRC Scratch-pad W1 Register */
/* Write-Once (write is enabled again only after cold boot) */
#define PWRC_SCRATCH_PAD5_W1      0x188D00D0

/* PWRC_SCRATCH_PAD5_W1.SCRATCH_PAD - Register for software to save non-volatile data */
#define PWRC_SCRATCH_PAD5_W1__SCRATCH_PAD__SHIFT       0
#define PWRC_SCRATCH_PAD5_W1__SCRATCH_PAD__WIDTH       32
#define PWRC_SCRATCH_PAD5_W1__SCRATCH_PAD__MASK        0xFFFFFFFF
#define PWRC_SCRATCH_PAD5_W1__SCRATCH_PAD__INV_MASK    0x00000000
#define PWRC_SCRATCH_PAD5_W1__SCRATCH_PAD__HW_DEFAULT  0x0

/* M3 Status Register */
/* This registers monitors the status of the M3 domain. */
#define PWRC_M3_STATUS            0x188D00D4

/* PWRC_M3_STATUS.M3_SLEEPING - M3_SLEEPING */
/* 0 : m3_sleeping not asserted. */
/* 1 : m3_sleeping asserted. */
#define PWRC_M3_STATUS__M3_SLEEPING__SHIFT       0
#define PWRC_M3_STATUS__M3_SLEEPING__WIDTH       1
#define PWRC_M3_STATUS__M3_SLEEPING__MASK        0x00000001
#define PWRC_M3_STATUS__M3_SLEEPING__INV_MASK    0xFFFFFFFE
#define PWRC_M3_STATUS__M3_SLEEPING__HW_DEFAULT  0x0

/* PWRC_M3_STATUS.M3_SLEEPDEEP - M3_SLEEPDEEP */
/* 0 : m3_sleepdeep not asserted. */
/* 1 : m3_sleepdeep asserted. */
#define PWRC_M3_STATUS__M3_SLEEPDEEP__SHIFT       1
#define PWRC_M3_STATUS__M3_SLEEPDEEP__WIDTH       1
#define PWRC_M3_STATUS__M3_SLEEPDEEP__MASK        0x00000002
#define PWRC_M3_STATUS__M3_SLEEPDEEP__INV_MASK    0xFFFFFFFD
#define PWRC_M3_STATUS__M3_SLEEPDEEP__HW_DEFAULT  0x0

/* PWRC RTC Write-Once Register */
/* This registers monitors the status of the write-once (W1) registers in the RETAIN_REG domain. */
#define PWRC_RTC_W1_DEBUG         0x188D00D8

/* PWRC_RTC_W1_DEBUG.BOOTSTRAP - BOOTSTRAP */
/* 0 : Register has already been written (R/O now). */
/* 1 : Register has not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__BOOTSTRAP__SHIFT       0
#define PWRC_RTC_W1_DEBUG__BOOTSTRAP__WIDTH       1
#define PWRC_RTC_W1_DEBUG__BOOTSTRAP__MASK        0x00000001
#define PWRC_RTC_W1_DEBUG__BOOTSTRAP__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_W1_DEBUG__BOOTSTRAP__HW_DEFAULT  0x0

/* PWRC_RTC_W1_DEBUG.HASH - HASH */
/* 0 : Registers has already been written by SW (R/O now). */
/* 1 : Registers 0..3 have already been written by HW (still open). */
/* 2 : Registers have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__HASH__SHIFT       1
#define PWRC_RTC_W1_DEBUG__HASH__WIDTH       2
#define PWRC_RTC_W1_DEBUG__HASH__MASK        0x00000006
#define PWRC_RTC_W1_DEBUG__HASH__INV_MASK    0xFFFFFFF9
#define PWRC_RTC_W1_DEBUG__HASH__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.KAS_SW_CFG - KAS_SW_CFG */
/* 0 : Register has already been written (R/O now). */
/* 1 : Register has not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__KAS_SW_CFG__SHIFT       3
#define PWRC_RTC_W1_DEBUG__KAS_SW_CFG__WIDTH       1
#define PWRC_RTC_W1_DEBUG__KAS_SW_CFG__MASK        0x00000008
#define PWRC_RTC_W1_DEBUG__KAS_SW_CFG__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_W1_DEBUG__KAS_SW_CFG__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.BCRL_VAL - BCRL_VAL */
/* 0 : Register has already been written (R/O now). */
/* 1 : Register has not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__BCRL_VAL__SHIFT       4
#define PWRC_RTC_W1_DEBUG__BCRL_VAL__WIDTH       1
#define PWRC_RTC_W1_DEBUG__BCRL_VAL__MASK        0x00000010
#define PWRC_RTC_W1_DEBUG__BCRL_VAL__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_W1_DEBUG__BCRL_VAL__HW_DEFAULT  0x0

/* PWRC_RTC_W1_DEBUG.UUID_VAL - UUID_VAL */
/* 0 : Registers have already been written (R/O now). */
/* 1 : Registers have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__UUID_VAL__SHIFT       5
#define PWRC_RTC_W1_DEBUG__UUID_VAL__WIDTH       1
#define PWRC_RTC_W1_DEBUG__UUID_VAL__MASK        0x00000020
#define PWRC_RTC_W1_DEBUG__UUID_VAL__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_W1_DEBUG__UUID_VAL__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.MODE_TYPE - MODE_TYPE */
/* 0 : Register has already been written (R/O now). */
/* 1 : Register has not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__MODE_TYPE__SHIFT       6
#define PWRC_RTC_W1_DEBUG__MODE_TYPE__WIDTH       1
#define PWRC_RTC_W1_DEBUG__MODE_TYPE__MASK        0x00000040
#define PWRC_RTC_W1_DEBUG__MODE_TYPE__INV_MASK    0xFFFFFFBF
#define PWRC_RTC_W1_DEBUG__MODE_TYPE__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.HBK - HBK */
/* 0 : Registers have already been written (R/O now). */
/* 1 : Registers have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__HBK__SHIFT       7
#define PWRC_RTC_W1_DEBUG__HBK__WIDTH       1
#define PWRC_RTC_W1_DEBUG__HBK__MASK        0x00000080
#define PWRC_RTC_W1_DEBUG__HBK__INV_MASK    0xFFFFFF7F
#define PWRC_RTC_W1_DEBUG__HBK__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.SP1_W1 - SCRACH_PAD1_W1 */
/* 0 : Register have already been written (R/O now). */
/* 1 : Register have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__SP1_W1__SHIFT       8
#define PWRC_RTC_W1_DEBUG__SP1_W1__WIDTH       1
#define PWRC_RTC_W1_DEBUG__SP1_W1__MASK        0x00000100
#define PWRC_RTC_W1_DEBUG__SP1_W1__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_W1_DEBUG__SP1_W1__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.SP2_W1 - SCRACH_PAD2_W1 */
/* 0 : Register have already been written (R/O now). */
/* 1 : Register have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__SP2_W1__SHIFT       9
#define PWRC_RTC_W1_DEBUG__SP2_W1__WIDTH       1
#define PWRC_RTC_W1_DEBUG__SP2_W1__MASK        0x00000200
#define PWRC_RTC_W1_DEBUG__SP2_W1__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_W1_DEBUG__SP2_W1__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.SP3_W1 - SCRACH_PAD3_W1 */
/* 0 : Register have already been written (R/O now). */
/* 1 : Register have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__SP3_W1__SHIFT       10
#define PWRC_RTC_W1_DEBUG__SP3_W1__WIDTH       1
#define PWRC_RTC_W1_DEBUG__SP3_W1__MASK        0x00000400
#define PWRC_RTC_W1_DEBUG__SP3_W1__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_W1_DEBUG__SP3_W1__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.SP4_W1 - SCRACH_PAD4_W1 */
/* 0 : Register have already been written (R/O now). */
/* 1 : Register have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__SP4_W1__SHIFT       11
#define PWRC_RTC_W1_DEBUG__SP4_W1__WIDTH       1
#define PWRC_RTC_W1_DEBUG__SP4_W1__MASK        0x00000800
#define PWRC_RTC_W1_DEBUG__SP4_W1__INV_MASK    0xFFFFF7FF
#define PWRC_RTC_W1_DEBUG__SP4_W1__HW_DEFAULT  0x1

/* PWRC_RTC_W1_DEBUG.SP5_W1 - SCRACH_PAD5_W1 */
/* 0 : Register have already been written (R/O now). */
/* 1 : Register have not been written yet (still open). */
#define PWRC_RTC_W1_DEBUG__SP5_W1__SHIFT       12
#define PWRC_RTC_W1_DEBUG__SP5_W1__WIDTH       1
#define PWRC_RTC_W1_DEBUG__SP5_W1__MASK        0x00001000
#define PWRC_RTC_W1_DEBUG__SP5_W1__INV_MASK    0xFFFFEFFF
#define PWRC_RTC_W1_DEBUG__SP5_W1__HW_DEFAULT  0x1


#endif  // __RETAINREGS_H__
