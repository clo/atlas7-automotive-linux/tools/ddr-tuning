/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __UPCTL_H__
#define __UPCTL_H__



/* UPCTL */
/* ==================================================================== */

/* SCFG */
/* State Configuration */
#define MEMC_SCFG                 0x10800000

/* MEMC_SCFG.hw_low_power_en - Enables the hardware low-power interface. Allows the system to request via hardware (c_sysreq input) to enter the memories into Self-Refresh. The handshaking between the request and acknowledge hardware low power signals (c_sysreq and c_sysack, respectively) is always performed, but the uPCTL response depends on the value set on this register field and by the value driven on the c_active_in input pin. 
1'b0 = Disabled. Requests are always denied and uPCTL is unaffected by c_sysreq
 1'b1 = Enabled. Requests are accepted or denied, depending on the current operational state of uPCTL and on the value of c_active_in. 
<b>Default Value:</b> 1'b0 */
#define MEMC_SCFG__HW_LOW_POWER_EN__SHIFT       0
#define MEMC_SCFG__HW_LOW_POWER_EN__WIDTH       1
#define MEMC_SCFG__HW_LOW_POWER_EN__MASK        0x00000001
#define MEMC_SCFG__HW_LOW_POWER_EN__INV_MASK    0xFFFFFFFE
#define MEMC_SCFG__HW_LOW_POWER_EN__HW_DEFAULT  0x0

/* MEMC_SCFG.nfifo_nif1_dis - For Synopsys internal use only for NFIFO testing. 
 1'b0 = Only supported setting.
 1'b1 = For Synopsys internal use only.
 <b>Default Value:</b> 1'b0 */
#define MEMC_SCFG__NFIFO_NIF1_DIS__SHIFT       6
#define MEMC_SCFG__NFIFO_NIF1_DIS__WIDTH       1
#define MEMC_SCFG__NFIFO_NIF1_DIS__MASK        0x00000040
#define MEMC_SCFG__NFIFO_NIF1_DIS__INV_MASK    0xFFFFFFBF
#define MEMC_SCFG__NFIFO_NIF1_DIS__HW_DEFAULT  0x0

/* MEMC_SCFG.ac_pdd_en - Enables assertion of ac_pdd to indicate to the PHY of an opportunity to switch to a Low power mode */
#define MEMC_SCFG__AC_PDD_EN__SHIFT       7
#define MEMC_SCFG__AC_PDD_EN__WIDTH       1
#define MEMC_SCFG__AC_PDD_EN__MASK        0x00000080
#define MEMC_SCFG__AC_PDD_EN__INV_MASK    0xFFFFFF7F
#define MEMC_SCFG__AC_PDD_EN__HW_DEFAULT  0x0

/* MEMC_SCFG.bbflags_timing - The n_bbflags is a NIF output vector which provides combined information about the status of each memory bank. The de-assertion is based on when precharge, activates, reads/writes are scheduled by the TCU block. It may be possible to de-assert n_bbflags earlier than calculated by the TCU block. Programming bbflags_timing is used to achieve this.The maximum recommended value is: UPCTL_TCU_SED_P - TRP.t_rp. The parameter UPCTL_TCU_SED_P is described in the section "Synthesis Timing Improvement Using the uPCTL_TCU_SED parameter". The programmed value is the maximum number of "early" cycles that n_bbflags maybe de-asserted. The actual achieved de-assertion depends on the traffic profile.
 In 1:2 mode the maximum allowed programmable value is 4'b0111
 In 1:1 mode the value can be 4'b1111
 <b>Default Value:</b> 1:2 Mode: 4'b0111, 1:1 Mode: 4'b1111 */
#define MEMC_SCFG__BBFLAGS_TIMING__SHIFT       8
#define MEMC_SCFG__BBFLAGS_TIMING__WIDTH       4
#define MEMC_SCFG__BBFLAGS_TIMING__MASK        0x00000F00
#define MEMC_SCFG__BBFLAGS_TIMING__INV_MASK    0xFFFFF0FF
#define MEMC_SCFG__BBFLAGS_TIMING__HW_DEFAULT  0x7

/* MEMC_SCFG.ac_pdd_add_del - Adds additional delay onto the assertion of ac_pdd */
#define MEMC_SCFG__AC_PDD_ADD_DEL__SHIFT       12
#define MEMC_SCFG__AC_PDD_ADD_DEL__WIDTH       5
#define MEMC_SCFG__AC_PDD_ADD_DEL__MASK        0x0001F000
#define MEMC_SCFG__AC_PDD_ADD_DEL__INV_MASK    0xFFFE0FFF
#define MEMC_SCFG__AC_PDD_ADD_DEL__HW_DEFAULT  0x0

/* SCTL */
/* State Control */
#define MEMC_SCTL                 0x10800004

/* MEMC_SCTL.state_cmd - Issues an operational state transition request to the uPCTL. 
3'b000 = INIT (move to Init_mem from Config) 
3'b001 = CFG (move to Config from Init_mem or Access) 
3'b010 = GO (move to Access from Config) 
3'b011 = SLEEP (move to Low_power from Access) 
3'b100 = WAKEUP (move to Access from Low_power) 
Others = Reserved 
<b>Default Value:</b> 3'b000 */
#define MEMC_SCTL__STATE_CMD__SHIFT       0
#define MEMC_SCTL__STATE_CMD__WIDTH       3
#define MEMC_SCTL__STATE_CMD__MASK        0x00000007
#define MEMC_SCTL__STATE_CMD__INV_MASK    0xFFFFFFF8
#define MEMC_SCTL__STATE_CMD__HW_DEFAULT  0x0

/* STAT */
/* State Status */
#define MEMC_STAT                 0x10800008

/* MEMC_STAT.ctl_stat - Returns the current operational state of the uPCTL. 
3'b000 = Init_mem 
3'b001 = Config 
3'b010 = Config_req 
3'b011 = Access 
3'b100 = Access_req 
3'b101 = Low_power 
3'b110 = Low_power_entry_req 
3'b111 = Low_power_exit_req 
Others = Reserved 
<b>Default Value:</b> 3'b000 */
#define MEMC_STAT__CTL_STAT__SHIFT       0
#define MEMC_STAT__CTL_STAT__WIDTH       3
#define MEMC_STAT__CTL_STAT__MASK        0x00000007
#define MEMC_STAT__CTL_STAT__INV_MASK    0xFFFFFFF8
#define MEMC_STAT__CTL_STAT__HW_DEFAULT  0x0

/* MEMC_STAT.lp_trig - Reports the status of what triggered an entry to Low_power state. Is only set if in Low_power state. The individual bits report the following: 
lp_trig[2]: Software driven due to SCTL.state_cmd==SLEEP. 
lp_trig[1]: Hardware driven due to Hardware Low Power Interface. 
lp_trig[0]: Hardware driven due to Auto Self Refresh (MCFG1.sr_idle>0). Note that if more than one trigger happens at the exact same time, more than one bit of lp_trig may be asserted high. 
<b>Default Value:</b> 3'b000 */
#define MEMC_STAT__LP_TRIG__SHIFT       4
#define MEMC_STAT__LP_TRIG__WIDTH       3
#define MEMC_STAT__LP_TRIG__MASK        0x00000070
#define MEMC_STAT__LP_TRIG__INV_MASK    0xFFFFFF8F
#define MEMC_STAT__LP_TRIG__HW_DEFAULT  0x0

/* INTRSTAT */
/* Interrupt Status */
#define MEMC_INTRSTAT             0x1080000C

/* MEMC_INTRSTAT.ecc_intr - Indicates that an ECC error has been detected 
1'b0 = No error 
1'b1 = ECC error 
<b>Default Value:</b> 1'b0 */
#define MEMC_INTRSTAT__ECC_INTR__SHIFT       0
#define MEMC_INTRSTAT__ECC_INTR__WIDTH       1
#define MEMC_INTRSTAT__ECC_INTR__MASK        0x00000001
#define MEMC_INTRSTAT__ECC_INTR__INV_MASK    0xFFFFFFFE
#define MEMC_INTRSTAT__ECC_INTR__HW_DEFAULT  0x0

/* MEMC_INTRSTAT.parity_intr - Indicates that a DFI parity error has been detected 
1'b0 = No error 
1'b1 = Parity error 
<b>Default Value:</b> 1'b0 */
#define MEMC_INTRSTAT__PARITY_INTR__SHIFT       1
#define MEMC_INTRSTAT__PARITY_INTR__WIDTH       1
#define MEMC_INTRSTAT__PARITY_INTR__MASK        0x00000002
#define MEMC_INTRSTAT__PARITY_INTR__INV_MASK    0xFFFFFFFD
#define MEMC_INTRSTAT__PARITY_INTR__HW_DEFAULT  0x0

/* MCMD */
/* Memory Command */
#define MEMC_MCMD                 0x10800040

/* MEMC_MCMD.cmd_opcode - Command to be issued to the memory. 
4'b0000 = Deselect. This is only used for timing purposes, no actual direct Deselect command is passed to the memories. 
4'b0001 = Precharge All (PREA) 
4'b0010 = Refresh (REF) 4'b0011 = Mode Register Set (MRS) - is MRW in LPDDR2/LPDDR3, MRS otherwise 
4'b0100 = ZQ Calibration Short (ZQCS, only applies to LPDDR2/LPDDR3/DDR3) 
4'b0101 = ZQ Calibration Long (ZQCL, only applies to LPDDR2/LPDDR3/DDR3) 
4'b0110 = Software Driven Reset (RSTL, only applies to DDR3) 
4'b0111 = Reserved 
4'b1000 - Mode Register Read (MRR) - is MRR in LPDDR2/LPDDR3, is SRR in mDRR and is MPR in DDR3 
4'b1001 - Deep Power Down Entry (DPDE, only applies to mDDR/LPDDR2/LPDDR3) 
4'b1010 - DFI Controller Update (DFICTRLUPD) 
Others - Reserved 
It is recommended that DPDE and RSTL commands using the MCMD register should not be performed in Config State as automatic Refresh logic runs in Config state and may cause these commands to operate incorrectly. 
<b>Default Value:</b> 4'b0000 */
#define MEMC_MCMD__CMD_OPCODE__SHIFT       0
#define MEMC_MCMD__CMD_OPCODE__WIDTH       4
#define MEMC_MCMD__CMD_OPCODE__MASK        0x0000000F
#define MEMC_MCMD__CMD_OPCODE__INV_MASK    0xFFFFFFF0
#define MEMC_MCMD__CMD_OPCODE__HW_DEFAULT  0x0

/* MEMC_MCMD.cmd_addr - Command Address */
#define MEMC_MCMD__CMD_ADDR__SHIFT       4
#define MEMC_MCMD__CMD_ADDR__WIDTH       13
#define MEMC_MCMD__CMD_ADDR__MASK        0x0001FFF0
#define MEMC_MCMD__CMD_ADDR__INV_MASK    0xFFFE000F
#define MEMC_MCMD__CMD_ADDR__HW_DEFAULT  0x0

/* MEMC_MCMD.bank_addr - Bank Address */
#define MEMC_MCMD__BANK_ADDR__SHIFT       17
#define MEMC_MCMD__BANK_ADDR__WIDTH       3
#define MEMC_MCMD__BANK_ADDR__MASK        0x000E0000
#define MEMC_MCMD__BANK_ADDR__INV_MASK    0xFFF1FFFF
#define MEMC_MCMD__BANK_ADDR__HW_DEFAULT  0x0

/* MEMC_MCMD.rank_sel - Rank select for the command to be executed. 
4'b0001 = Rank 0 
4'b0010 = Rank 1 
4'b0100 = Rank 2 
4'b1000 = Rank 3 
4'b0000 = Reserved 
Multiple 1'b1s in rank_sel mean multiple ranks are selected, which is useful broadcasting commands in parallel to multiple ranks during initialization and configuration of the memories. 
If MCMD.cmd_opcode=RSTL, all ranks should be selected as it cannot be performed to individual ranks. 
If MCMD.cmd_opcode=DPDE, all ranks should be selected as it cannot be performed to individual ranks. 
If MCMD.cmd_opcode=DFICTRLUPD, rank_sel is ignored as no actual command is sent to the memories. 
<b>Default Value:</b> 4'b0001 */
#define MEMC_MCMD__RANK_SEL__SHIFT       20
#define MEMC_MCMD__RANK_SEL__WIDTH       4
#define MEMC_MCMD__RANK_SEL__MASK        0x00F00000
#define MEMC_MCMD__RANK_SEL__INV_MASK    0xFF0FFFFF
#define MEMC_MCMD__RANK_SEL__HW_DEFAULT  0x1

/* MEMC_MCMD.cmd_add_del - Set the additional delay associated with each command to 2n internal timers clock cycles, where n is the bit field value. If n=0, the delay is 0. Max value is n=10. 
<b>Default Value:</b> 4'b0000 */
#define MEMC_MCMD__CMD_ADD_DEL__SHIFT       24
#define MEMC_MCMD__CMD_ADD_DEL__WIDTH       4
#define MEMC_MCMD__CMD_ADD_DEL__MASK        0x0F000000
#define MEMC_MCMD__CMD_ADD_DEL__INV_MASK    0xF0FFFFFF
#define MEMC_MCMD__CMD_ADD_DEL__HW_DEFAULT  0x0

/* MEMC_MCMD.start_cmd - Start command. When this bit is set to 1, the command operation defined in the cmd_opcode field is started. This bit is automatically cleared by the uPCTL after the command is finished. The application can poll this bit to determine when uPCTL is ready to accept another command. This bit cannot be cleared to 1�'b0 by software. 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCMD__START_CMD__SHIFT       31
#define MEMC_MCMD__START_CMD__WIDTH       1
#define MEMC_MCMD__START_CMD__MASK        0x80000000
#define MEMC_MCMD__START_CMD__INV_MASK    0x7FFFFFFF
#define MEMC_MCMD__START_CMD__HW_DEFAULT  0x0

/* POWCTL */
/* Power Up Control */
#define MEMC_POWCTL               0x10800044

/* MEMC_POWCTL.power_up_start - Start the memory power up sequence. When this bit is set to 1'b1, uPCTL starts the CKE and RESET# power up sequence to the memories. This bit is automatically cleared by uPCTL after the sequence is completed. This bit cannot be cleared to 1'b0 by software. 
<b>Default Value:</b> 1'b0 */
#define MEMC_POWCTL__POWER_UP_START__SHIFT       0
#define MEMC_POWCTL__POWER_UP_START__WIDTH       1
#define MEMC_POWCTL__POWER_UP_START__MASK        0x00000001
#define MEMC_POWCTL__POWER_UP_START__INV_MASK    0xFFFFFFFE
#define MEMC_POWCTL__POWER_UP_START__HW_DEFAULT  0x0

/* POWSTAT */
/* Power Up Status */
#define MEMC_POWSTAT              0x10800048

/* MEMC_POWSTAT.power_up_done - Returns the status of the memory power-up sequence. 
1'b0 = Power-up sequence has not been performed. 
1'b1 = Power-up sequence has been performed. 
<b>Default Value:</b> 1'b0 */
#define MEMC_POWSTAT__POWER_UP_DONE__SHIFT       0
#define MEMC_POWSTAT__POWER_UP_DONE__WIDTH       1
#define MEMC_POWSTAT__POWER_UP_DONE__MASK        0x00000001
#define MEMC_POWSTAT__POWER_UP_DONE__INV_MASK    0xFFFFFFFE
#define MEMC_POWSTAT__POWER_UP_DONE__HW_DEFAULT  0x0

/* CMDTSTAT */
/* Command Timers Status */
#define MEMC_CMDTSTAT             0x1080004C

/* MEMC_CMDTSTAT.cmd_timers_stat - Returns the status of the timers for memory commands. This ANDs all the command timers together. 
1'b0 = One or more command timers has not expired. 
1'b1 = All command timers have expired. 
<b>Default Value:</b> 1'b0 */
#define MEMC_CMDTSTAT__CMD_TIMERS_STAT__SHIFT       0
#define MEMC_CMDTSTAT__CMD_TIMERS_STAT__WIDTH       1
#define MEMC_CMDTSTAT__CMD_TIMERS_STAT__MASK        0x00000001
#define MEMC_CMDTSTAT__CMD_TIMERS_STAT__INV_MASK    0xFFFFFFFE
#define MEMC_CMDTSTAT__CMD_TIMERS_STAT__HW_DEFAULT  0x0

/* CMDTSTATEN */
/* Command Timers Status Enable */
#define MEMC_CMDTSTATEN           0x10800050

/* MEMC_CMDTSTATEN.cmd_timers_stat_en - Enables the generation of the status of the timers for memory commands. Is enabled before CMDTSTAT register is read. 
1'b0 - Disabled 
1'b1 - Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_CMDTSTATEN__CMD_TIMERS_STAT_EN__SHIFT       0
#define MEMC_CMDTSTATEN__CMD_TIMERS_STAT_EN__WIDTH       1
#define MEMC_CMDTSTATEN__CMD_TIMERS_STAT_EN__MASK        0x00000001
#define MEMC_CMDTSTATEN__CMD_TIMERS_STAT_EN__INV_MASK    0xFFFFFFFE
#define MEMC_CMDTSTATEN__CMD_TIMERS_STAT_EN__HW_DEFAULT  0x0

/* MRRCFG0 */
/* MRR Configuration 0 Register */
#define MEMC_MRRCFG0              0x10800060

/* MEMC_MRRCFG0.mrr_byte_sel - Selects which byte�s data to store when performing an MRR command via MCMD. 
<b>LegalValues:</b> 0 ... 8 
<b>Default Value:</b> 4'h0 */
#define MEMC_MRRCFG0__MRR_BYTE_SEL__SHIFT       0
#define MEMC_MRRCFG0__MRR_BYTE_SEL__WIDTH       4
#define MEMC_MRRCFG0__MRR_BYTE_SEL__MASK        0x0000000F
#define MEMC_MRRCFG0__MRR_BYTE_SEL__INV_MASK    0xFFFFFFF0
#define MEMC_MRRCFG0__MRR_BYTE_SEL__HW_DEFAULT  0x0

/* MRRSTAT0 */
/* MRR Status 0 Register */
#define MEMC_MRRSTAT0             0x10800064

/* MEMC_MRRSTAT0.mrr_beat0 - MPR/MRR read data beat 0 */
#define MEMC_MRRSTAT0__MRR_BEAT0__SHIFT       0
#define MEMC_MRRSTAT0__MRR_BEAT0__WIDTH       8
#define MEMC_MRRSTAT0__MRR_BEAT0__MASK        0x000000FF
#define MEMC_MRRSTAT0__MRR_BEAT0__INV_MASK    0xFFFFFF00
#define MEMC_MRRSTAT0__MRR_BEAT0__HW_DEFAULT  0x0

/* MEMC_MRRSTAT0.mrr_beat1 - MPR/MRR read data beat 1 */
#define MEMC_MRRSTAT0__MRR_BEAT1__SHIFT       8
#define MEMC_MRRSTAT0__MRR_BEAT1__WIDTH       8
#define MEMC_MRRSTAT0__MRR_BEAT1__MASK        0x0000FF00
#define MEMC_MRRSTAT0__MRR_BEAT1__INV_MASK    0xFFFF00FF
#define MEMC_MRRSTAT0__MRR_BEAT1__HW_DEFAULT  0x0

/* MEMC_MRRSTAT0.mrr_beat2 - MPR/MRR read data beat 2 */
#define MEMC_MRRSTAT0__MRR_BEAT2__SHIFT       16
#define MEMC_MRRSTAT0__MRR_BEAT2__WIDTH       8
#define MEMC_MRRSTAT0__MRR_BEAT2__MASK        0x00FF0000
#define MEMC_MRRSTAT0__MRR_BEAT2__INV_MASK    0xFF00FFFF
#define MEMC_MRRSTAT0__MRR_BEAT2__HW_DEFAULT  0x0

/* MEMC_MRRSTAT0.mrr_beat3 - MPR/MRR read data beat 3 */
#define MEMC_MRRSTAT0__MRR_BEAT3__SHIFT       24
#define MEMC_MRRSTAT0__MRR_BEAT3__WIDTH       8
#define MEMC_MRRSTAT0__MRR_BEAT3__MASK        0xFF000000
#define MEMC_MRRSTAT0__MRR_BEAT3__INV_MASK    0x00FFFFFF
#define MEMC_MRRSTAT0__MRR_BEAT3__HW_DEFAULT  0x0

/* MRRSTAT1 */
/* MRR Status 1 Register */
#define MEMC_MRRSTAT1             0x10800068

/* MEMC_MRRSTAT1.mrr_beat4 - MPR/MRR read data beat 4 */
#define MEMC_MRRSTAT1__MRR_BEAT4__SHIFT       0
#define MEMC_MRRSTAT1__MRR_BEAT4__WIDTH       8
#define MEMC_MRRSTAT1__MRR_BEAT4__MASK        0x000000FF
#define MEMC_MRRSTAT1__MRR_BEAT4__INV_MASK    0xFFFFFF00
#define MEMC_MRRSTAT1__MRR_BEAT4__HW_DEFAULT  0x0

/* MEMC_MRRSTAT1.mrr_beat5 - MPR/MRR read data beat 5 */
#define MEMC_MRRSTAT1__MRR_BEAT5__SHIFT       8
#define MEMC_MRRSTAT1__MRR_BEAT5__WIDTH       8
#define MEMC_MRRSTAT1__MRR_BEAT5__MASK        0x0000FF00
#define MEMC_MRRSTAT1__MRR_BEAT5__INV_MASK    0xFFFF00FF
#define MEMC_MRRSTAT1__MRR_BEAT5__HW_DEFAULT  0x0

/* MEMC_MRRSTAT1.mrr_beat6 - MPR/MRR read data beat 6 */
#define MEMC_MRRSTAT1__MRR_BEAT6__SHIFT       16
#define MEMC_MRRSTAT1__MRR_BEAT6__WIDTH       8
#define MEMC_MRRSTAT1__MRR_BEAT6__MASK        0x00FF0000
#define MEMC_MRRSTAT1__MRR_BEAT6__INV_MASK    0xFF00FFFF
#define MEMC_MRRSTAT1__MRR_BEAT6__HW_DEFAULT  0x0

/* MEMC_MRRSTAT1.mrr_beat7 - MPR/MRR read data beat 7 */
#define MEMC_MRRSTAT1__MRR_BEAT7__SHIFT       24
#define MEMC_MRRSTAT1__MRR_BEAT7__WIDTH       8
#define MEMC_MRRSTAT1__MRR_BEAT7__MASK        0xFF000000
#define MEMC_MRRSTAT1__MRR_BEAT7__INV_MASK    0x00FFFFFF
#define MEMC_MRRSTAT1__MRR_BEAT7__HW_DEFAULT  0x0

/* MCFG1 */
/* Memory Configuration 1 */
#define MEMC_MCFG1                0x1080007C

/* MEMC_MCFG1.sr_idle - Self Refresh idle period. Memories are placed into Self-Refresh mode if the NIF is idle in Access state for sr_idle * 32 * n_clk cycles. The automatic self refresh function is disabled when sr_idle=0. 
<b>Default Value:</b> 8'b0000000 */
#define MEMC_MCFG1__SR_IDLE__SHIFT       0
#define MEMC_MCFG1__SR_IDLE__WIDTH       8
#define MEMC_MCFG1__SR_IDLE__MASK        0x000000FF
#define MEMC_MCFG1__SR_IDLE__INV_MASK    0xFFFFFF00
#define MEMC_MCFG1__SR_IDLE__HW_DEFAULT  0x0

/* MEMC_MCFG1.tfaw_cfg_offset - Used to fine tune the tFAW setting as programmed in MCFG.tfaw_cfg tFAW= (4 + MCFG.tfaw_cfg)*tRRD - tfaw_cfg_offset 
<b>Default Value:</b> 3'b000 */
#define MEMC_MCFG1__TFAW_CFG_OFFSET__SHIFT       8
#define MEMC_MCFG1__TFAW_CFG_OFFSET__WIDTH       3
#define MEMC_MCFG1__TFAW_CFG_OFFSET__MASK        0x00000700
#define MEMC_MCFG1__TFAW_CFG_OFFSET__INV_MASK    0xFFFFF8FF
#define MEMC_MCFG1__TFAW_CFG_OFFSET__HW_DEFAULT  0x0

/* MEMC_MCFG1.hw_idle - Hardware idle period. The c_active output is driven low if the NIF is idle in Access state for hw_idle * 32 * n_clk cycles. The hardware idle function is disabled when hw_idle=0. 
<b>Default Value:</b> 8'b0000000 */
#define MEMC_MCFG1__HW_IDLE__SHIFT       16
#define MEMC_MCFG1__HW_IDLE__WIDTH       8
#define MEMC_MCFG1__HW_IDLE__MASK        0x00FF0000
#define MEMC_MCFG1__HW_IDLE__INV_MASK    0xFF00FFFF
#define MEMC_MCFG1__HW_IDLE__HW_DEFAULT  0x0

/* MEMC_MCFG1.zq_resistor_shared -  
<li>1 - Denotes that ZQ resistor is shared between ranks. Means ZQinit/ZQCL/ZQCS commands are sent to one rank at a time with tZQinit/tZQCL/tZQCS timing met between commands so that commands to different ranks do not overlap. 
<li>0 - ZQ resistor is not shared. 
ZQ resistor can only be shared for DDR3/LPDDR2/LPDDR3 devices. */
#define MEMC_MCFG1__ZQ_RESISTOR_SHARED__SHIFT       24
#define MEMC_MCFG1__ZQ_RESISTOR_SHARED__WIDTH       1
#define MEMC_MCFG1__ZQ_RESISTOR_SHARED__MASK        0x01000000
#define MEMC_MCFG1__ZQ_RESISTOR_SHARED__INV_MASK    0xFEFFFFFF
#define MEMC_MCFG1__ZQ_RESISTOR_SHARED__HW_DEFAULT  0x0

/* MEMC_MCFG1.hw_exit_idle_en - When this bit is programmed to 1'b1 the c_active_in pin can be used to exit from the automatic clock stop, power down or self-refresh modes. 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG1__HW_EXIT_IDLE_EN__SHIFT       31
#define MEMC_MCFG1__HW_EXIT_IDLE_EN__WIDTH       1
#define MEMC_MCFG1__HW_EXIT_IDLE_EN__MASK        0x80000000
#define MEMC_MCFG1__HW_EXIT_IDLE_EN__INV_MASK    0x7FFFFFFF
#define MEMC_MCFG1__HW_EXIT_IDLE_EN__HW_DEFAULT  0x0

/* MCFG */
/* Memory Configuration */
#define MEMC_MCFG                 0x10800080

/* MEMC_MCFG.mem_bl - DDR Burst Length. The BL setting in DDR2 / DDR3 must be consistent with the value programmed into the BL field of MR0. 
1'b0 = BL4, Burst length of 4 (MR0.BL=3'b010, DDR2 only) 
1'b1 = BL8, Burst length of 8 (MR0.BL=3'b011 for DDR2, MR0.BL=2'b00 for DDR3) 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__MEM_BL__SHIFT       0
#define MEMC_MCFG__MEM_BL__WIDTH       1
#define MEMC_MCFG__MEM_BL__MASK        0x00000001
#define MEMC_MCFG__MEM_BL__INV_MASK    0xFFFFFFFE
#define MEMC_MCFG__MEM_BL__HW_DEFAULT  0x0

/* MEMC_MCFG.cke_or_en - This bit is intended to be set for 4-rank RDIMMs, which have a 2-bit CKE input. If set, dfi_cke[0] is asserted to enable either of the even ranks (0 and 2), while dfi_cke[1] is asserted to enable either of the odd ranks (1 and 3). dfi_cke[3:2] are inactive (0) 
1'b0: Disabled 
1'b1: Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__CKE_OR_EN__SHIFT       1
#define MEMC_MCFG__CKE_OR_EN__WIDTH       1
#define MEMC_MCFG__CKE_OR_EN__MASK        0x00000002
#define MEMC_MCFG__CKE_OR_EN__INV_MASK    0xFFFFFFFD
#define MEMC_MCFG__CKE_OR_EN__HW_DEFAULT  0x0

/* MEMC_MCFG.bl8int_en - Setting this bit enables the BL8 interrupt function of DDR2. This is the capability to early terminate a BL8 after only 4 DDR beats by issuing the next command two cycles earlier. This functionality is only available for DDR2 memories and this setting is ignored for mDDR/LPDDR2/LPDDR3 and DDR3. 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__BL8INT_EN__SHIFT       2
#define MEMC_MCFG__BL8INT_EN__WIDTH       1
#define MEMC_MCFG__BL8INT_EN__MASK        0x00000004
#define MEMC_MCFG__BL8INT_EN__INV_MASK    0xFFFFFFFB
#define MEMC_MCFG__BL8INT_EN__HW_DEFAULT  0x0

/* MEMC_MCFG.two_t_en - Enables 2T timing for memory commands. 
1'b0= Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__TWO_T_EN__SHIFT       3
#define MEMC_MCFG__TWO_T_EN__WIDTH       1
#define MEMC_MCFG__TWO_T_EN__MASK        0x00000008
#define MEMC_MCFG__TWO_T_EN__INV_MASK    0xFFFFFFF7
#define MEMC_MCFG__TWO_T_EN__HW_DEFAULT  0x0

/* MEMC_MCFG.stagger_cs - For multi-rank commands from the DCU, stagger the assertion of CS_N to odd and even ranks by one n_clk cycle. This is useful when using RDIMMs, when multi-rank commands may be interpreted as writes to control words in the register chip. 
1'b0 = Do not stagger CS_N 
1'b1 = Stagger CS_N 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__STAGGER_CS__SHIFT       4
#define MEMC_MCFG__STAGGER_CS__WIDTH       1
#define MEMC_MCFG__STAGGER_CS__MASK        0x00000010
#define MEMC_MCFG__STAGGER_CS__INV_MASK    0xFFFFFFEF
#define MEMC_MCFG__STAGGER_CS__HW_DEFAULT  0x0

/* MEMC_MCFG.ddr3_en - Select DDR2 or DDR3 protocol. Ignored, if mDDR or LPDDR2 or LPDDR3 support is enabled. 
1'b0 = DDR2 Protocol Rules 
1'b1 = DDR3 Protocol Rules 
<b>Default Value:</b> 1'b1 */
#define MEMC_MCFG__DDR3_EN__SHIFT       5
#define MEMC_MCFG__DDR3_EN__WIDTH       1
#define MEMC_MCFG__DDR3_EN__MASK        0x00000020
#define MEMC_MCFG__DDR3_EN__INV_MASK    0xFFFFFFDF
#define MEMC_MCFG__DDR3_EN__HW_DEFAULT  0x0

/* MEMC_MCFG.lpddr2_s4 - Enables LPDDR2-S4 support. 
1'b0 = LPDDR2-S4 disabled (LPDDR2-S2 enabled) 
1'b1 = LPDDR2-S4 enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__LPDDR2_S4__SHIFT       6
#define MEMC_MCFG__LPDDR2_S4__WIDTH       1
#define MEMC_MCFG__LPDDR2_S4__MASK        0x00000040
#define MEMC_MCFG__LPDDR2_S4__INV_MASK    0xFFFFFFBF
#define MEMC_MCFG__LPDDR2_S4__HW_DEFAULT  0x0

/* MEMC_MCFG.mddr_lpddr23_bst_even - Ensures BST commands only placed an even no. of cycles from previous column command (Read or Write). 
1'b0 = Disabled 
1'b1 = Enabled If using the �Gen 2 DDR MultiPHYs� in LPDDR2/LPDDR3, set this to 1'b1. Otherwise, set it to 1'b0. 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__MDDR_LPDDR23_BST_EVEN__SHIFT       7
#define MEMC_MCFG__MDDR_LPDDR23_BST_EVEN__WIDTH       1
#define MEMC_MCFG__MDDR_LPDDR23_BST_EVEN__MASK        0x00000080
#define MEMC_MCFG__MDDR_LPDDR23_BST_EVEN__INV_MASK    0xFFFFFF7F
#define MEMC_MCFG__MDDR_LPDDR23_BST_EVEN__HW_DEFAULT  0x0

/* MEMC_MCFG.pd_idle - Power-down idle period in n_clk cycles. Memories are placed into power-down mode if the NIF is idle for pd_idle n_clk cycles. The automatic power down function is disabled when pd_idle=0. 
<b>Default Value:</b> 8'b0000000 */
#define MEMC_MCFG__PD_IDLE__SHIFT       8
#define MEMC_MCFG__PD_IDLE__WIDTH       8
#define MEMC_MCFG__PD_IDLE__MASK        0x0000FF00
#define MEMC_MCFG__PD_IDLE__INV_MASK    0xFFFF00FF
#define MEMC_MCFG__PD_IDLE__HW_DEFAULT  0x0

/* MEMC_MCFG.pd_type - Sets the Power down type. 
1'b0 = Precharge Power Down 
1'b1 = Active Power Down 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__PD_TYPE__SHIFT       16
#define MEMC_MCFG__PD_TYPE__WIDTH       1
#define MEMC_MCFG__PD_TYPE__MASK        0x00010000
#define MEMC_MCFG__PD_TYPE__INV_MASK    0xFFFEFFFF
#define MEMC_MCFG__PD_TYPE__HW_DEFAULT  0x0

/* MEMC_MCFG.pd_exit_mode - Selects the mode for Power Down Exit. For DDR2/DDR3, the power down exit mode setting in uPCTL must be consistent with the value programmed into the power down exit mode bit of MR0. For mDDR/LPDDR2/LPDDR3, only fast exit mode is valid. 
1'b0 = slow exit 
1'b1 = fast exit 
<b>Default Value:</b> 1'b0 */
#define MEMC_MCFG__PD_EXIT_MODE__SHIFT       17
#define MEMC_MCFG__PD_EXIT_MODE__WIDTH       1
#define MEMC_MCFG__PD_EXIT_MODE__MASK        0x00020000
#define MEMC_MCFG__PD_EXIT_MODE__INV_MASK    0xFFFDFFFF
#define MEMC_MCFG__PD_EXIT_MODE__HW_DEFAULT  0x0

/* MEMC_MCFG.tfaw_cfg - Sets tFAW to be 4, 5 or 6 times tRRD. 
2'b00 = set tFAW=4*tRRD - MCFG1.tfaw_cfg_offset 
2'b01 = set tFAW=5*tRRD - MCFG1.tfaw_cfg_offset 
2'b10 = set tFAW=6*tRRD - MCFG1.tfaw_cfg_offset 
<b>Default Value:</b> 2'b01 */
#define MEMC_MCFG__TFAW_CFG__SHIFT       18
#define MEMC_MCFG__TFAW_CFG__WIDTH       2
#define MEMC_MCFG__TFAW_CFG__MASK        0x000C0000
#define MEMC_MCFG__TFAW_CFG__INV_MASK    0xFFF3FFFF
#define MEMC_MCFG__TFAW_CFG__HW_DEFAULT  0x1

/* MEMC_MCFG.mddr_lpddr23_bl - programmed into the BL field of MR. 
2'b00 = BL2, Burst length of 2 (MR.BL=3'b001, mDDR only) 
2'b01 = BL4, Burst length of 4 (MR.BL=3'b010, for mDDR and LPDDR2 only) 
2'b10 = BL8, Burst length of 8 (MR.BL=3'b011, for mDDR, LPDDR2 and LPDDR3) 
2'b11 = BL16, Burst length of 16 (MR.BL=3'b100, for mDDR and LPDDR2 only) 
This value is effective only if MCFG.mddr_lpddr23_en[1:0]!=2�b00. Otherwise, MCFG.mem_bl is used to define uPCTL�s Burst Length (for DDR2/DDR3). 
<b>Default Value:</b> 2'b00 */
#define MEMC_MCFG__MDDR_LPDDR23_BL__SHIFT       20
#define MEMC_MCFG__MDDR_LPDDR23_BL__WIDTH       2
#define MEMC_MCFG__MDDR_LPDDR23_BL__MASK        0x00300000
#define MEMC_MCFG__MDDR_LPDDR23_BL__INV_MASK    0xFFCFFFFF
#define MEMC_MCFG__MDDR_LPDDR23_BL__HW_DEFAULT  0x0

/* MEMC_MCFG.mddr_lpddr23_en - mDDR/LPDDR2 Enable. Enables support for mDDR or LPDDR2 or LPDDR3. 
2'b00 = mDDR/LPDDR2/LPDDR3 Disabled 
2'b01 = LPDDR3 Enabled 
2'b10 = mDDR Enabled 
2'b11 = LPDDR2 Enabled 
<b>Default Value:</b> 2'b00 */
#define MEMC_MCFG__MDDR_LPDDR23_EN__SHIFT       22
#define MEMC_MCFG__MDDR_LPDDR23_EN__WIDTH       2
#define MEMC_MCFG__MDDR_LPDDR23_EN__MASK        0x00C00000
#define MEMC_MCFG__MDDR_LPDDR23_EN__INV_MASK    0xFF3FFFFF
#define MEMC_MCFG__MDDR_LPDDR23_EN__HW_DEFAULT  0x0

/* MEMC_MCFG.mddr_lpddr23_clkstop_idle - Clock stop idle period in n_clk cycles. Memories are placed into clock stop mode if the NIF is idle for mddr_lpddr2_clkstop_idle n_clk cycles. The automatic clock stop function is disabled when mddr_lpddr2_clkstop_idle=0. Clock stop mode is only applicable in mDDR/LPDDR2/LPDDR3. 
<b>Default Value:</b> 8'b0000000 */
#define MEMC_MCFG__MDDR_LPDDR23_CLKSTOP_IDLE__SHIFT       24
#define MEMC_MCFG__MDDR_LPDDR23_CLKSTOP_IDLE__WIDTH       8
#define MEMC_MCFG__MDDR_LPDDR23_CLKSTOP_IDLE__MASK        0xFF000000
#define MEMC_MCFG__MDDR_LPDDR23_CLKSTOP_IDLE__INV_MASK    0x00FFFFFF
#define MEMC_MCFG__MDDR_LPDDR23_CLKSTOP_IDLE__HW_DEFAULT  0x0

/* PPCFG */
/* Partially Populated Memory Configuration */
#define MEMC_PPCFG                0x10800084

/* MEMC_PPCFG.ppmem_en - Partially Population Enable bit. Setting this bit enables the partial population of external memories where the entire application bus is routed to a reduced size memory system. The lower half of the SDRAM data bus, bit 0 up to bit UPCTL_M_DW/2-1, is the active portion when Partially Populated memories are enabled. An example of this is shown in "Example 8-1" and "Example 8-2". 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_PPCFG__PPMEM_EN__SHIFT       0
#define MEMC_PPCFG__PPMEM_EN__WIDTH       1
#define MEMC_PPCFG__PPMEM_EN__MASK        0x00000001
#define MEMC_PPCFG__PPMEM_EN__INV_MASK    0xFFFFFFFE
#define MEMC_PPCFG__PPMEM_EN__HW_DEFAULT  0x0

/* MEMC_PPCFG.rpmem_dis - Reduced Population Disable bits. Setting these bits disables the corresponding NIF/DDR data lanes from writing or reading data. Lane 0 is always present, hence only 8 bits are required for the remaining lanes including the ECC lane. 
Bit 0 of rpmem_dis covers byte lane 1. 
Bit 1 of rpmem_dis covers byte lane 2. 
There are no restrictions on which byte lanes can be disabled, other than byte lane 0 is required. Gaps between enabled byte lanes are allowed 
For each bit: 
1'b0 = lane exists 
1'b1 = lane is disabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_PPCFG__RPMEM_DIS__SHIFT       1
#define MEMC_PPCFG__RPMEM_DIS__WIDTH       8
#define MEMC_PPCFG__RPMEM_DIS__MASK        0x000001FE
#define MEMC_PPCFG__RPMEM_DIS__INV_MASK    0xFFFFFE01
#define MEMC_PPCFG__RPMEM_DIS__HW_DEFAULT  0x0

/* MEMC_PPCFG.qpmem_en - Quarter Population Enable bit. Setting this bit enables the quarter population of external memories where the entire application bus is routed to a reduced size memory system. The lower quarter of the SDRAM data bus, bit 0 up to bit UPCTL_M_DW/4-1, is the active portion when Quarter Populated memories are enabled. An example of this is shown in "Example 8-3" and "Example 8-4". 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_PPCFG__QPMEM_EN__SHIFT       16
#define MEMC_PPCFG__QPMEM_EN__WIDTH       1
#define MEMC_PPCFG__QPMEM_EN__MASK        0x00010000
#define MEMC_PPCFG__QPMEM_EN__INV_MASK    0xFFFEFFFF
#define MEMC_PPCFG__QPMEM_EN__HW_DEFAULT  0x0

/* MSTAT */
/* Memory Status */
#define MEMC_MSTAT                0x10800088

/* MEMC_MSTAT.power_down - Indicates if uPCTL has placed the memories in Power Down. 
1'b0 = Memory is not in Power Down 
1'b1 = Memory is in Power-Down 
<b>Default Value:</b> 1'b0 */
#define MEMC_MSTAT__POWER_DOWN__SHIFT       0
#define MEMC_MSTAT__POWER_DOWN__WIDTH       1
#define MEMC_MSTAT__POWER_DOWN__MASK        0x00000001
#define MEMC_MSTAT__POWER_DOWN__INV_MASK    0xFFFFFFFE
#define MEMC_MSTAT__POWER_DOWN__HW_DEFAULT  0x0

/* MEMC_MSTAT.clock_stop - Indicates if uPCTL has placed the memories in Clock Stop. 
1'b0 = Memory is not in Clock Stop 
1'b1 = Memory is in Clock Stop 
<b>Default Value:</b> 1'b0 */
#define MEMC_MSTAT__CLOCK_STOP__SHIFT       1
#define MEMC_MSTAT__CLOCK_STOP__WIDTH       1
#define MEMC_MSTAT__CLOCK_STOP__MASK        0x00000002
#define MEMC_MSTAT__CLOCK_STOP__INV_MASK    0xFFFFFFFD
#define MEMC_MSTAT__CLOCK_STOP__HW_DEFAULT  0x0

/* MEMC_MSTAT.self_refresh - Indicates if uPCTL, through auto self refresh, has placed the memories in Self Refresh. 
1'b0 = Memory is not in Self Refresh 
1'b1 = Memory is in Self Refresh 
<b>Default Value:</b> 1'b0 */
#define MEMC_MSTAT__SELF_REFRESH__SHIFT       2
#define MEMC_MSTAT__SELF_REFRESH__WIDTH       1
#define MEMC_MSTAT__SELF_REFRESH__MASK        0x00000004
#define MEMC_MSTAT__SELF_REFRESH__INV_MASK    0xFFFFFFFB
#define MEMC_MSTAT__SELF_REFRESH__HW_DEFAULT  0x0

/* LPDDR23ZQCFG */
/* LPDDR2/LPDDR3 ZQ Configuration */
#define MEMC_LPDDR23ZQCFG         0x1080008C

/* MEMC_LPDDR23ZQCFG.zqcs_ma - Value to drive on memory address bits [11:4] for an automatic hardware generated ZQCS command (LPDDR2/LPDDR3). Corresponds to MA7 .. MA0 of Mode Register Write (MRW) command which is used to send ZQCS command to memory. 
<b>Default Value:</b> 8'h0A */
#define MEMC_LPDDR23ZQCFG__ZQCS_MA__SHIFT       0
#define MEMC_LPDDR23ZQCFG__ZQCS_MA__WIDTH       8
#define MEMC_LPDDR23ZQCFG__ZQCS_MA__MASK        0x000000FF
#define MEMC_LPDDR23ZQCFG__ZQCS_MA__INV_MASK    0xFFFFFF00
#define MEMC_LPDDR23ZQCFG__ZQCS_MA__HW_DEFAULT  0xA

/* MEMC_LPDDR23ZQCFG.zqcs_op - Value to drive on memory address bits [19:12] for an automatic hardware generated ZQCS command (LPDDR2/LPDDR3). Corresponds to OP7 .. OP0 of Mode Register Write (MRW) command which is used to send ZQCS command to memory. 
<b>Default Value:</b> 8'h56 */
#define MEMC_LPDDR23ZQCFG__ZQCS_OP__SHIFT       8
#define MEMC_LPDDR23ZQCFG__ZQCS_OP__WIDTH       8
#define MEMC_LPDDR23ZQCFG__ZQCS_OP__MASK        0x0000FF00
#define MEMC_LPDDR23ZQCFG__ZQCS_OP__INV_MASK    0xFFFF00FF
#define MEMC_LPDDR23ZQCFG__ZQCS_OP__HW_DEFAULT  0x56

/* MEMC_LPDDR23ZQCFG.zqcl_ma - Value to drive on memory address bits [11:4] for an automatic hardware generated ZQCL command (LPDDR2/LPDDR3). Corresponds to MA7 .. MA0 of Mode Register Write (MRW) command which is used to send ZQCL command to memory. 
<b>Default Value:</b> 8'h0A */
#define MEMC_LPDDR23ZQCFG__ZQCL_MA__SHIFT       16
#define MEMC_LPDDR23ZQCFG__ZQCL_MA__WIDTH       8
#define MEMC_LPDDR23ZQCFG__ZQCL_MA__MASK        0x00FF0000
#define MEMC_LPDDR23ZQCFG__ZQCL_MA__INV_MASK    0xFF00FFFF
#define MEMC_LPDDR23ZQCFG__ZQCL_MA__HW_DEFAULT  0xA

/* MEMC_LPDDR23ZQCFG.zqcl_op - Value to drive on memory address bits [19:12] for an automatic hardware generated ZQCL command (LPDDR2/LPDDR3). Corresponds to OP7 .. OP0 of Mode Register Write (MRW) command which is used to send ZQCL command to memory. 
<b>Default Value:</b> 8'hAB */
#define MEMC_LPDDR23ZQCFG__ZQCL_OP__SHIFT       24
#define MEMC_LPDDR23ZQCFG__ZQCL_OP__WIDTH       8
#define MEMC_LPDDR23ZQCFG__ZQCL_OP__MASK        0xFF000000
#define MEMC_LPDDR23ZQCFG__ZQCL_OP__INV_MASK    0x00FFFFFF
#define MEMC_LPDDR23ZQCFG__ZQCL_OP__HW_DEFAULT  0xAB

/* DTUPDES */
/* DTU Status */
#define MEMC_DTUPDES              0x10800094

/* MEMC_DTUPDES.dtu_err_b0 - Detected at least 1 bit error for bit 0 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B0__SHIFT       0
#define MEMC_DTUPDES__DTU_ERR_B0__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B0__MASK        0x00000001
#define MEMC_DTUPDES__DTU_ERR_B0__INV_MASK    0xFFFFFFFE
#define MEMC_DTUPDES__DTU_ERR_B0__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b1 - Detected at least 1 bit error for bit 1 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B1__SHIFT       1
#define MEMC_DTUPDES__DTU_ERR_B1__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B1__MASK        0x00000002
#define MEMC_DTUPDES__DTU_ERR_B1__INV_MASK    0xFFFFFFFD
#define MEMC_DTUPDES__DTU_ERR_B1__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b2 - Detected at least 1 bit error for bit 2 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B2__SHIFT       2
#define MEMC_DTUPDES__DTU_ERR_B2__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B2__MASK        0x00000004
#define MEMC_DTUPDES__DTU_ERR_B2__INV_MASK    0xFFFFFFFB
#define MEMC_DTUPDES__DTU_ERR_B2__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b3 - Detected at least 1 bit error for bit 3 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B3__SHIFT       3
#define MEMC_DTUPDES__DTU_ERR_B3__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B3__MASK        0x00000008
#define MEMC_DTUPDES__DTU_ERR_B3__INV_MASK    0xFFFFFFF7
#define MEMC_DTUPDES__DTU_ERR_B3__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b4 - Detected at least 1 bit error for bit 4 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B4__SHIFT       4
#define MEMC_DTUPDES__DTU_ERR_B4__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B4__MASK        0x00000010
#define MEMC_DTUPDES__DTU_ERR_B4__INV_MASK    0xFFFFFFEF
#define MEMC_DTUPDES__DTU_ERR_B4__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b5 - Detected at least 1 bit error for bit 5 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B5__SHIFT       5
#define MEMC_DTUPDES__DTU_ERR_B5__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B5__MASK        0x00000020
#define MEMC_DTUPDES__DTU_ERR_B5__INV_MASK    0xFFFFFFDF
#define MEMC_DTUPDES__DTU_ERR_B5__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b6 - Detected at least 1 bit error for bit 6 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B6__SHIFT       6
#define MEMC_DTUPDES__DTU_ERR_B6__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B6__MASK        0x00000040
#define MEMC_DTUPDES__DTU_ERR_B6__INV_MASK    0xFFFFFFBF
#define MEMC_DTUPDES__DTU_ERR_B6__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_err_b7 - Detected at least 1 bit error for bit 7 in the programmable data buffers */
#define MEMC_DTUPDES__DTU_ERR_B7__SHIFT       7
#define MEMC_DTUPDES__DTU_ERR_B7__WIDTH       1
#define MEMC_DTUPDES__DTU_ERR_B7__MASK        0x00000080
#define MEMC_DTUPDES__DTU_ERR_B7__INV_MASK    0xFFFFFF7F
#define MEMC_DTUPDES__DTU_ERR_B7__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_random_error - Indicates that the random data generated had some failures when written and read to/from the memories */
#define MEMC_DTUPDES__DTU_RANDOM_ERROR__SHIFT       8
#define MEMC_DTUPDES__DTU_RANDOM_ERROR__WIDTH       1
#define MEMC_DTUPDES__DTU_RANDOM_ERROR__MASK        0x00000100
#define MEMC_DTUPDES__DTU_RANDOM_ERROR__INV_MASK    0xFFFFFEFF
#define MEMC_DTUPDES__DTU_RANDOM_ERROR__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_eaffl - Indicates the number of entries in the FIFO that is holding the log of error addresses for random data comparison */
#define MEMC_DTUPDES__DTU_EAFFL__SHIFT       9
#define MEMC_DTUPDES__DTU_EAFFL__WIDTH       4
#define MEMC_DTUPDES__DTU_EAFFL__MASK        0x00001E00
#define MEMC_DTUPDES__DTU_EAFFL__INV_MASK    0xFFFFE1FF
#define MEMC_DTUPDES__DTU_EAFFL__HW_DEFAULT  0x0

/* MEMC_DTUPDES.dtu_rd_beat_missing - Indicates if one or more read beats of data did not return from memory */
#define MEMC_DTUPDES__DTU_RD_BEAT_MISSING__SHIFT       13
#define MEMC_DTUPDES__DTU_RD_BEAT_MISSING__WIDTH       1
#define MEMC_DTUPDES__DTU_RD_BEAT_MISSING__MASK        0x00002000
#define MEMC_DTUPDES__DTU_RD_BEAT_MISSING__INV_MASK    0xFFFFDFFF
#define MEMC_DTUPDES__DTU_RD_BEAT_MISSING__HW_DEFAULT  0x0

/* DTUNA */
/* DTU Number of Random Addresses Created */
#define MEMC_DTUNA                0x10800098

/* MEMC_DTUNA.dtu_num_address - Indicates the number of addresses that were created on the NIF interface during random data generation */
#define MEMC_DTUNA__DTU_NUM_ADDRESS__SHIFT       0
#define MEMC_DTUNA__DTU_NUM_ADDRESS__WIDTH       32
#define MEMC_DTUNA__DTU_NUM_ADDRESS__MASK        0xFFFFFFFF
#define MEMC_DTUNA__DTU_NUM_ADDRESS__INV_MASK    0x00000000
#define MEMC_DTUNA__DTU_NUM_ADDRESS__HW_DEFAULT  0x0

/* DTUNE */
/* DTU Number of Errors */
#define MEMC_DTUNE                0x1080009C

/* MEMC_DTUNE.dtu_num_errors - Indicates the number of errors that were detected on the readback of the NIF data during random data generation */
#define MEMC_DTUNE__DTU_NUM_ERRORS__SHIFT       0
#define MEMC_DTUNE__DTU_NUM_ERRORS__WIDTH       32
#define MEMC_DTUNE__DTU_NUM_ERRORS__MASK        0xFFFFFFFF
#define MEMC_DTUNE__DTU_NUM_ERRORS__INV_MASK    0x00000000
#define MEMC_DTUNE__DTU_NUM_ERRORS__HW_DEFAULT  0x0

/* DTUPRD0 */
/* DTU Parallel Read 0 */
#define MEMC_DTUPRD0              0x108000A0

/* MEMC_DTUPRD0.dtu_allbits_0 - Allows all the bit 0 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD0__DTU_ALLBITS_0__SHIFT       0
#define MEMC_DTUPRD0__DTU_ALLBITS_0__WIDTH       16
#define MEMC_DTUPRD0__DTU_ALLBITS_0__MASK        0x0000FFFF
#define MEMC_DTUPRD0__DTU_ALLBITS_0__INV_MASK    0xFFFF0000
#define MEMC_DTUPRD0__DTU_ALLBITS_0__HW_DEFAULT  0x0

/* MEMC_DTUPRD0.dtu_allbits_1 - Allows all the bit 1 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD0__DTU_ALLBITS_1__SHIFT       16
#define MEMC_DTUPRD0__DTU_ALLBITS_1__WIDTH       16
#define MEMC_DTUPRD0__DTU_ALLBITS_1__MASK        0xFFFF0000
#define MEMC_DTUPRD0__DTU_ALLBITS_1__INV_MASK    0x0000FFFF
#define MEMC_DTUPRD0__DTU_ALLBITS_1__HW_DEFAULT  0x0

/* DTUPRD1 */
/* DTU Parallel Read 1 */
#define MEMC_DTUPRD1              0x108000A4

/* MEMC_DTUPRD1.dtu_allbits_2 - Allows all the bit 2 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD1__DTU_ALLBITS_2__SHIFT       0
#define MEMC_DTUPRD1__DTU_ALLBITS_2__WIDTH       16
#define MEMC_DTUPRD1__DTU_ALLBITS_2__MASK        0x0000FFFF
#define MEMC_DTUPRD1__DTU_ALLBITS_2__INV_MASK    0xFFFF0000
#define MEMC_DTUPRD1__DTU_ALLBITS_2__HW_DEFAULT  0x0

/* MEMC_DTUPRD1.dtu_allbits_3 - Allows all the bit 3 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD1__DTU_ALLBITS_3__SHIFT       16
#define MEMC_DTUPRD1__DTU_ALLBITS_3__WIDTH       16
#define MEMC_DTUPRD1__DTU_ALLBITS_3__MASK        0xFFFF0000
#define MEMC_DTUPRD1__DTU_ALLBITS_3__INV_MASK    0x0000FFFF
#define MEMC_DTUPRD1__DTU_ALLBITS_3__HW_DEFAULT  0x0

/* DTUPRD2 */
/* DTU Parallel Read 2 */
#define MEMC_DTUPRD2              0x108000A8

/* MEMC_DTUPRD2.dtu_allbits_4 - Allows all the bit 4 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD2__DTU_ALLBITS_4__SHIFT       0
#define MEMC_DTUPRD2__DTU_ALLBITS_4__WIDTH       16
#define MEMC_DTUPRD2__DTU_ALLBITS_4__MASK        0x0000FFFF
#define MEMC_DTUPRD2__DTU_ALLBITS_4__INV_MASK    0xFFFF0000
#define MEMC_DTUPRD2__DTU_ALLBITS_4__HW_DEFAULT  0x0

/* MEMC_DTUPRD2.dtu_allbits_5 - Allows all the bit 5 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD2__DTU_ALLBITS_5__SHIFT       16
#define MEMC_DTUPRD2__DTU_ALLBITS_5__WIDTH       16
#define MEMC_DTUPRD2__DTU_ALLBITS_5__MASK        0xFFFF0000
#define MEMC_DTUPRD2__DTU_ALLBITS_5__INV_MASK    0x0000FFFF
#define MEMC_DTUPRD2__DTU_ALLBITS_5__HW_DEFAULT  0x0

/* DTUPRD3 */
/* DTU Parallel Read 3 */
#define MEMC_DTUPRD3              0x108000AC

/* MEMC_DTUPRD3.dtu_allbits_6 - Allows all the bit 6 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD3__DTU_ALLBITS_6__SHIFT       0
#define MEMC_DTUPRD3__DTU_ALLBITS_6__WIDTH       16
#define MEMC_DTUPRD3__DTU_ALLBITS_6__MASK        0x0000FFFF
#define MEMC_DTUPRD3__DTU_ALLBITS_6__INV_MASK    0xFFFF0000
#define MEMC_DTUPRD3__DTU_ALLBITS_6__HW_DEFAULT  0x0

/* MEMC_DTUPRD3.dtu_allbits_7 - Allows all the bit 7 from each of the received read bytes to be read in parallel.Used as part of read data eye training where a transition is required to be monitored to train the eye. */
#define MEMC_DTUPRD3__DTU_ALLBITS_7__SHIFT       16
#define MEMC_DTUPRD3__DTU_ALLBITS_7__WIDTH       16
#define MEMC_DTUPRD3__DTU_ALLBITS_7__MASK        0xFFFF0000
#define MEMC_DTUPRD3__DTU_ALLBITS_7__INV_MASK    0x0000FFFF
#define MEMC_DTUPRD3__DTU_ALLBITS_7__HW_DEFAULT  0x0

/* DTUAWDT */
/* DTU Address Width */
#define MEMC_DTUAWDT              0x108000B0

/* MEMC_DTUAWDT.column_addr_width - Width of the memory column address bits 
2'b00 = 7 bits wide 
2'b01 = 8 bits wide 
2'b10 = 9 bits wide 
2'b11 = 10 bits wide 
<b>Default Value:</b> UPCTL_M_CAW-7 */
#define MEMC_DTUAWDT__COLUMN_ADDR_WIDTH__SHIFT       0
#define MEMC_DTUAWDT__COLUMN_ADDR_WIDTH__WIDTH       2
#define MEMC_DTUAWDT__COLUMN_ADDR_WIDTH__MASK        0x00000003
#define MEMC_DTUAWDT__COLUMN_ADDR_WIDTH__INV_MASK    0xFFFFFFFC
#define MEMC_DTUAWDT__COLUMN_ADDR_WIDTH__HW_DEFAULT  0x3

/* MEMC_DTUAWDT.bank_addr_width - Width of the memory bank address bits 
2'b00 = 2 bits wide (4 banks) 
2'b01 = 3 bits wide (8 banks) 
Others = Reserved 
<b>Default Value:</b> UPCTL_M_BAW-2 */
#define MEMC_DTUAWDT__BANK_ADDR_WIDTH__SHIFT       3
#define MEMC_DTUAWDT__BANK_ADDR_WIDTH__WIDTH       2
#define MEMC_DTUAWDT__BANK_ADDR_WIDTH__MASK        0x00000018
#define MEMC_DTUAWDT__BANK_ADDR_WIDTH__INV_MASK    0xFFFFFFE7
#define MEMC_DTUAWDT__BANK_ADDR_WIDTH__HW_DEFAULT  0x1

/* MEMC_DTUAWDT.row_addr_width - Width of the memory row address bits 
2'b00 = 13 bits wide 
2'b01 = 14 bits wide 
2'b10 = 15 bits wide 
2'b11 = 16 bits wide 
<b>Default Value:</b> UPCTL_M_RAW-13 */
#define MEMC_DTUAWDT__ROW_ADDR_WIDTH__SHIFT       6
#define MEMC_DTUAWDT__ROW_ADDR_WIDTH__WIDTH       2
#define MEMC_DTUAWDT__ROW_ADDR_WIDTH__MASK        0x000000C0
#define MEMC_DTUAWDT__ROW_ADDR_WIDTH__INV_MASK    0xFFFFFF3F
#define MEMC_DTUAWDT__ROW_ADDR_WIDTH__HW_DEFAULT  0x3

/* MEMC_DTUAWDT.number_ranks - Number of supported memory ranks. 
2'b00 = 1 rank 
2'b01 = 2 ranks 
2'b10 = 3 ranks 
2'b11 = 4 ranks 
<b>Default Value:</b> UPCTL_M_NRANKS-1 */
#define MEMC_DTUAWDT__NUMBER_RANKS__SHIFT       9
#define MEMC_DTUAWDT__NUMBER_RANKS__WIDTH       2
#define MEMC_DTUAWDT__NUMBER_RANKS__MASK        0x00000600
#define MEMC_DTUAWDT__NUMBER_RANKS__INV_MASK    0xFFFFF9FF
#define MEMC_DTUAWDT__NUMBER_RANKS__HW_DEFAULT  0x0

/* TOGCNT1U */
/* Toggle Counter 1U Register */
#define MEMC_TOGCNT1U             0x108000C0

/* MEMC_TOGCNT1U.toggle_counter_1u - The number of internal timers clock cycles in 1 us. 
<b>Default Value:</b> 10'h064 */
#define MEMC_TOGCNT1U__TOGGLE_COUNTER_1U__SHIFT       0
#define MEMC_TOGCNT1U__TOGGLE_COUNTER_1U__WIDTH       10
#define MEMC_TOGCNT1U__TOGGLE_COUNTER_1U__MASK        0x000003FF
#define MEMC_TOGCNT1U__TOGGLE_COUNTER_1U__INV_MASK    0xFFFFFC00
#define MEMC_TOGCNT1U__TOGGLE_COUNTER_1U__HW_DEFAULT  0x64

/* TINIT */
/* t_init Timing Register */
#define MEMC_TINIT                0x108000C4

/* MEMC_TINIT.t_init - Defines the time period (in us) to hold dfi_cke and dfi_reset_n stable during the memory power up sequence. The value programmed must correspond to at least 200 us. The actual time period defined is TINIT * TOGCNT1U * internal timers clock period. During Verilog simulations, changing this value to a small value (such as 1) is recommended, to avoid excessively long simulation times. This may cause a memory model error, due to a violation of the CKE setup sequence, but this is expected if this value is not programmed to at least 200us and memory models should be able to tolerate this violation without malfunction of the model. 
<b>Default Value:</b> 9'h0C8 */
#define MEMC_TINIT__T_INIT__SHIFT       0
#define MEMC_TINIT__T_INIT__WIDTH       9
#define MEMC_TINIT__T_INIT__MASK        0x000001FF
#define MEMC_TINIT__T_INIT__INV_MASK    0xFFFFFE00
#define MEMC_TINIT__T_INIT__HW_DEFAULT  0xC8

/* TRSTH */
/* t_rsth Timing Register */
#define MEMC_TRSTH                0x108000C8

/* MEMC_TRSTH.t_rsth - Defines the time period (in us) to hold the dfi_reset_n signal high after it is deasserted during the DDR3 Power Up/Reset sequence. The value programmed for DDR3 must correspond to minimum 500us of delay. For mDDR and DDR2, this register should be programmed to 0.The actual time period defined is TRSTH * TOGCNT1U * internal timers clock period. During DDR3 Verilog simulations, changing this value to a small value (such as 1) instead of a value of 500 or more is recommended, to avoid excessively long simulation times. This may cause a memory model error, due to a violation of the RST_N setup sequence, but this is expected if this value is not programmed to at least 500us and memory models should be able to tolerate this violation without malfunction of the model. 
<b>Default Value:</b> 0 */
#define MEMC_TRSTH__T_RSTH__SHIFT       0
#define MEMC_TRSTH__T_RSTH__WIDTH       10
#define MEMC_TRSTH__T_RSTH__MASK        0x000003FF
#define MEMC_TRSTH__T_RSTH__INV_MASK    0xFFFFFC00
#define MEMC_TRSTH__T_RSTH__HW_DEFAULT  0x0

/* TOGCNT100N */
/* Toggle Counter 100N Register */
#define MEMC_TOGCNT100N           0x108000CC

/* MEMC_TOGCNT100N.toggle_counter_100n - The number of internal timers clock cycles in 100 ns. This internal timer is used in conjunction with TREFI to count periodic refresh intervals. 
<b>Default Value:</b> 1 */
#define MEMC_TOGCNT100N__TOGGLE_COUNTER_100N__SHIFT       0
#define MEMC_TOGCNT100N__TOGGLE_COUNTER_100N__WIDTH       7
#define MEMC_TOGCNT100N__TOGGLE_COUNTER_100N__MASK        0x0000007F
#define MEMC_TOGCNT100N__TOGGLE_COUNTER_100N__INV_MASK    0xFFFFFF80
#define MEMC_TOGCNT100N__TOGGLE_COUNTER_100N__HW_DEFAULT  0x1

/* TREFI */
/* t_refi Timing Register */
#define MEMC_TREFI                0x108000D0

/* MEMC_TREFI.t_refi - Defines the time period (in 100ns units) of the Refresh interval. The actual time period defined is TREFI * TOGCNT100N * internal timers clock period. Programming a value of 0 in t_refi disables the generation of Refresh commands. 
<b>Default Value:</b> 2 */
#define MEMC_TREFI__T_REFI__SHIFT       0
#define MEMC_TREFI__T_REFI__WIDTH       13
#define MEMC_TREFI__T_REFI__MASK        0x00001FFF
#define MEMC_TREFI__T_REFI__INV_MASK    0xFFFFE000
#define MEMC_TREFI__T_REFI__HW_DEFAULT  0x4E

/* MEMC_TREFI.num_add_ref - Number of additional Refreshes per t_refi interval. 
0-1 refresh performed every t_refi interval 
1-2 refreshes performed every t_refi interval 
2-3 refreshes performed every t_refi interval 
3-4 refreshes performed every t_refi interval 
4-5 refreshes performed every t_refi interval 
5-6 refreshes performed every t_refi interval 
6-7 refreshes performed every t_refi interval 
7-8 refreshes performed every t_refi interval 
For mDDR, max supported value is 6, as tRFCmax is 8*tREFI. 
<b>Default Value:</b> 0 */
#define MEMC_TREFI__NUM_ADD_REF__SHIFT       16
#define MEMC_TREFI__NUM_ADD_REF__WIDTH       3
#define MEMC_TREFI__NUM_ADD_REF__MASK        0x00070000
#define MEMC_TREFI__NUM_ADD_REF__INV_MASK    0xFFF8FFFF
#define MEMC_TREFI__NUM_ADD_REF__HW_DEFAULT  0x0

/* MEMC_TREFI.upd_ref - Update Refresh counter values. Write 1'b1 to this bit whenever num_add_ref and/or t_refi register bit fields are updated. When this is set to 1'b1, the internal counters for Refresh interval and for how many refreshes performed each refresh interval get updated to use latest values of t_refi and num_add_ref respectively. 
It is cleared to 1'b0 automatically by hardware within a couple of clock cycles. 
This bit cannot be cleared to 1'b0 by software. 
If not set to 1'b1 when num_add_ref/t_refi are changed, then the previous values are continued to be used by the internal logic. 
<b>Default Value:</b> 0 */
#define MEMC_TREFI__UPD_REF__SHIFT       31
#define MEMC_TREFI__UPD_REF__WIDTH       1
#define MEMC_TREFI__UPD_REF__MASK        0x80000000
#define MEMC_TREFI__UPD_REF__INV_MASK    0x7FFFFFFF
#define MEMC_TREFI__UPD_REF__HW_DEFAULT  0x0

/* TMRD */
/* t_mrd Timing Register */
#define MEMC_TMRD                 0x108000D4

/* MEMC_TMRD.t_mrd - Mode Register Set command cycle time in memory clock cycles. 
mDDR: Time from MRS to any valid command. 
LPDDR2/LPDDR3: Time from MRS (MRW) to any valid command. 
DDR2: Time from MRS to any valid command. 
DDR3: Time from MRS to MRS command.
 
mDDR Legal Values: 2 
LPDDR2 Legal Values: 5 
LPDDR3 Legal Values: 5 
DDR2 Legal Values: 2..3 
DDR3 Legal Values: 2..4 
<b>Default value:</b> 1 */
#define MEMC_TMRD__T_MRD__SHIFT       0
#define MEMC_TMRD__T_MRD__WIDTH       3
#define MEMC_TMRD__T_MRD__MASK        0x00000007
#define MEMC_TMRD__T_MRD__INV_MASK    0xFFFFFFF8
#define MEMC_TMRD__T_MRD__HW_DEFAULT  0x1

/* TRFC */
/* t_rfc Timing Register */
#define MEMC_TRFC                 0x108000D8

/* MEMC_TRFC.t_rfc - Refresh to Active/Refresh command time in memory clock cycles. 
mDDR Legal Values: 7..28 
LPDDR2 Legal Values: 15..112 
LPDDR3 Legal Values: 22..168 
DDR2 Legal Values: 15..131 
DDR3 Legal Values: 36.. 374
 
<b>Default Value:</b> 2 */
#define MEMC_TRFC__T_RFC__SHIFT       0
#define MEMC_TRFC__T_RFC__WIDTH       9
#define MEMC_TRFC__T_RFC__MASK        0x000001FF
#define MEMC_TRFC__T_RFC__INV_MASK    0xFFFFFE00
#define MEMC_TRFC__T_RFC__HW_DEFAULT  0x2

/* TRP */
/* t_rp Timing Register */
#define MEMC_TRP                  0x108000DC

/* MEMC_TRP.t_rp - Precharge period in memory clock cycles. 
For LPDDR2/LPDDR3, this should be set to TRPpb. 
mDDR Legal Values: 2..3 
LPDDR2 Legal Values: 3..13 
LPDDR3 Legal Values: 3..20 
DDR2 Legal Values: 3..7 
DDR3 Legal Values: 5..14 
<b>Default Value:</b> 6 */
#define MEMC_TRP__T_RP__SHIFT       0
#define MEMC_TRP__T_RP__WIDTH       5
#define MEMC_TRP__T_RP__MASK        0x0000001F
#define MEMC_TRP__T_RP__INV_MASK    0xFFFFFFE0
#define MEMC_TRP__T_RP__HW_DEFAULT  0x6

/* MEMC_TRP.prea_extra - Additional cycles required for a Precharge All (PREA) command - in addition to t_rp. In terms of memory clock cycles 
mDDR Value: 0 
LPDDR2 Value: Value that corresponds (tRPab -tRPpb). Rounded up in terms of memory clock cycles. Values can be 0, 1, 2. 
LPDDR3 Value: Value that corresponds (tRPab -tRPpb). Rounded up in terms of memory clock cycles. Values can be 0, 1, 2, 3. 
DDR2 Value: 1 if 8 Banks, 0 otherwise 
DDR3 Value: 0
 
<b>Default Value:</b> 1  */
#define MEMC_TRP__PREA_EXTRA__SHIFT       16
#define MEMC_TRP__PREA_EXTRA__WIDTH       2
#define MEMC_TRP__PREA_EXTRA__MASK        0x00030000
#define MEMC_TRP__PREA_EXTRA__INV_MASK    0xFFFCFFFF
#define MEMC_TRP__PREA_EXTRA__HW_DEFAULT  0x1

/* TRTW */
/* t_rtw Timing Register */
#define MEMC_TRTW                 0x108000E0

/* MEMC_TRTW.t_rtw - Read to Write turnaround time in memory clock cycles. 
mDDR Legal Values: 3..11 
LPDDR2 Legal Values: 1..11 
LPDDR3 Legal Values: 1..12 
DDR2 Legal Values: 2..10 
DDR3 Legal Values: 2..10 
<b>Default Value:</b> 2 */
#define MEMC_TRTW__T_RTW__SHIFT       0
#define MEMC_TRTW__T_RTW__WIDTH       4
#define MEMC_TRTW__T_RTW__MASK        0x0000000F
#define MEMC_TRTW__T_RTW__INV_MASK    0xFFFFFFF0
#define MEMC_TRTW__T_RTW__HW_DEFAULT  0x2

/* TAL */
/* Additive Latency Register */
#define MEMC_TAL                  0x108000E4

/* MEMC_TAL.t_al - Additive Latency in memory clock cycles. 
For DDR2 this must match the value programmed into the AL field of MR1. 
For DDR3 this must be 0, CL-1, CL-2 depending weather the AL value in MR1 is 0,1, or 2 respectively. CL is the CAS latency programmed into MR0. 
For mDDR, LPDDR2 and LPDDR3, there is no AL field in the mode registers, and this setting should be set to 0. 
mDDR Legal Values: 0 
LPDDR2 Legal Values: 0 
LPDDR3 Legal Values: 0 
DDR2 Legal Values: AL 
DDR3 Legal Values: 0, CL-1, CL-2 (depending on AL=0,1,2 in MR1) 
<b>Default Value:</b> 0 */
#define MEMC_TAL__T_AL__SHIFT       0
#define MEMC_TAL__T_AL__WIDTH       4
#define MEMC_TAL__T_AL__MASK        0x0000000F
#define MEMC_TAL__T_AL__INV_MASK    0xFFFFFFF0
#define MEMC_TAL__T_AL__HW_DEFAULT  0x0

/* TCL */
/* CAS Latency Timing Register */
#define MEMC_TCL                  0x108000E8

/* MEMC_TCL.t_cl - CAS Latency in memory clock cycles. 
If mDDR/DDR2/DDR3, the uPCTL setting must match the value programmed into the CL field of MR0. 
If LPDDR2/LPDDR3, the uPCTL setting must match RL (Read Latency), where RL is the value programmed into the "RL &amp; WL" field of MR2 
mDDR/DDR2/3 Legal Value: CL 
LPDDR2/LPDDR3 Legal Value: RL
 
<b>Default Value:</b> 4 */
#define MEMC_TCL__T_CL__SHIFT       0
#define MEMC_TCL__T_CL__WIDTH       4
#define MEMC_TCL__T_CL__MASK        0x0000000F
#define MEMC_TCL__T_CL__INV_MASK    0xFFFFFFF0
#define MEMC_TCL__T_CL__HW_DEFAULT  0x4

/* TCWL */
/* CAS Write Latency Timing Register */
#define MEMC_TCWL                 0x108000EC

/* MEMC_TCWL.t_cwl - CAS Write Latency in memory clock cycles. 
For mDDR, the setting must be 1. 
For LPDDR2/LPDDR3 the setting must match WL (Write Latency), where WL is the value programmed into the "RL &amp; WL" field of MR2. 
For DDR2 the setting must match CL-1, where CL is the value programmed into the CL field of MR0. 
For DDR3, the setting must match the value programmed in the memory CWL field of MR2. 
mDDR Legal Value: 1 
LPDDR2/LPDDR3 Legal Values: WL 
DDR2 Legal Value: CL-1 
DDR3 Legal Value: CWL 
<b>Default Value:</b> 3 */
#define MEMC_TCWL__T_CWL__SHIFT       0
#define MEMC_TCWL__T_CWL__WIDTH       4
#define MEMC_TCWL__T_CWL__MASK        0x0000000F
#define MEMC_TCWL__T_CWL__INV_MASK    0xFFFFFFF0
#define MEMC_TCWL__T_CWL__HW_DEFAULT  0x3

/* TRAS */
/* t_ras Timing Register */
#define MEMC_TRAS                 0x108000F0

/* MEMC_TRAS.t_ras - Activate to Precharge command time in memory clock cycles. 
mDDR Legal Values: 4..8 
LPDDR2 Legal Values: 7..23 
LPDDR3 Legal Values: 7..34 
DDR2 Legal Values: 8..24 
DDR3 Legal Values: 15..38 
<b>Default Value:</b> 16 */
#define MEMC_TRAS__T_RAS__SHIFT       0
#define MEMC_TRAS__T_RAS__WIDTH       6
#define MEMC_TRAS__T_RAS__MASK        0x0000003F
#define MEMC_TRAS__T_RAS__INV_MASK    0xFFFFFFC0
#define MEMC_TRAS__T_RAS__HW_DEFAULT  0x10

/* TRC */
/* t_rc Timing Register */
#define MEMC_TRC                  0x108000F4

/* MEMC_TRC.t_rc - Row Cycle time in memory clock cycles. Specifies the minimum Activate to Activate distance for accesses to same bank. 
mDDR Legal Values: 5..11 
LPDDR2 Legal Values: 10..36 
LPDDR3 Legal Values: 11..53 
DDR2 Legal Values: 11..31 
DDR3 Legal Values: 20..52 
<b>Default Value:</b> 22 */
#define MEMC_TRC__T_RC__SHIFT       0
#define MEMC_TRC__T_RC__WIDTH       6
#define MEMC_TRC__T_RC__MASK        0x0000003F
#define MEMC_TRC__T_RC__INV_MASK    0xFFFFFFC0
#define MEMC_TRC__T_RC__HW_DEFAULT  0x16

/* TRCD */
/* t_rcd Timing Register */
#define MEMC_TRCD                 0x108000F8

/* MEMC_TRCD.t_rcd - Row to Column delay in memory clock cycles. Specifies the minimum Activate to Column distance. 
mDDR Legal Values: 2..3 
LPDDR2 Legal Values: 3..13 
LPDDR3 Legal Values: 4..20 
DDR2 Legal Values: 3..7 
DDR3 Legal Values: 5..14 
<b>Default Value:</b> 6 */
#define MEMC_TRCD__T_RCD__SHIFT       0
#define MEMC_TRCD__T_RCD__WIDTH       5
#define MEMC_TRCD__T_RCD__MASK        0x0000001F
#define MEMC_TRCD__T_RCD__INV_MASK    0xFFFFFFE0
#define MEMC_TRCD__T_RCD__HW_DEFAULT  0x6

/* TRRD */
/* t_rrd Timing Register */
#define MEMC_TRRD                 0x108000FC

/* MEMC_TRRD.t_rrd - Row-to-Row delay in memory clock cycles. Specifies the minimum Activate-to-Activate distance for consecutive accesses to different banks in the same rank. 
mDDR Legal Values: 1..2 
LPDDR2 Legal Values: 2..6 
LPDDR3 Legal Values: 2..8 
DDR2 Legal Values: 2..6 
DDR3 Legal Values: 4..8 
<b>Default Value:</b> 4 */
#define MEMC_TRRD__T_RRD__SHIFT       0
#define MEMC_TRRD__T_RRD__WIDTH       4
#define MEMC_TRRD__T_RRD__MASK        0x0000000F
#define MEMC_TRRD__T_RRD__INV_MASK    0xFFFFFFF0
#define MEMC_TRRD__T_RRD__HW_DEFAULT  0x4

/* TRTP */
/* t_rtp Timing Register */
#define MEMC_TRTP                 0x10800100

/* MEMC_TRTP.t_rtp - Read to Precharge time in memory clock cycles. Specifies the minimum distance Read to Precharge for consecutive accesses to same bank. 
mDDR Value: 0 
LPDDR2 Legal Values: 2..4 
LPDDR3 Legal Values: 2..6 
DDR2 Legal Values: 2..4 
DDR3 Legal Values: 3..8 
<b>Default Value:</b> 3 */
#define MEMC_TRTP__T_RTP__SHIFT       0
#define MEMC_TRTP__T_RTP__WIDTH       4
#define MEMC_TRTP__T_RTP__MASK        0x0000000F
#define MEMC_TRTP__T_RTP__INV_MASK    0xFFFFFFF0
#define MEMC_TRTP__T_RTP__HW_DEFAULT  0x3

/* TWR */
/* t_wr Timing Register */
#define MEMC_TWR                  0x10800104

/* MEMC_TWR.t_wr - Write recovery time in memory clock cycles. When using close page the uPCTL setting must be consistent with the WR field setting of MR0. 
mDDR Legal Values: 2..3 
LPDDR2 Legal Values: 3..8 
LPDDR3 Legal Values: 3..12 
DDR2 Legal Values: 3..8 
DDR3 Legal Values: 6..16 
<b>Default Value:</b> 6 */
#define MEMC_TWR__T_WR__SHIFT       0
#define MEMC_TWR__T_WR__WIDTH       5
#define MEMC_TWR__T_WR__MASK        0x0000001F
#define MEMC_TWR__T_WR__INV_MASK    0xFFFFFFE0
#define MEMC_TWR__T_WR__HW_DEFAULT  0x6

/* TWTR */
/* t_wtr Timing Register */
#define MEMC_TWTR                 0x10800108

/* MEMC_TWTR.t_wtr - Write to Read turnaround time, in memory clock cycles. 
mDDR Legal Values: 1..2 
LPDDR2 Legal Values: 2..4 
LPDDR3 Legal Values: 4..6 
DDR2 Legal Values: 2..4 
DDR3 Legal Values: 3..8 
<b>Default Value:</b> 4 */
#define MEMC_TWTR__T_WTR__SHIFT       0
#define MEMC_TWTR__T_WTR__WIDTH       4
#define MEMC_TWTR__T_WTR__MASK        0x0000000F
#define MEMC_TWTR__T_WTR__INV_MASK    0xFFFFFFF0
#define MEMC_TWTR__T_WTR__HW_DEFAULT  0x4

/* TEXSR */
/* t_exsr Timing Register */
#define MEMC_TEXSR                0x1080010C

/* MEMC_TEXSR.t_exsr - Exit Self Refresh to first valid command delay, in memory clock cycles. 
For mDDR, this should be programmed to match tXSR. 
For LPDDR2/LPDDR3, this should be programmed to match tXSR. 
For DDR2, this should be programmed to match tXSRD (SRE to read-related command) as defined by the memory device specification. 
For DDR3, this should be programmed to match tXSDLL (SRE to a command requiring DLL locked) as defined by the memory device specification. 
mDDR Legal Values: 17..40 
LPDDR2 Legal Values: 17..117 
LPDDR3 Legal Values: 24..176 
DDR2 Typical Value: 200 
DDR3 Typical Value: 512 
<b>Default Value:</b> 1 */
#define MEMC_TEXSR__T_EXSR__SHIFT       0
#define MEMC_TEXSR__T_EXSR__WIDTH       10
#define MEMC_TEXSR__T_EXSR__MASK        0x000003FF
#define MEMC_TEXSR__T_EXSR__INV_MASK    0xFFFFFC00
#define MEMC_TEXSR__T_EXSR__HW_DEFAULT  0x1

/* TXP */
/* t_xp Timing Register */
#define MEMC_TXP                  0x10800110

/* MEMC_TXP.t_xp - Exit Power Down to first valid command delay when DLL is on (fast exit), measured in memory clock cycles. 
<b>Default Value:</b> 1 
<b>Legal Values:</b> 1..7 */
#define MEMC_TXP__T_XP__SHIFT       0
#define MEMC_TXP__T_XP__WIDTH       3
#define MEMC_TXP__T_XP__MASK        0x00000007
#define MEMC_TXP__T_XP__INV_MASK    0xFFFFFFF8
#define MEMC_TXP__T_XP__HW_DEFAULT  0x1

/* TXPDLL */
/* t_xpdll Timing Register */
#define MEMC_TXPDLL               0x10800114

/* MEMC_TXPDLL.t_xpdll - Exit Power Down to first valid command delay when DLL is off (slow exit), measured in memory clock cycles. 
mDDR/LPDDR2/LPDDR3 Value: 0 
DDR2/DDR3 Legal Values: 3..63 
<b>Default value:</b> 0 */
#define MEMC_TXPDLL__T_XPDLL__SHIFT       0
#define MEMC_TXPDLL__T_XPDLL__WIDTH       6
#define MEMC_TXPDLL__T_XPDLL__MASK        0x0000003F
#define MEMC_TXPDLL__T_XPDLL__INV_MASK    0xFFFFFFC0
#define MEMC_TXPDLL__T_XPDLL__HW_DEFAULT  0x0

/* TZQCS */
/* t_zqcs Timing Register */
#define MEMC_TZQCS                0x10800118

/* MEMC_TZQCS.t_zqcs - SDRAM ZQ Calibration Short period, in memory clock cycles. Should be programmed to match the tZQCS timing value as defined in the memory specification. 
mDDR Value: 0 
LPDDR2 Legal Values: 15..48 
LPDDR3 Legal Values: 15..72 
DDR2 Value: 0 
DDR3 Typical Value: 64 
<b>Default Value:</b> 0 */
#define MEMC_TZQCS__T_ZQCS__SHIFT       0
#define MEMC_TZQCS__T_ZQCS__WIDTH       7
#define MEMC_TZQCS__T_ZQCS__MASK        0x0000007F
#define MEMC_TZQCS__T_ZQCS__INV_MASK    0xFFFFFF80
#define MEMC_TZQCS__T_ZQCS__HW_DEFAULT  0x0

/* TZQCSI */
/* t_zqcsi Timing Register */
#define MEMC_TZQCSI               0x1080011C

/* MEMC_TZQCSI.t_zqcsi - SDRAM ZQCS interval, measured in Refresh interval units. The total time period defined is TZQCSI*TREFI * TOGCNT100N * internal timers clock period.Programming a value of 0 in t_zqcsi disables the auto-ZQCS functionality in the uPCTL. 
mDDR Value: 0 
LPDDR2 Legal Values: 0..4294967295 
LPDDR3 Legal Values: 0..4294967295 
DDR2 Value: 0 
DDR3 Legal Values: 0..4294967295 
<b>Default Value:</b> 0 */
#define MEMC_TZQCSI__T_ZQCSI__SHIFT       0
#define MEMC_TZQCSI__T_ZQCSI__WIDTH       32
#define MEMC_TZQCSI__T_ZQCSI__MASK        0xFFFFFFFF
#define MEMC_TZQCSI__T_ZQCSI__INV_MASK    0x00000000
#define MEMC_TZQCSI__T_ZQCSI__HW_DEFAULT  0x0

/* TDQS */
/* t_dqs Timing Register */
#define MEMC_TDQS                 0x10800120

/* MEMC_TDQS.t_dqs - Additional data turnaround time in memory clock cycles for accesses to different ranks. Used to increase the distance between column commands to different ranks, allowing more tolerance as the driver source changes on the bidirectional DQS and/or DQ signals. 
mDDR Legal Values: 1..12 
LPDDR2 Legal Values: 1..12 
LPDDR3 Legal Values: 1..12 
DDR2 Legal Values: 1..12 
DDR3 Legal Values: 1..12 
<b>Default value:</b> 1 */
#define MEMC_TDQS__T_DQS__SHIFT       0
#define MEMC_TDQS__T_DQS__WIDTH       4
#define MEMC_TDQS__T_DQS__MASK        0x0000000F
#define MEMC_TDQS__T_DQS__INV_MASK    0xFFFFFFF0
#define MEMC_TDQS__T_DQS__HW_DEFAULT  0x1

/* TCKSRE */
/* Self-refresh Entry Timing Register */
#define MEMC_TCKSRE               0x10800124

/* MEMC_TCKSRE.t_cksre - This is the time after Self Refresh Entry that CKE is held high before going low. In memory clock cycles. Specifies the clock disable delay after SRE. 
In DDR3, this should be programmed to match the greatest value between 10ns and 5 memory clock periods. 
In LPDDR3, this should be programmed to 2. 
mDDR Value: 0 
LPDDR2 Value: 0 
LPDDR3 Value: 2 
DDR2 Value: 0 
DDR3 Legal Values: 5..15 
<b>Default Value:</b> 0 */
#define MEMC_TCKSRE__T_CKSRE__SHIFT       0
#define MEMC_TCKSRE__T_CKSRE__WIDTH       5
#define MEMC_TCKSRE__T_CKSRE__MASK        0x0000001F
#define MEMC_TCKSRE__T_CKSRE__INV_MASK    0xFFFFFFE0
#define MEMC_TCKSRE__T_CKSRE__HW_DEFAULT  0x0

/* TCKSRX */
/* Self-refresh Exit Timing Register */
#define MEMC_TCKSRX               0x10800128

/* MEMC_TCKSRX.t_cksrx - This is the time (before Self Refresh Exit) that CKE is maintained high before issuing SRX. In memory clock cycles. Specifies the clock stable time before SRX. 
In DDR3, This should be programmed to match the greatest value between 10ns and 5 memory clock periods. 
In LPDDR2/LPDDR3, this should be programmed to 2. 
mDDR Value: 0 
LPDDR2 Value: 2 
LPDDR3 Legal Values: 2 
DDR2 Value: 0 
DDR3 Legal Values: 5..15 
<b>Default Value:</b> 0 */
#define MEMC_TCKSRX__T_CKSRX__SHIFT       0
#define MEMC_TCKSRX__T_CKSRX__WIDTH       5
#define MEMC_TCKSRX__T_CKSRX__MASK        0x0000001F
#define MEMC_TCKSRX__T_CKSRX__INV_MASK    0xFFFFFFE0
#define MEMC_TCKSRX__T_CKSRX__HW_DEFAULT  0x0

/* TCKE */
/* t_cke Timing Register */
#define MEMC_TCKE                 0x1080012C

/* MEMC_TCKE.t_cke - CKE minimum pulse width in memory clock cycles. 
mDDR Legal Value: 2 
LPDDR2 Legal Values: 3 
LPDDR3 Legal Values: 3 
DDR2 Legal Value: 3 
DDR3 Legal Values: 3..6 
<b>Default Value:</b> 3 */
#define MEMC_TCKE__T_CKE__SHIFT       0
#define MEMC_TCKE__T_CKE__WIDTH       3
#define MEMC_TCKE__T_CKE__MASK        0x00000007
#define MEMC_TCKE__T_CKE__INV_MASK    0xFFFFFFF8
#define MEMC_TCKE__T_CKE__HW_DEFAULT  0x3

/* TMOD */
/* t_mod Timing Register */
#define MEMC_TMOD                 0x10800130

/* MEMC_TMOD.t_mod - In DDR3 mode, this is the time from MRS to any valid non-MRS command (except DESELECT or NOP) in memory clock cycles. 
mDDR Value: 0 
LPDDR2 Value: 0 
LPDDR3 Legal Values: 0 
DDR2 Value: 0 
DDR3 Legal Values: 0..31 
<b>Default Value:</b> 0 */
#define MEMC_TMOD__T_MOD__SHIFT       0
#define MEMC_TMOD__T_MOD__WIDTH       5
#define MEMC_TMOD__T_MOD__MASK        0x0000001F
#define MEMC_TMOD__T_MOD__INV_MASK    0xFFFFFFE0
#define MEMC_TMOD__T_MOD__HW_DEFAULT  0x0

/* TRSTL */
/* t_rstl Timing Register */
#define MEMC_TRSTL                0x10800134

/* MEMC_TRSTL.t_rstl - Memory Reset Low time, in memory clock cycles. Defines the time period to hold dfi_reset_n signal low during a software driven DDR3 Reset Operation. The value programmed must correspond to at least 100 ns of delay. 
mDDR Value: 0 
LPDDR2 Value: 0 
LPDDR3 Value: 0 
DDR2 Value: 0 
DDR3 Legal Values: 1..127 
<b>Default Value:</b> 0 */
#define MEMC_TRSTL__T_RSTL__SHIFT       0
#define MEMC_TRSTL__T_RSTL__WIDTH       7
#define MEMC_TRSTL__T_RSTL__MASK        0x0000007F
#define MEMC_TRSTL__T_RSTL__INV_MASK    0xFFFFFF80
#define MEMC_TRSTL__T_RSTL__HW_DEFAULT  0x0

/* TZQCL */
/* t_zqcl Timing Register */
#define MEMC_TZQCL                0x10800138

/* MEMC_TZQCL.t_zqcl - SDRAM ZQ Calibration Long period in memory clock cycles. If LPDDR2/LPDDR3, should be programmed to tZQCL. If DDR3, should be programmed to match the memory tZQinit timing value for the first ZQCL command during memory initialization; should be programmed to match tZQoper timing value after reset and initialization. 
mDDR Value: 0 
LPDDR2 Legal Values: 60..192 
LPDDR3 Legal Values: 60..288 
DDR2 Value: 0 
DDR3 Legal Values: 0..1023 
<b>Default Value:</b> 0 */
#define MEMC_TZQCL__T_ZQCL__SHIFT       0
#define MEMC_TZQCL__T_ZQCL__WIDTH       10
#define MEMC_TZQCL__T_ZQCL__MASK        0x000003FF
#define MEMC_TZQCL__T_ZQCL__INV_MASK    0xFFFFFC00
#define MEMC_TZQCL__T_ZQCL__HW_DEFAULT  0x0

/* TMRR */
/* t_mrr Timing Register */
#define MEMC_TMRR                 0x1080013C

/* MEMC_TMRR.t_mrr - Time for a Mode Register Read (MRR command from MCMD). 
<b>Default Value:</b> 8'h02 */
#define MEMC_TMRR__T_MRR__SHIFT       0
#define MEMC_TMRR__T_MRR__WIDTH       8
#define MEMC_TMRR__T_MRR__MASK        0x000000FF
#define MEMC_TMRR__T_MRR__INV_MASK    0xFFFFFF00
#define MEMC_TMRR__T_MRR__HW_DEFAULT  0x2

/* TCKESR */
/* t_ckesr Timing Register */
#define MEMC_TCKESR               0x10800140

/* MEMC_TCKESR.t_ckesr - Minimum CKE low width for Self Refresh entry to exit timing in memory clock cycles. Recommended settings: 
mDDR: t_ckesr = 0 
LPDDR2/LPDDR3: t_ckesr = tCKESR setting from memories, rounded up in terms of memory cycles. 
DDR2: t_ckesr = 0 
DDR3: t_ckesr = t_cke + 1 
mDDR Value: 0 
LPDDR2 Legal Values: 3..8 
LPDDR3 Legal Values: 3..12 
DDR2 Value: 0 
DDR3 Legal Values: 4..7 
<b>Default Value:</b> 4 */
#define MEMC_TCKESR__T_CKESR__SHIFT       0
#define MEMC_TCKESR__T_CKESR__WIDTH       4
#define MEMC_TCKESR__T_CKESR__MASK        0x0000000F
#define MEMC_TCKESR__T_CKESR__INV_MASK    0xFFFFFFF0
#define MEMC_TCKESR__T_CKESR__HW_DEFAULT  0x4

/* TDPD */
/* t_dpd Timing Register */
#define MEMC_TDPD                 0x10800144

/* MEMC_TDPD.t_dpd - Minimum Deep Power Down time. Is in terms of us. When a MCMD.DPDE command occurs, TDPD time is waited before MCMD.start_cmd can be cleared. MCMD_cmd_add_del (if any) does not start until TDPD has completed. This ensures TDPD requirement for the memory is not violated. 
The actual time period defined is TDPD* TOGCNT1U * internal timers clock period. 
Only applies for mDDR and LPDDR2 as Deep Power Down (DPD) is only valid for these memory types. 
For mDDR, tDPD=0, while for LPDDR2, tDPD=500 us. 
For LPDDR2/LPDDR3, if 500 us is waited externally by system, then set tDPD=0. 
mDDR Value: 0 
LPDDR2 Legal Values: 0 or 500 
LPDDR3 Legal Values: 0 or 500 
DDR2 Legal Value: 0 
DDR3 Legal Values: 0 
<b>Default Value:</b> 0 */
#define MEMC_TDPD__T_DPD__SHIFT       0
#define MEMC_TDPD__T_DPD__WIDTH       10
#define MEMC_TDPD__T_DPD__MASK        0x000003FF
#define MEMC_TDPD__T_DPD__INV_MASK    0xFFFFFC00
#define MEMC_TDPD__T_DPD__HW_DEFAULT  0x0

/* TREFI_MEM_DDR3 */
/* DDR3 Memory tREFI Value Timing Register */
#define MEMC_TREFI_MEM_DDR3       0x10800148

/* MEMC_TREFI_MEM_DDR3.mem_trefi_dddr3 - DDR3 tREFI (7.8us/3.9us) time, in memory clock cycles. 
In DDR3 mode the value used to ensure that a maximum of 16 Refresh commands can be issued within 2 x tREFI, which is a DDR3 restriction. 
mDDR Value: 0 
LPDDR2 Values: 0 
LPDDR3 Values: 0 
DDR2 Value: 0 
<b>Default Value:</b> 0 */
#define MEMC_TREFI_MEM_DDR3__MEM_TREFI_DDDR3__SHIFT       0
#define MEMC_TREFI_MEM_DDR3__MEM_TREFI_DDDR3__WIDTH       15
#define MEMC_TREFI_MEM_DDR3__MEM_TREFI_DDDR3__MASK        0x00007FFF
#define MEMC_TREFI_MEM_DDR3__MEM_TREFI_DDDR3__INV_MASK    0xFFFF8000
#define MEMC_TREFI_MEM_DDR3__MEM_TREFI_DDDR3__HW_DEFAULT  0x0

/* DTUWACTL */
/* DTU Write Address Control */
#define MEMC_DTUWACTL             0x10800200

/* MEMC_DTUWACTL.dtu_wr_col - Write column where data is to be targetted */
#define MEMC_DTUWACTL__DTU_WR_COL__SHIFT       0
#define MEMC_DTUWACTL__DTU_WR_COL__WIDTH       10
#define MEMC_DTUWACTL__DTU_WR_COL__MASK        0x000003FF
#define MEMC_DTUWACTL__DTU_WR_COL__INV_MASK    0xFFFFFC00
#define MEMC_DTUWACTL__DTU_WR_COL__HW_DEFAULT  0x0

/* MEMC_DTUWACTL.dtu_wr_bank - Write bank where data is to be targetted */
#define MEMC_DTUWACTL__DTU_WR_BANK__SHIFT       10
#define MEMC_DTUWACTL__DTU_WR_BANK__WIDTH       3
#define MEMC_DTUWACTL__DTU_WR_BANK__MASK        0x00001C00
#define MEMC_DTUWACTL__DTU_WR_BANK__INV_MASK    0xFFFFE3FF
#define MEMC_DTUWACTL__DTU_WR_BANK__HW_DEFAULT  0x0

/* MEMC_DTUWACTL.dtu_wr_row - Write row where data is to be targetted */
#define MEMC_DTUWACTL__DTU_WR_ROW__SHIFT       13
#define MEMC_DTUWACTL__DTU_WR_ROW__WIDTH       16
#define MEMC_DTUWACTL__DTU_WR_ROW__MASK        0x1FFFE000
#define MEMC_DTUWACTL__DTU_WR_ROW__INV_MASK    0xE0001FFF
#define MEMC_DTUWACTL__DTU_WR_ROW__HW_DEFAULT  0x0

/* MEMC_DTUWACTL.dtu_wr_rank - Write rank where data is to be targetted */
#define MEMC_DTUWACTL__DTU_WR_RANK__SHIFT       30
#define MEMC_DTUWACTL__DTU_WR_RANK__WIDTH       2
#define MEMC_DTUWACTL__DTU_WR_RANK__MASK        0xC0000000
#define MEMC_DTUWACTL__DTU_WR_RANK__INV_MASK    0x3FFFFFFF
#define MEMC_DTUWACTL__DTU_WR_RANK__HW_DEFAULT  0x0

/* DTURACTL */
/* DTU Read Address Control */
#define MEMC_DTURACTL             0x10800204

/* MEMC_DTURACTL.dtu_rd_col - Read column from where data comes */
#define MEMC_DTURACTL__DTU_RD_COL__SHIFT       0
#define MEMC_DTURACTL__DTU_RD_COL__WIDTH       10
#define MEMC_DTURACTL__DTU_RD_COL__MASK        0x000003FF
#define MEMC_DTURACTL__DTU_RD_COL__INV_MASK    0xFFFFFC00
#define MEMC_DTURACTL__DTU_RD_COL__HW_DEFAULT  0x0

/* MEMC_DTURACTL.dtu_rd_bank - Read bank from where data comes */
#define MEMC_DTURACTL__DTU_RD_BANK__SHIFT       10
#define MEMC_DTURACTL__DTU_RD_BANK__WIDTH       3
#define MEMC_DTURACTL__DTU_RD_BANK__MASK        0x00001C00
#define MEMC_DTURACTL__DTU_RD_BANK__INV_MASK    0xFFFFE3FF
#define MEMC_DTURACTL__DTU_RD_BANK__HW_DEFAULT  0x0

/* MEMC_DTURACTL.dtu_rd_row - Read row from where data comes */
#define MEMC_DTURACTL__DTU_RD_ROW__SHIFT       13
#define MEMC_DTURACTL__DTU_RD_ROW__WIDTH       16
#define MEMC_DTURACTL__DTU_RD_ROW__MASK        0x1FFFE000
#define MEMC_DTURACTL__DTU_RD_ROW__INV_MASK    0xE0001FFF
#define MEMC_DTURACTL__DTU_RD_ROW__HW_DEFAULT  0x0

/* MEMC_DTURACTL.dtu_rd_rank - Read rank from where data comes */
#define MEMC_DTURACTL__DTU_RD_RANK__SHIFT       30
#define MEMC_DTURACTL__DTU_RD_RANK__WIDTH       2
#define MEMC_DTURACTL__DTU_RD_RANK__MASK        0xC0000000
#define MEMC_DTURACTL__DTU_RD_RANK__INV_MASK    0x3FFFFFFF
#define MEMC_DTURACTL__DTU_RD_RANK__HW_DEFAULT  0x0

/* DTUCFG */
/* DTU Configuration Control */
#define MEMC_DTUCFG               0x10800208

/* MEMC_DTUCFG.dtu_enable - When set allows the DTU module to take ownership of the NIF interface 
1: DTU enabled 
0: DTU disabled */
#define MEMC_DTUCFG__DTU_ENABLE__SHIFT       0
#define MEMC_DTUCFG__DTU_ENABLE__WIDTH       1
#define MEMC_DTUCFG__DTU_ENABLE__MASK        0x00000001
#define MEMC_DTUCFG__DTU_ENABLE__INV_MASK    0xFFFFFFFE
#define MEMC_DTUCFG__DTU_ENABLE__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_nalen - Length of the NIF transfer sequence that is passed through the PCTL for each created address */
#define MEMC_DTUCFG__DTU_NALEN__SHIFT       1
#define MEMC_DTUCFG__DTU_NALEN__WIDTH       6
#define MEMC_DTUCFG__DTU_NALEN__MASK        0x0000007E
#define MEMC_DTUCFG__DTU_NALEN__INV_MASK    0xFFFFFF81
#define MEMC_DTUCFG__DTU_NALEN__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_incr_cols - Increment the column address until we saturate. Return to zero if DTUCFG.dtu_incr_banks is set to 1 and we are not at bank 7. */
#define MEMC_DTUCFG__DTU_INCR_COLS__SHIFT       7
#define MEMC_DTUCFG__DTU_INCR_COLS__WIDTH       1
#define MEMC_DTUCFG__DTU_INCR_COLS__MASK        0x00000080
#define MEMC_DTUCFG__DTU_INCR_COLS__INV_MASK    0xFFFFFF7F
#define MEMC_DTUCFG__DTU_INCR_COLS__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_incr_banks - When the column address rolls over increment the bank address until we reach and conclude bank 7. */
#define MEMC_DTUCFG__DTU_INCR_BANKS__SHIFT       8
#define MEMC_DTUCFG__DTU_INCR_BANKS__WIDTH       1
#define MEMC_DTUCFG__DTU_INCR_BANKS__MASK        0x00000100
#define MEMC_DTUCFG__DTU_INCR_BANKS__INV_MASK    0xFFFFFEFF
#define MEMC_DTUCFG__DTU_INCR_BANKS__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_generate_random - Generate transfers using random data, otherwise generate transfers from the programmable write data buffers. */
#define MEMC_DTUCFG__DTU_GENERATE_RANDOM__SHIFT       9
#define MEMC_DTUCFG__DTU_GENERATE_RANDOM__WIDTH       1
#define MEMC_DTUCFG__DTU_GENERATE_RANDOM__MASK        0x00000200
#define MEMC_DTUCFG__DTU_GENERATE_RANDOM__INV_MASK    0xFFFFFDFF
#define MEMC_DTUCFG__DTU_GENERATE_RANDOM__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_target_lane - Selects one of the byte lanes for data comparison into the programmable read data buffer */
#define MEMC_DTUCFG__DTU_TARGET_LANE__SHIFT       10
#define MEMC_DTUCFG__DTU_TARGET_LANE__WIDTH       4
#define MEMC_DTUCFG__DTU_TARGET_LANE__MASK        0x00003C00
#define MEMC_DTUCFG__DTU_TARGET_LANE__INV_MASK    0xFFFFC3FF
#define MEMC_DTUCFG__DTU_TARGET_LANE__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_data_mask_en - Controls whether random generated data masks are transmitted. Unless enabled all data bytes are written to memory and expected to be read from memory. */
#define MEMC_DTUCFG__DTU_DATA_MASK_EN__SHIFT       14
#define MEMC_DTUCFG__DTU_DATA_MASK_EN__WIDTH       1
#define MEMC_DTUCFG__DTU_DATA_MASK_EN__MASK        0x00004000
#define MEMC_DTUCFG__DTU_DATA_MASK_EN__INV_MASK    0xFFFFBFFF
#define MEMC_DTUCFG__DTU_DATA_MASK_EN__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_wr_multi_rd - When set, puts the DTU in write once multiple reads mode */
#define MEMC_DTUCFG__DTU_WR_MULTI_RD__SHIFT       15
#define MEMC_DTUCFG__DTU_WR_MULTI_RD__WIDTH       1
#define MEMC_DTUCFG__DTU_WR_MULTI_RD__MASK        0x00008000
#define MEMC_DTUCFG__DTU_WR_MULTI_RD__INV_MASK    0xFFFF7FFF
#define MEMC_DTUCFG__DTU_WR_MULTI_RD__HW_DEFAULT  0x0

/* MEMC_DTUCFG.dtu_row_increments - Number of times to increment the row address when generating random data, up to a maximum of 127 times. */
#define MEMC_DTUCFG__DTU_ROW_INCREMENTS__SHIFT       16
#define MEMC_DTUCFG__DTU_ROW_INCREMENTS__WIDTH       7
#define MEMC_DTUCFG__DTU_ROW_INCREMENTS__MASK        0x007F0000
#define MEMC_DTUCFG__DTU_ROW_INCREMENTS__INV_MASK    0xFF80FFFF
#define MEMC_DTUCFG__DTU_ROW_INCREMENTS__HW_DEFAULT  0x0

/* DTUECTL */
/* DTU Execute Control */
#define MEMC_DTUECTL              0x1080020C

/* MEMC_DTUECTL.run_dtu - When set, initiates the running of the DTU read and write transfer. This bit automatically clears when the transfers are completed. */
#define MEMC_DTUECTL__RUN_DTU__SHIFT       0
#define MEMC_DTUECTL__RUN_DTU__WIDTH       1
#define MEMC_DTUECTL__RUN_DTU__MASK        0x00000001
#define MEMC_DTUECTL__RUN_DTU__INV_MASK    0xFFFFFFFE
#define MEMC_DTUECTL__RUN_DTU__HW_DEFAULT  0x0

/* MEMC_DTUECTL.run_err_reports - When set, initiates the calculation of the error status bits. This bit automatically clears when the re-calculation is done. This is only used in debug mode to verify the comparison logic. */
#define MEMC_DTUECTL__RUN_ERR_REPORTS__SHIFT       1
#define MEMC_DTUECTL__RUN_ERR_REPORTS__WIDTH       1
#define MEMC_DTUECTL__RUN_ERR_REPORTS__MASK        0x00000002
#define MEMC_DTUECTL__RUN_ERR_REPORTS__INV_MASK    0xFFFFFFFD
#define MEMC_DTUECTL__RUN_ERR_REPORTS__HW_DEFAULT  0x0

/* MEMC_DTUECTL.wr_multi_rd_rst - When set, resets the DTU in write once multiple reads mode, to allow a new write to be performed. This bit automatically clears. */
#define MEMC_DTUECTL__WR_MULTI_RD_RST__SHIFT       2
#define MEMC_DTUECTL__WR_MULTI_RD_RST__WIDTH       1
#define MEMC_DTUECTL__WR_MULTI_RD_RST__MASK        0x00000004
#define MEMC_DTUECTL__WR_MULTI_RD_RST__INV_MASK    0xFFFFFFFB
#define MEMC_DTUECTL__WR_MULTI_RD_RST__HW_DEFAULT  0x0

/* DTUWD0 */
/* DTU Write Data 0 */
#define MEMC_DTUWD0               0x10800210

/* MEMC_DTUWD0.dtu_wr_byte0 - Write data byte 0 */
#define MEMC_DTUWD0__DTU_WR_BYTE0__SHIFT       0
#define MEMC_DTUWD0__DTU_WR_BYTE0__WIDTH       8
#define MEMC_DTUWD0__DTU_WR_BYTE0__MASK        0x000000FF
#define MEMC_DTUWD0__DTU_WR_BYTE0__INV_MASK    0xFFFFFF00
#define MEMC_DTUWD0__DTU_WR_BYTE0__HW_DEFAULT  0x0

/* MEMC_DTUWD0.dtu_wr_byte1 - Write data byte 1 */
#define MEMC_DTUWD0__DTU_WR_BYTE1__SHIFT       8
#define MEMC_DTUWD0__DTU_WR_BYTE1__WIDTH       8
#define MEMC_DTUWD0__DTU_WR_BYTE1__MASK        0x0000FF00
#define MEMC_DTUWD0__DTU_WR_BYTE1__INV_MASK    0xFFFF00FF
#define MEMC_DTUWD0__DTU_WR_BYTE1__HW_DEFAULT  0x0

/* MEMC_DTUWD0.dtu_wr_byte2 - Write data byte 2 */
#define MEMC_DTUWD0__DTU_WR_BYTE2__SHIFT       16
#define MEMC_DTUWD0__DTU_WR_BYTE2__WIDTH       8
#define MEMC_DTUWD0__DTU_WR_BYTE2__MASK        0x00FF0000
#define MEMC_DTUWD0__DTU_WR_BYTE2__INV_MASK    0xFF00FFFF
#define MEMC_DTUWD0__DTU_WR_BYTE2__HW_DEFAULT  0x0

/* MEMC_DTUWD0.dtu_wr_byte3 - Write data byte 3 */
#define MEMC_DTUWD0__DTU_WR_BYTE3__SHIFT       24
#define MEMC_DTUWD0__DTU_WR_BYTE3__WIDTH       8
#define MEMC_DTUWD0__DTU_WR_BYTE3__MASK        0xFF000000
#define MEMC_DTUWD0__DTU_WR_BYTE3__INV_MASK    0x00FFFFFF
#define MEMC_DTUWD0__DTU_WR_BYTE3__HW_DEFAULT  0x0

/* DTUWD1 */
/* DTU Write Data 1 */
#define MEMC_DTUWD1               0x10800214

/* MEMC_DTUWD1.dtu_wr_byte4 - Write data byte 4 */
#define MEMC_DTUWD1__DTU_WR_BYTE4__SHIFT       0
#define MEMC_DTUWD1__DTU_WR_BYTE4__WIDTH       8
#define MEMC_DTUWD1__DTU_WR_BYTE4__MASK        0x000000FF
#define MEMC_DTUWD1__DTU_WR_BYTE4__INV_MASK    0xFFFFFF00
#define MEMC_DTUWD1__DTU_WR_BYTE4__HW_DEFAULT  0x0

/* MEMC_DTUWD1.dtu_wr_byte5 - Write data byte 5 */
#define MEMC_DTUWD1__DTU_WR_BYTE5__SHIFT       8
#define MEMC_DTUWD1__DTU_WR_BYTE5__WIDTH       8
#define MEMC_DTUWD1__DTU_WR_BYTE5__MASK        0x0000FF00
#define MEMC_DTUWD1__DTU_WR_BYTE5__INV_MASK    0xFFFF00FF
#define MEMC_DTUWD1__DTU_WR_BYTE5__HW_DEFAULT  0x0

/* MEMC_DTUWD1.dtu_wr_byte6 - Write data byte 6 */
#define MEMC_DTUWD1__DTU_WR_BYTE6__SHIFT       16
#define MEMC_DTUWD1__DTU_WR_BYTE6__WIDTH       8
#define MEMC_DTUWD1__DTU_WR_BYTE6__MASK        0x00FF0000
#define MEMC_DTUWD1__DTU_WR_BYTE6__INV_MASK    0xFF00FFFF
#define MEMC_DTUWD1__DTU_WR_BYTE6__HW_DEFAULT  0x0

/* MEMC_DTUWD1.dtu_wr_byte7 - Write data byte 7 */
#define MEMC_DTUWD1__DTU_WR_BYTE7__SHIFT       24
#define MEMC_DTUWD1__DTU_WR_BYTE7__WIDTH       8
#define MEMC_DTUWD1__DTU_WR_BYTE7__MASK        0xFF000000
#define MEMC_DTUWD1__DTU_WR_BYTE7__INV_MASK    0x00FFFFFF
#define MEMC_DTUWD1__DTU_WR_BYTE7__HW_DEFAULT  0x0

/* DTUWD2 */
/* DTU Write Data 2 */
#define MEMC_DTUWD2               0x10800218

/* MEMC_DTUWD2.dtu_wr_byte8 - Write data byte 8 */
#define MEMC_DTUWD2__DTU_WR_BYTE8__SHIFT       0
#define MEMC_DTUWD2__DTU_WR_BYTE8__WIDTH       8
#define MEMC_DTUWD2__DTU_WR_BYTE8__MASK        0x000000FF
#define MEMC_DTUWD2__DTU_WR_BYTE8__INV_MASK    0xFFFFFF00
#define MEMC_DTUWD2__DTU_WR_BYTE8__HW_DEFAULT  0x0

/* MEMC_DTUWD2.dtu_wr_byte9 - Write data byte 9 */
#define MEMC_DTUWD2__DTU_WR_BYTE9__SHIFT       8
#define MEMC_DTUWD2__DTU_WR_BYTE9__WIDTH       8
#define MEMC_DTUWD2__DTU_WR_BYTE9__MASK        0x0000FF00
#define MEMC_DTUWD2__DTU_WR_BYTE9__INV_MASK    0xFFFF00FF
#define MEMC_DTUWD2__DTU_WR_BYTE9__HW_DEFAULT  0x0

/* MEMC_DTUWD2.dtu_wr_byte10 - Write data byte 10 */
#define MEMC_DTUWD2__DTU_WR_BYTE10__SHIFT       16
#define MEMC_DTUWD2__DTU_WR_BYTE10__WIDTH       8
#define MEMC_DTUWD2__DTU_WR_BYTE10__MASK        0x00FF0000
#define MEMC_DTUWD2__DTU_WR_BYTE10__INV_MASK    0xFF00FFFF
#define MEMC_DTUWD2__DTU_WR_BYTE10__HW_DEFAULT  0x0

/* MEMC_DTUWD2.dtu_wr_byte11 - Write data byte 11 */
#define MEMC_DTUWD2__DTU_WR_BYTE11__SHIFT       24
#define MEMC_DTUWD2__DTU_WR_BYTE11__WIDTH       8
#define MEMC_DTUWD2__DTU_WR_BYTE11__MASK        0xFF000000
#define MEMC_DTUWD2__DTU_WR_BYTE11__INV_MASK    0x00FFFFFF
#define MEMC_DTUWD2__DTU_WR_BYTE11__HW_DEFAULT  0x0

/* DTUWD3 */
/* DTU Write Data 3 */
#define MEMC_DTUWD3               0x1080021C

/* MEMC_DTUWD3.dtu_wr_byte12 - Write data byte 12 */
#define MEMC_DTUWD3__DTU_WR_BYTE12__SHIFT       0
#define MEMC_DTUWD3__DTU_WR_BYTE12__WIDTH       8
#define MEMC_DTUWD3__DTU_WR_BYTE12__MASK        0x000000FF
#define MEMC_DTUWD3__DTU_WR_BYTE12__INV_MASK    0xFFFFFF00
#define MEMC_DTUWD3__DTU_WR_BYTE12__HW_DEFAULT  0x0

/* MEMC_DTUWD3.dtu_wr_byte13 - Write data byte 13 */
#define MEMC_DTUWD3__DTU_WR_BYTE13__SHIFT       8
#define MEMC_DTUWD3__DTU_WR_BYTE13__WIDTH       8
#define MEMC_DTUWD3__DTU_WR_BYTE13__MASK        0x0000FF00
#define MEMC_DTUWD3__DTU_WR_BYTE13__INV_MASK    0xFFFF00FF
#define MEMC_DTUWD3__DTU_WR_BYTE13__HW_DEFAULT  0x0

/* MEMC_DTUWD3.dtu_wr_byte14 - Write data byte 14 */
#define MEMC_DTUWD3__DTU_WR_BYTE14__SHIFT       16
#define MEMC_DTUWD3__DTU_WR_BYTE14__WIDTH       8
#define MEMC_DTUWD3__DTU_WR_BYTE14__MASK        0x00FF0000
#define MEMC_DTUWD3__DTU_WR_BYTE14__INV_MASK    0xFF00FFFF
#define MEMC_DTUWD3__DTU_WR_BYTE14__HW_DEFAULT  0x0

/* MEMC_DTUWD3.dtu_wr_byte15 - Write data byte 15 */
#define MEMC_DTUWD3__DTU_WR_BYTE15__SHIFT       24
#define MEMC_DTUWD3__DTU_WR_BYTE15__WIDTH       8
#define MEMC_DTUWD3__DTU_WR_BYTE15__MASK        0xFF000000
#define MEMC_DTUWD3__DTU_WR_BYTE15__INV_MASK    0x00FFFFFF
#define MEMC_DTUWD3__DTU_WR_BYTE15__HW_DEFAULT  0x0

/* DTUWDM */
/* DTU Write Data mask */
#define MEMC_DTUWDM               0x10800220

/* MEMC_DTUWDM.dm_wr_byte - Write data mask bit, one bit for each byte. Each bit should be 0 for a byte lane that contains valid write data. */
#define MEMC_DTUWDM__DM_WR_BYTE__SHIFT       0
#define MEMC_DTUWDM__DM_WR_BYTE__WIDTH       16
#define MEMC_DTUWDM__DM_WR_BYTE__MASK        0x0000FFFF
#define MEMC_DTUWDM__DM_WR_BYTE__INV_MASK    0xFFFF0000
#define MEMC_DTUWDM__DM_WR_BYTE__HW_DEFAULT  0x0

/* DTURD0 */
/* DTU Read Data 0 */
#define MEMC_DTURD0               0x10800224

/* MEMC_DTURD0.dtu_rd_byte0 - Read data byte 0 */
#define MEMC_DTURD0__DTU_RD_BYTE0__SHIFT       0
#define MEMC_DTURD0__DTU_RD_BYTE0__WIDTH       8
#define MEMC_DTURD0__DTU_RD_BYTE0__MASK        0x000000FF
#define MEMC_DTURD0__DTU_RD_BYTE0__INV_MASK    0xFFFFFF00
#define MEMC_DTURD0__DTU_RD_BYTE0__HW_DEFAULT  0x0

/* MEMC_DTURD0.dtu_rd_byte1 - Read data byte 1 */
#define MEMC_DTURD0__DTU_RD_BYTE1__SHIFT       8
#define MEMC_DTURD0__DTU_RD_BYTE1__WIDTH       8
#define MEMC_DTURD0__DTU_RD_BYTE1__MASK        0x0000FF00
#define MEMC_DTURD0__DTU_RD_BYTE1__INV_MASK    0xFFFF00FF
#define MEMC_DTURD0__DTU_RD_BYTE1__HW_DEFAULT  0x0

/* MEMC_DTURD0.dtu_rd_byte2 - Read data byte 2 */
#define MEMC_DTURD0__DTU_RD_BYTE2__SHIFT       16
#define MEMC_DTURD0__DTU_RD_BYTE2__WIDTH       8
#define MEMC_DTURD0__DTU_RD_BYTE2__MASK        0x00FF0000
#define MEMC_DTURD0__DTU_RD_BYTE2__INV_MASK    0xFF00FFFF
#define MEMC_DTURD0__DTU_RD_BYTE2__HW_DEFAULT  0x0

/* MEMC_DTURD0.dtu_rd_byte3 - Read data byte 3 */
#define MEMC_DTURD0__DTU_RD_BYTE3__SHIFT       24
#define MEMC_DTURD0__DTU_RD_BYTE3__WIDTH       8
#define MEMC_DTURD0__DTU_RD_BYTE3__MASK        0xFF000000
#define MEMC_DTURD0__DTU_RD_BYTE3__INV_MASK    0x00FFFFFF
#define MEMC_DTURD0__DTU_RD_BYTE3__HW_DEFAULT  0x0

/* DTURD1 */
/* DTU Read Data 1 */
#define MEMC_DTURD1               0x10800228

/* MEMC_DTURD1.dtu_rd_byte4 - Read data byte 4 */
#define MEMC_DTURD1__DTU_RD_BYTE4__SHIFT       0
#define MEMC_DTURD1__DTU_RD_BYTE4__WIDTH       8
#define MEMC_DTURD1__DTU_RD_BYTE4__MASK        0x000000FF
#define MEMC_DTURD1__DTU_RD_BYTE4__INV_MASK    0xFFFFFF00
#define MEMC_DTURD1__DTU_RD_BYTE4__HW_DEFAULT  0x0

/* MEMC_DTURD1.dtu_rd_byte5 - Read data byte 5 */
#define MEMC_DTURD1__DTU_RD_BYTE5__SHIFT       8
#define MEMC_DTURD1__DTU_RD_BYTE5__WIDTH       8
#define MEMC_DTURD1__DTU_RD_BYTE5__MASK        0x0000FF00
#define MEMC_DTURD1__DTU_RD_BYTE5__INV_MASK    0xFFFF00FF
#define MEMC_DTURD1__DTU_RD_BYTE5__HW_DEFAULT  0x0

/* MEMC_DTURD1.dtu_rd_byte6 - Read data byte 6 */
#define MEMC_DTURD1__DTU_RD_BYTE6__SHIFT       16
#define MEMC_DTURD1__DTU_RD_BYTE6__WIDTH       8
#define MEMC_DTURD1__DTU_RD_BYTE6__MASK        0x00FF0000
#define MEMC_DTURD1__DTU_RD_BYTE6__INV_MASK    0xFF00FFFF
#define MEMC_DTURD1__DTU_RD_BYTE6__HW_DEFAULT  0x0

/* MEMC_DTURD1.dtu_rd_byte7 - Read data byte 7 */
#define MEMC_DTURD1__DTU_RD_BYTE7__SHIFT       24
#define MEMC_DTURD1__DTU_RD_BYTE7__WIDTH       8
#define MEMC_DTURD1__DTU_RD_BYTE7__MASK        0xFF000000
#define MEMC_DTURD1__DTU_RD_BYTE7__INV_MASK    0x00FFFFFF
#define MEMC_DTURD1__DTU_RD_BYTE7__HW_DEFAULT  0x0

/* DTURD2 */
/* DTU Read Data 2 */
#define MEMC_DTURD2               0x1080022C

/* MEMC_DTURD2.dtu_rd_byte8 - Read data byte 8 */
#define MEMC_DTURD2__DTU_RD_BYTE8__SHIFT       0
#define MEMC_DTURD2__DTU_RD_BYTE8__WIDTH       8
#define MEMC_DTURD2__DTU_RD_BYTE8__MASK        0x000000FF
#define MEMC_DTURD2__DTU_RD_BYTE8__INV_MASK    0xFFFFFF00
#define MEMC_DTURD2__DTU_RD_BYTE8__HW_DEFAULT  0x0

/* MEMC_DTURD2.dtu_rd_byte9 - Read data byte 9 */
#define MEMC_DTURD2__DTU_RD_BYTE9__SHIFT       8
#define MEMC_DTURD2__DTU_RD_BYTE9__WIDTH       8
#define MEMC_DTURD2__DTU_RD_BYTE9__MASK        0x0000FF00
#define MEMC_DTURD2__DTU_RD_BYTE9__INV_MASK    0xFFFF00FF
#define MEMC_DTURD2__DTU_RD_BYTE9__HW_DEFAULT  0x0

/* MEMC_DTURD2.dtu_rd_byte10 - Read data byte 10 */
#define MEMC_DTURD2__DTU_RD_BYTE10__SHIFT       16
#define MEMC_DTURD2__DTU_RD_BYTE10__WIDTH       8
#define MEMC_DTURD2__DTU_RD_BYTE10__MASK        0x00FF0000
#define MEMC_DTURD2__DTU_RD_BYTE10__INV_MASK    0xFF00FFFF
#define MEMC_DTURD2__DTU_RD_BYTE10__HW_DEFAULT  0x0

/* MEMC_DTURD2.dtu_rd_byte11 - Read data byte 11 */
#define MEMC_DTURD2__DTU_RD_BYTE11__SHIFT       24
#define MEMC_DTURD2__DTU_RD_BYTE11__WIDTH       8
#define MEMC_DTURD2__DTU_RD_BYTE11__MASK        0xFF000000
#define MEMC_DTURD2__DTU_RD_BYTE11__INV_MASK    0x00FFFFFF
#define MEMC_DTURD2__DTU_RD_BYTE11__HW_DEFAULT  0x0

/* DTURD3 */
/* DTU Read Data 3 */
#define MEMC_DTURD3               0x10800230

/* MEMC_DTURD3.dtu_rd_byte12 - Read data byte 12 */
#define MEMC_DTURD3__DTU_RD_BYTE12__SHIFT       0
#define MEMC_DTURD3__DTU_RD_BYTE12__WIDTH       8
#define MEMC_DTURD3__DTU_RD_BYTE12__MASK        0x000000FF
#define MEMC_DTURD3__DTU_RD_BYTE12__INV_MASK    0xFFFFFF00
#define MEMC_DTURD3__DTU_RD_BYTE12__HW_DEFAULT  0x0

/* MEMC_DTURD3.dtu_rd_byte13 - Read data byte 13 */
#define MEMC_DTURD3__DTU_RD_BYTE13__SHIFT       8
#define MEMC_DTURD3__DTU_RD_BYTE13__WIDTH       8
#define MEMC_DTURD3__DTU_RD_BYTE13__MASK        0x0000FF00
#define MEMC_DTURD3__DTU_RD_BYTE13__INV_MASK    0xFFFF00FF
#define MEMC_DTURD3__DTU_RD_BYTE13__HW_DEFAULT  0x0

/* MEMC_DTURD3.dtu_rd_byte14 - Read data byte 14 */
#define MEMC_DTURD3__DTU_RD_BYTE14__SHIFT       16
#define MEMC_DTURD3__DTU_RD_BYTE14__WIDTH       8
#define MEMC_DTURD3__DTU_RD_BYTE14__MASK        0x00FF0000
#define MEMC_DTURD3__DTU_RD_BYTE14__INV_MASK    0xFF00FFFF
#define MEMC_DTURD3__DTU_RD_BYTE14__HW_DEFAULT  0x0

/* MEMC_DTURD3.dtu_rd_byte15 - Read data byte 15 */
#define MEMC_DTURD3__DTU_RD_BYTE15__SHIFT       24
#define MEMC_DTURD3__DTU_RD_BYTE15__WIDTH       8
#define MEMC_DTURD3__DTU_RD_BYTE15__MASK        0xFF000000
#define MEMC_DTURD3__DTU_RD_BYTE15__INV_MASK    0x00FFFFFF
#define MEMC_DTURD3__DTU_RD_BYTE15__HW_DEFAULT  0x0

/* DTULFSRWD */
/* DTU LFSR Seed for Write Data Generation */
#define MEMC_DTULFSRWD            0x10800234

/* MEMC_DTULFSRWD.dtu_lfsr_wseed - This is the initial seed for the random write data generation LFSR (linear feedback shift register), shared with the write mask generation. */
#define MEMC_DTULFSRWD__DTU_LFSR_WSEED__SHIFT       0
#define MEMC_DTULFSRWD__DTU_LFSR_WSEED__WIDTH       32
#define MEMC_DTULFSRWD__DTU_LFSR_WSEED__MASK        0xFFFFFFFF
#define MEMC_DTULFSRWD__DTU_LFSR_WSEED__INV_MASK    0x00000000
#define MEMC_DTULFSRWD__DTU_LFSR_WSEED__HW_DEFAULT  0x0

/* DTULFSRRD */
/* DTU LFSR Seed for Read Data Generation */
#define MEMC_DTULFSRRD            0x10800238

/* MEMC_DTULFSRRD.dtu_lfsr_rseed - This is the initial seed for the random read data generation LFSR (linear feedback shift register), this is shared with the read mask generation. The read data mask is reconstructed the same as the write data mask was created, allowing the �on the fly comparison� ignore bytes which were not written. */
#define MEMC_DTULFSRRD__DTU_LFSR_RSEED__SHIFT       0
#define MEMC_DTULFSRRD__DTU_LFSR_RSEED__WIDTH       32
#define MEMC_DTULFSRRD__DTU_LFSR_RSEED__MASK        0xFFFFFFFF
#define MEMC_DTULFSRRD__DTU_LFSR_RSEED__INV_MASK    0x00000000
#define MEMC_DTULFSRRD__DTU_LFSR_RSEED__HW_DEFAULT  0x0

/* DTUEAF */
/* DTU Error address FIFO */
#define MEMC_DTUEAF               0x1080023C

/* MEMC_DTUEAF.ea_column - Indicates the column address that the error occurred in during random data generation. There could be a number of entries in this FIFO. If FIFO is empty one reads zeroes. */
#define MEMC_DTUEAF__EA_COLUMN__SHIFT       0
#define MEMC_DTUEAF__EA_COLUMN__WIDTH       10
#define MEMC_DTUEAF__EA_COLUMN__MASK        0x000003FF
#define MEMC_DTUEAF__EA_COLUMN__INV_MASK    0xFFFFFC00
#define MEMC_DTUEAF__EA_COLUMN__HW_DEFAULT  0x0

/* MEMC_DTUEAF.ea_bank - Indicates the bank that the error occurred in during random data generation. There could be a number of entries in this FIFO. If FIFO is empty one reads zeroes. */
#define MEMC_DTUEAF__EA_BANK__SHIFT       10
#define MEMC_DTUEAF__EA_BANK__WIDTH       3
#define MEMC_DTUEAF__EA_BANK__MASK        0x00001C00
#define MEMC_DTUEAF__EA_BANK__INV_MASK    0xFFFFE3FF
#define MEMC_DTUEAF__EA_BANK__HW_DEFAULT  0x0

/* MEMC_DTUEAF.ea_row - Indicates the row that the error occurred in during random data generation. There could be a number of entries in this FIFO. If FIFO is empty one reads zeroes. */
#define MEMC_DTUEAF__EA_ROW__SHIFT       13
#define MEMC_DTUEAF__EA_ROW__WIDTH       16
#define MEMC_DTUEAF__EA_ROW__MASK        0x1FFFE000
#define MEMC_DTUEAF__EA_ROW__INV_MASK    0xE0001FFF
#define MEMC_DTUEAF__EA_ROW__HW_DEFAULT  0x0

/* MEMC_DTUEAF.ea_rank - Indicates the rank that the error occurred in during random data generation. There could be a number of entries in this FIFO. If FIFO is empty one reads zeroes. */
#define MEMC_DTUEAF__EA_RANK__SHIFT       30
#define MEMC_DTUEAF__EA_RANK__WIDTH       2
#define MEMC_DTUEAF__EA_RANK__MASK        0xC0000000
#define MEMC_DTUEAF__EA_RANK__INV_MASK    0x3FFFFFFF
#define MEMC_DTUEAF__EA_RANK__HW_DEFAULT  0x0

/* DFITCTRLDELAY */
/* DFI tctrl_delay Register */
#define MEMC_DFITCTRLDELAY        0x10800240

/* MEMC_DFITCTRLDELAY.tctrl_delay - Specifies the number of DFI clock cycles after an assertion or deassertion of the DFI control signals that the control signals at the PHY-DRAM interface reflect the assertion or de-assertion. If the DFI clock and the memory clock are not phase-aligned, this timing parameter should be rounded up to the next integer value. 
<b>Default Value:</b> 2 */
#define MEMC_DFITCTRLDELAY__TCTRL_DELAY__SHIFT       0
#define MEMC_DFITCTRLDELAY__TCTRL_DELAY__WIDTH       4
#define MEMC_DFITCTRLDELAY__TCTRL_DELAY__MASK        0x0000000F
#define MEMC_DFITCTRLDELAY__TCTRL_DELAY__INV_MASK    0xFFFFFFF0
#define MEMC_DFITCTRLDELAY__TCTRL_DELAY__HW_DEFAULT  0x2

/* DFIODTCFG */
/* DFI ODT Configuration */
#define MEMC_DFIODTCFG            0x10800244

/* MEMC_DFIODTCFG.rank0_odt_read_nsel - Enable/disable ODT for rank 0 when a read access is occurring on a different rank 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFIODTCFG__RANK0_ODT_READ_NSEL__SHIFT       0
#define MEMC_DFIODTCFG__RANK0_ODT_READ_NSEL__WIDTH       1
#define MEMC_DFIODTCFG__RANK0_ODT_READ_NSEL__MASK        0x00000001
#define MEMC_DFIODTCFG__RANK0_ODT_READ_NSEL__INV_MASK    0xFFFFFFFE
#define MEMC_DFIODTCFG__RANK0_ODT_READ_NSEL__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG.rank0_odt_read_sel - Enable/disable ODT for rank 0 when a read access is occurring on this rank 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFIODTCFG__RANK0_ODT_READ_SEL__SHIFT       1
#define MEMC_DFIODTCFG__RANK0_ODT_READ_SEL__WIDTH       1
#define MEMC_DFIODTCFG__RANK0_ODT_READ_SEL__MASK        0x00000002
#define MEMC_DFIODTCFG__RANK0_ODT_READ_SEL__INV_MASK    0xFFFFFFFD
#define MEMC_DFIODTCFG__RANK0_ODT_READ_SEL__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG.rank0_odt_write_nsel - Enable/disable ODT for rank 0 when a write access is occurring on a different rank 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_NSEL__SHIFT       2
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_NSEL__WIDTH       1
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_NSEL__MASK        0x00000004
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_NSEL__INV_MASK    0xFFFFFFFB
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_NSEL__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG.rank0_odt_write_sel - Enable/disable ODT for rank 0 when a write access is occurring on this rank 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_SEL__SHIFT       3
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_SEL__WIDTH       1
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_SEL__MASK        0x00000008
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_SEL__INV_MASK    0xFFFFFFF7
#define MEMC_DFIODTCFG__RANK0_ODT_WRITE_SEL__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG.rank0_odt_default - Default ODT value of rank 0 when there is no read/write activity 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFIODTCFG__RANK0_ODT_DEFAULT__SHIFT       4
#define MEMC_DFIODTCFG__RANK0_ODT_DEFAULT__WIDTH       1
#define MEMC_DFIODTCFG__RANK0_ODT_DEFAULT__MASK        0x00000010
#define MEMC_DFIODTCFG__RANK0_ODT_DEFAULT__INV_MASK    0xFFFFFFEF
#define MEMC_DFIODTCFG__RANK0_ODT_DEFAULT__HW_DEFAULT  0x0

/* DFIODTCFG1 */
/* DFI ODT Timing Configuration 1: for Latency and Length */
#define MEMC_DFIODTCFG1           0x10800248

/* MEMC_DFIODTCFG1.odt_lat_w - ODT latency for writes Latency after a write command that dfi_odt is set. This is in terms of SDR cycles 
<b>Default Value:</b> 0 */
#define MEMC_DFIODTCFG1__ODT_LAT_W__SHIFT       0
#define MEMC_DFIODTCFG1__ODT_LAT_W__WIDTH       5
#define MEMC_DFIODTCFG1__ODT_LAT_W__MASK        0x0000001F
#define MEMC_DFIODTCFG1__ODT_LAT_W__INV_MASK    0xFFFFFFE0
#define MEMC_DFIODTCFG1__ODT_LAT_W__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG1.odt_lat_r - ODT latency for reads Latency after a read command that dfi_odt is set. This is in terms of SDR cycles. 
<b>Default Value:</b> 0 */
#define MEMC_DFIODTCFG1__ODT_LAT_R__SHIFT       8
#define MEMC_DFIODTCFG1__ODT_LAT_R__WIDTH       5
#define MEMC_DFIODTCFG1__ODT_LAT_R__MASK        0x00001F00
#define MEMC_DFIODTCFG1__ODT_LAT_R__INV_MASK    0xFFFFE0FF
#define MEMC_DFIODTCFG1__ODT_LAT_R__HW_DEFAULT  0x0

/* MEMC_DFIODTCFG1.odt_len_bl8_w - ODT length for BL8 write transfers Length of dfi_odt signal for BL8 writes. This is in terms of SDR cycles. For BL4 writes, the length of dfi_odt is always 2 cycles shorter than the value in this register field. 
<b>Default Value:</b> 6 */
#define MEMC_DFIODTCFG1__ODT_LEN_BL8_W__SHIFT       16
#define MEMC_DFIODTCFG1__ODT_LEN_BL8_W__WIDTH       4
#define MEMC_DFIODTCFG1__ODT_LEN_BL8_W__MASK        0x000F0000
#define MEMC_DFIODTCFG1__ODT_LEN_BL8_W__INV_MASK    0xFFF0FFFF
#define MEMC_DFIODTCFG1__ODT_LEN_BL8_W__HW_DEFAULT  0x6

/* MEMC_DFIODTCFG1.odt_len_b8_r - ODT latency for BL8 read transfers Length of dfi_odt signal for BL8 reads. This is in terms of SDR cycles. For BL4 reads, the length of dfi_odt is always 2 cycles shorter than the value in this register field. 
<b>Default Value:</b> 6 */
#define MEMC_DFIODTCFG1__ODT_LEN_B8_R__SHIFT       24
#define MEMC_DFIODTCFG1__ODT_LEN_B8_R__WIDTH       4
#define MEMC_DFIODTCFG1__ODT_LEN_B8_R__MASK        0x0F000000
#define MEMC_DFIODTCFG1__ODT_LEN_B8_R__INV_MASK    0xF0FFFFFF
#define MEMC_DFIODTCFG1__ODT_LEN_B8_R__HW_DEFAULT  0x6

/* DFIODTRANKMAP */
/* DFI ODT Rank Mapping */
#define MEMC_DFIODTRANKMAP        0x1080024C

/* MEMC_DFIODTRANKMAP.odt_rank_map0 - Rank mapping for dfi_odt[0] 
Determines which rank access(es) will cause dfi_odt[0] to be asserted 
Bit 3= 1: dfi_odt[0] will be asserted to terminate rank 3 
Bit 2= 1: dfi_odt[0] will be asserted to terminate rank 2 
Bit 1= 1: dfi_odt[0] will be asserted to terminate rank 1 
Bit 0= 1: dfi_odt[0] will be asserted to terminate rank 0 
<b>Default Value:</b> 1 */
#define MEMC_DFIODTRANKMAP__ODT_RANK_MAP0__SHIFT       0
#define MEMC_DFIODTRANKMAP__ODT_RANK_MAP0__WIDTH       4
#define MEMC_DFIODTRANKMAP__ODT_RANK_MAP0__MASK        0x0000000F
#define MEMC_DFIODTRANKMAP__ODT_RANK_MAP0__INV_MASK    0xFFFFFFF0
#define MEMC_DFIODTRANKMAP__ODT_RANK_MAP0__HW_DEFAULT  0x1

/* DFITPHYWRDATA */
/* DFI tphy_wrdata Register */
#define MEMC_DFITPHYWRDATA        0x10800250

/* MEMC_DFITPHYWRDATA.tphy_wrdata - Specifies the number of DFI clock cycles between when the dfi_wrdata_en signal is asserted to when the associated write data is driven on the dfi_wrdata signal. This has no impact on performance, only adjusts the relative time between enable and data transfer. 
<b>Default Value:</b> 1 */
#define MEMC_DFITPHYWRDATA__TPHY_WRDATA__SHIFT       0
#define MEMC_DFITPHYWRDATA__TPHY_WRDATA__WIDTH       6
#define MEMC_DFITPHYWRDATA__TPHY_WRDATA__MASK        0x0000003F
#define MEMC_DFITPHYWRDATA__TPHY_WRDATA__INV_MASK    0xFFFFFFC0
#define MEMC_DFITPHYWRDATA__TPHY_WRDATA__HW_DEFAULT  0x1

/* DFITPHYWRLAT */
/* DFI tphy_wrlat Register */
#define MEMC_DFITPHYWRLAT         0x10800254

/* MEMC_DFITPHYWRLAT.tphy_wrlat - Specifies the number of DFI clock cycles between when a write command is sent on the DFI control interface and when the dfi_wrdata_en signal is asserted. 
<b>Default Value:</b> 1 */
#define MEMC_DFITPHYWRLAT__TPHY_WRLAT__SHIFT       0
#define MEMC_DFITPHYWRLAT__TPHY_WRLAT__WIDTH       6
#define MEMC_DFITPHYWRLAT__TPHY_WRLAT__MASK        0x0000003F
#define MEMC_DFITPHYWRLAT__TPHY_WRLAT__INV_MASK    0xFFFFFFC0
#define MEMC_DFITPHYWRLAT__TPHY_WRLAT__HW_DEFAULT  0x1

/* DFITPHYWRDATALAT */
/* DFI tphy_wrdata_lat Register */
#define MEMC_DFITPHYWRDATALAT     0x10800258

/* MEMC_DFITPHYWRDATALAT.tphy_wrdata_lat - Specifies the number of DFI clock cycles of latency of the DFI write data through the PHY */
#define MEMC_DFITPHYWRDATALAT__TPHY_WRDATA_LAT__SHIFT       0
#define MEMC_DFITPHYWRDATALAT__TPHY_WRDATA_LAT__WIDTH       6
#define MEMC_DFITPHYWRDATALAT__TPHY_WRDATA_LAT__MASK        0x0000003F
#define MEMC_DFITPHYWRDATALAT__TPHY_WRDATA_LAT__INV_MASK    0xFFFFFFC0
#define MEMC_DFITPHYWRDATALAT__TPHY_WRDATA_LAT__HW_DEFAULT  0x0

/* DFITRDDATAEN */
/* DFI trddata_en Register */
#define MEMC_DFITRDDATAEN         0x10800260

/* MEMC_DFITRDDATAEN.trddata_en - Specifies the number of DFI clock cycles from the assertion of a read command on the DFI to the assertion of the dfi_rddata_en signal. 
<b>Default Value:</b> 1 */
#define MEMC_DFITRDDATAEN__TRDDATA_EN__SHIFT       0
#define MEMC_DFITRDDATAEN__TRDDATA_EN__WIDTH       6
#define MEMC_DFITRDDATAEN__TRDDATA_EN__MASK        0x0000003F
#define MEMC_DFITRDDATAEN__TRDDATA_EN__INV_MASK    0xFFFFFFC0
#define MEMC_DFITRDDATAEN__TRDDATA_EN__HW_DEFAULT  0x1

/* DFITPHYRDLAT */
/* DFI tphy_rdlat Register */
#define MEMC_DFITPHYRDLAT         0x10800264

/* MEMC_DFITPHYRDLAT.tphy_rdlat - Specifies the maximum number of DFI clock cycles allowed from the assertion of the dfi_rddata_en signal to the assertion of the dfi_rddata_valid signal. 
<b>Default Value:</b> 15 */
#define MEMC_DFITPHYRDLAT__TPHY_RDLAT__SHIFT       0
#define MEMC_DFITPHYRDLAT__TPHY_RDLAT__WIDTH       6
#define MEMC_DFITPHYRDLAT__TPHY_RDLAT__MASK        0x0000003F
#define MEMC_DFITPHYRDLAT__TPHY_RDLAT__INV_MASK    0xFFFFFFC0
#define MEMC_DFITPHYRDLAT__TPHY_RDLAT__HW_DEFAULT  0xF

/* DFITPHYUPDTYPE0 */
/* DFI tphyupd_type0 Register */
#define MEMC_DFITPHYUPDTYPE0      0x10800270

/* MEMC_DFITPHYUPDTYPE0.tphyupd_type0 - Specifies the maximum number of DFI clock cycles that the dfi_phyupd_req signal may remain asserted after the assertion of the dfi_phyupd_ack signal for dfi_phyupd_type = 0x0. The dfi_phyupd_req signal may de-assert at any cycle after the assertion of the dfi_phyupd_ack signal. 
<b>Default Value:</b> 16 */
#define MEMC_DFITPHYUPDTYPE0__TPHYUPD_TYPE0__SHIFT       0
#define MEMC_DFITPHYUPDTYPE0__TPHYUPD_TYPE0__WIDTH       14
#define MEMC_DFITPHYUPDTYPE0__TPHYUPD_TYPE0__MASK        0x00003FFF
#define MEMC_DFITPHYUPDTYPE0__TPHYUPD_TYPE0__INV_MASK    0xFFFFC000
#define MEMC_DFITPHYUPDTYPE0__TPHYUPD_TYPE0__HW_DEFAULT  0x10

/* DFITPHYUPDTYPE1 */
/* DFI tphyupd_type1 Register */
#define MEMC_DFITPHYUPDTYPE1      0x10800274

/* MEMC_DFITPHYUPDTYPE1.tphyupd_type1 - Specifies the maximum number of DFI clock cycles that the dfi_phyupd_req signal may remain asserted after the assertion of the dfi_phyupd_ack signal for dfi_phyupd_type = 0x1. The dfi_phyupd_req signal may de-assert at any cycle after the assertion of the dfi_phyupd_ack signal. 
<b>Default Value:</b> 16 */
#define MEMC_DFITPHYUPDTYPE1__TPHYUPD_TYPE1__SHIFT       0
#define MEMC_DFITPHYUPDTYPE1__TPHYUPD_TYPE1__WIDTH       14
#define MEMC_DFITPHYUPDTYPE1__TPHYUPD_TYPE1__MASK        0x00003FFF
#define MEMC_DFITPHYUPDTYPE1__TPHYUPD_TYPE1__INV_MASK    0xFFFFC000
#define MEMC_DFITPHYUPDTYPE1__TPHYUPD_TYPE1__HW_DEFAULT  0x10

/* DFITPHYUPDTYPE2 */
/* DFI tphyupd_type2 Register */
#define MEMC_DFITPHYUPDTYPE2      0x10800278

/* MEMC_DFITPHYUPDTYPE2.tphyupd_type2 - Specifies the maximum number of DFI clock cycles that the dfi_phyupd_req signal may remain asserted after the assertion of the dfi_phyupd_ack signal for dfi_phyupd_type = 0x2. The dfi_phyupd_req signal may de-assert at any cycle after the assertion of the dfi_phyupd_ack signal. 
<b>Default Value:</b> 16 */
#define MEMC_DFITPHYUPDTYPE2__TPHYUPD_TYPE2__SHIFT       0
#define MEMC_DFITPHYUPDTYPE2__TPHYUPD_TYPE2__WIDTH       14
#define MEMC_DFITPHYUPDTYPE2__TPHYUPD_TYPE2__MASK        0x00003FFF
#define MEMC_DFITPHYUPDTYPE2__TPHYUPD_TYPE2__INV_MASK    0xFFFFC000
#define MEMC_DFITPHYUPDTYPE2__TPHYUPD_TYPE2__HW_DEFAULT  0x10

/* DFITPHYUPDTYPE3 */
/* DFI tphyupd_type3 Register */
#define MEMC_DFITPHYUPDTYPE3      0x1080027C

/* MEMC_DFITPHYUPDTYPE3.tphyupd_type3 - Specifies the maximum number of DFI clock cycles that the dfi_phyupd_req signal may remain asserted after the assertion of the dfi_phyupd_ack signal for dfi_phyupd_type = 0x3. The dfi_phyupd_req signal may de-assert at any cycle after the assertion of the dfi_phyupd_ack signal. 
<b>Default Value:</b> 16 */
#define MEMC_DFITPHYUPDTYPE3__TPHYUPD_TYPE3__SHIFT       0
#define MEMC_DFITPHYUPDTYPE3__TPHYUPD_TYPE3__WIDTH       14
#define MEMC_DFITPHYUPDTYPE3__TPHYUPD_TYPE3__MASK        0x00003FFF
#define MEMC_DFITPHYUPDTYPE3__TPHYUPD_TYPE3__INV_MASK    0xFFFFC000
#define MEMC_DFITPHYUPDTYPE3__TPHYUPD_TYPE3__HW_DEFAULT  0x10

/* DFITCTRLUPDMIN */
/* DFI TCTRLUPDMIN */
#define MEMC_DFITCTRLUPDMIN       0x10800280

/* MEMC_DFITCTRLUPDMIN.tctrlupd_min - Specifies the minimum number of DFI clock cycles that the dfi_ctrlupd_req signal must be asserted. 
<b>Default Value:</b> 16 */
#define MEMC_DFITCTRLUPDMIN__TCTRLUPD_MIN__SHIFT       0
#define MEMC_DFITCTRLUPDMIN__TCTRLUPD_MIN__WIDTH       16
#define MEMC_DFITCTRLUPDMIN__TCTRLUPD_MIN__MASK        0x0000FFFF
#define MEMC_DFITCTRLUPDMIN__TCTRLUPD_MIN__INV_MASK    0xFFFF0000
#define MEMC_DFITCTRLUPDMIN__TCTRLUPD_MIN__HW_DEFAULT  0x10

/* DFITCTRLUPDMAX */
/* DFI TCTRLUPDMAX */
#define MEMC_DFITCTRLUPDMAX       0x10800284

/* MEMC_DFITCTRLUPDMAX.tctrlupd_max - Specifies the maximum number of DFI clock cycles that the dfi_ctrlupd_req signal can assert. 
<b>Default Value:</b> 64 */
#define MEMC_DFITCTRLUPDMAX__TCTRLUPD_MAX__SHIFT       0
#define MEMC_DFITCTRLUPDMAX__TCTRLUPD_MAX__WIDTH       16
#define MEMC_DFITCTRLUPDMAX__TCTRLUPD_MAX__MASK        0x0000FFFF
#define MEMC_DFITCTRLUPDMAX__TCTRLUPD_MAX__INV_MASK    0xFFFF0000
#define MEMC_DFITCTRLUPDMAX__TCTRLUPD_MAX__HW_DEFAULT  0x40

/* DFITCTRLUPDDLY */
/* DFI TCTRLUPDDLY */
#define MEMC_DFITCTRLUPDDLY       0x10800288

/* MEMC_DFITCTRLUPDDLY.tctrlupd_dly - Delay in DFI clock cycles between time a uPCTL-initiated update could be started and time uPCTL-initated update actually starts (dfi_ctrlupd_req going high). 
<b>Default Value:</b> 8 */
#define MEMC_DFITCTRLUPDDLY__TCTRLUPD_DLY__SHIFT       0
#define MEMC_DFITCTRLUPDDLY__TCTRLUPD_DLY__WIDTH       4
#define MEMC_DFITCTRLUPDDLY__TCTRLUPD_DLY__MASK        0x0000000F
#define MEMC_DFITCTRLUPDDLY__TCTRLUPD_DLY__INV_MASK    0xFFFFFFF0
#define MEMC_DFITCTRLUPDDLY__TCTRLUPD_DLY__HW_DEFAULT  0x8

/* DFIUPDCFG */
/* DFI Configuration Register */
#define MEMC_DFIUPDCFG            0x10800290

/* MEMC_DFIUPDCFG.dfi_ctrlupd_en - Enables the generation of periodic uPCTL-initiated updates: 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b1 */
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN__SHIFT       0
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN__WIDTH       1
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN__MASK        0x00000001
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN__INV_MASK    0xFFFFFFFE
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN__HW_DEFAULT  0x1

/* MEMC_DFIUPDCFG.dfi_phyupd_en - Enables the support for acknowledging PHY-initiated updates: 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b1 */
#define MEMC_DFIUPDCFG__DFI_PHYUPD_EN__SHIFT       1
#define MEMC_DFIUPDCFG__DFI_PHYUPD_EN__WIDTH       1
#define MEMC_DFIUPDCFG__DFI_PHYUPD_EN__MASK        0x00000002
#define MEMC_DFIUPDCFG__DFI_PHYUPD_EN__INV_MASK    0xFFFFFFFD
#define MEMC_DFIUPDCFG__DFI_PHYUPD_EN__HW_DEFAULT  0x1

/* MEMC_DFIUPDCFG.dfi_ctrlupd_en_wk - Enables the generation of uPCTL-initiated updates during Wakeup (from Low_power to Access state transition): 
1'b0 = Disabled 
1'b1 = Enabled 
<b>Default Value:</b> 1'b1 */
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN_WK__SHIFT       2
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN_WK__WIDTH       1
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN_WK__MASK        0x00000004
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN_WK__INV_MASK    0xFFFFFFFB
#define MEMC_DFIUPDCFG__DFI_CTRLUPD_EN_WK__HW_DEFAULT  0x1

/* DFITREFMSKI */
/* DFI REFMSK Interval */
#define MEMC_DFITREFMSKI          0x10800294

/* MEMC_DFITREFMSKI.trefmski - Time period of the DFI masked Refresh interval. 
This value is only used if TREFI==0. 
Defines the time period (in 100 ns units) of the masked Refresh (REFMSK) interval. 
The actual time period defined is DFITREFMSKI * TOGCNT100N * internal timers clock period. 
<b>Default Value:</b> 0 */
#define MEMC_DFITREFMSKI__TREFMSKI__SHIFT       0
#define MEMC_DFITREFMSKI__TREFMSKI__WIDTH       13
#define MEMC_DFITREFMSKI__TREFMSKI__MASK        0x00001FFF
#define MEMC_DFITREFMSKI__TREFMSKI__INV_MASK    0xFFFFE000
#define MEMC_DFITREFMSKI__TREFMSKI__HW_DEFAULT  0x0

/* DFITCTRLUPDI */
/* DFI tctrlupd_interval Register */
#define MEMC_DFITCTRLUPDI         0x10800298

/* MEMC_DFITCTRLUPDI.tctrlupdi - DFI uPCTL-initiated updates interval, measured in terms of Refresh interval units. 
If TREFI!=0, the time period is defined as DFITCTRLUPDI * TREFI * TOGCNT100N * internal timers clock period. 
If TREFI==0 and DFITREFMSKI!=0, the period changes to DFITCTRLUPDI * DFITREFMSKI * TOGCNT100N * internal timers clock period. 
Programming a value of 0 is the same as programming a value of 1; for instance, a uPCTL-initiated update occurs every Refresh interval. 
<b>Default Value:</b> 0 */
#define MEMC_DFITCTRLUPDI__TCTRLUPDI__SHIFT       0
#define MEMC_DFITCTRLUPDI__TCTRLUPDI__WIDTH       32
#define MEMC_DFITCTRLUPDI__TCTRLUPDI__MASK        0xFFFFFFFF
#define MEMC_DFITCTRLUPDI__TCTRLUPDI__INV_MASK    0x00000000
#define MEMC_DFITCTRLUPDI__TCTRLUPDI__HW_DEFAULT  0x0

/* DFITRCFG0 */
/* DFI Training Configuration 0 Register */
#define MEMC_DFITRCFG0            0x108002AC

/* MEMC_DFITRCFG0.dfi_rdlvl_rank_sel - Determines the value to drive on the output signal dfi_rdlvl_cs_n. The value on dfi_rdlvl_cs_n is the inverse of the setting in this field. 
<b>Default Value:</b> 4'b0000 */
#define MEMC_DFITRCFG0__DFI_RDLVL_RANK_SEL__SHIFT       0
#define MEMC_DFITRCFG0__DFI_RDLVL_RANK_SEL__WIDTH       4
#define MEMC_DFITRCFG0__DFI_RDLVL_RANK_SEL__MASK        0x0000000F
#define MEMC_DFITRCFG0__DFI_RDLVL_RANK_SEL__INV_MASK    0xFFFFFFF0
#define MEMC_DFITRCFG0__DFI_RDLVL_RANK_SEL__HW_DEFAULT  0x0

/* MEMC_DFITRCFG0.dfi_rdlvl_edge - Determines the value to drive on the output signal dfi_rdlvl_edge. The value on dfi_rdlvl_edge is the same as the setting in this field. 
<b>Default Value:</b> 9'b000000000 */
#define MEMC_DFITRCFG0__DFI_RDLVL_EDGE__SHIFT       4
#define MEMC_DFITRCFG0__DFI_RDLVL_EDGE__WIDTH       9
#define MEMC_DFITRCFG0__DFI_RDLVL_EDGE__MASK        0x00001FF0
#define MEMC_DFITRCFG0__DFI_RDLVL_EDGE__INV_MASK    0xFFFFE00F
#define MEMC_DFITRCFG0__DFI_RDLVL_EDGE__HW_DEFAULT  0x0

/* MEMC_DFITRCFG0.dfi_wrlvl_rank_sel - Determines the value to drive on the output signal dfi_wrlvl_cs_n. The value on dfi_wrlvl_cs_n is the inverse of the setting in this field. 
<b>Default Value:</b> 4'b0000 */
#define MEMC_DFITRCFG0__DFI_WRLVL_RANK_SEL__SHIFT       16
#define MEMC_DFITRCFG0__DFI_WRLVL_RANK_SEL__WIDTH       4
#define MEMC_DFITRCFG0__DFI_WRLVL_RANK_SEL__MASK        0x000F0000
#define MEMC_DFITRCFG0__DFI_WRLVL_RANK_SEL__INV_MASK    0xFFF0FFFF
#define MEMC_DFITRCFG0__DFI_WRLVL_RANK_SEL__HW_DEFAULT  0x0

/* DFITRSTAT0 */
/* DFI Training Status 0 Register */
#define MEMC_DFITRSTAT0           0x108002B0

/* MEMC_DFITRSTAT0.dfi_rdlvl_mode - Reports the value of the input signal dfi_rdlvl_mode. 
Note: This is a sample of the signal taken to the APB clock domain. 
<b>Default Value:</b> 2'b00 */
#define MEMC_DFITRSTAT0__DFI_RDLVL_MODE__SHIFT       0
#define MEMC_DFITRSTAT0__DFI_RDLVL_MODE__WIDTH       2
#define MEMC_DFITRSTAT0__DFI_RDLVL_MODE__MASK        0x00000003
#define MEMC_DFITRSTAT0__DFI_RDLVL_MODE__INV_MASK    0xFFFFFFFC
#define MEMC_DFITRSTAT0__DFI_RDLVL_MODE__HW_DEFAULT  0x3

/* MEMC_DFITRSTAT0.dfi_rdlvl_gate_mode - Reports the value of the input signal dfi_rdlvl_gate_mode. 
Note: This is a sample of the signal taken to the APB clock domain. 
<b>Default Value:</b> 2'b00 */
#define MEMC_DFITRSTAT0__DFI_RDLVL_GATE_MODE__SHIFT       8
#define MEMC_DFITRSTAT0__DFI_RDLVL_GATE_MODE__WIDTH       2
#define MEMC_DFITRSTAT0__DFI_RDLVL_GATE_MODE__MASK        0x00000300
#define MEMC_DFITRSTAT0__DFI_RDLVL_GATE_MODE__INV_MASK    0xFFFFFCFF
#define MEMC_DFITRSTAT0__DFI_RDLVL_GATE_MODE__HW_DEFAULT  0x3

/* MEMC_DFITRSTAT0.dfi_wrlvl_mode - Reports the value of the input signal dfi_wrlvl_mode. 
Note: This is a sample of the signal taken to the APB clock domain. 
<b>Default Value:</b> 2'b00 */
#define MEMC_DFITRSTAT0__DFI_WRLVL_MODE__SHIFT       16
#define MEMC_DFITRSTAT0__DFI_WRLVL_MODE__WIDTH       2
#define MEMC_DFITRSTAT0__DFI_WRLVL_MODE__MASK        0x00030000
#define MEMC_DFITRSTAT0__DFI_WRLVL_MODE__INV_MASK    0xFFFCFFFF
#define MEMC_DFITRSTAT0__DFI_WRLVL_MODE__HW_DEFAULT  0x3

/* DFITRWRLVLEN */
/* DFI Training dfi_wrlvl_en Register */
#define MEMC_DFITRWRLVLEN         0x108002B4

/* MEMC_DFITRWRLVLEN.dfi_wrlvl_en - Determines the value to drive on the output signal dfi_wrlvl_en. 
<b>Default Value:</b> 9'b000000000 */
#define MEMC_DFITRWRLVLEN__DFI_WRLVL_EN__SHIFT       0
#define MEMC_DFITRWRLVLEN__DFI_WRLVL_EN__WIDTH       9
#define MEMC_DFITRWRLVLEN__DFI_WRLVL_EN__MASK        0x000001FF
#define MEMC_DFITRWRLVLEN__DFI_WRLVL_EN__INV_MASK    0xFFFFFE00
#define MEMC_DFITRWRLVLEN__DFI_WRLVL_EN__HW_DEFAULT  0x0

/* DFITRRDLVLEN */
/* DFI Training dfi_rdlvl_en Register */
#define MEMC_DFITRRDLVLEN         0x108002B8

/* MEMC_DFITRRDLVLEN.dfi_rdlvl_en - Determines the value to drive on the output signal dfi_rdlvl_en. 
<b>Default Value:</b> 9'b000000000 */
#define MEMC_DFITRRDLVLEN__DFI_RDLVL_EN__SHIFT       0
#define MEMC_DFITRRDLVLEN__DFI_RDLVL_EN__WIDTH       9
#define MEMC_DFITRRDLVLEN__DFI_RDLVL_EN__MASK        0x000001FF
#define MEMC_DFITRRDLVLEN__DFI_RDLVL_EN__INV_MASK    0xFFFFFE00
#define MEMC_DFITRRDLVLEN__DFI_RDLVL_EN__HW_DEFAULT  0x0

/* DFITRRDLVLGATEEN */
/* DFI Training dfi_rdlvl_gate_en Register */
#define MEMC_DFITRRDLVLGATEEN     0x108002BC

/* MEMC_DFITRRDLVLGATEEN.dfi_rdlvl_gate_en - Determines the value to drive on the output signal dfi_rdlvl_gate_en. 
<b>Default Value:</b> 9'b000000000 */
#define MEMC_DFITRRDLVLGATEEN__DFI_RDLVL_GATE_EN__SHIFT       0
#define MEMC_DFITRRDLVLGATEEN__DFI_RDLVL_GATE_EN__WIDTH       9
#define MEMC_DFITRRDLVLGATEEN__DFI_RDLVL_GATE_EN__MASK        0x000001FF
#define MEMC_DFITRRDLVLGATEEN__DFI_RDLVL_GATE_EN__INV_MASK    0xFFFFFE00
#define MEMC_DFITRRDLVLGATEEN__DFI_RDLVL_GATE_EN__HW_DEFAULT  0x0

/* DFISTSTAT0 */
/* DFI Status Status 0 Register */
#define MEMC_DFISTSTAT0           0x108002C0

/* MEMC_DFISTSTAT0.dfi_init_complete - Reports the value of the input signal dfi_init_complete. 
Note: This is a sample of the signal taken to the APB clock domain 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTSTAT0__DFI_INIT_COMPLETE__SHIFT       0
#define MEMC_DFISTSTAT0__DFI_INIT_COMPLETE__WIDTH       1
#define MEMC_DFISTSTAT0__DFI_INIT_COMPLETE__MASK        0x00000001
#define MEMC_DFISTSTAT0__DFI_INIT_COMPLETE__INV_MASK    0xFFFFFFFE
#define MEMC_DFISTSTAT0__DFI_INIT_COMPLETE__HW_DEFAULT  0x0

/* MEMC_DFISTSTAT0.dfi_init_start - Reports the value of the output signal dfi_init_start. 
Note: This is a sample of the signal taken to the APB clock domain 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTSTAT0__DFI_INIT_START__SHIFT       1
#define MEMC_DFISTSTAT0__DFI_INIT_START__WIDTH       1
#define MEMC_DFISTSTAT0__DFI_INIT_START__MASK        0x00000002
#define MEMC_DFISTSTAT0__DFI_INIT_START__INV_MASK    0xFFFFFFFD
#define MEMC_DFISTSTAT0__DFI_INIT_START__HW_DEFAULT  0x0

/* MEMC_DFISTSTAT0.dfi_freq_ratio - Reports the value of the output signal dfi_freq_ratio. 
Note: This is a sample of the signal taken to the APB clock domain 
<b>Default Value:</b> 2'b00 */
#define MEMC_DFISTSTAT0__DFI_FREQ_RATIO__SHIFT       4
#define MEMC_DFISTSTAT0__DFI_FREQ_RATIO__WIDTH       2
#define MEMC_DFISTSTAT0__DFI_FREQ_RATIO__MASK        0x00000030
#define MEMC_DFISTSTAT0__DFI_FREQ_RATIO__INV_MASK    0xFFFFFFCF
#define MEMC_DFISTSTAT0__DFI_FREQ_RATIO__HW_DEFAULT  0x0

/* MEMC_DFISTSTAT0.dfi_data_byte_disable - Reports the value of the output signal dfi_data_byte_disable. 
Note: This is a sample of the signal taken to the APB clock domain. 
<b>Default Value:</b> 9'b000000000 */
#define MEMC_DFISTSTAT0__DFI_DATA_BYTE_DISABLE__SHIFT       16
#define MEMC_DFISTSTAT0__DFI_DATA_BYTE_DISABLE__WIDTH       9
#define MEMC_DFISTSTAT0__DFI_DATA_BYTE_DISABLE__MASK        0x01FF0000
#define MEMC_DFISTSTAT0__DFI_DATA_BYTE_DISABLE__INV_MASK    0xFE00FFFF
#define MEMC_DFISTSTAT0__DFI_DATA_BYTE_DISABLE__HW_DEFAULT  0x0

/* DFISTCFG0 */
/* DFI Status Configuration 0 Register */
#define MEMC_DFISTCFG0            0x108002C4

/* MEMC_DFISTCFG0.dfi_init_start - Sets the value of the dfi_init_start signal. 
1'b0 - dfi_init_start is driven low 
1'b1 - dfi__init_start is driven high 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG0__DFI_INIT_START__SHIFT       0
#define MEMC_DFISTCFG0__DFI_INIT_START__WIDTH       1
#define MEMC_DFISTCFG0__DFI_INIT_START__MASK        0x00000001
#define MEMC_DFISTCFG0__DFI_INIT_START__INV_MASK    0xFFFFFFFE
#define MEMC_DFISTCFG0__DFI_INIT_START__HW_DEFAULT  0x0

/* MEMC_DFISTCFG0.dfi_freq_ratio_en - Enables the driving of the dfi_freq_ratio signal. 
When enabled, the dfi_freq_ratio value driven is dependent on configuration parameter UPCTL_FREQ_RATIO: 2'b00 is driven when UPCTL_FREQ_RATIO=1 and 2'b01 is driven when UPCTL_FREQ_RATIO=2 
1'b0 - Drive dfi_freq_ratio to default value of 2'b00 
1'b1 - Drive dfi_freq_ratio value according to how configuration parameter is set. 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG0__DFI_FREQ_RATIO_EN__SHIFT       1
#define MEMC_DFISTCFG0__DFI_FREQ_RATIO_EN__WIDTH       1
#define MEMC_DFISTCFG0__DFI_FREQ_RATIO_EN__MASK        0x00000002
#define MEMC_DFISTCFG0__DFI_FREQ_RATIO_EN__INV_MASK    0xFFFFFFFD
#define MEMC_DFISTCFG0__DFI_FREQ_RATIO_EN__HW_DEFAULT  0x0

/* MEMC_DFISTCFG0.dfi_data_byte_disable_en - Enables the driving of the dfi_data_byte_disable signal. The value driven on dfi_data_byte_disable is dependent on the setting of PPCFG register. 
1'b0 - Drive dfi_data_byte_disable to default value of all zeroes. 
1'b1 - Drive dfi_data_byte_disable according to value as defined by PPCFG register setting. 
Note: should be set to 1'b1 only after PPCFG is correctly set. 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG0__DFI_DATA_BYTE_DISABLE_EN__SHIFT       2
#define MEMC_DFISTCFG0__DFI_DATA_BYTE_DISABLE_EN__WIDTH       1
#define MEMC_DFISTCFG0__DFI_DATA_BYTE_DISABLE_EN__MASK        0x00000004
#define MEMC_DFISTCFG0__DFI_DATA_BYTE_DISABLE_EN__INV_MASK    0xFFFFFFFB
#define MEMC_DFISTCFG0__DFI_DATA_BYTE_DISABLE_EN__HW_DEFAULT  0x0

/* DFISTCFG1 */
/* DFI Status Configuration 1 Register */
#define MEMC_DFISTCFG1            0x108002C8

/* MEMC_DFISTCFG1.dfi_dram_clk_disable_en - Enables support of the dfi_dram_clk_disable signal with Self Refresh (SR). 
1'b0 - Disable dfi_dram_clk_disable support in relation to SR 
1'b1 - Enable dfi_dram_clk_disable support in relation to SR 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN__SHIFT       0
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN__WIDTH       1
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN__MASK        0x00000001
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN__INV_MASK    0xFFFFFFFE
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN__HW_DEFAULT  0x0

/* MEMC_DFISTCFG1.dfi_dram_clk_disable_en_dpd - Enables support of the dfi_dram_clk_disable signal with Deep Power Down (DPD). DPD is only for mDDR/LPDDR2. 
1'b0 - Disable dfi_dram_clk_disable support in relation to DPD 
1'b1 - Enable dfi_dram_clk_disable support in relation to DPD 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN_DPD__SHIFT       1
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN_DPD__WIDTH       1
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN_DPD__MASK        0x00000002
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN_DPD__INV_MASK    0xFFFFFFFD
#define MEMC_DFISTCFG1__DFI_DRAM_CLK_DISABLE_EN_DPD__HW_DEFAULT  0x0

/* DFITDRAMCLKEN */
/* DFI tdram_clk_enable Register */
#define MEMC_DFITDRAMCLKEN        0x108002D0

/* MEMC_DFITDRAMCLKEN.tdram_clk_enable - Specifies the number of DFI clock cycles from the de-assertion of the dfi_dram_clk_disable signal on the DFI until the first valid rising edge of the clock to the DRAM memory devices, at the PHY-DRAM boundary. If the DFI clock and the memory clock are not phasealigned, this timing parameter should be rounded up to the next integer value. 
<b>Default Value:</b> 2 */
#define MEMC_DFITDRAMCLKEN__TDRAM_CLK_ENABLE__SHIFT       0
#define MEMC_DFITDRAMCLKEN__TDRAM_CLK_ENABLE__WIDTH       4
#define MEMC_DFITDRAMCLKEN__TDRAM_CLK_ENABLE__MASK        0x0000000F
#define MEMC_DFITDRAMCLKEN__TDRAM_CLK_ENABLE__INV_MASK    0xFFFFFFF0
#define MEMC_DFITDRAMCLKEN__TDRAM_CLK_ENABLE__HW_DEFAULT  0x2

/* DFITDRAMCLKDIS */
/* DFI tdram_clk_disable Register */
#define MEMC_DFITDRAMCLKDIS       0x108002D4

/* MEMC_DFITDRAMCLKDIS.tdram_clk_disable - Specifies the number of DFI clock cycles from the assertion of the dfi_dram_clk_disable signal on the DFI until the clock to the DRAM memory devices, at the PHY-DRAM boundary, maintains a low value. If the DFI clock and the memory clock are not phasealigned, this timing parameter should be rounded up to the next integer value. 
<b>Default Value:</b> 2 */
#define MEMC_DFITDRAMCLKDIS__TDRAM_CLK_DISABLE__SHIFT       0
#define MEMC_DFITDRAMCLKDIS__TDRAM_CLK_DISABLE__WIDTH       4
#define MEMC_DFITDRAMCLKDIS__TDRAM_CLK_DISABLE__MASK        0x0000000F
#define MEMC_DFITDRAMCLKDIS__TDRAM_CLK_DISABLE__INV_MASK    0xFFFFFFF0
#define MEMC_DFITDRAMCLKDIS__TDRAM_CLK_DISABLE__HW_DEFAULT  0x2

/* DFISTCFG2 */
/* DFI Status Configuration 2 Register */
#define MEMC_DFISTCFG2            0x108002D8

/* MEMC_DFISTCFG2.parity_intr_en - Enable interrupt generation for DFI parity error (from input signal dfi_parity_error). 
1'b0 - Disable interrupt 
1'b1 - Enable interrupt 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG2__PARITY_INTR_EN__SHIFT       0
#define MEMC_DFISTCFG2__PARITY_INTR_EN__WIDTH       1
#define MEMC_DFISTCFG2__PARITY_INTR_EN__MASK        0x00000001
#define MEMC_DFISTCFG2__PARITY_INTR_EN__INV_MASK    0xFFFFFFFE
#define MEMC_DFISTCFG2__PARITY_INTR_EN__HW_DEFAULT  0x0

/* MEMC_DFISTCFG2.parity_en - Enables the DFI parity generation feature (driven on output signal dfi_parity_in) 
1'b0 - Disable DFI parity generation 
1'b1 - Enable DFI parity generation 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTCFG2__PARITY_EN__SHIFT       1
#define MEMC_DFISTCFG2__PARITY_EN__WIDTH       1
#define MEMC_DFISTCFG2__PARITY_EN__MASK        0x00000002
#define MEMC_DFISTCFG2__PARITY_EN__INV_MASK    0xFFFFFFFD
#define MEMC_DFISTCFG2__PARITY_EN__HW_DEFAULT  0x0

/* DFISTPARCLR */
/* DFI Status Parity Clear Register */
#define MEMC_DFISTPARCLR          0x108002DC

/* MEMC_DFISTPARCLR.parity_intr_clr - Set this bit to 1'b1 to clear the interrupt generated by an DFI parity error (as enabled by DFISTCFG2.parity_intr_en). It also clears the INTRSTAT.parity_intr register field. It is automatically cleared by hardware when the interrupt has been cleared. 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTPARCLR__PARITY_INTR_CLR__SHIFT       0
#define MEMC_DFISTPARCLR__PARITY_INTR_CLR__WIDTH       1
#define MEMC_DFISTPARCLR__PARITY_INTR_CLR__MASK        0x00000001
#define MEMC_DFISTPARCLR__PARITY_INTR_CLR__INV_MASK    0xFFFFFFFE
#define MEMC_DFISTPARCLR__PARITY_INTR_CLR__HW_DEFAULT  0x0

/* MEMC_DFISTPARCLR.parity_log_clr - Set this bit to 1'b1 to clear the DFI Status Parity Log register (DFISTPARLOG). 
1'b0 = Do not clear DFI status Parity Log register 
1'b1 = Clear DFI status Parity Log register 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFISTPARCLR__PARITY_LOG_CLR__SHIFT       1
#define MEMC_DFISTPARCLR__PARITY_LOG_CLR__WIDTH       1
#define MEMC_DFISTPARCLR__PARITY_LOG_CLR__MASK        0x00000002
#define MEMC_DFISTPARCLR__PARITY_LOG_CLR__INV_MASK    0xFFFFFFFD
#define MEMC_DFISTPARCLR__PARITY_LOG_CLR__HW_DEFAULT  0x0

/* DFISTPARLOG */
/* DFI Status Parity Log Register */
#define MEMC_DFISTPARLOG          0x108002E0

/* MEMC_DFISTPARLOG.parity_err_cnt - Increments any time the DFI parity logic detects a parity error(s) (on dfi_parity_error). 
<b>Default Value:</b> 0 */
#define MEMC_DFISTPARLOG__PARITY_ERR_CNT__SHIFT       0
#define MEMC_DFISTPARLOG__PARITY_ERR_CNT__WIDTH       32
#define MEMC_DFISTPARLOG__PARITY_ERR_CNT__MASK        0xFFFFFFFF
#define MEMC_DFISTPARLOG__PARITY_ERR_CNT__INV_MASK    0x00000000
#define MEMC_DFISTPARLOG__PARITY_ERR_CNT__HW_DEFAULT  0x0

/* DFILPCFG0 */
/* DFI Low Power Configuration 0 Register */
#define MEMC_DFILPCFG0            0x108002F0

/* MEMC_DFILPCFG0.dfi_lp_en_pd - Enables DFI Low Power interface handshaking during Power Down Entry/Exit. 
1'b0 - Disabled 
1'b1 - Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFILPCFG0__DFI_LP_EN_PD__SHIFT       0
#define MEMC_DFILPCFG0__DFI_LP_EN_PD__WIDTH       1
#define MEMC_DFILPCFG0__DFI_LP_EN_PD__MASK        0x00000001
#define MEMC_DFILPCFG0__DFI_LP_EN_PD__INV_MASK    0xFFFFFFFE
#define MEMC_DFILPCFG0__DFI_LP_EN_PD__HW_DEFAULT  0x0

/* MEMC_DFILPCFG0.dfi_lp_wakeup_pd - Value to drive on dfi_lp_wakeup signal when Power Down mode is entered. 
Determines the DFI�s tlp_wakeup time: 
4'b0000 - 16 cycles 
4'b0001 - 32 cycles 
4'b0010 - 64 cycles 
4'b0011 - 128 cycles 
4'b0100 - 256 cycles 
4'b0101 - 512 cycles 
4'b0110 - 1024 cycles 
4'b0111 - 2048 cycles 
4'b1000 - 4096 cycles 
4'b1001 - 8192 cycles 
4'b1010 - 16384 cycles 
4'b1011 - 32768 cycles 
4'b1100 - 65536 cycles 
4'b1101 - 131072 cycles 
4'b1110 - 262144 cycles 
4'b1111 - Unlimited 
<b>Default Value:</b> 4'b0000 */
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_PD__SHIFT       4
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_PD__WIDTH       4
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_PD__MASK        0x000000F0
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_PD__INV_MASK    0xFFFFFF0F
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_PD__HW_DEFAULT  0x0

/* MEMC_DFILPCFG0.dfi_lp_en_sr - Enables DFI Low Power interface handshaking during Self Refresh Entry/Exit. 
1'b0 - Disabled 
1'b1 - Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFILPCFG0__DFI_LP_EN_SR__SHIFT       8
#define MEMC_DFILPCFG0__DFI_LP_EN_SR__WIDTH       1
#define MEMC_DFILPCFG0__DFI_LP_EN_SR__MASK        0x00000100
#define MEMC_DFILPCFG0__DFI_LP_EN_SR__INV_MASK    0xFFFFFEFF
#define MEMC_DFILPCFG0__DFI_LP_EN_SR__HW_DEFAULT  0x0

/* MEMC_DFILPCFG0.dfi_lp_wakeup_sr - Value to drive on dfi_lp_wakeup signal when Self Refresh mode is entered. 
Determines the DFI�s tlp_wakeup time: 
4'b0000 - 16 cycles 
4'b0001 - 32 cycles 
4'b0010 - 64 cycles 
4'b0011 - 128 cycles 
4'b0100 - 256 cycles 
4'b0101 - 512 cycles 
4'b0110 - 1024 cycles 
4'b0111 - 2048 cycles 
4'b1000 - 4096 cycles 
4'b1001 - 8192 cycles 
4'b1010 - 16384 cycles 
4'b1011 - 32768 cycles 
4'b1100 - 65536 cycles 
4'b1101 - 131072 cycles 
4'b1110 - 262144 cycles 
4'b1111 - Unlimited 
<b>Default Value:</b> 4'b0000 */
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_SR__SHIFT       12
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_SR__WIDTH       4
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_SR__MASK        0x0000F000
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_SR__INV_MASK    0xFFFF0FFF
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_SR__HW_DEFAULT  0x0

/* MEMC_DFILPCFG0.dfi_tlp_resp - Setting for tlp_resp time. Same value is used for both Power Down and Self refresh and Deep Power Down modes. 
DFI 2.1 specification, recommends using value of 7 always. 
<b>Default Value:</b> 7 */
#define MEMC_DFILPCFG0__DFI_TLP_RESP__SHIFT       16
#define MEMC_DFILPCFG0__DFI_TLP_RESP__WIDTH       4
#define MEMC_DFILPCFG0__DFI_TLP_RESP__MASK        0x000F0000
#define MEMC_DFILPCFG0__DFI_TLP_RESP__INV_MASK    0xFFF0FFFF
#define MEMC_DFILPCFG0__DFI_TLP_RESP__HW_DEFAULT  0x7

/* MEMC_DFILPCFG0.dfi_lp_en_dpd - Enables DFI Low Power interface handshaking during Deep Power Down Entry/Exit. 
1'b0 - Disabled 
1'b1 - Enabled 
<b>Default Value:</b> 1'b0 */
#define MEMC_DFILPCFG0__DFI_LP_EN_DPD__SHIFT       24
#define MEMC_DFILPCFG0__DFI_LP_EN_DPD__WIDTH       1
#define MEMC_DFILPCFG0__DFI_LP_EN_DPD__MASK        0x01000000
#define MEMC_DFILPCFG0__DFI_LP_EN_DPD__INV_MASK    0xFEFFFFFF
#define MEMC_DFILPCFG0__DFI_LP_EN_DPD__HW_DEFAULT  0x0

/* MEMC_DFILPCFG0.dfi_lp_wakeup_dpd - Value to drive on dfi_lp_wakeup signal when Deep Power Down mode is entered. 
Determines the DFI�s tlp_wakeup time: 
4'b0000 - 16 cycles 
4'b0001 - 32 cycles 
4'b0010 - 64 cycles 
4'b0011 - 128 cycles 
4'b0100 - 256 cycles 
4'b0101 - 512 cycles 
4'b0110 - 1024 cycles 
4'b0111 - 2048 cycles 
4'b1000 - 4096 cycles 
4'b1001 - 8192 cycles 
4'b1010 - 16384 cycles 
4'b1011 - 32768 cycles 
4'b1100 - 65536 cycles 
4'b1101 - 131072 cycles 
4'b1110 - 262144 cycles 
4'b1111 - Unlimited 
<b>Default Value:</b> 4'b0000 */
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_DPD__SHIFT       28
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_DPD__WIDTH       4
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_DPD__MASK        0xF0000000
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_DPD__INV_MASK    0x0FFFFFFF
#define MEMC_DFILPCFG0__DFI_LP_WAKEUP_DPD__HW_DEFAULT  0x0

/* DFITRWRLVLRESP0 */
/* DFI Training dfi_wrlvl_resp Status 0 Register */
#define MEMC_DFITRWRLVLRESP0      0x10800300

/* MEMC_DFITRWRLVLRESP0.dfi_wrlvl_resp0 - Reports the status of the dif_wrlvl_resp[31:0] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: The actual width of this register is dependent in DFI_WRLVL_WRESP value. If DFI_WRLVL_WRESP<32, then only DFI_WRLVL_WRESP bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRWRLVLRESP0__DFI_WRLVL_RESP0__SHIFT       0
#define MEMC_DFITRWRLVLRESP0__DFI_WRLVL_RESP0__WIDTH       32
#define MEMC_DFITRWRLVLRESP0__DFI_WRLVL_RESP0__MASK        0xFFFFFFFF
#define MEMC_DFITRWRLVLRESP0__DFI_WRLVL_RESP0__INV_MASK    0x00000000
#define MEMC_DFITRWRLVLRESP0__DFI_WRLVL_RESP0__HW_DEFAULT  0x0

/* DFITRWRLVLRESP1 */
/* DFI Training dfi_wrlvl_resp Status 1 Register */
#define MEMC_DFITRWRLVLRESP1      0x10800304

/* MEMC_DFITRWRLVLRESP1.dfi_wrlvl_resp1 - Reports the status of the dif_wrlvl_resp[63:32] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: This register does only exists if DFI_WRLVL_WRESP>32. 
Note: The actual width of this register is dependent in DFI_WRLVL_WRESP value. If DFI_WRLVL_WRESP<64, then only DFI_WRLVL_WRESP-32 bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRWRLVLRESP1__DFI_WRLVL_RESP1__SHIFT       0
#define MEMC_DFITRWRLVLRESP1__DFI_WRLVL_RESP1__WIDTH       32
#define MEMC_DFITRWRLVLRESP1__DFI_WRLVL_RESP1__MASK        0xFFFFFFFF
#define MEMC_DFITRWRLVLRESP1__DFI_WRLVL_RESP1__INV_MASK    0x00000000
#define MEMC_DFITRWRLVLRESP1__DFI_WRLVL_RESP1__HW_DEFAULT  0x0

/* DFITRWRLVLRESP2 */
/* DFI Training dfi_wrlvl_resp Status 2 Register */
#define MEMC_DFITRWRLVLRESP2      0x10800308

/* MEMC_DFITRWRLVLRESP2.dfi_wrlvl_resp2 - Reports the status of the dif_wrlvl_resp[71:64] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: This register does only exists if DFI_WRLVL_WRESP>64. 
Note: The actual width of this register is dependent in DFI_WRLVL_WRESP value. If DFI_WRLVL_WRESP<72, then only DFI_WRLVL_WRESP-64 bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRWRLVLRESP2__DFI_WRLVL_RESP2__SHIFT       0
#define MEMC_DFITRWRLVLRESP2__DFI_WRLVL_RESP2__WIDTH       8
#define MEMC_DFITRWRLVLRESP2__DFI_WRLVL_RESP2__MASK        0x000000FF
#define MEMC_DFITRWRLVLRESP2__DFI_WRLVL_RESP2__INV_MASK    0xFFFFFF00
#define MEMC_DFITRWRLVLRESP2__DFI_WRLVL_RESP2__HW_DEFAULT  0x0

/* DFITRRDLVLRESP0 */
/* DFI Training dfi_rdlvl_resp Status 0 Register */
#define MEMC_DFITRRDLVLRESP0      0x1080030C

/* MEMC_DFITRRDLVLRESP0.dfi_rdlvl_resp0 - Reports the status of the dif_rdlvl_resp[31:0] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: The actual width of this register is dependent in DFI_RDLVL_WRESP value. If DFI_RDLVL_WRESP<32, then only DFI_RDLVL_WRESP bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRRDLVLRESP0__DFI_RDLVL_RESP0__SHIFT       0
#define MEMC_DFITRRDLVLRESP0__DFI_RDLVL_RESP0__WIDTH       32
#define MEMC_DFITRRDLVLRESP0__DFI_RDLVL_RESP0__MASK        0xFFFFFFFF
#define MEMC_DFITRRDLVLRESP0__DFI_RDLVL_RESP0__INV_MASK    0x00000000
#define MEMC_DFITRRDLVLRESP0__DFI_RDLVL_RESP0__HW_DEFAULT  0x0

/* DFITRRDLVLRESP1 */
/* DFI Training dfi_rdlvl_resp Status 1 Register */
#define MEMC_DFITRRDLVLRESP1      0x10800310

/* MEMC_DFITRRDLVLRESP1.dfi_rdlvl_resp1 - Reports the status of the dif_rdlvl_resp[63:32] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: This register does only exists if DFI_RDLVL_WRESP>32. 
Note: The actual width of this register is dependent in DFI_RDLVL_WRESP value. If DFI_RDLVL_WRESP<64, then only DFI_RDLVL_WRESP-32 bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRRDLVLRESP1__DFI_RDLVL_RESP1__SHIFT       0
#define MEMC_DFITRRDLVLRESP1__DFI_RDLVL_RESP1__WIDTH       32
#define MEMC_DFITRRDLVLRESP1__DFI_RDLVL_RESP1__MASK        0xFFFFFFFF
#define MEMC_DFITRRDLVLRESP1__DFI_RDLVL_RESP1__INV_MASK    0x00000000
#define MEMC_DFITRRDLVLRESP1__DFI_RDLVL_RESP1__HW_DEFAULT  0x0

/* DFITRRDLVLRESP2 */
/* DFI Training dfi_rdlvl_resp Status 2 Register */
#define MEMC_DFITRRDLVLRESP2      0x10800314

/* MEMC_DFITRRDLVLRESP2.dfi_rdlvl_resp2 - Reports the status of the dif_rdlvl_resp[71:64] signal. 
Note: This is a sample of the signal taken to the APB clock domain 
Note: This register does only exists if DFI_RDLVL_WRESP>64. 
Note: The actual width of this register is dependent in DFI_RDLVL_WRESP value. If DFI_RDLVL_WRESP<72, then only DFI_RDLVL_WRESP-64 bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRRDLVLRESP2__DFI_RDLVL_RESP2__SHIFT       0
#define MEMC_DFITRRDLVLRESP2__DFI_RDLVL_RESP2__WIDTH       8
#define MEMC_DFITRRDLVLRESP2__DFI_RDLVL_RESP2__MASK        0x000000FF
#define MEMC_DFITRRDLVLRESP2__DFI_RDLVL_RESP2__INV_MASK    0xFFFFFF00
#define MEMC_DFITRRDLVLRESP2__DFI_RDLVL_RESP2__HW_DEFAULT  0x0

/* DFITRWRLVLDELAY0 */
/* DFI Training dfi_wrlvl_delay Configuration 0 Register */
#define MEMC_DFITRWRLVLDELAY0     0x10800318

/* MEMC_DFITRWRLVLDELAY0.dfi_wrlvl_delay0 - Sets the value to be driven on the signal dfi_wrlvl_delay_x[31:0]. 
Note: The actual width of this register is dependent in DFI_WRLVL_WDLY value. If DFI_WRLVL_WDLY<32, then only DFI_WRLVL_WDLY bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRWRLVLDELAY0__DFI_WRLVL_DELAY0__SHIFT       0
#define MEMC_DFITRWRLVLDELAY0__DFI_WRLVL_DELAY0__WIDTH       1
#define MEMC_DFITRWRLVLDELAY0__DFI_WRLVL_DELAY0__MASK        0x00000001
#define MEMC_DFITRWRLVLDELAY0__DFI_WRLVL_DELAY0__INV_MASK    0xFFFFFFFE
#define MEMC_DFITRWRLVLDELAY0__DFI_WRLVL_DELAY0__HW_DEFAULT  0x0

/* DFITRRDLVLDELAY0 */
/* DFI Training dfi_rdlvl_delay Configuration 0 Register */
#define MEMC_DFITRRDLVLDELAY0     0x10800324

/* MEMC_DFITRRDLVLDELAY0.dfi_rdlvl_delay0 - Sets the value to be driven on the signal dfi_rdlvl_delay_x[31:0]. 
Note: The actual width of this register is dependent in DFI_RDLVL_WDLY value. If DFI_RDLVL_WDLY<32, then only DFI_RDLVL_WDLY bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRRDLVLDELAY0__DFI_RDLVL_DELAY0__SHIFT       0
#define MEMC_DFITRRDLVLDELAY0__DFI_RDLVL_DELAY0__WIDTH       1
#define MEMC_DFITRRDLVLDELAY0__DFI_RDLVL_DELAY0__MASK        0x00000001
#define MEMC_DFITRRDLVLDELAY0__DFI_RDLVL_DELAY0__INV_MASK    0xFFFFFFFE
#define MEMC_DFITRRDLVLDELAY0__DFI_RDLVL_DELAY0__HW_DEFAULT  0x0

/* DFITRRDLVLGATEDELAY0 */
/* DFI Training dfi_rdlvl_gate_delay Configuration 0 Register */
#define MEMC_DFITRRDLVLGATEDELAY0 0x10800330

/* MEMC_DFITRRDLVLGATEDELAY0.dfi_rdlvl_gate_delay0 - Sets the value to be driven on the signal dfi_rdlvl_gate_delay_x[31:0]. 
Note: The actual width of this register is dependent in DFI_RDLVL_GATE_WDLY value. If DFI_RDLVL_GATE_WDLY<32, then only DFI_RDLVL_GATE_WDLY bits are valid. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRRDLVLGATEDELAY0__DFI_RDLVL_GATE_DELAY0__SHIFT       0
#define MEMC_DFITRRDLVLGATEDELAY0__DFI_RDLVL_GATE_DELAY0__WIDTH       1
#define MEMC_DFITRRDLVLGATEDELAY0__DFI_RDLVL_GATE_DELAY0__MASK        0x00000001
#define MEMC_DFITRRDLVLGATEDELAY0__DFI_RDLVL_GATE_DELAY0__INV_MASK    0xFFFFFFFE
#define MEMC_DFITRRDLVLGATEDELAY0__DFI_RDLVL_GATE_DELAY0__HW_DEFAULT  0x0

/* DFITRCMD */
/* DFI Training Command */
#define MEMC_DFITRCMD             0x1080033C

/* MEMC_DFITRCMD.dfitrcmd_opcode - DFI Training Command Opcode. Select which DFI Training command to generate for one n_clk cycle: 
2'b00 - dfi_wrlvl_load 
2'b01 - dfi_wrlvl_strobe 
2'b10 - dfi_rdlvl_load 
2'b11 - Reserved. 
<b>Default Value:</b> 2'b00 */
#define MEMC_DFITRCMD__DFITRCMD_OPCODE__SHIFT       0
#define MEMC_DFITRCMD__DFITRCMD_OPCODE__WIDTH       2
#define MEMC_DFITRCMD__DFITRCMD_OPCODE__MASK        0x00000003
#define MEMC_DFITRCMD__DFITRCMD_OPCODE__INV_MASK    0xFFFFFFFC
#define MEMC_DFITRCMD__DFITRCMD_OPCODE__HW_DEFAULT  0x0

/* MEMC_DFITRCMD.dfitrcmd_en - DFI Training Command Enable. Selects which bits of chosen DFI Training command to drive to 1'b1. Default Value: 9'b000000000 */
#define MEMC_DFITRCMD__DFITRCMD_EN__SHIFT       4
#define MEMC_DFITRCMD__DFITRCMD_EN__WIDTH       9
#define MEMC_DFITRCMD__DFITRCMD_EN__MASK        0x00001FF0
#define MEMC_DFITRCMD__DFITRCMD_EN__INV_MASK    0xFFFFE00F
#define MEMC_DFITRCMD__DFITRCMD_EN__HW_DEFAULT  0x0

/* MEMC_DFITRCMD.dfitrcmd_start - DFI Training Command Start. When this bit is set to 1, the command operation defined in the dfitrcmd_opcode field is started. This bit is automatically cleared by the uPCTL after the command is finished. The application can poll this bit to determine when uPCTL is ready to accept another command. This bit cannot be cleared to 1'b0 by software. 
<b>Default Value:</b> 0 */
#define MEMC_DFITRCMD__DFITRCMD_START__SHIFT       31
#define MEMC_DFITRCMD__DFITRCMD_START__WIDTH       1
#define MEMC_DFITRCMD__DFITRCMD_START__MASK        0x80000000
#define MEMC_DFITRCMD__DFITRCMD_START__INV_MASK    0x7FFFFFFF
#define MEMC_DFITRCMD__DFITRCMD_START__HW_DEFAULT  0x0

/* IPVR */
/* IP Version */
#define MEMC_IPVR                 0x108003F8

/* MEMC_IPVR.ip_version - ASCII value for each number in the version followed by a *. For example, 32_30_31_2A represents the version 2.01*. 
<b>Default Value:</b> See the DesignWare Cores SDRAM Universal DDR Memory Controller (uMCTL) Release Notes for the current value of this bit field. */
#define MEMC_IPVR__IP_VERSION__SHIFT       0
#define MEMC_IPVR__IP_VERSION__WIDTH       32
#define MEMC_IPVR__IP_VERSION__MASK        0xFFFFFFFF
#define MEMC_IPVR__IP_VERSION__INV_MASK    0x00000000
#define MEMC_IPVR__IP_VERSION__HW_DEFAULT  0x3236302A

/* IPTR */
/* IP Type */
#define MEMC_IPTR                 0x108003FC

/* MEMC_IPTR.ip_type - Contains the IP's identification code which is an ASCII value to identify the component and it is currently set to the string "DWC". This value never changes. 
<b>Default Value:</b> 32'h44574300 */
#define MEMC_IPTR__IP_TYPE__SHIFT       0
#define MEMC_IPTR__IP_TYPE__WIDTH       32
#define MEMC_IPTR__IP_TYPE__MASK        0xFFFFFFFF
#define MEMC_IPTR__IP_TYPE__INV_MASK    0x00000000
#define MEMC_IPTR__IP_TYPE__HW_DEFAULT  0x44574300


#endif  // __UPCTL_H__
