/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __DDRPHY_H__
#define __DDRPHY_H__



/* DDRPHY */
/* ==================================================================== */

/* Revision Identification Register */
/* The Revision Identification Register returns the revision number of the PHY. (*) The PUB revision number consists of three digits and is normally written in the form [PUBMJR].[PUBMDR][PUBMNR]. (*) The PHY revision number consists of three digits and is normally written in the form [PHYMJR].[PHYMDR][PHYMNR]. The PHY revision default values are dependent upon the DDR3/2 PHY being used and reference the AC/DATX8 components and not the overall PHY release version. The values located in the DWC_DDR3PHY_define.v (as described in "DDR3PHY Top-Level Verilog Parameters" on page 254). (*) The User-Defined Revision Identification (UDRID) field is for general purpose use as defined by the user with the `DWC_UDRID Verilog macro. */
#define MEMC_PHY_RIDR             0x10810000

/* MEMC_PHY_RIDR.PUBMNR - PUB Minor Revision: Indicates minor update of the PUB such as bug fixes. Normally no new features are included. */
#define MEMC_PHY_RIDR__PUBMNR__SHIFT       0
#define MEMC_PHY_RIDR__PUBMNR__WIDTH       4
#define MEMC_PHY_RIDR__PUBMNR__MASK        0x0000000F
#define MEMC_PHY_RIDR__PUBMNR__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_RIDR__PUBMNR__HW_DEFAULT  0x8

/* MEMC_PHY_RIDR.PUBMDR - PUB Moderate Revision: Indicates moderate revision of the PUB such as addition of new features. Normally the new version is still compatible with previous versions. */
#define MEMC_PHY_RIDR__PUBMDR__SHIFT       4
#define MEMC_PHY_RIDR__PUBMDR__WIDTH       4
#define MEMC_PHY_RIDR__PUBMDR__MASK        0x000000F0
#define MEMC_PHY_RIDR__PUBMDR__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_RIDR__PUBMDR__HW_DEFAULT  0x1

/* MEMC_PHY_RIDR.PUBMJR - PUB Major Revision: Indicates major revision of the PUB such addition of the features that make the new version not compatible with previous versions. */
#define MEMC_PHY_RIDR__PUBMJR__SHIFT       8
#define MEMC_PHY_RIDR__PUBMJR__WIDTH       4
#define MEMC_PHY_RIDR__PUBMJR__MASK        0x00000F00
#define MEMC_PHY_RIDR__PUBMJR__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_RIDR__PUBMJR__HW_DEFAULT  0x3

/* MEMC_PHY_RIDR.PHYMNR - PHY Minor Revision: Indicates minor update of the PHY such as bug fixes. Normally no new features are included. */
#define MEMC_PHY_RIDR__PHYMNR__SHIFT       12
#define MEMC_PHY_RIDR__PHYMNR__WIDTH       4
#define MEMC_PHY_RIDR__PHYMNR__MASK        0x0000F000
#define MEMC_PHY_RIDR__PHYMNR__INV_MASK    0xFFFF0FFF
#define MEMC_PHY_RIDR__PHYMNR__HW_DEFAULT  0xB

/* MEMC_PHY_RIDR.PHYMDR - PHY Moderate Revision: Indicates moderate revision of the PHY such as addition of new features. Normally the new version is still compatible with previous versions. */
#define MEMC_PHY_RIDR__PHYMDR__SHIFT       16
#define MEMC_PHY_RIDR__PHYMDR__WIDTH       4
#define MEMC_PHY_RIDR__PHYMDR__MASK        0x000F0000
#define MEMC_PHY_RIDR__PHYMDR__INV_MASK    0xFFF0FFFF
#define MEMC_PHY_RIDR__PHYMDR__HW_DEFAULT  0x4

/* MEMC_PHY_RIDR.PHYMJR - PHY Major Revision: Indicates major revision of the PHY such addition of the features that make the new version not compatible with previous versions. */
#define MEMC_PHY_RIDR__PHYMJR__SHIFT       20
#define MEMC_PHY_RIDR__PHYMJR__WIDTH       4
#define MEMC_PHY_RIDR__PHYMJR__MASK        0x00F00000
#define MEMC_PHY_RIDR__PHYMJR__INV_MASK    0xFF0FFFFF
#define MEMC_PHY_RIDR__PHYMJR__HW_DEFAULT  0x1

/* MEMC_PHY_RIDR.UDRID - User-Defined Revision ID: General purpose revision identification set by the user. */
#define MEMC_PHY_RIDR__UDRID__SHIFT       24
#define MEMC_PHY_RIDR__UDRID__WIDTH       8
#define MEMC_PHY_RIDR__UDRID__MASK        0xFF000000
#define MEMC_PHY_RIDR__UDRID__INV_MASK    0x00FFFFFF
#define MEMC_PHY_RIDR__UDRID__HW_DEFAULT  0x0

/* PHY Initialization Register */
/* PHY Initialization Register is used to configure and control the initialization of the PHY. This includes the triggering of certain initialization routines, as well as the reset of the PHY and/or the PLLs used in the PHY. Any PIR register bit that is used to trigger an initialization routine (bits 0 to 15) is self clearing and will be set to 0 once the initialization is done. Any configuration register write that sets the PIR[INIT] register bit will trigger initialization as selected by the other PIR register bits. The completion status of this initialization can be checked by polling the "PHY General Status Registers 0-1 (PGSR0-1)" on page 81. Note that PGSR0[IDONE] is not cleared immediately after PIR[INIT] is set, and oftware must wait a minimum of 20 configuration clock cycles from when PIR[INIT] is set to when it starts polling for PGSR0[IDONE]. Note The PGSR0 register must be polled to see that the IDONE is set before writing another command to the PUB's PIR register. */
#define MEMC_PHY_PIR              0x10810004

/* MEMC_PHY_PIR.INIT - Initialization Trigger: A write of '1' to this bit triggers the DDR system initialization, including PHY initialization, DRAM initialization, and PHY training. The exact initialization steps to be executed are specified in bits 1 to 15 of this register. A bit setting of 1 means the step will be executed as part of the initialization sequence, while a setting of '0' means the step will be bypassed. The initialization trigger bit is self-clearing. */
#define MEMC_PHY_PIR__INIT__SHIFT       0
#define MEMC_PHY_PIR__INIT__WIDTH       1
#define MEMC_PHY_PIR__INIT__MASK        0x00000001
#define MEMC_PHY_PIR__INIT__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_PIR__INIT__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.ZCAL - Impedance Calibration: Performs PHY impedance calibration. When set the impedance calibration will be performed in parallel with PHY initialization (PLL initialization + DDL calibration + PHY reset). */
#define MEMC_PHY_PIR__ZCAL__SHIFT       1
#define MEMC_PHY_PIR__ZCAL__WIDTH       1
#define MEMC_PHY_PIR__ZCAL__MASK        0x00000002
#define MEMC_PHY_PIR__ZCAL__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_PIR__ZCAL__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.PLLINIT - PLL Initialization: Executes the PLL initialization sequence which includes correct driving of PLL power-down, reset and gear shift pins, and then waiting for the PHY PLLs to lock. */
#define MEMC_PHY_PIR__PLLINIT__SHIFT       4
#define MEMC_PHY_PIR__PLLINIT__WIDTH       1
#define MEMC_PHY_PIR__PLLINIT__MASK        0x00000010
#define MEMC_PHY_PIR__PLLINIT__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_PIR__PLLINIT__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.DCAL - Digital Delay Line (DDL) Calibration: Performs PHY delay line calibration. */
#define MEMC_PHY_PIR__DCAL__SHIFT       5
#define MEMC_PHY_PIR__DCAL__WIDTH       1
#define MEMC_PHY_PIR__DCAL__MASK        0x00000020
#define MEMC_PHY_PIR__DCAL__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_PIR__DCAL__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.PHYRST - PHY Reset: Resets the AC and DATX8 modules by asserting the AC/DATX8 reset pin. */
#define MEMC_PHY_PIR__PHYRST__SHIFT       6
#define MEMC_PHY_PIR__PHYRST__WIDTH       1
#define MEMC_PHY_PIR__PHYRST__MASK        0x00000040
#define MEMC_PHY_PIR__PHYRST__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_PIR__PHYRST__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.DRAMRST - DRAM Reset (DDR3 Only): Issues a reset to the DRAM (by driving the DRAM reset pin low) and wait 200us. This can be triggered in isolation or with the full DRAM initialization (DRAMINIT). For the later case, the reset is issued and 200us is waited before starting the full initialization sequence. */
#define MEMC_PHY_PIR__DRAMRST__SHIFT       7
#define MEMC_PHY_PIR__DRAMRST__WIDTH       1
#define MEMC_PHY_PIR__DRAMRST__MASK        0x00000080
#define MEMC_PHY_PIR__DRAMRST__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_PIR__DRAMRST__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.DRAMINIT - DRAM Initialization: Executes the DRAM initialization sequence. */
#define MEMC_PHY_PIR__DRAMINIT__SHIFT       8
#define MEMC_PHY_PIR__DRAMINIT__WIDTH       1
#define MEMC_PHY_PIR__DRAMINIT__MASK        0x00000100
#define MEMC_PHY_PIR__DRAMINIT__INV_MASK    0xFFFFFEFF
#define MEMC_PHY_PIR__DRAMINIT__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.WL - Write Leveling (DDR3 Only): Executes a PUB write leveling routine. */
#define MEMC_PHY_PIR__WL__SHIFT       9
#define MEMC_PHY_PIR__WL__WIDTH       1
#define MEMC_PHY_PIR__WL__MASK        0x00000200
#define MEMC_PHY_PIR__WL__INV_MASK    0xFFFFFDFF
#define MEMC_PHY_PIR__WL__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.QSGATE - Read DQS Gate Training: Executes a PUB training routine to determine the optimum position of the read data DQS strobe for maximum system timing margins. */
#define MEMC_PHY_PIR__QSGATE__SHIFT       10
#define MEMC_PHY_PIR__QSGATE__WIDTH       1
#define MEMC_PHY_PIR__QSGATE__MASK        0x00000400
#define MEMC_PHY_PIR__QSGATE__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_PIR__QSGATE__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.WLADJ - Write Leveling Adjust (DDR3 Only): Executes a PUB training routine that readjusts the write latency used during write in case the write leveling routine changed the expected latency. */
#define MEMC_PHY_PIR__WLADJ__SHIFT       11
#define MEMC_PHY_PIR__WLADJ__WIDTH       1
#define MEMC_PHY_PIR__WLADJ__MASK        0x00000800
#define MEMC_PHY_PIR__WLADJ__INV_MASK    0xFFFFF7FF
#define MEMC_PHY_PIR__WLADJ__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.RDDSKW - Read Data Bit Deskew: Executes a PUB training routine to deskew the DQ bits during read. */
#define MEMC_PHY_PIR__RDDSKW__SHIFT       12
#define MEMC_PHY_PIR__RDDSKW__WIDTH       1
#define MEMC_PHY_PIR__RDDSKW__MASK        0x00001000
#define MEMC_PHY_PIR__RDDSKW__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_PIR__RDDSKW__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.WRDSKW - Write Data Bit Deskew: Executes a PUB training routine to deskew the DQ bits during write. */
#define MEMC_PHY_PIR__WRDSKW__SHIFT       13
#define MEMC_PHY_PIR__WRDSKW__WIDTH       1
#define MEMC_PHY_PIR__WRDSKW__MASK        0x00002000
#define MEMC_PHY_PIR__WRDSKW__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_PIR__WRDSKW__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.RDEYE - Read Data Eye Training: Executes a PUB training routine to maximize the read data eye. */
#define MEMC_PHY_PIR__RDEYE__SHIFT       14
#define MEMC_PHY_PIR__RDEYE__WIDTH       1
#define MEMC_PHY_PIR__RDEYE__MASK        0x00004000
#define MEMC_PHY_PIR__RDEYE__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_PIR__RDEYE__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.WREYE - Write Data Eye Training: Executes a PUB training routine to maximize the write data eye. */
#define MEMC_PHY_PIR__WREYE__SHIFT       15
#define MEMC_PHY_PIR__WREYE__WIDTH       1
#define MEMC_PHY_PIR__WREYE__MASK        0x00008000
#define MEMC_PHY_PIR__WREYE__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_PIR__WREYE__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.ICPC - Initialization Complete Pin Configuration: Specifies how the DFI initialization complete output pin (dfi_init_complete) should be used to indicate the status of initialization. Valid value are: 0 = Asserted after PHY initialization (DLL locking and impedance calibration) is complete. 1 = Asserted after PHY initialization is complete and the triggered the PUB initialization (DRAM initialization, data training, or initialization trigger with no selected initialization) is complete. */
#define MEMC_PHY_PIR__ICPC__SHIFT       16
#define MEMC_PHY_PIR__ICPC__WIDTH       1
#define MEMC_PHY_PIR__ICPC__MASK        0x00010000
#define MEMC_PHY_PIR__ICPC__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_PIR__ICPC__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.PLLBYP - PLL Bypass: A setting of 1 on this bit will put all PHY PLLs in bypass mode. */
#define MEMC_PHY_PIR__PLLBYP__SHIFT       17
#define MEMC_PHY_PIR__PLLBYP__WIDTH       1
#define MEMC_PHY_PIR__PLLBYP__MASK        0x00020000
#define MEMC_PHY_PIR__PLLBYP__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_PIR__PLLBYP__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.CTLDINIT - Controller DRAM Initialization: Indicates if set that DRAM initialization will be performed by the controller. Otherwise if not set it indicates that DRAM initialization will be performed using the built-in initialization sequence or using software through the configuration port. */
#define MEMC_PHY_PIR__CTLDINIT__SHIFT       18
#define MEMC_PHY_PIR__CTLDINIT__WIDTH       1
#define MEMC_PHY_PIR__CTLDINIT__MASK        0x00040000
#define MEMC_PHY_PIR__CTLDINIT__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_PIR__CTLDINIT__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.RDIMMINIT - RDIMM Initialization: Executes the RDIMM buffer chip initialization before executing DRAM initialization. The RDIMM buffer chip initialization is run after the DRAM is reset and CKE have been driven high by the DRAM initialization sequence. */
#define MEMC_PHY_PIR__RDIMMINIT__SHIFT       19
#define MEMC_PHY_PIR__RDIMMINIT__WIDTH       1
#define MEMC_PHY_PIR__RDIMMINIT__MASK        0x00080000
#define MEMC_PHY_PIR__RDIMMINIT__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_PIR__RDIMMINIT__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.CLRSR - Clear Status Registers: A write of '1' to this bit will clear (reset to '0') all status registers, including PGSR and DXnGSR. The clear status register bit is selfclearing. Note, this bit does not clear the PGSR.IDONE bit. If the IDONE bit is set it remains at 1'b1 to indicate the PUB has competed its task. This bit is primarily for debug purposes and is typically not needed during normal functional operation. It can be used when PGSR.IDONE=1, to manually clear the PGSR status bits, although starting a new init process will automatically clear the PGSR status bits. Or it can be used to manually clear the DXnGSR status bits, although starting a new data training process will automatically clear the DXnGSR status bits. The following lists the impacted registers when PIR [27] (CLRSR) = 1'b1: (*) Only PGSR0 is affected by PIR[27] (CLRSR) - It will not clear any bits in PGSR1 or DXnGSR0-1 (*) The following bits are not cleared by PIR[27] (CLRSR): - PGSR0[31] (APLOCK) - PGSR0[30:28] (DTERR) - PGSR0[21] (WLERR) - PGSR0[19] (ZCERR) - PGSR0[2] (DCDONE) - PGSR0[1] (PLDONE) - PGSR0[0] (IDONE) (*) The following bits will always be zero: - PGSR0[20] (DIERR) - PGSR0[19] (ZCERR) - PGSR0[18] (DCERR) - PGSR0[17] (PLERR) - PGSR0[16] (IERR) (*) The following bits are cleared unconditionally by PIR[27] (CLRSR): - PGSR0[27] (WEERR) - PGSR0[26] (REERR) - PGSR0[25] (WDERR) - PGSR0[24] (RDERR) - PGSR0[23] (WLAERR) - PGSR0[22] (QSGERR) - PGSR0[11] (WEDONE) - PGSR0[10] (REDONE) - PGSR0[9] (WDDONE) - PGSR0[8] (RDDONE) - PGSR0[7] (WLADONE) - PGSR0[6] (QSGDONE) - PGSR0[5] (WLDONE) (*) The following bits are cleared conditionally by PIR[27] (CLRSR): - PGSR0[4] (DIDONE):Will be cleared when PIR[8](DRAMINIT) = 1'b1 - PGSR0[3] (ZCDONE): Will be cleared when PIR[1](ZCAL) = 1'b1 */
#define MEMC_PHY_PIR__CLRSR__SHIFT       27
#define MEMC_PHY_PIR__CLRSR__WIDTH       1
#define MEMC_PHY_PIR__CLRSR__MASK        0x08000000
#define MEMC_PHY_PIR__CLRSR__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_PIR__CLRSR__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.LOCKBYP - PLL Lock Bypass: Bypasses or stops, if set, the waiting of PLLs to lock. PLL lock wait is automatically triggered after reset. PLL lock wait may be triggered manually using INIT and PLLINIT bits of the PIR register. This bit is self-clearing. */
#define MEMC_PHY_PIR__LOCKBYP__SHIFT       28
#define MEMC_PHY_PIR__LOCKBYP__WIDTH       1
#define MEMC_PHY_PIR__LOCKBYP__MASK        0x10000000
#define MEMC_PHY_PIR__LOCKBYP__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_PIR__LOCKBYP__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.DCALBYP - Digital Delay Line (DDL) Calibration Bypass: Bypasses or stops, if set, DDL calibration that automatically triggers after reset. DDL calibration may be triggered manually using INIT and DCAL bits of the PIR register. This bit is selfclearing. */
#define MEMC_PHY_PIR__DCALBYP__SHIFT       29
#define MEMC_PHY_PIR__DCALBYP__WIDTH       1
#define MEMC_PHY_PIR__DCALBYP__MASK        0x20000000
#define MEMC_PHY_PIR__DCALBYP__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_PIR__DCALBYP__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.ZCALBYP - Impedance Calibration Bypass: Bypasses or stops, if set, impedance calibration of all ZQ control blocks that automatically triggers after reset. Impedance calibration may be triggered manually using INIT and ZCAL bits of the PIR register. This bit is self-clearing. */
#define MEMC_PHY_PIR__ZCALBYP__SHIFT       30
#define MEMC_PHY_PIR__ZCALBYP__WIDTH       1
#define MEMC_PHY_PIR__ZCALBYP__MASK        0x40000000
#define MEMC_PHY_PIR__ZCALBYP__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_PIR__ZCALBYP__HW_DEFAULT  0x0

/* MEMC_PHY_PIR.INITBYP - Initialization Bypass: Bypasses or stops, if set, all initialization routines currently running, including PHY initialization, DRAM initialization, and PHY training. Initialization may be triggered manually using INIT and the other relevant bits of the PIR register. This bit is self-clearing. */
#define MEMC_PHY_PIR__INITBYP__SHIFT       31
#define MEMC_PHY_PIR__INITBYP__WIDTH       1
#define MEMC_PHY_PIR__INITBYP__MASK        0x80000000
#define MEMC_PHY_PIR__INITBYP__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PIR__INITBYP__HW_DEFAULT  0x0

/* PHY General Configuration Register 0 */
/* These registers are used for miscellaneous PHY configurations such as enabling VT drift compensation and setting up write-leveling. */
#define MEMC_PHY_PGCR0            0x10810008

/* MEMC_PHY_PGCR0.WLLVT - Write Leveling LCDL Delay VT Compensation: Enables, if set, the VT drift compensation of the write leveling LCDL. */
#define MEMC_PHY_PGCR0__WLLVT__SHIFT       0
#define MEMC_PHY_PGCR0__WLLVT__WIDTH       1
#define MEMC_PHY_PGCR0__WLLVT__MASK        0x00000001
#define MEMC_PHY_PGCR0__WLLVT__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_PGCR0__WLLVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.WDLVT - Write DQ LCDL Delay VT Compensation: Enables, if set the VT drift compensation of the write DQ LCDL. */
#define MEMC_PHY_PGCR0__WDLVT__SHIFT       1
#define MEMC_PHY_PGCR0__WDLVT__WIDTH       1
#define MEMC_PHY_PGCR0__WDLVT__MASK        0x00000002
#define MEMC_PHY_PGCR0__WDLVT__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_PGCR0__WDLVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.RDLVT - Read DQS LCDL Delay VT Compensation: Enables, if set the VT drift compensation of the read DQS LCDL. */
#define MEMC_PHY_PGCR0__RDLVT__SHIFT       2
#define MEMC_PHY_PGCR0__RDLVT__WIDTH       1
#define MEMC_PHY_PGCR0__RDLVT__MASK        0x00000004
#define MEMC_PHY_PGCR0__RDLVT__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_PGCR0__RDLVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.RGLVT - Read DQS Gating LCDL Delay VT Compensation: Enables, if set the VT drift compensation of the read DQS gating LCDL. */
#define MEMC_PHY_PGCR0__RGLVT__SHIFT       3
#define MEMC_PHY_PGCR0__RGLVT__WIDTH       1
#define MEMC_PHY_PGCR0__RGLVT__MASK        0x00000008
#define MEMC_PHY_PGCR0__RGLVT__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_PGCR0__RGLVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.WDBVT - Write Data BDL VT Compensation: Enables, if set the VT drift compensation of the write data bit delay lines. */
#define MEMC_PHY_PGCR0__WDBVT__SHIFT       4
#define MEMC_PHY_PGCR0__WDBVT__WIDTH       1
#define MEMC_PHY_PGCR0__WDBVT__MASK        0x00000010
#define MEMC_PHY_PGCR0__WDBVT__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_PGCR0__WDBVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.RDBVT - Read Data BDL VT Compensation: Enables, if set the VT drift compensation of the read data bit delay lines. */
#define MEMC_PHY_PGCR0__RDBVT__SHIFT       5
#define MEMC_PHY_PGCR0__RDBVT__WIDTH       1
#define MEMC_PHY_PGCR0__RDBVT__MASK        0x00000020
#define MEMC_PHY_PGCR0__RDBVT__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_PGCR0__RDBVT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR0.DLTMODE - Delay Line Test Mode: Selects, if set, the delay line oscillator test mode. */
#define MEMC_PHY_PGCR0__DLTMODE__SHIFT       6
#define MEMC_PHY_PGCR0__DLTMODE__WIDTH       1
#define MEMC_PHY_PGCR0__DLTMODE__MASK        0x00000040
#define MEMC_PHY_PGCR0__DLTMODE__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_PGCR0__DLTMODE__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR0.DLTST - Delay Line Test Start: A write of '1' to this bit will trigger delay line oscillator mode period measurement. This bit is not self clearing and needs to be reset to '0' before the measurement can be re-triggered. */
#define MEMC_PHY_PGCR0__DLTST__SHIFT       7
#define MEMC_PHY_PGCR0__DLTST__WIDTH       1
#define MEMC_PHY_PGCR0__DLTST__MASK        0x00000080
#define MEMC_PHY_PGCR0__DLTST__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_PGCR0__DLTST__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR0.OSCEN - Oscillator Enable: Enables, if set, the delay line oscillation. */
#define MEMC_PHY_PGCR0__OSCEN__SHIFT       8
#define MEMC_PHY_PGCR0__OSCEN__WIDTH       1
#define MEMC_PHY_PGCR0__OSCEN__MASK        0x00000100
#define MEMC_PHY_PGCR0__OSCEN__INV_MASK    0xFFFFFEFF
#define MEMC_PHY_PGCR0__OSCEN__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR0.OSCDIV - Oscillator Mode Division: Specifies the factor by which the delay line oscillator mode output is divided down before it is output on the delay line digital test output pin dl_dto. Valid values are: 000 = Divide by 1 001 = Divide by 256 010 = Divide by 512 011 = Divide by 1024 100 = Divide by 2048 101 = Divide by 4096 110 = Divide by 8192 111 = Divide by 65536 */
#define MEMC_PHY_PGCR0__OSCDIV__SHIFT       9
#define MEMC_PHY_PGCR0__OSCDIV__WIDTH       3
#define MEMC_PHY_PGCR0__OSCDIV__MASK        0x00000E00
#define MEMC_PHY_PGCR0__OSCDIV__INV_MASK    0xFFFFF1FF
#define MEMC_PHY_PGCR0__OSCDIV__HW_DEFAULT  0x7

/* MEMC_PHY_PGCR0.OSCWDL - Oscillator Mode Write-Leveling Delay Line Select: Selects which of the two write leveling LCDLs is active. The delay select value of the inactive LCDL is set to zero while the delay select value of the active LCDL can be varied by the input write leveling delay select pin. Valid values are: 00 = No WL LCDL is active 01 = DDR WL LCDL is active 10 = SDR WL LCDL is active 11 = Both LCDLs are active */
#define MEMC_PHY_PGCR0__OSCWDL__SHIFT       12
#define MEMC_PHY_PGCR0__OSCWDL__WIDTH       2
#define MEMC_PHY_PGCR0__OSCWDL__MASK        0x00003000
#define MEMC_PHY_PGCR0__OSCWDL__INV_MASK    0xFFFFCFFF
#define MEMC_PHY_PGCR0__OSCWDL__HW_DEFAULT  0x3

/* MEMC_PHY_PGCR0.DTOSEL - Digital Test Output Select: Selects the PHY digital test output that should be driven onto PHY digital test output (phy_dto) pin: Valid values are: 00000 = DATX8 0 PLL digital test output 00001 = DATX8 1 PLL digital test output 00010 = DATX8 2 PLL digital test output 00011 = DATX8 3 PLL digital test output 00100 = DATX8 4 PLL digital test output 00101 = DATX8 5 PLL digital test output 00110 = DATX8 6 PLL digital test output 00111 = DATX8 7 PLL digital test output 01000 = DATX8 8 PLL digital test output 01001 = AC PLL digital test output 01010 - 01111 = Reserved 10000 = DATX8 0 delay line digital test output 10001 = DATX8 1 delay line digital test output 10010 = DATX8 2 delay line digital test output 10011 = DATX8 3 delay line digital test output 10100 = DATX8 4 delay line digital test output 10101 = DATX8 5 delay line digital test output 10110 = DATX8 6 delay line digital test output 10111 = DATX8 7 delay line digital test output 11000 = DATX8 8 delay line digital test output 11001 = AC delay line digital test output 11010 - 11111 = Reserved */
#define MEMC_PHY_PGCR0__DTOSEL__SHIFT       14
#define MEMC_PHY_PGCR0__DTOSEL__WIDTH       5
#define MEMC_PHY_PGCR0__DTOSEL__MASK        0x0007C000
#define MEMC_PHY_PGCR0__DTOSEL__INV_MASK    0xFFF83FFF
#define MEMC_PHY_PGCR0__DTOSEL__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR0.PUBMODE - Enables, if set, the PUB to control the interface to the PHY and SDRAM. In this mode the DFI commands from the controller are ignored. The bit must be set to 0 after the system determines it is convenient to pass control of the DFI bus to the controller. When set to 0 the DFI interface has control of the PHY and SDRAM interface except when triggering pub operations such as BIST, DCU or data training. */
#define MEMC_PHY_PGCR0__PUBMODE__SHIFT       25
#define MEMC_PHY_PGCR0__PUBMODE__WIDTH       1
#define MEMC_PHY_PGCR0__PUBMODE__MASK        0x02000000
#define MEMC_PHY_PGCR0__PUBMODE__INV_MASK    0xFDFFFFFF
#define MEMC_PHY_PGCR0__PUBMODE__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR0.CKEN - CK Enable: Controls whether the CK going to the SDRAM is enabled (toggling) or disabled (static value) and whether the CK is inverted. Two bits for each of the up to three CK pairs. Valid values for the two bits are: 00 = CK disabled (Driven to constant 0) 01 = CK toggling with inverted polarity 10 = CK toggling with normal polarity (This should be the default setting) 11 = CK disabled (Driven to constant 1) */
#define MEMC_PHY_PGCR0__CKEN__SHIFT       26
#define MEMC_PHY_PGCR0__CKEN__WIDTH       6
#define MEMC_PHY_PGCR0__CKEN__MASK        0xFC000000
#define MEMC_PHY_PGCR0__CKEN__INV_MASK    0x03FFFFFF
#define MEMC_PHY_PGCR0__CKEN__HW_DEFAULT  0x2A

/* PHY General Configuration Register 1 */
/* These registers are used for miscellaneous PHY configurations such as enabling VT drift compensation and setting up write-leveling. */
#define MEMC_PHY_PGCR1            0x1081000C

/* MEMC_PHY_PGCR1.PDDISDX - Power Down Disabled Byte: Indicates if set that the PLL and I/Os of a disabled byte should be powered down. */
#define MEMC_PHY_PGCR1__PDDISDX__SHIFT       0
#define MEMC_PHY_PGCR1__PDDISDX__WIDTH       1
#define MEMC_PHY_PGCR1__PDDISDX__MASK        0x00000001
#define MEMC_PHY_PGCR1__PDDISDX__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_PGCR1__PDDISDX__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.WLMODE - Write Leveling (Software) Mode: Indicates if set that the PUB is in software write leveling mode in which software executes single steps of DQS pulsing by writing '1' to PIR.WL. The write leveling DQ status from the DRAM is captured in DXnGSR0.WLDQ. */
#define MEMC_PHY_PGCR1__WLMODE__SHIFT       1
#define MEMC_PHY_PGCR1__WLMODE__WIDTH       1
#define MEMC_PHY_PGCR1__WLMODE__MASK        0x00000002
#define MEMC_PHY_PGCR1__WLMODE__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_PGCR1__WLMODE__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.WLSTEP - Write Leveling Step: Specifies the number of delay step-size increments during each step of write leveling. Valid values are: 0 = computed to be . of the associated lane's DXnGSR0.WLPRD value 1 = 1 step size */
#define MEMC_PHY_PGCR1__WLSTEP__SHIFT       2
#define MEMC_PHY_PGCR1__WLSTEP__WIDTH       1
#define MEMC_PHY_PGCR1__WLSTEP__MASK        0x00000004
#define MEMC_PHY_PGCR1__WLSTEP__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_PGCR1__WLSTEP__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.WSLOPT - Write System Latency Optimization: controls the insertion of a pipeline stage on the AC signals from the DFI interface to the PHY to cater for a negative write system latency (WSL) value (only -1 possible). 0x0 = A pipeline stage is inserted only if WL2 training results in a WSL of -1 for any rank 0x1 = Inserts a pipeline stage */
#define MEMC_PHY_PGCR1__WSLOPT__SHIFT       4
#define MEMC_PHY_PGCR1__WSLOPT__WIDTH       1
#define MEMC_PHY_PGCR1__WSLOPT__MASK        0x00000010
#define MEMC_PHY_PGCR1__WSLOPT__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_PGCR1__WSLOPT__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.ACHRST - AC PHY High-Speed Reset: a Write of '0' to this bit resets the AC macro without resetting the PUB RTL logic. This bit is not self-clearing and a '1' must be written to de-assert the reset. */
#define MEMC_PHY_PGCR1__ACHRST__SHIFT       5
#define MEMC_PHY_PGCR1__ACHRST__WIDTH       1
#define MEMC_PHY_PGCR1__ACHRST__MASK        0x00000020
#define MEMC_PHY_PGCR1__ACHRST__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_PGCR1__ACHRST__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.WLSELT - Write Leveling Select Type: Selects the encoding type for the write leveling select signal depending on the desired setup/hold margins for the internal pipelines. Refer to the DDR PHY Databook for details of how the select type is used. Valid values are: 0 = Type 1: Setup margin of 90 degrees and hold margin of 90 degrees 1 = Type 2: Setup margin of 135 degrees and hold margin of 45 degrees */
#define MEMC_PHY_PGCR1__WLSELT__SHIFT       6
#define MEMC_PHY_PGCR1__WLSELT__WIDTH       1
#define MEMC_PHY_PGCR1__WLSELT__MASK        0x00000040
#define MEMC_PHY_PGCR1__WLSELT__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_PGCR1__WLSELT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.MDLEN - Master Delay Line Enable: Enables, if set, the AC master delay line calibration to perform subsequent period measurements following the initial period measurements that are performed after reset or on when calibration is manually triggered. These additional measurements are accumulated and filtered as long as this bit remains high. */
#define MEMC_PHY_PGCR1__MDLEN__SHIFT       9
#define MEMC_PHY_PGCR1__MDLEN__WIDTH       1
#define MEMC_PHY_PGCR1__MDLEN__MASK        0x00000200
#define MEMC_PHY_PGCR1__MDLEN__INV_MASK    0xFFFFFDFF
#define MEMC_PHY_PGCR1__MDLEN__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.LPFEN - Low-Pass Filter Enable: Enables, if set, the low pass filtering of MDL period measurements. */
#define MEMC_PHY_PGCR1__LPFEN__SHIFT       10
#define MEMC_PHY_PGCR1__LPFEN__WIDTH       1
#define MEMC_PHY_PGCR1__LPFEN__MASK        0x00000400
#define MEMC_PHY_PGCR1__LPFEN__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_PGCR1__LPFEN__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.LPFDEPTH - Low-Pass Filter Depth: Specifies the number of measurements over which MDL period measurements are filtered. This determines the time constant of the low pass filter. Valid values are: 00 = 2 01 = 4 10 = 8 11 = 16 */
#define MEMC_PHY_PGCR1__LPFDEPTH__SHIFT       11
#define MEMC_PHY_PGCR1__LPFDEPTH__WIDTH       2
#define MEMC_PHY_PGCR1__LPFDEPTH__MASK        0x00001800
#define MEMC_PHY_PGCR1__LPFDEPTH__INV_MASK    0xFFFFE7FF
#define MEMC_PHY_PGCR1__LPFDEPTH__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.FDEPTH - Filter Depth: Specifies the number of measurements over which all AC and DATX8 initial period measurements, that happen after reset or when calibration is manually triggered, are averaged. Valid values are: 00 = 2 01 = 4 10 = 8 11 = 16 */
#define MEMC_PHY_PGCR1__FDEPTH__SHIFT       13
#define MEMC_PHY_PGCR1__FDEPTH__WIDTH       2
#define MEMC_PHY_PGCR1__FDEPTH__MASK        0x00006000
#define MEMC_PHY_PGCR1__FDEPTH__INV_MASK    0xFFFF9FFF
#define MEMC_PHY_PGCR1__FDEPTH__HW_DEFAULT  0x2

/* MEMC_PHY_PGCR1.DLDLMT - Delay Line VT Drift Limit: Specifies the minimum change in the delay line VT drift in one direction which should result in the assertion of the delay line VT drift status signal (vt_drift). The limit is specified in terms of delay select values. A value of 0 disables the assertion of delay line VT drift status signal. */
#define MEMC_PHY_PGCR1__DLDLMT__SHIFT       15
#define MEMC_PHY_PGCR1__DLDLMT__WIDTH       8
#define MEMC_PHY_PGCR1__DLDLMT__MASK        0x007F8000
#define MEMC_PHY_PGCR1__DLDLMT__INV_MASK    0xFF807FFF
#define MEMC_PHY_PGCR1__DLDLMT__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.ZCKSEL - Impedance Clock Divider Select: Selects the divide ratio for the clock used by the impedance control logic relative to the clock used by the memory controller and SDRAM. Valid values are: 00 = Divide by 2 01 = Divide by 8 10 = Divide by 32 11 = Divide by 64 For more information, refer to "Impedance Calibration" on page 164. */
#define MEMC_PHY_PGCR1__ZCKSEL__SHIFT       23
#define MEMC_PHY_PGCR1__ZCKSEL__WIDTH       2
#define MEMC_PHY_PGCR1__ZCKSEL__MASK        0x01800000
#define MEMC_PHY_PGCR1__ZCKSEL__INV_MASK    0xFE7FFFFF
#define MEMC_PHY_PGCR1__ZCKSEL__HW_DEFAULT  0x2

/* MEMC_PHY_PGCR1.DXHRST - DX PHY High-Speed Reset: a Write of '0' to this bit resets the DX macro without resetting the PUB RTL logic. This bit is not self-clearing and a '1' must be written to de-assert the reset. */
#define MEMC_PHY_PGCR1__DXHRST__SHIFT       25
#define MEMC_PHY_PGCR1__DXHRST__WIDTH       1
#define MEMC_PHY_PGCR1__DXHRST__MASK        0x02000000
#define MEMC_PHY_PGCR1__DXHRST__INV_MASK    0xFDFFFFFF
#define MEMC_PHY_PGCR1__DXHRST__HW_DEFAULT  0x1

/* MEMC_PHY_PGCR1.INHVT - VT Calculation Inhibit: Inhibits calculation of the next VT compensated delay line values. A value of 1 will inhibit the VT calculation. This bit should be set to 1 during writes to the delay line registers. */
#define MEMC_PHY_PGCR1__INHVT__SHIFT       26
#define MEMC_PHY_PGCR1__INHVT__WIDTH       1
#define MEMC_PHY_PGCR1__INHVT__MASK        0x04000000
#define MEMC_PHY_PGCR1__INHVT__INV_MASK    0xFBFFFFFF
#define MEMC_PHY_PGCR1__INHVT__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.IOLB - I/O Loop-Back Select: Selects where inside the I/O the loop-back of signals happens. Valid values are: 0 = Loopback is after output buffer; output enable must be asserted 1 = Loopback is before output buffer; output enable is don't care */
#define MEMC_PHY_PGCR1__IOLB__SHIFT       27
#define MEMC_PHY_PGCR1__IOLB__WIDTH       1
#define MEMC_PHY_PGCR1__IOLB__MASK        0x08000000
#define MEMC_PHY_PGCR1__IOLB__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_PGCR1__IOLB__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.LBDQSS - Loopback DQS Shift: Selects how the read DQS is shifted during loopback to ensure that the read DQS is centered into the read data eye. Valid values are: 0 = PUB sets the read DQS LCDL to 0; DQS is already shifted 90 degrees by write path 1 = The read DQS shift is set manually through software */
#define MEMC_PHY_PGCR1__LBDQSS__SHIFT       28
#define MEMC_PHY_PGCR1__LBDQSS__WIDTH       1
#define MEMC_PHY_PGCR1__LBDQSS__MASK        0x10000000
#define MEMC_PHY_PGCR1__LBDQSS__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_PGCR1__LBDQSS__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.LBGDQS - Loopback DQS Gating: Selects the DQS gating mode that should be used when the PHY is in loopback mode, including BIST loopback mode. Valid values are: 00 = DQS gate is always on 01 = DQS gate training will be triggered on the PUB 10 = DQS gate is set manually using software 11 = Reserved */
#define MEMC_PHY_PGCR1__LBGDQS__SHIFT       29
#define MEMC_PHY_PGCR1__LBGDQS__WIDTH       2
#define MEMC_PHY_PGCR1__LBGDQS__MASK        0x60000000
#define MEMC_PHY_PGCR1__LBGDQS__INV_MASK    0x9FFFFFFF
#define MEMC_PHY_PGCR1__LBGDQS__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR1.LBMODE - Loopback Mode: Indicates, if set, that the PHY/PUB is in loopback mode. */
#define MEMC_PHY_PGCR1__LBMODE__SHIFT       31
#define MEMC_PHY_PGCR1__LBMODE__WIDTH       1
#define MEMC_PHY_PGCR1__LBMODE__MASK        0x80000000
#define MEMC_PHY_PGCR1__LBMODE__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PGCR1__LBMODE__HW_DEFAULT  0x0

/* PHY General Status Register 0 */
/* These are general status registers for the PHY. They indicate, among other things, whether initialization, write leveling or period measurement calibrations are done. The following tables describe the bits of the PHY GSR registers. */
#define MEMC_PHY_PGSR0            0x10810010

/* MEMC_PHY_PGSR0.IDONE - Initialization Done: Indicates if set that the DDR system initialization has completed. This bit is set after all the selected initialization routines in PIR register have completed. */
#define MEMC_PHY_PGSR0__IDONE__SHIFT       0
#define MEMC_PHY_PGSR0__IDONE__WIDTH       1
#define MEMC_PHY_PGSR0__IDONE__MASK        0x00000001
#define MEMC_PHY_PGSR0__IDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_PGSR0__IDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.PLDONE - PLL Lock Done: Indicates if set that PLL locking has completed. */
#define MEMC_PHY_PGSR0__PLDONE__SHIFT       1
#define MEMC_PHY_PGSR0__PLDONE__WIDTH       1
#define MEMC_PHY_PGSR0__PLDONE__MASK        0x00000002
#define MEMC_PHY_PGSR0__PLDONE__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_PGSR0__PLDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.DCDONE - Digital Delay Line (DDL) Calibration Done: Indicates if set that DDL calibration has completed. */
#define MEMC_PHY_PGSR0__DCDONE__SHIFT       2
#define MEMC_PHY_PGSR0__DCDONE__WIDTH       1
#define MEMC_PHY_PGSR0__DCDONE__MASK        0x00000004
#define MEMC_PHY_PGSR0__DCDONE__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_PGSR0__DCDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.ZCDONE - Impedance Calibration Done: Indicates if set that impedance calibration has completed. */
#define MEMC_PHY_PGSR0__ZCDONE__SHIFT       3
#define MEMC_PHY_PGSR0__ZCDONE__WIDTH       1
#define MEMC_PHY_PGSR0__ZCDONE__MASK        0x00000008
#define MEMC_PHY_PGSR0__ZCDONE__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_PGSR0__ZCDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.DIDONE - DRAM Initialization Done: Indicates if set that DRAM initialization has completed. */
#define MEMC_PHY_PGSR0__DIDONE__SHIFT       4
#define MEMC_PHY_PGSR0__DIDONE__WIDTH       1
#define MEMC_PHY_PGSR0__DIDONE__MASK        0x00000010
#define MEMC_PHY_PGSR0__DIDONE__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_PGSR0__DIDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WLDONE - Write Leveling Done: Indicates if set that write leveling has completed. */
#define MEMC_PHY_PGSR0__WLDONE__SHIFT       5
#define MEMC_PHY_PGSR0__WLDONE__WIDTH       1
#define MEMC_PHY_PGSR0__WLDONE__MASK        0x00000020
#define MEMC_PHY_PGSR0__WLDONE__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_PGSR0__WLDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.QSGDONE - Read DQS Gate Training Done: Indicates if set that DQS gate training has completed. */
#define MEMC_PHY_PGSR0__QSGDONE__SHIFT       6
#define MEMC_PHY_PGSR0__QSGDONE__WIDTH       1
#define MEMC_PHY_PGSR0__QSGDONE__MASK        0x00000040
#define MEMC_PHY_PGSR0__QSGDONE__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_PGSR0__QSGDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WLADONE - Write Leveling Adjustment Done: Indicates if set that write leveling adjustment has completed. */
#define MEMC_PHY_PGSR0__WLADONE__SHIFT       7
#define MEMC_PHY_PGSR0__WLADONE__WIDTH       1
#define MEMC_PHY_PGSR0__WLADONE__MASK        0x00000080
#define MEMC_PHY_PGSR0__WLADONE__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_PGSR0__WLADONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.RDDONE - Read Data Bit Deskew Done: Indicates if set that read bit deskew has completed. */
#define MEMC_PHY_PGSR0__RDDONE__SHIFT       8
#define MEMC_PHY_PGSR0__RDDONE__WIDTH       1
#define MEMC_PHY_PGSR0__RDDONE__MASK        0x00000100
#define MEMC_PHY_PGSR0__RDDONE__INV_MASK    0xFFFFFEFF
#define MEMC_PHY_PGSR0__RDDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WDDONE - Write Data Bit Deskew Done: Indicates if set that write bit deskew has completed. */
#define MEMC_PHY_PGSR0__WDDONE__SHIFT       9
#define MEMC_PHY_PGSR0__WDDONE__WIDTH       1
#define MEMC_PHY_PGSR0__WDDONE__MASK        0x00000200
#define MEMC_PHY_PGSR0__WDDONE__INV_MASK    0xFFFFFDFF
#define MEMC_PHY_PGSR0__WDDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.REDONE - Read Data Eye Training Done: Indicates if set that read eye training has completed. */
#define MEMC_PHY_PGSR0__REDONE__SHIFT       10
#define MEMC_PHY_PGSR0__REDONE__WIDTH       1
#define MEMC_PHY_PGSR0__REDONE__MASK        0x00000400
#define MEMC_PHY_PGSR0__REDONE__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_PGSR0__REDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WEDONE - Write Data Eye Training Done: Indicates if set that write eye training has completed. */
#define MEMC_PHY_PGSR0__WEDONE__SHIFT       11
#define MEMC_PHY_PGSR0__WEDONE__WIDTH       1
#define MEMC_PHY_PGSR0__WEDONE__MASK        0x00000800
#define MEMC_PHY_PGSR0__WEDONE__INV_MASK    0xFFFFF7FF
#define MEMC_PHY_PGSR0__WEDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.ZCERR - Impedance Calibration Error: Indicates if set that there is an error in impedance calibration. */
#define MEMC_PHY_PGSR0__ZCERR__SHIFT       20
#define MEMC_PHY_PGSR0__ZCERR__WIDTH       1
#define MEMC_PHY_PGSR0__ZCERR__MASK        0x00100000
#define MEMC_PHY_PGSR0__ZCERR__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_PGSR0__ZCERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WLERR - Write Leveling Error: Indicates if set that there is an error in write leveling. */
#define MEMC_PHY_PGSR0__WLERR__SHIFT       21
#define MEMC_PHY_PGSR0__WLERR__WIDTH       1
#define MEMC_PHY_PGSR0__WLERR__MASK        0x00200000
#define MEMC_PHY_PGSR0__WLERR__INV_MASK    0xFFDFFFFF
#define MEMC_PHY_PGSR0__WLERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.QSGERR - Read DQS Gate Training Error: Indicates if set that there is an error in DQS gate training. */
#define MEMC_PHY_PGSR0__QSGERR__SHIFT       22
#define MEMC_PHY_PGSR0__QSGERR__WIDTH       1
#define MEMC_PHY_PGSR0__QSGERR__MASK        0x00400000
#define MEMC_PHY_PGSR0__QSGERR__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_PGSR0__QSGERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WLAERR - Write Data Leveling Adjustment Error: Indicates if set that there is an error in write leveling adjustment. */
#define MEMC_PHY_PGSR0__WLAERR__SHIFT       23
#define MEMC_PHY_PGSR0__WLAERR__WIDTH       1
#define MEMC_PHY_PGSR0__WLAERR__MASK        0x00800000
#define MEMC_PHY_PGSR0__WLAERR__INV_MASK    0xFF7FFFFF
#define MEMC_PHY_PGSR0__WLAERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.RDERR - Read Data Bit Deskew Error: Indicates if set that there is an error in read bit deskew. */
#define MEMC_PHY_PGSR0__RDERR__SHIFT       24
#define MEMC_PHY_PGSR0__RDERR__WIDTH       1
#define MEMC_PHY_PGSR0__RDERR__MASK        0x01000000
#define MEMC_PHY_PGSR0__RDERR__INV_MASK    0xFEFFFFFF
#define MEMC_PHY_PGSR0__RDERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WDERR - Write Data Bit Deskew Error: Indicates if set that there is an error in write bit deskew. */
#define MEMC_PHY_PGSR0__WDERR__SHIFT       25
#define MEMC_PHY_PGSR0__WDERR__WIDTH       1
#define MEMC_PHY_PGSR0__WDERR__MASK        0x02000000
#define MEMC_PHY_PGSR0__WDERR__INV_MASK    0xFDFFFFFF
#define MEMC_PHY_PGSR0__WDERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.REERR - Read Data Eye Training Error: Indicates if set that there is an error in read eye training. */
#define MEMC_PHY_PGSR0__REERR__SHIFT       26
#define MEMC_PHY_PGSR0__REERR__WIDTH       1
#define MEMC_PHY_PGSR0__REERR__MASK        0x04000000
#define MEMC_PHY_PGSR0__REERR__INV_MASK    0xFBFFFFFF
#define MEMC_PHY_PGSR0__REERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.WEERR - Write Eye Training Error: Indicates if set that there is an error in write eye training. */
#define MEMC_PHY_PGSR0__WEERR__SHIFT       27
#define MEMC_PHY_PGSR0__WEERR__WIDTH       1
#define MEMC_PHY_PGSR0__WEERR__MASK        0x08000000
#define MEMC_PHY_PGSR0__WEERR__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_PGSR0__WEERR__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.PLDONE_CHN - PLL Lock Done per Channel: Indicates PLL locking has completed for each underlying channel. Bit 28 represents channel 0 while bit 29 represents channel 1. */
#define MEMC_PHY_PGSR0__PLDONE_CHN__SHIFT       28
#define MEMC_PHY_PGSR0__PLDONE_CHN__WIDTH       2
#define MEMC_PHY_PGSR0__PLDONE_CHN__MASK        0x30000000
#define MEMC_PHY_PGSR0__PLDONE_CHN__INV_MASK    0xCFFFFFFF
#define MEMC_PHY_PGSR0__PLDONE_CHN__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR0.APLOCK - AC PLL Lock: Indicates, if set, that AC PLL has locked. This is a direct status of the AC PLL lock pin. */
#define MEMC_PHY_PGSR0__APLOCK__SHIFT       31
#define MEMC_PHY_PGSR0__APLOCK__WIDTH       1
#define MEMC_PHY_PGSR0__APLOCK__MASK        0x80000000
#define MEMC_PHY_PGSR0__APLOCK__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PGSR0__APLOCK__HW_DEFAULT  0x0

/* PHY General Status Register 1 */
/* These are general status registers for the PHY. They indicate, among other things, whether initialization, write leveling or period measurement calibrations are done. The following tables describe the bits of the PHY GSR registers. */
#define MEMC_PHY_PGSR1            0x10810014

/* MEMC_PHY_PGSR1.DLTDONE - Delay Line Test Done: Indicates, if set, that the PHY control block has finished doing period measurement of the AC delay line digital test output. */
#define MEMC_PHY_PGSR1__DLTDONE__SHIFT       0
#define MEMC_PHY_PGSR1__DLTDONE__WIDTH       1
#define MEMC_PHY_PGSR1__DLTDONE__MASK        0x00000001
#define MEMC_PHY_PGSR1__DLTDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_PGSR1__DLTDONE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR1.DLTCODE - Delay Line Test Code: Returns the code measured by the PHY control block that corresponds to the period of the AC delay line digital test output. */
#define MEMC_PHY_PGSR1__DLTCODE__SHIFT       1
#define MEMC_PHY_PGSR1__DLTCODE__WIDTH       24
#define MEMC_PHY_PGSR1__DLTCODE__MASK        0x01FFFFFE
#define MEMC_PHY_PGSR1__DLTCODE__INV_MASK    0xFE000001
#define MEMC_PHY_PGSR1__DLTCODE__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR1.VTSTOP - VT Stop: Indicates if set that the VT calculation logic has stopped computing the next values for the VT compensated delay line values. After assertion of the PGCR.INHVT, the VTSTOP bit should be read to ensure all VT compensation logic has stopped computations before writing to the delay line registers. */
#define MEMC_PHY_PGSR1__VTSTOP__SHIFT       30
#define MEMC_PHY_PGSR1__VTSTOP__WIDTH       1
#define MEMC_PHY_PGSR1__VTSTOP__MASK        0x40000000
#define MEMC_PHY_PGSR1__VTSTOP__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_PGSR1__VTSTOP__HW_DEFAULT  0x0

/* MEMC_PHY_PGSR1.PARERR - RDIMM Parity Error: Indicates, if set, that there was a parity error (i.e. err_out_n was sampled low) during one of the transactions to the RDIMM buffer chip. This bit remains asserted until cleared by the PIR.CLRSR. */
#define MEMC_PHY_PGSR1__PARERR__SHIFT       31
#define MEMC_PHY_PGSR1__PARERR__WIDTH       1
#define MEMC_PHY_PGSR1__PARERR__MASK        0x80000000
#define MEMC_PHY_PGSR1__PARERR__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PGSR1__PARERR__HW_DEFAULT  0x0

/* PLL Control Register */
/* The PLLCR register provides miscellaneous controls of the PLLs used in the AC and DATX8 macros, including PLL test modes and PLL bypass. */
#define MEMC_PHY_PLLCR            0x10810018

/* MEMC_PHY_PLLCR.DTC - Digital Test Control: Selects various PLL digital test signals and other test mode signals to be brought out via bit [1] of the PLL digital test output (pll_dto[1]). Valid values are: 00 = '0' (Test output is disabled) 01 = PLL x1 clock (X1) 10 = PLL reference (input) clock (REF_CLK) 11 = PLL feedback clock (FB_X1) */
#define MEMC_PHY_PLLCR__DTC__SHIFT       0
#define MEMC_PHY_PLLCR__DTC__WIDTH       2
#define MEMC_PHY_PLLCR__DTC__MASK        0x00000003
#define MEMC_PHY_PLLCR__DTC__INV_MASK    0xFFFFFFFC
#define MEMC_PHY_PLLCR__DTC__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.ATC - Analog Test Control: Selects various PLL analog test signals to be brought out via PLL analog test output pin (pll_ato). Valid values are: 0000 = Reserved 0001 = vdd_ckin 0010 = vrfbf 0011 = vdd_cko 0100 = vp_cp 0101 = vpfil(vp) 0110 = Reserved 0111 = gd 1000 = vcntrl_atb 1001 = vref_atb 1010 = vpsf_atb 1011 - 1111 = Reserved */
#define MEMC_PHY_PLLCR__ATC__SHIFT       2
#define MEMC_PHY_PLLCR__ATC__WIDTH       4
#define MEMC_PHY_PLLCR__ATC__MASK        0x0000003C
#define MEMC_PHY_PLLCR__ATC__INV_MASK    0xFFFFFFC3
#define MEMC_PHY_PLLCR__ATC__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.ATOEN - Analog Test Enable (ATOEN): Selects the analog test signal that should be driven on the analog test output pin. Otherwise the analog test output is tri-stated. This allows analog test output pins from multiple PLLs to be connected together. Valid values are: 0000 = All PLL analog test signals are tri-stated 0001 = AC PLL analog test signal is driven out 0010 = DATX8 0 PLL analog test signal is driven out 0011 = DATX8 1 PLL analog test signal is driven out 0100 = DATX8 2 PLL analog test signal is driven out 0101 = DATX8 3 PLL analog test signal is driven out 0110 = DATX8 4 PLL analog test signal is driven out 0111 = DATX8 5 PLL analog test signal is driven out 1000 = DATX8 6 PLL analog test signal is driven out 1001 = DATX8 7 PLL analog test signal is driven out 1010 = DATX8 8 PLL analog test signal is driven out 1011 - 1111 = Reserved */
#define MEMC_PHY_PLLCR__ATOEN__SHIFT       6
#define MEMC_PHY_PLLCR__ATOEN__WIDTH       4
#define MEMC_PHY_PLLCR__ATOEN__MASK        0x000003C0
#define MEMC_PHY_PLLCR__ATOEN__INV_MASK    0xFFFFFC3F
#define MEMC_PHY_PLLCR__ATOEN__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.GSHIFT - Gear Shift: Enables, if set, rapid locking mode. */
#define MEMC_PHY_PLLCR__GSHIFT__SHIFT       10
#define MEMC_PHY_PLLCR__GSHIFT__WIDTH       1
#define MEMC_PHY_PLLCR__GSHIFT__MASK        0x00000400
#define MEMC_PHY_PLLCR__GSHIFT__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_PLLCR__GSHIFT__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.CPIC - Charge Pump Integrating Current Control */
#define MEMC_PHY_PLLCR__CPIC__SHIFT       11
#define MEMC_PHY_PLLCR__CPIC__WIDTH       2
#define MEMC_PHY_PLLCR__CPIC__MASK        0x00001800
#define MEMC_PHY_PLLCR__CPIC__INV_MASK    0xFFFFE7FF
#define MEMC_PHY_PLLCR__CPIC__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.CPPC - Charge Pump Proportional Current Control */
#define MEMC_PHY_PLLCR__CPPC__SHIFT       13
#define MEMC_PHY_PLLCR__CPPC__WIDTH       4
#define MEMC_PHY_PLLCR__CPPC__MASK        0x0001E000
#define MEMC_PHY_PLLCR__CPPC__INV_MASK    0xFFFE1FFF
#define MEMC_PHY_PLLCR__CPPC__HW_DEFAULT  0xE

/* MEMC_PHY_PLLCR.QPMODE - PLL Quadrature Phase Mode: Enables, if set, the quadrature phase clock outputs. This mode is not used in this version of the PHY. */
#define MEMC_PHY_PLLCR__QPMODE__SHIFT       17
#define MEMC_PHY_PLLCR__QPMODE__WIDTH       1
#define MEMC_PHY_PLLCR__QPMODE__MASK        0x00020000
#define MEMC_PHY_PLLCR__QPMODE__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_PLLCR__QPMODE__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.FRQSEL - PLL Frequency Select: Selects the operating range of the PLL. Valid values for PHYs that go up to 2133 Mbps are: 00 = PLL reference clock (ctl_clk/REF_CLK) ranges from 335MHz to 533MHz 01 = PLL reference clock (ctl_clk/REF_CLK) ranges from 225MHz to 385MHz 10 = Reserved 11 = PLL reference clock (ctl_clk/REF_CLK) ranges from 166MHz to 275MHz Valid values for PHYs that don't go up to 2133 Mbps are: 00 = PLL reference clock (ctl_clk/REF_CLK) ranges from 250MHz to 400MHz 01 = PLL reference clock (ctl_clk/REF_CLK) ranges from 166MHz to 300MHz 10 = Reserved 11 = Reserved */
#define MEMC_PHY_PLLCR__FRQSEL__SHIFT       18
#define MEMC_PHY_PLLCR__FRQSEL__WIDTH       2
#define MEMC_PHY_PLLCR__FRQSEL__MASK        0x000C0000
#define MEMC_PHY_PLLCR__FRQSEL__INV_MASK    0xFFF3FFFF
#define MEMC_PHY_PLLCR__FRQSEL__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.PLLPD - PLL Power Down: Puts the PLLs in power down mode by driving the PLL power down pin. This bit is not self-clearing and a '0' must be written to de-assert the power-down. */
#define MEMC_PHY_PLLCR__PLLPD__SHIFT       29
#define MEMC_PHY_PLLCR__PLLPD__WIDTH       1
#define MEMC_PHY_PLLCR__PLLPD__MASK        0x20000000
#define MEMC_PHY_PLLCR__PLLPD__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_PLLCR__PLLPD__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.PLLRST - PLL Rest: Resets the PLLs by driving the PLL reset pin. This bit is not self-clearing and a '0' must be written to de-assert the reset. */
#define MEMC_PHY_PLLCR__PLLRST__SHIFT       30
#define MEMC_PHY_PLLCR__PLLRST__WIDTH       1
#define MEMC_PHY_PLLCR__PLLRST__MASK        0x40000000
#define MEMC_PHY_PLLCR__PLLRST__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_PLLCR__PLLRST__HW_DEFAULT  0x0

/* MEMC_PHY_PLLCR.BYP - PLL Bypass: Bypasses the PLL if set to 1. */
#define MEMC_PHY_PLLCR__BYP__SHIFT       31
#define MEMC_PHY_PLLCR__BYP__WIDTH       1
#define MEMC_PHY_PLLCR__BYP__MASK        0x80000000
#define MEMC_PHY_PLLCR__BYP__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PLLCR__BYP__HW_DEFAULT  0x0

/* PHY Timing Register 0 */
/* PHY Timing Registers are used to program different timing parameters used by the PUB logic. All the default values shown in these tables are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. The clocks in which the timing numbers are measured are specified in the description of each parameter. */
#define MEMC_PHY_PTR0             0x1081001C

/* MEMC_PHY_PTR0.tPHYRST - PHY Reset Time: Number of configuration or APB clock cycles that the PHY reset must remain asserted after PHY calibration is done before the reset to the PHY is de-asserted. This is used to extend the reset to the PHY so that the reset is asserted for some clock cycles after the clocks are stable. Valid values are from 1 to 63 (the value must be non-zero). */
#define MEMC_PHY_PTR0__T_PHYRST__SHIFT      0
#define MEMC_PHY_PTR0__T_PHYRST__WIDTH      6
#define MEMC_PHY_PTR0__T_PHYRST__MASK       0x0000003F
#define MEMC_PHY_PTR0__T_PHYRST__INV_MASK   0xFFFFFFC0
#define MEMC_PHY_PTR0__T_PHYRST__HW_DEFAULT 0x10

/* MEMC_PHY_PTR0.tPLLGS - PLL Gear Shift Time: Number of configuration or APB clock cycles from when the PLL reset pin is de-asserted to when the PLL gear shift pin is de-asserted. This must correspond to a value that is equal to or more than 4us. Default value corresponds to 4us. */
#define MEMC_PHY_PTR0__T_PLLGS__SHIFT      6
#define MEMC_PHY_PTR0__T_PLLGS__WIDTH      15
#define MEMC_PHY_PTR0__T_PLLGS__MASK       0x001FFFC0
#define MEMC_PHY_PTR0__T_PLLGS__INV_MASK   0xFFE0003F
#define MEMC_PHY_PTR0__T_PLLGS__HW_DEFAULT 0x856

/* MEMC_PHY_PTR0.tPLLPD - PLL Power-Down Time: Number of configuration or APB clock cycles that the PLL must remain in power-down mode, i.e. number of clock cycles from when PLL power-down pin is asserted to when PLL power-down pin is de-asserted. This must correspond to a value that is equal to or more than 1us. Default value corresponds to 1us. */
#define MEMC_PHY_PTR0__T_PLLPD__SHIFT      21
#define MEMC_PHY_PTR0__T_PLLPD__WIDTH      11
#define MEMC_PHY_PTR0__T_PLLPD__MASK       0xFFE00000
#define MEMC_PHY_PTR0__T_PLLPD__INV_MASK   0x001FFFFF
#define MEMC_PHY_PTR0__T_PLLPD__HW_DEFAULT 0x216

/* PHY Timing Register 1 */
/* PHY Timing Registers are used to program different timing parameters used by the PUB logic. All the default values shown in these tables are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. The clocks in which the timing numbers are measured are specified in the description of each parameter. */
#define MEMC_PHY_PTR1             0x10810020

/* MEMC_PHY_PTR1.tPLLRST - PLL Reset Time: Number of configuration or APB clock cycles that the PLL must remain in reset mode, i.e. number of clock cycles from when PLL power-down pin is de-asserted and PLL reset pin is asserted to when PLL reset pin is de-asserted. The setting must correspond to a value that is equal to, or greater than, 3us. */
#define MEMC_PHY_PTR1__T_PLLRST__SHIFT      0
#define MEMC_PHY_PTR1__T_PLLRST__WIDTH      13
#define MEMC_PHY_PTR1__T_PLLRST__MASK       0x00001FFF
#define MEMC_PHY_PTR1__T_PLLRST__INV_MASK   0xFFFFE000
#define MEMC_PHY_PTR1__T_PLLRST__HW_DEFAULT 0x12C0

/* MEMC_PHY_PTR1.tPLLLOCK - PLL Lock Time: Number of configuration or APB clock cycles for the PLL to stabilize and lock, i.e. number of clock cycles from when the PLL reset pin is de-asserted to when the PLL has lock and is ready for use. This must correspond to a value that is equal to or more than 100us. Default value corresponds to 100us. */
#define MEMC_PHY_PTR1__T_PLLLOCK__SHIFT      16
#define MEMC_PHY_PTR1__T_PLLLOCK__WIDTH      16
#define MEMC_PHY_PTR1__T_PLLLOCK__MASK       0xFFFF0000
#define MEMC_PHY_PTR1__T_PLLLOCK__INV_MASK   0x0000FFFF
#define MEMC_PHY_PTR1__T_PLLLOCK__HW_DEFAULT 0xD056

/* PHY Timing Register 2 */
/* PHY Timing Registers are used to program different timing parameters used by the PUB logic. All the default values shown in these tables are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. The clocks in which the timing numbers are measured are specified in the description of each parameter. */
#define MEMC_PHY_PTR2             0x10810024

/* MEMC_PHY_PTR2.tCALON - Calibration On Time: Number of clock cycles that the calibration clock is enabled (cal_clk_en asserted). */
#define MEMC_PHY_PTR2__T_CALON__SHIFT      0
#define MEMC_PHY_PTR2__T_CALON__WIDTH      5
#define MEMC_PHY_PTR2__T_CALON__MASK       0x0000001F
#define MEMC_PHY_PTR2__T_CALON__INV_MASK   0xFFFFFFE0
#define MEMC_PHY_PTR2__T_CALON__HW_DEFAULT 0xF

/* MEMC_PHY_PTR2.tCALS - Calibration Setup Time: Number of controller clock cycles from when calibration is enabled (cal_en asserted) to when the calibration clock is asserted again (cal_clk_en asserted). */
#define MEMC_PHY_PTR2__T_CALS__SHIFT      5
#define MEMC_PHY_PTR2__T_CALS__WIDTH      5
#define MEMC_PHY_PTR2__T_CALS__MASK       0x000003E0
#define MEMC_PHY_PTR2__T_CALS__INV_MASK   0xFFFFFC1F
#define MEMC_PHY_PTR2__T_CALS__HW_DEFAULT 0xF

/* MEMC_PHY_PTR2.tCALH - Calibration Hold Time: Number of controller clock cycles from when the clock was disabled (cal_clk_en deasserted) to when calibration is enable (cal_en asserted). */
#define MEMC_PHY_PTR2__T_CALH__SHIFT      10
#define MEMC_PHY_PTR2__T_CALH__WIDTH      5
#define MEMC_PHY_PTR2__T_CALH__MASK       0x00007C00
#define MEMC_PHY_PTR2__T_CALH__INV_MASK   0xFFFF83FF
#define MEMC_PHY_PTR2__T_CALH__HW_DEFAULT 0xF

/* MEMC_PHY_PTR2.tWLDLYS - Write Leveling Delay Settling Time: Number of controller clock cycles from when a new value of the write leveling delay is applies to the LCDL to when to DQS high is driven high. This allows the delay to settle. */
#define MEMC_PHY_PTR2__T_WLDLYS__SHIFT      15
#define MEMC_PHY_PTR2__T_WLDLYS__WIDTH      5
#define MEMC_PHY_PTR2__T_WLDLYS__MASK       0x000F8000
#define MEMC_PHY_PTR2__T_WLDLYS__INV_MASK   0xFFF07FFF
#define MEMC_PHY_PTR2__T_WLDLYS__HW_DEFAULT 0x10

/* PHY Timing Register 3 */
/* PHY Timing Registers are used to program different timing parameters used by the PUB logic. All the default values shown in these tables are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. The clocks in which the timing numbers are measured are specified in the description of each parameter. */
#define MEMC_PHY_PTR3             0x10810028

/* MEMC_PHY_PTR3.tDINIT0 - DRAM Initialization Time 0: DRAM initialization time in DRAM clock cycles corresponding to the following: DDR3 = CKE low time with power and clock stable (500 us) DDR2 = CKE low time with power and clock stable (200 us) Default value corresponds to DDR3 500 us at 1066 MHz. During Verilog simulations, it is recommended that this value is changed to a much smaller value in order to avoid long simulation times. However, this may cause a memory model error, due to a violation of the CKE setup sequence. This violation is expected if this value is not programmed to the required SDRAM CKE low time, but memory models should be able to tolerate this violation without malfunction of the model. */
#define MEMC_PHY_PTR3__T_DINIT0__SHIFT      0
#define MEMC_PHY_PTR3__T_DINIT0__WIDTH      20
#define MEMC_PHY_PTR3__T_DINIT0__MASK       0x000FFFFF
#define MEMC_PHY_PTR3__T_DINIT0__INV_MASK   0xFFF00000
#define MEMC_PHY_PTR3__T_DINIT0__HW_DEFAULT 0x82356

/* MEMC_PHY_PTR3.tDINIT1 - DRAM Initialization Time 1: DRAM initialization timein DRAM clock cycles corresponding to the following: DDR3 = CKE high time to first command (tRFC + 10 ns or 5 tCK, whichever is bigger) DDR2 = CKE high time to first command (400 ns) Default value corresponds to DDR3 tRFC of 360ns at 1066 MHz. */
#define MEMC_PHY_PTR3__T_DINIT1__SHIFT      20
#define MEMC_PHY_PTR3__T_DINIT1__WIDTH      9
#define MEMC_PHY_PTR3__T_DINIT1__MASK       0x1FF00000
#define MEMC_PHY_PTR3__T_DINIT1__INV_MASK   0xE00FFFFF
#define MEMC_PHY_PTR3__T_DINIT1__HW_DEFAULT 0x180

/* PHY Timing Register 4 */
/* PHY Timing Registers are used to program different timing parameters used by the PUB logic. All the default values shown in these tables are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. The clocks in which the timing numbers are measured are specified in the description of each parameter. */
#define MEMC_PHY_PTR4             0x1081002C

/* MEMC_PHY_PTR4.tDINIT2 - DRAM Initialization Time 2: DRAM initialization time in DRAM clock cycles corresponding to the following: DDR3 = Reset low time (200 us on power-up or 100 ns after power-up) Default value corresponds to DDR3 200 us at 1066 MHz. */
#define MEMC_PHY_PTR4__T_DINIT2__SHIFT      0
#define MEMC_PHY_PTR4__T_DINIT2__WIDTH      18
#define MEMC_PHY_PTR4__T_DINIT2__MASK       0x0003FFFF
#define MEMC_PHY_PTR4__T_DINIT2__INV_MASK   0xFFFC0000
#define MEMC_PHY_PTR4__T_DINIT2__HW_DEFAULT 0x34156

/* MEMC_PHY_PTR4.tDINIT3 - DRAM Initialization Time 3: DRAM initialization time in DRAM clock cycles corresponding to the following: DDR3 = Time from ZQ initialization command to first command (1 us) Default value corresponds to the DDR3 640ns at 1066 MHz. */
#define MEMC_PHY_PTR4__T_DINIT3__SHIFT      18
#define MEMC_PHY_PTR4__T_DINIT3__WIDTH      10
#define MEMC_PHY_PTR4__T_DINIT3__MASK       0x0FFC0000
#define MEMC_PHY_PTR4__T_DINIT3__INV_MASK   0xF003FFFF
#define MEMC_PHY_PTR4__T_DINIT3__HW_DEFAULT 0x2AB

/* AC Master Delay Line Register */
/* The AC Master Delay Line Registers return different period values measured by the master delay line in the AC macro. */
#define MEMC_PHY_ACMDLR           0x10810030

/* MEMC_PHY_ACMDLR.IPRD - Initial Period: Initial period measured by the master delay line calibration for VT drift compensation. This value is used as the denominator when calculating the ratios of updates during VT compensation. */
#define MEMC_PHY_ACMDLR__IPRD__SHIFT       0
#define MEMC_PHY_ACMDLR__IPRD__WIDTH       8
#define MEMC_PHY_ACMDLR__IPRD__MASK        0x000000FF
#define MEMC_PHY_ACMDLR__IPRD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_ACMDLR__IPRD__HW_DEFAULT  0x0

/* MEMC_PHY_ACMDLR.TPRD - Target Period: Target period measured by the master delay line calibration for VT drift compensation. This is the current measured value of the period and is continuously updated if the MDL is enabled to do so. */
#define MEMC_PHY_ACMDLR__TPRD__SHIFT       8
#define MEMC_PHY_ACMDLR__TPRD__WIDTH       8
#define MEMC_PHY_ACMDLR__TPRD__MASK        0x0000FF00
#define MEMC_PHY_ACMDLR__TPRD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_ACMDLR__TPRD__HW_DEFAULT  0x0

/* MEMC_PHY_ACMDLR.MDLD - MDL Delay: Delay select for the LCDL for the Master Delay Line. */
#define MEMC_PHY_ACMDLR__MDLD__SHIFT       16
#define MEMC_PHY_ACMDLR__MDLD__WIDTH       8
#define MEMC_PHY_ACMDLR__MDLD__MASK        0x00FF0000
#define MEMC_PHY_ACMDLR__MDLD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_ACMDLR__MDLD__HW_DEFAULT  0x0

/* AC Bit Delay Line Register */
/* The AC bit delay line registers is used to select the delay value on the BDLs used in the AC macro. A single BDL field in the ACBDLR register connects to a corresponding AC BDL. */
#define MEMC_PHY_ACBDLR           0x10810034

/* MEMC_PHY_ACBDLR.CK0BD - CK0 Bit Delay: Delay select for the BDL on CK0. */
#define MEMC_PHY_ACBDLR__CK0BD__SHIFT       0
#define MEMC_PHY_ACBDLR__CK0BD__WIDTH       6
#define MEMC_PHY_ACBDLR__CK0BD__MASK        0x0000003F
#define MEMC_PHY_ACBDLR__CK0BD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_ACBDLR__CK0BD__HW_DEFAULT  0x0

/* MEMC_PHY_ACBDLR.CK1BD - CK1 Bit Delay: Delay select for the BDL on CK1. */
#define MEMC_PHY_ACBDLR__CK1BD__SHIFT       6
#define MEMC_PHY_ACBDLR__CK1BD__WIDTH       6
#define MEMC_PHY_ACBDLR__CK1BD__MASK        0x00000FC0
#define MEMC_PHY_ACBDLR__CK1BD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_ACBDLR__CK1BD__HW_DEFAULT  0x0

/* MEMC_PHY_ACBDLR.CK2BD - CK2 Bit Delay: Delay select for the BDL on CK2. */
#define MEMC_PHY_ACBDLR__CK2BD__SHIFT       12
#define MEMC_PHY_ACBDLR__CK2BD__WIDTH       6
#define MEMC_PHY_ACBDLR__CK2BD__MASK        0x0003F000
#define MEMC_PHY_ACBDLR__CK2BD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_ACBDLR__CK2BD__HW_DEFAULT  0x0

/* MEMC_PHY_ACBDLR.ACBD - Address/Command Bit Delay: Delay select for the BDLs on address and command signals. */
#define MEMC_PHY_ACBDLR__ACBD__SHIFT       18
#define MEMC_PHY_ACBDLR__ACBD__WIDTH       6
#define MEMC_PHY_ACBDLR__ACBD__MASK        0x00FC0000
#define MEMC_PHY_ACBDLR__ACBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_ACBDLR__ACBD__HW_DEFAULT  0x0

/* AC I/O Configuration Register */
/* The AC I/O Configuration Register is used to control output enables, on-die termination enables, and power-down enables for SSTL I/Os for all address/command signals going to the SDRAM, including the SDRAM clock (CK). */
#define MEMC_PHY_ACIOCR           0x10810038

/* MEMC_PHY_ACIOCR.ACIOM - Address/Command I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for all address and command pins. This bit connects to bit [0] of the IOM pin on the D3F I/Os, and for other I/O libraries, it connects to the IOM pin of the I/O. */
#define MEMC_PHY_ACIOCR__ACIOM__SHIFT       0
#define MEMC_PHY_ACIOCR__ACIOM__WIDTH       1
#define MEMC_PHY_ACIOCR__ACIOM__MASK        0x00000001
#define MEMC_PHY_ACIOCR__ACIOM__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_ACIOCR__ACIOM__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.ACOE - Address/Command Output Enable: Enables, when set, the output driver on the I/O for all address and command pins. */
#define MEMC_PHY_ACIOCR__ACOE__SHIFT       1
#define MEMC_PHY_ACIOCR__ACOE__WIDTH       1
#define MEMC_PHY_ACIOCR__ACOE__MASK        0x00000002
#define MEMC_PHY_ACIOCR__ACOE__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_ACIOCR__ACOE__HW_DEFAULT  0x1

/* MEMC_PHY_ACIOCR.ACODT - Address/Command On-Die Termination: Enables, when set, the on-die termination on the I/O for RAS#, CAS#, WE#, BA[2:0], and A[15:0] pins. */
#define MEMC_PHY_ACIOCR__ACODT__SHIFT       2
#define MEMC_PHY_ACIOCR__ACODT__WIDTH       1
#define MEMC_PHY_ACIOCR__ACODT__MASK        0x00000004
#define MEMC_PHY_ACIOCR__ACODT__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_ACIOCR__ACODT__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.ACPDD - AC Power Down Driver: Powers down, when set, the output driver on the I/O for RAS#, CAS#, WE#, BA[2:0], and A[15:0] pins. */
#define MEMC_PHY_ACIOCR__ACPDD__SHIFT       3
#define MEMC_PHY_ACIOCR__ACPDD__WIDTH       1
#define MEMC_PHY_ACIOCR__ACPDD__MASK        0x00000008
#define MEMC_PHY_ACIOCR__ACPDD__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_ACIOCR__ACPDD__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.ACPDR - AC Power Down Receiver: Powers down, when set, the input receiver on the I/O for RAS#, CAS#, WE#, BA[2:0], and A[15:0] pins. */
#define MEMC_PHY_ACIOCR__ACPDR__SHIFT       4
#define MEMC_PHY_ACIOCR__ACPDR__WIDTH       1
#define MEMC_PHY_ACIOCR__ACPDR__MASK        0x00000010
#define MEMC_PHY_ACIOCR__ACPDR__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_ACIOCR__ACPDR__HW_DEFAULT  0x1

/* MEMC_PHY_ACIOCR.CKODT - CK On-Die Termination: Enables, when set, the on-die termination on the I/O for CK[0], CK[1], and CK[2] pins, respectively. */
#define MEMC_PHY_ACIOCR__CKODT__SHIFT       5
#define MEMC_PHY_ACIOCR__CKODT__WIDTH       1
#define MEMC_PHY_ACIOCR__CKODT__MASK        0x00000020
#define MEMC_PHY_ACIOCR__CKODT__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_ACIOCR__CKODT__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.CKPDD - CK Power Down Driver: Powers down, when set, the output driver on the I/O for CK[0], CK[1], and CK[2] pins, respectively. */
#define MEMC_PHY_ACIOCR__CKPDD__SHIFT       8
#define MEMC_PHY_ACIOCR__CKPDD__WIDTH       1
#define MEMC_PHY_ACIOCR__CKPDD__MASK        0x00000100
#define MEMC_PHY_ACIOCR__CKPDD__INV_MASK    0xFFFFFEFF
#define MEMC_PHY_ACIOCR__CKPDD__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.CKPDR - CK Power Down Receiver: Powers down, when set, the input receiver on the I/O for CK[0], CK[1], and CK[2] pins, respectively. */
#define MEMC_PHY_ACIOCR__CKPDR__SHIFT       11
#define MEMC_PHY_ACIOCR__CKPDR__WIDTH       1
#define MEMC_PHY_ACIOCR__CKPDR__MASK        0x00000800
#define MEMC_PHY_ACIOCR__CKPDR__INV_MASK    0xFFFFF7FF
#define MEMC_PHY_ACIOCR__CKPDR__HW_DEFAULT  0x1

/* MEMC_PHY_ACIOCR.RANKODT - Rank On-Die Termination: Enables, when set, the on-die termination on the I/O for CKE[3:0], ODT[3:0], and CS#[3:0] pins. RANKODT[0] controls the on-die termination for CKE[0], ODT[0], and CS#[0], RANKODT[1] controls the on-die termination for CKE[1], ODT[1], and CS#[1], and so on. */
#define MEMC_PHY_ACIOCR__RANKODT__SHIFT       14
#define MEMC_PHY_ACIOCR__RANKODT__WIDTH       1
#define MEMC_PHY_ACIOCR__RANKODT__MASK        0x00004000
#define MEMC_PHY_ACIOCR__RANKODT__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_ACIOCR__RANKODT__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.CSPDD - CS# Power Down Driver: Powers down, when set, the output driver on the I/O for CS#[3:0] pins. CSPDD[0] controls the power down for CS#[0], CSPDD[1] controls the power down for CS#[1], and so on. CKE and ODT driver power down is controlled by DSGCR register. */
#define MEMC_PHY_ACIOCR__CSPDD__SHIFT       18
#define MEMC_PHY_ACIOCR__CSPDD__WIDTH       1
#define MEMC_PHY_ACIOCR__CSPDD__MASK        0x00040000
#define MEMC_PHY_ACIOCR__CSPDD__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_ACIOCR__CSPDD__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.RANKPDR - Rank Power Down Receiver: Powers down, when set, the input receiver on the I/O CKE[3:0], ODT[3:0], and CS#[3:0] pins. RANKPDR[0] controls the power down for CKE[0], ODT[0], and CS#[0], RANKPDR[1] controls the power down for CKE[1], ODT[1], and CS#[1], and so on. */
#define MEMC_PHY_ACIOCR__RANKPDR__SHIFT       22
#define MEMC_PHY_ACIOCR__RANKPDR__WIDTH       1
#define MEMC_PHY_ACIOCR__RANKPDR__MASK        0x00400000
#define MEMC_PHY_ACIOCR__RANKPDR__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_ACIOCR__RANKPDR__HW_DEFAULT  0x1

/* MEMC_PHY_ACIOCR.RSTODT - SDRAM Reset On-Die Termination: Enables, when set, the on-die termination on the I/O for SDRAM RST# pin. */
#define MEMC_PHY_ACIOCR__RSTODT__SHIFT       26
#define MEMC_PHY_ACIOCR__RSTODT__WIDTH       1
#define MEMC_PHY_ACIOCR__RSTODT__MASK        0x04000000
#define MEMC_PHY_ACIOCR__RSTODT__INV_MASK    0xFBFFFFFF
#define MEMC_PHY_ACIOCR__RSTODT__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.RSTPDD - SDRAM Reset Power Down Driver: Powers down, when set, the output driver on the I/O for SDRAM RST# pin. */
#define MEMC_PHY_ACIOCR__RSTPDD__SHIFT       27
#define MEMC_PHY_ACIOCR__RSTPDD__WIDTH       1
#define MEMC_PHY_ACIOCR__RSTPDD__MASK        0x08000000
#define MEMC_PHY_ACIOCR__RSTPDD__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_ACIOCR__RSTPDD__HW_DEFAULT  0x0

/* MEMC_PHY_ACIOCR.RSTPDR - SDRAM Reset Power Down Receiver: Powers down, when set, the input receiver on the I/O for SDRAM RST# pin. */
#define MEMC_PHY_ACIOCR__RSTPDR__SHIFT       28
#define MEMC_PHY_ACIOCR__RSTPDR__WIDTH       1
#define MEMC_PHY_ACIOCR__RSTPDR__MASK        0x10000000
#define MEMC_PHY_ACIOCR__RSTPDR__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_ACIOCR__RSTPDR__HW_DEFAULT  0x1

/* MEMC_PHY_ACIOCR.RSTIOM - SDRAM Reset I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for SDRAM Reset. */
#define MEMC_PHY_ACIOCR__RSTIOM__SHIFT       29
#define MEMC_PHY_ACIOCR__RSTIOM__WIDTH       1
#define MEMC_PHY_ACIOCR__RSTIOM__MASK        0x20000000
#define MEMC_PHY_ACIOCR__RSTIOM__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_ACIOCR__RSTIOM__HW_DEFAULT  0x1

/* DATX8 Common Configuration Register */
/* The DATX8 Common Configuration Register is used to control features that affect all DATX8 macros. These include on-die termination enables and power-down enables for SSTL I/Os for all SDRAM data, data mask, and data strobe signals. */
#define MEMC_PHY_DXCCR            0x1081003C

/* MEMC_PHY_DXCCR.DXODT - Data On-Die Termination: Enables, when set, the on-die termination on the I/O for DQ, DM, and DQS/DQS# pins of all DATX8 macros. This bit is ORed with the ODT configuration bit of the individual DATX8 ("DATX8 General Configuration Register (DXnGCR)" on page 138) */
#define MEMC_PHY_DXCCR__DXODT__SHIFT       0
#define MEMC_PHY_DXCCR__DXODT__WIDTH       1
#define MEMC_PHY_DXCCR__DXODT__MASK        0x00000001
#define MEMC_PHY_DXCCR__DXODT__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DXCCR__DXODT__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DXIOM - Data I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for DQ, DM, and DQS/DQS# pins of all DATX8 macros. This bit is ORed with the IOM configuration bit of the individual DATX8. */
#define MEMC_PHY_DXCCR__DXIOM__SHIFT       1
#define MEMC_PHY_DXCCR__DXIOM__WIDTH       1
#define MEMC_PHY_DXCCR__DXIOM__MASK        0x00000002
#define MEMC_PHY_DXCCR__DXIOM__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DXCCR__DXIOM__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.MDLEN - Master Delay Line Enable: Enables, if set, all DATX8 master delay line calibration to perform subsequent period measurements following the initial period measurements that are performed after reset or on when calibration is manually triggered. These additional measurements are accumulated and filtered as long as this bit remains high. This bit is ANDed with the MDLEN bit in the individual DATX8. */
#define MEMC_PHY_DXCCR__MDLEN__SHIFT       2
#define MEMC_PHY_DXCCR__MDLEN__WIDTH       1
#define MEMC_PHY_DXCCR__MDLEN__MASK        0x00000004
#define MEMC_PHY_DXCCR__MDLEN__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DXCCR__MDLEN__HW_DEFAULT  0x1

/* MEMC_PHY_DXCCR.DXPDD - Data Power Down Driver: Powers down, when set, the output driver on I/O for DQ, DM, and DQS/DQS# pins of all DATX8 macros. This bit is ORed with the PDD configuration bit of the individual DATX8. */
#define MEMC_PHY_DXCCR__DXPDD__SHIFT       3
#define MEMC_PHY_DXCCR__DXPDD__WIDTH       1
#define MEMC_PHY_DXCCR__DXPDD__MASK        0x00000008
#define MEMC_PHY_DXCCR__DXPDD__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DXCCR__DXPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DXPDR - Data Power Down Receiver: Powers down, when set, the input receiver on I/O for DQ, DM, and DQS/DQS# pins of all DATX8 macros. This bit is ORed with the PDR configuration bit of the individual DATX8. */
#define MEMC_PHY_DXCCR__DXPDR__SHIFT       4
#define MEMC_PHY_DXCCR__DXPDR__WIDTH       1
#define MEMC_PHY_DXCCR__DXPDR__MASK        0x00000010
#define MEMC_PHY_DXCCR__DXPDR__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DXCCR__DXPDR__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DQSRES - DQS Resistor: Selects the on-die pull-down/pull-up resistor for DQS pins. DQSRES[3] selects pull-down (when set to 0) or pull-up (when set to 1). DQSRES[2:0] selects the resistor value as follows: 000 = Open: On-die resistor disconnected (use external resistor) 001 = 688 ohms 010 = 611 ohms 011 = 550 ohms 100 = 500 ohms 101 = 458 ohms 110 = 393 ohms 111 = 344 ohms */
#define MEMC_PHY_DXCCR__DQSRES__SHIFT       5
#define MEMC_PHY_DXCCR__DQSRES__WIDTH       4
#define MEMC_PHY_DXCCR__DQSRES__MASK        0x000001E0
#define MEMC_PHY_DXCCR__DQSRES__INV_MASK    0xFFFFFE1F
#define MEMC_PHY_DXCCR__DQSRES__HW_DEFAULT  0x4

/* MEMC_PHY_DXCCR.DQSNRES - DQS# Resistor: Selects the on-die pull-up/pull-down resistor for DQS# pins. Same encoding as DQSRES. */
#define MEMC_PHY_DXCCR__DQSNRES__SHIFT       9
#define MEMC_PHY_DXCCR__DQSNRES__WIDTH       4
#define MEMC_PHY_DXCCR__DQSNRES__MASK        0x00001E00
#define MEMC_PHY_DXCCR__DQSNRES__INV_MASK    0xFFFFE1FF
#define MEMC_PHY_DXCCR__DQSNRES__HW_DEFAULT  0xC

/* MEMC_PHY_DXCCR.MSBUDQ - Most Significant Byte Unused DQs: Specifies the number of DQ bits that are not used in the most significant byte. The used (valid) bits for this byte are [8-MSBDQ- 1:0]. To disable the whole byte, use the DXnGCR.DXEN register. */
#define MEMC_PHY_DXCCR__MSBUDQ__SHIFT       15
#define MEMC_PHY_DXCCR__MSBUDQ__WIDTH       3
#define MEMC_PHY_DXCCR__MSBUDQ__MASK        0x00038000
#define MEMC_PHY_DXCCR__MSBUDQ__INV_MASK    0xFFFC7FFF
#define MEMC_PHY_DXCCR__MSBUDQ__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.UDQODT - Unused DQ On-Die Termination: Enables, when set, the on-die termination on the I/O for unused DQ pins. */
#define MEMC_PHY_DXCCR__UDQODT__SHIFT       18
#define MEMC_PHY_DXCCR__UDQODT__WIDTH       1
#define MEMC_PHY_DXCCR__UDQODT__MASK        0x00040000
#define MEMC_PHY_DXCCR__UDQODT__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_DXCCR__UDQODT__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.UDQPDD - Unused DQ Power Down Driver: Powers down, when set, the output driver on the I/O for unused DQ pins. */
#define MEMC_PHY_DXCCR__UDQPDD__SHIFT       19
#define MEMC_PHY_DXCCR__UDQPDD__WIDTH       1
#define MEMC_PHY_DXCCR__UDQPDD__MASK        0x00080000
#define MEMC_PHY_DXCCR__UDQPDD__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_DXCCR__UDQPDD__HW_DEFAULT  0x1

/* MEMC_PHY_DXCCR.UDQPDR - Unused DQ Power Down Receiver: Powers down, when set, the input receiver on the I/O for unused DQ pins. */
#define MEMC_PHY_DXCCR__UDQPDR__SHIFT       20
#define MEMC_PHY_DXCCR__UDQPDR__WIDTH       1
#define MEMC_PHY_DXCCR__UDQPDR__MASK        0x00100000
#define MEMC_PHY_DXCCR__UDQPDR__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_DXCCR__UDQPDR__HW_DEFAULT  0x1

/* MEMC_PHY_DXCCR.UDQIOM - Unused DQ I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for unused DQ pins. */
#define MEMC_PHY_DXCCR__UDQIOM__SHIFT       21
#define MEMC_PHY_DXCCR__UDQIOM__WIDTH       1
#define MEMC_PHY_DXCCR__UDQIOM__MASK        0x00200000
#define MEMC_PHY_DXCCR__UDQIOM__INV_MASK    0xFFDFFFFF
#define MEMC_PHY_DXCCR__UDQIOM__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DYNDXPDD - Dynamic Data Power Down Driver: Dynamically powers down, when set, the output driver on I/O for the DQ pins of the active DATX8 macros. Applies onlywhen DXPDD and DXnGCR.DXPDD are not set to 1. Driver is powered-up on a DFI WRITE command and powered-down (twrlat + WL/2 + n) HDR cycles after the last DFI WRITE command. Note that n is defined by the register bit field DXCCR[27:24] (DDPDDCDO). */
#define MEMC_PHY_DXCCR__DYNDXPDD__SHIFT       22
#define MEMC_PHY_DXCCR__DYNDXPDD__WIDTH       1
#define MEMC_PHY_DXCCR__DYNDXPDD__MASK        0x00400000
#define MEMC_PHY_DXCCR__DYNDXPDD__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_DXCCR__DYNDXPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DYNDXPDR - Data Power Down Receiver: Dynamically powers down, when set, the input receiver on I/O for the DQ pins of the active DATX8 macros. Applies only when DXPDR and DXnGCR.DXPDR are not set to 1. Receiver is powered-up on a DFI READ command and powered-down (trddata_en + fixed_read_latency + n) HDR cycles after the last DFI READ command. Note that n is defined by the register bit field DXCCR[31:28] (DDPDRCDO). */
#define MEMC_PHY_DXCCR__DYNDXPDR__SHIFT       23
#define MEMC_PHY_DXCCR__DYNDXPDR__WIDTH       1
#define MEMC_PHY_DXCCR__DYNDXPDR__MASK        0x00800000
#define MEMC_PHY_DXCCR__DYNDXPDR__INV_MASK    0xFF7FFFFF
#define MEMC_PHY_DXCCR__DYNDXPDR__HW_DEFAULT  0x0

/* MEMC_PHY_DXCCR.DDPDDCDO - Dynamic Data Power Down Driver Count Down Offset: Offset applied in calculating window of time where driver is powered up */
#define MEMC_PHY_DXCCR__DDPDDCDO__SHIFT       24
#define MEMC_PHY_DXCCR__DDPDDCDO__WIDTH       4
#define MEMC_PHY_DXCCR__DDPDDCDO__MASK        0x0F000000
#define MEMC_PHY_DXCCR__DDPDDCDO__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_DXCCR__DDPDDCDO__HW_DEFAULT  0x4

/* MEMC_PHY_DXCCR.DDPDRCDO - Dynamic Data Power Down Receiver Count Down Offset: Offset applied in calculating window of time where receiver is powered up */
#define MEMC_PHY_DXCCR__DDPDRCDO__SHIFT       28
#define MEMC_PHY_DXCCR__DDPDRCDO__WIDTH       4
#define MEMC_PHY_DXCCR__DDPDRCDO__MASK        0xF0000000
#define MEMC_PHY_DXCCR__DDPDRCDO__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_DXCCR__DDPDRCDO__HW_DEFAULT  0x4

/* DDR System General Configuration Register */
/* The DDR System General Configuration Register is used to configure miscellaneous features of the DDR system including the DFI logic in the PUB. */
#define MEMC_PHY_DSGCR            0x10810040

/* MEMC_PHY_DSGCR.PUREN - PHY Update Request Enable: Specifies if set, that the PHY should issue PHYinitiated update request when there is DDL VT drift. */
#define MEMC_PHY_DSGCR__PUREN__SHIFT       0
#define MEMC_PHY_DSGCR__PUREN__WIDTH       1
#define MEMC_PHY_DSGCR__PUREN__MASK        0x00000001
#define MEMC_PHY_DSGCR__PUREN__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DSGCR__PUREN__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.BDISEN - Byte Disable Enable: Specifies if set that the PHY should respond to DFI byte disable request. Otherwise the byte disable from the DFI is ignored in which case bytes can only be disabled using the DXnGCR register. */
#define MEMC_PHY_DSGCR__BDISEN__SHIFT       1
#define MEMC_PHY_DSGCR__BDISEN__WIDTH       1
#define MEMC_PHY_DSGCR__BDISEN__MASK        0x00000002
#define MEMC_PHY_DSGCR__BDISEN__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DSGCR__BDISEN__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.ZUEN - Impedance Update Enable: Specifies, if set, that in addition to DDL VT update, the PHY could also perform impedance calibration (update). Refer to the "Impedance Control Register 0-1 (ZQnCR0-1)" on page 135 bit fields DFICU0, DFICU1 and DFICCU bits to control if an impedance calibration is performed (update) with a DFI controller update request. Refer to the "Impedance Control Register 0-1 (ZQnCR0-1)" on page 135 bit fields DFIPU0 and DFIPU1 bits to control if an impedance calibration is performed (update) with a DFI PHY update request. */
#define MEMC_PHY_DSGCR__ZUEN__SHIFT       2
#define MEMC_PHY_DSGCR__ZUEN__WIDTH       1
#define MEMC_PHY_DSGCR__ZUEN__MASK        0x00000004
#define MEMC_PHY_DSGCR__ZUEN__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DSGCR__ZUEN__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.LPIOPD - Low Power I/O Power Down: Specifies if set that the PHY should respond to the DFI low power opportunity request and power down the I/Os of the byte. */
#define MEMC_PHY_DSGCR__LPIOPD__SHIFT       3
#define MEMC_PHY_DSGCR__LPIOPD__WIDTH       1
#define MEMC_PHY_DSGCR__LPIOPD__MASK        0x00000008
#define MEMC_PHY_DSGCR__LPIOPD__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DSGCR__LPIOPD__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.LPPLLPD - Low Power PLL Power Down: Specifies if set that the PHY should respond to the DFI low power opportunity request and power down the PLL of the byte if the wakeup time request satisfies the PLL lock time. */
#define MEMC_PHY_DSGCR__LPPLLPD__SHIFT       4
#define MEMC_PHY_DSGCR__LPPLLPD__WIDTH       1
#define MEMC_PHY_DSGCR__LPPLLPD__MASK        0x00000010
#define MEMC_PHY_DSGCR__LPPLLPD__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DSGCR__LPPLLPD__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.CUAEN - Controller Update Acknowledge Enable: Specifies, if set, that the PHY should issue controller update acknowledge when the DFI controller update request is asserted. By default the PHY does not acknowledge controller initiated update requests but simply does an update whenever there is a controller update request. This speeds up the update. */
#define MEMC_PHY_DSGCR__CUAEN__SHIFT       5
#define MEMC_PHY_DSGCR__CUAEN__WIDTH       1
#define MEMC_PHY_DSGCR__CUAEN__MASK        0x00000020
#define MEMC_PHY_DSGCR__CUAEN__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DSGCR__CUAEN__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.DQSGX - DQS Gate Extension: Specifies if set that the DQS gating must be extended by two DRAM clock cycles and then re-centered, i.e. one clock cycle extension on either side. */
#define MEMC_PHY_DSGCR__DQSGX__SHIFT       6
#define MEMC_PHY_DSGCR__DQSGX__WIDTH       1
#define MEMC_PHY_DSGCR__DQSGX__MASK        0x00000040
#define MEMC_PHY_DSGCR__DQSGX__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DSGCR__DQSGX__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.BRRMODE - Bypass Rise-to-Rise Mode: Indicates if set that the PHY bypass mode is configured to run in rise-to-rise mode. Otherwise if not set the PHY bypass mode is running in rise-to-fall mode. */
#define MEMC_PHY_DSGCR__BRRMODE__SHIFT       7
#define MEMC_PHY_DSGCR__BRRMODE__WIDTH       1
#define MEMC_PHY_DSGCR__BRRMODE__MASK        0x00000080
#define MEMC_PHY_DSGCR__BRRMODE__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_DSGCR__BRRMODE__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.PUAD - PHY Update Acknowledge Delay: Specifies the number of clock cycles that the indication for the completion of PHY update from the PHY to the controller should be delayed. This essentially delays, by this many clock cycles, the de-assertion of dfi_ctrlup_ack and dfi_phyupd_req signals relative to the time when the delay lines or I/Os are updated. */
#define MEMC_PHY_DSGCR__PUAD__SHIFT       8
#define MEMC_PHY_DSGCR__PUAD__WIDTH       4
#define MEMC_PHY_DSGCR__PUAD__MASK        0x00000F00
#define MEMC_PHY_DSGCR__PUAD__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_DSGCR__PUAD__HW_DEFAULT  0x4

/* MEMC_PHY_DSGCR.DTOODT - DTO On-Die Termination: Enables, when set, the on-die termination on the I/O for DTO pins. */
#define MEMC_PHY_DSGCR__DTOODT__SHIFT       12
#define MEMC_PHY_DSGCR__DTOODT__WIDTH       1
#define MEMC_PHY_DSGCR__DTOODT__MASK        0x00001000
#define MEMC_PHY_DSGCR__DTOODT__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_DSGCR__DTOODT__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.DTOPDD - DTO Power Down Driver: Powers down, when set, the output driver on the I/O for DTO pins. */
#define MEMC_PHY_DSGCR__DTOPDD__SHIFT       13
#define MEMC_PHY_DSGCR__DTOPDD__WIDTH       1
#define MEMC_PHY_DSGCR__DTOPDD__MASK        0x00002000
#define MEMC_PHY_DSGCR__DTOPDD__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_DSGCR__DTOPDD__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.DTOPDR - DTO Power Down Receiver: Powers down, when set, the input receiver on the I/O for DTO pins. */
#define MEMC_PHY_DSGCR__DTOPDR__SHIFT       14
#define MEMC_PHY_DSGCR__DTOPDR__WIDTH       1
#define MEMC_PHY_DSGCR__DTOPDR__MASK        0x00004000
#define MEMC_PHY_DSGCR__DTOPDR__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_DSGCR__DTOPDR__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.DTOIOM - DTO I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for DTO pins. */
#define MEMC_PHY_DSGCR__DTOIOM__SHIFT       15
#define MEMC_PHY_DSGCR__DTOIOM__WIDTH       1
#define MEMC_PHY_DSGCR__DTOIOM__MASK        0x00008000
#define MEMC_PHY_DSGCR__DTOIOM__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_DSGCR__DTOIOM__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.DTOOE - DTO Output Enable: Enables, when set, the output driver on the I/O for DTO pins. */
#define MEMC_PHY_DSGCR__DTOOE__SHIFT       16
#define MEMC_PHY_DSGCR__DTOOE__WIDTH       1
#define MEMC_PHY_DSGCR__DTOOE__MASK        0x00010000
#define MEMC_PHY_DSGCR__DTOOE__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_DSGCR__DTOOE__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.ATOAE - ATO Analog Test Enable: Enables, if set, the analog test output (ATO) I/O. */
#define MEMC_PHY_DSGCR__ATOAE__SHIFT       17
#define MEMC_PHY_DSGCR__ATOAE__WIDTH       1
#define MEMC_PHY_DSGCR__ATOAE__MASK        0x00020000
#define MEMC_PHY_DSGCR__ATOAE__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_DSGCR__ATOAE__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.RRMODE - Rise-to-Rise Mode: Indicates if set that the PHY mission mode is configured to run in rise-to-rise mode. Otherwise if not set the PHY mission modeis running in rise-tofall mode. */
#define MEMC_PHY_DSGCR__RRMODE__SHIFT       18
#define MEMC_PHY_DSGCR__RRMODE__WIDTH       1
#define MEMC_PHY_DSGCR__RRMODE__MASK        0x00040000
#define MEMC_PHY_DSGCR__RRMODE__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_DSGCR__RRMODE__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.SDRMODE - Single Data Rate Mode: Indicates if set that the external controller is configured to run in single data rate (SDR) mode. Otherwise if not set the controller is running in half data rate (HDR) mode. This bit not supported in the current version of the PUB. */
#define MEMC_PHY_DSGCR__SDRMODE__SHIFT       19
#define MEMC_PHY_DSGCR__SDRMODE__WIDTH       1
#define MEMC_PHY_DSGCR__SDRMODE__MASK        0x00080000
#define MEMC_PHY_DSGCR__SDRMODE__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_DSGCR__SDRMODE__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.CKEPDD - CKE Power Down Driver: Powers down, when set, the output driver on the I/O for CKE[3:0] pins. CKEPDD[0] controls the power down for CKE[0], CKEPDD[1] controls the power down for CKE[1], and so on. */
#define MEMC_PHY_DSGCR__CKEPDD__SHIFT       20
#define MEMC_PHY_DSGCR__CKEPDD__WIDTH       1
#define MEMC_PHY_DSGCR__CKEPDD__MASK        0x00100000
#define MEMC_PHY_DSGCR__CKEPDD__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_DSGCR__CKEPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.ODTPDD - ODT Power Down Driver: Powers down, when set, the output driver on the I/O for ODT[3:0] pins. ODTPDD[0] controls the power down for ODT[0], ODTPDD[1] controls the power down for ODT[1], and so on. */
#define MEMC_PHY_DSGCR__ODTPDD__SHIFT       24
#define MEMC_PHY_DSGCR__ODTPDD__WIDTH       1
#define MEMC_PHY_DSGCR__ODTPDD__MASK        0x01000000
#define MEMC_PHY_DSGCR__ODTPDD__INV_MASK    0xFEFFFFFF
#define MEMC_PHY_DSGCR__ODTPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DSGCR.CKOE - SDRAM CK Output Enable: Enables, when set, the output driver on the I/O for SDRAM CK/CK# pins. */
#define MEMC_PHY_DSGCR__CKOE__SHIFT       28
#define MEMC_PHY_DSGCR__CKOE__WIDTH       1
#define MEMC_PHY_DSGCR__CKOE__MASK        0x10000000
#define MEMC_PHY_DSGCR__CKOE__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_DSGCR__CKOE__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.ODTOE - SDRAM ODT Output Enable: Enables, when set, the output driver on the I/O for SDRAM ODT pins. */
#define MEMC_PHY_DSGCR__ODTOE__SHIFT       29
#define MEMC_PHY_DSGCR__ODTOE__WIDTH       1
#define MEMC_PHY_DSGCR__ODTOE__MASK        0x20000000
#define MEMC_PHY_DSGCR__ODTOE__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_DSGCR__ODTOE__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.RSTOE - SDRAM Reset Output Enable: Enables, when set, the output driver on the I/O for SDRAM RST# pin. */
#define MEMC_PHY_DSGCR__RSTOE__SHIFT       30
#define MEMC_PHY_DSGCR__RSTOE__WIDTH       1
#define MEMC_PHY_DSGCR__RSTOE__MASK        0x40000000
#define MEMC_PHY_DSGCR__RSTOE__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_DSGCR__RSTOE__HW_DEFAULT  0x1

/* MEMC_PHY_DSGCR.CKEOE - SDRAM CKE Output Enable: Enables, when set, the output driver on the I/O for SDRAM CKE pins. */
#define MEMC_PHY_DSGCR__CKEOE__SHIFT       31
#define MEMC_PHY_DSGCR__CKEOE__WIDTH       1
#define MEMC_PHY_DSGCR__CKEOE__MASK        0x80000000
#define MEMC_PHY_DSGCR__CKEOE__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_DSGCR__CKEOE__HW_DEFAULT  0x1

/* DRAM Configuration Register */
/* This register is used to configure the DRAM system. */
#define MEMC_PHY_DCR              0x10810044

/* MEMC_PHY_DCR.DDRMD - DDR Mode: SDRAM DDR mode. Valid values are: 000 = Reserved 001 = Reserved 010 = DDR2 011 = DDR3 100 - 111 = Reserved */
#define MEMC_PHY_DCR__DDRMD__SHIFT       0
#define MEMC_PHY_DCR__DDRMD__WIDTH       3
#define MEMC_PHY_DCR__DDRMD__MASK        0x00000007
#define MEMC_PHY_DCR__DDRMD__INV_MASK    0xFFFFFFF8
#define MEMC_PHY_DCR__DDRMD__HW_DEFAULT  0x3

/* MEMC_PHY_DCR.DDR8BNK - DDR 8-Bank: Indicates if set that the SDRAM used has 8 banks. tRPA = tRP+1 and tFAW are used for 8-bank DRAMs, other tRPA = tRP and no tFAW is used. Note that a setting of 1 for DRAMs that have fewer than 8 banks results in correct functionality, but less tight DRAM command spacing for the parameters. */
#define MEMC_PHY_DCR__DDR8BNK__SHIFT       3
#define MEMC_PHY_DCR__DDR8BNK__WIDTH       1
#define MEMC_PHY_DCR__DDR8BNK__MASK        0x00000008
#define MEMC_PHY_DCR__DDR8BNK__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DCR__DDR8BNK__HW_DEFAULT  0x1

/* MEMC_PHY_DCR.PDQ - Primary DQ (DDR3 Only): Specifies the DQ pin in a byte that is designated as a primary pin for Multi-Purpose Register (MPR) reads. Valid values are 0 to 7 for DQ[0] to DQ[7], respectively. */
#define MEMC_PHY_DCR__PDQ__SHIFT       4
#define MEMC_PHY_DCR__PDQ__WIDTH       3
#define MEMC_PHY_DCR__PDQ__MASK        0x00000070
#define MEMC_PHY_DCR__PDQ__INV_MASK    0xFFFFFF8F
#define MEMC_PHY_DCR__PDQ__HW_DEFAULT  0x0

/* MEMC_PHY_DCR.MPRDQ - Multi-Purpose Register (MPR) DQ (DDR3 Only): Specifies the value that is driven on non-primary DQ pins during MPR reads. Valid values are: 0 = Primary DQ drives out the data from MPR (0-1-0-1); non-primary DQs drive '0' 1 = Primary DQ and non-primary DQs all drive the same data from MPR (0-1-0-1) */
#define MEMC_PHY_DCR__MPRDQ__SHIFT       7
#define MEMC_PHY_DCR__MPRDQ__WIDTH       1
#define MEMC_PHY_DCR__MPRDQ__MASK        0x00000080
#define MEMC_PHY_DCR__MPRDQ__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_DCR__MPRDQ__HW_DEFAULT  0x0

/* MEMC_PHY_DCR.BYTEMASK - Byte Mask: Mask applied to all beats of read data on all bytes lanes during read DQS gate training. This allows training to be conducted based on selected bit(s) from the byte lanes. Valid values for each bit are: 0 = Disable compare for that bit 1 = Enable compare for that bit Note that this mask applies in DDR3 MPR operation mode as well and must be in keeping with the PDQ field setting. */
#define MEMC_PHY_DCR__BYTEMASK__SHIFT       10
#define MEMC_PHY_DCR__BYTEMASK__WIDTH       8
#define MEMC_PHY_DCR__BYTEMASK__MASK        0x0003FC00
#define MEMC_PHY_DCR__BYTEMASK__INV_MASK    0xFFFC03FF
#define MEMC_PHY_DCR__BYTEMASK__HW_DEFAULT  0x1

/* MEMC_PHY_DCR.NOSRA - No Simultaneous Rank Access: Specifies if set that simultaneous rank access on the same clock cycle is not allowed. This means that multiple chip select signals should not be asserted at the same time. This may be required on some DIMM systems. */
#define MEMC_PHY_DCR__NOSRA__SHIFT       27
#define MEMC_PHY_DCR__NOSRA__WIDTH       1
#define MEMC_PHY_DCR__NOSRA__MASK        0x08000000
#define MEMC_PHY_DCR__NOSRA__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_DCR__NOSRA__HW_DEFAULT  0x0

/* MEMC_PHY_DCR.DDR2T - DDR 2T Timing: Indicates if set that 2T timing should be used by PUB internally generated SDRAM transactions. */
#define MEMC_PHY_DCR__DDR2T__SHIFT       28
#define MEMC_PHY_DCR__DDR2T__WIDTH       1
#define MEMC_PHY_DCR__DDR2T__MASK        0x10000000
#define MEMC_PHY_DCR__DDR2T__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_DCR__DDR2T__HW_DEFAULT  0x0

/* MEMC_PHY_DCR.UDIMM - Un-buffered DIMM Address Mirroring: Indicates if set that there is address mirroring on the second rank of an un-buffered DIMM (the rank connected to CS#[1]). In this case, the PUB re-scrambles the bank and address when sending mode register commands to the second rank. This only applies to PUB internal SDRAM transactions. Transactions generated by the controller must make its own adjustments when using an un-buffered DIMM. DCR[NOSRA] must be set if address mirroring is enabled. */
#define MEMC_PHY_DCR__UDIMM__SHIFT       29
#define MEMC_PHY_DCR__UDIMM__WIDTH       1
#define MEMC_PHY_DCR__UDIMM__MASK        0x20000000
#define MEMC_PHY_DCR__UDIMM__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_DCR__UDIMM__HW_DEFAULT  0x0

/* DRAM Timing Parameter Register 0 */
/* Various timings for the SDRAM are programmable for compatibility with different speed grades or different vendors. The following tables describe the parameters that can be programmed through the SDRAM Timing Parameters Registers. These timing parameters are in DRAM clock cycles and are derived from corresponding parameters in the SDRAM datasheet divided by the DRAM clock cycle time. Noninteger values should be rounded up to the next integer. All the default values shown in Table 3-22 to Table 3-24 are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. Refer to the DRAM datasheet for detailed description about these timing parameters. */
#define MEMC_PHY_DTPR0            0x10810048

/* MEMC_PHY_DTPR0.tRTP - Internal read to precharge command delay. Valid values are 2 to 15. */
#define MEMC_PHY_DTPR0__T_RTP__SHIFT      0
#define MEMC_PHY_DTPR0__T_RTP__WIDTH      4
#define MEMC_PHY_DTPR0__T_RTP__MASK       0x0000000F
#define MEMC_PHY_DTPR0__T_RTP__INV_MASK   0xFFFFFFF0
#define MEMC_PHY_DTPR0__T_RTP__HW_DEFAULT 0x8

/* MEMC_PHY_DTPR0.tWTR - Internal write to read command delay. Valid values are 1 to 15. */
#define MEMC_PHY_DTPR0__T_WTR__SHIFT      4
#define MEMC_PHY_DTPR0__T_WTR__WIDTH      4
#define MEMC_PHY_DTPR0__T_WTR__MASK       0x000000F0
#define MEMC_PHY_DTPR0__T_WTR__INV_MASK   0xFFFFFF0F
#define MEMC_PHY_DTPR0__T_WTR__HW_DEFAULT 0x8

/* MEMC_PHY_DTPR0.tRP - Precharge command period: The minimum time between a precharge command and any other command. Note that the Controller automatically derives tRPA for 8- bank DDR2 devices by adding 1 to tRP. Valid values are 2 to 15. */
#define MEMC_PHY_DTPR0__T_RP__SHIFT      8
#define MEMC_PHY_DTPR0__T_RP__WIDTH      4
#define MEMC_PHY_DTPR0__T_RP__MASK       0x00000F00
#define MEMC_PHY_DTPR0__T_RP__INV_MASK   0xFFFFF0FF
#define MEMC_PHY_DTPR0__T_RP__HW_DEFAULT 0xE

/* MEMC_PHY_DTPR0.tRCD - Activate to read or write delay. Minimum time from when an activate command is issued to when a read or write to the activated row can be issued. Valid values are 2 to 15. */
#define MEMC_PHY_DTPR0__T_RCD__SHIFT      12
#define MEMC_PHY_DTPR0__T_RCD__WIDTH      4
#define MEMC_PHY_DTPR0__T_RCD__MASK       0x0000F000
#define MEMC_PHY_DTPR0__T_RCD__INV_MASK   0xFFFF0FFF
#define MEMC_PHY_DTPR0__T_RCD__HW_DEFAULT 0xE

/* MEMC_PHY_DTPR0.tRAS - Activate to precharge command delay. Valid values are 2 to 63. */
#define MEMC_PHY_DTPR0__T_RAS__SHIFT      16
#define MEMC_PHY_DTPR0__T_RAS__WIDTH      6
#define MEMC_PHY_DTPR0__T_RAS__MASK       0x003F0000
#define MEMC_PHY_DTPR0__T_RAS__INV_MASK   0xFFC0FFFF
#define MEMC_PHY_DTPR0__T_RAS__HW_DEFAULT 0x24

/* MEMC_PHY_DTPR0.tRRD - Activate to activate command delay (different banks). Valid values are 1 to 15. */
#define MEMC_PHY_DTPR0__T_RRD__SHIFT      22
#define MEMC_PHY_DTPR0__T_RRD__WIDTH      4
#define MEMC_PHY_DTPR0__T_RRD__MASK       0x03C00000
#define MEMC_PHY_DTPR0__T_RRD__INV_MASK   0xFC3FFFFF
#define MEMC_PHY_DTPR0__T_RRD__HW_DEFAULT 0x7

/* MEMC_PHY_DTPR0.tRC - Activate to activate command delay (same bank). Valid values are 2 to 63. */
#define MEMC_PHY_DTPR0__T_RC__SHIFT      26
#define MEMC_PHY_DTPR0__T_RC__WIDTH      6
#define MEMC_PHY_DTPR0__T_RC__MASK       0xFC000000
#define MEMC_PHY_DTPR0__T_RC__INV_MASK   0x03FFFFFF
#define MEMC_PHY_DTPR0__T_RC__HW_DEFAULT 0x32

/* DRAM Timing Parameter Register 1 */
/* Various timings for the SDRAM are programmable for compatibility with different speed grades or different vendors. The following tables describe the parameters that can be programmed through the SDRAM Timing Parameters Registers. These timing parameters are in DRAM clock cycles and are derived from corresponding parameters in the SDRAM datasheet divided by the DRAM clock cycle time. Noninteger values should be rounded up to the next integer. All the default values shown in Table 3-22 to Table 3-24 are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. Refer to the DRAM datasheet for detailed description about these timing parameters. */
#define MEMC_PHY_DTPR1            0x1081004C

/* MEMC_PHY_DTPR1.tMRD - Load mode cycle time: The minimum time between a load mode register command and any other command. For DDR3 this is the minimum time between two load mode register commands. Valid values for DDR2 are 2 to 3. For DDR3, the value used for tMRD is 4 plus the value programmed in these bits, i.e. tMRD value for DDR3 ranges from 4 to 7. */
#define MEMC_PHY_DTPR1__T_MRD__SHIFT      0
#define MEMC_PHY_DTPR1__T_MRD__WIDTH      2
#define MEMC_PHY_DTPR1__T_MRD__MASK       0x00000003
#define MEMC_PHY_DTPR1__T_MRD__INV_MASK   0xFFFFFFFC
#define MEMC_PHY_DTPR1__T_MRD__HW_DEFAULT 0x2

/* MEMC_PHY_DTPR1.tMOD - Load mode update delay (DDR3 only). The minimum time between a load mode register command and a non-load mode register command. Valid values are: 000 = 12 001 = 13 010 = 14 011 = 15 100 = 16 101 = 17 110 - 111 = Reserved */
#define MEMC_PHY_DTPR1__T_MOD__SHIFT      2
#define MEMC_PHY_DTPR1__T_MOD__WIDTH      3
#define MEMC_PHY_DTPR1__T_MOD__MASK       0x0000001C
#define MEMC_PHY_DTPR1__T_MOD__INV_MASK   0xFFFFFFE3
#define MEMC_PHY_DTPR1__T_MOD__HW_DEFAULT 0x4

/* MEMC_PHY_DTPR1.tFAW - 4-bank activate period. No more than 4-bank activate commands may be issued in a given tFAW period. Only applies to 8-bank devices. Valid values are 2 to 63. */
#define MEMC_PHY_DTPR1__T_FAW__SHIFT      5
#define MEMC_PHY_DTPR1__T_FAW__WIDTH      6
#define MEMC_PHY_DTPR1__T_FAW__MASK       0x000007E0
#define MEMC_PHY_DTPR1__T_FAW__INV_MASK   0xFFFFF81F
#define MEMC_PHY_DTPR1__T_FAW__HW_DEFAULT 0x26

/* MEMC_PHY_DTPR1.tRFC - Refresh-to-Refresh: Indicates the minimum time, in clock cycles, between two refresh commands or between a refresh and an active command. This is derived from the minimum refresh interval from the datasheet, tRFC(min), divided by the clock cycle time. The default number of clock cycles is for the largest JEDEC tRFC(min parameter value supported. */
#define MEMC_PHY_DTPR1__T_RFC__SHIFT      11
#define MEMC_PHY_DTPR1__T_RFC__WIDTH      9
#define MEMC_PHY_DTPR1__T_RFC__MASK       0x000FF800
#define MEMC_PHY_DTPR1__T_RFC__INV_MASK   0xFFF007FF
#define MEMC_PHY_DTPR1__T_RFC__HW_DEFAULT 0x176

/* MEMC_PHY_DTPR1.tWLMRD - Minimum delay from when write leveling mode is programmed to the first DQS/DQS# rising edge. */
#define MEMC_PHY_DTPR1__T_WLMRD__SHIFT      20
#define MEMC_PHY_DTPR1__T_WLMRD__WIDTH      6
#define MEMC_PHY_DTPR1__T_WLMRD__MASK       0x03F00000
#define MEMC_PHY_DTPR1__T_WLMRD__INV_MASK   0xFC0FFFFF
#define MEMC_PHY_DTPR1__T_WLMRD__HW_DEFAULT 0x28

/* MEMC_PHY_DTPR1.tWLO - Write leveling output delay: Number of clock cycles from when write leveling DQS is driven high by the control block to when the results from the SDRAM on DQ is sampled by the control block. This must include the SDRAM tWLO timing parameter plus the round trip delay from control block to SDRAM back to control block. */
#define MEMC_PHY_DTPR1__T_WLO__SHIFT      26
#define MEMC_PHY_DTPR1__T_WLO__WIDTH      4
#define MEMC_PHY_DTPR1__T_WLO__MASK       0x3C000000
#define MEMC_PHY_DTPR1__T_WLO__INV_MASK   0xC3FFFFFF
#define MEMC_PHY_DTPR1__T_WLO__HW_DEFAULT 0x8

/* MEMC_PHY_DTPR1.tAOND_tAOFD - ODT turn-on/turn-off delays (DDR2 only). The delays are in clock cycles. Valid values are: 00 = 2/2.5 01 = 3/3.5 10 = 4/4.5 11 = 5/5.5 Most DDR2 devices utilize a fixed value of 2/2.5. For non-standard SDRAMs, the user must ensure that the operational Write Latency is always greater than or equal to the ODT turn-on delay. For example, a DDR2 SDRAM with CAS latency set to 3 and CAS additive latency set to 0 has a Write Latency of 2. Thus 2/2.5 can be used, but not 3/3.5 or higher. */
#define MEMC_PHY_DTPR1__T_AOND_T_AOFD__SHIFT     30
#define MEMC_PHY_DTPR1__T_AOND_T_AOFD__WIDTH     2
#define MEMC_PHY_DTPR1__T_AOND_T_AOFD__MASK      0xC0000000
#define MEMC_PHY_DTPR1__T_AOND_T_AOFD__INV_MASK  0x3FFFFFFF
#define MEMC_PHY_DTPR1__T_AOND_T_AOFD__HW_DEFAULT 0x0

/* DRAM Timing Parameter Register 2 */
/* Various timings for the SDRAM are programmable for compatibility with different speed grades or different vendors. The following tables describe the parameters that can be programmed through the SDRAM Timing Parameters Registers. These timing parameters are in DRAM clock cycles and are derived from corresponding parameters in the SDRAM datasheet divided by the DRAM clock cycle time. Noninteger values should be rounded up to the next integer. All the default values shown in Table 3-22 to Table 3-24 are in decimal format and correspond to the highest speed the PHY can be compiled for, i.e. JEDEC DDR3-2133N speed grade. This corresponds to DRAM bit rate of 2133Mbps, DRAM clock frequency of 1066MHz, controller clock frequency of 533MHz, and configuration or APB clock frequency of 533MHz. Refer to the DRAM datasheet for detailed description about these timing parameters. */
#define MEMC_PHY_DTPR2            0x10810050

/* MEMC_PHY_DTPR2.tXS - Self refresh exit delay. The minimum time between a self refresh exit command and any other command. This parameter must be set to the maximum of the various minimum self refresh exit delay parameters specified in the SDRAM datasheet, i.e. max(tXSNR, tXSRD) for DDR2 and max(tXS, tXSDLL) for DDR3. Valid values are 2 to 1023. */
#define MEMC_PHY_DTPR2__T_XS__SHIFT      0
#define MEMC_PHY_DTPR2__T_XS__WIDTH      10
#define MEMC_PHY_DTPR2__T_XS__MASK       0x000003FF
#define MEMC_PHY_DTPR2__T_XS__INV_MASK   0xFFFFFC00
#define MEMC_PHY_DTPR2__T_XS__HW_DEFAULT 0x200

/* MEMC_PHY_DTPR2.tXP - Power down exit delay. The minimum time between a power down exit command and any other command. This parameter must be set to the maximum of the various minimum power down exit delay parameters specified in the SDRAM datasheet, i.e. max(tXP, tXARD, tXARDS) for DDR2 and max(tXP, tXPDLL) for DDR3. Valid values are 2 to 31. */
#define MEMC_PHY_DTPR2__T_XP__SHIFT      10
#define MEMC_PHY_DTPR2__T_XP__WIDTH      5
#define MEMC_PHY_DTPR2__T_XP__MASK       0x00007C00
#define MEMC_PHY_DTPR2__T_XP__INV_MASK   0xFFFF83FF
#define MEMC_PHY_DTPR2__T_XP__HW_DEFAULT 0x1A

/* MEMC_PHY_DTPR2.tCKE - CKE minimum pulse width. Also specifies the minimum time that the SDRAM must remain in power down or self refresh mode. For DDR3 this parameter must be set to the value of tCKESR which is usually bigger than the value of tCKE. Valid values are 2 to 15. */
#define MEMC_PHY_DTPR2__T_CKE__SHIFT      15
#define MEMC_PHY_DTPR2__T_CKE__WIDTH      4
#define MEMC_PHY_DTPR2__T_CKE__MASK       0x00078000
#define MEMC_PHY_DTPR2__T_CKE__INV_MASK   0xFFF87FFF
#define MEMC_PHY_DTPR2__T_CKE__HW_DEFAULT 0x6

/* MEMC_PHY_DTPR2.tDLLK - DLL locking time. Valid values are 2 to 1023. */
#define MEMC_PHY_DTPR2__T_DLLK__SHIFT      19
#define MEMC_PHY_DTPR2__T_DLLK__WIDTH      10
#define MEMC_PHY_DTPR2__T_DLLK__MASK       0x1FF80000
#define MEMC_PHY_DTPR2__T_DLLK__INV_MASK   0xE007FFFF
#define MEMC_PHY_DTPR2__T_DLLK__HW_DEFAULT 0x200

/* MEMC_PHY_DTPR2.tRTODT - Read to ODT delay (DDR3 only). Specifies whether ODT can be enabled immediately after the read post-amble or one clock delay has to be added. Valid values are: 0 = ODT may be turned on immediately after read post-amble 1 = ODT may not be turned on until one clock after the read post-amble If tRTODT is set to 1, then the read-to-write latency is increased by 1 if ODT is enabled. */
#define MEMC_PHY_DTPR2__T_RTODT__SHIFT      29
#define MEMC_PHY_DTPR2__T_RTODT__WIDTH      1
#define MEMC_PHY_DTPR2__T_RTODT__MASK       0x20000000
#define MEMC_PHY_DTPR2__T_RTODT__INV_MASK   0xDFFFFFFF
#define MEMC_PHY_DTPR2__T_RTODT__HW_DEFAULT 0x0

/* MEMC_PHY_DTPR2.tRTW - Read to Write command delay. Valid values are: 0 = standard bus turn around delay 1 = add 1 clock to standard bus turn around delay This parameter allows the user to increase the delay between issuing Write commands to the SDRAM when preceded by Read commands. This provides an option to increase bus turn-around margin for high frequency systems. */
#define MEMC_PHY_DTPR2__T_RTW__SHIFT      30
#define MEMC_PHY_DTPR2__T_RTW__WIDTH      1
#define MEMC_PHY_DTPR2__T_RTW__MASK       0x40000000
#define MEMC_PHY_DTPR2__T_RTW__INV_MASK   0xBFFFFFFF
#define MEMC_PHY_DTPR2__T_RTW__HW_DEFAULT 0x0

/* MEMC_PHY_DTPR2.tCCD - Read to read and write to write command delay. Valid values are: 0 = BL/2 for DDR2 and 4 for DDR3 1 = BL/2 + 1 for DDR2 and 5 for DDR3 */
#define MEMC_PHY_DTPR2__T_CCD__SHIFT      31
#define MEMC_PHY_DTPR2__T_CCD__WIDTH      1
#define MEMC_PHY_DTPR2__T_CCD__MASK       0x80000000
#define MEMC_PHY_DTPR2__T_CCD__INV_MASK   0x7FFFFFFF
#define MEMC_PHY_DTPR2__T_CCD__HW_DEFAULT 0x0

/* DDR3 Mode Register 0 */
/* SDRAM definitions controlled by the Mode Register 0 (MR0) include, among other things, burst length, burst type, CAS latency, operating (normal or test) mode, DLL reset, write recovery, and power-down modes. The PUB Mode Register 0 (MR0) maps to DDR3 Mode Register 0 (MR0) and DDR2 Mode Register (MR). */
#define MEMC_PHY_MR0              0x10810054

/* MEMC_PHY_MR0.BL - Burst Length: Determines the maximum number of column locations that can be accessed during a given read or write command. Valid values are: Valid values for DDR3 are: 00 = 8 (Fixed) 01 = 4 or 8 (On the fly) 10 = 4 (Fixed) 11 = Reserved */
#define MEMC_PHY_MR0__BL__SHIFT       0
#define MEMC_PHY_MR0__BL__WIDTH       2
#define MEMC_PHY_MR0__BL__MASK        0x00000003
#define MEMC_PHY_MR0__BL__INV_MASK    0xFFFFFFFC
#define MEMC_PHY_MR0__BL__HW_DEFAULT  0x2

/* MEMC_PHY_MR0.CL_0 - see bits[6:4] */
#define MEMC_PHY_MR0__CL_0__SHIFT       2
#define MEMC_PHY_MR0__CL_0__WIDTH       1
#define MEMC_PHY_MR0__CL_0__MASK        0x00000004
#define MEMC_PHY_MR0__CL_0__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_MR0__CL_0__HW_DEFAULT  0x0

/* MEMC_PHY_MR0.BT - Burst Type: Indicates whether a burst is sequential (0) or interleaved (1). */
#define MEMC_PHY_MR0__BT__SHIFT       3
#define MEMC_PHY_MR0__BT__WIDTH       1
#define MEMC_PHY_MR0__BT__MASK        0x00000008
#define MEMC_PHY_MR0__BT__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_MR0__BT__HW_DEFAULT  0x0

/* MEMC_PHY_MR0.CL - CAS Latency, together with CL_0, [6:4,2]: The delay, in clock cycles, between when the SDRAM registers a read command to when data is available. Valid values are: 0010 = 5 0100 = 6 0110 = 7 1000 = 8 1010 = 9 1100 = 10 1110 = 11 0001 = 12 0011 = 13 0101 = 14 All other settings are reserved and should not be used. */
#define MEMC_PHY_MR0__CL__SHIFT       4
#define MEMC_PHY_MR0__CL__WIDTH       3
#define MEMC_PHY_MR0__CL__MASK        0x00000070
#define MEMC_PHY_MR0__CL__INV_MASK    0xFFFFFF8F
#define MEMC_PHY_MR0__CL__HW_DEFAULT  0x5

/* MEMC_PHY_MR0.TM - Operating Mode: Selects either normal operating mode (0) or test mode (1). Test mode is reserved for the manufacturer and should not be used. */
#define MEMC_PHY_MR0__TM__SHIFT       7
#define MEMC_PHY_MR0__TM__WIDTH       1
#define MEMC_PHY_MR0__TM__MASK        0x00000080
#define MEMC_PHY_MR0__TM__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_MR0__TM__HW_DEFAULT  0x0

/* MEMC_PHY_MR0.DR - DLL Reset: Writing a '1' to this bit will reset the SDRAM DLL. This bit is selfclearing, i.e. it returns back to '0' after the DLL reset has been issued. */
#define MEMC_PHY_MR0__DR__SHIFT       8
#define MEMC_PHY_MR0__DR__WIDTH       1
#define MEMC_PHY_MR0__DR__MASK        0x00000100
#define MEMC_PHY_MR0__DR__INV_MASK    0xFFFFFEFF
#define MEMC_PHY_MR0__DR__HW_DEFAULT  0x0

/* MEMC_PHY_MR0.WR - Write Recovery: This is the value of the write recovery in clock cycles. It is calculated by dividing the datasheet write recovery time, tWR (ns) by the datasheet clock cycle time, tCK (ns) and rounding up a non-integer value to the next integer. Valid values are: 000 = 16 001 = 5 010 = 6 011 = 7 100 = 8 101 = 10 110 = 12 111 = 14 All other settings are reserved and should not be used. NOTE: tWR (ns) is the time from the first SDRAM positive clock edge after the last data-in pair of a write command, to when a precharge of the same bank can be issued. */
#define MEMC_PHY_MR0__WR__SHIFT       9
#define MEMC_PHY_MR0__WR__WIDTH       3
#define MEMC_PHY_MR0__WR__MASK        0x00000E00
#define MEMC_PHY_MR0__WR__INV_MASK    0xFFFFF1FF
#define MEMC_PHY_MR0__WR__HW_DEFAULT  0x5

/* MEMC_PHY_MR0.PD - Power-Down Control: Controls the exit time for power-down modes. Refer to the SDRAM datasheet for details on power-down modes. Valid values are: 0 = Slow exit (DLL off) 1 = Fast exit (DLL on) */
#define MEMC_PHY_MR0__PD__SHIFT       12
#define MEMC_PHY_MR0__PD__WIDTH       1
#define MEMC_PHY_MR0__PD__MASK        0x00001000
#define MEMC_PHY_MR0__PD__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_MR0__PD__HW_DEFAULT  0x0

/* MEMC_PHY_MR0.RSVD - Reserved. These are JEDEC reserved bits and are recommended by JEDEC to be programmed to '0'. */
#define MEMC_PHY_MR0__RSVD__SHIFT       13
#define MEMC_PHY_MR0__RSVD__WIDTH       3
#define MEMC_PHY_MR0__RSVD__MASK        0x0000E000
#define MEMC_PHY_MR0__RSVD__INV_MASK    0xFFFF1FFF
#define MEMC_PHY_MR0__RSVD__HW_DEFAULT  0x0

/* DDR3 Mode Register 1 */
/* SDRAM definitions controlled by the Mode register 1 (MR1) include, among other things, DLL enable/disable, drive strength, on-die termination (ODT) resistance, CAS additive latency, off-chip driver (OCD) impedance calibration, DQS_b and RDQS/RDQS_b enable/disable, and output enable. The PUB Mode Register 1 (MR0) maps to DDR3 Mode Register 1 (MR1) and DDR2 Extended Mode Register (EMR)). */
#define MEMC_PHY_MR1              0x10810058

/* MEMC_PHY_MR1.DE - DLL Enable/Disable: Enable (0) or disable (1) the DLL. DLL must be enabled for normal operation. Note: SDRAM DLL off mode is not supported 0 [5, 1] DIC Output Driver Impedance Control: Controls the output drive strength. Valid values are: 00 = RZQ/6 01 = RZQ7 10 = Reserved 11 = Reserved 0 [9, 6,2] RTT On Die Termination: Selects the effective resistance for SDRAM on die termination. Valid values are: 000 = ODT disabled 001 = RZQ/4 010 = RZQ/2 011 = RZQ/6 100 = RZQ/12 101 = RZQ/8 All other settings are reserved and should not be used. */
#define MEMC_PHY_MR1__DE__SHIFT       0
#define MEMC_PHY_MR1__DE__WIDTH       1
#define MEMC_PHY_MR1__DE__MASK        0x00000001
#define MEMC_PHY_MR1__DE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_MR1__DE__HW_DEFAULT  0x0

/* MEMC_PHY_MR1.AL - Posted CAS Additive Latency: Setting additive latency that allows read and write commands to be issued to the SDRAM earlier than normal (refer to the SDRAM datasheet for details). Valid values are: 00 = 0 (AL disabled) 01 = CL - 1 10 = CL - 2 11 = Reserved */
#define MEMC_PHY_MR1__AL__SHIFT       3
#define MEMC_PHY_MR1__AL__WIDTH       2
#define MEMC_PHY_MR1__AL__MASK        0x00000018
#define MEMC_PHY_MR1__AL__INV_MASK    0xFFFFFFE7
#define MEMC_PHY_MR1__AL__HW_DEFAULT  0x0

/* MEMC_PHY_MR1.LEVEL - Write Leveling Enable: Enables write-leveling when set. */
#define MEMC_PHY_MR1__LEVEL__SHIFT       7
#define MEMC_PHY_MR1__LEVEL__WIDTH       1
#define MEMC_PHY_MR1__LEVEL__MASK        0x00000080
#define MEMC_PHY_MR1__LEVEL__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_MR1__LEVEL__HW_DEFAULT  0x0

/* MEMC_PHY_MR1.TDQS - Termination Data Strobe: When enabled ('1') TDQS provides additional termination resistance outputs that may be useful in some system configurations. Refer to the SDRAM datasheet for details. */
#define MEMC_PHY_MR1__TDQS__SHIFT       11
#define MEMC_PHY_MR1__TDQS__WIDTH       1
#define MEMC_PHY_MR1__TDQS__MASK        0x00000800
#define MEMC_PHY_MR1__TDQS__INV_MASK    0xFFFFF7FF
#define MEMC_PHY_MR1__TDQS__HW_DEFAULT  0x0

/* MEMC_PHY_MR1.QOFF - Output Enable/Disable: When '0', all outputs function normal; when '1' all SDRAM outputs are disabled removing output buffer current. This feature is intended to be used for IDD characterization of read current and should not be used in normal operation. */
#define MEMC_PHY_MR1__QOFF__SHIFT       12
#define MEMC_PHY_MR1__QOFF__WIDTH       1
#define MEMC_PHY_MR1__QOFF__MASK        0x00001000
#define MEMC_PHY_MR1__QOFF__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_MR1__QOFF__HW_DEFAULT  0x0

/* MEMC_PHY_MR1.RSVD - Reserved. These are JEDEC reserved bits and are recommended by JEDEC to be programmed to '0'. */
#define MEMC_PHY_MR1__RSVD__SHIFT       13
#define MEMC_PHY_MR1__RSVD__WIDTH       3
#define MEMC_PHY_MR1__RSVD__MASK        0x0000E000
#define MEMC_PHY_MR1__RSVD__INV_MASK    0xFFFF1FFF
#define MEMC_PHY_MR1__RSVD__HW_DEFAULT  0x0

/* DDR3 Mode Register 2 */
/* Mode Register 2 controls among other things refresh related features of SDRAMs. The PUB Mode Register 2 (MR2) maps to DDR3 Mode Register 2 (MR2) and DDR2 Extended Mode Register 2 (EMR2). */
#define MEMC_PHY_MR2              0x1081005C

/* MEMC_PHY_MR2.PASR - Partial Array Self Refresh: Specifies that data located in areas of the array beyond the specified location will be lost if self refresh is entered. Valid settings for 4 banks are: 000 = Full Array 001 = Half Array (BA[1:0] = 00 &amp; 01) 010 = Quarter Array (BA[1:0] = 00) 011 = Not defined 100 = 3/4 Array (BA[1:0] = 01, 10, &amp; 11) 101 = Half Array (BA[1:0] = 10 &amp; 11) 110 = Quarter Array (BA[1:0] = 11) 111 = Not defined Valid settings for 8 banks are: 000 = Full Array 001 = Half Array (BA[2:0] = 000, 001, 010 &amp; 011) 010 = Quarter Array (BA[2:0] = 000, 001) 011 = 1/8 Array (BA[2:0] = 000) 100 = 3/4 Array (BA[2:0] = 010, 011, 100, 101, 110 &amp; 111) 101 = Half Array (BA[2:0] = 100, 101, 110 &amp; 111) 110 = Quarter Array (BA[2:0] = 110 &amp; 111) 111 = 1/8 Array (BA[2:0] 111) */
#define MEMC_PHY_MR2__PASR__SHIFT       0
#define MEMC_PHY_MR2__PASR__WIDTH       3
#define MEMC_PHY_MR2__PASR__MASK        0x00000007
#define MEMC_PHY_MR2__PASR__INV_MASK    0xFFFFFFF8
#define MEMC_PHY_MR2__PASR__HW_DEFAULT  0x0

/* MEMC_PHY_MR2.CWL - CAS Write Latency: The delay, in clock cycles, between when the SDRAM registers a write command to when write data is available. Valid values are: 000 = 5 (tCK > 2.5ns) 001 = 6 (2.5ns > tCK > 1.875ns) 010 = 7 (1.875ns > tCK> 1.5ns) 011 = 8 (1.5ns > tCK > 1.25ns) 100 = 9 (1.25ns > tCK > 1.07ns) 101 = 10 (1.07ns > tCK > 0.935ns) 110 = 11 (0.935ns > tCK > 0.833ns) 111 = 12 (0.833ns > tCK > 0.75ns) All other settings are reserved and should not be used. */
#define MEMC_PHY_MR2__CWL__SHIFT       3
#define MEMC_PHY_MR2__CWL__WIDTH       3
#define MEMC_PHY_MR2__CWL__MASK        0x00000038
#define MEMC_PHY_MR2__CWL__INV_MASK    0xFFFFFFC7
#define MEMC_PHY_MR2__CWL__HW_DEFAULT  0x0

/* MEMC_PHY_MR2.ASR - Auto Self-Refresh: When enabled ('1'), SDRAM automatically provides self-refresh power management functions for all supported operating temperature values. Otherwise the SRT bit must be programmed to indicate the temperature range. */
#define MEMC_PHY_MR2__ASR__SHIFT       6
#define MEMC_PHY_MR2__ASR__WIDTH       1
#define MEMC_PHY_MR2__ASR__MASK        0x00000040
#define MEMC_PHY_MR2__ASR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_MR2__ASR__HW_DEFAULT  0x0

/* MEMC_PHY_MR2.SRT - Self-Refresh Temperature Range: Selects either normal ('0') or extended ('1') operating temperature range during self-refresh. */
#define MEMC_PHY_MR2__SRT__SHIFT       7
#define MEMC_PHY_MR2__SRT__WIDTH       1
#define MEMC_PHY_MR2__SRT__MASK        0x00000080
#define MEMC_PHY_MR2__SRT__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_MR2__SRT__HW_DEFAULT  0x0

/* MEMC_PHY_MR2.RTTWR - Dynamic ODT: Selects RTT for dynamic ODT. Valid values are: 00 = Dynamic ODT off 01 = RZQ/4 10 = RZQ/2 11 = Reserved */
#define MEMC_PHY_MR2__RTTWR__SHIFT       9
#define MEMC_PHY_MR2__RTTWR__WIDTH       2
#define MEMC_PHY_MR2__RTTWR__MASK        0x00000600
#define MEMC_PHY_MR2__RTTWR__INV_MASK    0xFFFFF9FF
#define MEMC_PHY_MR2__RTTWR__HW_DEFAULT  0x0

/* MEMC_PHY_MR2.RSVD - These are JEDEC reserved bits and are recommended by JEDEC to be programmed to '0' */
#define MEMC_PHY_MR2__RSVD__SHIFT       11
#define MEMC_PHY_MR2__RSVD__WIDTH       5
#define MEMC_PHY_MR2__RSVD__MASK        0x0000F800
#define MEMC_PHY_MR2__RSVD__INV_MASK    0xFFFF07FF
#define MEMC_PHY_MR2__RSVD__HW_DEFAULT  0x0

/* DDR3 Mode Register 3 */
/* Mode Register 3 controls, among other things, the multi-purpose register. The PUB Mode Register 3 (MR3) maps to DDR3 Mode Register 3 (MR3) and DDR2 Extended Mode Register 3 (EMR3). */
#define MEMC_PHY_MR3              0x10810060

/* MEMC_PHY_MR3.MPRLOC - Multi-Purpose Register (MPR) Location: Selects MPR data location: Valid value are: 00 = Predefined pattern for system calibration All other settings are reserved and should not be used. */
#define MEMC_PHY_MR3__MPRLOC__SHIFT       0
#define MEMC_PHY_MR3__MPRLOC__WIDTH       2
#define MEMC_PHY_MR3__MPRLOC__MASK        0x00000003
#define MEMC_PHY_MR3__MPRLOC__INV_MASK    0xFFFFFFFC
#define MEMC_PHY_MR3__MPRLOC__HW_DEFAULT  0x0

/* MEMC_PHY_MR3.MPR - Multi-Purpose Register Enable: Enables, if set, that read data should come from the Multi-Purpose Register. Otherwise read data come from the DRAM array. */
#define MEMC_PHY_MR3__MPR__SHIFT       2
#define MEMC_PHY_MR3__MPR__WIDTH       1
#define MEMC_PHY_MR3__MPR__MASK        0x00000004
#define MEMC_PHY_MR3__MPR__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_MR3__MPR__HW_DEFAULT  0x0

/* MEMC_PHY_MR3.RSVD - These are JEDEC reserved bits and are recommended by JEDEC to be programmed to '0' */
#define MEMC_PHY_MR3__RSVD__SHIFT       3
#define MEMC_PHY_MR3__RSVD__WIDTH       13
#define MEMC_PHY_MR3__RSVD__MASK        0x0000FFF8
#define MEMC_PHY_MR3__RSVD__INV_MASK    0xFFFF0007
#define MEMC_PHY_MR3__RSVD__HW_DEFAULT  0x0

/* ODT Configuration Register */
/* This register configures how ODT should be controlled on the different ranks when writing or reading a particular rank. This provides full flexibility on how the overall termination of the SDRAM system is set depending on how many ranks are used and whether the DIMM slots are occupied or not. For example, assume the system is a 2-rank configuration and during Read commands the user wishes to always enable the ODT on the SDRAM which is not providing the Read data. In this case, the user would write "0010" to RDODT0 and "0001" to RDODT1. */
#define MEMC_PHY_ODTCR            0x10810064

/* MEMC_PHY_ODTCR.RDODT0 - Read ODT: Specifies whether ODT should be enabled ('1') or disabled ('0') on each of the up to four ranks when a read command is sent to rank n. RDODT0, RDODT1, RDODT2, and RDODT3 specify ODT settings when a read is to rank 0, rank 1, rank 2, and rank 3, respectively. The four bits of each field each represent a rank, the LSB being rank 0 and the MSB being rank 3. Default is to disable ODT during reads. */
#define MEMC_PHY_ODTCR__RDODT0__SHIFT       0
#define MEMC_PHY_ODTCR__RDODT0__WIDTH       1
#define MEMC_PHY_ODTCR__RDODT0__MASK        0x00000001
#define MEMC_PHY_ODTCR__RDODT0__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_ODTCR__RDODT0__HW_DEFAULT  0x0

/* MEMC_PHY_ODTCR.WRODT0 - Write ODT: Specifies whether ODT should be enabled ('1') or disabled ('0') on each of the up to four ranks when a write command is sent to rank n. WRODT0, WRODT1, WRODT2, and WRODT3 specify ODT settings when a write is to rank 0, rank 1, rank 2, and rank 3, respectively. The four bits of each field each represent a rank, the LSB being rank 0 and the MSB being rank 3. Default is to enable ODT only on rank being written to. */
#define MEMC_PHY_ODTCR__WRODT0__SHIFT       16
#define MEMC_PHY_ODTCR__WRODT0__WIDTH       1
#define MEMC_PHY_ODTCR__WRODT0__MASK        0x00010000
#define MEMC_PHY_ODTCR__WRODT0__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_ODTCR__WRODT0__HW_DEFAULT  0x1

/* Data Training Configuration Register */
/* This register is used to set various configurations for data training. */
#define MEMC_PHY_DTCR             0x10810068

/* MEMC_PHY_DTCR.DTRPTN - Data Training Repeat Number: Repeat number used to confirm stability of DDR write or read */
#define MEMC_PHY_DTCR__DTRPTN__SHIFT       0
#define MEMC_PHY_DTCR__DTRPTN__WIDTH       4
#define MEMC_PHY_DTCR__DTRPTN__MASK        0x0000000F
#define MEMC_PHY_DTCR__DTRPTN__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_DTCR__DTRPTN__HW_DEFAULT  0x7

/* MEMC_PHY_DTCR.DTRANK - Data Training Rank: Select the SDRAM rank to be used during Read DQS gate training, Read/Write Data Bit Deskew, Read/Write Eye Training. */
#define MEMC_PHY_DTCR__DTRANK__SHIFT       4
#define MEMC_PHY_DTCR__DTRANK__WIDTH       2
#define MEMC_PHY_DTCR__DTRANK__MASK        0x00000030
#define MEMC_PHY_DTCR__DTRANK__INV_MASK    0xFFFFFFCF
#define MEMC_PHY_DTCR__DTRANK__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTMPR - Read Data Training Using MPR (DDR3 Only): Specifies, if set, that DQS gate training should use the SDRAM Multi-Purpose Register (MPR) register. Otherwise data-training is performed by first writing to some locations in the SDRAM and then reading them back. */
#define MEMC_PHY_DTCR__DTMPR__SHIFT       6
#define MEMC_PHY_DTCR__DTMPR__WIDTH       1
#define MEMC_PHY_DTCR__DTMPR__MASK        0x00000040
#define MEMC_PHY_DTCR__DTMPR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DTCR__DTMPR__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTCMPD - Read Data Training Compare Data: Specifies, if set, that DQS gate training should also check if the returning read data is correct. Otherwise data-training only checks if the correct number of DQS edges were returned. */
#define MEMC_PHY_DTCR__DTCMPD__SHIFT       7
#define MEMC_PHY_DTCR__DTCMPD__WIDTH       1
#define MEMC_PHY_DTCR__DTCMPD__MASK        0x00000080
#define MEMC_PHY_DTCR__DTCMPD__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_DTCR__DTCMPD__HW_DEFAULT  0x1

/* MEMC_PHY_DTCR.DTWDQM - Training WDQ Margin: Defines how close to 0 or how close to 2*(wdq calibration_value) the WDQ lcdl can be moved during training. Basically defines how much timing margin. */
#define MEMC_PHY_DTCR__DTWDQM__SHIFT       8
#define MEMC_PHY_DTCR__DTWDQM__WIDTH       4
#define MEMC_PHY_DTCR__DTWDQM__MASK        0x00000F00
#define MEMC_PHY_DTCR__DTWDQM__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_DTCR__DTWDQM__HW_DEFAULT  0x5

/* MEMC_PHY_DTCR.DTWBDDM - Data Training Write Bit Deskew Data Mask. If set it enables write bit deskew of the data mask */
#define MEMC_PHY_DTCR__DTWBDDM__SHIFT       12
#define MEMC_PHY_DTCR__DTWBDDM__WIDTH       1
#define MEMC_PHY_DTCR__DTWBDDM__MASK        0x00001000
#define MEMC_PHY_DTCR__DTWBDDM__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_DTCR__DTWBDDM__HW_DEFAULT  0x1

/* MEMC_PHY_DTCR.DTBDC - Data Training Bit Deskew Centering: Enables, if set, eye centering capability during write and read bit deskew training. */
#define MEMC_PHY_DTCR__DTBDC__SHIFT       13
#define MEMC_PHY_DTCR__DTBDC__WIDTH       1
#define MEMC_PHY_DTCR__DTBDC__MASK        0x00002000
#define MEMC_PHY_DTCR__DTBDC__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_DTCR__DTBDC__HW_DEFAULT  0x1

/* MEMC_PHY_DTCR.DTWDQMO - Data Training WDQ Margin Override: If set the Training WDQ Margin value specified in DTCR[11:8] (DTWDQM) is used during data training. Otherwise the value is computed as . of the ddr_clk period measurement found durig calibration of the WDQ LCDL. */
#define MEMC_PHY_DTCR__DTWDQMO__SHIFT       14
#define MEMC_PHY_DTCR__DTWDQMO__WIDTH       1
#define MEMC_PHY_DTCR__DTWDQMO__MASK        0x00004000
#define MEMC_PHY_DTCR__DTWDQMO__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_DTCR__DTWDQMO__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTDBS - Data Training Debug Byte Select: Selects the byte during data training debug mode. */
#define MEMC_PHY_DTCR__DTDBS__SHIFT       16
#define MEMC_PHY_DTCR__DTDBS__WIDTH       4
#define MEMC_PHY_DTCR__DTDBS__MASK        0x000F0000
#define MEMC_PHY_DTCR__DTDBS__INV_MASK    0xFFF0FFFF
#define MEMC_PHY_DTCR__DTDBS__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTDEN - Data Training Debug Enable: Enables, if set, the data training debug mode. */
#define MEMC_PHY_DTCR__DTDEN__SHIFT       20
#define MEMC_PHY_DTCR__DTDEN__WIDTH       1
#define MEMC_PHY_DTCR__DTDEN__MASK        0x00100000
#define MEMC_PHY_DTCR__DTDEN__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_DTCR__DTDEN__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTDSTP - Data Training Debug Step: A write of 1 to this bit steps the data training algorithm through a single step. This bit is self-clearing. */
#define MEMC_PHY_DTCR__DTDSTP__SHIFT       21
#define MEMC_PHY_DTCR__DTDSTP__WIDTH       1
#define MEMC_PHY_DTCR__DTDSTP__MASK        0x00200000
#define MEMC_PHY_DTCR__DTDSTP__INV_MASK    0xFFDFFFFF
#define MEMC_PHY_DTCR__DTDSTP__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTEXD - Data Training Extended Write DQS: Enables, if set, an extended write DQS whereby two additional pulses of DQS are added as post-amble to a burst of writes. Generally this should only be enabled when running read bit deskew with the intention of performing read eye deskew prior to running write leveling adjustment. */
#define MEMC_PHY_DTCR__DTEXD__SHIFT       22
#define MEMC_PHY_DTCR__DTEXD__WIDTH       1
#define MEMC_PHY_DTCR__DTEXD__MASK        0x00400000
#define MEMC_PHY_DTCR__DTEXD__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_DTCR__DTEXD__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.DTEXG - Data Training with Early/Extended Gate: Specifies if set that the DQS gate training should be performed with an early/extended gate as specified in DSGCR.DQSGX. */
#define MEMC_PHY_DTCR__DTEXG__SHIFT       23
#define MEMC_PHY_DTCR__DTEXG__WIDTH       1
#define MEMC_PHY_DTCR__DTEXG__MASK        0x00800000
#define MEMC_PHY_DTCR__DTEXG__INV_MASK    0xFF7FFFFF
#define MEMC_PHY_DTCR__DTEXG__HW_DEFAULT  0x0

/* MEMC_PHY_DTCR.RANKEN - Rank Enable: Specifies the ranks that are enabled for data-training. Bit 0 controls rank 0, bit 1 controls rank 1, bit 2 controls rank 2, and bit 3 controls rank 3. Setting the bit to '1' enables the rank, and setting it to '0' disables the rank. */
#define MEMC_PHY_DTCR__RANKEN__SHIFT       24
#define MEMC_PHY_DTCR__RANKEN__WIDTH       1
#define MEMC_PHY_DTCR__RANKEN__MASK        0x01000000
#define MEMC_PHY_DTCR__RANKEN__INV_MASK    0xFEFFFFFF
#define MEMC_PHY_DTCR__RANKEN__HW_DEFAULT  0x1

/* MEMC_PHY_DTCR.RFSHDT - Refresh During Training: A non-zero value specifies that a burst of refreshes equal to the number specified in this field should be sent to the SDRAM after training each rank except the last rank. */
#define MEMC_PHY_DTCR__RFSHDT__SHIFT       28
#define MEMC_PHY_DTCR__RFSHDT__WIDTH       4
#define MEMC_PHY_DTCR__RFSHDT__MASK        0xF0000000
#define MEMC_PHY_DTCR__RFSHDT__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_DTCR__RFSHDT__HW_DEFAULT  0x9

/* Data Training Address Register 0 */
/* These registers optionally set the addresses used during data training. The data training address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst-length-8 boundary (A[2:0] = DTCOL[2:0] = 000). Note For restrictions on programming the DTAR0-3 registers, see "Data Eye Training" on page 193. */
#define MEMC_PHY_DTAR0            0x1081006C

/* MEMC_PHY_DTAR0.DTCOL - Data Training Column Address: Selects the SDRAM column address to be used during data training. The lower four bits of this address must always be "000". */
#define MEMC_PHY_DTAR0__DTCOL__SHIFT       0
#define MEMC_PHY_DTAR0__DTCOL__WIDTH       12
#define MEMC_PHY_DTAR0__DTCOL__MASK        0x00000FFF
#define MEMC_PHY_DTAR0__DTCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_DTAR0__DTCOL__HW_DEFAULT  0x0

/* MEMC_PHY_DTAR0.DTROW - Data Training Row Address: Selects the SDRAM row address to be used during data training. */
#define MEMC_PHY_DTAR0__DTROW__SHIFT       12
#define MEMC_PHY_DTAR0__DTROW__WIDTH       16
#define MEMC_PHY_DTAR0__DTROW__MASK        0x0FFFF000
#define MEMC_PHY_DTAR0__DTROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_DTAR0__DTROW__HW_DEFAULT  0x0

/* MEMC_PHY_DTAR0.DTBANK - Data Training Bank Address: Selects the SDRAM bank address to be used during data training. */
#define MEMC_PHY_DTAR0__DTBANK__SHIFT       28
#define MEMC_PHY_DTAR0__DTBANK__WIDTH       3
#define MEMC_PHY_DTAR0__DTBANK__MASK        0x70000000
#define MEMC_PHY_DTAR0__DTBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_DTAR0__DTBANK__HW_DEFAULT  0x0

/* Data Training Address Register 1 */
/* These registers optionally set the addresses used during data training. The data training address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst-length-8 boundary (A[2:0] = DTCOL[2:0] = 000). Note For restrictions on programming the DTAR0-3 registers, see "Data Eye Training" on page 193. */
#define MEMC_PHY_DTAR1            0x10810070

/* MEMC_PHY_DTAR1.DTCOL - Data Training Column Address: Selects the SDRAM column address to be used during data training. The lower four bits of this address must always be "000". */
#define MEMC_PHY_DTAR1__DTCOL__SHIFT       0
#define MEMC_PHY_DTAR1__DTCOL__WIDTH       12
#define MEMC_PHY_DTAR1__DTCOL__MASK        0x00000FFF
#define MEMC_PHY_DTAR1__DTCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_DTAR1__DTCOL__HW_DEFAULT  0x8

/* MEMC_PHY_DTAR1.DTROW - Data Training Row Address: Selects the SDRAM row address to be used during data training. */
#define MEMC_PHY_DTAR1__DTROW__SHIFT       12
#define MEMC_PHY_DTAR1__DTROW__WIDTH       16
#define MEMC_PHY_DTAR1__DTROW__MASK        0x0FFFF000
#define MEMC_PHY_DTAR1__DTROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_DTAR1__DTROW__HW_DEFAULT  0x0

/* MEMC_PHY_DTAR1.DTBANK - Data Training Bank Address: Selects the SDRAM bank address to be used during data training. */
#define MEMC_PHY_DTAR1__DTBANK__SHIFT       28
#define MEMC_PHY_DTAR1__DTBANK__WIDTH       3
#define MEMC_PHY_DTAR1__DTBANK__MASK        0x70000000
#define MEMC_PHY_DTAR1__DTBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_DTAR1__DTBANK__HW_DEFAULT  0x0

/* Data Training Address Register 2 */
/* These registers optionally set the addresses used during data training. The data training address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst-length-8 boundary (A[2:0] = DTCOL[2:0] = 000). Note For restrictions on programming the DTAR0-3 registers, see "Data Eye Training" on page 193. */
#define MEMC_PHY_DTAR2            0x10810074

/* MEMC_PHY_DTAR2.DTCOL - Data Training Column Address: Selects the SDRAM column address to be used during data training. The lower four bits of this address must always be "000". */
#define MEMC_PHY_DTAR2__DTCOL__SHIFT       0
#define MEMC_PHY_DTAR2__DTCOL__WIDTH       12
#define MEMC_PHY_DTAR2__DTCOL__MASK        0x00000FFF
#define MEMC_PHY_DTAR2__DTCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_DTAR2__DTCOL__HW_DEFAULT  0x10

/* MEMC_PHY_DTAR2.DTROW - Data Training Row Address: Selects the SDRAM row address to be used during data training. */
#define MEMC_PHY_DTAR2__DTROW__SHIFT       12
#define MEMC_PHY_DTAR2__DTROW__WIDTH       16
#define MEMC_PHY_DTAR2__DTROW__MASK        0x0FFFF000
#define MEMC_PHY_DTAR2__DTROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_DTAR2__DTROW__HW_DEFAULT  0x0

/* MEMC_PHY_DTAR2.DTBANK - Data Training Bank Address: Selects the SDRAM bank address to be used during data training. */
#define MEMC_PHY_DTAR2__DTBANK__SHIFT       28
#define MEMC_PHY_DTAR2__DTBANK__WIDTH       3
#define MEMC_PHY_DTAR2__DTBANK__MASK        0x70000000
#define MEMC_PHY_DTAR2__DTBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_DTAR2__DTBANK__HW_DEFAULT  0x0

/* Data Training Address Register 3 */
/* These registers optionally set the addresses used during data training. The data training address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst-length-8 boundary (A[2:0] = DTCOL[2:0] = 000). Note For restrictions on programming the DTAR0-3 registers, see "Data Eye Training" on page 193. */
#define MEMC_PHY_DTAR3            0x10810078

/* MEMC_PHY_DTAR3.DTCOL - Data Training Column Address: Selects the SDRAM column address to be used during data training. The lower four bits of this address must always be "000". */
#define MEMC_PHY_DTAR3__DTCOL__SHIFT       0
#define MEMC_PHY_DTAR3__DTCOL__WIDTH       12
#define MEMC_PHY_DTAR3__DTCOL__MASK        0x00000FFF
#define MEMC_PHY_DTAR3__DTCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_DTAR3__DTCOL__HW_DEFAULT  0x18

/* MEMC_PHY_DTAR3.DTROW - Data Training Row Address: Selects the SDRAM row address to be used during data training. */
#define MEMC_PHY_DTAR3__DTROW__SHIFT       12
#define MEMC_PHY_DTAR3__DTROW__WIDTH       16
#define MEMC_PHY_DTAR3__DTROW__MASK        0x0FFFF000
#define MEMC_PHY_DTAR3__DTROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_DTAR3__DTROW__HW_DEFAULT  0x0

/* MEMC_PHY_DTAR3.DTBANK - Data Training Bank Address: Selects the SDRAM bank address to be used during data training. */
#define MEMC_PHY_DTAR3__DTBANK__SHIFT       28
#define MEMC_PHY_DTAR3__DTBANK__WIDTH       3
#define MEMC_PHY_DTAR3__DTBANK__MASK        0x70000000
#define MEMC_PHY_DTAR3__DTBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_DTAR3__DTBANK__HW_DEFAULT  0x0

/* Data Training Data Register 0 */
/* These registers optionally set the 8 data bytes used during data training. Data training uses a total burst length of 8, i.e. one access for burst length of 8, two accesses for burst length of 4 and four accesses for burst length of 2. The data set in DTDR registers is used as the data for each of the 8 data cycles. The data for the first 4 clock cycles (DTBYTE0 to DTBYTE3) come from DTDR0 register, and the data for the last 4 cy les (DTBYT4 to DTBYTE7) come from DTDR1 register. */
#define MEMC_PHY_DTDR0            0x1081007C

/* MEMC_PHY_DTDR0.DTBYTE0 - Data Training Data: The first 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR0__DTBYTE0__SHIFT       0
#define MEMC_PHY_DTDR0__DTBYTE0__WIDTH       8
#define MEMC_PHY_DTDR0__DTBYTE0__MASK        0x000000FF
#define MEMC_PHY_DTDR0__DTBYTE0__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DTDR0__DTBYTE0__HW_DEFAULT  0x11

/* MEMC_PHY_DTDR0.DTBYTE1 - Data Training Data: The first 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR0__DTBYTE1__SHIFT       8
#define MEMC_PHY_DTDR0__DTBYTE1__WIDTH       8
#define MEMC_PHY_DTDR0__DTBYTE1__MASK        0x0000FF00
#define MEMC_PHY_DTDR0__DTBYTE1__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DTDR0__DTBYTE1__HW_DEFAULT  0xEE

/* MEMC_PHY_DTDR0.DTBYTE2 - Data Training Data: The first 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR0__DTBYTE2__SHIFT       16
#define MEMC_PHY_DTDR0__DTBYTE2__WIDTH       8
#define MEMC_PHY_DTDR0__DTBYTE2__MASK        0x00FF0000
#define MEMC_PHY_DTDR0__DTBYTE2__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DTDR0__DTBYTE2__HW_DEFAULT  0x22

/* MEMC_PHY_DTDR0.DTBYTE3 - Data Training Data: The first 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR0__DTBYTE3__SHIFT       24
#define MEMC_PHY_DTDR0__DTBYTE3__WIDTH       8
#define MEMC_PHY_DTDR0__DTBYTE3__MASK        0xFF000000
#define MEMC_PHY_DTDR0__DTBYTE3__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DTDR0__DTBYTE3__HW_DEFAULT  0xDD

/* Data Training Data Register 1 */
/* These registers optionally set the 8 data bytes used during data training. Data training uses a total burst length of 8, i.e. one access for burst length of 8, two accesses for burst length of 4 and four accesses for burst length of 2. The data set in DTDR registers is used as the data for each of the 8 data cycles. The data for the first 4 clock cycles (DTBYTE0 to DTBYTE3) come from DTDR0 register, and the data for the last 4 cy les (DTBYT4 to DTBYTE7) come from DTDR1 register. */
#define MEMC_PHY_DTDR1            0x10810080

/* MEMC_PHY_DTDR1.DTBYTE4 - Data Training Data: The second 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR1__DTBYTE4__SHIFT       0
#define MEMC_PHY_DTDR1__DTBYTE4__WIDTH       8
#define MEMC_PHY_DTDR1__DTBYTE4__MASK        0x000000FF
#define MEMC_PHY_DTDR1__DTBYTE4__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DTDR1__DTBYTE4__HW_DEFAULT  0x44

/* MEMC_PHY_DTDR1.DTBYTE5 - Data Training Data: The second 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR1__DTBYTE5__SHIFT       8
#define MEMC_PHY_DTDR1__DTBYTE5__WIDTH       8
#define MEMC_PHY_DTDR1__DTBYTE5__MASK        0x0000FF00
#define MEMC_PHY_DTDR1__DTBYTE5__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DTDR1__DTBYTE5__HW_DEFAULT  0xBB

/* MEMC_PHY_DTDR1.DTBYTE6 - Data Training Data: The second 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR1__DTBYTE6__SHIFT       16
#define MEMC_PHY_DTDR1__DTBYTE6__WIDTH       8
#define MEMC_PHY_DTDR1__DTBYTE6__MASK        0x00FF0000
#define MEMC_PHY_DTDR1__DTBYTE6__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DTDR1__DTBYTE6__HW_DEFAULT  0x88

/* MEMC_PHY_DTDR1.DTBYTE7 - Data Training Data: The second 4 bytes of data used during data training. This same data byte is used for each Byte Lane. Default sequence is a walking 1 while toggling data every data cycle. */
#define MEMC_PHY_DTDR1__DTBYTE7__SHIFT       24
#define MEMC_PHY_DTDR1__DTBYTE7__WIDTH       8
#define MEMC_PHY_DTDR1__DTBYTE7__MASK        0xFF000000
#define MEMC_PHY_DTDR1__DTBYTE7__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DTDR1__DTBYTE7__HW_DEFAULT  0x77

/* Data Training Eye Data Register 0 */
/* These registers are updated after successfully running write data eye training and read data eye training respectively. The write data eye center is determined by the formula (DTWBMX + DTWLMX + DTWLMN - DTWBMN) / 2, and the read data eye center is determined by the formula (DTRBMX + DTRLMX + DTRLMN - DTRBMN) / 2. */
#define MEMC_PHY_DTEDR0           0x10810084

/* MEMC_PHY_DTEDR0.DTWLMN - Data Training WDQ LCDL Minimum. */
#define MEMC_PHY_DTEDR0__DTWLMN__SHIFT       0
#define MEMC_PHY_DTEDR0__DTWLMN__WIDTH       8
#define MEMC_PHY_DTEDR0__DTWLMN__MASK        0x000000FF
#define MEMC_PHY_DTEDR0__DTWLMN__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DTEDR0__DTWLMN__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR0.DTWLMX - Data Training WDQ LCDL Maximum. */
#define MEMC_PHY_DTEDR0__DTWLMX__SHIFT       8
#define MEMC_PHY_DTEDR0__DTWLMX__WIDTH       8
#define MEMC_PHY_DTEDR0__DTWLMX__MASK        0x0000FF00
#define MEMC_PHY_DTEDR0__DTWLMX__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DTEDR0__DTWLMX__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR0.DTWBMN - Data Training Write BDL Shift Minimum. */
#define MEMC_PHY_DTEDR0__DTWBMN__SHIFT       16
#define MEMC_PHY_DTEDR0__DTWBMN__WIDTH       8
#define MEMC_PHY_DTEDR0__DTWBMN__MASK        0x00FF0000
#define MEMC_PHY_DTEDR0__DTWBMN__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DTEDR0__DTWBMN__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR0.DTWBMX - Data Training Write BDL Shift Maximum. */
#define MEMC_PHY_DTEDR0__DTWBMX__SHIFT       24
#define MEMC_PHY_DTEDR0__DTWBMX__WIDTH       8
#define MEMC_PHY_DTEDR0__DTWBMX__MASK        0xFF000000
#define MEMC_PHY_DTEDR0__DTWBMX__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DTEDR0__DTWBMX__HW_DEFAULT  0x0

/* Data Training Eye Data Register 1 */
/* These registers are updated after successfully running write data eye training and read data eye training respectively. The write data eye center is determined by the formula (DTWBMX + DTWLMX + DTWLMN - DTWBMN) / 2, and the read data eye center is determined by the formula (DTRBMX + DTRLMX + DTRLMN - DTRBMN) / 2. */
#define MEMC_PHY_DTEDR1           0x10810088

/* MEMC_PHY_DTEDR1.DTRLMN - Data Training RDQS LCDL Minimum. */
#define MEMC_PHY_DTEDR1__DTRLMN__SHIFT       0
#define MEMC_PHY_DTEDR1__DTRLMN__WIDTH       8
#define MEMC_PHY_DTEDR1__DTRLMN__MASK        0x000000FF
#define MEMC_PHY_DTEDR1__DTRLMN__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DTEDR1__DTRLMN__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR1.DTRLMX - Data Training RDQS LCDL Maximum. */
#define MEMC_PHY_DTEDR1__DTRLMX__SHIFT       8
#define MEMC_PHY_DTEDR1__DTRLMX__WIDTH       8
#define MEMC_PHY_DTEDR1__DTRLMX__MASK        0x0000FF00
#define MEMC_PHY_DTEDR1__DTRLMX__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DTEDR1__DTRLMX__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR1.DTRBMN - Data Training Read BDL Shift Minimum. */
#define MEMC_PHY_DTEDR1__DTRBMN__SHIFT       16
#define MEMC_PHY_DTEDR1__DTRBMN__WIDTH       8
#define MEMC_PHY_DTEDR1__DTRBMN__MASK        0x00FF0000
#define MEMC_PHY_DTEDR1__DTRBMN__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DTEDR1__DTRBMN__HW_DEFAULT  0x0

/* MEMC_PHY_DTEDR1.DTRBMX - Data Training Read BDL Shift Maximum. */
#define MEMC_PHY_DTEDR1__DTRBMX__SHIFT       24
#define MEMC_PHY_DTEDR1__DTRBMX__WIDTH       8
#define MEMC_PHY_DTEDR1__DTRBMX__MASK        0xFF000000
#define MEMC_PHY_DTEDR1__DTRBMX__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DTEDR1__DTRBMX__HW_DEFAULT  0x0

/* PHY General Configuration Register 2 */
/* These registers are used for miscellaneous PHY configurations such as enabling VT drift compensation and setting up write-leveling. */
#define MEMC_PHY_PGCR2            0x1081008C

/* MEMC_PHY_PGCR2.tREFPRD - Refresh Period: Indicates the period in clock cycles after which the PUB has to issue a refresh command to the SDRAM. This is derived from the maximum refresh interval from the datasheet, tRFC(max) or REFI, divided by the clock cycle time. A further 400 clocks must be subtracted from the derived number to account for command flow and missed slots of refreshes in the internal PUB blocks. The default corresponds to DDR3 9*7.8us at 1066MHz when a burst of 9 refreshes are issued at every refresh interval. */
#define MEMC_PHY_PGCR2__T_REFPRD__SHIFT      0
#define MEMC_PHY_PGCR2__T_REFPRD__WIDTH      18
#define MEMC_PHY_PGCR2__T_REFPRD__MASK       0x0003FFFF
#define MEMC_PHY_PGCR2__T_REFPRD__INV_MASK   0xFFFC0000
#define MEMC_PHY_PGCR2__T_REFPRD__HW_DEFAULT 0x12480

/* MEMC_PHY_PGCR2.NOBUB - No Bubbles: Specified whether reads should be returned to the controller with no bubbles. Enabling no-bubble reads increases the read latency. Valid values are: 0 = Bubbles are allowed during reads 1 = Bubbles are not allowed during reads */
#define MEMC_PHY_PGCR2__NOBUB__SHIFT       18
#define MEMC_PHY_PGCR2__NOBUB__WIDTH       1
#define MEMC_PHY_PGCR2__NOBUB__MASK        0x00040000
#define MEMC_PHY_PGCR2__NOBUB__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_PGCR2__NOBUB__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR2.FXDLAT - Fixed Latency: Specified whether all reads should be returned to the controller with a fixed read latency. Enabling fixed read latency increases the read latency. Valid values are: 0 = Disable fixed read latency 1 = Enable fixed read latency Fixed read latency is calculated as (12 + (maximum DXnGTR.RxDGSL)/2) HDR clock cycles */
#define MEMC_PHY_PGCR2__FXDLAT__SHIFT       19
#define MEMC_PHY_PGCR2__FXDLAT__WIDTH       1
#define MEMC_PHY_PGCR2__FXDLAT__MASK        0x00080000
#define MEMC_PHY_PGCR2__FXDLAT__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_PGCR2__FXDLAT__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR2.DTPMXTMR - Data Training PUB Mode Timer Exit: Specifies the number of controller clocks to wait when entering and exiting pub mode data training. The default value ensures controller refreshes do not cause memory model errors when entering and exiting data training. The value should be increased if controller initiated SDRAM ZQ short or long operation may occur just before or just after the execution of data training. */
#define MEMC_PHY_PGCR2__DTPMXTMR__SHIFT       20
#define MEMC_PHY_PGCR2__DTPMXTMR__WIDTH       8
#define MEMC_PHY_PGCR2__DTPMXTMR__MASK        0x0FF00000
#define MEMC_PHY_PGCR2__DTPMXTMR__INV_MASK    0xF00FFFFF
#define MEMC_PHY_PGCR2__DTPMXTMR__HW_DEFAULT  0xF

/* MEMC_PHY_PGCR2.SHRAC - Shared-AC mode: set to 1 to enable shared address/command mode with two independent data channels - available only if shared address/command mode support is compiled in. */
#define MEMC_PHY_PGCR2__SHRAC__SHIFT       28
#define MEMC_PHY_PGCR2__SHRAC__WIDTH       1
#define MEMC_PHY_PGCR2__SHRAC__MASK        0x10000000
#define MEMC_PHY_PGCR2__SHRAC__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_PGCR2__SHRAC__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR2.ACPDDC - AC Power-Down with Dual Channels : Set to 1 to power-down address/command lane when both data channels are powered-down. Only valid in shared-AC mode. */
#define MEMC_PHY_PGCR2__ACPDDC__SHIFT       29
#define MEMC_PHY_PGCR2__ACPDDC__WIDTH       1
#define MEMC_PHY_PGCR2__ACPDDC__MASK        0x20000000
#define MEMC_PHY_PGCR2__ACPDDC__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_PGCR2__ACPDDC__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR2.LPMSTRC0 - Low-Power Master Channel 0: set to 1 to have channel 0 act as master to drive channel 1 low-power functions simultaneously. Only valid in shared-AC mode. */
#define MEMC_PHY_PGCR2__LPMSTRC0__SHIFT       30
#define MEMC_PHY_PGCR2__LPMSTRC0__WIDTH       1
#define MEMC_PHY_PGCR2__LPMSTRC0__MASK        0x40000000
#define MEMC_PHY_PGCR2__LPMSTRC0__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_PGCR2__LPMSTRC0__HW_DEFAULT  0x0

/* MEMC_PHY_PGCR2.DYNACPDD - Dynamic AC Power Down Driver: Powers down, when set, the output driver on I/O for ADDR and BA. This bit is ORed with bit ACIOCR[3] (ACPDD). */
#define MEMC_PHY_PGCR2__DYNACPDD__SHIFT       31
#define MEMC_PHY_PGCR2__DYNACPDD__WIDTH       1
#define MEMC_PHY_PGCR2__DYNACPDD__MASK        0x80000000
#define MEMC_PHY_PGCR2__DYNACPDD__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_PGCR2__DYNACPDD__HW_DEFAULT  0x0

/* RDIMM General Configuration Register 0 */
/* RDIMM General Configuration Registers are used to configure and control the PUB and the PHY for RDIMM support. The following tables describe the bits of the RDIMMPGR registers. RDIMMGCR0 have several register controls for the RDIMM-specific I/Os (PAR_IN, ERROUT#, QCSEN#, and MIRROR). Only those controls that require individualized control (such as power-down, termination, and I/O mode) is included in the register. Other common I/O controls, such as slew rate, I/O DDR mode, and I/O loop-back are controlled from common registers such as ACIOCR and PGCR1. */
#define MEMC_PHY_RDIMMGCR0        0x108100B0

/* MEMC_PHY_RDIMMGCR0.RDIMM - Registered DIMM: Indicates if set that a registered DIMM is used. In this case, the PUB increases the SDRAM write and read latencies (WL/RL) by 1 and also enforces that accesses adhere to RDIMM buffer chip. This only applies to PUB internal SDRAM transactions. Transactions generated by the controller must make its own adjustments to WL/RL when using a registered DIMM. The DCR.NOSRA register bit must be set to '1' if using the standard RDIMM buffer chip so that normal DRAM accesses do not assert multiple chip select bits at the same time. */
#define MEMC_PHY_RDIMMGCR0__RDIMM__SHIFT       0
#define MEMC_PHY_RDIMMGCR0__RDIMM__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__RDIMM__MASK        0x00000001
#define MEMC_PHY_RDIMMGCR0__RDIMM__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_RDIMMGCR0__RDIMM__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.ERRNOREG - Parity Error No Registering: Indicates, if set, that parity error signal from the RDIMM should be passed to the DFI controller without any synchronization or registering. Otherwise, the error signal is synchronized as shown in Figure 4-28 on page 245. */
#define MEMC_PHY_RDIMMGCR0__ERRNOREG__SHIFT       1
#define MEMC_PHY_RDIMMGCR0__ERRNOREG__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERRNOREG__MASK        0x00000002
#define MEMC_PHY_RDIMMGCR0__ERRNOREG__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_RDIMMGCR0__ERRNOREG__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.SOPERR - Stop On Parity Error: Indicates, if set, that the PUB is to stop driving commands to the DRAM upon encountering a parity error. Transactions can resume only after status is cleared via PIR.CLRSR. */
#define MEMC_PHY_RDIMMGCR0__SOPERR__SHIFT       2
#define MEMC_PHY_RDIMMGCR0__SOPERR__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__SOPERR__MASK        0x00000004
#define MEMC_PHY_RDIMMGCR0__SOPERR__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_RDIMMGCR0__SOPERR__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.PARINODT - PAR_IN On-Die Termination: Enables, when set, the on-die termination on the I/O for PAR_IN pin. */
#define MEMC_PHY_RDIMMGCR0__PARINODT__SHIFT       14
#define MEMC_PHY_RDIMMGCR0__PARINODT__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__PARINODT__MASK        0x00004000
#define MEMC_PHY_RDIMMGCR0__PARINODT__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_RDIMMGCR0__PARINODT__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.PARINPDD - PAR_IN Power Down Driver: Powers down, when set, the output driver on the I/O for PAR_IN pin. */
#define MEMC_PHY_RDIMMGCR0__PARINPDD__SHIFT       15
#define MEMC_PHY_RDIMMGCR0__PARINPDD__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__PARINPDD__MASK        0x00008000
#define MEMC_PHY_RDIMMGCR0__PARINPDD__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_RDIMMGCR0__PARINPDD__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.PARINPDR - PAR_IN Power Down Receiver: Powers down, when set, the input receiver on the I/O for PAR_IN pin. */
#define MEMC_PHY_RDIMMGCR0__PARINPDR__SHIFT       16
#define MEMC_PHY_RDIMMGCR0__PARINPDR__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__PARINPDR__MASK        0x00010000
#define MEMC_PHY_RDIMMGCR0__PARINPDR__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_RDIMMGCR0__PARINPDR__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.PARINIOM - PAR_IN I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for PAR_IN pin. */
#define MEMC_PHY_RDIMMGCR0__PARINIOM__SHIFT       17
#define MEMC_PHY_RDIMMGCR0__PARINIOM__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__PARINIOM__MASK        0x00020000
#define MEMC_PHY_RDIMMGCR0__PARINIOM__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_RDIMMGCR0__PARINIOM__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.PARINOE - PAR_IN Output Enable: Enables, when set, the output driver on the I/O for PAR_IN pin. */
#define MEMC_PHY_RDIMMGCR0__PARINOE__SHIFT       18
#define MEMC_PHY_RDIMMGCR0__PARINOE__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__PARINOE__MASK        0x00040000
#define MEMC_PHY_RDIMMGCR0__PARINOE__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_RDIMMGCR0__PARINOE__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.ERROUTODT - ERROUT# On-Die Termination: Enables, when set, the on-die termination on the I/O for ERROUT# pin. */
#define MEMC_PHY_RDIMMGCR0__ERROUTODT__SHIFT       19
#define MEMC_PHY_RDIMMGCR0__ERROUTODT__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERROUTODT__MASK        0x00080000
#define MEMC_PHY_RDIMMGCR0__ERROUTODT__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_RDIMMGCR0__ERROUTODT__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.ERROUTPDD - ERROUT# Power Down Driver: Powers down, when set, the output driver on the I/O for ERROUT# pin. */
#define MEMC_PHY_RDIMMGCR0__ERROUTPDD__SHIFT       20
#define MEMC_PHY_RDIMMGCR0__ERROUTPDD__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERROUTPDD__MASK        0x00100000
#define MEMC_PHY_RDIMMGCR0__ERROUTPDD__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_RDIMMGCR0__ERROUTPDD__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.ERROUTPDR - ERROUT# Power Down Receiver: Powers down, when set, the input receiver on the I/O for ERROUT# pin. */
#define MEMC_PHY_RDIMMGCR0__ERROUTPDR__SHIFT       21
#define MEMC_PHY_RDIMMGCR0__ERROUTPDR__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERROUTPDR__MASK        0x00200000
#define MEMC_PHY_RDIMMGCR0__ERROUTPDR__INV_MASK    0xFFDFFFFF
#define MEMC_PHY_RDIMMGCR0__ERROUTPDR__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.ERROUTIOM - ERROUT# I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for ERROUT# pin. */
#define MEMC_PHY_RDIMMGCR0__ERROUTIOM__SHIFT       22
#define MEMC_PHY_RDIMMGCR0__ERROUTIOM__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERROUTIOM__MASK        0x00400000
#define MEMC_PHY_RDIMMGCR0__ERROUTIOM__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_RDIMMGCR0__ERROUTIOM__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.ERROUTOE - ERROUT# Output Enable: Enables, when set, the output driver on the I/O for ERROUT# pin. */
#define MEMC_PHY_RDIMMGCR0__ERROUTOE__SHIFT       23
#define MEMC_PHY_RDIMMGCR0__ERROUTOE__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__ERROUTOE__MASK        0x00800000
#define MEMC_PHY_RDIMMGCR0__ERROUTOE__INV_MASK    0xFF7FFFFF
#define MEMC_PHY_RDIMMGCR0__ERROUTOE__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.RDIMMODT - RDIMM Outputs On-Die Termination: Enables, when set, the on-die termination on the I/O for QCSEN# and MIRROR pins. */
#define MEMC_PHY_RDIMMGCR0__RDIMMODT__SHIFT       24
#define MEMC_PHY_RDIMMGCR0__RDIMMODT__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__RDIMMODT__MASK        0x01000000
#define MEMC_PHY_RDIMMGCR0__RDIMMODT__INV_MASK    0xFEFFFFFF
#define MEMC_PHY_RDIMMGCR0__RDIMMODT__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.RDIMMPDD - RDIMM Outputs Power Down Driver: Powers down, when set, the output driver on the I/O for QCSEN# and MIRROR pins. */
#define MEMC_PHY_RDIMMGCR0__RDIMMPDD__SHIFT       25
#define MEMC_PHY_RDIMMGCR0__RDIMMPDD__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__RDIMMPDD__MASK        0x02000000
#define MEMC_PHY_RDIMMGCR0__RDIMMPDD__INV_MASK    0xFDFFFFFF
#define MEMC_PHY_RDIMMGCR0__RDIMMPDD__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.RDIMMPDR - RDIMM Outputs Power Down Receiver: Powers down, when set, the input receiver on the I/O for QCSEN# and MIRROR pins. */
#define MEMC_PHY_RDIMMGCR0__RDIMMPDR__SHIFT       26
#define MEMC_PHY_RDIMMGCR0__RDIMMPDR__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__RDIMMPDR__MASK        0x04000000
#define MEMC_PHY_RDIMMGCR0__RDIMMPDR__INV_MASK    0xFBFFFFFF
#define MEMC_PHY_RDIMMGCR0__RDIMMPDR__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.RDIMMIOM - RDIMM Outputs I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for QCSEN# and MIRROR pins. */
#define MEMC_PHY_RDIMMGCR0__RDIMMIOM__SHIFT       27
#define MEMC_PHY_RDIMMGCR0__RDIMMIOM__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__RDIMMIOM__MASK        0x08000000
#define MEMC_PHY_RDIMMGCR0__RDIMMIOM__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_RDIMMGCR0__RDIMMIOM__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.QCSENOE - QCSEN# Output Enable: Enables, when set, the output driver on the I/O for QCSEN# pin. */
#define MEMC_PHY_RDIMMGCR0__QCSENOE__SHIFT       28
#define MEMC_PHY_RDIMMGCR0__QCSENOE__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__QCSENOE__MASK        0x10000000
#define MEMC_PHY_RDIMMGCR0__QCSENOE__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_RDIMMGCR0__QCSENOE__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.MIRROROE - MIRROR Output Enable: Enables, when set, the output driver on the I/O for MIRROR pin. */
#define MEMC_PHY_RDIMMGCR0__MIRROROE__SHIFT       29
#define MEMC_PHY_RDIMMGCR0__MIRROROE__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__MIRROROE__MASK        0x20000000
#define MEMC_PHY_RDIMMGCR0__MIRROROE__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_RDIMMGCR0__MIRROROE__HW_DEFAULT  0x1

/* MEMC_PHY_RDIMMGCR0.QCSEN - RDMIMM Quad CS Enable: Enables, if set, the Quad CS mode for the RDIMM registering buffer chip. This register bit controls the buffer chip QCSEN# signal. */
#define MEMC_PHY_RDIMMGCR0__QCSEN__SHIFT       30
#define MEMC_PHY_RDIMMGCR0__QCSEN__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__QCSEN__MASK        0x40000000
#define MEMC_PHY_RDIMMGCR0__QCSEN__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_RDIMMGCR0__QCSEN__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMGCR0.MIRROR - RDIMM Mirror: Selects between two different ballouts of the RDIMM buffer chip for front or back operation. This register bit controls the buffer chip MIRROR signal. */
#define MEMC_PHY_RDIMMGCR0__MIRROR__SHIFT       31
#define MEMC_PHY_RDIMMGCR0__MIRROR__WIDTH       1
#define MEMC_PHY_RDIMMGCR0__MIRROR__MASK        0x80000000
#define MEMC_PHY_RDIMMGCR0__MIRROR__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_RDIMMGCR0__MIRROR__HW_DEFAULT  0x0

/* RDIMM General Configuration Register 1 */
/* RDIMM General Configuration Registers are used to configure and control the PUB and the PHY for RDIMM support. The following tables describe the bits of the RDIMMPGR registers. RDIMMGCR0 have several register controls for the RDIMM-specific I/Os (PAR_IN, ERROUT#, QCSEN#, and MIRROR). Only those controls that require individualized control (such as power-down, termination, and I/O mode) is included in the register. Other common I/O controls, such as slew rate, I/O DDR mode, and I/O loop-back are controlled from common registers such as ACIOCR and PGCR1. */
#define MEMC_PHY_RDIMMGCR1        0x108100B4

/* MEMC_PHY_RDIMMGCR1.tBCSTAB - Stabilization time: Number of DRAM clock cycles for the RDIMM buffer chip to stabilize. This parameter corresponds to the buffer chip tSTAB parameter. Default value is in decimal format and corresponds to 6us at 533MHz. */
#define MEMC_PHY_RDIMMGCR1__T_BCSTAB__SHIFT      0
#define MEMC_PHY_RDIMMGCR1__T_BCSTAB__WIDTH      12
#define MEMC_PHY_RDIMMGCR1__T_BCSTAB__MASK       0x00000FFF
#define MEMC_PHY_RDIMMGCR1__T_BCSTAB__INV_MASK   0xFFFFF000
#define MEMC_PHY_RDIMMGCR1__T_BCSTAB__HW_DEFAULT 0xC80

/* MEMC_PHY_RDIMMGCR1.tBCMRD - Command word to command word programming delay: Number of DRAM clock cycles between two RDIMM buffer chip command programming accesses. The value used for tBCMRD is 8 plus the value programmed in these bits, i.e. tBCMRD value ranges from 8 to 15. This parameter corresponds to the buffer chip tMRD parameter. */
#define MEMC_PHY_RDIMMGCR1__T_BCMRD__SHIFT      12
#define MEMC_PHY_RDIMMGCR1__T_BCMRD__WIDTH      3
#define MEMC_PHY_RDIMMGCR1__T_BCMRD__MASK       0x00007000
#define MEMC_PHY_RDIMMGCR1__T_BCMRD__INV_MASK   0xFFFF8FFF
#define MEMC_PHY_RDIMMGCR1__T_BCMRD__HW_DEFAULT 0x0

/* MEMC_PHY_RDIMMGCR1.CRINIT - Control Registers Initialization Enable: Indicates which RDIMM buffer chip control registers (RC0 to RC15) should be initialized (written) when the PUB is triggered to initialize the buffer chip. A setting of '1' on CRINIT[n] bit means that CRn should be written during initialization. */
#define MEMC_PHY_RDIMMGCR1__CRINIT__SHIFT       16
#define MEMC_PHY_RDIMMGCR1__CRINIT__WIDTH       16
#define MEMC_PHY_RDIMMGCR1__CRINIT__MASK        0xFFFF0000
#define MEMC_PHY_RDIMMGCR1__CRINIT__INV_MASK    0x0000FFFF
#define MEMC_PHY_RDIMMGCR1__CRINIT__HW_DEFAULT  0xFFFF

/* RDIMM Control Register 0 */
/* RDIMM Control Registers (RDIMMCR0-1) are used to mirror the register bits of the RDIMM buffer control words. The RDIMM buffer chip has sixteen control words. These are referred to as RC0 to RC15 in the RDIMM Buffer JEDEC specification. The following tables describe the bits of the PUB RDIMMCR registers and how they correspond to RDIMM buffer control words. */
#define MEMC_PHY_RDIMMCR0         0x108100B8

/* MEMC_PHY_RDIMMCR0.RC0 - Control Word 0 (Global Features Control Word): Bit definitions are: RC0[0]: 0 = Output inversion enabled, 1 = Output inversion disabled. RC0[1]: 0 = Floating outputs disabled, 1 = Floating outputs enabled. RC0[2]: 0 = A outputs enabled, 1 = A outputs disabled. RC0[3]: 0 = B outputs enabled, 1 = B outputs disabled. */
#define MEMC_PHY_RDIMMCR0__RC0__SHIFT       0
#define MEMC_PHY_RDIMMCR0__RC0__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC0__MASK        0x0000000F
#define MEMC_PHY_RDIMMCR0__RC0__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_RDIMMCR0__RC0__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC1 - Control Word 1 (Clock Driver Enable Control Word): Bit definitions are: RC1[0]: 0 = Y0/Y0# clock enabled, 1 = Y0/Y0# clock disabled. RC1[1]: 0 = Y1/Y1# clock enabled, 1 = Y1/Y1# clock disabled. RC1[2]: 0 = Y2/Y2# clock enabled, 1 = Y2/Y2# clock disabled. RC1[3]: 0 = Y3/Y3# clock enabled, 1 = Y3/Y3# clock disabled. */
#define MEMC_PHY_RDIMMCR0__RC1__SHIFT       4
#define MEMC_PHY_RDIMMCR0__RC1__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC1__MASK        0x000000F0
#define MEMC_PHY_RDIMMCR0__RC1__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_RDIMMCR0__RC1__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC2 - Control Word 2 (Timing Control Word): Bit definitions are: RC2[0]: 0 = Standard (1/2 clock) pre-launch, 1 = Prelaunch controlled by RC12. RC2[1]: 0 = Reserved. RC2[2]: 0 = 100 Ohm input bus termination, 1 = 150 Ohm input bus termination. RC2[3]: 0 = Operation frequency band 1, 1 = Test mode frequency band 2. */
#define MEMC_PHY_RDIMMCR0__RC2__SHIFT       8
#define MEMC_PHY_RDIMMCR0__RC2__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC2__MASK        0x00000F00
#define MEMC_PHY_RDIMMCR0__RC2__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_RDIMMCR0__RC2__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC3 - Control Word 3 (Command/Address Signals Driver Characteristics Control Word): RC3[1:0] is driver settings for command/address A outputs, and RC3[3:2] is driver settings for command/address B outputs. Bit definitions are: 00 = Light drive (4 or 5 DRAM loads) 01 = Moderate drive (8 or 10 DRAM loads) 10 = Strong drive (16 or 20 DRAM loads) 11 = Reserved */
#define MEMC_PHY_RDIMMCR0__RC3__SHIFT       12
#define MEMC_PHY_RDIMMCR0__RC3__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC3__MASK        0x0000F000
#define MEMC_PHY_RDIMMCR0__RC3__INV_MASK    0xFFFF0FFF
#define MEMC_PHY_RDIMMCR0__RC3__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC4 - Control Word 4 (Control Signals Driver Characteristics Control Word): RC4[1:0] is driver settings for control A outputs, and RC4[3:2] is driver settings for control B outputs. Bit definitions are: 00 = Light drive (4 or 5 DRAM loads) 01 = Moderate drive (8 or 10 DRAM loads) 10 = Reserved 11 = Reserved */
#define MEMC_PHY_RDIMMCR0__RC4__SHIFT       16
#define MEMC_PHY_RDIMMCR0__RC4__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC4__MASK        0x000F0000
#define MEMC_PHY_RDIMMCR0__RC4__INV_MASK    0xFFF0FFFF
#define MEMC_PHY_RDIMMCR0__RC4__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC5 - Control Word 5 (CK Driver Characteristics Control Word): RC5[1:0] is driver settings for clock Y1, Y1#, Y3, and Y3# outputs, and RC5[3:2] is driver settings for clock Y0, Y0#, Y2, and Y2# outputs. Bit definitions are: 00 = Light drive (4 or 5 DRAM loads) 01 = Moderate drive (8 or 10 DRAM loads) 10 = Strong drive (16 or 20 DRAM loads) 11 = Reserved */
#define MEMC_PHY_RDIMMCR0__RC5__SHIFT       20
#define MEMC_PHY_RDIMMCR0__RC5__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC5__MASK        0x00F00000
#define MEMC_PHY_RDIMMCR0__RC5__INV_MASK    0xFF0FFFFF
#define MEMC_PHY_RDIMMCR0__RC5__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC6 - Control Word 6: Reserved, free to use by vendor. */
#define MEMC_PHY_RDIMMCR0__RC6__SHIFT       24
#define MEMC_PHY_RDIMMCR0__RC6__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC6__MASK        0x0F000000
#define MEMC_PHY_RDIMMCR0__RC6__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_RDIMMCR0__RC6__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR0.RC7 - Control Word 7: Reserved, free to use by vendor. */
#define MEMC_PHY_RDIMMCR0__RC7__SHIFT       28
#define MEMC_PHY_RDIMMCR0__RC7__WIDTH       4
#define MEMC_PHY_RDIMMCR0__RC7__MASK        0xF0000000
#define MEMC_PHY_RDIMMCR0__RC7__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_RDIMMCR0__RC7__HW_DEFAULT  0x0

/* RDIMM Control Register 1 */
/* RDIMM Control Registers (RDIMMCR0-1) are used to mirror the register bits of the RDIMM buffer control words. The RDIMM buffer chip has sixteen control words. These are referred to as RC0 to RC15 in the RDIMM Buffer JEDEC specification. The following tables describe the bits of the PUB RDIMMCR registers and how they correspond to RDIMM buffer control words. */
#define MEMC_PHY_RDIMMCR1         0x108100BC

/* MEMC_PHY_RDIMMCR1.RC8 - Control Word 8 (Additional Input Bus Termination Setting Control Word): RC8[2:0] is Input Bus Termination (IBT) setting as follows: 000 = IBT as defined in RC2. 001 = Reserved 010 = 200 Ohm 011 = Reserved 100 = 300 Ohm 101 = Reserved 110 = Reserved 111 = Off RC8[3]: 0 = IBT off when MIRROR is HIGH, 1 = IBT on when MIRROR is high */
#define MEMC_PHY_RDIMMCR1__RC8__SHIFT       0
#define MEMC_PHY_RDIMMCR1__RC8__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC8__MASK        0x0000000F
#define MEMC_PHY_RDIMMCR1__RC8__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_RDIMMCR1__RC8__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC9 - Control Word 9 (Power Saving Settings Control Word): Bit definitions are: RC9[0]: 0 = Floating outputs as defined in RC0, 1 = Weak drive enabled. RC9[1]: 0 = Reserved. RC9[2]: 0 = CKE power down with IBT ON, QxODT is a function of DxODT, 1 = CKE power down with IBT off, QxODT held LOW. RC9[2] is valid only when RC9[3] is 1. RC9[3]: 0 = CKE power down mode disabled, 1 = CKE power down mode enabled. */
#define MEMC_PHY_RDIMMCR1__RC9__SHIFT       4
#define MEMC_PHY_RDIMMCR1__RC9__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC9__MASK        0x000000F0
#define MEMC_PHY_RDIMMCR1__RC9__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_RDIMMCR1__RC9__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC10 - Control Word 10 (RDIMM Operating Speed Control Word): RC10[2:0] is RDIMM operating speed setting as follows: 000 = DDR3/DDR3L-800 001 = DDR3/DDR3L-1066 010 = DDR3/DDR3L-1333 011 = DDR3/DDR3L-1600 100 = Reserved 101 = Reserved 110 = Reserved 111 = Reserved RC10[3]: Don't care. */
#define MEMC_PHY_RDIMMCR1__RC10__SHIFT       8
#define MEMC_PHY_RDIMMCR1__RC10__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC10__MASK        0x00000F00
#define MEMC_PHY_RDIMMCR1__RC10__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_RDIMMCR1__RC10__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC11 - Control Word 11 (Operating Voltage VDD Control Word): RC10[1:0] is VDD operating voltage setting as follows: 00 = DDR3 1.5V mode 01 = DDR3L 1.35V mode 10 = Reserved 11 = Reserved RC10[3:2]: Reserved. */
#define MEMC_PHY_RDIMMCR1__RC11__SHIFT       12
#define MEMC_PHY_RDIMMCR1__RC11__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC11__MASK        0x0000F000
#define MEMC_PHY_RDIMMCR1__RC11__INV_MASK    0xFFFF0FFF
#define MEMC_PHY_RDIMMCR1__RC11__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC12 - Control Word 12: Reserved for future use. */
#define MEMC_PHY_RDIMMCR1__RC12__SHIFT       16
#define MEMC_PHY_RDIMMCR1__RC12__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC12__MASK        0x000F0000
#define MEMC_PHY_RDIMMCR1__RC12__INV_MASK    0xFFF0FFFF
#define MEMC_PHY_RDIMMCR1__RC12__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC13 - Control Word 13: Reserved for future use. */
#define MEMC_PHY_RDIMMCR1__RC13__SHIFT       20
#define MEMC_PHY_RDIMMCR1__RC13__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC13__MASK        0x00F00000
#define MEMC_PHY_RDIMMCR1__RC13__INV_MASK    0xFF0FFFFF
#define MEMC_PHY_RDIMMCR1__RC13__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC14 - Control Word 14: Reserved for future use. */
#define MEMC_PHY_RDIMMCR1__RC14__SHIFT       24
#define MEMC_PHY_RDIMMCR1__RC14__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC14__MASK        0x0F000000
#define MEMC_PHY_RDIMMCR1__RC14__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_RDIMMCR1__RC14__HW_DEFAULT  0x0

/* MEMC_PHY_RDIMMCR1.RC15 - Control Word 15: Reserved for future use. */
#define MEMC_PHY_RDIMMCR1__RC15__SHIFT       28
#define MEMC_PHY_RDIMMCR1__RC15__WIDTH       4
#define MEMC_PHY_RDIMMCR1__RC15__MASK        0xF0000000
#define MEMC_PHY_RDIMMCR1__RC15__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_RDIMMCR1__RC15__HW_DEFAULT  0x0

/* DCU Address Register */
/* DCU Address Register selects the DCU cache and the address of the cache that should be accessed when subsequent configuration writes or reads to the caches is triggered using the "DCU Data Register (DCUDR)" on page 120. A write to the DCUAR will automatically transfer the first cache data word into the DCU Data Register (DCUDR) and optionally increment the address if the cache assess is a read. */
#define MEMC_PHY_DCUAR            0x108100C0

/* MEMC_PHY_DCUAR.CWADDR - Cache Word Address: Address of the cache word to be accessed. */
#define MEMC_PHY_DCUAR__CWADDR__SHIFT       0
#define MEMC_PHY_DCUAR__CWADDR__WIDTH       4
#define MEMC_PHY_DCUAR__CWADDR__MASK        0x0000000F
#define MEMC_PHY_DCUAR__CWADDR__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_DCUAR__CWADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCUAR.CSADDR - Cache Slice Address: Address of the cache slice to be accessed. */
#define MEMC_PHY_DCUAR__CSADDR__SHIFT       4
#define MEMC_PHY_DCUAR__CSADDR__WIDTH       4
#define MEMC_PHY_DCUAR__CSADDR__MASK        0x000000F0
#define MEMC_PHY_DCUAR__CSADDR__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_DCUAR__CSADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCUAR.CSEL - Cache Select: Selects the cache to be accessed. Valid values are: 00 = Command cache 01 = Expected data cache 10 = Read data cache 11 = Reserved */
#define MEMC_PHY_DCUAR__CSEL__SHIFT       8
#define MEMC_PHY_DCUAR__CSEL__WIDTH       2
#define MEMC_PHY_DCUAR__CSEL__MASK        0x00000300
#define MEMC_PHY_DCUAR__CSEL__INV_MASK    0xFFFFFCFF
#define MEMC_PHY_DCUAR__CSEL__HW_DEFAULT  0x0

/* MEMC_PHY_DCUAR.INCA - Increment Address: Specifies, if set, that the cache address specified in WADDR and SADDR should be automatically incremented after each access of the cache. The increment happens in such a way that all the slices of a selected word are first accessed before going to the next word. */
#define MEMC_PHY_DCUAR__INCA__SHIFT       10
#define MEMC_PHY_DCUAR__INCA__WIDTH       1
#define MEMC_PHY_DCUAR__INCA__MASK        0x00000400
#define MEMC_PHY_DCUAR__INCA__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_DCUAR__INCA__HW_DEFAULT  0x0

/* MEMC_PHY_DCUAR.ATYPE - Access Type: Specifies the type of access to be performed using this address. Valid values are: 0 = Write access 1 = Read access */
#define MEMC_PHY_DCUAR__ATYPE__SHIFT       11
#define MEMC_PHY_DCUAR__ATYPE__WIDTH       1
#define MEMC_PHY_DCUAR__ATYPE__MASK        0x00000800
#define MEMC_PHY_DCUAR__ATYPE__INV_MASK    0xFFFFF7FF
#define MEMC_PHY_DCUAR__ATYPE__HW_DEFAULT  0x0

/* DCU Data Register */
/* DCU Data Register is used to hold the data to be written to or read from the cache. */
#define MEMC_PHY_DCUDR            0x108100C4

/* MEMC_PHY_DCUDR.CDATA - Cache Data: Data to be written to or read from a cache. This data corresponds to the cache word slice specified by the DCU Address Register. */
#define MEMC_PHY_DCUDR__CDATA__SHIFT       0
#define MEMC_PHY_DCUDR__CDATA__WIDTH       32
#define MEMC_PHY_DCUDR__CDATA__MASK        0xFFFFFFFF
#define MEMC_PHY_DCUDR__CDATA__INV_MASK    0x00000000
#define MEMC_PHY_DCUDR__CDATA__HW_DEFAULT  0x0

/* DCU Run Register */
/* DCU Run Register is used to trigger the running of DRAM commands in the DRAM Command Unit command cache. */
#define MEMC_PHY_DCURR            0x108100C8

/* MEMC_PHY_DCURR.DINST - DCU Instruction: Selects the DCU command to be executed: Valid values are: 0000 = NOP: No operation 0001 = Run: Triggers the execution of commands in the command cache. 0010 = Stop: Stops the execution of commands in the command cache. 0011 = Stop Loop: Stops the execution of an infinite loop in the command cache. 0100 = Reset: Resets all DCU run time registers. See "DCU Status" on page 238 for details. 0101 - 1111 Reserved */
#define MEMC_PHY_DCURR__DINST__SHIFT       0
#define MEMC_PHY_DCURR__DINST__WIDTH       4
#define MEMC_PHY_DCURR__DINST__MASK        0x0000000F
#define MEMC_PHY_DCURR__DINST__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_DCURR__DINST__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.SADDR - Start Address: Cache word address where the execution of commands should begin. */
#define MEMC_PHY_DCURR__SADDR__SHIFT       4
#define MEMC_PHY_DCURR__SADDR__WIDTH       4
#define MEMC_PHY_DCURR__SADDR__MASK        0x000000F0
#define MEMC_PHY_DCURR__SADDR__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_DCURR__SADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.EADDR - End Address: Cache word address where the execution of command should end. */
#define MEMC_PHY_DCURR__EADDR__SHIFT       8
#define MEMC_PHY_DCURR__EADDR__WIDTH       4
#define MEMC_PHY_DCURR__EADDR__MASK        0x00000F00
#define MEMC_PHY_DCURR__EADDR__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_DCURR__EADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.NFAIL - Number of Failures: Specifies the number of failures after which the execution of commands and the capture of read data should stop if SONF bit of this register is set. Execution of commands and the capture of read data will stop after (NFAIL+1) failures if SONF is set. Valid values are from 0 to 254. */
#define MEMC_PHY_DCURR__NFAIL__SHIFT       12
#define MEMC_PHY_DCURR__NFAIL__WIDTH       8
#define MEMC_PHY_DCURR__NFAIL__MASK        0x000FF000
#define MEMC_PHY_DCURR__NFAIL__INV_MASK    0xFFF00FFF
#define MEMC_PHY_DCURR__NFAIL__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.SONF - Stop On Nth Fail: Specifies if set that the execution of commands and the capture of read data should stop when there are N read data failures. The number of failures is specified by NFAIL. Otherwise commands execute until the end of the program or until manually stopped using a STOP command. */
#define MEMC_PHY_DCURR__SONF__SHIFT       20
#define MEMC_PHY_DCURR__SONF__WIDTH       1
#define MEMC_PHY_DCURR__SONF__MASK        0x00100000
#define MEMC_PHY_DCURR__SONF__INV_MASK    0xFFEFFFFF
#define MEMC_PHY_DCURR__SONF__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.SCOF - Stop Capture On Full: Specifies if set that the capture of read data should stop when the capture cache is full. */
#define MEMC_PHY_DCURR__SCOF__SHIFT       21
#define MEMC_PHY_DCURR__SCOF__WIDTH       1
#define MEMC_PHY_DCURR__SCOF__MASK        0x00200000
#define MEMC_PHY_DCURR__SCOF__INV_MASK    0xFFDFFFFF
#define MEMC_PHY_DCURR__SCOF__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.RCEN - Read Capture Enable: Indicates if set that read data coming back from the SDRAM should be captured into the read data cache. */
#define MEMC_PHY_DCURR__RCEN__SHIFT       22
#define MEMC_PHY_DCURR__RCEN__WIDTH       1
#define MEMC_PHY_DCURR__RCEN__MASK        0x00400000
#define MEMC_PHY_DCURR__RCEN__INV_MASK    0xFFBFFFFF
#define MEMC_PHY_DCURR__RCEN__HW_DEFAULT  0x0

/* MEMC_PHY_DCURR.XCEN - Expected Compare Enable: Indicates if set that read data coming back from the SDRAM should be should be compared with the expected data. */
#define MEMC_PHY_DCURR__XCEN__SHIFT       23
#define MEMC_PHY_DCURR__XCEN__WIDTH       1
#define MEMC_PHY_DCURR__XCEN__MASK        0x00800000
#define MEMC_PHY_DCURR__XCEN__INV_MASK    0xFF7FFFFF
#define MEMC_PHY_DCURR__XCEN__HW_DEFAULT  0x0

/* DCU Loop Register */
/* DCU Loop Register is used to configure the looping when commands are executed from the command cache. */
#define MEMC_PHY_DCULR            0x108100CC

/* MEMC_PHY_DCULR.LSADDR - Loop Start Address: Command cache word address where the loop should start. */
#define MEMC_PHY_DCULR__LSADDR__SHIFT       0
#define MEMC_PHY_DCULR__LSADDR__WIDTH       4
#define MEMC_PHY_DCULR__LSADDR__MASK        0x0000000F
#define MEMC_PHY_DCULR__LSADDR__INV_MASK    0xFFFFFFF0
#define MEMC_PHY_DCULR__LSADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCULR.LEADDR - Loop End Address: Command cache word address where the loop should end. */
#define MEMC_PHY_DCULR__LEADDR__SHIFT       4
#define MEMC_PHY_DCULR__LEADDR__WIDTH       4
#define MEMC_PHY_DCULR__LEADDR__MASK        0x000000F0
#define MEMC_PHY_DCULR__LEADDR__INV_MASK    0xFFFFFF0F
#define MEMC_PHY_DCULR__LEADDR__HW_DEFAULT  0x0

/* MEMC_PHY_DCULR.LCNT - Loop Count: The number of times that the loop should be executed if LINF is not set. */
#define MEMC_PHY_DCULR__LCNT__SHIFT       8
#define MEMC_PHY_DCULR__LCNT__WIDTH       8
#define MEMC_PHY_DCULR__LCNT__MASK        0x0000FF00
#define MEMC_PHY_DCULR__LCNT__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DCULR__LCNT__HW_DEFAULT  0x0

/* MEMC_PHY_DCULR.LINF - Loop Infinite: Indicates if set that the loop should be executed indefinitely until stopped by the STOP command. Otherwise the loop is execute LCNT times. */
#define MEMC_PHY_DCULR__LINF__SHIFT       16
#define MEMC_PHY_DCULR__LINF__WIDTH       1
#define MEMC_PHY_DCULR__LINF__MASK        0x00010000
#define MEMC_PHY_DCULR__LINF__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_DCULR__LINF__HW_DEFAULT  0x0

/* MEMC_PHY_DCULR.IDA - Increment DRAM Address: Indicates if set that DRAM addresses should be incremented every time a DRAM read/write command inside the loop is executed. */
#define MEMC_PHY_DCULR__IDA__SHIFT       17
#define MEMC_PHY_DCULR__IDA__WIDTH       1
#define MEMC_PHY_DCULR__IDA__MASK        0x00020000
#define MEMC_PHY_DCULR__IDA__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_DCULR__IDA__HW_DEFAULT  0x0

/* MEMC_PHY_DCULR.XLEADDR - Expected Data Loop End Address: The last expected data cache word address that contains valid expected data. Expected data should looped between 0 and this address. XLEADDR field uses only the following bits based on the cache depth: (*) DCU expected data cache = 4, XLEADDR[1:0] (*) DCU expected data cache = 8, XLEADDR[2:0] (*) DCU expected data cache = 16, XLEADDR[3:0] */
#define MEMC_PHY_DCULR__XLEADDR__SHIFT       28
#define MEMC_PHY_DCULR__XLEADDR__WIDTH       4
#define MEMC_PHY_DCULR__XLEADDR__MASK        0xF0000000
#define MEMC_PHY_DCULR__XLEADDR__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_DCULR__XLEADDR__HW_DEFAULT  0xF

/* DCU General Configuration Register */
/* DCU General Configuration Register (GCUGCR) is used to specify miscellaneous GCU configurations prior to triggering the execution of commands. */
#define MEMC_PHY_DCUGCR           0x108100D0

/* MEMC_PHY_DCUGCR.RCSW - Read Capture Start Word: The capture and compare of read data should start after Nth word. For example setting this value to 12 will skip the first 12 read data. */
#define MEMC_PHY_DCUGCR__RCSW__SHIFT       0
#define MEMC_PHY_DCUGCR__RCSW__WIDTH       16
#define MEMC_PHY_DCUGCR__RCSW__MASK        0x0000FFFF
#define MEMC_PHY_DCUGCR__RCSW__INV_MASK    0xFFFF0000
#define MEMC_PHY_DCUGCR__RCSW__HW_DEFAULT  0x0

/* DCU Timing Parameter Register */
/* The DCU Timing Parameter Register is used to program generic timing parameters for the repeat field of the command cache. Refer to "Command Execution" on page 233 for information on using these parameters. */
#define MEMC_PHY_DCUTPR           0x108100D4

/* MEMC_PHY_DCUTPR.tDCUT0 - DCU Generic Timing Parameter 0. */
#define MEMC_PHY_DCUTPR__T_DCUT0__SHIFT      0
#define MEMC_PHY_DCUTPR__T_DCUT0__WIDTH      8
#define MEMC_PHY_DCUTPR__T_DCUT0__MASK       0x000000FF
#define MEMC_PHY_DCUTPR__T_DCUT0__INV_MASK   0xFFFFFF00
#define MEMC_PHY_DCUTPR__T_DCUT0__HW_DEFAULT 0x0

/* MEMC_PHY_DCUTPR.tDCUT1 - DCU Generic Timing Parameter 1. */
#define MEMC_PHY_DCUTPR__T_DCUT1__SHIFT      8
#define MEMC_PHY_DCUTPR__T_DCUT1__WIDTH      8
#define MEMC_PHY_DCUTPR__T_DCUT1__MASK       0x0000FF00
#define MEMC_PHY_DCUTPR__T_DCUT1__INV_MASK   0xFFFF00FF
#define MEMC_PHY_DCUTPR__T_DCUT1__HW_DEFAULT 0x0

/* MEMC_PHY_DCUTPR.tDCUT2 - DCU Generic Timing Parameter 2. */
#define MEMC_PHY_DCUTPR__T_DCUT2__SHIFT      16
#define MEMC_PHY_DCUTPR__T_DCUT2__WIDTH      8
#define MEMC_PHY_DCUTPR__T_DCUT2__MASK       0x00FF0000
#define MEMC_PHY_DCUTPR__T_DCUT2__INV_MASK   0xFF00FFFF
#define MEMC_PHY_DCUTPR__T_DCUT2__HW_DEFAULT 0x0

/* MEMC_PHY_DCUTPR.tDCUT3 - DCU Generic Timing Parameter 3. */
#define MEMC_PHY_DCUTPR__T_DCUT3__SHIFT      24
#define MEMC_PHY_DCUTPR__T_DCUT3__WIDTH      8
#define MEMC_PHY_DCUTPR__T_DCUT3__MASK       0xFF000000
#define MEMC_PHY_DCUTPR__T_DCUT3__INV_MASK   0x00FFFFFF
#define MEMC_PHY_DCUTPR__T_DCUT3__HW_DEFAULT 0x0

/* DCU Status Register 0 */
/* DCU status registers are used to report various status of the DCU, including number of reads returned and number of read failures. The following tables describe the bits of the DCUSR registers. */
#define MEMC_PHY_DCUSR0           0x108100D8

/* MEMC_PHY_DCUSR0.RDONE - Run Done: Indicates if set that the DCU has finished executing the commands in the command cache. This bit is also set to indicate that a STOP command has successfully been executed and command execution has stopped. */
#define MEMC_PHY_DCUSR0__RDONE__SHIFT       0
#define MEMC_PHY_DCUSR0__RDONE__WIDTH       1
#define MEMC_PHY_DCUSR0__RDONE__MASK        0x00000001
#define MEMC_PHY_DCUSR0__RDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DCUSR0__RDONE__HW_DEFAULT  0x0

/* MEMC_PHY_DCUSR0.CFAIL - Capture Fail: Indicates if set that at least one read data word has failed. */
#define MEMC_PHY_DCUSR0__CFAIL__SHIFT       1
#define MEMC_PHY_DCUSR0__CFAIL__WIDTH       1
#define MEMC_PHY_DCUSR0__CFAIL__MASK        0x00000002
#define MEMC_PHY_DCUSR0__CFAIL__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DCUSR0__CFAIL__HW_DEFAULT  0x0

/* MEMC_PHY_DCUSR0.CFULL - Capture Full: Indicates if set that the capture cache is full. */
#define MEMC_PHY_DCUSR0__CFULL__SHIFT       2
#define MEMC_PHY_DCUSR0__CFULL__WIDTH       1
#define MEMC_PHY_DCUSR0__CFULL__MASK        0x00000004
#define MEMC_PHY_DCUSR0__CFULL__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DCUSR0__CFULL__HW_DEFAULT  0x0

/* DCU Status Register 1 */
/* DCU status registers are used to report various status of the DCU, including number of reads returned and number of read failures. The following tables describe the bits of the DCUSR registers. */
#define MEMC_PHY_DCUSR1           0x108100DC

/* MEMC_PHY_DCUSR1.RDCNT - Read Count: Number of read words returned from the SDRAM. */
#define MEMC_PHY_DCUSR1__RDCNT__SHIFT       0
#define MEMC_PHY_DCUSR1__RDCNT__WIDTH       16
#define MEMC_PHY_DCUSR1__RDCNT__MASK        0x0000FFFF
#define MEMC_PHY_DCUSR1__RDCNT__INV_MASK    0xFFFF0000
#define MEMC_PHY_DCUSR1__RDCNT__HW_DEFAULT  0x0

/* MEMC_PHY_DCUSR1.FLCNT - Fail Count: Number of read words that have failed. */
#define MEMC_PHY_DCUSR1__FLCNT__SHIFT       16
#define MEMC_PHY_DCUSR1__FLCNT__WIDTH       8
#define MEMC_PHY_DCUSR1__FLCNT__MASK        0x00FF0000
#define MEMC_PHY_DCUSR1__FLCNT__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DCUSR1__FLCNT__HW_DEFAULT  0x0

/* MEMC_PHY_DCUSR1.LPCNT - Loop Count: Indicates the value of the loop count. This is useful when the program has stopped because of failures to assess how many reads were executed before first fail. */
#define MEMC_PHY_DCUSR1__LPCNT__SHIFT       24
#define MEMC_PHY_DCUSR1__LPCNT__WIDTH       8
#define MEMC_PHY_DCUSR1__LPCNT__MASK        0xFF000000
#define MEMC_PHY_DCUSR1__LPCNT__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DCUSR1__LPCNT__HW_DEFAULT  0x0

/* BIST Run Register */
/* BIST Run Register is used to trigger the running of built-in-self test routines on both the address/command lane and the date byte lanes. */
#define MEMC_PHY_BISTRR           0x10810100

/* MEMC_PHY_BISTRR.BINST - BIST Instruction: Selects the BIST instruction to be executed: Valid values are: 000 = NOP: No operation 001 = Run: Triggers the running of the BIST. 010 = Stop: Stops the running of the BIST. 011 = Reset: Resets all BIST run-time registers, such as error counters. 100 - 111 Reserved */
#define MEMC_PHY_BISTRR__BINST__SHIFT       0
#define MEMC_PHY_BISTRR__BINST__WIDTH       3
#define MEMC_PHY_BISTRR__BINST__MASK        0x00000007
#define MEMC_PHY_BISTRR__BINST__INV_MASK    0xFFFFFFF8
#define MEMC_PHY_BISTRR__BINST__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BMODE - BIST Mode: Selects the mode in which BIST is run. Valid values are: 0 = Loopback mode: Address, commands and data loop back at the PHY I/Os. 1 = DRAM mode: Address, commands and data go to DRAM for normal memory accesses. */
#define MEMC_PHY_BISTRR__BMODE__SHIFT       3
#define MEMC_PHY_BISTRR__BMODE__WIDTH       1
#define MEMC_PHY_BISTRR__BMODE__MASK        0x00000008
#define MEMC_PHY_BISTRR__BMODE__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_BISTRR__BMODE__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BINF - BIST Infinite Run: Specifies if set that the BIST should be run indefinitely until when it is either stopped or a failure has been encountered. Otherwise BIST is run until number of BIST words specified in the BISTWCR register has been generated. */
#define MEMC_PHY_BISTRR__BINF__SHIFT       4
#define MEMC_PHY_BISTRR__BINF__WIDTH       1
#define MEMC_PHY_BISTRR__BINF__MASK        0x00000010
#define MEMC_PHY_BISTRR__BINF__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_BISTRR__BINF__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.NFAIL - Number of Failures: Specifies the number of failures after which the execution of commands and the capture of read data should stop if BSONF bit of this register is set. Execution of commands and the capture of read data will stop after (NFAIL+1) failures if BSONF is set. */
#define MEMC_PHY_BISTRR__NFAIL__SHIFT       5
#define MEMC_PHY_BISTRR__NFAIL__WIDTH       8
#define MEMC_PHY_BISTRR__NFAIL__MASK        0x00001FE0
#define MEMC_PHY_BISTRR__NFAIL__INV_MASK    0xFFFFE01F
#define MEMC_PHY_BISTRR__NFAIL__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BSONF - BIST Stop On Nth Fail: Specifies if set that the BIST should stop when an nth data word or address/command comparison error has been encountered. */
#define MEMC_PHY_BISTRR__BSONF__SHIFT       13
#define MEMC_PHY_BISTRR__BSONF__WIDTH       1
#define MEMC_PHY_BISTRR__BSONF__MASK        0x00002000
#define MEMC_PHY_BISTRR__BSONF__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_BISTRR__BSONF__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BDXEN - BIST DATX8 Enable: Enables the running of BIST on the data byte lane PHYs. This bit is exclusive with BACEN, i.e. both cannot be set to '1' at the same time. */
#define MEMC_PHY_BISTRR__BDXEN__SHIFT       14
#define MEMC_PHY_BISTRR__BDXEN__WIDTH       1
#define MEMC_PHY_BISTRR__BDXEN__MASK        0x00004000
#define MEMC_PHY_BISTRR__BDXEN__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_BISTRR__BDXEN__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BACEN - BIST AC Enable: Enables the running of BIST on the address/command lane PHY. This bit is exclusive with BDXEN, i.e. both cannot be set to '1' at the same time. */
#define MEMC_PHY_BISTRR__BACEN__SHIFT       15
#define MEMC_PHY_BISTRR__BACEN__WIDTH       1
#define MEMC_PHY_BISTRR__BACEN__MASK        0x00008000
#define MEMC_PHY_BISTRR__BACEN__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_BISTRR__BACEN__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BDMEN - BIST Data Mask Enable: Enables if set that the data mask BIST should be included in the BIST run, i.e. data pattern generated and loopback data compared. This is valid only for loopback mode. */
#define MEMC_PHY_BISTRR__BDMEN__SHIFT       16
#define MEMC_PHY_BISTRR__BDMEN__WIDTH       1
#define MEMC_PHY_BISTRR__BDMEN__MASK        0x00010000
#define MEMC_PHY_BISTRR__BDMEN__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_BISTRR__BDMEN__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BDPAT - BIST Data Pattern: Selects the data pattern used during BIST. Valid values are: 00 = Walking 0 01 = Walking 1 10 = LFSR-based pseudo-random 11 = User programmable (Not valid for AC loopback). */
#define MEMC_PHY_BISTRR__BDPAT__SHIFT       17
#define MEMC_PHY_BISTRR__BDPAT__WIDTH       2
#define MEMC_PHY_BISTRR__BDPAT__MASK        0x00060000
#define MEMC_PHY_BISTRR__BDPAT__INV_MASK    0xFFF9FFFF
#define MEMC_PHY_BISTRR__BDPAT__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BDXSEL - BIST DATX8 Select: Select the byte lane for comparison of loopback/read data. Valid values are 0 to 8. */
#define MEMC_PHY_BISTRR__BDXSEL__SHIFT       19
#define MEMC_PHY_BISTRR__BDXSEL__WIDTH       4
#define MEMC_PHY_BISTRR__BDXSEL__MASK        0x00780000
#define MEMC_PHY_BISTRR__BDXSEL__INV_MASK    0xFF87FFFF
#define MEMC_PHY_BISTRR__BDXSEL__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BCKSEL - BIST CK Select: Selects the CK that should be used to register the AC loopback signals from the I/Os. Valid values are: 00 = CK[0] 01 = CK[1] 10 = CK[2] 11 = Reserved */
#define MEMC_PHY_BISTRR__BCKSEL__SHIFT       23
#define MEMC_PHY_BISTRR__BCKSEL__WIDTH       2
#define MEMC_PHY_BISTRR__BCKSEL__MASK        0x01800000
#define MEMC_PHY_BISTRR__BCKSEL__INV_MASK    0xFE7FFFFF
#define MEMC_PHY_BISTRR__BCKSEL__HW_DEFAULT  0x0

/* MEMC_PHY_BISTRR.BCCSEL - BIST Clock Cycle Select: Selects the clock numbers on which the AC loopback data is written into the FIFO. Data is written into the loopback FIFO once every four clock cycles. Valid values are: 00 = Clock cycle 0, 4, 8, 12, etc. 01 = Clock cycle 1, 5, 9, 13, etc. 10 = Clock cycle 2, 6, 10, 14, etc. 11 = Clock cycle 3, 7, 11, 15, etc. */
#define MEMC_PHY_BISTRR__BCCSEL__SHIFT       25
#define MEMC_PHY_BISTRR__BCCSEL__WIDTH       2
#define MEMC_PHY_BISTRR__BCCSEL__MASK        0x06000000
#define MEMC_PHY_BISTRR__BCCSEL__INV_MASK    0xF9FFFFFF
#define MEMC_PHY_BISTRR__BCCSEL__HW_DEFAULT  0x0

/* BIST Word Count Register */
/* BIST Word Count Register is used to specify the number of words to generate during BIST. */
#define MEMC_PHY_BISTWCR          0x10810104

/* MEMC_PHY_BISTWCR.BWCNT - BIST Word Count: Indicates the number of words to generate during BIST. This must be a multiple of DRAM burst length (BL) divided by 2, e.g. for BL=8, valid values are 4, 8, 12, 16, and so on. */
#define MEMC_PHY_BISTWCR__BWCNT__SHIFT       0
#define MEMC_PHY_BISTWCR__BWCNT__WIDTH       16
#define MEMC_PHY_BISTWCR__BWCNT__MASK        0x0000FFFF
#define MEMC_PHY_BISTWCR__BWCNT__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTWCR__BWCNT__HW_DEFAULT  0x20

/* BIST Mask Register 0 */
/* BIST mask registers are used to disable data comparison on individual data bits of the address/command lane and data byte lane. A bit that is masked will not contribute to the word error counters or bit error counters. The following tables describe the bits of the BIST mask registers. */
#define MEMC_PHY_BISTMSKR0        0x10810108

/* MEMC_PHY_BISTMSKR0.AMSK - Mask bit for each of the up to 16 address bits. */
#define MEMC_PHY_BISTMSKR0__AMSK__SHIFT       0
#define MEMC_PHY_BISTMSKR0__AMSK__WIDTH       16
#define MEMC_PHY_BISTMSKR0__AMSK__MASK        0x0000FFFF
#define MEMC_PHY_BISTMSKR0__AMSK__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTMSKR0__AMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR0.BAMSK - Mask bit for each of the up to 3 bank address bits. */
#define MEMC_PHY_BISTMSKR0__BAMSK__SHIFT       16
#define MEMC_PHY_BISTMSKR0__BAMSK__WIDTH       3
#define MEMC_PHY_BISTMSKR0__BAMSK__MASK        0x00070000
#define MEMC_PHY_BISTMSKR0__BAMSK__INV_MASK    0xFFF8FFFF
#define MEMC_PHY_BISTMSKR0__BAMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR0.WEMSK - Mask bit for the WE#. */
#define MEMC_PHY_BISTMSKR0__WEMSK__SHIFT       19
#define MEMC_PHY_BISTMSKR0__WEMSK__WIDTH       1
#define MEMC_PHY_BISTMSKR0__WEMSK__MASK        0x00080000
#define MEMC_PHY_BISTMSKR0__WEMSK__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_BISTMSKR0__WEMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR0.CKEMSK - Mask bit for each of the up to 4 CKE bits. */
#define MEMC_PHY_BISTMSKR0__CKEMSK__SHIFT       20
#define MEMC_PHY_BISTMSKR0__CKEMSK__WIDTH       4
#define MEMC_PHY_BISTMSKR0__CKEMSK__MASK        0x00F00000
#define MEMC_PHY_BISTMSKR0__CKEMSK__INV_MASK    0xFF0FFFFF
#define MEMC_PHY_BISTMSKR0__CKEMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR0.CSMSK - Mask bit for each of the up to 4 CS# bits. */
#define MEMC_PHY_BISTMSKR0__CSMSK__SHIFT       24
#define MEMC_PHY_BISTMSKR0__CSMSK__WIDTH       4
#define MEMC_PHY_BISTMSKR0__CSMSK__MASK        0x0F000000
#define MEMC_PHY_BISTMSKR0__CSMSK__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_BISTMSKR0__CSMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR0.ODTMSK - Mask bit for each of the up to 4 ODT bits. */
#define MEMC_PHY_BISTMSKR0__ODTMSK__SHIFT       28
#define MEMC_PHY_BISTMSKR0__ODTMSK__WIDTH       4
#define MEMC_PHY_BISTMSKR0__ODTMSK__MASK        0xF0000000
#define MEMC_PHY_BISTMSKR0__ODTMSK__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_BISTMSKR0__ODTMSK__HW_DEFAULT  0x0

/* BIST Mask Register 1 */
/* BIST mask registers are used to disable data comparison on individual data bits of the address/command lane and data byte lane. A bit that is masked will not contribute to the word error counters or bit error counters. The following tables describe the bits of the BIST mask registers. */
#define MEMC_PHY_BISTMSKR1        0x1081010C

/* MEMC_PHY_BISTMSKR1.RASMSK - Mask bit for the RAS. */
#define MEMC_PHY_BISTMSKR1__RASMSK__SHIFT       0
#define MEMC_PHY_BISTMSKR1__RASMSK__WIDTH       1
#define MEMC_PHY_BISTMSKR1__RASMSK__MASK        0x00000001
#define MEMC_PHY_BISTMSKR1__RASMSK__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_BISTMSKR1__RASMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR1.CASMSK - Mask bit for the CAS. */
#define MEMC_PHY_BISTMSKR1__CASMSK__SHIFT       1
#define MEMC_PHY_BISTMSKR1__CASMSK__WIDTH       1
#define MEMC_PHY_BISTMSKR1__CASMSK__MASK        0x00000002
#define MEMC_PHY_BISTMSKR1__CASMSK__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_BISTMSKR1__CASMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR1.PARMSK - Mask bit for the PAR_IN. Only for DIMM parity support and only if the design is compiled for less than 3 ranks. */
#define MEMC_PHY_BISTMSKR1__PARMSK__SHIFT       27
#define MEMC_PHY_BISTMSKR1__PARMSK__WIDTH       1
#define MEMC_PHY_BISTMSKR1__PARMSK__MASK        0x08000000
#define MEMC_PHY_BISTMSKR1__PARMSK__INV_MASK    0xF7FFFFFF
#define MEMC_PHY_BISTMSKR1__PARMSK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTMSKR1.DMMSK - Mask bit for the data mask (DM) bit. */
#define MEMC_PHY_BISTMSKR1__DMMSK__SHIFT       28
#define MEMC_PHY_BISTMSKR1__DMMSK__WIDTH       4
#define MEMC_PHY_BISTMSKR1__DMMSK__MASK        0xF0000000
#define MEMC_PHY_BISTMSKR1__DMMSK__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_BISTMSKR1__DMMSK__HW_DEFAULT  0x0

/* BIST Mask Register 2 */
/* BIST mask registers are used to disable data comparison on individual data bits of the address/command lane and data byte lane. A bit that is masked will not contribute to the word error counters or bit error counters. The following tables describe the bits of the BIST mask registers. */
#define MEMC_PHY_BISTMSKR2        0x10810110

/* MEMC_PHY_BISTMSKR2.DQMSK - Mask bit for each of the 8 data (DQ) bits. */
#define MEMC_PHY_BISTMSKR2__DQMSK__SHIFT       0
#define MEMC_PHY_BISTMSKR2__DQMSK__WIDTH       32
#define MEMC_PHY_BISTMSKR2__DQMSK__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTMSKR2__DQMSK__INV_MASK    0x00000000
#define MEMC_PHY_BISTMSKR2__DQMSK__HW_DEFAULT  0x0

/* BIST LFSR Seed Register */
/* BIST LFSR Seed Register Specifies the seed for generating pseudo-random BIST patterns. */
#define MEMC_PHY_BISTLSR          0x10810114

/* MEMC_PHY_BISTLSR.SEED - LFSR seed for pseudo-random BIST patterns. */
#define MEMC_PHY_BISTLSR__SEED__SHIFT       0
#define MEMC_PHY_BISTLSR__SEED__WIDTH       32
#define MEMC_PHY_BISTLSR__SEED__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTLSR__SEED__INV_MASK    0x00000000
#define MEMC_PHY_BISTLSR__SEED__HW_DEFAULT  0x1234ABCD

/* BIST Address Register 0 */
/* BIST address registers specify the starting address, address increment, and maximum address used during BIST when in DRAM mode. The following tables describe the bits of the BISTAR registers. The BIST address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst boundary. For example, for BL8, A[2:0] = DTCOL[2:0] = 0. */
#define MEMC_PHY_BISTAR0          0x10810118

/* MEMC_PHY_BISTAR0.BCOL - BIST Column Address: Selects the SDRAM column address to be used during BIST. The lower bits of this address must be "0000" for BL16, "000" for BL8, "00" for BL4 and "0" for BL2. */
#define MEMC_PHY_BISTAR0__BCOL__SHIFT       0
#define MEMC_PHY_BISTAR0__BCOL__WIDTH       12
#define MEMC_PHY_BISTAR0__BCOL__MASK        0x00000FFF
#define MEMC_PHY_BISTAR0__BCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_BISTAR0__BCOL__HW_DEFAULT  0x0

/* MEMC_PHY_BISTAR0.BROW - BIST Row Address: Selects the SDRAM row address to be used during BIST. */
#define MEMC_PHY_BISTAR0__BROW__SHIFT       12
#define MEMC_PHY_BISTAR0__BROW__WIDTH       16
#define MEMC_PHY_BISTAR0__BROW__MASK        0x0FFFF000
#define MEMC_PHY_BISTAR0__BROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_BISTAR0__BROW__HW_DEFAULT  0x0

/* MEMC_PHY_BISTAR0.BBANK - BIST Bank Address: Selects the SDRAM bank address to be used during BIST. */
#define MEMC_PHY_BISTAR0__BBANK__SHIFT       28
#define MEMC_PHY_BISTAR0__BBANK__WIDTH       3
#define MEMC_PHY_BISTAR0__BBANK__MASK        0x70000000
#define MEMC_PHY_BISTAR0__BBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_BISTAR0__BBANK__HW_DEFAULT  0x0

/* BIST Address Register 1 */
/* BIST address registers specify the starting address, address increment, and maximum address used during BIST when in DRAM mode. The following tables describe the bits of the BISTAR registers. The BIST address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst boundary. For example, for BL8, A[2:0] = DTCOL[2:0] = 0. */
#define MEMC_PHY_BISTAR1          0x1081011C

/* MEMC_PHY_BISTAR1.BRANK - BIST Rank: Selects the SDRAM rank to be used during BIST. Valid values range from 0 to maximum ranks minus 1. */
#define MEMC_PHY_BISTAR1__BRANK__SHIFT       0
#define MEMC_PHY_BISTAR1__BRANK__WIDTH       2
#define MEMC_PHY_BISTAR1__BRANK__MASK        0x00000003
#define MEMC_PHY_BISTAR1__BRANK__INV_MASK    0xFFFFFFFC
#define MEMC_PHY_BISTAR1__BRANK__HW_DEFAULT  0x0

/* MEMC_PHY_BISTAR1.BMRANK - BIST Maximum Rank: Specifies the maximum SDRAM rank to be used during BIST. The default value is set to maximum ranks minus 1. Example default shown here is for a 4-rank system */
#define MEMC_PHY_BISTAR1__BMRANK__SHIFT       2
#define MEMC_PHY_BISTAR1__BMRANK__WIDTH       2
#define MEMC_PHY_BISTAR1__BMRANK__MASK        0x0000000C
#define MEMC_PHY_BISTAR1__BMRANK__INV_MASK    0xFFFFFFF3
#define MEMC_PHY_BISTAR1__BMRANK__HW_DEFAULT  0x3

/* MEMC_PHY_BISTAR1.BAINC - BIST Address Increment: Selects the value by which the SDRAM address is incremented for each write/read access. This value must be at the beginning of a burst boundary, i.e. the lower bits must be "0000" for BL16, "000" for BL8, "00" for BL4 and "0" for BL2. */
#define MEMC_PHY_BISTAR1__BAINC__SHIFT       4
#define MEMC_PHY_BISTAR1__BAINC__WIDTH       12
#define MEMC_PHY_BISTAR1__BAINC__MASK        0x0000FFF0
#define MEMC_PHY_BISTAR1__BAINC__INV_MASK    0xFFFF000F
#define MEMC_PHY_BISTAR1__BAINC__HW_DEFAULT  0x0

/* BIST Address Register 2 */
/* BIST address registers specify the starting address, address increment, and maximum address used during BIST when in DRAM mode. The following tables describe the bits of the BISTAR registers. The BIST address is based on a decoded bank/row/column address. The user should ensure the column address starts at the beginning of a burst boundary. For example, for BL8, A[2:0] = DTCOL[2:0] = 0. */
#define MEMC_PHY_BISTAR2          0x10810120

/* MEMC_PHY_BISTAR2.BMCOL - BIST Maximum Column Address: Specifies the maximum SDRAM column address to be used during BIST before the address increments to the next row. */
#define MEMC_PHY_BISTAR2__BMCOL__SHIFT       0
#define MEMC_PHY_BISTAR2__BMCOL__WIDTH       12
#define MEMC_PHY_BISTAR2__BMCOL__MASK        0x00000FFF
#define MEMC_PHY_BISTAR2__BMCOL__INV_MASK    0xFFFFF000
#define MEMC_PHY_BISTAR2__BMCOL__HW_DEFAULT  0xFFF

/* MEMC_PHY_BISTAR2.BMROW - BIST Maximum Row Address: Specifies the maximum SDRAM row address to be used during BIST before the address increments to the next bank. */
#define MEMC_PHY_BISTAR2__BMROW__SHIFT       12
#define MEMC_PHY_BISTAR2__BMROW__WIDTH       16
#define MEMC_PHY_BISTAR2__BMROW__MASK        0x0FFFF000
#define MEMC_PHY_BISTAR2__BMROW__INV_MASK    0xF0000FFF
#define MEMC_PHY_BISTAR2__BMROW__HW_DEFAULT  0xFFFF

/* MEMC_PHY_BISTAR2.BMBANK - BIST Maximum Bank Address: Specifies the maximum SDRAM bank address to be used during BIST before the address increments to the next rank. */
#define MEMC_PHY_BISTAR2__BMBANK__SHIFT       28
#define MEMC_PHY_BISTAR2__BMBANK__WIDTH       3
#define MEMC_PHY_BISTAR2__BMBANK__MASK        0x70000000
#define MEMC_PHY_BISTAR2__BMBANK__INV_MASK    0x8FFFFFFF
#define MEMC_PHY_BISTAR2__BMBANK__HW_DEFAULT  0x7

/* BIST User Data Pattern Register */
/* This register specifies the user data pattern to be used during BIST. The pattern transmitted on DQ[2*n] is BUDP0[0] on the first beat, BUDP0[1] on the second beat and so on. BUDP1 is transmitted on DQ[2*n+1] in a similar way. After the sixteenth beat, the data pattern wraps around and is repeated until BIST is stopped. BISTUDPR is only valid for DQ pins and is not applicable to AC loopback. */
#define MEMC_PHY_BISTUDPR         0x10810124

/* MEMC_PHY_BISTUDPR.BUDP0 - BIST User Data Pattern 0: Data to be applied on even DQ pins during BIST. */
#define MEMC_PHY_BISTUDPR__BUDP0__SHIFT       0
#define MEMC_PHY_BISTUDPR__BUDP0__WIDTH       16
#define MEMC_PHY_BISTUDPR__BUDP0__MASK        0x0000FFFF
#define MEMC_PHY_BISTUDPR__BUDP0__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTUDPR__BUDP0__HW_DEFAULT  0x0

/* MEMC_PHY_BISTUDPR.BUDP1 - BIST User Data Pattern 1: Data to be applied on odd DQ pins during BIST. */
#define MEMC_PHY_BISTUDPR__BUDP1__SHIFT       16
#define MEMC_PHY_BISTUDPR__BUDP1__WIDTH       16
#define MEMC_PHY_BISTUDPR__BUDP1__MASK        0xFFFF0000
#define MEMC_PHY_BISTUDPR__BUDP1__INV_MASK    0x0000FFFF
#define MEMC_PHY_BISTUDPR__BUDP1__HW_DEFAULT  0xFFFF

/* BIST General Status Register */
/* BIST General Status Register is used to return miscellaneous status of the BIST engine, including done status. */
#define MEMC_PHY_BISTGSR          0x10810128

/* MEMC_PHY_BISTGSR.BDONE - BIST Done: Indicates if set that the BIST has finished executing. This bit is reset to zero when BIST is triggered. */
#define MEMC_PHY_BISTGSR__BDONE__SHIFT       0
#define MEMC_PHY_BISTGSR__BDONE__WIDTH       1
#define MEMC_PHY_BISTGSR__BDONE__MASK        0x00000001
#define MEMC_PHY_BISTGSR__BDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_BISTGSR__BDONE__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.BACERR - BIST Address/Command Error: indicates if set that there is a data comparison error in the address/command lane. */
#define MEMC_PHY_BISTGSR__BACERR__SHIFT       1
#define MEMC_PHY_BISTGSR__BACERR__WIDTH       1
#define MEMC_PHY_BISTGSR__BACERR__MASK        0x00000002
#define MEMC_PHY_BISTGSR__BACERR__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_BISTGSR__BACERR__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.BDXERR - BIST Data Error: indicates if set that there is a data comparison error in the byte lane. */
#define MEMC_PHY_BISTGSR__BDXERR__SHIFT       2
#define MEMC_PHY_BISTGSR__BDXERR__WIDTH       1
#define MEMC_PHY_BISTGSR__BDXERR__MASK        0x00000004
#define MEMC_PHY_BISTGSR__BDXERR__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_BISTGSR__BDXERR__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.PARBER - PAR_IN Bit Error (DIMM Only): Indicates the number of bit errors on PAR_IN */
#define MEMC_PHY_BISTGSR__PARBER__SHIFT       16
#define MEMC_PHY_BISTGSR__PARBER__WIDTH       2
#define MEMC_PHY_BISTGSR__PARBER__MASK        0x00030000
#define MEMC_PHY_BISTGSR__PARBER__INV_MASK    0xFFFCFFFF
#define MEMC_PHY_BISTGSR__PARBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.DMBER - DM Bit Error: Indicates the number of bit errors on data mask (DM) bit. DMBER[1:0] are for even DQS cycles first DM beat, and DMBER[3:2] are for even DQS cycles second DM beat. Similarly, DMBER[5:4] are for odd DQS cycles first DM beat, and DMBER[7:6] are for odd DQS cycles second DM beat. */
#define MEMC_PHY_BISTGSR__DMBER__SHIFT       20
#define MEMC_PHY_BISTGSR__DMBER__WIDTH       8
#define MEMC_PHY_BISTGSR__DMBER__MASK        0x0FF00000
#define MEMC_PHY_BISTGSR__DMBER__INV_MASK    0xF00FFFFF
#define MEMC_PHY_BISTGSR__DMBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.RASBER - RAS Bit Error: Indicates the number of bit errors on RAS. */
#define MEMC_PHY_BISTGSR__RASBER__SHIFT       28
#define MEMC_PHY_BISTGSR__RASBER__WIDTH       2
#define MEMC_PHY_BISTGSR__RASBER__MASK        0x30000000
#define MEMC_PHY_BISTGSR__RASBER__INV_MASK    0xCFFFFFFF
#define MEMC_PHY_BISTGSR__RASBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTGSR.CASBER - CAS Bit Error: Indicates the number of bit errors on CAS. */
#define MEMC_PHY_BISTGSR__CASBER__SHIFT       30
#define MEMC_PHY_BISTGSR__CASBER__WIDTH       2
#define MEMC_PHY_BISTGSR__CASBER__MASK        0xC0000000
#define MEMC_PHY_BISTGSR__CASBER__INV_MASK    0x3FFFFFFF
#define MEMC_PHY_BISTGSR__CASBER__HW_DEFAULT  0x0

/* BIST Word Error Register */
/* BIST Word Error Register is used to return the number of errors on the full word (all bits) of the address/command lane and data byte lane. An error on any bit of the address/command bus will increment the address/command word error count. Similarly any error on the data bits or data mask bits will increment the data byte lane word error count. */
#define MEMC_PHY_BISTWER          0x1081012C

/* MEMC_PHY_BISTWER.ACWER - Address/Command Word Error: Indicates the number of word errors on the address/command lane. An error on any bit of the address/command bus increments the error count. */
#define MEMC_PHY_BISTWER__ACWER__SHIFT       0
#define MEMC_PHY_BISTWER__ACWER__WIDTH       16
#define MEMC_PHY_BISTWER__ACWER__MASK        0x0000FFFF
#define MEMC_PHY_BISTWER__ACWER__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTWER__ACWER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTWER.DXWER - Byte Word Error: Indicates the number of word errors on the byte lane. An error on any bit of the data bus including the data mask bit increments the error count. */
#define MEMC_PHY_BISTWER__DXWER__SHIFT       16
#define MEMC_PHY_BISTWER__DXWER__WIDTH       16
#define MEMC_PHY_BISTWER__DXWER__MASK        0xFFFF0000
#define MEMC_PHY_BISTWER__DXWER__INV_MASK    0x0000FFFF
#define MEMC_PHY_BISTWER__DXWER__HW_DEFAULT  0x0

/* BIST Bit Error Register 0 */
/* BIST error registers are used to return the number of errors on individual data bits of the address/command lane and data byte lane. Bit error registers only return up to 4 errors on the address/command bits and up to 8 errors on the data bits. To diagnose more errors on a bit, use the BIST mask registers to mask all but the bit of interest and re-run the BIST. In this case the word error counter is equal to the bit error counter. The following tables describe the bits of the BIST bit error registers. */
#define MEMC_PHY_BISTBER0         0x10810130

/* MEMC_PHY_BISTBER0.ABER - Address Bit Error: Each group of two bits indicate the bit error count on each of the up to 16 address bits. [1:0] is the error count for A[0], [3:2] for A[1], and so on. 0x0000000 */
#define MEMC_PHY_BISTBER0__ABER__SHIFT       0
#define MEMC_PHY_BISTBER0__ABER__WIDTH       32
#define MEMC_PHY_BISTBER0__ABER__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTBER0__ABER__INV_MASK    0x00000000
#define MEMC_PHY_BISTBER0__ABER__HW_DEFAULT  0x0

/* BIST Bit Error Register 1 */
/* BIST error registers are used to return the number of errors on individual data bits of the address/command lane and data byte lane. Bit error registers only return up to 4 errors on the address/command bits and up to 8 errors on the data bits. To diagnose more errors on a bit, use the BIST mask registers to mask all but the bit of interest and re-run the BIST. In this case the word error counter is equal to the bit error counter. The following tables describe the bits of the BIST bit error registers. */
#define MEMC_PHY_BISTBER1         0x10810134

/* MEMC_PHY_BISTBER1.BABER - Bank Address Bit Error: Each group of two bits indicate the bit error count on each of the up to 3 bank address bits. [1:0] is the error count for BA[0], [3:2] for BA[1], and so on. */
#define MEMC_PHY_BISTBER1__BABER__SHIFT       0
#define MEMC_PHY_BISTBER1__BABER__WIDTH       6
#define MEMC_PHY_BISTBER1__BABER__MASK        0x0000003F
#define MEMC_PHY_BISTBER1__BABER__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_BISTBER1__BABER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTBER1.WEBER - WE# Bit Error: Indicates the number of bit errors on WE#. */
#define MEMC_PHY_BISTBER1__WEBER__SHIFT       6
#define MEMC_PHY_BISTBER1__WEBER__WIDTH       2
#define MEMC_PHY_BISTBER1__WEBER__MASK        0x000000C0
#define MEMC_PHY_BISTBER1__WEBER__INV_MASK    0xFFFFFF3F
#define MEMC_PHY_BISTBER1__WEBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTBER1.CKEBER - CKE Bit Error: Each group of two bits indicate the bit error count on each of the up to 4 CKE bits. [1:0] is the error count for CKE[0], [3:2] for CKE[1], and so on. */
#define MEMC_PHY_BISTBER1__CKEBER__SHIFT       8
#define MEMC_PHY_BISTBER1__CKEBER__WIDTH       8
#define MEMC_PHY_BISTBER1__CKEBER__MASK        0x0000FF00
#define MEMC_PHY_BISTBER1__CKEBER__INV_MASK    0xFFFF00FF
#define MEMC_PHY_BISTBER1__CKEBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTBER1.CSBER - CS# Bit Error: Each group of two bits indicate the bit error count on each of the up to 4 CS# bits. [1:0] is the error count for CS#[0], [3:2] for CS#[1], and so on. */
#define MEMC_PHY_BISTBER1__CSBER__SHIFT       16
#define MEMC_PHY_BISTBER1__CSBER__WIDTH       8
#define MEMC_PHY_BISTBER1__CSBER__MASK        0x00FF0000
#define MEMC_PHY_BISTBER1__CSBER__INV_MASK    0xFF00FFFF
#define MEMC_PHY_BISTBER1__CSBER__HW_DEFAULT  0x0

/* MEMC_PHY_BISTBER1.ODTBER - ODT Bit Error: Each group of two bits indicates the bit error count on each of the up to 4 ODT bits. [1:0] is the error count for ODT[0], [3:2] for ODT[1], and so on. */
#define MEMC_PHY_BISTBER1__ODTBER__SHIFT       24
#define MEMC_PHY_BISTBER1__ODTBER__WIDTH       8
#define MEMC_PHY_BISTBER1__ODTBER__MASK        0xFF000000
#define MEMC_PHY_BISTBER1__ODTBER__INV_MASK    0x00FFFFFF
#define MEMC_PHY_BISTBER1__ODTBER__HW_DEFAULT  0x0

/* BIST Bit Error Register 2 */
/* BIST error registers are used to return the number of errors on individual data bits of the address/command lane and data byte lane. Bit error registers only return up to 4 errors on the address/command bits and up to 8 errors on the data bits. To diagnose more errors on a bit, use the BIST mask registers to mask all but the bit of interest and re-run the BIST. In this case the word error counter is equal to the bit error counter. The following tables describe the bits of the BIST bit error registers. */
#define MEMC_PHY_BISTBER2         0x10810138

/* MEMC_PHY_BISTBER2.DQBER0 - Data Bit Error: The error count for even DQS cycles. The first 16 bits indicate the error count for the first data beat (i.e. the data driven out on DQ[7:0] on the rising edge of DQS). The second 16 bits indicate the error on the second data beat (i.e. the error count of the data driven out on DQ[7:0] on the falling edge of DQS). For each of the 16-bit group, the first 2 bits are for DQ[0], the second for DQ[1], and so on. */
#define MEMC_PHY_BISTBER2__DQBER0__SHIFT       0
#define MEMC_PHY_BISTBER2__DQBER0__WIDTH       32
#define MEMC_PHY_BISTBER2__DQBER0__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTBER2__DQBER0__INV_MASK    0x00000000
#define MEMC_PHY_BISTBER2__DQBER0__HW_DEFAULT  0x0

/* BIST Bit Error Register 3 */
/* BIST error registers are used to return the number of errors on individual data bits of the address/command lane and data byte lane. Bit error registers only return up to 4 errors on the address/command bits and up to 8 errors on the data bits. To diagnose more errors on a bit, use the BIST mask registers to mask all but the bit of interest and re-run the BIST. In this case the word error counter is equal to the bit error counter. The following tables describe the bits of the BIST bit error registers. */
#define MEMC_PHY_BISTBER3         0x1081013C

/* MEMC_PHY_BISTBER3.DQBER1 - Data Bit Error: The error count for odd DQS cycles. The first 16 bits indicate the error count for the first data beat (i.e. the data driven out on DQ[7:0] on the rising edge of DQS). The second 16 bits indicate the error on the second data beat (i.e. the error count of the data driven out on DQ[7:0] on the falling edge of DQS). For each of the 16-bit group, the first 2 bits are for DQ[0], the second for DQ[1], and so on. */
#define MEMC_PHY_BISTBER3__DQBER1__SHIFT       0
#define MEMC_PHY_BISTBER3__DQBER1__WIDTH       32
#define MEMC_PHY_BISTBER3__DQBER1__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTBER3__DQBER1__INV_MASK    0x00000000
#define MEMC_PHY_BISTBER3__DQBER1__HW_DEFAULT  0x0

/* BIST Word Count Register */
/* BIST Word Count Status Register is used to return the number of words received from the address/command lane and data byte lane. */
#define MEMC_PHY_BISTWCSR         0x10810140

/* MEMC_PHY_BISTWCSR.ACWCNT - Address/Command Word Count: Indicates the number of words received from the address/command lane. */
#define MEMC_PHY_BISTWCSR__ACWCNT__SHIFT       0
#define MEMC_PHY_BISTWCSR__ACWCNT__WIDTH       16
#define MEMC_PHY_BISTWCSR__ACWCNT__MASK        0x0000FFFF
#define MEMC_PHY_BISTWCSR__ACWCNT__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTWCSR__ACWCNT__HW_DEFAULT  0x0

/* MEMC_PHY_BISTWCSR.DXWCNT - Byte Word Count: Indicates the number of words received from the byte lane. */
#define MEMC_PHY_BISTWCSR__DXWCNT__SHIFT       16
#define MEMC_PHY_BISTWCSR__DXWCNT__WIDTH       16
#define MEMC_PHY_BISTWCSR__DXWCNT__MASK        0xFFFF0000
#define MEMC_PHY_BISTWCSR__DXWCNT__INV_MASK    0x0000FFFF
#define MEMC_PHY_BISTWCSR__DXWCNT__HW_DEFAULT  0x0

/* BIST Fail Word Register 0 */
/* BIST fail word registers are used to return the last failing word of the address/command lane and data byte lane. The following tables describe the bits of the BIST fail word registers. */
#define MEMC_PHY_BISTFWR0         0x10810144

/* MEMC_PHY_BISTFWR0.AWEBS - Bit status during a word error for each of the up to 16 address bits. */
#define MEMC_PHY_BISTFWR0__AWEBS__SHIFT       0
#define MEMC_PHY_BISTFWR0__AWEBS__WIDTH       16
#define MEMC_PHY_BISTFWR0__AWEBS__MASK        0x0000FFFF
#define MEMC_PHY_BISTFWR0__AWEBS__INV_MASK    0xFFFF0000
#define MEMC_PHY_BISTFWR0__AWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR0.BAWEBS - Bit status during a word error for each of the up to 3 bank address bits. */
#define MEMC_PHY_BISTFWR0__BAWEBS__SHIFT       16
#define MEMC_PHY_BISTFWR0__BAWEBS__WIDTH       3
#define MEMC_PHY_BISTFWR0__BAWEBS__MASK        0x00070000
#define MEMC_PHY_BISTFWR0__BAWEBS__INV_MASK    0xFFF8FFFF
#define MEMC_PHY_BISTFWR0__BAWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR0.WEWEBS - Bit status during a word error for the WE#. */
#define MEMC_PHY_BISTFWR0__WEWEBS__SHIFT       19
#define MEMC_PHY_BISTFWR0__WEWEBS__WIDTH       1
#define MEMC_PHY_BISTFWR0__WEWEBS__MASK        0x00080000
#define MEMC_PHY_BISTFWR0__WEWEBS__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_BISTFWR0__WEWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR0.CKEWEBS - Bit status during a word error for each of the up to 4 CKE bits. */
#define MEMC_PHY_BISTFWR0__CKEWEBS__SHIFT       20
#define MEMC_PHY_BISTFWR0__CKEWEBS__WIDTH       4
#define MEMC_PHY_BISTFWR0__CKEWEBS__MASK        0x00F00000
#define MEMC_PHY_BISTFWR0__CKEWEBS__INV_MASK    0xFF0FFFFF
#define MEMC_PHY_BISTFWR0__CKEWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR0.CSWEBS - Bit status during a word error for each of the up to 4 CS# bits. */
#define MEMC_PHY_BISTFWR0__CSWEBS__SHIFT       24
#define MEMC_PHY_BISTFWR0__CSWEBS__WIDTH       4
#define MEMC_PHY_BISTFWR0__CSWEBS__MASK        0x0F000000
#define MEMC_PHY_BISTFWR0__CSWEBS__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_BISTFWR0__CSWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR0.ODTWEBS - Bit status during a word error for each of the up to 4 ODT bits. */
#define MEMC_PHY_BISTFWR0__ODTWEBS__SHIFT       28
#define MEMC_PHY_BISTFWR0__ODTWEBS__WIDTH       4
#define MEMC_PHY_BISTFWR0__ODTWEBS__MASK        0xF0000000
#define MEMC_PHY_BISTFWR0__ODTWEBS__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_BISTFWR0__ODTWEBS__HW_DEFAULT  0x0

/* BIST Fail Word Register 1 */
/* BIST fail word registers are used to return the last failing word of the address/command lane and data byte lane. The following tables describe the bits of the BIST fail word registers. */
#define MEMC_PHY_BISTFWR1         0x10810148

/* MEMC_PHY_BISTFWR1.RASWEBS - Bit status during a word error for the RAS. */
#define MEMC_PHY_BISTFWR1__RASWEBS__SHIFT       0
#define MEMC_PHY_BISTFWR1__RASWEBS__WIDTH       1
#define MEMC_PHY_BISTFWR1__RASWEBS__MASK        0x00000001
#define MEMC_PHY_BISTFWR1__RASWEBS__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_BISTFWR1__RASWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR1.CASWEBS - Bit status during a word error for the CAS. */
#define MEMC_PHY_BISTFWR1__CASWEBS__SHIFT       1
#define MEMC_PHY_BISTFWR1__CASWEBS__WIDTH       1
#define MEMC_PHY_BISTFWR1__CASWEBS__MASK        0x00000002
#define MEMC_PHY_BISTFWR1__CASWEBS__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_BISTFWR1__CASWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR1.PARWEBS - Bit status during a word error for the PAR_IN. Only for DIMM parity support */
#define MEMC_PHY_BISTFWR1__PARWEBS__SHIFT       26
#define MEMC_PHY_BISTFWR1__PARWEBS__WIDTH       1
#define MEMC_PHY_BISTFWR1__PARWEBS__MASK        0x04000000
#define MEMC_PHY_BISTFWR1__PARWEBS__INV_MASK    0xFBFFFFFF
#define MEMC_PHY_BISTFWR1__PARWEBS__HW_DEFAULT  0x0

/* MEMC_PHY_BISTFWR1.DMWEBS - Bit status during a word error for the data mask (DM) bit. DMWEBS [0] is for the first DM beat, DMWEBS [1] is for the second DM beat, and so on. */
#define MEMC_PHY_BISTFWR1__DMWEBS__SHIFT       28
#define MEMC_PHY_BISTFWR1__DMWEBS__WIDTH       4
#define MEMC_PHY_BISTFWR1__DMWEBS__MASK        0xF0000000
#define MEMC_PHY_BISTFWR1__DMWEBS__INV_MASK    0x0FFFFFFF
#define MEMC_PHY_BISTFWR1__DMWEBS__HW_DEFAULT  0x0

/* BIST Fail Word Register 2 */
/* BIST fail word registers are used to return the last failing word of the address/command lane and data byte lane. The following tables describe the bits of the BIST fail word registers. */
#define MEMC_PHY_BISTFWR2         0x1081014C

/* MEMC_PHY_BISTFWR2.DQWEBS - Bit status during a word error for each of the 8 data (DQ) bits. The first 8 bits indicate the status of the first data beat (i.e. the status of the data driven out on DQ[7:0] on the rising edge of DQS). The second 8 bits indicate the status of the second data beat (i.e. the status of the data driven out on DQ[7:0] on the falling edge of DQS), and so on. For each of the 8-bit group, the first bit is for DQ[0], the second bit is for DQ[1], and so on. */
#define MEMC_PHY_BISTFWR2__DQWEBS__SHIFT       0
#define MEMC_PHY_BISTFWR2__DQWEBS__WIDTH       32
#define MEMC_PHY_BISTFWR2__DQWEBS__MASK        0xFFFFFFFF
#define MEMC_PHY_BISTFWR2__DQWEBS__INV_MASK    0x00000000
#define MEMC_PHY_BISTFWR2__DQWEBS__HW_DEFAULT  0x0

/* Anti-Aging Control Register */
/* This register controls the anti-aging feature and how the feature is applied to the DWC_DDRPHYDATX8 macros when the pub input pin ctlr_idle is asserted. */
#define MEMC_PHY_AACR             0x10810174

/* MEMC_PHY_AACR.AATR - Anti-Aging Toggle Rate: Defines the number of controller clock (ctl_clk) cycles after which the PUB will toggle the data going to DATX8 if the data channel between the controller/PUB and DATX8 has been idle for this long. The default value correspond to a toggling count of 4096 ctl_clk cycles. For a ctl_clk running at 533MHz the toggle rate will be approximately 7.68us. The default value may also be overridden by the macro DWC_AACR_AATR_DFLT. */
#define MEMC_PHY_AACR__AATR__SHIFT       0
#define MEMC_PHY_AACR__AATR__WIDTH       30
#define MEMC_PHY_AACR__AATR__MASK        0x3FFFFFFF
#define MEMC_PHY_AACR__AATR__INV_MASK    0xC0000000
#define MEMC_PHY_AACR__AATR__HW_DEFAULT  0xFF

/* MEMC_PHY_AACR.AAENC - Anti-Aging Enable Control: Enables if set the automatic toggling of the data going to the DATX8 when the data channel from the controller/PUB to DATX8 is idle for programmable number of clock cycles. */
#define MEMC_PHY_AACR__AAENC__SHIFT       30
#define MEMC_PHY_AACR__AAENC__WIDTH       1
#define MEMC_PHY_AACR__AAENC__MASK        0x40000000
#define MEMC_PHY_AACR__AAENC__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_AACR__AAENC__HW_DEFAULT  0x0

/* MEMC_PHY_AACR.AAOENC - Anti-Aging PAD Output Enable Control: Enables if set anti-aging toggling on the pad output enable signal "ctl_oe_n" going into the DATX8s. This will increase power consumption for the anti-aging feature. */
#define MEMC_PHY_AACR__AAOENC__SHIFT       31
#define MEMC_PHY_AACR__AAOENC__WIDTH       1
#define MEMC_PHY_AACR__AAOENC__MASK        0x80000000
#define MEMC_PHY_AACR__AAOENC__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_AACR__AAOENC__HW_DEFAULT  0x0

/* Impedance Control Register 0 */
/* These registers are used for impedance control and calibration to enable the programmable and PVTcompensated on-die termination (ODT) and output impedance of the functional SSTL cells. The following tables describe the bits of the ZQnCR register. Refer to the SSTL PHY data book for details on impedance control. */
#define MEMC_PHY_ZQ0CR0           0x10810180

/* MEMC_PHY_ZQ0CR0.ZDATA - Impedance Over-Ride Data: Data used to directly drive the impedance control. ZDATA field mapping for D3F I/Os is as follows: ZDATA[27:21] is used to select the pull-up on-die termination impedance ZDATA[20:14] is used to select the pull-down on-die termination impedance ZDATA[13:7] is used to select the pull-up output impedance ZDATA[6:0] is used to select the pull-down output impedance ZDATA field mapping for D3A/B/R I/Os is as follows: ZDATA[27:20] is reserved and returns zeros on reads ZDATA[19:15] is used to select the pull-up on-die termination impedance ZDATA[14:10] is used to select the pull-down on-die termination impedance ZDATA[9:5] is used to select the pull-up output impedance ZDATA[4:0] is used to select the pull-down output impedance The default value is 0x000014A for I/O type D3C/R and 0x0001830 for I/O type D3F. */
#define MEMC_PHY_ZQ0CR0__ZDATA__SHIFT       0
#define MEMC_PHY_ZQ0CR0__ZDATA__WIDTH       20
#define MEMC_PHY_ZQ0CR0__ZDATA__MASK        0x000FFFFF
#define MEMC_PHY_ZQ0CR0__ZDATA__INV_MASK    0xFFF00000
#define MEMC_PHY_ZQ0CR0__ZDATA__HW_DEFAULT  0x14A

/* MEMC_PHY_ZQ0CR0.ZDEN - Impedance Over-ride Enable: When this bit is set, it allows users to directly drive the impedance control using the data programmed in the ZDATA field. Otherwise, the control is generated automatically by the impedance control logic. */
#define MEMC_PHY_ZQ0CR0__ZDEN__SHIFT       28
#define MEMC_PHY_ZQ0CR0__ZDEN__WIDTH       1
#define MEMC_PHY_ZQ0CR0__ZDEN__MASK        0x10000000
#define MEMC_PHY_ZQ0CR0__ZDEN__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_ZQ0CR0__ZDEN__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0CR0.ZCALBYP - Impedance Calibration Bypass: Bypasses, if set, impedance calibration of this ZQ control block when impedance calibration is already in progress. Impedance calibration can be disabled prior to trigger by using the ZCALEN bit. */
#define MEMC_PHY_ZQ0CR0__ZCALBYP__SHIFT       29
#define MEMC_PHY_ZQ0CR0__ZCALBYP__WIDTH       1
#define MEMC_PHY_ZQ0CR0__ZCALBYP__MASK        0x20000000
#define MEMC_PHY_ZQ0CR0__ZCALBYP__INV_MASK    0xDFFFFFFF
#define MEMC_PHY_ZQ0CR0__ZCALBYP__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0CR0.ZCALEN - Impedance Calibration Enable: Enables if set the impedance calibration of this ZQ control block when impedance calibration is triggered using either the ZCAL bit of PIR register or the DFI update interface. */
#define MEMC_PHY_ZQ0CR0__ZCALEN__SHIFT       30
#define MEMC_PHY_ZQ0CR0__ZCALEN__WIDTH       1
#define MEMC_PHY_ZQ0CR0__ZCALEN__MASK        0x40000000
#define MEMC_PHY_ZQ0CR0__ZCALEN__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_ZQ0CR0__ZCALEN__HW_DEFAULT  0x1

/* MEMC_PHY_ZQ0CR0.ZQPD - ZQ Power Down: Powers down, if set, the PZQ cell. */
#define MEMC_PHY_ZQ0CR0__ZQPD__SHIFT       31
#define MEMC_PHY_ZQ0CR0__ZQPD__WIDTH       1
#define MEMC_PHY_ZQ0CR0__ZQPD__MASK        0x80000000
#define MEMC_PHY_ZQ0CR0__ZQPD__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_ZQ0CR0__ZQPD__HW_DEFAULT  0x0

/* Impedance Control Register 1 */
/* These registers are used for impedance control and calibration to enable the programmable and PVTcompensated on-die termination (ODT) and output impedance of the functional SSTL cells. The following tables describe the bits of the ZQnCR register. Refer to the SSTL PHY data book for details on impedance control. */
#define MEMC_PHY_ZQ0CR1           0x10810184

/* MEMC_PHY_ZQ0CR1.ZPROG - Impedance Divide Ratio: Selects the external resistor divide ratio to be used to set the output impedance and the on-die termination as follows: ZPROG[7:4] = On-die termination divide select ZPROG[3:0] = Output impedance divide select */
#define MEMC_PHY_ZQ0CR1__ZPROG__SHIFT       0
#define MEMC_PHY_ZQ0CR1__ZPROG__WIDTH       8
#define MEMC_PHY_ZQ0CR1__ZPROG__MASK        0x000000FF
#define MEMC_PHY_ZQ0CR1__ZPROG__INV_MASK    0xFFFFFF00
#define MEMC_PHY_ZQ0CR1__ZPROG__HW_DEFAULT  0x7B

/* MEMC_PHY_ZQ0CR1.DFICU0 - DFI Controller Update Interface 0: Sets this impedance controller to be enabled for calibration when the DFI controller update interface 0 (channel 0) requests an update. */
#define MEMC_PHY_ZQ0CR1__DFICU0__SHIFT       12
#define MEMC_PHY_ZQ0CR1__DFICU0__WIDTH       1
#define MEMC_PHY_ZQ0CR1__DFICU0__MASK        0x00001000
#define MEMC_PHY_ZQ0CR1__DFICU0__INV_MASK    0xFFFFEFFF
#define MEMC_PHY_ZQ0CR1__DFICU0__HW_DEFAULT  0x1

/* MEMC_PHY_ZQ0CR1.DFICU1 - DFI Controller Update Interface 1: Sets this impedance controller to be enabled for calibration when the DFI controller update interface 1 (channel 1) requests an update. Only valid in shared-AC mode. */
#define MEMC_PHY_ZQ0CR1__DFICU1__SHIFT       13
#define MEMC_PHY_ZQ0CR1__DFICU1__WIDTH       1
#define MEMC_PHY_ZQ0CR1__DFICU1__MASK        0x00002000
#define MEMC_PHY_ZQ0CR1__DFICU1__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_ZQ0CR1__DFICU1__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0CR1.DFICCU - DFI Concurrent Controller Update Interface: Sets this impedance controller to be enabled for calibration when both of the DFI controller update interfaces request an update on the same clock. This provides the ability to enable impedance calibration updates for the Address/Command lane. Only valid in shared-AC mode. */
#define MEMC_PHY_ZQ0CR1__DFICCU__SHIFT       14
#define MEMC_PHY_ZQ0CR1__DFICCU__WIDTH       1
#define MEMC_PHY_ZQ0CR1__DFICCU__MASK        0x00004000
#define MEMC_PHY_ZQ0CR1__DFICCU__INV_MASK    0xFFFFBFFF
#define MEMC_PHY_ZQ0CR1__DFICCU__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0CR1.DFIPU0 - DFI Update Interface 0: Sets this impedance controller to be enabled for calibration when the DFI PHY update interface 0 (channel 0) requests an update. */
#define MEMC_PHY_ZQ0CR1__DFIPU0__SHIFT       16
#define MEMC_PHY_ZQ0CR1__DFIPU0__WIDTH       1
#define MEMC_PHY_ZQ0CR1__DFIPU0__MASK        0x00010000
#define MEMC_PHY_ZQ0CR1__DFIPU0__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_ZQ0CR1__DFIPU0__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0CR1.DFIPU1 - DFI Update Interface 1: Sets this impedance controller to be enabled for calibration when the DFI PHY update interface 1 (channel 1) requests an update. Only valid in shared-AC mode. */
#define MEMC_PHY_ZQ0CR1__DFIPU1__SHIFT       17
#define MEMC_PHY_ZQ0CR1__DFIPU1__WIDTH       1
#define MEMC_PHY_ZQ0CR1__DFIPU1__MASK        0x00020000
#define MEMC_PHY_ZQ0CR1__DFIPU1__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_ZQ0CR1__DFIPU1__HW_DEFAULT  0x0

/* ZQ Status Register 0 */
/* These registers are used to provide the status for impedance control and calibration to enable the programmable and PVT-compensated on-die termination (ODT) and output impedance of the functional SSTL cells. Refer to the SSTL PHY databook for details on impedance control. */
#define MEMC_PHY_ZQ0SR0           0x10810188

/* MEMC_PHY_ZQ0SR0.ZCTRL - Impedance Control: Current value of impedance control. ZCTRL field mapping for D3F I/Os is as follows: ZCTRL[27:21] is used to select the pull-up on-die termination impedance ZCTRL[20:14] is used to select the pull-down on-die termination impedance ZCTRL[13:7] is used to select the pull-up output impedance ZCTRL[6:0] is used to select the pull-down output impedance ZCTRL field mapping for D3A/B/R I/Os is as follows: ZCTRL[27:20] is reserved and returns zeros on reads ZCTRL[19:15] is used to select the pull-up on-die termination impedance ZCTRL[14:10] is used to select the pull-down on-die termination impedance ZCTRL[9:5] is used to select the pull-up output impedance ZCTRL[4:0] is used to select the pull-down output impedance Note: The default value is 0x000014A for I/O type D3C/D3R and 0x0001839 for I/O type D3F. */
#define MEMC_PHY_ZQ0SR0__ZCTRL__SHIFT       0
#define MEMC_PHY_ZQ0SR0__ZCTRL__WIDTH       28
#define MEMC_PHY_ZQ0SR0__ZCTRL__MASK        0x0FFFFFFF
#define MEMC_PHY_ZQ0SR0__ZCTRL__INV_MASK    0xF0000000
#define MEMC_PHY_ZQ0SR0__ZCTRL__HW_DEFAULT  0x14A

/* MEMC_PHY_ZQ0SR0.ZERR - Impedance Calibration Error: If set, indicates that there was an error during impedance calibration. */
#define MEMC_PHY_ZQ0SR0__ZERR__SHIFT       30
#define MEMC_PHY_ZQ0SR0__ZERR__WIDTH       1
#define MEMC_PHY_ZQ0SR0__ZERR__MASK        0x40000000
#define MEMC_PHY_ZQ0SR0__ZERR__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_ZQ0SR0__ZERR__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0SR0.ZDONE - Impedance Calibration Done: Indicates that impedance calibration has completed. */
#define MEMC_PHY_ZQ0SR0__ZDONE__SHIFT       31
#define MEMC_PHY_ZQ0SR0__ZDONE__WIDTH       1
#define MEMC_PHY_ZQ0SR0__ZDONE__MASK        0x80000000
#define MEMC_PHY_ZQ0SR0__ZDONE__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_ZQ0SR0__ZDONE__HW_DEFAULT  0x0

/* ZQ Status Register 1 */
/* These registers are used to provide the status for impedance control and calibration to enable the programmable and PVT-compensated on-die termination (ODT) and output impedance of the functional SSTL cells. Refer to the SSTL PHY databook for details on impedance control. */
#define MEMC_PHY_ZQ0SR1           0x1081018C

/* MEMC_PHY_ZQ0SR1.ZPD - Output impedance pull-down calibration status. Valid status encodings are: 00 = Completed with no errors 01 = Overflow error 10 = Underflow error 11 = Calibration in progress */
#define MEMC_PHY_ZQ0SR1__ZPD__SHIFT       0
#define MEMC_PHY_ZQ0SR1__ZPD__WIDTH       2
#define MEMC_PHY_ZQ0SR1__ZPD__MASK        0x00000003
#define MEMC_PHY_ZQ0SR1__ZPD__INV_MASK    0xFFFFFFFC
#define MEMC_PHY_ZQ0SR1__ZPD__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0SR1.ZPU - Output impedance pull-up calibration status. Similar status encodings as ZPD. */
#define MEMC_PHY_ZQ0SR1__ZPU__SHIFT       2
#define MEMC_PHY_ZQ0SR1__ZPU__WIDTH       2
#define MEMC_PHY_ZQ0SR1__ZPU__MASK        0x0000000C
#define MEMC_PHY_ZQ0SR1__ZPU__INV_MASK    0xFFFFFFF3
#define MEMC_PHY_ZQ0SR1__ZPU__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0SR1.OPD - On-die termination (ODT) pull-down calibration status. Similar status encodings as ZPD. */
#define MEMC_PHY_ZQ0SR1__OPD__SHIFT       4
#define MEMC_PHY_ZQ0SR1__OPD__WIDTH       2
#define MEMC_PHY_ZQ0SR1__OPD__MASK        0x00000030
#define MEMC_PHY_ZQ0SR1__OPD__INV_MASK    0xFFFFFFCF
#define MEMC_PHY_ZQ0SR1__OPD__HW_DEFAULT  0x0

/* MEMC_PHY_ZQ0SR1.OPU - On-die termination (ODT) pull-up calibration status. Similar status encodings as ZPD. */
#define MEMC_PHY_ZQ0SR1__OPU__SHIFT       6
#define MEMC_PHY_ZQ0SR1__OPU__WIDTH       2
#define MEMC_PHY_ZQ0SR1__OPU__MASK        0x000000C0
#define MEMC_PHY_ZQ0SR1__OPU__INV_MASK    0xFFFFFF3F
#define MEMC_PHY_ZQ0SR1__OPU__HW_DEFAULT  0x0

/* DATX8 General Configuration Register */
/* This register is used for miscellaneous configurations specific to a particular instantiation of the DATX8 macro. */
#define MEMC_PHY_DX0GCR           0x108101C0

/* MEMC_PHY_DX0GCR.DXEN - Data Byte Enable: Enables if set the data byte. Setting this bit to '0' disables the byte, i.e. the byte is not used in PHY initialization or training and is ignored during SDRAM read/write operations. */
#define MEMC_PHY_DX0GCR__DXEN__SHIFT       0
#define MEMC_PHY_DX0GCR__DXEN__WIDTH       1
#define MEMC_PHY_DX0GCR__DXEN__MASK        0x00000001
#define MEMC_PHY_DX0GCR__DXEN__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX0GCR__DXEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.DQSODT - DQS On-Die Termination: Enables, when set, the on-die termination on the I/O for DQS/DQS# pin of the byte. This bit is ORed with the common DATX8 ODT configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). Note: This bit is only valid when DXnGCR0[9] is '0'. */
#define MEMC_PHY_DX0GCR__DQSODT__SHIFT       1
#define MEMC_PHY_DX0GCR__DQSODT__WIDTH       1
#define MEMC_PHY_DX0GCR__DQSODT__MASK        0x00000002
#define MEMC_PHY_DX0GCR__DQSODT__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX0GCR__DQSODT__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DQODT - Data On-Die Termination: Enables, when set, the on-die termination on the I/O for DQ and DM pins of the byte. This bit is ORed with the common DATX8 ODT configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). Note: This bit is only valid when DXnGCR0[10] is '0'. */
#define MEMC_PHY_DX0GCR__DQODT__SHIFT       2
#define MEMC_PHY_DX0GCR__DQODT__WIDTH       1
#define MEMC_PHY_DX0GCR__DQODT__MASK        0x00000004
#define MEMC_PHY_DX0GCR__DQODT__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX0GCR__DQODT__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DXIOM - Data I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the IOM configuration bit of the individual DATX8(see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX0GCR__DXIOM__SHIFT       3
#define MEMC_PHY_DX0GCR__DXIOM__WIDTH       1
#define MEMC_PHY_DX0GCR__DXIOM__MASK        0x00000008
#define MEMC_PHY_DX0GCR__DXIOM__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX0GCR__DXIOM__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DXPDD - Data Power Down Driver: Powers down, when set, the output driver on I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the common PDD configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX0GCR__DXPDD__SHIFT       4
#define MEMC_PHY_DX0GCR__DXPDD__WIDTH       1
#define MEMC_PHY_DX0GCR__DXPDD__MASK        0x00000010
#define MEMC_PHY_DX0GCR__DXPDD__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX0GCR__DXPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DXPDR - Data Power Down Receiver: Powers down, when set, the input receiver on I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the common PDR configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX0GCR__DXPDR__SHIFT       5
#define MEMC_PHY_DX0GCR__DXPDR__WIDTH       1
#define MEMC_PHY_DX0GCR__DXPDR__MASK        0x00000020
#define MEMC_PHY_DX0GCR__DXPDR__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX0GCR__DXPDR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DQSRPD - DQSR Power Down: Powers down, if set, the PDQSR cell. This bit is ORed with the common PDR configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91) */
#define MEMC_PHY_DX0GCR__DQSRPD__SHIFT       6
#define MEMC_PHY_DX0GCR__DQSRPD__WIDTH       1
#define MEMC_PHY_DX0GCR__DQSRPD__MASK        0x00000040
#define MEMC_PHY_DX0GCR__DQSRPD__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX0GCR__DQSRPD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DSEN - Write DQS Enable: Controls whether the write DQS going to the SDRAM is enabled (toggling) or disabled (static value) and whether the DQS is inverted. DQS# is always the inversion of DQS. These values are valid only when DQS/DQS# output enable is on, otherwise the DQS/DQS# is tristated. Valid settings are: 00 = Reserved 01 = DQS toggling with normal polarity (This should be the default setting) 10 = Reserved 11 = Reserved */
#define MEMC_PHY_DX0GCR__DSEN__SHIFT       7
#define MEMC_PHY_DX0GCR__DSEN__WIDTH       2
#define MEMC_PHY_DX0GCR__DSEN__MASK        0x00000180
#define MEMC_PHY_DX0GCR__DSEN__INV_MASK    0xFFFFFE7F
#define MEMC_PHY_DX0GCR__DSEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.DQSRTT - DQS Dynamic RTT Control: If set, the on die termination (ODT) control of the DQS/DQS# SSTL I/O is dynamically generated to enable the ODT during read operation and disabled otherwise. By setting this bit to '0' the dynamic ODT feature is disabled. To control ODT statically this bit must be set to '0' and DXnGCR0[1] (DQSODT) is used to enable ODT (when set to '1') or disable ODT(when set to '0'). */
#define MEMC_PHY_DX0GCR__DQSRTT__SHIFT       9
#define MEMC_PHY_DX0GCR__DQSRTT__WIDTH       1
#define MEMC_PHY_DX0GCR__DQSRTT__MASK        0x00000200
#define MEMC_PHY_DX0GCR__DQSRTT__INV_MASK    0xFFFFFDFF
#define MEMC_PHY_DX0GCR__DQSRTT__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.DQRTT - DQ Dynamic RTT Control: If set, the on die termination (ODT) control of the DQ/DM SSTL I/O is dynamically generated to enable the ODT during read operation and disabled otherwise. By setting this bit to '0' the dynamic ODT feature is disabled. To control ODT statically this bit must be set to '0' and DXnGCR0[2] (DQODT) is used to enable ODT (when set to '1') or disable ODT(when set to '0'). */
#define MEMC_PHY_DX0GCR__DQRTT__SHIFT       10
#define MEMC_PHY_DX0GCR__DQRTT__WIDTH       1
#define MEMC_PHY_DX0GCR__DQRTT__MASK        0x00000400
#define MEMC_PHY_DX0GCR__DQRTT__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_DX0GCR__DQRTT__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.RTTOH - RTT Output Hold: Indicates the number of clock cycles (from 0 to 3) after the read data postamble for which ODT control should remain set to DQSODT for DQS or DQODT for DQ/DM before disabling it (setting it to '0') when using dynamic ODT control. ODT is disabled almost RTTOH clock cycles after the read postamble. */
#define MEMC_PHY_DX0GCR__RTTOH__SHIFT       11
#define MEMC_PHY_DX0GCR__RTTOH__WIDTH       2
#define MEMC_PHY_DX0GCR__RTTOH__MASK        0x00001800
#define MEMC_PHY_DX0GCR__RTTOH__INV_MASK    0xFFFFE7FF
#define MEMC_PHY_DX0GCR__RTTOH__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.RTTOAL - RTT On Additive Latency: Indicates when the ODT control of DQ/DQS SSTL I/Os is set to the value in DQODT/DQSODT during read cycles. Valid values are: 0 = ODT control is set to DQSODT/DQODT almost two cycles before read data preamble 1 = ODT control is set to DQSODT/DQODT almost one cycle before read data preamble */
#define MEMC_PHY_DX0GCR__RTTOAL__SHIFT       13
#define MEMC_PHY_DX0GCR__RTTOAL__WIDTH       1
#define MEMC_PHY_DX0GCR__RTTOAL__MASK        0x00002000
#define MEMC_PHY_DX0GCR__RTTOAL__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_DX0GCR__RTTOAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.DXOEO - Data Byte Output Enable Override: Specifies whether the output I/O output enable for the byte lane should be set to a fixed value. Valid values are: 00 = No override. Output enable is controlled by DFI transactions 01 = Ouput enable is asserted (I/O is forced to output mode). 10 = Output enable is de-asserted (I/O is forced to input mode) 11 = Reserved */
#define MEMC_PHY_DX0GCR__DXOEO__SHIFT       14
#define MEMC_PHY_DX0GCR__DXOEO__WIDTH       2
#define MEMC_PHY_DX0GCR__DXOEO__MASK        0x0000C000
#define MEMC_PHY_DX0GCR__DXOEO__INV_MASK    0xFFFF3FFF
#define MEMC_PHY_DX0GCR__DXOEO__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.PLLRST - PLL Rest: Resets the byte PLL by driving the PLL reset pin. This bit is not selfclearing and a '0' must be written to de-assert the reset. This bit is ORed with the global PLLRST configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX0GCR__PLLRST__SHIFT       16
#define MEMC_PHY_DX0GCR__PLLRST__WIDTH       1
#define MEMC_PHY_DX0GCR__PLLRST__MASK        0x00010000
#define MEMC_PHY_DX0GCR__PLLRST__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_DX0GCR__PLLRST__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.PLLPD - PLL Power Down: Puts the byte PLL in power down mode by driving the PLL power down pin. This bit is not self-clearing and a '0' must be written to de-assert the power-down. This bit is ORed with the global PLLPD configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX0GCR__PLLPD__SHIFT       17
#define MEMC_PHY_DX0GCR__PLLPD__WIDTH       1
#define MEMC_PHY_DX0GCR__PLLPD__MASK        0x00020000
#define MEMC_PHY_DX0GCR__PLLPD__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_DX0GCR__PLLPD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.GSHIFT - Gear Shift: Enables, if set, rapid locking mode on the byte PLL. This bit is ORed with the global GSHIFT configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX0GCR__GSHIFT__SHIFT       18
#define MEMC_PHY_DX0GCR__GSHIFT__WIDTH       1
#define MEMC_PHY_DX0GCR__GSHIFT__MASK        0x00040000
#define MEMC_PHY_DX0GCR__GSHIFT__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_DX0GCR__GSHIFT__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.PLLBYP - PLL Bypass: Puts the byte PLL in bypass mode by driving the PLL bypass pin. This bit is not self-clearing and a '0' must be written to de-assert the bypass. This bit is ORed with the global BYP configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX0GCR__PLLBYP__SHIFT       19
#define MEMC_PHY_DX0GCR__PLLBYP__WIDTH       1
#define MEMC_PHY_DX0GCR__PLLBYP__MASK        0x00080000
#define MEMC_PHY_DX0GCR__PLLBYP__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_DX0GCR__PLLBYP__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GCR.WLRKEN - Write Level Rank Enable: Specifies the ranks that should be write leveled for this byte. Write leveling responses from ranks that are not enabled for write leveling for a particular byte are ignored and write leveling is flagged as done for these ranks. WLRKEN[0] enables rank 0, [1] enables rank 1, [2] enables rank 2, and [3] enables rank 3. */
#define MEMC_PHY_DX0GCR__WLRKEN__SHIFT       26
#define MEMC_PHY_DX0GCR__WLRKEN__WIDTH       4
#define MEMC_PHY_DX0GCR__WLRKEN__MASK        0x3C000000
#define MEMC_PHY_DX0GCR__WLRKEN__INV_MASK    0xC3FFFFFF
#define MEMC_PHY_DX0GCR__WLRKEN__HW_DEFAULT  0xF

/* MEMC_PHY_DX0GCR.MDLEN - Master Delay Line Enable: Enables, if set, the DATX8 master delay line calibration to perform subsequent period measurements following the initial period measurements that are performed after reset or when calibration is manually triggered. These additional measurements are accumulated and filtered as long as this bit remains high. This bit is ANDed with the common DATX8 MDL enable bit. */
#define MEMC_PHY_DX0GCR__MDLEN__SHIFT       30
#define MEMC_PHY_DX0GCR__MDLEN__WIDTH       1
#define MEMC_PHY_DX0GCR__MDLEN__MASK        0x40000000
#define MEMC_PHY_DX0GCR__MDLEN__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_DX0GCR__MDLEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX0GCR.CALBYP - Calibration Bypass: Prevents, if set, period measurement calibration from automatically triggering after PHY initialization. */
#define MEMC_PHY_DX0GCR__CALBYP__SHIFT       31
#define MEMC_PHY_DX0GCR__CALBYP__WIDTH       1
#define MEMC_PHY_DX0GCR__CALBYP__MASK        0x80000000
#define MEMC_PHY_DX0GCR__CALBYP__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_DX0GCR__CALBYP__HW_DEFAULT  0x0

/* DATX8 General Status Register 0 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX0GSR0          0x108101C4

/* MEMC_PHY_DX0GSR0.WDQCAL - Write DQ Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the write DQ LCDL. */
#define MEMC_PHY_DX0GSR0__WDQCAL__SHIFT       0
#define MEMC_PHY_DX0GSR0__WDQCAL__WIDTH       1
#define MEMC_PHY_DX0GSR0__WDQCAL__MASK        0x00000001
#define MEMC_PHY_DX0GSR0__WDQCAL__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX0GSR0__WDQCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.RDQSCAL - Read DQS Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS LCDL. */
#define MEMC_PHY_DX0GSR0__RDQSCAL__SHIFT       1
#define MEMC_PHY_DX0GSR0__RDQSCAL__WIDTH       1
#define MEMC_PHY_DX0GSR0__RDQSCAL__MASK        0x00000002
#define MEMC_PHY_DX0GSR0__RDQSCAL__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX0GSR0__RDQSCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.RDQSNCAL - Read DQS# Calibration (Type B/B1 PHY Only): Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS# LCDL. */
#define MEMC_PHY_DX0GSR0__RDQSNCAL__SHIFT       2
#define MEMC_PHY_DX0GSR0__RDQSNCAL__WIDTH       1
#define MEMC_PHY_DX0GSR0__RDQSNCAL__MASK        0x00000004
#define MEMC_PHY_DX0GSR0__RDQSNCAL__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX0GSR0__RDQSNCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.GDQSCAL - Read DQS gating Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS gating LCDL. */
#define MEMC_PHY_DX0GSR0__GDQSCAL__SHIFT       3
#define MEMC_PHY_DX0GSR0__GDQSCAL__WIDTH       1
#define MEMC_PHY_DX0GSR0__GDQSCAL__MASK        0x00000008
#define MEMC_PHY_DX0GSR0__GDQSCAL__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX0GSR0__GDQSCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.WLCAL - Write Leveling Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the write leveling slave delay line. */
#define MEMC_PHY_DX0GSR0__WLCAL__SHIFT       4
#define MEMC_PHY_DX0GSR0__WLCAL__WIDTH       1
#define MEMC_PHY_DX0GSR0__WLCAL__MASK        0x00000010
#define MEMC_PHY_DX0GSR0__WLCAL__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX0GSR0__WLCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.WLDONE - Write Leveling Done: Indicates, if set, that the DATX8 has completed write leveling. */
#define MEMC_PHY_DX0GSR0__WLDONE__SHIFT       5
#define MEMC_PHY_DX0GSR0__WLDONE__WIDTH       1
#define MEMC_PHY_DX0GSR0__WLDONE__MASK        0x00000020
#define MEMC_PHY_DX0GSR0__WLDONE__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX0GSR0__WLDONE__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.WLERR - Write Leveling Error: Indicates, if set, that there is a write leveling error in the DATX8. */
#define MEMC_PHY_DX0GSR0__WLERR__SHIFT       6
#define MEMC_PHY_DX0GSR0__WLERR__WIDTH       1
#define MEMC_PHY_DX0GSR0__WLERR__MASK        0x00000040
#define MEMC_PHY_DX0GSR0__WLERR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX0GSR0__WLERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.WLPRD - Write Leveling Period: Returns the DDR clock period measured by the write leveling LCDL during calibration. The measured period is used to generate the control of the write leveling pipeline which is a function of the write-leveling delay and the clock period. This value is PVT compensated. */
#define MEMC_PHY_DX0GSR0__WLPRD__SHIFT       7
#define MEMC_PHY_DX0GSR0__WLPRD__WIDTH       8
#define MEMC_PHY_DX0GSR0__WLPRD__MASK        0x00007F80
#define MEMC_PHY_DX0GSR0__WLPRD__INV_MASK    0xFFFF807F
#define MEMC_PHY_DX0GSR0__WLPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.DPLOCK - DATX8 PLL Lock: Indicates, if set, that the DATX8 PLL has locked. This is a direct status of the DATX8 PLL lock pin. */
#define MEMC_PHY_DX0GSR0__DPLOCK__SHIFT       15
#define MEMC_PHY_DX0GSR0__DPLOCK__WIDTH       1
#define MEMC_PHY_DX0GSR0__DPLOCK__MASK        0x00008000
#define MEMC_PHY_DX0GSR0__DPLOCK__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_DX0GSR0__DPLOCK__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.GDQSPRD - Read DQS gating Period: Returns the DDR clock period measured by the read DQS gating LCDL during calibration. This value is PVT compensated. */
#define MEMC_PHY_DX0GSR0__GDQSPRD__SHIFT       16
#define MEMC_PHY_DX0GSR0__GDQSPRD__WIDTH       8
#define MEMC_PHY_DX0GSR0__GDQSPRD__MASK        0x00FF0000
#define MEMC_PHY_DX0GSR0__GDQSPRD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX0GSR0__GDQSPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.QSGERR - DQS Gate Training Error: Indicates if set that there is an error in DQS gate training. One bit for each of the up to 4 ranks. */
#define MEMC_PHY_DX0GSR0__QSGERR__SHIFT       24
#define MEMC_PHY_DX0GSR0__QSGERR__WIDTH       4
#define MEMC_PHY_DX0GSR0__QSGERR__MASK        0x0F000000
#define MEMC_PHY_DX0GSR0__QSGERR__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_DX0GSR0__QSGERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR0.WLDQ - Write Leveling DQ Status: Captures the write leveling DQ status from the DRAM during software write leveling. */
#define MEMC_PHY_DX0GSR0__WLDQ__SHIFT       28
#define MEMC_PHY_DX0GSR0__WLDQ__WIDTH       1
#define MEMC_PHY_DX0GSR0__WLDQ__MASK        0x10000000
#define MEMC_PHY_DX0GSR0__WLDQ__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_DX0GSR0__WLDQ__HW_DEFAULT  0x0

/* DATX8 General Status Register 1 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX0GSR1          0x108101C8

/* MEMC_PHY_DX0GSR1.DLTDONE - Delay Line Test Done: Indicates, if set, that the PHY control block has finished doing period measurement of the DATX8 delay line digital test output. */
#define MEMC_PHY_DX0GSR1__DLTDONE__SHIFT       0
#define MEMC_PHY_DX0GSR1__DLTDONE__WIDTH       1
#define MEMC_PHY_DX0GSR1__DLTDONE__MASK        0x00000001
#define MEMC_PHY_DX0GSR1__DLTDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX0GSR1__DLTDONE__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR1.DLTCODE - Delay Line Test Code: Returns the code measured by the PHY control block that corresponds to the period of the DATX8 delay line digital test output. */
#define MEMC_PHY_DX0GSR1__DLTCODE__SHIFT       1
#define MEMC_PHY_DX0GSR1__DLTCODE__WIDTH       24
#define MEMC_PHY_DX0GSR1__DLTCODE__MASK        0x01FFFFFE
#define MEMC_PHY_DX0GSR1__DLTCODE__INV_MASK    0xFE000001
#define MEMC_PHY_DX0GSR1__DLTCODE__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 0 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX0BDLR0         0x108101CC

/* MEMC_PHY_DX0BDLR0.DQ0WBD - DQ0 Write Bit Delay: Delay select for the BDL on DQ0 write path. */
#define MEMC_PHY_DX0BDLR0__DQ0WBD__SHIFT       0
#define MEMC_PHY_DX0BDLR0__DQ0WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR0__DQ0WBD__MASK        0x0000003F
#define MEMC_PHY_DX0BDLR0__DQ0WBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX0BDLR0__DQ0WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR0.DQ1WBD - DQ1 Write Bit Delay: Delay select for the BDL on DQ1 write path. */
#define MEMC_PHY_DX0BDLR0__DQ1WBD__SHIFT       6
#define MEMC_PHY_DX0BDLR0__DQ1WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR0__DQ1WBD__MASK        0x00000FC0
#define MEMC_PHY_DX0BDLR0__DQ1WBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX0BDLR0__DQ1WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR0.DQ2WBD - DQ2 Write Bit Delay: Delay select for the BDL on DQ2 write path. */
#define MEMC_PHY_DX0BDLR0__DQ2WBD__SHIFT       12
#define MEMC_PHY_DX0BDLR0__DQ2WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR0__DQ2WBD__MASK        0x0003F000
#define MEMC_PHY_DX0BDLR0__DQ2WBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX0BDLR0__DQ2WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR0.DQ3WBD - DQ3 Write Bit Delay: Delay select for the BDL on DQ3 write path */
#define MEMC_PHY_DX0BDLR0__DQ3WBD__SHIFT       18
#define MEMC_PHY_DX0BDLR0__DQ3WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR0__DQ3WBD__MASK        0x00FC0000
#define MEMC_PHY_DX0BDLR0__DQ3WBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX0BDLR0__DQ3WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR0.DQ4WBD - DQ4 Write Bit Delay: Delay select for the BDL on DQ4 write path. */
#define MEMC_PHY_DX0BDLR0__DQ4WBD__SHIFT       24
#define MEMC_PHY_DX0BDLR0__DQ4WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR0__DQ4WBD__MASK        0x3F000000
#define MEMC_PHY_DX0BDLR0__DQ4WBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX0BDLR0__DQ4WBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 1 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX0BDLR1         0x108101D0

/* MEMC_PHY_DX0BDLR1.DQ5WBD - DQ5 Write Bit Delay: Delay select for the BDL on DQ5 write path. */
#define MEMC_PHY_DX0BDLR1__DQ5WBD__SHIFT       0
#define MEMC_PHY_DX0BDLR1__DQ5WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR1__DQ5WBD__MASK        0x0000003F
#define MEMC_PHY_DX0BDLR1__DQ5WBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX0BDLR1__DQ5WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR1.DQ6WBD - DQ6 Write Bit Delay: Delay select for the BDL on DQ6 write path. */
#define MEMC_PHY_DX0BDLR1__DQ6WBD__SHIFT       6
#define MEMC_PHY_DX0BDLR1__DQ6WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR1__DQ6WBD__MASK        0x00000FC0
#define MEMC_PHY_DX0BDLR1__DQ6WBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX0BDLR1__DQ6WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR1.DQ7WBD - DQ7 Write Bit Delay: Delay select for the BDL on DQ7 write path. */
#define MEMC_PHY_DX0BDLR1__DQ7WBD__SHIFT       12
#define MEMC_PHY_DX0BDLR1__DQ7WBD__WIDTH       6
#define MEMC_PHY_DX0BDLR1__DQ7WBD__MASK        0x0003F000
#define MEMC_PHY_DX0BDLR1__DQ7WBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX0BDLR1__DQ7WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR1.DMWBD - DM Write Bit Delay: Delay select for the BDL on DM write path. */
#define MEMC_PHY_DX0BDLR1__DMWBD__SHIFT       18
#define MEMC_PHY_DX0BDLR1__DMWBD__WIDTH       6
#define MEMC_PHY_DX0BDLR1__DMWBD__MASK        0x00FC0000
#define MEMC_PHY_DX0BDLR1__DMWBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX0BDLR1__DMWBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR1.DSWBD - DQS Write Bit Delay: Delay select for the BDL on DQS write path */
#define MEMC_PHY_DX0BDLR1__DSWBD__SHIFT       24
#define MEMC_PHY_DX0BDLR1__DSWBD__WIDTH       6
#define MEMC_PHY_DX0BDLR1__DSWBD__MASK        0x3F000000
#define MEMC_PHY_DX0BDLR1__DSWBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX0BDLR1__DSWBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 2 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX0BDLR2         0x108101D4

/* MEMC_PHY_DX0BDLR2.DSOEBD - DQS Output Enable Bit Delay: Delay select for the BDL on DQS output enable path */
#define MEMC_PHY_DX0BDLR2__DSOEBD__SHIFT       0
#define MEMC_PHY_DX0BDLR2__DSOEBD__WIDTH       6
#define MEMC_PHY_DX0BDLR2__DSOEBD__MASK        0x0000003F
#define MEMC_PHY_DX0BDLR2__DSOEBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX0BDLR2__DSOEBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR2.DQOEBD - DQ Output Enable Bit Delay: Delay select for the BDL on DQ/DM output enable path. */
#define MEMC_PHY_DX0BDLR2__DQOEBD__SHIFT       6
#define MEMC_PHY_DX0BDLR2__DQOEBD__WIDTH       6
#define MEMC_PHY_DX0BDLR2__DQOEBD__MASK        0x00000FC0
#define MEMC_PHY_DX0BDLR2__DQOEBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX0BDLR2__DQOEBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR2.DSRBD - DQS Read Bit Delay: Delay select for the BDL on DQS read path */
#define MEMC_PHY_DX0BDLR2__DSRBD__SHIFT       12
#define MEMC_PHY_DX0BDLR2__DSRBD__WIDTH       6
#define MEMC_PHY_DX0BDLR2__DSRBD__MASK        0x0003F000
#define MEMC_PHY_DX0BDLR2__DSRBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX0BDLR2__DSRBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 3 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX0BDLR3         0x108101D8

/* MEMC_PHY_DX0BDLR3.DQ0RBD - DQ0 Read Bit Delay: Delay select for the BDL on DQ0 read path. */
#define MEMC_PHY_DX0BDLR3__DQ0RBD__SHIFT       0
#define MEMC_PHY_DX0BDLR3__DQ0RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR3__DQ0RBD__MASK        0x0000003F
#define MEMC_PHY_DX0BDLR3__DQ0RBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX0BDLR3__DQ0RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR3.DQ1RBD - DQ1 Read Bit Delay: Delay select for the BDL on DQ1 read path. */
#define MEMC_PHY_DX0BDLR3__DQ1RBD__SHIFT       6
#define MEMC_PHY_DX0BDLR3__DQ1RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR3__DQ1RBD__MASK        0x00000FC0
#define MEMC_PHY_DX0BDLR3__DQ1RBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX0BDLR3__DQ1RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR3.DQ2RBD - DQ2 Read Bit Delay: Delay select for the BDL on DQ2 read path. */
#define MEMC_PHY_DX0BDLR3__DQ2RBD__SHIFT       12
#define MEMC_PHY_DX0BDLR3__DQ2RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR3__DQ2RBD__MASK        0x0003F000
#define MEMC_PHY_DX0BDLR3__DQ2RBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX0BDLR3__DQ2RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR3.DQ3RBD - DQ3 Read Bit Delay: Delay select for the BDL on DQ3 read path */
#define MEMC_PHY_DX0BDLR3__DQ3RBD__SHIFT       18
#define MEMC_PHY_DX0BDLR3__DQ3RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR3__DQ3RBD__MASK        0x00FC0000
#define MEMC_PHY_DX0BDLR3__DQ3RBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX0BDLR3__DQ3RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR3.DQ4RBD - DQ4 Read Bit Delay: Delay select for the BDL on DQ4 read path. */
#define MEMC_PHY_DX0BDLR3__DQ4RBD__SHIFT       24
#define MEMC_PHY_DX0BDLR3__DQ4RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR3__DQ4RBD__MASK        0x3F000000
#define MEMC_PHY_DX0BDLR3__DQ4RBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX0BDLR3__DQ4RBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 4 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX0BDLR4         0x108101DC

/* MEMC_PHY_DX0BDLR4.DQ5RBD - DQ5 Read Bit Delay: Delay select for the BDL on DQ5 read path. */
#define MEMC_PHY_DX0BDLR4__DQ5RBD__SHIFT       0
#define MEMC_PHY_DX0BDLR4__DQ5RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR4__DQ5RBD__MASK        0x0000003F
#define MEMC_PHY_DX0BDLR4__DQ5RBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX0BDLR4__DQ5RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR4.DQ6RBD - DQ6 Read Bit Delay: Delay select for the BDL on DQ6 read path. */
#define MEMC_PHY_DX0BDLR4__DQ6RBD__SHIFT       6
#define MEMC_PHY_DX0BDLR4__DQ6RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR4__DQ6RBD__MASK        0x00000FC0
#define MEMC_PHY_DX0BDLR4__DQ6RBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX0BDLR4__DQ6RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR4.DQ7RBD - DQ7 Read Bit Delay: Delay select for the BDL on DQ7 read path. */
#define MEMC_PHY_DX0BDLR4__DQ7RBD__SHIFT       12
#define MEMC_PHY_DX0BDLR4__DQ7RBD__WIDTH       6
#define MEMC_PHY_DX0BDLR4__DQ7RBD__MASK        0x0003F000
#define MEMC_PHY_DX0BDLR4__DQ7RBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX0BDLR4__DQ7RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0BDLR4.DMRBD - DM Read Bit Delay: Delay select for the BDL on DM read path. */
#define MEMC_PHY_DX0BDLR4__DMRBD__SHIFT       18
#define MEMC_PHY_DX0BDLR4__DMRBD__WIDTH       6
#define MEMC_PHY_DX0BDLR4__DMRBD__MASK        0x00FC0000
#define MEMC_PHY_DX0BDLR4__DMRBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX0BDLR4__DMRBD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 0 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX0LCDLR0        0x108101E0

/* MEMC_PHY_DX0LCDLR0.R0WLD - Rank 0 Write Leveling Delay: Rank 0 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX0LCDLR0__R0WLD__SHIFT       0
#define MEMC_PHY_DX0LCDLR0__R0WLD__WIDTH       8
#define MEMC_PHY_DX0LCDLR0__R0WLD__MASK        0x000000FF
#define MEMC_PHY_DX0LCDLR0__R0WLD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX0LCDLR0__R0WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR0.R1WLD - Rank 1 Write Leveling Delay: Rank 1 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX0LCDLR0__R1WLD__SHIFT       8
#define MEMC_PHY_DX0LCDLR0__R1WLD__WIDTH       8
#define MEMC_PHY_DX0LCDLR0__R1WLD__MASK        0x0000FF00
#define MEMC_PHY_DX0LCDLR0__R1WLD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX0LCDLR0__R1WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR0.R2WLD - Rank 2 Write Leveling Delay: Rank 2 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX0LCDLR0__R2WLD__SHIFT       16
#define MEMC_PHY_DX0LCDLR0__R2WLD__WIDTH       8
#define MEMC_PHY_DX0LCDLR0__R2WLD__MASK        0x00FF0000
#define MEMC_PHY_DX0LCDLR0__R2WLD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX0LCDLR0__R2WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR0.R3WLD - Rank 3 Write Leveling Delay: Rank 3 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX0LCDLR0__R3WLD__SHIFT       24
#define MEMC_PHY_DX0LCDLR0__R3WLD__WIDTH       8
#define MEMC_PHY_DX0LCDLR0__R3WLD__MASK        0xFF000000
#define MEMC_PHY_DX0LCDLR0__R3WLD__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DX0LCDLR0__R3WLD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 1 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX0LCDLR1        0x108101E4

/* MEMC_PHY_DX0LCDLR1.WDQD - Write Data Delay: Delay select for the write data (WDQ) LCDL */
#define MEMC_PHY_DX0LCDLR1__WDQD__SHIFT       0
#define MEMC_PHY_DX0LCDLR1__WDQD__WIDTH       8
#define MEMC_PHY_DX0LCDLR1__WDQD__MASK        0x000000FF
#define MEMC_PHY_DX0LCDLR1__WDQD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX0LCDLR1__WDQD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR1.RDQSD - Read DQS Delay: Delay select for the read DQS (RDQS) LCDL */
#define MEMC_PHY_DX0LCDLR1__RDQSD__SHIFT       8
#define MEMC_PHY_DX0LCDLR1__RDQSD__WIDTH       8
#define MEMC_PHY_DX0LCDLR1__RDQSD__MASK        0x0000FF00
#define MEMC_PHY_DX0LCDLR1__RDQSD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX0LCDLR1__RDQSD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 2 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX0LCDLR2        0x108101E8

/* MEMC_PHY_DX0LCDLR2.R0DQSGD - Rank 0 Read DQS Gating Delay: Rank 0 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX0LCDLR2__R0DQSGD__SHIFT       0
#define MEMC_PHY_DX0LCDLR2__R0DQSGD__WIDTH       8
#define MEMC_PHY_DX0LCDLR2__R0DQSGD__MASK        0x000000FF
#define MEMC_PHY_DX0LCDLR2__R0DQSGD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX0LCDLR2__R0DQSGD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR2.R1 - DQSGD Rank 1 Read DQS Gating Delay: Rank 1 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX0LCDLR2__R1__SHIFT       8
#define MEMC_PHY_DX0LCDLR2__R1__WIDTH       8
#define MEMC_PHY_DX0LCDLR2__R1__MASK        0x0000FF00
#define MEMC_PHY_DX0LCDLR2__R1__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX0LCDLR2__R1__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR2.R2 - DQSGD Rank 2 Read DQS Gating Delay: Rank 2 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX0LCDLR2__R2__SHIFT       16
#define MEMC_PHY_DX0LCDLR2__R2__WIDTH       8
#define MEMC_PHY_DX0LCDLR2__R2__MASK        0x00FF0000
#define MEMC_PHY_DX0LCDLR2__R2__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX0LCDLR2__R2__HW_DEFAULT  0x0

/* MEMC_PHY_DX0LCDLR2.R3 - DQSGD Rank 3 Read DQS Gating Delay: Rank 3 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX0LCDLR2__R3__SHIFT       24
#define MEMC_PHY_DX0LCDLR2__R3__WIDTH       8
#define MEMC_PHY_DX0LCDLR2__R3__MASK        0xFF000000
#define MEMC_PHY_DX0LCDLR2__R3__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DX0LCDLR2__R3__HW_DEFAULT  0x0

/* DATX8 Master Delay Line Register */
/* The DATX8 Master Delay Line Registers return different period values measured by the master delay lines in the DATX8 macros. In the default normal operating conditions, the value of the DXnMDLR registers change based on ongoing calibration and measurements (refer to "Delay Line VT Drift Detection and Compensation" on page 163). For testing purposes, the calibration of the Master Delay Line must be stopped before the DXnMDLR register can be written without the calibration changing the value written. This can be achieved with the following methods: Method 1: Used for enabling delay line testing: 1. Write DXCCR[2] (MDLEN) = 1'b0 2. Write PGCR1 . Write PGCR1[26] (INHVT) = 1'b1 to stop VT compensation . Write PGCR1[22:15] (DLDLMT) =8'b0 to prevent the PUB from requesting a PHY initiated DFI update request . Write PGCR1[25] (DXHRST) = 1'b0 to prevent phy_qvld from being asserted when switching into oscillator test mode 3. Write PGCR0[5:0] = 6'b000000 OR disable the DFI update interface from issuing controller initiated DFI updates 4. Write DXnMDLR register = 32'h00000000 to clear the IPRD, TPRD and MDLD bit fields 5. Write PGCR0[6] (DLTMODE) = 1'b1 6. Write PGCR1[25] (DXHRST) = 1'b1 7. <perform delay line testing> 8. Issue system reset when done Method 2: Used for stopping the Master Delay Line calibrations without entering delay line test mode 1. Write DXCCR[2] (MDLEN) = 1'b0 2. Write PGCR1[26] (INHVT) = 1'b1 to stop VT compensation and write PGCR[22:15]=8'b0 to prevent a PHY initiated DFI update request 3. Write PGCR0[5:0] = 6'b000000 OR disable the DFI update interface from issuing controller initiated DFI updates 4. Wait for approximately 5000 clock periods for all Master Delay Lines to complete the calibration 5. Calibrations will be completed and the DXnMDLR registers can be written. 6. To re-enable master delay line calibrations, VT calculations and VT compensation 7. Write DXCCR[2] (MDLEN) = 1'b1 8. Write PGCR1[26] (INHVT) = 1'b0 9. Write PGCR0[5:0] = 6'b111111 10. System ready */
#define MEMC_PHY_DX0MDLR          0x108101EC

/* MEMC_PHY_DX0MDLR.IPRD - Initial Period: Initial period measured by the master delay line calibration for VT drift compensation. This value is used as the denominator when calculating the ratios of updates during VT compensation. */
#define MEMC_PHY_DX0MDLR__IPRD__SHIFT       0
#define MEMC_PHY_DX0MDLR__IPRD__WIDTH       8
#define MEMC_PHY_DX0MDLR__IPRD__MASK        0x000000FF
#define MEMC_PHY_DX0MDLR__IPRD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX0MDLR__IPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0MDLR.TPRD - Target Period: Target period measured by the master delay line calibration for VT drift compensation. This is the current measured value of the period and is continuously updated if the MDL is enabled to do so. */
#define MEMC_PHY_DX0MDLR__TPRD__SHIFT       8
#define MEMC_PHY_DX0MDLR__TPRD__WIDTH       8
#define MEMC_PHY_DX0MDLR__TPRD__MASK        0x0000FF00
#define MEMC_PHY_DX0MDLR__TPRD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX0MDLR__TPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX0MDLR.MDLD - MDL Delay: Delay select for the LCDL for the Master Delay Line. */
#define MEMC_PHY_DX0MDLR__MDLD__SHIFT       16
#define MEMC_PHY_DX0MDLR__MDLD__WIDTH       8
#define MEMC_PHY_DX0MDLR__MDLD__MASK        0x00FF0000
#define MEMC_PHY_DX0MDLR__MDLD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX0MDLR__MDLD__HW_DEFAULT  0x0

/* DATX8 General Timing Register */
/* This register is used to control various timing settings of the byte lane, including DQS gating system latency and write leveling system latency. */
#define MEMC_PHY_DX0GTR           0x108101F0

/* MEMC_PHY_DX0GTR.R0DGSL - Rank n DQS Gating System Latency: This is used to increase the number of clock cycles needed to expect valid DDR read data by up to seven extra clock cycles. This is used to compensate for board delays and other system delays. Power-up default is 000 (i.e. no extra clock cycles required). The SL fields are initially set by the PUB during automatic DQS data training but these values can be overwritten by a direct write to this register. Every three bits of this register control the latency of each of the (up to) four ranks. R0DGSL controls the latency of rank 0, R1DGSL controls rank 1, and so on. Valid values are 0 to 7: */
#define MEMC_PHY_DX0GTR__R0DGSL__SHIFT       0
#define MEMC_PHY_DX0GTR__R0DGSL__WIDTH       3
#define MEMC_PHY_DX0GTR__R0DGSL__MASK        0x00000007
#define MEMC_PHY_DX0GTR__R0DGSL__INV_MASK    0xFFFFFFF8
#define MEMC_PHY_DX0GTR__R0DGSL__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GTR.R0WLSL - Rank n Write Leveling System Latency: This is used to adjust the write latency after write leveling. Power-up default is 01 (i.e. no extra clock cycles required). The SL fields are initially set by the PUB during automatic write leveling but these values can be overwritten by a direct write to this register. Every two bits of this register control the latency of each of the (up to) four ranks. R0WLSL controls the latency of rank 0, R1WLSL controls rank 1, and so on. Valid values: 00 = Write latency = WL - 1 01 = Write latency = WL 10 = Write latency = WL + 1 11 = Reserved */
#define MEMC_PHY_DX0GTR__R0WLSL__SHIFT       12
#define MEMC_PHY_DX0GTR__R0WLSL__WIDTH       2
#define MEMC_PHY_DX0GTR__R0WLSL__MASK        0x00003000
#define MEMC_PHY_DX0GTR__R0WLSL__INV_MASK    0xFFFFCFFF
#define MEMC_PHY_DX0GTR__R0WLSL__HW_DEFAULT  0x1

/* DATX8 General Status Register 2 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX0GSR2          0x108101F4

/* MEMC_PHY_DX0GSR2.RDERR - Read Bit Deskew Error: Indicates, if set, that the DATX8 has encountered an error during execution of the read bit deskew training. */
#define MEMC_PHY_DX0GSR2__RDERR__SHIFT       0
#define MEMC_PHY_DX0GSR2__RDERR__WIDTH       1
#define MEMC_PHY_DX0GSR2__RDERR__MASK        0x00000001
#define MEMC_PHY_DX0GSR2__RDERR__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX0GSR2__RDERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.RDWN - Read Bit Deskew Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the read bit deskew training. */
#define MEMC_PHY_DX0GSR2__RDWN__SHIFT       1
#define MEMC_PHY_DX0GSR2__RDWN__WIDTH       1
#define MEMC_PHY_DX0GSR2__RDWN__MASK        0x00000002
#define MEMC_PHY_DX0GSR2__RDWN__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX0GSR2__RDWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.WDERR - Write Bit Deskew Error: Indicates, if set, that the DATX8 has encountered an error during execution of the write bit deskew training. */
#define MEMC_PHY_DX0GSR2__WDERR__SHIFT       2
#define MEMC_PHY_DX0GSR2__WDERR__WIDTH       1
#define MEMC_PHY_DX0GSR2__WDERR__MASK        0x00000004
#define MEMC_PHY_DX0GSR2__WDERR__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX0GSR2__WDERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.WDWN - Write Bit Deskew Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the write bit deskew training. */
#define MEMC_PHY_DX0GSR2__WDWN__SHIFT       3
#define MEMC_PHY_DX0GSR2__WDWN__WIDTH       1
#define MEMC_PHY_DX0GSR2__WDWN__MASK        0x00000008
#define MEMC_PHY_DX0GSR2__WDWN__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX0GSR2__WDWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.REERR - Read Data Eye Training Error: Indicates, if set, that the DATX8 has encountered an error during execution of the read data eye training. */
#define MEMC_PHY_DX0GSR2__REERR__SHIFT       4
#define MEMC_PHY_DX0GSR2__REERR__WIDTH       1
#define MEMC_PHY_DX0GSR2__REERR__MASK        0x00000010
#define MEMC_PHY_DX0GSR2__REERR__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX0GSR2__REERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.REWN - Read Data Eye Training Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the read data eye training. */
#define MEMC_PHY_DX0GSR2__REWN__SHIFT       5
#define MEMC_PHY_DX0GSR2__REWN__WIDTH       1
#define MEMC_PHY_DX0GSR2__REWN__MASK        0x00000020
#define MEMC_PHY_DX0GSR2__REWN__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX0GSR2__REWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.WEERR - Write Data Eye Training Error: Indicates, if set, that the DATX8 has encountered an error during execution of the write data eye training. */
#define MEMC_PHY_DX0GSR2__WEERR__SHIFT       6
#define MEMC_PHY_DX0GSR2__WEERR__WIDTH       1
#define MEMC_PHY_DX0GSR2__WEERR__MASK        0x00000040
#define MEMC_PHY_DX0GSR2__WEERR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX0GSR2__WEERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.WEWN - Write Data Eye Training Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the write data eye training. */
#define MEMC_PHY_DX0GSR2__WEWN__SHIFT       7
#define MEMC_PHY_DX0GSR2__WEWN__WIDTH       1
#define MEMC_PHY_DX0GSR2__WEWN__MASK        0x00000080
#define MEMC_PHY_DX0GSR2__WEWN__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_DX0GSR2__WEWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX0GSR2.ESTAT - Error Status: If an error occurred for this lane as indicated by RDERR, WDERR, REERR or WEERR the error status code can provide additional information regard when the error occurred during the algorithm execution. */
#define MEMC_PHY_DX0GSR2__ESTAT__SHIFT       8
#define MEMC_PHY_DX0GSR2__ESTAT__WIDTH       4
#define MEMC_PHY_DX0GSR2__ESTAT__MASK        0x00000F00
#define MEMC_PHY_DX0GSR2__ESTAT__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_DX0GSR2__ESTAT__HW_DEFAULT  0x0

/* DATX8 General Configuration Register */
/* This register is used for miscellaneous configurations specific to a particular instantiation of the DATX8 macro. */
#define MEMC_PHY_DX1GCR           0x10810200

/* MEMC_PHY_DX1GCR.DXEN - Data Byte Enable: Enables if set the data byte. Setting this bit to '0' disables the byte, i.e. the byte is not used in PHY initialization or training and is ignored during SDRAM read/write operations. */
#define MEMC_PHY_DX1GCR__DXEN__SHIFT       0
#define MEMC_PHY_DX1GCR__DXEN__WIDTH       1
#define MEMC_PHY_DX1GCR__DXEN__MASK        0x00000001
#define MEMC_PHY_DX1GCR__DXEN__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX1GCR__DXEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.DQSODT - DQS On-Die Termination: Enables, when set, the on-die termination on the I/O for DQS/DQS# pin of the byte. This bit is ORed with the common DATX8 ODT configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). Note: This bit is only valid when DXnGCR0[9] is '0'. */
#define MEMC_PHY_DX1GCR__DQSODT__SHIFT       1
#define MEMC_PHY_DX1GCR__DQSODT__WIDTH       1
#define MEMC_PHY_DX1GCR__DQSODT__MASK        0x00000002
#define MEMC_PHY_DX1GCR__DQSODT__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX1GCR__DQSODT__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DQODT - Data On-Die Termination: Enables, when set, the on-die termination on the I/O for DQ and DM pins of the byte. This bit is ORed with the common DATX8 ODT configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). Note: This bit is only valid when DXnGCR0[10] is '0'. */
#define MEMC_PHY_DX1GCR__DQODT__SHIFT       2
#define MEMC_PHY_DX1GCR__DQODT__WIDTH       1
#define MEMC_PHY_DX1GCR__DQODT__MASK        0x00000004
#define MEMC_PHY_DX1GCR__DQODT__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX1GCR__DQODT__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DXIOM - Data I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the IOM configuration bit of the individual DATX8(see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX1GCR__DXIOM__SHIFT       3
#define MEMC_PHY_DX1GCR__DXIOM__WIDTH       1
#define MEMC_PHY_DX1GCR__DXIOM__MASK        0x00000008
#define MEMC_PHY_DX1GCR__DXIOM__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX1GCR__DXIOM__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DXPDD - Data Power Down Driver: Powers down, when set, the output driver on I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the common PDD configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX1GCR__DXPDD__SHIFT       4
#define MEMC_PHY_DX1GCR__DXPDD__WIDTH       1
#define MEMC_PHY_DX1GCR__DXPDD__MASK        0x00000010
#define MEMC_PHY_DX1GCR__DXPDD__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX1GCR__DXPDD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DXPDR - Data Power Down Receiver: Powers down, when set, the input receiver on I/O for DQ, DM, and DQS/DQS# pins of the byte. This bit is ORed with the common PDR configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91). */
#define MEMC_PHY_DX1GCR__DXPDR__SHIFT       5
#define MEMC_PHY_DX1GCR__DXPDR__WIDTH       1
#define MEMC_PHY_DX1GCR__DXPDR__MASK        0x00000020
#define MEMC_PHY_DX1GCR__DXPDR__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX1GCR__DXPDR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DQSRPD - DQSR Power Down: Powers down, if set, the PDQSR cell. This bit is ORed with the common PDR configuration bit (see "DATX8 Common Configuration Register (DXCCR)" on page 91) */
#define MEMC_PHY_DX1GCR__DQSRPD__SHIFT       6
#define MEMC_PHY_DX1GCR__DQSRPD__WIDTH       1
#define MEMC_PHY_DX1GCR__DQSRPD__MASK        0x00000040
#define MEMC_PHY_DX1GCR__DQSRPD__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX1GCR__DQSRPD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DSEN - Write DQS Enable: Controls whether the write DQS going to the SDRAM is enabled (toggling) or disabled (static value) and whether the DQS is inverted. DQS# is always the inversion of DQS. These values are valid only when DQS/DQS# output enable is on, otherwise the DQS/DQS# is tristated. Valid settings are: 00 = Reserved 01 = DQS toggling with normal polarity (This should be the default setting) 10 = Reserved 11 = Reserved */
#define MEMC_PHY_DX1GCR__DSEN__SHIFT       7
#define MEMC_PHY_DX1GCR__DSEN__WIDTH       2
#define MEMC_PHY_DX1GCR__DSEN__MASK        0x00000180
#define MEMC_PHY_DX1GCR__DSEN__INV_MASK    0xFFFFFE7F
#define MEMC_PHY_DX1GCR__DSEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.DQSRTT - DQS Dynamic RTT Control: If set, the on die termination (ODT) control of the DQS/DQS# SSTL I/O is dynamically generated to enable the ODT during read operation and disabled otherwise. By setting this bit to '0' the dynamic ODT feature is disabled. To control ODT statically this bit must be set to '0' and DXnGCR0[1] (DQSODT) is used to enable ODT (when set to '1') or disable ODT(when set to '0'). */
#define MEMC_PHY_DX1GCR__DQSRTT__SHIFT       9
#define MEMC_PHY_DX1GCR__DQSRTT__WIDTH       1
#define MEMC_PHY_DX1GCR__DQSRTT__MASK        0x00000200
#define MEMC_PHY_DX1GCR__DQSRTT__INV_MASK    0xFFFFFDFF
#define MEMC_PHY_DX1GCR__DQSRTT__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.DQRTT - DQ Dynamic RTT Control: If set, the on die termination (ODT) control of the DQ/DM SSTL I/O is dynamically generated to enable the ODT during read operation and disabled otherwise. By setting this bit to '0' the dynamic ODT feature is disabled. To control ODT statically this bit must be set to '0' and DXnGCR0[2] (DQODT) is used to enable ODT (when set to '1') or disable ODT(when set to '0'). */
#define MEMC_PHY_DX1GCR__DQRTT__SHIFT       10
#define MEMC_PHY_DX1GCR__DQRTT__WIDTH       1
#define MEMC_PHY_DX1GCR__DQRTT__MASK        0x00000400
#define MEMC_PHY_DX1GCR__DQRTT__INV_MASK    0xFFFFFBFF
#define MEMC_PHY_DX1GCR__DQRTT__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.RTTOH - RTT Output Hold: Indicates the number of clock cycles (from 0 to 3) after the read data postamble for which ODT control should remain set to DQSODT for DQS or DQODT for DQ/DM before disabling it (setting it to '0') when using dynamic ODT control. ODT is disabled almost RTTOH clock cycles after the read postamble. */
#define MEMC_PHY_DX1GCR__RTTOH__SHIFT       11
#define MEMC_PHY_DX1GCR__RTTOH__WIDTH       2
#define MEMC_PHY_DX1GCR__RTTOH__MASK        0x00001800
#define MEMC_PHY_DX1GCR__RTTOH__INV_MASK    0xFFFFE7FF
#define MEMC_PHY_DX1GCR__RTTOH__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.RTTOAL - RTT On Additive Latency: Indicates when the ODT control of DQ/DQS SSTL I/Os is set to the value in DQODT/DQSODT during read cycles. Valid values are: 0 = ODT control is set to DQSODT/DQODT almost two cycles before read data preamble 1 = ODT control is set to DQSODT/DQODT almost one cycle before read data preamble */
#define MEMC_PHY_DX1GCR__RTTOAL__SHIFT       13
#define MEMC_PHY_DX1GCR__RTTOAL__WIDTH       1
#define MEMC_PHY_DX1GCR__RTTOAL__MASK        0x00002000
#define MEMC_PHY_DX1GCR__RTTOAL__INV_MASK    0xFFFFDFFF
#define MEMC_PHY_DX1GCR__RTTOAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.DXOEO - Data Byte Output Enable Override: Specifies whether the output I/O output enable for the byte lane should be set to a fixed value. Valid values are: 00 = No override. Output enable is controlled by DFI transactions 01 = Ouput enable is asserted (I/O is forced to output mode). 10 = Output enable is de-asserted (I/O is forced to input mode) 11 = Reserved */
#define MEMC_PHY_DX1GCR__DXOEO__SHIFT       14
#define MEMC_PHY_DX1GCR__DXOEO__WIDTH       2
#define MEMC_PHY_DX1GCR__DXOEO__MASK        0x0000C000
#define MEMC_PHY_DX1GCR__DXOEO__INV_MASK    0xFFFF3FFF
#define MEMC_PHY_DX1GCR__DXOEO__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.PLLRST - PLL Rest: Resets the byte PLL by driving the PLL reset pin. This bit is not selfclearing and a '0' must be written to de-assert the reset. This bit is ORed with the global PLLRST configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX1GCR__PLLRST__SHIFT       16
#define MEMC_PHY_DX1GCR__PLLRST__WIDTH       1
#define MEMC_PHY_DX1GCR__PLLRST__MASK        0x00010000
#define MEMC_PHY_DX1GCR__PLLRST__INV_MASK    0xFFFEFFFF
#define MEMC_PHY_DX1GCR__PLLRST__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.PLLPD - PLL Power Down: Puts the byte PLL in power down mode by driving the PLL power down pin. This bit is not self-clearing and a '0' must be written to de-assert the power-down. This bit is ORed with the global PLLPD configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX1GCR__PLLPD__SHIFT       17
#define MEMC_PHY_DX1GCR__PLLPD__WIDTH       1
#define MEMC_PHY_DX1GCR__PLLPD__MASK        0x00020000
#define MEMC_PHY_DX1GCR__PLLPD__INV_MASK    0xFFFDFFFF
#define MEMC_PHY_DX1GCR__PLLPD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.GSHIFT - Gear Shift: Enables, if set, rapid locking mode on the byte PLL. This bit is ORed with the global GSHIFT configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX1GCR__GSHIFT__SHIFT       18
#define MEMC_PHY_DX1GCR__GSHIFT__WIDTH       1
#define MEMC_PHY_DX1GCR__GSHIFT__MASK        0x00040000
#define MEMC_PHY_DX1GCR__GSHIFT__INV_MASK    0xFFFBFFFF
#define MEMC_PHY_DX1GCR__GSHIFT__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.PLLBYP - PLL Bypass: Puts the byte PLL in bypass mode by driving the PLL bypass pin. This bit is not self-clearing and a '0' must be written to de-assert the bypass. This bit is ORed with the global BYP configuration bit (see Table 3-10 on page 83). */
#define MEMC_PHY_DX1GCR__PLLBYP__SHIFT       19
#define MEMC_PHY_DX1GCR__PLLBYP__WIDTH       1
#define MEMC_PHY_DX1GCR__PLLBYP__MASK        0x00080000
#define MEMC_PHY_DX1GCR__PLLBYP__INV_MASK    0xFFF7FFFF
#define MEMC_PHY_DX1GCR__PLLBYP__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GCR.WLRKEN - Write Level Rank Enable: Specifies the ranks that should be write leveled for this byte. Write leveling responses from ranks that are not enabled for write leveling for a particular byte are ignored and write leveling is flagged as done for these ranks. WLRKEN[0] enables rank 0, [1] enables rank 1, [2] enables rank 2, and [3] enables rank 3. */
#define MEMC_PHY_DX1GCR__WLRKEN__SHIFT       26
#define MEMC_PHY_DX1GCR__WLRKEN__WIDTH       4
#define MEMC_PHY_DX1GCR__WLRKEN__MASK        0x3C000000
#define MEMC_PHY_DX1GCR__WLRKEN__INV_MASK    0xC3FFFFFF
#define MEMC_PHY_DX1GCR__WLRKEN__HW_DEFAULT  0xF

/* MEMC_PHY_DX1GCR.MDLEN - Master Delay Line Enable: Enables, if set, the DATX8 master delay line calibration to perform subsequent period measurements following the initial period measurements that are performed after reset or when calibration is manually triggered. These additional measurements are accumulated and filtered as long as this bit remains high. This bit is ANDed with the common DATX8 MDL enable bit. */
#define MEMC_PHY_DX1GCR__MDLEN__SHIFT       30
#define MEMC_PHY_DX1GCR__MDLEN__WIDTH       1
#define MEMC_PHY_DX1GCR__MDLEN__MASK        0x40000000
#define MEMC_PHY_DX1GCR__MDLEN__INV_MASK    0xBFFFFFFF
#define MEMC_PHY_DX1GCR__MDLEN__HW_DEFAULT  0x1

/* MEMC_PHY_DX1GCR.CALBYP - Calibration Bypass: Prevents, if set, period measurement calibration from automatically triggering after PHY initialization. */
#define MEMC_PHY_DX1GCR__CALBYP__SHIFT       31
#define MEMC_PHY_DX1GCR__CALBYP__WIDTH       1
#define MEMC_PHY_DX1GCR__CALBYP__MASK        0x80000000
#define MEMC_PHY_DX1GCR__CALBYP__INV_MASK    0x7FFFFFFF
#define MEMC_PHY_DX1GCR__CALBYP__HW_DEFAULT  0x0

/* DATX8 General Status Register 0 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX1GSR0          0x10810204

/* MEMC_PHY_DX1GSR0.WDQCAL - Write DQ Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the write DQ LCDL. */
#define MEMC_PHY_DX1GSR0__WDQCAL__SHIFT       0
#define MEMC_PHY_DX1GSR0__WDQCAL__WIDTH       1
#define MEMC_PHY_DX1GSR0__WDQCAL__MASK        0x00000001
#define MEMC_PHY_DX1GSR0__WDQCAL__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX1GSR0__WDQCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.RDQSCAL - Read DQS Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS LCDL. */
#define MEMC_PHY_DX1GSR0__RDQSCAL__SHIFT       1
#define MEMC_PHY_DX1GSR0__RDQSCAL__WIDTH       1
#define MEMC_PHY_DX1GSR0__RDQSCAL__MASK        0x00000002
#define MEMC_PHY_DX1GSR0__RDQSCAL__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX1GSR0__RDQSCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.RDQSNCAL - Read DQS# Calibration (Type B/B1 PHY Only): Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS# LCDL. */
#define MEMC_PHY_DX1GSR0__RDQSNCAL__SHIFT       2
#define MEMC_PHY_DX1GSR0__RDQSNCAL__WIDTH       1
#define MEMC_PHY_DX1GSR0__RDQSNCAL__MASK        0x00000004
#define MEMC_PHY_DX1GSR0__RDQSNCAL__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX1GSR0__RDQSNCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.GDQSCAL - Read DQS gating Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the read DQS gating LCDL. */
#define MEMC_PHY_DX1GSR0__GDQSCAL__SHIFT       3
#define MEMC_PHY_DX1GSR0__GDQSCAL__WIDTH       1
#define MEMC_PHY_DX1GSR0__GDQSCAL__MASK        0x00000008
#define MEMC_PHY_DX1GSR0__GDQSCAL__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX1GSR0__GDQSCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.WLCAL - Write Leveling Calibration: Indicates, if set, that the DATX8 has finished doing period measurement calibration for the write leveling slave delay line. */
#define MEMC_PHY_DX1GSR0__WLCAL__SHIFT       4
#define MEMC_PHY_DX1GSR0__WLCAL__WIDTH       1
#define MEMC_PHY_DX1GSR0__WLCAL__MASK        0x00000010
#define MEMC_PHY_DX1GSR0__WLCAL__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX1GSR0__WLCAL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.WLDONE - Write Leveling Done: Indicates, if set, that the DATX8 has completed write leveling. */
#define MEMC_PHY_DX1GSR0__WLDONE__SHIFT       5
#define MEMC_PHY_DX1GSR0__WLDONE__WIDTH       1
#define MEMC_PHY_DX1GSR0__WLDONE__MASK        0x00000020
#define MEMC_PHY_DX1GSR0__WLDONE__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX1GSR0__WLDONE__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.WLERR - Write Leveling Error: Indicates, if set, that there is a write leveling error in the DATX8. */
#define MEMC_PHY_DX1GSR0__WLERR__SHIFT       6
#define MEMC_PHY_DX1GSR0__WLERR__WIDTH       1
#define MEMC_PHY_DX1GSR0__WLERR__MASK        0x00000040
#define MEMC_PHY_DX1GSR0__WLERR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX1GSR0__WLERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.WLPRD - Write Leveling Period: Returns the DDR clock period measured by the write leveling LCDL during calibration. The measured period is used to generate the control of the write leveling pipeline which is a function of the write-leveling delay and the clock period. This value is PVT compensated. */
#define MEMC_PHY_DX1GSR0__WLPRD__SHIFT       7
#define MEMC_PHY_DX1GSR0__WLPRD__WIDTH       8
#define MEMC_PHY_DX1GSR0__WLPRD__MASK        0x00007F80
#define MEMC_PHY_DX1GSR0__WLPRD__INV_MASK    0xFFFF807F
#define MEMC_PHY_DX1GSR0__WLPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.DPLOCK - DATX8 PLL Lock: Indicates, if set, that the DATX8 PLL has locked. This is a direct status of the DATX8 PLL lock pin. */
#define MEMC_PHY_DX1GSR0__DPLOCK__SHIFT       15
#define MEMC_PHY_DX1GSR0__DPLOCK__WIDTH       1
#define MEMC_PHY_DX1GSR0__DPLOCK__MASK        0x00008000
#define MEMC_PHY_DX1GSR0__DPLOCK__INV_MASK    0xFFFF7FFF
#define MEMC_PHY_DX1GSR0__DPLOCK__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.GDQSPRD - Read DQS gating Period: Returns the DDR clock period measured by the read DQS gating LCDL during calibration. This value is PVT compensated. */
#define MEMC_PHY_DX1GSR0__GDQSPRD__SHIFT       16
#define MEMC_PHY_DX1GSR0__GDQSPRD__WIDTH       8
#define MEMC_PHY_DX1GSR0__GDQSPRD__MASK        0x00FF0000
#define MEMC_PHY_DX1GSR0__GDQSPRD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX1GSR0__GDQSPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.QSGERR - DQS Gate Training Error: Indicates if set that there is an error in DQS gate training. One bit for each of the up to 4 ranks. */
#define MEMC_PHY_DX1GSR0__QSGERR__SHIFT       24
#define MEMC_PHY_DX1GSR0__QSGERR__WIDTH       4
#define MEMC_PHY_DX1GSR0__QSGERR__MASK        0x0F000000
#define MEMC_PHY_DX1GSR0__QSGERR__INV_MASK    0xF0FFFFFF
#define MEMC_PHY_DX1GSR0__QSGERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR0.WLDQ - Write Leveling DQ Status: Captures the write leveling DQ status from the DRAM during software write leveling. */
#define MEMC_PHY_DX1GSR0__WLDQ__SHIFT       28
#define MEMC_PHY_DX1GSR0__WLDQ__WIDTH       1
#define MEMC_PHY_DX1GSR0__WLDQ__MASK        0x10000000
#define MEMC_PHY_DX1GSR0__WLDQ__INV_MASK    0xEFFFFFFF
#define MEMC_PHY_DX1GSR0__WLDQ__HW_DEFAULT  0x0

/* DATX8 General Status Register 1 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX1GSR1          0x10810208

/* MEMC_PHY_DX1GSR1.DLTDONE - Delay Line Test Done: Indicates, if set, that the PHY control block has finished doing period measurement of the DATX8 delay line digital test output. */
#define MEMC_PHY_DX1GSR1__DLTDONE__SHIFT       0
#define MEMC_PHY_DX1GSR1__DLTDONE__WIDTH       1
#define MEMC_PHY_DX1GSR1__DLTDONE__MASK        0x00000001
#define MEMC_PHY_DX1GSR1__DLTDONE__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX1GSR1__DLTDONE__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR1.DLTCODE - Delay Line Test Code: Returns the code measured by the PHY control block that corresponds to the period of the DATX8 delay line digital test output. */
#define MEMC_PHY_DX1GSR1__DLTCODE__SHIFT       1
#define MEMC_PHY_DX1GSR1__DLTCODE__WIDTH       24
#define MEMC_PHY_DX1GSR1__DLTCODE__MASK        0x01FFFFFE
#define MEMC_PHY_DX1GSR1__DLTCODE__INV_MASK    0xFE000001
#define MEMC_PHY_DX1GSR1__DLTCODE__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 0 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX1BDLR0         0x1081020C

/* MEMC_PHY_DX1BDLR0.DQ0WBD - DQ0 Write Bit Delay: Delay select for the BDL on DQ0 write path. */
#define MEMC_PHY_DX1BDLR0__DQ0WBD__SHIFT       0
#define MEMC_PHY_DX1BDLR0__DQ0WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR0__DQ0WBD__MASK        0x0000003F
#define MEMC_PHY_DX1BDLR0__DQ0WBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX1BDLR0__DQ0WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR0.DQ1WBD - DQ1 Write Bit Delay: Delay select for the BDL on DQ1 write path. */
#define MEMC_PHY_DX1BDLR0__DQ1WBD__SHIFT       6
#define MEMC_PHY_DX1BDLR0__DQ1WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR0__DQ1WBD__MASK        0x00000FC0
#define MEMC_PHY_DX1BDLR0__DQ1WBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX1BDLR0__DQ1WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR0.DQ2WBD - DQ2 Write Bit Delay: Delay select for the BDL on DQ2 write path. */
#define MEMC_PHY_DX1BDLR0__DQ2WBD__SHIFT       12
#define MEMC_PHY_DX1BDLR0__DQ2WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR0__DQ2WBD__MASK        0x0003F000
#define MEMC_PHY_DX1BDLR0__DQ2WBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX1BDLR0__DQ2WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR0.DQ3WBD - DQ3 Write Bit Delay: Delay select for the BDL on DQ3 write path */
#define MEMC_PHY_DX1BDLR0__DQ3WBD__SHIFT       18
#define MEMC_PHY_DX1BDLR0__DQ3WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR0__DQ3WBD__MASK        0x00FC0000
#define MEMC_PHY_DX1BDLR0__DQ3WBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX1BDLR0__DQ3WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR0.DQ4WBD - DQ4 Write Bit Delay: Delay select for the BDL on DQ4 write path. */
#define MEMC_PHY_DX1BDLR0__DQ4WBD__SHIFT       24
#define MEMC_PHY_DX1BDLR0__DQ4WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR0__DQ4WBD__MASK        0x3F000000
#define MEMC_PHY_DX1BDLR0__DQ4WBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX1BDLR0__DQ4WBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 1 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX1BDLR1         0x10810210

/* MEMC_PHY_DX1BDLR1.DQ5WBD - DQ5 Write Bit Delay: Delay select for the BDL on DQ5 write path. */
#define MEMC_PHY_DX1BDLR1__DQ5WBD__SHIFT       0
#define MEMC_PHY_DX1BDLR1__DQ5WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR1__DQ5WBD__MASK        0x0000003F
#define MEMC_PHY_DX1BDLR1__DQ5WBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX1BDLR1__DQ5WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR1.DQ6WBD - DQ6 Write Bit Delay: Delay select for the BDL on DQ6 write path. */
#define MEMC_PHY_DX1BDLR1__DQ6WBD__SHIFT       6
#define MEMC_PHY_DX1BDLR1__DQ6WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR1__DQ6WBD__MASK        0x00000FC0
#define MEMC_PHY_DX1BDLR1__DQ6WBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX1BDLR1__DQ6WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR1.DQ7WBD - DQ7 Write Bit Delay: Delay select for the BDL on DQ7 write path. */
#define MEMC_PHY_DX1BDLR1__DQ7WBD__SHIFT       12
#define MEMC_PHY_DX1BDLR1__DQ7WBD__WIDTH       6
#define MEMC_PHY_DX1BDLR1__DQ7WBD__MASK        0x0003F000
#define MEMC_PHY_DX1BDLR1__DQ7WBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX1BDLR1__DQ7WBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR1.DMWBD - DM Write Bit Delay: Delay select for the BDL on DM write path. */
#define MEMC_PHY_DX1BDLR1__DMWBD__SHIFT       18
#define MEMC_PHY_DX1BDLR1__DMWBD__WIDTH       6
#define MEMC_PHY_DX1BDLR1__DMWBD__MASK        0x00FC0000
#define MEMC_PHY_DX1BDLR1__DMWBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX1BDLR1__DMWBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR1.DSWBD - DQS Write Bit Delay: Delay select for the BDL on DQS write path */
#define MEMC_PHY_DX1BDLR1__DSWBD__SHIFT       24
#define MEMC_PHY_DX1BDLR1__DSWBD__WIDTH       6
#define MEMC_PHY_DX1BDLR1__DSWBD__MASK        0x3F000000
#define MEMC_PHY_DX1BDLR1__DSWBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX1BDLR1__DSWBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 2 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX1BDLR2         0x10810214

/* MEMC_PHY_DX1BDLR2.DSOEBD - DQS Output Enable Bit Delay: Delay select for the BDL on DQS output enable path */
#define MEMC_PHY_DX1BDLR2__DSOEBD__SHIFT       0
#define MEMC_PHY_DX1BDLR2__DSOEBD__WIDTH       6
#define MEMC_PHY_DX1BDLR2__DSOEBD__MASK        0x0000003F
#define MEMC_PHY_DX1BDLR2__DSOEBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX1BDLR2__DSOEBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR2.DQOEBD - DQ Output Enable Bit Delay: Delay select for the BDL on DQ/DM output enable path. */
#define MEMC_PHY_DX1BDLR2__DQOEBD__SHIFT       6
#define MEMC_PHY_DX1BDLR2__DQOEBD__WIDTH       6
#define MEMC_PHY_DX1BDLR2__DQOEBD__MASK        0x00000FC0
#define MEMC_PHY_DX1BDLR2__DQOEBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX1BDLR2__DQOEBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR2.DSRBD - DQS Read Bit Delay: Delay select for the BDL on DQS read path */
#define MEMC_PHY_DX1BDLR2__DSRBD__SHIFT       12
#define MEMC_PHY_DX1BDLR2__DSRBD__WIDTH       6
#define MEMC_PHY_DX1BDLR2__DSRBD__MASK        0x0003F000
#define MEMC_PHY_DX1BDLR2__DSRBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX1BDLR2__DSRBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 3 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX1BDLR3         0x10810218

/* MEMC_PHY_DX1BDLR3.DQ0RBD - DQ0 Read Bit Delay: Delay select for the BDL on DQ0 read path. */
#define MEMC_PHY_DX1BDLR3__DQ0RBD__SHIFT       0
#define MEMC_PHY_DX1BDLR3__DQ0RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR3__DQ0RBD__MASK        0x0000003F
#define MEMC_PHY_DX1BDLR3__DQ0RBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX1BDLR3__DQ0RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR3.DQ1RBD - DQ1 Read Bit Delay: Delay select for the BDL on DQ1 read path. */
#define MEMC_PHY_DX1BDLR3__DQ1RBD__SHIFT       6
#define MEMC_PHY_DX1BDLR3__DQ1RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR3__DQ1RBD__MASK        0x00000FC0
#define MEMC_PHY_DX1BDLR3__DQ1RBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX1BDLR3__DQ1RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR3.DQ2RBD - DQ2 Read Bit Delay: Delay select for the BDL on DQ2 read path. */
#define MEMC_PHY_DX1BDLR3__DQ2RBD__SHIFT       12
#define MEMC_PHY_DX1BDLR3__DQ2RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR3__DQ2RBD__MASK        0x0003F000
#define MEMC_PHY_DX1BDLR3__DQ2RBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX1BDLR3__DQ2RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR3.DQ3RBD - DQ3 Read Bit Delay: Delay select for the BDL on DQ3 read path */
#define MEMC_PHY_DX1BDLR3__DQ3RBD__SHIFT       18
#define MEMC_PHY_DX1BDLR3__DQ3RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR3__DQ3RBD__MASK        0x00FC0000
#define MEMC_PHY_DX1BDLR3__DQ3RBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX1BDLR3__DQ3RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR3.DQ4RBD - DQ4 Read Bit Delay: Delay select for the BDL on DQ4 read path. */
#define MEMC_PHY_DX1BDLR3__DQ4RBD__SHIFT       24
#define MEMC_PHY_DX1BDLR3__DQ4RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR3__DQ4RBD__MASK        0x3F000000
#define MEMC_PHY_DX1BDLR3__DQ4RBD__INV_MASK    0xC0FFFFFF
#define MEMC_PHY_DX1BDLR3__DQ4RBD__HW_DEFAULT  0x0

/* DATX8 Bit Delay Line Register 4 */
/* The DATX8 bit delay line registers are used to select the delay value on the BDLs used in the DATX8 macros. A single BDL field in the BDLR register connects to a corresponding BDL. The following tables describe the bits of the DATX8 BDLR register. */
#define MEMC_PHY_DX1BDLR4         0x1081021C

/* MEMC_PHY_DX1BDLR4.DQ5RBD - DQ5 Read Bit Delay: Delay select for the BDL on DQ5 read path. */
#define MEMC_PHY_DX1BDLR4__DQ5RBD__SHIFT       0
#define MEMC_PHY_DX1BDLR4__DQ5RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR4__DQ5RBD__MASK        0x0000003F
#define MEMC_PHY_DX1BDLR4__DQ5RBD__INV_MASK    0xFFFFFFC0
#define MEMC_PHY_DX1BDLR4__DQ5RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR4.DQ6RBD - DQ6 Read Bit Delay: Delay select for the BDL on DQ6 read path. */
#define MEMC_PHY_DX1BDLR4__DQ6RBD__SHIFT       6
#define MEMC_PHY_DX1BDLR4__DQ6RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR4__DQ6RBD__MASK        0x00000FC0
#define MEMC_PHY_DX1BDLR4__DQ6RBD__INV_MASK    0xFFFFF03F
#define MEMC_PHY_DX1BDLR4__DQ6RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR4.DQ7RBD - DQ7 Read Bit Delay: Delay select for the BDL on DQ7 read path. */
#define MEMC_PHY_DX1BDLR4__DQ7RBD__SHIFT       12
#define MEMC_PHY_DX1BDLR4__DQ7RBD__WIDTH       6
#define MEMC_PHY_DX1BDLR4__DQ7RBD__MASK        0x0003F000
#define MEMC_PHY_DX1BDLR4__DQ7RBD__INV_MASK    0xFFFC0FFF
#define MEMC_PHY_DX1BDLR4__DQ7RBD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1BDLR4.DMRBD - DM Read Bit Delay: Delay select for the BDL on DM read path. */
#define MEMC_PHY_DX1BDLR4__DMRBD__SHIFT       18
#define MEMC_PHY_DX1BDLR4__DMRBD__WIDTH       6
#define MEMC_PHY_DX1BDLR4__DMRBD__MASK        0x00FC0000
#define MEMC_PHY_DX1BDLR4__DMRBD__INV_MASK    0xFF03FFFF
#define MEMC_PHY_DX1BDLR4__DMRBD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 0 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX1LCDLR0        0x10810220

/* MEMC_PHY_DX1LCDLR0.R0WLD - Rank 0 Write Leveling Delay: Rank 0 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX1LCDLR0__R0WLD__SHIFT       0
#define MEMC_PHY_DX1LCDLR0__R0WLD__WIDTH       8
#define MEMC_PHY_DX1LCDLR0__R0WLD__MASK        0x000000FF
#define MEMC_PHY_DX1LCDLR0__R0WLD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX1LCDLR0__R0WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR0.R1WLD - Rank 1 Write Leveling Delay: Rank 1 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX1LCDLR0__R1WLD__SHIFT       8
#define MEMC_PHY_DX1LCDLR0__R1WLD__WIDTH       8
#define MEMC_PHY_DX1LCDLR0__R1WLD__MASK        0x0000FF00
#define MEMC_PHY_DX1LCDLR0__R1WLD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX1LCDLR0__R1WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR0.R2WLD - Rank 2 Write Leveling Delay: Rank 2 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX1LCDLR0__R2WLD__SHIFT       16
#define MEMC_PHY_DX1LCDLR0__R2WLD__WIDTH       8
#define MEMC_PHY_DX1LCDLR0__R2WLD__MASK        0x00FF0000
#define MEMC_PHY_DX1LCDLR0__R2WLD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX1LCDLR0__R2WLD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR0.R3WLD - Rank 3 Write Leveling Delay: Rank 3 delay select for the write leveling (WL) LCDL */
#define MEMC_PHY_DX1LCDLR0__R3WLD__SHIFT       24
#define MEMC_PHY_DX1LCDLR0__R3WLD__WIDTH       8
#define MEMC_PHY_DX1LCDLR0__R3WLD__MASK        0xFF000000
#define MEMC_PHY_DX1LCDLR0__R3WLD__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DX1LCDLR0__R3WLD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 1 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX1LCDLR1        0x10810224

/* MEMC_PHY_DX1LCDLR1.WDQD - Write Data Delay: Delay select for the write data (WDQ) LCDL */
#define MEMC_PHY_DX1LCDLR1__WDQD__SHIFT       0
#define MEMC_PHY_DX1LCDLR1__WDQD__WIDTH       8
#define MEMC_PHY_DX1LCDLR1__WDQD__MASK        0x000000FF
#define MEMC_PHY_DX1LCDLR1__WDQD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX1LCDLR1__WDQD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR1.RDQSD - Read DQS Delay: Delay select for the read DQS (RDQS) LCDL */
#define MEMC_PHY_DX1LCDLR1__RDQSD__SHIFT       8
#define MEMC_PHY_DX1LCDLR1__RDQSD__WIDTH       8
#define MEMC_PHY_DX1LCDLR1__RDQSD__MASK        0x0000FF00
#define MEMC_PHY_DX1LCDLR1__RDQSD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX1LCDLR1__RDQSD__HW_DEFAULT  0x0

/* DATX8 Local Calibrated Delay Line Register 2 */
/* The DATX8 local calibrated delay line registers are used to select the delay value on the LCDLs used in the DATX8 macros. A single LCDL field in the LCDLR register connects to a corresponding LCDL. The following tables describe the bits of the DATX8 LCDLR registers. The write data delay (WDQD) and the read DQS delay (RDQSD) are automatically derived from the measured period during calibration. WDQD and RDQSD correspond to a 90 degrees phase shift for DQ during writes and DQS during reads, respectively. The 90 degrees phase shift is used to centre DQS into the write and read data eyes. A 90 degrees phase shift is equivalent to half the DDR clock period. After calibration WDQD and RDQSD fields will contain a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). The write leveling delay (RnWLD) and read DQS gating delay DQSGD are derived from the write leveling and DQS training algorithms. These are normally run automatically by the PHY control block or they can be executed in software by the user. After calibration, the RnWLD field contains a value that corresponds to the DDR clock period (or half of the SDRAM clock period), while RnDQSGD field contains a value that corresponds to half the DDR clock period (or a quarter of the SDRAM clock period). After the initial setting, all LCDL register fields are automatically updated by the VT compensation logic to track drifts due to voltage and temperature. The user can however override these values by writing to the respective fields of the register and/or optionally disabling the automatic drift compensation. Note RxWLD (Rank 0-3) field values for the DXnLCDLR0 are programmed to the same value. Cautio It is recommended not running data training and only using fixed values for this register. The fixed values should be applied after PHY initialization. If the fixed values are provided before PHY initialization, then the training algorithm will clear the values and find a data trained result. */
#define MEMC_PHY_DX1LCDLR2        0x10810228

/* MEMC_PHY_DX1LCDLR2.R0DQSGD - Rank 0 Read DQS Gating Delay: Rank 0 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX1LCDLR2__R0DQSGD__SHIFT       0
#define MEMC_PHY_DX1LCDLR2__R0DQSGD__WIDTH       8
#define MEMC_PHY_DX1LCDLR2__R0DQSGD__MASK        0x000000FF
#define MEMC_PHY_DX1LCDLR2__R0DQSGD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX1LCDLR2__R0DQSGD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR2.R1 - DQSGD Rank 1 Read DQS Gating Delay: Rank 1 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX1LCDLR2__R1__SHIFT       8
#define MEMC_PHY_DX1LCDLR2__R1__WIDTH       8
#define MEMC_PHY_DX1LCDLR2__R1__MASK        0x0000FF00
#define MEMC_PHY_DX1LCDLR2__R1__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX1LCDLR2__R1__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR2.R2 - DQSGD Rank 2 Read DQS Gating Delay: Rank 2 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX1LCDLR2__R2__SHIFT       16
#define MEMC_PHY_DX1LCDLR2__R2__WIDTH       8
#define MEMC_PHY_DX1LCDLR2__R2__MASK        0x00FF0000
#define MEMC_PHY_DX1LCDLR2__R2__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX1LCDLR2__R2__HW_DEFAULT  0x0

/* MEMC_PHY_DX1LCDLR2.R3 - DQSGD Rank 3 Read DQS Gating Delay: Rank 3 delay select for the read DQS gating (DQSG) LCDL */
#define MEMC_PHY_DX1LCDLR2__R3__SHIFT       24
#define MEMC_PHY_DX1LCDLR2__R3__WIDTH       8
#define MEMC_PHY_DX1LCDLR2__R3__MASK        0xFF000000
#define MEMC_PHY_DX1LCDLR2__R3__INV_MASK    0x00FFFFFF
#define MEMC_PHY_DX1LCDLR2__R3__HW_DEFAULT  0x0

/* DATX8 Master Delay Line Register */
/* The DATX8 Master Delay Line Registers return different period values measured by the master delay lines in the DATX8 macros. In the default normal operating conditions, the value of the DXnMDLR registers change based on ongoing calibration and measurements (refer to "Delay Line VT Drift Detection and Compensation" on page 163). For testing purposes, the calibration of the Master Delay Line must be stopped before the DXnMDLR register can be written without the calibration changing the value written. This can be achieved with the following methods: Method 1: Used for enabling delay line testing: 1. Write DXCCR[2] (MDLEN) = 1'b0 2. Write PGCR1 . Write PGCR1[26] (INHVT) = 1'b1 to stop VT compensation . Write PGCR1[22:15] (DLDLMT) =8'b0 to prevent the PUB from requesting a PHY initiated DFI update request . Write PGCR1[25] (DXHRST) = 1'b0 to prevent phy_qvld from being asserted when switching into oscillator test mode 3. Write PGCR0[5:0] = 6'b000000 OR disable the DFI update interface from issuing controller initiated DFI updates 4. Write DXnMDLR register = 32'h00000000 to clear the IPRD, TPRD and MDLD bit fields 5. Write PGCR0[6] (DLTMODE) = 1'b1 6. Write PGCR1[25] (DXHRST) = 1'b1 7. <perform delay line testing> 8. Issue system reset when done Method 2: Used for stopping the Master Delay Line calibrations without entering delay line test mode 1. Write DXCCR[2] (MDLEN) = 1'b0 2. Write PGCR1[26] (INHVT) = 1'b1 to stop VT compensation and write PGCR[22:15]=8'b0 to prevent a PHY initiated DFI update request 3. Write PGCR0[5:0] = 6'b000000 OR disable the DFI update interface from issuing controller initiated DFI updates 4. Wait for approximately 5000 clock periods for all Master Delay Lines to complete the calibration 5. Calibrations will be completed and the DXnMDLR registers can be written. 6. To re-enable master delay line calibrations, VT calculations and VT compensation 7. Write DXCCR[2] (MDLEN) = 1'b1 8. Write PGCR1[26] (INHVT) = 1'b0 9. Write PGCR0[5:0] = 6'b111111 10. System ready */
#define MEMC_PHY_DX1MDLR          0x1081022C

/* MEMC_PHY_DX1MDLR.IPRD - Initial Period: Initial period measured by the master delay line calibration for VT drift compensation. This value is used as the denominator when calculating the ratios of updates during VT compensation. */
#define MEMC_PHY_DX1MDLR__IPRD__SHIFT       0
#define MEMC_PHY_DX1MDLR__IPRD__WIDTH       8
#define MEMC_PHY_DX1MDLR__IPRD__MASK        0x000000FF
#define MEMC_PHY_DX1MDLR__IPRD__INV_MASK    0xFFFFFF00
#define MEMC_PHY_DX1MDLR__IPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1MDLR.TPRD - Target Period: Target period measured by the master delay line calibration for VT drift compensation. This is the current measured value of the period and is continuously updated if the MDL is enabled to do so. */
#define MEMC_PHY_DX1MDLR__TPRD__SHIFT       8
#define MEMC_PHY_DX1MDLR__TPRD__WIDTH       8
#define MEMC_PHY_DX1MDLR__TPRD__MASK        0x0000FF00
#define MEMC_PHY_DX1MDLR__TPRD__INV_MASK    0xFFFF00FF
#define MEMC_PHY_DX1MDLR__TPRD__HW_DEFAULT  0x0

/* MEMC_PHY_DX1MDLR.MDLD - MDL Delay: Delay select for the LCDL for the Master Delay Line. */
#define MEMC_PHY_DX1MDLR__MDLD__SHIFT       16
#define MEMC_PHY_DX1MDLR__MDLD__WIDTH       8
#define MEMC_PHY_DX1MDLR__MDLD__MASK        0x00FF0000
#define MEMC_PHY_DX1MDLR__MDLD__INV_MASK    0xFF00FFFF
#define MEMC_PHY_DX1MDLR__MDLD__HW_DEFAULT  0x0

/* DATX8 General Timing Register */
/* This register is used to control various timing settings of the byte lane, including DQS gating system latency and write leveling system latency. */
#define MEMC_PHY_DX1GTR           0x10810230

/* MEMC_PHY_DX1GTR.R0DGSL - Rank n DQS Gating System Latency: This is used to increase the number of clock cycles needed to expect valid DDR read data by up to seven extra clock cycles. This is used to compensate for board delays and other system delays. Power-up default is 000 (i.e. no extra clock cycles required). The SL fields are initially set by the PUB during automatic DQS data training but these values can be overwritten by a direct write to this register. Every three bits of this register control the latency of each of the (up to) four ranks. R0DGSL controls the latency of rank 0, R1DGSL controls rank 1, and so on. Valid values are 0 to 7: */
#define MEMC_PHY_DX1GTR__R0DGSL__SHIFT       0
#define MEMC_PHY_DX1GTR__R0DGSL__WIDTH       3
#define MEMC_PHY_DX1GTR__R0DGSL__MASK        0x00000007
#define MEMC_PHY_DX1GTR__R0DGSL__INV_MASK    0xFFFFFFF8
#define MEMC_PHY_DX1GTR__R0DGSL__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GTR.R0WLSL - Rank n Write Leveling System Latency: This is used to adjust the write latency after write leveling. Power-up default is 01 (i.e. no extra clock cycles required). The SL fields are initially set by the PUB during automatic write leveling but these values can be overwritten by a direct write to this register. Every two bits of this register control the latency of each of the (up to) four ranks. R0WLSL controls the latency of rank 0, R1WLSL controls rank 1, and so on. Valid values: 00 = Write latency = WL - 1 01 = Write latency = WL 10 = Write latency = WL + 1 11 = Reserved */
#define MEMC_PHY_DX1GTR__R0WLSL__SHIFT       12
#define MEMC_PHY_DX1GTR__R0WLSL__WIDTH       2
#define MEMC_PHY_DX1GTR__R0WLSL__MASK        0x00003000
#define MEMC_PHY_DX1GTR__R0WLSL__INV_MASK    0xFFFFCFFF
#define MEMC_PHY_DX1GTR__R0WLSL__HW_DEFAULT  0x1

/* DATX8 General Status Register 2 */
/* These are general status registers for the DATX8. They indicate among other things whether write leveling or period measurement calibration is done. Note that WDQCAL, RDQSCAL, RDQSNCAL, GDQSCAL, and WLCAL are calibration measurement-done flags that should be used for debug purposes if the global calibration done flag (PGSR0[CAL]) is not asserted. These flags will typically assert for a minimum of two configuration clock cycles and then de-assert when all measurement done flags are asserted. */
#define MEMC_PHY_DX1GSR2          0x10810234

/* MEMC_PHY_DX1GSR2.RDERR - Read Bit Deskew Error: Indicates, if set, that the DATX8 has encountered an error during execution of the read bit deskew training. */
#define MEMC_PHY_DX1GSR2__RDERR__SHIFT       0
#define MEMC_PHY_DX1GSR2__RDERR__WIDTH       1
#define MEMC_PHY_DX1GSR2__RDERR__MASK        0x00000001
#define MEMC_PHY_DX1GSR2__RDERR__INV_MASK    0xFFFFFFFE
#define MEMC_PHY_DX1GSR2__RDERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.RDWN - Read Bit Deskew Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the read bit deskew training. */
#define MEMC_PHY_DX1GSR2__RDWN__SHIFT       1
#define MEMC_PHY_DX1GSR2__RDWN__WIDTH       1
#define MEMC_PHY_DX1GSR2__RDWN__MASK        0x00000002
#define MEMC_PHY_DX1GSR2__RDWN__INV_MASK    0xFFFFFFFD
#define MEMC_PHY_DX1GSR2__RDWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.WDERR - Write Bit Deskew Error: Indicates, if set, that the DATX8 has encountered an error during execution of the write bit deskew training. */
#define MEMC_PHY_DX1GSR2__WDERR__SHIFT       2
#define MEMC_PHY_DX1GSR2__WDERR__WIDTH       1
#define MEMC_PHY_DX1GSR2__WDERR__MASK        0x00000004
#define MEMC_PHY_DX1GSR2__WDERR__INV_MASK    0xFFFFFFFB
#define MEMC_PHY_DX1GSR2__WDERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.WDWN - Write Bit Deskew Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the write bit deskew training. */
#define MEMC_PHY_DX1GSR2__WDWN__SHIFT       3
#define MEMC_PHY_DX1GSR2__WDWN__WIDTH       1
#define MEMC_PHY_DX1GSR2__WDWN__MASK        0x00000008
#define MEMC_PHY_DX1GSR2__WDWN__INV_MASK    0xFFFFFFF7
#define MEMC_PHY_DX1GSR2__WDWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.REERR - Read Data Eye Training Error: Indicates, if set, that the DATX8 has encountered an error during execution of the read data eye training. */
#define MEMC_PHY_DX1GSR2__REERR__SHIFT       4
#define MEMC_PHY_DX1GSR2__REERR__WIDTH       1
#define MEMC_PHY_DX1GSR2__REERR__MASK        0x00000010
#define MEMC_PHY_DX1GSR2__REERR__INV_MASK    0xFFFFFFEF
#define MEMC_PHY_DX1GSR2__REERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.REWN - Read Data Eye Training Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the read data eye training. */
#define MEMC_PHY_DX1GSR2__REWN__SHIFT       5
#define MEMC_PHY_DX1GSR2__REWN__WIDTH       1
#define MEMC_PHY_DX1GSR2__REWN__MASK        0x00000020
#define MEMC_PHY_DX1GSR2__REWN__INV_MASK    0xFFFFFFDF
#define MEMC_PHY_DX1GSR2__REWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.WEERR - Write Data Eye Training Error: Indicates, if set, that the DATX8 has encountered an error during execution of the write data eye training. */
#define MEMC_PHY_DX1GSR2__WEERR__SHIFT       6
#define MEMC_PHY_DX1GSR2__WEERR__WIDTH       1
#define MEMC_PHY_DX1GSR2__WEERR__MASK        0x00000040
#define MEMC_PHY_DX1GSR2__WEERR__INV_MASK    0xFFFFFFBF
#define MEMC_PHY_DX1GSR2__WEERR__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.WEWN - Write Data Eye Training Warning: Indicates, if set, that the DATX8 has encountered a warning during execution of the write data eye training. */
#define MEMC_PHY_DX1GSR2__WEWN__SHIFT       7
#define MEMC_PHY_DX1GSR2__WEWN__WIDTH       1
#define MEMC_PHY_DX1GSR2__WEWN__MASK        0x00000080
#define MEMC_PHY_DX1GSR2__WEWN__INV_MASK    0xFFFFFF7F
#define MEMC_PHY_DX1GSR2__WEWN__HW_DEFAULT  0x0

/* MEMC_PHY_DX1GSR2.ESTAT - Error Status: If an error occurred for this lane as indicated by RDERR, WDERR, REERR or WEERR the error status code can provide additional information regard when the error occurred during the algorithm execution. */
#define MEMC_PHY_DX1GSR2__ESTAT__SHIFT       8
#define MEMC_PHY_DX1GSR2__ESTAT__WIDTH       4
#define MEMC_PHY_DX1GSR2__ESTAT__MASK        0x00000F00
#define MEMC_PHY_DX1GSR2__ESTAT__INV_MASK    0xFFFFF0FF
#define MEMC_PHY_DX1GSR2__ESTAT__HW_DEFAULT  0x0

/* DDRPHY_LUT_AC_PD_0 */
/* entry 0 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0 0x10810400

/* MEMC_PHY_DDRPHY_LUT_AC_PD_0.val - value of entry 0 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_0__VAL__HW_DEFAULT  0x0

/* DDRPHY_LUT_AC_PD_1 */
/* entry 1 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1 0x10810404

/* MEMC_PHY_DDRPHY_LUT_AC_PD_1.val - value of entry 1 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_1__VAL__HW_DEFAULT  0x1

/* DDRPHY_LUT_AC_PD_2 */
/* entry 2 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2 0x10810408

/* MEMC_PHY_DDRPHY_LUT_AC_PD_2.val - value of entry 2 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_2__VAL__HW_DEFAULT  0x2

/* DDRPHY_LUT_AC_PD_3 */
/* entry 3 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3 0x1081040C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_3.val - value of entry 3 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_3__VAL__HW_DEFAULT  0x3

/* DDRPHY_LUT_AC_PD_4 */
/* entry 4 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4 0x10810410

/* MEMC_PHY_DDRPHY_LUT_AC_PD_4.val - value of entry 4 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_4__VAL__HW_DEFAULT  0x4

/* DDRPHY_LUT_AC_PD_5 */
/* entry 5 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5 0x10810414

/* MEMC_PHY_DDRPHY_LUT_AC_PD_5.val - value of entry 5 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_5__VAL__HW_DEFAULT  0x5

/* DDRPHY_LUT_AC_PD_6 */
/* entry 6 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6 0x10810418

/* MEMC_PHY_DDRPHY_LUT_AC_PD_6.val - value of entry 6 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_6__VAL__HW_DEFAULT  0x6

/* DDRPHY_LUT_AC_PD_7 */
/* entry 7 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7 0x1081041C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_7.val - value of entry 7 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_7__VAL__HW_DEFAULT  0x7

/* DDRPHY_LUT_AC_PD_8 */
/* entry 8 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8 0x10810420

/* MEMC_PHY_DDRPHY_LUT_AC_PD_8.val - value of entry 8 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_8__VAL__HW_DEFAULT  0x8

/* DDRPHY_LUT_AC_PD_9 */
/* entry 9 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9 0x10810424

/* MEMC_PHY_DDRPHY_LUT_AC_PD_9.val - value of entry 9 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_9__VAL__HW_DEFAULT  0x9

/* DDRPHY_LUT_AC_PD_10 */
/* entry 10 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10 0x10810428

/* MEMC_PHY_DDRPHY_LUT_AC_PD_10.val - value of entry 10 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_10__VAL__HW_DEFAULT  0xA

/* DDRPHY_LUT_AC_PD_11 */
/* entry 11 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11 0x1081042C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_11.val - value of entry 11 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_11__VAL__HW_DEFAULT  0xB

/* DDRPHY_LUT_AC_PD_12 */
/* entry 12 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12 0x10810430

/* MEMC_PHY_DDRPHY_LUT_AC_PD_12.val - value of entry 12 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_12__VAL__HW_DEFAULT  0xC

/* DDRPHY_LUT_AC_PD_13 */
/* entry 13 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13 0x10810434

/* MEMC_PHY_DDRPHY_LUT_AC_PD_13.val - value of entry 13 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_13__VAL__HW_DEFAULT  0xD

/* DDRPHY_LUT_AC_PD_14 */
/* entry 14 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14 0x10810438

/* MEMC_PHY_DDRPHY_LUT_AC_PD_14.val - value of entry 14 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_14__VAL__HW_DEFAULT  0xE

/* DDRPHY_LUT_AC_PD_15 */
/* entry 15 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15 0x1081043C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_15.val - value of entry 15 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_15__VAL__HW_DEFAULT  0xF

/* DDRPHY_LUT_AC_PD_16 */
/* entry 16 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16 0x10810440

/* MEMC_PHY_DDRPHY_LUT_AC_PD_16.val - value of entry 16 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_16__VAL__HW_DEFAULT  0x10

/* DDRPHY_LUT_AC_PD_17 */
/* entry 17 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17 0x10810444

/* MEMC_PHY_DDRPHY_LUT_AC_PD_17.val - value of entry 17 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_17__VAL__HW_DEFAULT  0x11

/* DDRPHY_LUT_AC_PD_18 */
/* entry 18 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18 0x10810448

/* MEMC_PHY_DDRPHY_LUT_AC_PD_18.val - value of entry 18 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_18__VAL__HW_DEFAULT  0x12

/* DDRPHY_LUT_AC_PD_19 */
/* entry 19 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19 0x1081044C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_19.val - value of entry 19 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_19__VAL__HW_DEFAULT  0x13

/* DDRPHY_LUT_AC_PD_20 */
/* entry 20 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20 0x10810450

/* MEMC_PHY_DDRPHY_LUT_AC_PD_20.val - value of entry 20 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_20__VAL__HW_DEFAULT  0x14

/* DDRPHY_LUT_AC_PD_21 */
/* entry 21 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21 0x10810454

/* MEMC_PHY_DDRPHY_LUT_AC_PD_21.val - value of entry 21 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_21__VAL__HW_DEFAULT  0x15

/* DDRPHY_LUT_AC_PD_22 */
/* entry 22 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22 0x10810458

/* MEMC_PHY_DDRPHY_LUT_AC_PD_22.val - value of entry 22 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_22__VAL__HW_DEFAULT  0x16

/* DDRPHY_LUT_AC_PD_23 */
/* entry 23 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23 0x1081045C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_23.val - value of entry 23 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_23__VAL__HW_DEFAULT  0x17

/* DDRPHY_LUT_AC_PD_24 */
/* entry 24 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24 0x10810460

/* MEMC_PHY_DDRPHY_LUT_AC_PD_24.val - value of entry 24 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_24__VAL__HW_DEFAULT  0x18

/* DDRPHY_LUT_AC_PD_25 */
/* entry 25 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25 0x10810464

/* MEMC_PHY_DDRPHY_LUT_AC_PD_25.val - value of entry 25 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_25__VAL__HW_DEFAULT  0x19

/* DDRPHY_LUT_AC_PD_26 */
/* entry 26 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26 0x10810468

/* MEMC_PHY_DDRPHY_LUT_AC_PD_26.val - value of entry 26 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_26__VAL__HW_DEFAULT  0x1A

/* DDRPHY_LUT_AC_PD_27 */
/* entry 27 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27 0x1081046C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_27.val - value of entry 27 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_27__VAL__HW_DEFAULT  0x1B

/* DDRPHY_LUT_AC_PD_28 */
/* entry 28 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28 0x10810470

/* MEMC_PHY_DDRPHY_LUT_AC_PD_28.val - value of entry 28 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_28__VAL__HW_DEFAULT  0x1C

/* DDRPHY_LUT_AC_PD_29 */
/* entry 29 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29 0x10810474

/* MEMC_PHY_DDRPHY_LUT_AC_PD_29.val - value of entry 29 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_29__VAL__HW_DEFAULT  0x1D

/* DDRPHY_LUT_AC_PD_30 */
/* entry 30 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30 0x10810478

/* MEMC_PHY_DDRPHY_LUT_AC_PD_30.val - value of entry 30 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_30__VAL__HW_DEFAULT  0x1E

/* DDRPHY_LUT_AC_PD_31 */
/* entry 31 in LUT of pull-down output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31 0x1081047C

/* MEMC_PHY_DDRPHY_LUT_AC_PD_31.val - value of entry 31 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PD_31__VAL__HW_DEFAULT  0x1F

/* DDRPHY_LUT_AC_PU_0 */
/* entry 0 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0 0x10810480

/* MEMC_PHY_DDRPHY_LUT_AC_PU_0.val - value of entry 0 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_0__VAL__HW_DEFAULT  0x0

/* DDRPHY_LUT_AC_PU_1 */
/* entry 1 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1 0x10810484

/* MEMC_PHY_DDRPHY_LUT_AC_PU_1.val - value of entry 1 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_1__VAL__HW_DEFAULT  0x1

/* DDRPHY_LUT_AC_PU_2 */
/* entry 2 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2 0x10810488

/* MEMC_PHY_DDRPHY_LUT_AC_PU_2.val - value of entry 2 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_2__VAL__HW_DEFAULT  0x2

/* DDRPHY_LUT_AC_PU_3 */
/* entry 3 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3 0x1081048C

/* MEMC_PHY_DDRPHY_LUT_AC_PU_3.val - value of entry 3 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_3__VAL__HW_DEFAULT  0x3

/* DDRPHY_LUT_AC_PU_4 */
/* entry 4 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4 0x10810490

/* MEMC_PHY_DDRPHY_LUT_AC_PU_4.val - value of entry 4 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_4__VAL__HW_DEFAULT  0x4

/* DDRPHY_LUT_AC_PU_5 */
/* entry 5 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5 0x10810494

/* MEMC_PHY_DDRPHY_LUT_AC_PU_5.val - value of entry 5 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_5__VAL__HW_DEFAULT  0x5

/* DDRPHY_LUT_AC_PU_6 */
/* entry 6 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6 0x10810498

/* MEMC_PHY_DDRPHY_LUT_AC_PU_6.val - value of entry 6 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_6__VAL__HW_DEFAULT  0x6

/* DDRPHY_LUT_AC_PU_7 */
/* entry 7 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7 0x1081049C

/* MEMC_PHY_DDRPHY_LUT_AC_PU_7.val - value of entry 7 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_7__VAL__HW_DEFAULT  0x7

/* DDRPHY_LUT_AC_PU_8 */
/* entry 8 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8 0x108104A0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_8.val - value of entry 8 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_8__VAL__HW_DEFAULT  0x8

/* DDRPHY_LUT_AC_PU_9 */
/* entry 9 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9 0x108104A4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_9.val - value of entry 9 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_9__VAL__HW_DEFAULT  0x9

/* DDRPHY_LUT_AC_PU_10 */
/* entry 10 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10 0x108104A8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_10.val - value of entry 10 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_10__VAL__HW_DEFAULT  0xA

/* DDRPHY_LUT_AC_PU_11 */
/* entry 11 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11 0x108104AC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_11.val - value of entry 11 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_11__VAL__HW_DEFAULT  0xB

/* DDRPHY_LUT_AC_PU_12 */
/* entry 12 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12 0x108104B0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_12.val - value of entry 12 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_12__VAL__HW_DEFAULT  0xC

/* DDRPHY_LUT_AC_PU_13 */
/* entry 13 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13 0x108104B4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_13.val - value of entry 13 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_13__VAL__HW_DEFAULT  0xD

/* DDRPHY_LUT_AC_PU_14 */
/* entry 14 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14 0x108104B8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_14.val - value of entry 14 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_14__VAL__HW_DEFAULT  0xE

/* DDRPHY_LUT_AC_PU_15 */
/* entry 15 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15 0x108104BC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_15.val - value of entry 15 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_15__VAL__HW_DEFAULT  0xF

/* DDRPHY_LUT_AC_PU_16 */
/* entry 16 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16 0x108104C0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_16.val - value of entry 16 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_16__VAL__HW_DEFAULT  0x10

/* DDRPHY_LUT_AC_PU_17 */
/* entry 17 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17 0x108104C4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_17.val - value of entry 17 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_17__VAL__HW_DEFAULT  0x11

/* DDRPHY_LUT_AC_PU_18 */
/* entry 18 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18 0x108104C8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_18.val - value of entry 18 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_18__VAL__HW_DEFAULT  0x12

/* DDRPHY_LUT_AC_PU_19 */
/* entry 19 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19 0x108104CC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_19.val - value of entry 19 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_19__VAL__HW_DEFAULT  0x13

/* DDRPHY_LUT_AC_PU_20 */
/* entry 20 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20 0x108104D0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_20.val - value of entry 20 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_20__VAL__HW_DEFAULT  0x14

/* DDRPHY_LUT_AC_PU_21 */
/* entry 21 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21 0x108104D4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_21.val - value of entry 21 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_21__VAL__HW_DEFAULT  0x15

/* DDRPHY_LUT_AC_PU_22 */
/* entry 22 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22 0x108104D8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_22.val - value of entry 22 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_22__VAL__HW_DEFAULT  0x16

/* DDRPHY_LUT_AC_PU_23 */
/* entry 23 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23 0x108104DC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_23.val - value of entry 23 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_23__VAL__HW_DEFAULT  0x17

/* DDRPHY_LUT_AC_PU_24 */
/* entry 24 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24 0x108104E0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_24.val - value of entry 24 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_24__VAL__HW_DEFAULT  0x18

/* DDRPHY_LUT_AC_PU_25 */
/* entry 25 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25 0x108104E4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_25.val - value of entry 25 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_25__VAL__HW_DEFAULT  0x19

/* DDRPHY_LUT_AC_PU_26 */
/* entry 26 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26 0x108104E8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_26.val - value of entry 26 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_26__VAL__HW_DEFAULT  0x1A

/* DDRPHY_LUT_AC_PU_27 */
/* entry 27 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27 0x108104EC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_27.val - value of entry 27 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_27__VAL__HW_DEFAULT  0x1B

/* DDRPHY_LUT_AC_PU_28 */
/* entry 28 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28 0x108104F0

/* MEMC_PHY_DDRPHY_LUT_AC_PU_28.val - value of entry 28 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_28__VAL__HW_DEFAULT  0x1C

/* DDRPHY_LUT_AC_PU_29 */
/* entry 29 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29 0x108104F4

/* MEMC_PHY_DDRPHY_LUT_AC_PU_29.val - value of entry 29 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_29__VAL__HW_DEFAULT  0x1D

/* DDRPHY_LUT_AC_PU_30 */
/* entry 30 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30 0x108104F8

/* MEMC_PHY_DDRPHY_LUT_AC_PU_30.val - value of entry 30 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_30__VAL__HW_DEFAULT  0x1E

/* DDRPHY_LUT_AC_PU_31 */
/* entry 31 in LUT of pull-up output impedance for AC pads (excluding CLK) */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31 0x108104FC

/* MEMC_PHY_DDRPHY_LUT_AC_PU_31.val - value of entry 31 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_AC_PU_31__VAL__HW_DEFAULT  0x1F

/* DDRPHY_LUT_CLK_PD_0 */
/* entry 0 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0 0x10810500

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_0.val - value of entry 0 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_0__VAL__HW_DEFAULT  0x0

/* DDRPHY_LUT_CLK_PD_1 */
/* entry 1 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1 0x10810504

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_1.val - value of entry 1 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_1__VAL__HW_DEFAULT  0x1

/* DDRPHY_LUT_CLK_PD_2 */
/* entry 2 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2 0x10810508

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_2.val - value of entry 2 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_2__VAL__HW_DEFAULT  0x2

/* DDRPHY_LUT_CLK_PD_3 */
/* entry 3 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3 0x1081050C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_3.val - value of entry 3 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_3__VAL__HW_DEFAULT  0x3

/* DDRPHY_LUT_CLK_PD_4 */
/* entry 4 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4 0x10810510

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_4.val - value of entry 4 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_4__VAL__HW_DEFAULT  0x4

/* DDRPHY_LUT_CLK_PD_5 */
/* entry 5 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5 0x10810514

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_5.val - value of entry 5 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_5__VAL__HW_DEFAULT  0x5

/* DDRPHY_LUT_CLK_PD_6 */
/* entry 6 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6 0x10810518

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_6.val - value of entry 6 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_6__VAL__HW_DEFAULT  0x6

/* DDRPHY_LUT_CLK_PD_7 */
/* entry 7 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7 0x1081051C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_7.val - value of entry 7 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_7__VAL__HW_DEFAULT  0x7

/* DDRPHY_LUT_CLK_PD_8 */
/* entry 8 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8 0x10810520

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_8.val - value of entry 8 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_8__VAL__HW_DEFAULT  0x8

/* DDRPHY_LUT_CLK_PD_9 */
/* entry 9 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9 0x10810524

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_9.val - value of entry 9 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_9__VAL__HW_DEFAULT  0x9

/* DDRPHY_LUT_CLK_PD_10 */
/* entry 10 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10 0x10810528

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_10.val - value of entry 10 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_10__VAL__HW_DEFAULT  0xA

/* DDRPHY_LUT_CLK_PD_11 */
/* entry 11 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11 0x1081052C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_11.val - value of entry 11 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_11__VAL__HW_DEFAULT  0xB

/* DDRPHY_LUT_CLK_PD_12 */
/* entry 12 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12 0x10810530

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_12.val - value of entry 12 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_12__VAL__HW_DEFAULT  0xC

/* DDRPHY_LUT_CLK_PD_13 */
/* entry 13 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13 0x10810534

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_13.val - value of entry 13 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_13__VAL__HW_DEFAULT  0xD

/* DDRPHY_LUT_CLK_PD_14 */
/* entry 14 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14 0x10810538

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_14.val - value of entry 14 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_14__VAL__HW_DEFAULT  0xE

/* DDRPHY_LUT_CLK_PD_15 */
/* entry 15 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15 0x1081053C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_15.val - value of entry 15 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_15__VAL__HW_DEFAULT  0xF

/* DDRPHY_LUT_CLK_PD_16 */
/* entry 16 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16 0x10810540

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_16.val - value of entry 16 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_16__VAL__HW_DEFAULT  0x10

/* DDRPHY_LUT_CLK_PD_17 */
/* entry 17 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17 0x10810544

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_17.val - value of entry 17 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_17__VAL__HW_DEFAULT  0x11

/* DDRPHY_LUT_CLK_PD_18 */
/* entry 18 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18 0x10810548

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_18.val - value of entry 18 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_18__VAL__HW_DEFAULT  0x12

/* DDRPHY_LUT_CLK_PD_19 */
/* entry 19 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19 0x1081054C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_19.val - value of entry 19 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_19__VAL__HW_DEFAULT  0x13

/* DDRPHY_LUT_CLK_PD_20 */
/* entry 20 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20 0x10810550

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_20.val - value of entry 20 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_20__VAL__HW_DEFAULT  0x14

/* DDRPHY_LUT_CLK_PD_21 */
/* entry 21 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21 0x10810554

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_21.val - value of entry 21 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_21__VAL__HW_DEFAULT  0x15

/* DDRPHY_LUT_CLK_PD_22 */
/* entry 22 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22 0x10810558

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_22.val - value of entry 22 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_22__VAL__HW_DEFAULT  0x16

/* DDRPHY_LUT_CLK_PD_23 */
/* entry 23 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23 0x1081055C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_23.val - value of entry 23 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_23__VAL__HW_DEFAULT  0x17

/* DDRPHY_LUT_CLK_PD_24 */
/* entry 24 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24 0x10810560

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_24.val - value of entry 24 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_24__VAL__HW_DEFAULT  0x18

/* DDRPHY_LUT_CLK_PD_25 */
/* entry 25 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25 0x10810564

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_25.val - value of entry 25 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_25__VAL__HW_DEFAULT  0x19

/* DDRPHY_LUT_CLK_PD_26 */
/* entry 26 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26 0x10810568

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_26.val - value of entry 26 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_26__VAL__HW_DEFAULT  0x1A

/* DDRPHY_LUT_CLK_PD_27 */
/* entry 27 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27 0x1081056C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_27.val - value of entry 27 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_27__VAL__HW_DEFAULT  0x1B

/* DDRPHY_LUT_CLK_PD_28 */
/* entry 28 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28 0x10810570

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_28.val - value of entry 28 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_28__VAL__HW_DEFAULT  0x1C

/* DDRPHY_LUT_CLK_PD_29 */
/* entry 29 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29 0x10810574

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_29.val - value of entry 29 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_29__VAL__HW_DEFAULT  0x1D

/* DDRPHY_LUT_CLK_PD_30 */
/* entry 30 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30 0x10810578

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_30.val - value of entry 30 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_30__VAL__HW_DEFAULT  0x1E

/* DDRPHY_LUT_CLK_PD_31 */
/* entry 31 in LUT of pull-down output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31 0x1081057C

/* MEMC_PHY_DDRPHY_LUT_CLK_PD_31.val - value of entry 31 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PD_31__VAL__HW_DEFAULT  0x1F

/* DDRPHY_LUT_CLK_PU_0 */
/* entry 0 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0 0x10810580

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_0.val - value of entry 0 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_0__VAL__HW_DEFAULT  0x0

/* DDRPHY_LUT_CLK_PU_1 */
/* entry 1 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1 0x10810584

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_1.val - value of entry 1 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_1__VAL__HW_DEFAULT  0x1

/* DDRPHY_LUT_CLK_PU_2 */
/* entry 2 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2 0x10810588

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_2.val - value of entry 2 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_2__VAL__HW_DEFAULT  0x2

/* DDRPHY_LUT_CLK_PU_3 */
/* entry 3 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3 0x1081058C

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_3.val - value of entry 3 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_3__VAL__HW_DEFAULT  0x3

/* DDRPHY_LUT_CLK_PU_4 */
/* entry 4 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4 0x10810590

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_4.val - value of entry 4 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_4__VAL__HW_DEFAULT  0x4

/* DDRPHY_LUT_CLK_PU_5 */
/* entry 5 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5 0x10810594

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_5.val - value of entry 5 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_5__VAL__HW_DEFAULT  0x5

/* DDRPHY_LUT_CLK_PU_6 */
/* entry 6 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6 0x10810598

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_6.val - value of entry 6 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_6__VAL__HW_DEFAULT  0x6

/* DDRPHY_LUT_CLK_PU_7 */
/* entry 7 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7 0x1081059C

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_7.val - value of entry 7 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_7__VAL__HW_DEFAULT  0x7

/* DDRPHY_LUT_CLK_PU_8 */
/* entry 8 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8 0x108105A0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_8.val - value of entry 8 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_8__VAL__HW_DEFAULT  0x8

/* DDRPHY_LUT_CLK_PU_9 */
/* entry 9 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9 0x108105A4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_9.val - value of entry 9 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_9__VAL__HW_DEFAULT  0x9

/* DDRPHY_LUT_CLK_PU_10 */
/* entry 10 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10 0x108105A8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_10.val - value of entry 10 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_10__VAL__HW_DEFAULT  0xA

/* DDRPHY_LUT_CLK_PU_11 */
/* entry 11 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11 0x108105AC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_11.val - value of entry 11 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_11__VAL__HW_DEFAULT  0xB

/* DDRPHY_LUT_CLK_PU_12 */
/* entry 12 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12 0x108105B0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_12.val - value of entry 12 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_12__VAL__HW_DEFAULT  0xC

/* DDRPHY_LUT_CLK_PU_13 */
/* entry 13 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13 0x108105B4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_13.val - value of entry 13 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_13__VAL__HW_DEFAULT  0xD

/* DDRPHY_LUT_CLK_PU_14 */
/* entry 14 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14 0x108105B8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_14.val - value of entry 14 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_14__VAL__HW_DEFAULT  0xE

/* DDRPHY_LUT_CLK_PU_15 */
/* entry 15 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15 0x108105BC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_15.val - value of entry 15 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_15__VAL__HW_DEFAULT  0xF

/* DDRPHY_LUT_CLK_PU_16 */
/* entry 16 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16 0x108105C0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_16.val - value of entry 16 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_16__VAL__HW_DEFAULT  0x10

/* DDRPHY_LUT_CLK_PU_17 */
/* entry 17 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17 0x108105C4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_17.val - value of entry 17 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_17__VAL__HW_DEFAULT  0x11

/* DDRPHY_LUT_CLK_PU_18 */
/* entry 18 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18 0x108105C8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_18.val - value of entry 18 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_18__VAL__HW_DEFAULT  0x12

/* DDRPHY_LUT_CLK_PU_19 */
/* entry 19 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19 0x108105CC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_19.val - value of entry 19 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_19__VAL__HW_DEFAULT  0x13

/* DDRPHY_LUT_CLK_PU_20 */
/* entry 20 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20 0x108105D0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_20.val - value of entry 20 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_20__VAL__HW_DEFAULT  0x14

/* DDRPHY_LUT_CLK_PU_21 */
/* entry 21 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21 0x108105D4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_21.val - value of entry 21 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_21__VAL__HW_DEFAULT  0x15

/* DDRPHY_LUT_CLK_PU_22 */
/* entry 22 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22 0x108105D8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_22.val - value of entry 22 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_22__VAL__HW_DEFAULT  0x16

/* DDRPHY_LUT_CLK_PU_23 */
/* entry 23 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23 0x108105DC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_23.val - value of entry 23 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_23__VAL__HW_DEFAULT  0x17

/* DDRPHY_LUT_CLK_PU_24 */
/* entry 24 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24 0x108105E0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_24.val - value of entry 24 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_24__VAL__HW_DEFAULT  0x18

/* DDRPHY_LUT_CLK_PU_25 */
/* entry 25 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25 0x108105E4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_25.val - value of entry 25 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_25__VAL__HW_DEFAULT  0x19

/* DDRPHY_LUT_CLK_PU_26 */
/* entry 26 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26 0x108105E8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_26.val - value of entry 26 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_26__VAL__HW_DEFAULT  0x1A

/* DDRPHY_LUT_CLK_PU_27 */
/* entry 27 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27 0x108105EC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_27.val - value of entry 27 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_27__VAL__HW_DEFAULT  0x1B

/* DDRPHY_LUT_CLK_PU_28 */
/* entry 28 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28 0x108105F0

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_28.val - value of entry 28 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_28__VAL__HW_DEFAULT  0x1C

/* DDRPHY_LUT_CLK_PU_29 */
/* entry 29 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29 0x108105F4

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_29.val - value of entry 29 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_29__VAL__HW_DEFAULT  0x1D

/* DDRPHY_LUT_CLK_PU_30 */
/* entry 30 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30 0x108105F8

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_30.val - value of entry 30 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_30__VAL__HW_DEFAULT  0x1E

/* DDRPHY_LUT_CLK_PU_31 */
/* entry 31 in LUT of pull-up output impedance for CLK pads */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31 0x108105FC

/* MEMC_PHY_DDRPHY_LUT_CLK_PU_31.val - value of entry 31 in the LUT */
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31__VAL__SHIFT       0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31__VAL__WIDTH       5
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31__VAL__MASK        0x0000001F
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31__VAL__INV_MASK    0xFFFFFFE0
#define MEMC_PHY_DDRPHY_LUT_CLK_PU_31__VAL__HW_DEFAULT  0x1F

/* ZCTRL of AC after the LUT */
#define MEMC_PHY_ZQ0SR0_AC        0x10810600

/* MEMC_PHY_ZQ0SR0_AC.ZCTRL - Impedance Control: Current value of impedance control of AC after the LUT. ZCTRL field mapping is as follows: ZCTRL[19:15] is used to select the pull-up on-die termination impedance ZCTRL[14:10] is used to select the pull-down on-die termination impedance ZCTRL[9:5] is used to select the pull-up output impedance ZCTRL[4:0] is used to select the pull-down output impedance. */
#define MEMC_PHY_ZQ0SR0_AC__ZCTRL__SHIFT       0
#define MEMC_PHY_ZQ0SR0_AC__ZCTRL__WIDTH       20
#define MEMC_PHY_ZQ0SR0_AC__ZCTRL__MASK        0x000FFFFF
#define MEMC_PHY_ZQ0SR0_AC__ZCTRL__INV_MASK    0xFFF00000
#define MEMC_PHY_ZQ0SR0_AC__ZCTRL__HW_DEFAULT  0x14A

/* ZCTRL of CLK after the LUT */
#define MEMC_PHY_ZQ0SR0_CLK       0x10810604

/* MEMC_PHY_ZQ0SR0_CLK.ZCTRL - Impedance Control: Current value of impedance control of CLK after the LUT. ZCTRL field mapping is as follows: ZCTRL[19:15] is used to select the pull-up on-die termination impedance ZCTRL[14:10] is used to select the pull-down on-die termination impedance ZCTRL[9:5] is used to select the pull-up output impedance ZCTRL[4:0] is used to select the pull-down output impedance. */
#define MEMC_PHY_ZQ0SR0_CLK__ZCTRL__SHIFT       0
#define MEMC_PHY_ZQ0SR0_CLK__ZCTRL__WIDTH       20
#define MEMC_PHY_ZQ0SR0_CLK__ZCTRL__MASK        0x000FFFFF
#define MEMC_PHY_ZQ0SR0_CLK__ZCTRL__INV_MASK    0xFFF00000
#define MEMC_PHY_ZQ0SR0_CLK__ZCTRL__HW_DEFAULT  0x14A


#endif  // __DDRPHY_H__
