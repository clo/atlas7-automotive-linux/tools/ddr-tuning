/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_G2D_H
#define _SOC_G2D_H

#define G2D_FB_BASE_OFFSET              0x00
#define G2D_ENG_STATUS_OFFSET           0x04
#define G2D_ENG_CTRL_OFFSET             0x08
#define G2D_RB_OFFSET_OFFSET            0x0C /* Funny, but this is it. */
#define G2D_RB_LENGTH_OFFSET            0x10
#define G2D_RB_RD_PTR_OFFSET            0x14
#define G2D_RB_WR_PTR_OFFSET            0x18
#define G2D_DST_OFFSET_OFFSET           0x1C
#define G2D_DST_FORMAT_OFFSET           0x20
#define G2D_DST_LT_OFFSET               0x24
#define G2D_DST_RB_OFFSET               0x28
#define G2D_CLIP_LT_OFFSET              0x2C
#define G2D_CLIP_RB_OFFSET              0x30
#define G2D_SRC_OFFSET_OFFSET           0x34
#define G2D_SRC_FORMAT_OFFSET           0x38
#define G2D_SRC_LT_OFFSET               0x3C
#define G2D_SRC_RB_OFFSET               0x40
#define G2D_PAT_OFFSET_OFFSET           0x44
#define G2D_FILL_COLOR_OFFSET           0x48
#define G2D_COLOR_KEY_OFFSET            0x4C
#define G2D_GBL_ALPHA_OFFSET            0x50
#define G2D_DRAW_CTL_OFFSET             0x54
#define G2D_BLT_TIME_OFFSET             0x58
/* 0x5C -- 0x7C are reserved addresses. */
#define G2D_INTERRUPT_ENABLE_OFFSET     0x80
#define G2D_INTERRUPT_CLEAR_OFFSET      0x84
#define G2D_INTERRUPT_STATUS_OFFSET     0x88
#define G2D_HW_RESERVED_OFFSET          0x8C

#define G2D_FB_BASE                     (G2D_FB_BASE_OFFSET          + _G2D_MODULE_BASE)
#define G2D_ENG_STATUS                  (G2D_ENG_STATUS_OFFSET       + _G2D_MODULE_BASE)
#define G2D_ENG_CTRL                    (G2D_ENG_CTRL_OFFSET         + _G2D_MODULE_BASE)
#define G2D_RB_OFFSET                   (G2D_RB_OFFSET_OFFSET        + _G2D_MODULE_BASE)
#define G2D_RB_LENGTH                   (G2D_RB_LENGTH_OFFSET        + _G2D_MODULE_BASE)
#define G2D_RB_RD_PTR                   (G2D_RB_RD_PTR_OFFSET        + _G2D_MODULE_BASE)
#define G2D_RB_WR_PTR                   (G2D_RB_WR_PTR_OFFSET        + _G2D_MODULE_BASE)
#define G2D_DST_OFFSET                  (G2D_DST_OFFSET_OFFSET       + _G2D_MODULE_BASE)
#define G2D_DST_FORMAT                  (G2D_DST_FORMAT_OFFSET       + _G2D_MODULE_BASE)
#define G2D_DST_LT                      (G2D_DST_LT_OFFSET           + _G2D_MODULE_BASE)
#define G2D_DST_RB                      (G2D_DST_RB_OFFSET           + _G2D_MODULE_BASE)
#define G2D_CLIP_LT                     (G2D_CLIP_LT_OFFSET          + _G2D_MODULE_BASE)
#define G2D_CLIP_RB                     (G2D_CLIP_RB_OFFSET          + _G2D_MODULE_BASE)
#define G2D_SRC_OFFSET                  (G2D_SRC_OFFSET_OFFSET       + _G2D_MODULE_BASE)
#define G2D_SRC_FORMAT                  (G2D_SRC_FORMAT_OFFSET       + _G2D_MODULE_BASE)
#define G2D_SRC_LT                      (G2D_SRC_LT_OFFSET           + _G2D_MODULE_BASE)
#define G2D_SRC_RB                      (G2D_SRC_RB_OFFSET           + _G2D_MODULE_BASE)
#define G2D_PAT_OFFSET                  (G2D_PAT_OFFSET_OFFSET       + _G2D_MODULE_BASE)
#define G2D_FILL_COLOR                  (G2D_FILL_COLOR_OFFSET       + _G2D_MODULE_BASE)
#define G2D_COLOR_KEY                   (G2D_COLOR_KEY_OFFSET        + _G2D_MODULE_BASE)
#define G2D_GBL_ALPHA                   (G2D_GBL_ALPHA_OFFSET        + _G2D_MODULE_BASE)
#define G2D_DRAW_CTL                    (G2D_DRAW_CTL_OFFSET         + _G2D_MODULE_BASE)
#define G2D_BLT_TIME                    (G2D_BLT_TIME_OFFSET         + _G2D_MODULE_BASE)
#define G2D_INTERRUPT_ENABLE            (G2D_INTERRUPT_ENABLE_OFFSET + _G2D_MODULE_BASE)
#define G2D_INTERRUPT_CLEAR             (G2D_INTERRUPT_CLEAR_OFFSET  + _G2D_MODULE_BASE)
#define G2D_INTERRUPT_STATUS            (G2D_INTERRUPT_STATUS_OFFSET + _G2D_MODULE_BASE)
#define G2D_HW_RESERVED                 (G2D_HW_RESERVED_OFFSET      + _G2D_MODULE_BASE)

#define MAX_REG_COUNT                   0x40
#define BOUNDARY                        0x18


#endif /* _SOC_G2D_H */
