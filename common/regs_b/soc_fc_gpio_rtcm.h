/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
 
#ifndef _SOC_FC__GPIO_RTCM
#define _SOC_FC__GPIO_RTCM

#define _GPIO_RTCM_MODULE_BASE                   0x18890000

#define rGPIO_RTCM_CFG_0                          (*(volatile unsigned *) 0x18890000U)
#define rGPIO_RTCM_CFG_1                          (*(volatile unsigned *) 0x18890004U)
#define rGPIO_RTCM_CFG_2                          (*(volatile unsigned *) 0x18890008U)
#define rGPIO_RTCM_CFG_3                          (*(volatile unsigned *) 0x1889000CU)
#define rGPIO_RTCM_CFG_4                          (*(volatile unsigned *) 0x18890010U)
#define rGPIO_RTCM_CFG_5                          (*(volatile unsigned *) 0x18890014U)
#define rGPIO_RTCM_CFG_6                          (*(volatile unsigned *) 0x18890018U)
#define rGPIO_RTCM_CFG_7                          (*(volatile unsigned *) 0x1889001CU)
#define rGPIO_RTCM_CFG_8                          (*(volatile unsigned *) 0x18890020U)
#define rGPIO_RTCM_CFG_9                          (*(volatile unsigned *) 0x18890024U)
#define rGPIO_RTCM_CFG_10                         (*(volatile unsigned *) 0x18890028U)
#define rGPIO_RTCM_CFG_11                         (*(volatile unsigned *) 0x1889002CU)
#define rGPIO_RTCM_CFG_12                         (*(volatile unsigned *) 0x18890030U)
#define rGPIO_RTCM_CFG_13                         (*(volatile unsigned *) 0x18890034U)
#define rGPIO_RTCM_STATUS                         (*(volatile unsigned *) 0x1889008CU)
#endif // _SOC_FC__GPIO_RTCM 

