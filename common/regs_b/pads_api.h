/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

   #include "iocrtc.h"
   #include "ioctop.h"

////////////////////////////////////////////////
// au.ac97_bit_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__AC97_BIT_CLK  1
// padopt: 
//   0 = x_i2s_bclk
#define set_funcsel__au__ac97_bit_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_din FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__AC97_DIN  1
// padopt: 
//   0 = x_i2s_din
#define set_funcsel__au__ac97_din(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_dout FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__AC97_DOUT  1
// padopt: 
//   0 = x_i2s_dout0
#define set_funcsel__au__ac97_dout(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_sync FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__AC97_SYNC  1
// padopt: 
//   0 = x_i2s_ws
#define set_funcsel__au__ac97_sync(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.digmic FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__DIGMIC  3
// padopt: 
//   0 = x_df_cs_b_1
//   1 = x_gpio_3
//   2 = x_jtag_tck
#define set_funcsel__au__digmic(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((3 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((5 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((7 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_clk_audio FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_CLK_AUDIO  1
// padopt: 
//   0 = x_usp0_clk
#define set_funcsel__au__func_dbg_clk_audio(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((4 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_cs_audio FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_CS_AUDIO  1
// padopt: 
//   0 = x_usp0_fs
#define set_funcsel__au__func_dbg_cs_audio(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((4 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__au__func_dbg_dconv_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__au__func_dbg_dconv_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__au__func_dbg_dconv_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__au__func_dbg_dconv_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__au__func_dbg_dconv_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__au__func_dbg_dconv_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__au__func_dbg_dconv_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__au__func_dbg_dconv_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((6 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_8  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__au__func_dbg_dconv_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_9  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__au__func_dbg_dconv_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_10  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__au__func_dbg_dconv_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_11  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__au__func_dbg_dconv_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_12  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__au__func_dbg_dconv_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_DCONV_13  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__au__func_dbg_dconv_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_eoc_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_EOC_OUT  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__au__func_dbg_eoc_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_bclk_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_I2S_BCLK_OUT  1
// padopt: 
//   0 = x_i2s_bclk
#define set_funcsel__au__func_dbg_i2s_bclk_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((6 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_fs_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_I2S_FS_OUT  1
// padopt: 
//   0 = x_i2s_ws
#define set_funcsel__au__func_dbg_i2s_fs_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((6 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_rx FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_I2S_RX  1
// padopt: 
//   0 = x_i2s_din
#define set_funcsel__au__func_dbg_i2s_rx(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((6 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_tx_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_I2S_TX_OUT  1
// padopt: 
//   0 = x_i2s_dout0
#define set_funcsel__au__func_dbg_i2s_tx_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((6 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_keycomp_out_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_KEYCOMP_OUT_0  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__au__func_dbg_keycomp_out_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((6 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_keycomp_out_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_KEYCOMP_OUT_1  1
// padopt: 
//   0 = x_df_ry_by
#define set_funcsel__au__func_dbg_keycomp_out_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((6 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_miso_audio FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_MISO_AUDIO  1
// padopt: 
//   0 = x_usp0_tx
#define set_funcsel__au__func_dbg_miso_audio(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((4 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_mosi_audio FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_MOSI_AUDIO  1
// padopt: 
//   0 = x_usp0_rx
#define set_funcsel__au__func_dbg_mosi_audio(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((4 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_soc FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__FUNC_DBG_SOC  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__au__func_dbg_soc(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((6 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_din_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_DIN_1  1
// padopt: 
//   0 = x_i2s_din
#define set_funcsel__au__i2s_din_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout0_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_DOUT0_1  1
// padopt: 
//   0 = x_i2s_dout0
#define set_funcsel__au__i2s_dout0_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout1_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_DOUT1_1  1
// padopt: 
//   0 = x_i2s_dout1
#define set_funcsel__au__i2s_dout1_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout2_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_DOUT2_1  1
// padopt: 
//   0 = x_i2s_dout2
#define set_funcsel__au__i2s_dout2_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_extclk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_EXTCLK_1  1
// padopt: 
//   0 = x_i2s_mclk
#define set_funcsel__au__i2s_extclk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_mclk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_MCLK_1  1
// padopt: 
//   0 = x_i2s_mclk
#define set_funcsel__au__i2s_mclk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_sclk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_SCLK_1  1
// padopt: 
//   0 = x_i2s_bclk
#define set_funcsel__au__i2s_sclk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_ws_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__I2S_WS_1  1
// padopt: 
//   0 = x_i2s_ws
#define set_funcsel__au__i2s_ws_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((1 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.spdif_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__SPDIF_OUT  3
// padopt: 
//   0 = x_i2s_mclk
//   1 = x_i2s_dout1
//   2 = x_usp0_tx
#define set_funcsel__au__spdif_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((3 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((3 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((3 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URFS_0  4
// padopt: 
//   0 = x_i2s_dout2
//   1 = x_uart4_tx
//   2 = x_jtag_trstn
//   3 = x_jtag_tdi
#define set_funcsel__au__urfs_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((5 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((3 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((3 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((6 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URFS_1  3
// padopt: 
//   0 = x_i2s_dout2
//   1 = x_uart4_rx
//   2 = x_jtag_trstn
#define set_funcsel__au__urfs_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((6 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((3 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((4 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URFS_2  3
// padopt: 
//   0 = x_uart4_tx
//   1 = x_jtag_trstn
//   2 = x_sdio5_dat_3
#define set_funcsel__au__urfs_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((4 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((6 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((3 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URXD_0  1
// padopt: 
//   0 = x_usp0_rx
#define set_funcsel__au__urxd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URXD_1  1
// padopt: 
//   0 = x_usp1_rx
#define set_funcsel__au__urxd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__URXD_2  3
// padopt: 
//   0 = x_spi1_din
//   1 = x_rgmii_mdio
//   2 = x_sdio5_dat_0
#define set_funcsel__au__urxd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((2 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((3 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__USCLK_0  1
// padopt: 
//   0 = x_usp0_clk
#define set_funcsel__au__usclk_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__USCLK_1  1
// padopt: 
//   0 = x_usp1_clk
#define set_funcsel__au__usclk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__USCLK_2  3
// padopt: 
//   0 = x_spi1_clk
//   1 = x_rgmii_txclk
//   2 = x_sdio5_clk
#define set_funcsel__au__usclk_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((2 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((3 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTFS_0  1
// padopt: 
//   0 = x_usp0_fs
#define set_funcsel__au__utfs_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTFS_1  1
// padopt: 
//   0 = x_usp1_fs
#define set_funcsel__au__utfs_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTFS_2  3
// padopt: 
//   0 = x_spi1_en
//   1 = x_rgmii_intr_n
//   2 = x_sdio5_dat_1
#define set_funcsel__au__utfs_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((2 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((3 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTXD_0  1
// padopt: 
//   0 = x_usp0_tx
#define set_funcsel__au__utxd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTXD_1  1
// padopt: 
//   0 = x_usp1_tx
#define set_funcsel__au__utxd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((1 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__AU__UTXD_2  3
// padopt: 
//   0 = x_spi1_dout
//   1 = x_rgmii_mdc
//   2 = x_sdio5_cmd
#define set_funcsel__au__utxd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((2 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((3 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C0__CAN_RXD_0_TRNSV0  1
// padopt: 
//   0 = x_can0_rx
#define set_funcsel__c0__can_rxd_0_trnsv0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C0__CAN_RXD_0_TRNSV1  1
// padopt: 
//   0 = x_rtc_gpio_2
#define set_funcsel__c0__can_rxd_0_trnsv1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((5 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_txd_0_trnsv0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C0__CAN_TXD_0_TRNSV0  1
// padopt: 
//   0 = x_can0_tx
#define set_funcsel__c0__can_txd_0_trnsv0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_txd_0_trnsv1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C0__CAN_TXD_0_TRNSV1  1
// padopt: 
//   0 = x_rtc_gpio_3
#define set_funcsel__c0__can_txd_0_trnsv1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((5 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c1.can_rxd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C1__CAN_RXD_1  4
// padopt: 
//   0 = x_uart3_rx
//   1 = x_usp1_rx
//   2 = x_rtc_gpio_2
//   3 = x_jtag_tdi
#define set_funcsel__c1__can_rxd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((2 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((2 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((4 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c1.can_txd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C1__CAN_TXD_1  4
// padopt: 
//   0 = x_uart3_tx
//   1 = x_usp1_tx
//   2 = x_rtc_gpio_3
//   3 = x_jtag_tck
#define set_funcsel__c1__can_txd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((2 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((2 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((4 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_en FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C__CAN_TRNSVR_EN  2
// padopt: 
//   0 = x_rtc_gpio_2
//   1 = x_rtc_gpio_0
#define set_funcsel__c__can_trnsvr_en(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((6 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((2 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_intr FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C__CAN_TRNSVR_INTR  1
// padopt: 
//   0 = x_rtc_gpio_1
#define set_funcsel__c__can_trnsvr_intr(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((2 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_stb_n FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__C__CAN_TRNSVR_STB_N  1
// padopt: 
//   0 = x_rtc_gpio_3
#define set_funcsel__c__can_trnsvr_stb_n(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((6 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_CLK  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__ca__audio_lpc_func_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_csb FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_CSB  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__ca__audio_lpc_func_csb(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_0  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__ca__audio_lpc_func_data_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_1  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__ca__audio_lpc_func_data_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_2  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__ca__audio_lpc_func_data_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_3  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__ca__audio_lpc_func_data_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_4  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__ca__audio_lpc_func_data_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_5  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__ca__audio_lpc_func_data_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((4 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_6  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__ca__audio_lpc_func_data_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((4 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__AUDIO_LPC_FUNC_DATA_7  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__ca__audio_lpc_func_data_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((4 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_CLK  1
// padopt: 
//   0 = x_sdio3_clk
#define set_funcsel__ca__bt_lpc_func_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_csb FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_CSB  1
// padopt: 
//   0 = x_sdio3_cmd
#define set_funcsel__ca__bt_lpc_func_csb(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_DATA_0  1
// padopt: 
//   0 = x_sdio3_dat_0
#define set_funcsel__ca__bt_lpc_func_data_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_DATA_1  1
// padopt: 
//   0 = x_sdio3_dat_1
#define set_funcsel__ca__bt_lpc_func_data_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_DATA_2  1
// padopt: 
//   0 = x_sdio3_dat_2
#define set_funcsel__ca__bt_lpc_func_data_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__BT_LPC_FUNC_DATA_3  1
// padopt: 
//   0 = x_sdio3_dat_3
#define set_funcsel__ca__bt_lpc_func_data_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((5 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__COEX_0  1
// padopt: 
//   0 = x_coex_pio_0
#define set_funcsel__ca__coex_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((1 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__COEX_1  1
// padopt: 
//   0 = x_coex_pio_1
#define set_funcsel__ca__coex_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((1 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__COEX_2  1
// padopt: 
//   0 = x_coex_pio_2
#define set_funcsel__ca__coex_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((1 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__COEX_3  1
// padopt: 
//   0 = x_coex_pio_3
#define set_funcsel__ca__coex_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((1 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__CURATOR_LPC_FUNC_CLK  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__ca__curator_lpc_func_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_csb FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__CURATOR_LPC_FUNC_CSB  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__ca__curator_lpc_func_csb(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_data_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__CURATOR_LPC_FUNC_DATA_0  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__ca__curator_lpc_func_data_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_data_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__CURATOR_LPC_FUNC_DATA_1  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__ca__curator_lpc_func_data_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PCM_DEBUG_CLK  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__ca__pcm_debug_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((5 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_data FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PCM_DEBUG_DATA  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__ca__pcm_debug_data(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((5 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_data_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PCM_DEBUG_DATA_OUT  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__ca__pcm_debug_data_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((5 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_sync FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PCM_DEBUG_SYNC  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__ca__pcm_debug_sync(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((5 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_4  1
// padopt: 
//   0 = x_gpio_2
#define set_funcsel__ca__pio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_5  1
// padopt: 
//   0 = x_gpio_3
#define set_funcsel__ca__pio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_6  1
// padopt: 
//   0 = x_gpio_6
#define set_funcsel__ca__pio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((6 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_7  1
// padopt: 
//   0 = x_gpio_7
#define set_funcsel__ca__pio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((6 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_8  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__ca__pio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_9  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__ca__pio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_10  1
// padopt: 
//   0 = x_df_ale
#define set_funcsel__ca__pio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((5 << SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_11  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__ca__pio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((5 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_12  1
// padopt: 
//   0 = x_df_ry_by
#define set_funcsel__ca__pio_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((5 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_13  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__ca__pio_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_14  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__ca__pio_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__PIO_15  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__ca__pio_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_CLK  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__ca__sdio_debug_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_cmd FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_CMD  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__ca__sdio_debug_cmd(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_DATA_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__ca__sdio_debug_data_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_DATA_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__ca__sdio_debug_data_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_DATA_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__ca__sdio_debug_data_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SDIO_DEBUG_DATA_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__ca__sdio_debug_data_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((5 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SPI_FUNC_CLK  1
// padopt: 
//   0 = x_vip_pxclk
#define set_funcsel__ca__spi_func_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((5 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_csb FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SPI_FUNC_CSB  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__ca__spi_func_csb(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_mosi FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SPI_FUNC_MOSI  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__ca__spi_func_mosi(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_miso FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__SPI_MISO  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__ca__spi_miso(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_RXCLK  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__ca__trb_func_rxclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((4 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_RXDATA_0  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__ca__trb_func_rxdata_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((4 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_RXDATA_1  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__ca__trb_func_rxdata_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((4 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_RXDATA_2  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__ca__trb_func_rxdata_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((4 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_RXDATA_3  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__ca__trb_func_rxdata_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((4 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_TXCLK  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__ca__trb_func_txclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_TXDATA_0  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__ca__trb_func_txdata_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_TXDATA_1  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__ca__trb_func_txdata_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_TXDATA_2  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__ca__trb_func_txdata_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__TRB_FUNC_TXDATA_3  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__ca__trb_func_txdata_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((5 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_cts FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__UART_DEBUG_CTS  1
// padopt: 
//   0 = x_uart1_rx
#define set_funcsel__ca__uart_debug_cts(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((3 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_rts FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__UART_DEBUG_RTS  1
// padopt: 
//   0 = x_uart1_tx
#define set_funcsel__ca__uart_debug_rts(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((3 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_rx FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__UART_DEBUG_RX  1
// padopt: 
//   0 = x_uart0_rx
#define set_funcsel__ca__uart_debug_rx(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((3 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_tx FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CA__UART_DEBUG_TX  1
// padopt: 
//   0 = x_uart0_tx
#define set_funcsel__ca__uart_debug_tx(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((3 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// clkc.trg_ref_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CLKC__TRG_REF_CLK  2
// padopt: 
//   0 = x_trg_shutdown_b_out
//   1 = x_vip_4
#define set_funcsel__clkc__trg_ref_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_CLR, SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_SET, ((2 << SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// clkc.tst_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CLKC__TST_CLK  2
// padopt: 
//   0 = x_df_ale
//   1 = x_l_lck
#define set_funcsel__clkc__tst_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((6 << SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clampdn0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_CLAMPDN0  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__cvbs__cvbsafe_debug_clampdn0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clampup0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_CLAMPUP0  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__cvbs__cvbsafe_debug_clampup0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clkout0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_CLKOUT0  1
// padopt: 
//   0 = x_vip_pxclk
#define set_funcsel__cvbs__cvbsafe_debug_clkout0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((7 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_0  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__cvbs__cvbsafe_debug_data0_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_1  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__cvbs__cvbsafe_debug_data0_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_2  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__cvbs__cvbsafe_debug_data0_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_3  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__cvbs__cvbsafe_debug_data0_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_4  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__cvbs__cvbsafe_debug_data0_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_5  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__cvbs__cvbsafe_debug_data0_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_6  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__cvbs__cvbsafe_debug_data0_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_7  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__cvbs__cvbsafe_debug_data0_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((7 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_8  1
// padopt: 
//   0 = x_vip_hsync
#define set_funcsel__cvbs__cvbsafe_debug_data0_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((7 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA0_9  1
// padopt: 
//   0 = x_vip_vsync
#define set_funcsel__cvbs__cvbsafe_debug_data0_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((7 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data_valid0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_DATA_VALID0  1
// padopt: 
//   0 = x_lcd_gpio_20
#define set_funcsel__cvbs__cvbsafe_debug_data_valid0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((3 << SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_ext_clk26 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_EXT_CLK26  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__cvbs__cvbsafe_debug_ext_clk26(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((3 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((3 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((3 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((3 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TEST_MUX_OUT_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__cvbs__cvbsafe_debug_test_mux_out_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((3 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_testclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__CVBS__CVBSAFE_DEBUG_TESTCLK  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__cvbs__cvbsafe_debug_testclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((3 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_cts_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_CTS_B  1
// padopt: 
//   0 = x_uart1_rx
#define set_funcsel__gn__gnss_cts_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((4 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_eclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_ECLK  1
// padopt: 
//   0 = x_i2s_bclk
#define set_funcsel__gn__gnss_eclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_poff FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_FORCE_POFF  1
// padopt: 
//   0 = x_gpio_4
#define set_funcsel__gn__gnss_force_poff(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_poff_ack FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_FORCE_POFF_ACK  1
// padopt: 
//   0 = x_gpio_5
#define set_funcsel__gn__gnss_force_poff_ack(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_pon FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_FORCE_PON  1
// padopt: 
//   0 = x_gpio_2
#define set_funcsel__gn__gnss_force_pon(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_pon_ack FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_FORCE_PON_ACK  1
// padopt: 
//   0 = x_gpio_3
#define set_funcsel__gn__gnss_force_pon_ack(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_irq1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_IRQ1  1
// padopt: 
//   0 = x_i2s_mclk
#define set_funcsel__gn__gnss_irq1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_irq2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_IRQ2  1
// padopt: 
//   0 = x_i2s_din
#define set_funcsel__gn__gnss_irq2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_m0_porst_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_M0_PORST_B  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__gn__gnss_m0_porst_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_poff_req FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_POFF_REQ  1
// padopt: 
//   0 = x_gpio_6
#define set_funcsel__gn__gnss_poff_req(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_pon_req FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_PON_REQ  1
// padopt: 
//   0 = x_gpio_1
#define set_funcsel__gn__gnss_pon_req(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((7 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_rst_32k_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_RST_32K_B  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__gn__gnss_rst_32k_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_rts_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_RTS_B  1
// padopt: 
//   0 = x_uart1_tx
#define set_funcsel__gn__gnss_rts_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((4 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_scl FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SCL  1
// padopt: 
//   0 = x_scl_0
#define set_funcsel__gn__gnss_scl(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((2 << SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sda FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SDA  1
// padopt: 
//   0 = x_sda_0
#define set_funcsel__gn__gnss_sda(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((2 << SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__gn__gnss_sw_status_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__gn__gnss_sw_status_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__gn__gnss_sw_status_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__gn__gnss_sw_status_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__gn__gnss_sw_status_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__gn__gnss_sw_status_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__gn__gnss_sw_status_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__gn__gnss_sw_status_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__gn__gnss_sw_status_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__gn__gnss_sw_status_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__gn__gnss_sw_status_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__gn__gnss_sw_status_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((7 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__gn__gnss_sw_status_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((7 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__gn__gnss_sw_status_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((7 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__gn__gnss_sw_status_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((7 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SW_STATUS_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__gn__gnss_sw_status_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((7 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_swclktck FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SWCLKTCK  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__gn__gnss_swclktck(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_swdiotms FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_SWDIOTMS  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__gn__gnss_swdiotms(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((7 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_tm FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_TM  1
// padopt: 
//   0 = x_i2s_dout0
#define set_funcsel__gn__gnss_tm(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_tsync FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_TSYNC  1
// padopt: 
//   0 = x_i2s_ws
#define set_funcsel__gn__gnss_tsync(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_uart_rx FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_UART_RX  1
// padopt: 
//   0 = x_uart0_rx
#define set_funcsel__gn__gnss_uart_rx(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((4 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_uart_tx FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__GNSS_UART_TX  1
// padopt: 
//   0 = x_uart0_tx
#define set_funcsel__gn__gnss_uart_tx(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((4 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__gn__io_gnsssys_sw_cfg_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__gn__io_gnsssys_sw_cfg_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__gn__io_gnsssys_sw_cfg_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__gn__io_gnsssys_sw_cfg_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__gn__io_gnsssys_sw_cfg_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__gn__io_gnsssys_sw_cfg_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__gn__io_gnsssys_sw_cfg_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__gn__io_gnsssys_sw_cfg_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((7 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_8  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__gn__io_gnsssys_sw_cfg_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((7 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_9  1
// padopt: 
//   0 = x_df_ry_by
#define set_funcsel__gn__io_gnsssys_sw_cfg_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((7 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_10  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__gn__io_gnsssys_sw_cfg_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_11  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__gn__io_gnsssys_sw_cfg_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_12  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__gn__io_gnsssys_sw_cfg_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_13  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__gn__io_gnsssys_sw_cfg_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_14  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__gn__io_gnsssys_sw_cfg_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__IO_GNSSSYS_SW_CFG_15  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__gn__io_gnsssys_sw_cfg_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((7 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_ACQ_CLK  2
// padopt: 
//   0 = x_trg_acq_clk
//   1 = x_vip_3
#define set_funcsel__gn__trg_acq_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_d0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_ACQ_D0  2
// padopt: 
//   0 = x_trg_acq_d0
//   1 = x_vip_2
#define set_funcsel__gn__trg_acq_d0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_d1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_ACQ_D1  2
// padopt: 
//   0 = x_trg_acq_d1
//   1 = x_vip_0
#define set_funcsel__gn__trg_acq_d1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_irq_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_IRQ_B  2
// padopt: 
//   0 = x_trg_irq_b
//   1 = x_vip_1
#define set_funcsel__gn__trg_irq_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_shutdown_b_out FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_SHUTDOWN_B_OUT  4
// padopt: 
//   0 = x_trg_shutdown_b_out
//   1 = x_vip_hsync
//   2 = x_i2s_dout2
//   3 = x_gpio_4
#define set_funcsel__gn__trg_shutdown_b_out(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_CLR, SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_SET, ((1 << SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((3 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((4 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((5 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_SPI_CLK  2
// padopt: 
//   0 = x_trg_spi_clk
//   1 = x_vip_pxclk
#define set_funcsel__gn__trg_spi_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((3 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_cs_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_SPI_CS_B  2
// padopt: 
//   0 = x_trg_spi_cs_b
//   1 = x_vip_5
#define set_funcsel__gn__trg_spi_cs_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_di FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_SPI_DI  2
// padopt: 
//   0 = x_trg_spi_di
//   1 = x_vip_6
#define set_funcsel__gn__trg_spi_di(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_do FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GN__TRG_SPI_DO  2
// padopt: 
//   0 = x_trg_spi_do
//   1 = x_vip_7
#define set_funcsel__gn__trg_spi_do(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((1 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((3 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.scl_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2__SCL_0  1
// padopt: 
//   0 = x_scl_0
#define set_funcsel__i2__scl_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((1 << SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.scl_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2__SCL_1  1
// padopt: 
//   0 = x_gpio_7
#define set_funcsel__i2__scl_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.sda_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2__SDA_0  1
// padopt: 
//   0 = x_sda_0
#define set_funcsel__i2__sda_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((1 << SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.sda_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2__SDA_1  1
// padopt: 
//   0 = x_gpio_6
#define set_funcsel__i2__sda_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_bs FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S0__HS_I2S0_BS  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__i2s0__hs_i2s0_bs(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S0__HS_I2S0_RXD0  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__i2s0__hs_i2s0_rxd0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S0__HS_I2S0_RXD1  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__i2s0__hs_i2s0_rxd1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_ws FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S0__HS_I2S0_WS  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__i2s0__hs_i2s0_ws(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_bs FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S1__HS_I2S1_BS  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__i2s1__hs_i2s1_bs(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S1__HS_I2S1_RXD0  5
// padopt: 
//   0 = x_ldd_4
//   1 = x_coex_pio_2
//   2 = x_coex_pio_0
//   3 = x_i2s_dout2
//   4 = x_vip_hsync
#define set_funcsel__i2s1__hs_i2s1_rxd0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((4 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((4 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((2 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((7 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 4) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((4 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S1__HS_I2S1_RXD1  5
// padopt: 
//   0 = x_ldd_15
//   1 = x_coex_pio_3
//   2 = x_coex_pio_1
//   3 = x_i2s_din
//   4 = x_vip_vsync
#define set_funcsel__i2s1__hs_i2s1_rxd1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((4 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((4 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((2 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((7 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : ((padopt) == 4) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((4 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_ws FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__I2S1__HS_I2S1_WS  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__i2s1__hs_i2s1_ws(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((2 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.jt_dbg_nsrst FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__JT_DBG_NSRST  1
// padopt: 
//   0 = x_gpio_6
#define set_funcsel__jtag__jt_dbg_nsrst(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((5 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.ntrst FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__NTRST  2
// padopt: 
//   0 = x_low_bat_ind_b
//   1 = x_jtag_trstn
#define set_funcsel__jtag__ntrst(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((3 << SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((1 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.swdiotms FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__SWDIOTMS  2
// padopt: 
//   0 = x_rtc_gpio_2
//   1 = x_jtag_tms
#define set_funcsel__jtag__swdiotms(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((3 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((1 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tck FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__TCK  2
// padopt: 
//   0 = x_rtc_gpio_0
//   1 = x_jtag_tck
#define set_funcsel__jtag__tck(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((3 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((1 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tdi FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__TDI  2
// padopt: 
//   0 = x_rtc_gpio_1
//   1 = x_jtag_tdi
#define set_funcsel__jtag__tdi(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((3 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((1 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tdo FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__JTAG__TDO  2
// padopt: 
//   0 = x_rtc_gpio_3
//   1 = x_jtag_tdo
#define set_funcsel__jtag__tdo(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((3 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((1 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__KS__KAS_SPI_CLK  1
// padopt: 
//   0 = x_usp0_clk
#define set_funcsel__ks__kas_spi_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_cs_n FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__KS__KAS_SPI_CS_N  1
// padopt: 
//   0 = x_usp0_fs
#define set_funcsel__ks__kas_spi_cs_n(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_di FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__KS__KAS_SPI_DI  1
// padopt: 
//   0 = x_usp0_rx
#define set_funcsel__ks__kas_spi_di(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_do FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__KS__KAS_SPI_DO  1
// padopt: 
//   0 = x_usp0_tx
#define set_funcsel__ks__kas_spi_do(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((2 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_de FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__L_DE  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__ld__l_de(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_fck FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__L_FCK  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__ld__l_fck(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_lck FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__L_LCK  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__ld__l_lck(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_pclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__L_PCLK  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__ld__l_pclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__ld__ldd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__ld__ldd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__ld__ldd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__ld__ldd_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((1 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__ld__ldd_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__ld__ldd_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__ld__ldd_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__ld__ldd_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__ld__ldd_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__ld__ldd_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__ld__ldd_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__ld__ldd_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((1 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__ld__ldd_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((1 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__ld__ldd_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((1 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__ld__ldd_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((1 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__ld__ldd_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((1 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_16 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_16  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__ld__ldd_16(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_17 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_17  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__ld__ldd_17(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_18 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_18  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__ld__ldd_18(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_19 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_19  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__ld__ldd_19(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_20 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_20  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__ld__ldd_20(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_21 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_21  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__ld__ldd_21(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_22 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_22  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__ld__ldd_22(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_23 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LD__LDD_23  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__ld__ldd_23(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((2 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fa_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FA_1  1
// padopt: 
//   0 = x_lcd_gpio_20
#define set_funcsel__lr__lcdrom_fa_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((2 << SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fce_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FCE_B  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__lr__lcdrom_fce_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__lr__lcdrom_fd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__lr__lcdrom_fd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__lr__lcdrom_fd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__lr__lcdrom_fd_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__lr__lcdrom_fd_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__lr__lcdrom_fd_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__lr__lcdrom_fd_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__lr__lcdrom_fd_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__lr__lcdrom_fd_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__lr__lcdrom_fd_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__lr__lcdrom_fd_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__lr__lcdrom_fd_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((2 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__lr__lcdrom_fd_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((2 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__lr__lcdrom_fd_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((2 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__lr__lcdrom_fd_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((2 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FD_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__lr__lcdrom_fd_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((2 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_foe_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FOE_B  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__lr__lcdrom_foe_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_frdy FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FRDY  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__lr__lcdrom_frdy(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fwe_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__LR__LCDROM_FWE_B  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__lr__lcdrom_fwe_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((2 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__nd__df_ad_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__nd__df_ad_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__nd__df_ad_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__nd__df_ad_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__nd__df_ad_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__nd__df_ad_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__nd__df_ad_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_AD_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__nd__df_ad_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((1 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ale FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_ALE  1
// padopt: 
//   0 = x_df_ale
#define set_funcsel__nd__df_ale(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cle FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_CLE  1
// padopt: 
//   0 = x_df_cle
#define set_funcsel__nd__df_cle(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cs_b_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_CS_B_0  1
// padopt: 
//   0 = x_df_cs_b_0
#define set_funcsel__nd__df_cs_b_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cs_b_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_CS_B_1  1
// padopt: 
//   0 = x_df_cs_b_1
#define set_funcsel__nd__df_cs_b_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_dqs FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_DQS  1
// padopt: 
//   0 = x_df_dqs
#define set_funcsel__nd__df_dqs(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_re_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_RE_B  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__nd__df_re_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ry_by FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_RY_BY  1
// padopt: 
//   0 = x_df_ry_by
#define set_funcsel__nd__df_ry_by(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_we_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_WE_B  1
// padopt: 
//   0 = x_df_we_b
#define set_funcsel__nd__df_we_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((1 << SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_wp_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__ND__DF_WP_B  1
// padopt: 
//   0 = x_gpio_5
#define set_funcsel__nd__df_wp_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.dr_dir FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PS__DR_DIR  1
// padopt: 
//   0 = x_gpio_1
#define set_funcsel__ps__dr_dir(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.odo_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PS__ODO_0  1
// padopt: 
//   0 = x_gpio_0
#define set_funcsel__ps__odo_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.odo_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PS__ODO_1  1
// padopt: 
//   0 = x_gpio_2
#define set_funcsel__ps__odo_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((5 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.cko_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__CKO_0  4
// padopt: 
//   0 = x_gpio_4
//   1 = x_rgmii_txclk
//   2 = x_vip_pxclk
//   3 = x_jtag_tdi
#define set_funcsel__pw__cko_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((4 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((2 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((5 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.cko_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__CKO_1  3
// padopt: 
//   0 = x_gpio_5
//   1 = x_rgmii_mdc
//   2 = x_jtag_trstn
#define set_funcsel__pw__cko_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((4 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((5 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.i2s01_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__I2S01_CLK  3
// padopt: 
//   0 = x_gpio_6
//   1 = x_i2s_dout2
//   2 = x_coex_pio_3
#define set_funcsel__pw__i2s01_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((3 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((2 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__PWM0  2
// padopt: 
//   0 = x_gpio_0
//   1 = x_jtag_tdo
#define set_funcsel__pw__pwm0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((5 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__PWM1  3
// padopt: 
//   0 = x_gpio_1
//   1 = x_jtag_tms
//   2 = x_coex_pio_2
#define set_funcsel__pw__pwm1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((5 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((2 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__PWM2  3
// padopt: 
//   0 = x_gpio_2
//   1 = x_rgmii_txd_1
//   2 = x_jtag_tck
#define set_funcsel__pw__pwm2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((3 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((5 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PW__PWM3  2
// padopt: 
//   0 = x_gpio_3
//   1 = x_lcd_gpio_20
#define set_funcsel__pw__pwm3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((3 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((4 << SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.core_on FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__CORE_ON  1
// padopt: 
//   0 = x_core_on
#define set_funcsel__pwc__core_on(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CORE_ON__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CORE_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.ext_on FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__EXT_ON  1
// padopt: 
//   0 = x_ext_on
#define set_funcsel__pwc__ext_on(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__EXT_ON__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__EXT_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.gpio3_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__GPIO3_CLK  1
// padopt: 
//   0 = x_rtc_gpio_3
#define set_funcsel__pwc__gpio3_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((4 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.io_on FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__IO_ON  1
// padopt: 
//   0 = x_io_on
#define set_funcsel__pwc__io_on(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.lowbatt_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__LOWBATT_B  1
// padopt: 
//   0 = x_low_bat_ind_b
#define set_funcsel__pwc__lowbatt_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.mem_on FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__MEM_ON  1
// padopt: 
//   0 = x_mem_on
#define set_funcsel__pwc__mem_on(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__MEM_ON__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__MEM_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.on_key_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__ON_KEY_B  1
// padopt: 
//   0 = x_on_key_b
#define set_funcsel__pwc__on_key_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__ON_KEY_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__ON_KEY_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__WAKEUP_SRC_0  1
// padopt: 
//   0 = x_rtc_gpio_0
#define set_funcsel__pwc__wakeup_src_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__WAKEUP_SRC_1  1
// padopt: 
//   0 = x_rtc_gpio_1
#define set_funcsel__pwc__wakeup_src_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__WAKEUP_SRC_2  1
// padopt: 
//   0 = x_rtc_gpio_2
#define set_funcsel__pwc__wakeup_src_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__PWC__WAKEUP_SRC_3  1
// padopt: 
//   0 = x_rtc_gpio_3
#define set_funcsel__pwc__wakeup_src_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((1 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC0  1
// padopt: 
//   0 = x_rgmii_rxc_ctl
#define set_funcsel__rg__eth_mac0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC1  1
// padopt: 
//   0 = x_rgmii_rxd_0
#define set_funcsel__rg__eth_mac1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC2  1
// padopt: 
//   0 = x_rgmii_rxd_1
#define set_funcsel__rg__eth_mac2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC3  1
// padopt: 
//   0 = x_rgmii_rxd_2
#define set_funcsel__rg__eth_mac3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC4  1
// padopt: 
//   0 = x_rgmii_rxd_3
#define set_funcsel__rg__eth_mac4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC5  1
// padopt: 
//   0 = x_rgmii_rx_clk
#define set_funcsel__rg__eth_mac5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC6  1
// padopt: 
//   0 = x_rgmii_tx_ctl
#define set_funcsel__rg__eth_mac6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC7  1
// padopt: 
//   0 = x_rgmii_txd_0
#define set_funcsel__rg__eth_mac7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC8  1
// padopt: 
//   0 = x_rgmii_txd_1
#define set_funcsel__rg__eth_mac8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC9  1
// padopt: 
//   0 = x_rgmii_txd_2
#define set_funcsel__rg__eth_mac9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC10  1
// padopt: 
//   0 = x_rgmii_txd_3
#define set_funcsel__rg__eth_mac10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__ETH_MAC11  1
// padopt: 
//   0 = x_rgmii_txclk
#define set_funcsel__rg__eth_mac11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((1 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.gmac_phy_intr_n FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__GMAC_PHY_INTR_N  1
// padopt: 
//   0 = x_rgmii_intr_n
#define set_funcsel__rg__gmac_phy_intr_n(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_mac_md FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__RGMII_MAC_MD  1
// padopt: 
//   0 = x_rgmii_mdio
#define set_funcsel__rg__rgmii_mac_md(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_mac_mdc FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__RGMII_MAC_MDC  1
// padopt: 
//   0 = x_rgmii_mdc
#define set_funcsel__rg__rgmii_mac_mdc(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((1 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_phy_ref_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__RG__RGMII_PHY_REF_CLK  2
// padopt: 
//   0 = x_rgmii_intr_n
//   1 = x_l_pclk
#define set_funcsel__rg__rgmii_phy_ref_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((5 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((4 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_clk_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_CLK_0  1
// padopt: 
//   0 = x_df_cle
#define set_funcsel__sd0__sd_clk_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((2 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_cmd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_CMD_0  1
// padopt: 
//   0 = x_df_ale
#define set_funcsel__sd0__sd_cmd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((2 << SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__sd0__sd_dat_0_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__sd0__sd_dat_0_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__sd0__sd_dat_0_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__sd0__sd_dat_0_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__sd0__sd_dat_0_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__sd0__sd_dat_0_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__sd0__sd_dat_0_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD0__SD_DAT_0_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__sd0__sd_dat_0_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((2 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_clk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_CLK_1  1
// padopt: 
//   0 = x_df_we_b
#define set_funcsel__sd1__sd_clk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((3 << SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_cmd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_CMD_1  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__sd1__sd_cmd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((3 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_0  2
// padopt: 
//   0 = x_df_ad_0
//   1 = x_df_ad_4
#define set_funcsel__sd1__sd_dat_1_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((4 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_1  2
// padopt: 
//   0 = x_df_ad_1
//   1 = x_df_ad_5
#define set_funcsel__sd1__sd_dat_1_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((4 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_2  2
// padopt: 
//   0 = x_df_ad_2
//   1 = x_df_ad_6
#define set_funcsel__sd1__sd_dat_1_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((4 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_3  2
// padopt: 
//   0 = x_df_ad_3
//   1 = x_df_ad_7
#define set_funcsel__sd1__sd_dat_1_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((4 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__sd1__sd_dat_1_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__sd1__sd_dat_1_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__sd1__sd_dat_1_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD1__SD_DAT_1_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__sd1__sd_dat_1_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((3 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_cd_b_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_CD_B_2  2
// padopt: 
//   0 = x_gpio_5
//   1 = x_jtag_tck
#define set_funcsel__sd2__sd_cd_b_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((6 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_clk_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_CLK_2  1
// padopt: 
//   0 = x_sdio2_clk
#define set_funcsel__sd2__sd_clk_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_cmd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_CMD_2  1
// padopt: 
//   0 = x_sdio2_cmd
#define set_funcsel__sd2__sd_cmd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_DAT_2_0  1
// padopt: 
//   0 = x_sdio2_dat_0
#define set_funcsel__sd2__sd_dat_2_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_DAT_2_1  1
// padopt: 
//   0 = x_sdio2_dat_1
#define set_funcsel__sd2__sd_dat_2_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_DAT_2_2  1
// padopt: 
//   0 = x_sdio2_dat_2
#define set_funcsel__sd2__sd_dat_2_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_DAT_2_3  1
// padopt: 
//   0 = x_sdio2_dat_3
#define set_funcsel__sd2__sd_dat_2_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((1 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_wp_b_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD2__SD_WP_B_2  2
// padopt: 
//   0 = x_gpio_4
//   1 = x_jtag_trstn
#define set_funcsel__sd2__sd_wp_b_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((7 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_clk_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_CLK_3  1
// padopt: 
//   0 = x_sdio3_clk
#define set_funcsel__sd3__sd_clk_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_cmd_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_CMD_3  1
// padopt: 
//   0 = x_sdio3_cmd
#define set_funcsel__sd3__sd_cmd_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_DAT_3_0  1
// padopt: 
//   0 = x_sdio3_dat_0
#define set_funcsel__sd3__sd_dat_3_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_DAT_3_1  1
// padopt: 
//   0 = x_sdio3_dat_1
#define set_funcsel__sd3__sd_dat_3_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_DAT_3_2  1
// padopt: 
//   0 = x_sdio3_dat_2
#define set_funcsel__sd3__sd_dat_3_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD3__SD_DAT_3_3  1
// padopt: 
//   0 = x_sdio3_dat_3
#define set_funcsel__sd3__sd_dat_3_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((1 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_clk_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_CLK_5  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__sd5__sd_clk_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_cmd_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_CMD_5  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__sd5__sd_cmd_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_DAT_5_0  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__sd5__sd_dat_5_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_DAT_5_1  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__sd5__sd_dat_5_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_DAT_5_2  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__sd5__sd_dat_5_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD5__SD_DAT_5_3  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__sd5__sd_dat_5_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((1 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_clk_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_CLK_6  2
// padopt: 
//   0 = x_vip_5
//   1 = x_rgmii_txclk
#define set_funcsel__sd6__sd_clk_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((3 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_cmd_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_CMD_6  2
// padopt: 
//   0 = x_vip_4
//   1 = x_rgmii_txd_2
#define set_funcsel__sd6__sd_cmd_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((3 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_DAT_6_0  2
// padopt: 
//   0 = x_vip_0
//   1 = x_rgmii_txd_3
#define set_funcsel__sd6__sd_dat_6_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((3 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_DAT_6_1  2
// padopt: 
//   0 = x_vip_1
//   1 = x_rgmii_mdc
#define set_funcsel__sd6__sd_dat_6_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((3 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_DAT_6_2  2
// padopt: 
//   0 = x_vip_2
//   1 = x_rgmii_mdio
#define set_funcsel__sd6__sd_dat_6_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((3 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SD6__SD_DAT_6_3  2
// padopt: 
//   0 = x_vip_3
//   1 = x_rgmii_intr_n
#define set_funcsel__sd6__sd_dat_6_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((4 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((3 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.ext_ldo_on FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__EXT_LDO_ON  1
// padopt: 
//   0 = x_low_bat_ind_b
#define set_funcsel__sp0__ext_ldo_on(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((2 << SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_clk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_CLK  1
// padopt: 
//   0 = x_spi0_clk
#define set_funcsel__sp0__qspi_clk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_cs_b FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_CS_B  1
// padopt: 
//   0 = x_spi0_cs_b
#define set_funcsel__sp0__qspi_cs_b(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_DATA_0  1
// padopt: 
//   0 = x_spi0_io_0
#define set_funcsel__sp0__qspi_data_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_DATA_1  1
// padopt: 
//   0 = x_spi0_io_1
#define set_funcsel__sp0__qspi_data_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((1 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_DATA_2  1
// padopt: 
//   0 = x_spi0_io_2
#define set_funcsel__sp0__qspi_data_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_CLR, SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_SET, ((1 << SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__SHIFT) & SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP0__QSPI_DATA_3  1
// padopt: 
//   0 = x_spi0_io_3
#define set_funcsel__sp0__qspi_data_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_CLR, SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_SET, ((1 << SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__SHIFT) & SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_clk_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP1__SPI_CLK_1  1
// padopt: 
//   0 = x_spi1_clk
#define set_funcsel__sp1__spi_clk_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((1 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_din_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP1__SPI_DIN_1  1
// padopt: 
//   0 = x_spi1_din
#define set_funcsel__sp1__spi_din_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((1 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_dout_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP1__SPI_DOUT_1  1
// padopt: 
//   0 = x_spi1_dout
#define set_funcsel__sp1__spi_dout_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((1 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_en_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__SP1__SPI_EN_1  1
// padopt: 
//   0 = x_spi1_en
#define set_funcsel__sp1__spi_en_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((1 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.traceclk FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACECLK  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__tpiu__traceclk(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracectl FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACECTL  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__tpiu__tracectl(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__tpiu__tracedata_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__tpiu__tracedata_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__tpiu__tracedata_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__tpiu__tracedata_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((5 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__tpiu__tracedata_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__tpiu__tracedata_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__tpiu__tracedata_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__tpiu__tracedata_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__tpiu__tracedata_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__tpiu__tracedata_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__tpiu__tracedata_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__tpiu__tracedata_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((5 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__tpiu__tracedata_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((5 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__tpiu__tracedata_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((5 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__tpiu__tracedata_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((5 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__TPIU__TRACEDATA_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__tpiu__tracedata_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((5 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.cts_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U0__CTS_0  1
// padopt: 
//   0 = x_gpio_2
#define set_funcsel__u0__cts_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.rts_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U0__RTS_0  1
// padopt: 
//   0 = x_gpio_1
#define set_funcsel__u0__rts_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.rxd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U0__RXD_0  1
// padopt: 
//   0 = x_uart0_rx
#define set_funcsel__u0__rxd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.txd_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U0__TXD_0  1
// padopt: 
//   0 = x_uart0_tx
#define set_funcsel__u0__txd_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u1.rxd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U1__RXD_1  1
// padopt: 
//   0 = x_uart1_rx
#define set_funcsel__u1__rxd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u1.txd_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U1__TXD_1  1
// padopt: 
//   0 = x_uart1_tx
#define set_funcsel__u1__txd_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.cts_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U2__CTS_2  2
// padopt: 
//   0 = x_coex_pio_3
//   1 = x_jtag_tdi
#define set_funcsel__u2__cts_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((3 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((2 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.rts_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U2__RTS_2  2
// padopt: 
//   0 = x_coex_pio_2
//   1 = x_jtag_tck
#define set_funcsel__u2__rts_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((3 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((2 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.rxd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U2__RXD_2  3
// padopt: 
//   0 = x_can0_rx
//   1 = x_jtag_tms
//   2 = x_coex_pio_1
#define set_funcsel__u2__rxd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((2 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((2 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((3 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.txd_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U2__TXD_2  3
// padopt: 
//   0 = x_can0_tx
//   1 = x_jtag_tdo
//   2 = x_coex_pio_0
#define set_funcsel__u2__txd_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((2 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((2 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((3 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.cts_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U3__CTS_3  3
// padopt: 
//   0 = x_gpio_6
//   1 = x_rgmii_intr_n
//   2 = x_uart4_rx
#define set_funcsel__u3__cts_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((4 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((2 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.rts_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U3__RTS_3  3
// padopt: 
//   0 = x_gpio_7
//   1 = x_rgmii_mdio
//   2 = x_uart4_tx
#define set_funcsel__u3__rts_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((2 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((4 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((2 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.rxd_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U3__RXD_3  3
// padopt: 
//   0 = x_uart3_rx
//   1 = x_vip_vsync
//   2 = x_jtag_tdi
#define set_funcsel__u3__rxd_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((2 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((3 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.txd_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U3__TXD_3  3
// padopt: 
//   0 = x_uart3_tx
//   1 = x_vip_hsync
//   2 = x_jtag_tck
#define set_funcsel__u3__txd_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((1 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((2 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((3 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.cts_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U4__CTS_4  3
// padopt: 
//   0 = x_gpio_3
//   1 = x_rgmii_txd_3
//   2 = x_i2s_dout2
#define set_funcsel__u4__cts_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((4 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.rts_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U4__RTS_4  3
// padopt: 
//   0 = x_gpio_4
//   1 = x_rgmii_txd_2
//   2 = x_i2s_dout1
#define set_funcsel__u4__rts_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((4 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((4 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((2 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.rxd_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U4__RXD_4  1
// padopt: 
//   0 = x_uart4_rx
#define set_funcsel__u4__rxd_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((1 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.txd_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__U4__TXD_4  1
// padopt: 
//   0 = x_uart4_tx
#define set_funcsel__u4__txd_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((1 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// usb0.drvvbus FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__USB0__DRVVBUS  2
// padopt: 
//   0 = x_df_cs_b_1
//   1 = x_jtag_tdi
#define set_funcsel__usb0__drvvbus(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((2 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((7 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// usb1.drvvbus FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__USB1__DRVVBUS  2
// padopt: 
//   0 = x_uart0_rx
//   1 = x_jtag_trstn
#define set_funcsel__usb1__drvvbus(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((2 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((2 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_0  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__vi__vip1_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_1  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__vi__vip1_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_2  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__vi__vip1_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_3  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__vi__vip1_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_4  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__vi__vip1_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_5  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__vi__vip1_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_6  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__vi__vip1_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_7  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__vi__vip1_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((1 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_8  1
// padopt: 
//   0 = x_vip_pxclk
#define set_funcsel__vi__vip1_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((1 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_9  1
// padopt: 
//   0 = x_vip_hsync
#define set_funcsel__vi__vip1_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((1 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_10  1
// padopt: 
//   0 = x_vip_vsync
#define set_funcsel__vi__vip1_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((1 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_11  1
// padopt: 
//   0 = x_rgmii_rxc_ctl
#define set_funcsel__vi__vip1_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_12  1
// padopt: 
//   0 = x_rgmii_rxd_0
#define set_funcsel__vi__vip1_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_13  1
// padopt: 
//   0 = x_rgmii_rxd_1
#define set_funcsel__vi__vip1_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_14  1
// padopt: 
//   0 = x_rgmii_rxd_2
#define set_funcsel__vi__vip1_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_15  1
// padopt: 
//   0 = x_rgmii_rxd_3
#define set_funcsel__vi__vip1_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_16 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_16  1
// padopt: 
//   0 = x_rgmii_rx_clk
#define set_funcsel__vi__vip1_16(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((2 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_17 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_17  1
// padopt: 
//   0 = x_rgmii_tx_ctl
#define set_funcsel__vi__vip1_17(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_18 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_18  1
// padopt: 
//   0 = x_rgmii_txd_0
#define set_funcsel__vi__vip1_18(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_19 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_19  1
// padopt: 
//   0 = x_rgmii_txd_1
#define set_funcsel__vi__vip1_19(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_20 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_20  1
// padopt: 
//   0 = x_rgmii_txd_2
#define set_funcsel__vi__vip1_20(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_21 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VI__VIP1_21  1
// padopt: 
//   0 = x_rgmii_txd_3
#define set_funcsel__vi__vip1_21(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((2 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_0  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__visbus__dout_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_1  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__visbus__dout_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_2  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__visbus__dout_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_3  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__visbus__dout_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_4  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__visbus__dout_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_5  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__visbus__dout_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_6  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__visbus__dout_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_7  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__visbus__dout_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_8  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__visbus__dout_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_9  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__visbus__dout_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_10  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__visbus__dout_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_11  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__visbus__dout_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((6 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_12  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__visbus__dout_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((6 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_13  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__visbus__dout_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((6 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_14  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__visbus__dout_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((6 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_15  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__visbus__dout_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((6 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_16 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_16  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__visbus__dout_16(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_17 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_17  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__visbus__dout_17(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_18 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_18  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__visbus__dout_18(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_19 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_19  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__visbus__dout_19(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((6 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_20 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_20  1
// padopt: 
//   0 = x_sdio3_clk
#define set_funcsel__visbus__dout_20(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_21 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_21  1
// padopt: 
//   0 = x_sdio3_cmd
#define set_funcsel__visbus__dout_21(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_22 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_22  1
// padopt: 
//   0 = x_sdio3_dat_0
#define set_funcsel__visbus__dout_22(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_23 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_23  1
// padopt: 
//   0 = x_sdio3_dat_1
#define set_funcsel__visbus__dout_23(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_24 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_24  1
// padopt: 
//   0 = x_sdio3_dat_2
#define set_funcsel__visbus__dout_24(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_25 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_25  1
// padopt: 
//   0 = x_sdio3_dat_3
#define set_funcsel__visbus__dout_25(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((6 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_26 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_26  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__visbus__dout_26(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_27 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_27  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__visbus__dout_27(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_28 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_28  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__visbus__dout_28(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_29 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_29  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__visbus__dout_29(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_30 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_30  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__visbus__dout_30(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_31 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__VISBUS__DOUT_31  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__visbus__dout_31(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((6 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_0  1
// padopt: 
//   0 = x_rtc_gpio_0
#define set_funcsel__gp__rtc_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((0 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_1  1
// padopt: 
//   0 = x_rtc_gpio_1
#define set_funcsel__gp__rtc_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((0 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_2  1
// padopt: 
//   0 = x_rtc_gpio_2
#define set_funcsel__gp__rtc_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((0 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_3  1
// padopt: 
//   0 = x_rtc_gpio_3
#define set_funcsel__gp__rtc_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((0 << SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_4  1
// padopt: 
//   0 = x_low_bat_ind_b
#define set_funcsel__gp__rtc_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_CLR, SW_RTC_FUNC_SEL_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_0_REG_SET, ((0 << SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_FUNC_SEL_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_13  1
// padopt: 
//   0 = x_io_on
#define set_funcsel__gp__rtc_gpio_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__IO_ON__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__IO_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_5  1
// padopt: 
//   0 = x_can0_tx
#define set_funcsel__gp__rtc_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_6  1
// padopt: 
//   0 = x_can0_rx
#define set_funcsel__gp__rtc_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_7  1
// padopt: 
//   0 = x_spi0_clk
#define set_funcsel__gp__rtc_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CLK__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_8  1
// padopt: 
//   0 = x_spi0_cs_b
#define set_funcsel__gp__rtc_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_CS_B__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_9  1
// padopt: 
//   0 = x_spi0_io_0
#define set_funcsel__gp__rtc_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_0__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_10  1
// padopt: 
//   0 = x_spi0_io_1
#define set_funcsel__gp__rtc_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_CLR, SW_RTC_FUNC_SEL_1_REG_CLR__SPI0_IO_1__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_1_REG_SET, ((0 << SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__SHIFT) & SW_RTC_FUNC_SEL_1_REG_SET__SPI0_IO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_11  1
// padopt: 
//   0 = x_spi0_io_2
#define set_funcsel__gp__rtc_gpio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_CLR, SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_2__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_SET, ((0 << SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__SHIFT) & SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RTC_GPIO_12  1
// padopt: 
//   0 = x_spi0_io_3
#define set_funcsel__gp__rtc_gpio_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_CLR, SW_RTC_FUNC_SEL_2_REG_CLR__SPI0_IO_3__MASK), \
                         SET_IO_REG(SW_RTC_FUNC_SEL_2_REG_SET, ((0 << SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__SHIFT) & SW_RTC_FUNC_SEL_2_REG_SET__SPI0_IO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_0  1
// padopt: 
//   0 = x_spi1_en
#define set_funcsel__gp__sp_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((0 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_1  1
// padopt: 
//   0 = x_spi1_clk
#define set_funcsel__gp__sp_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((0 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_2  1
// padopt: 
//   0 = x_spi1_din
#define set_funcsel__gp__sp_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((0 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_3  1
// padopt: 
//   0 = x_spi1_dout
#define set_funcsel__gp__sp_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_CLR, SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_0_REG_SET, ((0 << SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_4  1
// padopt: 
//   0 = x_usp0_clk
#define set_funcsel__gp__sp_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_5  1
// padopt: 
//   0 = x_usp0_tx
#define set_funcsel__gp__sp_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_6  1
// padopt: 
//   0 = x_usp0_rx
#define set_funcsel__gp__sp_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_7  1
// padopt: 
//   0 = x_usp0_fs
#define set_funcsel__gp__sp_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_8  1
// padopt: 
//   0 = x_usp1_clk
#define set_funcsel__gp__sp_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_9  1
// padopt: 
//   0 = x_usp1_tx
#define set_funcsel__gp__sp_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_10  1
// padopt: 
//   0 = x_usp1_rx
#define set_funcsel__gp__sp_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SP_GPIO_11  1
// padopt: 
//   0 = x_usp1_fs
#define set_funcsel__gp__sp_gpio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_CLR, SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_21_REG_SET, ((0 << SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__SHIFT) & SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_0  1
// padopt: 
//   0 = x_trg_spi_clk
#define set_funcsel__gp__gnss_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_1  1
// padopt: 
//   0 = x_trg_spi_di
#define set_funcsel__gp__gnss_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_2  1
// padopt: 
//   0 = x_trg_spi_do
#define set_funcsel__gp__gnss_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_3  1
// padopt: 
//   0 = x_trg_spi_cs_b
#define set_funcsel__gp__gnss_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_4  1
// padopt: 
//   0 = x_trg_acq_d1
#define set_funcsel__gp__gnss_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_5  1
// padopt: 
//   0 = x_trg_irq_b
#define set_funcsel__gp__gnss_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_6  1
// padopt: 
//   0 = x_trg_acq_d0
#define set_funcsel__gp__gnss_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_7  1
// padopt: 
//   0 = x_trg_acq_clk
#define set_funcsel__gp__gnss_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_CLR, SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_1_REG_SET, ((0 << SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__SHIFT) & SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GNSS_GPIO_8  1
// padopt: 
//   0 = x_trg_shutdown_b_out
#define set_funcsel__gp__gnss_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_CLR, SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_2_REG_SET, ((0 << SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_0  1
// padopt: 
//   0 = x_sdio2_clk
#define set_funcsel__gp__sdio_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_1  1
// padopt: 
//   0 = x_sdio2_cmd
#define set_funcsel__gp__sdio_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_2  1
// padopt: 
//   0 = x_sdio2_dat_0
#define set_funcsel__gp__sdio_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_3  1
// padopt: 
//   0 = x_sdio2_dat_1
#define set_funcsel__gp__sdio_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_4  1
// padopt: 
//   0 = x_sdio2_dat_2
#define set_funcsel__gp__sdio_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO_GPIO_5  1
// padopt: 
//   0 = x_sdio2_dat_3
#define set_funcsel__gp__sdio_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_CLR, SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_3_REG_SET, ((0 << SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_0  1
// padopt: 
//   0 = x_df_ad_0
#define set_funcsel__gp__nand_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_1  1
// padopt: 
//   0 = x_df_ad_1
#define set_funcsel__gp__nand_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_2  1
// padopt: 
//   0 = x_df_ad_2
#define set_funcsel__gp__nand_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_3  1
// padopt: 
//   0 = x_df_ad_3
#define set_funcsel__gp__nand_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_4  1
// padopt: 
//   0 = x_df_ad_4
#define set_funcsel__gp__nand_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_5  1
// padopt: 
//   0 = x_df_ad_5
#define set_funcsel__gp__nand_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_6  1
// padopt: 
//   0 = x_df_ad_6
#define set_funcsel__gp__nand_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_7  1
// padopt: 
//   0 = x_df_ad_7
#define set_funcsel__gp__nand_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_CLR, SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_4_REG_SET, ((0 << SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT) & SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_8  1
// padopt: 
//   0 = x_df_cle
#define set_funcsel__gp__nand_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_9  1
// padopt: 
//   0 = x_df_ale
#define set_funcsel__gp__nand_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_10  1
// padopt: 
//   0 = x_df_we_b
#define set_funcsel__gp__nand_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_11  1
// padopt: 
//   0 = x_df_re_b
#define set_funcsel__gp__nand_gpio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_12  1
// padopt: 
//   0 = x_df_ry_by
#define set_funcsel__gp__nand_gpio_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_13  1
// padopt: 
//   0 = x_df_cs_b_0
#define set_funcsel__gp__nand_gpio_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_14  1
// padopt: 
//   0 = x_df_cs_b_1
#define set_funcsel__gp__nand_gpio_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__NAND_GPIO_15  1
// padopt: 
//   0 = x_df_dqs
#define set_funcsel__gp__nand_gpio_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_CLR, SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_5_REG_SET, ((0 << SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__SHIFT) & SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_4  1
// padopt: 
//   0 = x_ldd_0
#define set_funcsel__gp__lcd_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_5  1
// padopt: 
//   0 = x_ldd_1
#define set_funcsel__gp__lcd_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_6  1
// padopt: 
//   0 = x_ldd_2
#define set_funcsel__gp__lcd_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_7  1
// padopt: 
//   0 = x_ldd_3
#define set_funcsel__gp__lcd_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_8  1
// padopt: 
//   0 = x_ldd_4
#define set_funcsel__gp__lcd_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_9  1
// padopt: 
//   0 = x_ldd_5
#define set_funcsel__gp__lcd_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_10  1
// padopt: 
//   0 = x_ldd_6
#define set_funcsel__gp__lcd_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_11  1
// padopt: 
//   0 = x_ldd_7
#define set_funcsel__gp__lcd_gpio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_12  1
// padopt: 
//   0 = x_ldd_8
#define set_funcsel__gp__lcd_gpio_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_13  1
// padopt: 
//   0 = x_ldd_9
#define set_funcsel__gp__lcd_gpio_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_14  1
// padopt: 
//   0 = x_ldd_10
#define set_funcsel__gp__lcd_gpio_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_15 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_15  1
// padopt: 
//   0 = x_ldd_11
#define set_funcsel__gp__lcd_gpio_15(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_CLR, SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_7_REG_SET, ((0 << SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT) & SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_16 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_16  1
// padopt: 
//   0 = x_ldd_12
#define set_funcsel__gp__lcd_gpio_16(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((0 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_17 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_17  1
// padopt: 
//   0 = x_ldd_13
#define set_funcsel__gp__lcd_gpio_17(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((0 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_18 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_18  1
// padopt: 
//   0 = x_ldd_14
#define set_funcsel__gp__lcd_gpio_18(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((0 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_19 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_19  1
// padopt: 
//   0 = x_ldd_15
#define set_funcsel__gp__lcd_gpio_19(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((0 << SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_0  1
// padopt: 
//   0 = x_l_pclk
#define set_funcsel__gp__lcd_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_1  1
// padopt: 
//   0 = x_l_lck
#define set_funcsel__gp__lcd_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_2  1
// padopt: 
//   0 = x_l_fck
#define set_funcsel__gp__lcd_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_3  1
// padopt: 
//   0 = x_l_de
#define set_funcsel__gp__lcd_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_CLR, SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_6_REG_SET, ((0 << SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT) & SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_20 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LCD_GPIO_20  1
// padopt: 
//   0 = x_lcd_gpio_20
#define set_funcsel__gp__lcd_gpio_20(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_CLR, SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_8_REG_SET, ((0 << SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_0  1
// padopt: 
//   0 = x_vip_0
#define set_funcsel__gp__vip_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_1  1
// padopt: 
//   0 = x_vip_1
#define set_funcsel__gp__vip_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_2  1
// padopt: 
//   0 = x_vip_2
#define set_funcsel__gp__vip_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_3  1
// padopt: 
//   0 = x_vip_3
#define set_funcsel__gp__vip_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_4  1
// padopt: 
//   0 = x_vip_4
#define set_funcsel__gp__vip_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_5  1
// padopt: 
//   0 = x_vip_5
#define set_funcsel__gp__vip_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_6  1
// padopt: 
//   0 = x_vip_6
#define set_funcsel__gp__vip_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_7  1
// padopt: 
//   0 = x_vip_7
#define set_funcsel__gp__vip_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_CLR, SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_9_REG_SET, ((0 << SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT) & SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_8  1
// padopt: 
//   0 = x_vip_pxclk
#define set_funcsel__gp__vip_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((0 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_9  1
// padopt: 
//   0 = x_vip_hsync
#define set_funcsel__gp__vip_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((0 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__VIP_GPIO_10  1
// padopt: 
//   0 = x_vip_vsync
#define set_funcsel__gp__vip_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_CLR, SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_10_REG_SET, ((0 << SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_0  1
// padopt: 
//   0 = x_sdio3_clk
#define set_funcsel__gp__sdio3_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_1  1
// padopt: 
//   0 = x_sdio3_cmd
#define set_funcsel__gp__sdio3_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_2  1
// padopt: 
//   0 = x_sdio3_dat_0
#define set_funcsel__gp__sdio3_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_3  1
// padopt: 
//   0 = x_sdio3_dat_1
#define set_funcsel__gp__sdio3_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_4  1
// padopt: 
//   0 = x_sdio3_dat_2
#define set_funcsel__gp__sdio3_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_5  1
// padopt: 
//   0 = x_sdio3_dat_3
#define set_funcsel__gp__sdio3_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_CLR, SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_11_REG_SET, ((0 << SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_6  1
// padopt: 
//   0 = x_coex_pio_0
#define set_funcsel__gp__sdio3_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((0 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_7  1
// padopt: 
//   0 = x_coex_pio_1
#define set_funcsel__gp__sdio3_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((0 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_8  1
// padopt: 
//   0 = x_coex_pio_2
#define set_funcsel__gp__sdio3_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((0 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO3_GPIO_9  1
// padopt: 
//   0 = x_coex_pio_3
#define set_funcsel__gp__sdio3_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_CLR, SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_18_REG_SET, ((0 << SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_0  1
// padopt: 
//   0 = x_sdio5_clk
#define set_funcsel__gp__sdio5_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_1  1
// padopt: 
//   0 = x_sdio5_cmd
#define set_funcsel__gp__sdio5_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_2  1
// padopt: 
//   0 = x_sdio5_dat_0
#define set_funcsel__gp__sdio5_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_3  1
// padopt: 
//   0 = x_sdio5_dat_1
#define set_funcsel__gp__sdio5_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_4  1
// padopt: 
//   0 = x_sdio5_dat_2
#define set_funcsel__gp__sdio5_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__SDIO5_GPIO_5  1
// padopt: 
//   0 = x_sdio5_dat_3
#define set_funcsel__gp__sdio5_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_CLR, SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_12_REG_SET, ((0 << SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_0  1
// padopt: 
//   0 = x_rgmii_txd_0
#define set_funcsel__gp__rgmii_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_1  1
// padopt: 
//   0 = x_rgmii_txd_1
#define set_funcsel__gp__rgmii_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_2  1
// padopt: 
//   0 = x_rgmii_txd_2
#define set_funcsel__gp__rgmii_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_3  1
// padopt: 
//   0 = x_rgmii_txd_3
#define set_funcsel__gp__rgmii_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_4  1
// padopt: 
//   0 = x_rgmii_txclk
#define set_funcsel__gp__rgmii_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_5  1
// padopt: 
//   0 = x_rgmii_tx_ctl
#define set_funcsel__gp__rgmii_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_6  1
// padopt: 
//   0 = x_rgmii_rxd_0
#define set_funcsel__gp__rgmii_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_7  1
// padopt: 
//   0 = x_rgmii_rxd_1
#define set_funcsel__gp__rgmii_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_CLR, SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_13_REG_SET, ((0 << SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_8  1
// padopt: 
//   0 = x_rgmii_rxd_2
#define set_funcsel__gp__rgmii_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_9  1
// padopt: 
//   0 = x_rgmii_rxd_3
#define set_funcsel__gp__rgmii_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_10 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_10  1
// padopt: 
//   0 = x_rgmii_rx_clk
#define set_funcsel__gp__rgmii_gpio_10(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_11 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_11  1
// padopt: 
//   0 = x_rgmii_rxc_ctl
#define set_funcsel__gp__rgmii_gpio_11(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_12 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_12  1
// padopt: 
//   0 = x_rgmii_mdio
#define set_funcsel__gp__rgmii_gpio_12(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_13 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_13  1
// padopt: 
//   0 = x_rgmii_mdc
#define set_funcsel__gp__rgmii_gpio_13(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_14 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__RGMII_GPIO_14  1
// padopt: 
//   0 = x_rgmii_intr_n
#define set_funcsel__gp__rgmii_gpio_14(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_CLR, SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_14_REG_SET, ((0 << SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_0  1
// padopt: 
//   0 = x_i2s_mclk
#define set_funcsel__gp__i2s_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_1  1
// padopt: 
//   0 = x_i2s_bclk
#define set_funcsel__gp__i2s_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_2  1
// padopt: 
//   0 = x_i2s_ws
#define set_funcsel__gp__i2s_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_3  1
// padopt: 
//   0 = x_i2s_dout0
#define set_funcsel__gp__i2s_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_4  1
// padopt: 
//   0 = x_i2s_dout1
#define set_funcsel__gp__i2s_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_5  1
// padopt: 
//   0 = x_i2s_dout2
#define set_funcsel__gp__i2s_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__I2S_GPIO_6  1
// padopt: 
//   0 = x_i2s_din
#define set_funcsel__gp__i2s_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_CLR, SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_15_REG_SET, ((0 << SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT) & SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_0  1
// padopt: 
//   0 = x_gpio_0
#define set_funcsel__gp__gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_1  1
// padopt: 
//   0 = x_gpio_1
#define set_funcsel__gp__gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_2  1
// padopt: 
//   0 = x_gpio_2
#define set_funcsel__gp__gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_3  1
// padopt: 
//   0 = x_gpio_3
#define set_funcsel__gp__gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_4  1
// padopt: 
//   0 = x_gpio_4
#define set_funcsel__gp__gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_5  1
// padopt: 
//   0 = x_gpio_5
#define set_funcsel__gp__gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_6  1
// padopt: 
//   0 = x_gpio_6
#define set_funcsel__gp__gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_7  1
// padopt: 
//   0 = x_gpio_7
#define set_funcsel__gp__gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_8  1
// padopt: 
//   0 = x_sda_0
#define set_funcsel__gp__gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((0 << SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__GPIO_9  1
// padopt: 
//   0 = x_scl_0
#define set_funcsel__gp__gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_CLR, SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_17_REG_SET, ((0 << SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__SHIFT) & SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_0  1
// padopt: 
//   0 = x_uart0_tx
#define set_funcsel__gp__uart_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_1  1
// padopt: 
//   0 = x_uart0_rx
#define set_funcsel__gp__uart_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_2  1
// padopt: 
//   0 = x_uart1_tx
#define set_funcsel__gp__uart_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_3  1
// padopt: 
//   0 = x_uart1_rx
#define set_funcsel__gp__uart_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_4  1
// padopt: 
//   0 = x_uart3_tx
#define set_funcsel__gp__uart_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_5  1
// padopt: 
//   0 = x_uart3_rx
#define set_funcsel__gp__uart_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_CLR, SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_19_REG_SET, ((0 << SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__SHIFT) & SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_6  1
// padopt: 
//   0 = x_uart4_tx
#define set_funcsel__gp__uart_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((0 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__UART_GPIO_7  1
// padopt: 
//   0 = x_uart4_rx
#define set_funcsel__gp__uart_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_CLR, SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_20_REG_SET, ((0 << SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__SHIFT) & SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__JTAG_GPIO_0  1
// padopt: 
//   0 = x_jtag_tdo
#define set_funcsel__gp__jtag_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((0 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__JTAG_GPIO_1  1
// padopt: 
//   0 = x_jtag_tms
#define set_funcsel__gp__jtag_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((0 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__JTAG_GPIO_2  1
// padopt: 
//   0 = x_jtag_tck
#define set_funcsel__gp__jtag_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((0 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__JTAG_GPIO_3  1
// padopt: 
//   0 = x_jtag_tdi
#define set_funcsel__gp__jtag_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((0 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__JTAG_GPIO_4  1
// padopt: 
//   0 = x_jtag_trstn
#define set_funcsel__gp__jtag_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_CLR, SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_24_REG_SET, ((0 << SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_0 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_0  1
// padopt: 
//   0 = x_lvds_tx0d0p
#define set_funcsel__gp__lvds_gpio_0(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_23_REG_CLR, SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_23_REG_SET, ((0 << SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__SHIFT) & SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_1 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_1  1
// padopt: 
//   0 = x_lvds_tx0d0n
#define set_funcsel__gp__lvds_gpio_1(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_23_REG_CLR, SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_23_REG_SET, ((0 << SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__SHIFT) & SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_2 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_2  1
// padopt: 
//   0 = x_lvds_tx0d1p
#define set_funcsel__gp__lvds_gpio_2(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_3 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_3  1
// padopt: 
//   0 = x_lvds_tx0d1n
#define set_funcsel__gp__lvds_gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_4 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_4  1
// padopt: 
//   0 = x_lvds_tx0d2p
#define set_funcsel__gp__lvds_gpio_4(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_5 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_5  1
// padopt: 
//   0 = x_lvds_tx0d2n
#define set_funcsel__gp__lvds_gpio_5(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_6 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_6  1
// padopt: 
//   0 = x_lvds_tx0d3p
#define set_funcsel__gp__lvds_gpio_6(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_7 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_7  1
// padopt: 
//   0 = x_lvds_tx0d3n
#define set_funcsel__gp__lvds_gpio_7(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_8 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_8  1
// padopt: 
//   0 = x_lvds_tx0d4p
#define set_funcsel__gp__lvds_gpio_8(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_9 FuncSel
////////////////////////////////////////////////
#define  PADOPT_COUNT__GP__LVDS_GPIO_9  1
// padopt: 
//   0 = x_lvds_tx0d4n
#define set_funcsel__gp__lvds_gpio_9(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_CLR, SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_22_REG_SET, ((0 << SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__SHIFT) & SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_bit_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_bclk  {sw_top_pull_en_10[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__ac97_bit_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_din PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__ac97_din(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_dout PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout0  {sw_top_pull_en_10[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__ac97_dout(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.ac97_sync PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_ws  {sw_top_pull_en_10[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__ac97_sync(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.digmic PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_1  {sw_top_pull_en_3[29:28] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__digmic(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_clk_audio PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_clk  {sw_top_pull_en_14[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_clk_audio(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_cs_audio PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_fs  {sw_top_pull_en_14[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_cs_audio(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_dconv_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_dconv_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_dconv_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_eoc_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_eoc_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_bclk_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_bclk  {sw_top_pull_en_10[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_i2s_bclk_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_fs_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_ws  {sw_top_pull_en_10[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_i2s_fs_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_rx PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_i2s_rx(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_i2s_tx_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout0  {sw_top_pull_en_10[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_i2s_tx_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_keycomp_out_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_keycomp_out_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_keycomp_out_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ry_by  {sw_top_pull_en_3[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__func_dbg_keycomp_out_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_miso_audio PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_tx  {sw_top_pull_en_14[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_miso_audio(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_mosi_audio PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_rx  {sw_top_pull_en_14[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__func_dbg_mosi_audio(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.func_dbg_soc PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__au__func_dbg_soc(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_din_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_din_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout0_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout0  {sw_top_pull_en_10[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_dout0_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout1_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout1  {sw_top_pull_en_10[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_dout1_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_dout2_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_dout2_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_extclk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_mclk  {sw_top_pull_en_10[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_extclk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_mclk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_mclk  {sw_top_pull_en_10[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_mclk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_sclk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_bclk  {sw_top_pull_en_10[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_sclk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.i2s_ws_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_ws  {sw_top_pull_en_10[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__i2s_ws_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.spdif_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_mclk  {sw_top_pull_en_10[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_i2s_dout1  {sw_top_pull_en_10[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_usp0_tx  {sw_top_pull_en_14[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__spdif_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_uart4_tx  {sw_top_pull_en_13[16] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   2 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   3 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__au__urfs_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_uart4_rx  {sw_top_pull_en_13[18] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   2 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__au__urfs_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_RX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urfs_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart4_tx  {sw_top_pull_en_13[16] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   2 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__urfs_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_rx  {sw_top_pull_en_14[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__urxd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_rx  {sw_top_pull_en_14[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__urxd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.urxd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_din  {sw_top_pull_en_0[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_mdio  {sw_top_pull_en_9[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   2 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__urxd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_clk  {sw_top_pull_en_14[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__usclk_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_clk  {sw_top_pull_en_14[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__usclk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.usclk_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_clk  {sw_top_pull_en_0[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_txclk  {sw_top_pull_en_9[9:8] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
//   2 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__au__usclk_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_fs  {sw_top_pull_en_14[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__utfs_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_fs  {sw_top_pull_en_14[14] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__utfs_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utfs_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_en  {sw_top_pull_en_0[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   2 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__utfs_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_tx  {sw_top_pull_en_14[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__utxd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_tx  {sw_top_pull_en_14[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__au__utxd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// au.utxd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_dout  {sw_top_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_mdc  {sw_top_pull_en_9[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__au__utxd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_rx  {sw_rtc_pull_en_0[22] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__c0__can_rxd_0_trnsv0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__c0__can_rxd_0_trnsv1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_txd_0_trnsv0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_tx  {sw_rtc_pull_en_0[20] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__c0__can_txd_0_trnsv0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c0.can_txd_0_trnsv1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__c0__can_txd_0_trnsv1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c1.can_rxd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_rx  {sw_top_pull_en_13[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_usp1_rx  {sw_top_pull_en_14[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
//   3 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__c1__can_rxd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_RX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c1.can_txd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_tx  {sw_top_pull_en_13[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_usp1_tx  {sw_top_pull_en_14[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   3 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__c1__can_txd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_TX__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_en PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
//   1 = x_rtc_gpio_0  {sw_rtc_pull_en_0[0] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__c__can_trnsvr_en(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_intr PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_1  {sw_rtc_pull_en_0[2] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__c__can_trnsvr_intr(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// c.can_trnsvr_stb_n PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__c__can_trnsvr_stb_n(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_csb PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_csb(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.audio_lpc_func_data_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__audio_lpc_func_data_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_clk  {sw_top_pull_en_7[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__ca__bt_lpc_func_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_csb PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_cmd  {sw_top_pull_en_7[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__bt_lpc_func_csb(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_0  {sw_top_pull_en_7[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__bt_lpc_func_data_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_1  {sw_top_pull_en_7[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__bt_lpc_func_data_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_2  {sw_top_pull_en_7[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__bt_lpc_func_data_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.bt_lpc_func_data_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_3  {sw_top_pull_en_7[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__bt_lpc_func_data_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_0  {sw_top_pull_en_12[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__coex_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_1  {sw_top_pull_en_12[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__coex_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_2  {sw_top_pull_en_12[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__coex_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.coex_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_3  {sw_top_pull_en_12[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__coex_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__curator_lpc_func_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_csb PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__curator_lpc_func_csb(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_data_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__curator_lpc_func_data_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.curator_lpc_func_data_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__curator_lpc_func_data_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__ca__pcm_debug_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_data PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pcm_debug_data(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_data_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pcm_debug_data_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pcm_debug_sync PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pcm_debug_sync(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ca__pio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ca__pio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__pio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_7  {sw_top_pull_en_11[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__pio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_7__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ale  {sw_top_pull_en_3[21:20] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ry_by  {sw_top_pull_en_3[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__pio_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__pio_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__pio_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.pio_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__pio_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_cmd PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_cmd(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_data_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_data_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_data_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.sdio_debug_data_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__sdio_debug_data_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__spi_func_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_csb PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__spi_func_csb(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_func_mosi PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__spi_func_mosi(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.spi_miso PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__spi_miso(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__ca__trb_func_rxclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__trb_func_rxdata_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__trb_func_rxdata_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__trb_func_rxdata_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_rxdata_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__ca__trb_func_rxdata_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__trb_func_txclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__trb_func_txdata_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__trb_func_txdata_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__trb_func_txdata_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.trb_func_txdata_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ca__trb_func_txdata_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_cts PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_rx  {sw_top_pull_en_13[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__uart_debug_cts(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_rts PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_tx  {sw_top_pull_en_13[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__uart_debug_rts(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_rx PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_rx  {sw_top_pull_en_13[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__uart_debug_rx(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ca.uart_debug_tx PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_tx  {sw_top_pull_en_13[0] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__ca__uart_debug_tx(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// clkc.trg_ref_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_shutdown_b_out  {sw_top_pull_en_1[16] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__clkc__trg_ref_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// clkc.tst_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ale  {sw_top_pull_en_3[21:20] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__clkc__tst_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clampdn0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_clampdn0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clampup0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_clampup0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_clkout0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_clkout0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data0_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_vsync  {sw_top_pull_en_6[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data0_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_data_valid0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lcd_gpio_20  {sw_top_pull_en_5[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_data_valid0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_ext_clk26 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_ext_clk26(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_test_mux_out_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_test_mux_out_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// cvbs.cvbsafe_debug_testclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__cvbs__cvbsafe_debug_testclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_cts_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_rx  {sw_top_pull_en_13[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_cts_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_eclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_bclk  {sw_top_pull_en_10[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_eclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_poff PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_force_poff(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_poff_ack PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_5  {sw_top_pull_en_11[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_force_poff_ack(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_pon PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_force_pon(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_force_pon_ack PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_force_pon_ack(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_irq1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_mclk  {sw_top_pull_en_10[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_irq1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_irq2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_irq2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_m0_porst_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_m0_porst_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_poff_req PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_poff_req(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_pon_req PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_1  {sw_top_pull_en_11[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_pon_req(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_rst_32k_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_rst_32k_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_rts_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_tx  {sw_top_pull_en_13[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_rts_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_scl PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_scl_0  {sw_top_pull_en_11[26] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_scl(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SCL_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sda PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sda_0  {sw_top_pull_en_11[24] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_sda(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SDA_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_sw_status_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_sw_status_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_swclktck PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_swclktck(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_swdiotms PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__gnss_swdiotms(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_tm PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout0  {sw_top_pull_en_10[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_tm(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_tsync PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_ws  {sw_top_pull_en_10[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_tsync(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_uart_rx PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_rx  {sw_top_pull_en_13[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_uart_rx(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.gnss_uart_tx PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_tx  {sw_top_pull_en_13[0] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__gnss_uart_tx(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ry_by  {sw_top_pull_en_3[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__gn__io_gnsssys_sw_cfg_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.io_gnsssys_sw_cfg_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gn__io_gnsssys_sw_cfg_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_clk  {sw_top_pull_en_1[14] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_acq_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_d0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_d0  {sw_top_pull_en_1[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_acq_d0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_acq_d1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_d1  {sw_top_pull_en_1[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_acq_d1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_irq_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_irq_b  {sw_top_pull_en_1[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_irq_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_shutdown_b_out PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_shutdown_b_out  {sw_top_pull_en_1[16] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   3 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gn__trg_shutdown_b_out(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_clk  {sw_top_pull_en_1[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_spi_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_cs_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_cs_b  {sw_top_pull_en_1[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_spi_cs_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_di PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_di  {sw_top_pull_en_1[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_spi_di(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gn.trg_spi_do PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_do  {sw_top_pull_en_1[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gn__trg_spi_do(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.scl_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_scl_0  {sw_top_pull_en_11[26] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__i2__scl_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SCL_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.scl_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_7  {sw_top_pull_en_11[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__i2__scl_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_7__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.sda_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sda_0  {sw_top_pull_en_11[24] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__i2__sda_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SDA_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2.sda_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__i2__sda_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_bs PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__i2s0__hs_i2s0_bs(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__i2s0__hs_i2s0_rxd0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__i2s0__hs_i2s0_rxd1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s0.hs_i2s0_ws PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__i2s0__hs_i2s0_ws(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_bs PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__i2s1__hs_i2s1_bs(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_coex_pio_2  {sw_top_pull_en_12[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_coex_pio_0  {sw_top_pull_en_12[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   3 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   4 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__i2s1__hs_i2s1_rxd0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 4) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_coex_pio_3  {sw_top_pull_en_12[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_coex_pio_1  {sw_top_pull_en_12[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   3 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   4 = x_vip_vsync  {sw_top_pull_en_6[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__i2s1__hs_i2s1_rxd1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : ((padopt) == 4) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// i2s1.hs_i2s1_ws PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__i2s1__hs_i2s1_ws(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.jt_dbg_nsrst PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__jtag__jt_dbg_nsrst(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.ntrst PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_low_bat_ind_b  {sw_rtc_pull_en_0[8] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
//   1 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__jtag__ntrst(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.swdiotms PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
//   1 = x_jtag_tms  {sw_top_pull_en_17[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__jtag__swdiotms(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tck PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_0  {sw_rtc_pull_en_0[0] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
//   1 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__jtag__tck(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tdi PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_1  {sw_rtc_pull_en_0[2] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
//   1 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__jtag__tdi(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// jtag.tdo PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_jtag_tdo  {sw_top_pull_en_17[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__jtag__tdo(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_clk  {sw_top_pull_en_14[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ks__kas_spi_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_cs_n PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_fs  {sw_top_pull_en_14[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ks__kas_spi_cs_n(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_di PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_rx  {sw_top_pull_en_14[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ks__kas_spi_di(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ks.kas_spi_do PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_tx  {sw_top_pull_en_14[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ks__kas_spi_do(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_de PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__l_de(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_fck PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__l_fck(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_lck PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__l_lck(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.l_pclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__l_pclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_16 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_16(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_17 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_17(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_18 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_18(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_19 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_19(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_20 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_20(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_21 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_21(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_22 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_22(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ld.ldd_23 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__ld__ldd_23(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fa_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lcd_gpio_20  {sw_top_pull_en_5[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fa_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fce_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fce_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fd_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fd_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_foe_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_foe_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_frdy PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_frdy(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// lr.lcdrom_fwe_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__lr__lcdrom_fwe_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ad_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ad_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ale PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ale  {sw_top_pull_en_3[21:20] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ale(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cle PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cle  {sw_top_pull_en_3[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__nd__df_cle(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CLE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cs_b_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_0  {sw_top_pull_en_3[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_cs_b_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_cs_b_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_1  {sw_top_pull_en_3[29:28] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_cs_b_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_dqs PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_dqs  {sw_top_pull_en_3[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__nd__df_dqs(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_DQS__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_DQS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_re_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_re_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_ry_by PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ry_by  {sw_top_pull_en_3[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_ry_by(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_we_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_we_b  {sw_top_pull_en_3[23:22] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__nd__df_we_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// nd.df_wp_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_5  {sw_top_pull_en_11[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__nd__df_wp_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.dr_dir PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_1  {sw_top_pull_en_11[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ps__dr_dir(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.odo_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_0  {sw_top_pull_en_11[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ps__odo_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// ps.odo_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__ps__odo_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.cko_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_rgmii_txclk  {sw_top_pull_en_9[9:8] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
//   2 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   3 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__pw__cko_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : ((padopt) == 3) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.cko_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_5  {sw_top_pull_en_11[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_rgmii_mdc  {sw_top_pull_en_9[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__pw__cko_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.i2s01_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_coex_pio_3  {sw_top_pull_en_12[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__pw__i2s01_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_0  {sw_top_pull_en_11[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_jtag_tdo  {sw_top_pull_en_17[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__pw__pwm0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_1  {sw_top_pull_en_11[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_jtag_tms  {sw_top_pull_en_17[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   2 = x_coex_pio_2  {sw_top_pull_en_12[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__pw__pwm1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_txd_1  {sw_top_pull_en_9[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__pw__pwm2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pw.pwm3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_lcd_gpio_20  {sw_top_pull_en_5[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__pw__pwm3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.core_on PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_core_on  {sw_rtc_pull_en_0[16] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__core_on(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CORE_ON__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CORE_ON__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CORE_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.ext_on PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ext_on  {sw_rtc_pull_en_0[12] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__ext_on(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__EXT_ON__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__EXT_ON__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__EXT_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.gpio3_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__pwc__gpio3_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.io_on PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_io_on  {sw_rtc_pull_en_0[18] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__io_on(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__IO_ON__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__IO_ON__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__IO_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.lowbatt_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_low_bat_ind_b  {sw_rtc_pull_en_0[8] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__pwc__lowbatt_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.mem_on PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_mem_on  {sw_rtc_pull_en_0[14] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__mem_on(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__MEM_ON__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__MEM_ON__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__MEM_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.on_key_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_on_key_b  {sw_rtc_pull_en_0[10] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__pwc__on_key_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__ON_KEY_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__ON_KEY_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_0  {sw_rtc_pull_en_0[0] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__wakeup_src_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_1  {sw_rtc_pull_en_0[2] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__wakeup_src_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__pwc__wakeup_src_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// pwc.wakeup_src_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__pwc__wakeup_src_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxc_ctl  {sw_top_pull_en_9[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_0  {sw_top_pull_en_9[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_1  {sw_top_pull_en_9[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_2  {sw_top_pull_en_9[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_3  {sw_top_pull_en_9[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rx_clk  {sw_top_pull_en_9[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_tx_ctl  {sw_top_pull_en_9[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_0  {sw_top_pull_en_9[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_1  {sw_top_pull_en_9[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_2  {sw_top_pull_en_9[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_3  {sw_top_pull_en_9[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__eth_mac10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.eth_mac11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txclk  {sw_top_pull_en_9[9:8] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__rg__eth_mac11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.gmac_phy_intr_n PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__rg__gmac_phy_intr_n(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_mac_md PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_mdio  {sw_top_pull_en_9[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__rg__rgmii_mac_md(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_mac_mdc PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_mdc  {sw_top_pull_en_9[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__rgmii_mac_mdc(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// rg.rgmii_phy_ref_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__rg__rgmii_phy_ref_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_clk_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cle  {sw_top_pull_en_3[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__sd0__sd_clk_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CLE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_cmd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ale  {sw_top_pull_en_3[21:20] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_cmd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd0.sd_dat_0_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd0__sd_dat_0_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_clk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_we_b  {sw_top_pull_en_3[23:22] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_clk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_cmd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_cmd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd1.sd_dat_1_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd1__sd_dat_1_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_cd_b_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_5  {sw_top_pull_en_11[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sd2__sd_cd_b_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_clk_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_clk  {sw_top_pull_en_2[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__sd2__sd_clk_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_cmd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_cmd  {sw_top_pull_en_2[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd2__sd_cmd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_0  {sw_top_pull_en_2[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd2__sd_dat_2_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_1  {sw_top_pull_en_2[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd2__sd_dat_2_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_2  {sw_top_pull_en_2[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd2__sd_dat_2_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_dat_2_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_3  {sw_top_pull_en_2[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd2__sd_dat_2_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd2.sd_wp_b_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__sd2__sd_wp_b_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_clk_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_clk  {sw_top_pull_en_7[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__sd3__sd_clk_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_cmd_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_cmd  {sw_top_pull_en_7[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd3__sd_cmd_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_0  {sw_top_pull_en_7[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd3__sd_dat_3_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_1  {sw_top_pull_en_7[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd3__sd_dat_3_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_2  {sw_top_pull_en_7[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd3__sd_dat_3_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd3.sd_dat_3_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_3  {sw_top_pull_en_7[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd3__sd_dat_3_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_clk_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__sd5__sd_clk_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_cmd_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd5__sd_cmd_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd5__sd_dat_5_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd5__sd_dat_5_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd5__sd_dat_5_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd5.sd_dat_5_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd5__sd_dat_5_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_clk_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_txclk  {sw_top_pull_en_9[9:8] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__sd6__sd_clk_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_cmd_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_txd_2  {sw_top_pull_en_9[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__sd6__sd_cmd_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_txd_3  {sw_top_pull_en_9[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__sd6__sd_dat_6_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_mdc  {sw_top_pull_en_9[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__sd6__sd_dat_6_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_mdio  {sw_top_pull_en_9[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd6__sd_dat_6_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sd6.sd_dat_6_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__sd6__sd_dat_6_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.ext_ldo_on PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_low_bat_ind_b  {sw_rtc_pull_en_0[8] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__sp0__ext_ldo_on(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_clk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_clk  {sw_rtc_pull_en_0[24] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_clk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_cs_b PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_cs_b  {sw_rtc_pull_en_0[26] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_cs_b(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_0  {sw_rtc_pull_en_0[28] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_data_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_1  {sw_rtc_pull_en_0[30] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_data_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_2  {sw_rtc_pull_en_1[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_data_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_1_REG_CLR, SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_1_REG_SET, (((pullopt) << SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__SHIFT) & SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp0.qspi_data_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_3  {sw_rtc_pull_en_1[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp0__qspi_data_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_1_REG_CLR, SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_1_REG_SET, (((pullopt) << SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__SHIFT) & SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_clk_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_clk  {sw_top_pull_en_0[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp1__spi_clk_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_din_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_din  {sw_top_pull_en_0[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp1__spi_din_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_dout_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_dout  {sw_top_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp1__spi_dout_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// sp1.spi_en_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_en  {sw_top_pull_en_0[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__sp1__spi_en_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.traceclk PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__traceclk(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracectl PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracectl(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// tpiu.tracedata_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__tpiu__tracedata_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.cts_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u0__cts_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.rts_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_1  {sw_top_pull_en_11[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u0__rts_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.rxd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_rx  {sw_top_pull_en_13[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u0__rxd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u0.txd_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_tx  {sw_top_pull_en_13[0] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u0__txd_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u1.rxd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_rx  {sw_top_pull_en_13[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u1__rxd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u1.txd_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_tx  {sw_top_pull_en_13[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u1__txd_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.cts_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_3  {sw_top_pull_en_12[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u2__cts_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.rts_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_2  {sw_top_pull_en_12[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   1 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u2__rts_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.rxd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_rx  {sw_rtc_pull_en_0[22] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
//   1 = x_jtag_tms  {sw_top_pull_en_17[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   2 = x_coex_pio_1  {sw_top_pull_en_12[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__u2__rxd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u2.txd_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_tx  {sw_rtc_pull_en_0[20] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
//   1 = x_jtag_tdo  {sw_top_pull_en_17[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   2 = x_coex_pio_0  {sw_top_pull_en_12[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__u2__txd_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.cts_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   2 = x_uart4_rx  {sw_top_pull_en_13[18] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u3__cts_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.rts_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_7  {sw_top_pull_en_11[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_rgmii_mdio  {sw_top_pull_en_9[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   2 = x_uart4_tx  {sw_top_pull_en_13[16] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u3__rts_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_7__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_7__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.rxd_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_rx  {sw_top_pull_en_13[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_vip_vsync  {sw_top_pull_en_6[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u3__rxd_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u3.txd_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_tx  {sw_top_pull_en_13[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u3__txd_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_TX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.cts_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
//   1 = x_rgmii_txd_3  {sw_top_pull_en_9[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u4__cts_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.rts_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_rgmii_txd_2  {sw_top_pull_en_9[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
//   2 = x_i2s_dout1  {sw_top_pull_en_10[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__u4__rts_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : ((padopt) == 2) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.rxd_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart4_rx  {sw_top_pull_en_13[18] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u4__rxd_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// u4.txd_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart4_tx  {sw_top_pull_en_13[16] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__u4__txd_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// usb0.drvvbus PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_1  {sw_top_pull_en_3[29:28] <= pullopt , pad_type == zio_pad3v_4we_PU}
//   1 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__usb0__drvvbus(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// usb1.drvvbus PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_rx  {sw_top_pull_en_13[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
//   1 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__usb1__drvvbus(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK)), \
                         1) \
    : ((padopt) == 1) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_vsync  {sw_top_pull_en_6[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxc_ctl  {sw_top_pull_en_9[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_0  {sw_top_pull_en_9[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_1  {sw_top_pull_en_9[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_2  {sw_top_pull_en_9[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_3  {sw_top_pull_en_9[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_16 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rx_clk  {sw_top_pull_en_9[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_16(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_17 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_tx_ctl  {sw_top_pull_en_9[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_17(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_18 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_0  {sw_top_pull_en_9[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_18(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_19 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_1  {sw_top_pull_en_9[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_19(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_20 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_2  {sw_top_pull_en_9[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_20(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// vi.vip1_21 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_3  {sw_top_pull_en_9[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__vi__vip1_21(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_16 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_16(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_17 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_17(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_18 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_18(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_19 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__visbus__dout_19(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_20 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_clk  {sw_top_pull_en_7[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__visbus__dout_20(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_21 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_cmd  {sw_top_pull_en_7[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_21(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_22 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_0  {sw_top_pull_en_7[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_22(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_23 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_1  {sw_top_pull_en_7[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_23(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_24 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_2  {sw_top_pull_en_7[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_24(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_25 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_3  {sw_top_pull_en_7[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_25(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_26 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__visbus__dout_26(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_27 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_27(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_28 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_28(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_29 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_29(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_30 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_30(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// visbus.dout_31 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__visbus__dout_31(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_0  {sw_rtc_pull_en_0[0] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_1  {sw_rtc_pull_en_0[2] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_2  {sw_rtc_pull_en_0[4] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rtc_gpio_3  {sw_rtc_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__RTC_GPIO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__RTC_GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_low_bat_ind_b  {sw_rtc_pull_en_0[8] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__LOW_BAT_IND_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__LOW_BAT_IND_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_io_on  {sw_rtc_pull_en_0[18] <= pullopt , pad_type == PRDW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__IO_ON__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__IO_ON__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__IO_ON__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_tx  {sw_rtc_pull_en_0[20] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_TX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_can0_rx  {sw_rtc_pull_en_0[22] <= pullopt , pad_type == PRUW0204SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__CAN0_RX__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__CAN0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_clk  {sw_rtc_pull_en_0[24] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_CLK__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_cs_b  {sw_rtc_pull_en_0[26] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_CS_B__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_0  {sw_rtc_pull_en_0[28] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_0__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_1  {sw_rtc_pull_en_0[30] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_0_REG_CLR, SW_RTC_PULL_EN_0_REG_CLR__SPI0_IO_1__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_0_REG_SET, (((pullopt) << SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__SHIFT) & SW_RTC_PULL_EN_0_REG_SET__SPI0_IO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_2  {sw_rtc_pull_en_1[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_1_REG_CLR, SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_2__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_1_REG_SET, (((pullopt) << SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__SHIFT) & SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rtc_gpio_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi0_io_3  {sw_rtc_pull_en_1[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__rtc_gpio_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_RTC_PULL_EN_1_REG_CLR, SW_RTC_PULL_EN_1_REG_CLR__SPI0_IO_3__MASK), \
                         SET_IO_REG(SW_RTC_PULL_EN_1_REG_SET, (((pullopt) << SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__SHIFT) & SW_RTC_PULL_EN_1_REG_SET__SPI0_IO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_en  {sw_top_pull_en_0[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_clk  {sw_top_pull_en_0[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_din  {sw_top_pull_en_0[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_spi1_dout  {sw_top_pull_en_0[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_0_REG_CLR, SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_0_REG_SET, (((pullopt) << SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__SHIFT) & SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_clk  {sw_top_pull_en_14[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_tx  {sw_top_pull_en_14[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_rx  {sw_top_pull_en_14[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp0_fs  {sw_top_pull_en_14[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP0_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP0_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_clk  {sw_top_pull_en_14[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_tx  {sw_top_pull_en_14[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_TX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_rx  {sw_top_pull_en_14[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_RX__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sp_gpio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_usp1_fs  {sw_top_pull_en_14[14] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__sp_gpio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_14_REG_CLR, SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_14_REG_SET, (((pullopt) << SW_TOP_PULL_EN_14_REG_SET__USP1_FS__SHIFT) & SW_TOP_PULL_EN_14_REG_SET__USP1_FS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_clk  {sw_top_pull_en_1[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_di  {sw_top_pull_en_1[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_do  {sw_top_pull_en_1[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_spi_cs_b  {sw_top_pull_en_1[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_d1  {sw_top_pull_en_1[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_irq_b  {sw_top_pull_en_1[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_d0  {sw_top_pull_en_1[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_acq_clk  {sw_top_pull_en_1[14] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gnss_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_trg_shutdown_b_out  {sw_top_pull_en_1[16] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gnss_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_1_REG_CLR, SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_1_REG_SET, (((pullopt) << SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT) & SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_clk  {sw_top_pull_en_2[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__gp__sdio_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_cmd  {sw_top_pull_en_2[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_0  {sw_top_pull_en_2[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_1  {sw_top_pull_en_2[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_2  {sw_top_pull_en_2[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio2_dat_3  {sw_top_pull_en_2[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_2_REG_CLR, SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_2_REG_SET, (((pullopt) << SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__SHIFT) & SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_0  {sw_top_pull_en_3[15:14] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_1  {sw_top_pull_en_3[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_2  {sw_top_pull_en_3[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_3  {sw_top_pull_en_3[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_4  {sw_top_pull_en_3[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_5  {sw_top_pull_en_3[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_6  {sw_top_pull_en_3[3:2] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ad_7  {sw_top_pull_en_3[1:0] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cle  {sw_top_pull_en_3[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__nand_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CLE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CLE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ale  {sw_top_pull_en_3[21:20] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_we_b  {sw_top_pull_en_3[23:22] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_re_b  {sw_top_pull_en_3[25:24] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_ry_by  {sw_top_pull_en_3[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_0  {sw_top_pull_en_3[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_cs_b_1  {sw_top_pull_en_3[29:28] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__nand_gpio_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.nand_gpio_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_df_dqs  {sw_top_pull_en_3[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__nand_gpio_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_3_REG_CLR, SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_3_REG_SET, (((pullopt) << SW_TOP_PULL_EN_3_REG_SET__DF_DQS__SHIFT) & SW_TOP_PULL_EN_3_REG_SET__DF_DQS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_0  {sw_top_pull_en_4[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_1  {sw_top_pull_en_4[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_2  {sw_top_pull_en_4[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_3  {sw_top_pull_en_4[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_4  {sw_top_pull_en_4[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_5  {sw_top_pull_en_4[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_6  {sw_top_pull_en_4[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_7  {sw_top_pull_en_4[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_8  {sw_top_pull_en_4[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_9  {sw_top_pull_en_4[27:26] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_10  {sw_top_pull_en_4[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_15 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_11  {sw_top_pull_en_4[31:30] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_15(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_16 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_12  {sw_top_pull_en_5[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_16(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_17 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_13  {sw_top_pull_en_5[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_17(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_18 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_14  {sw_top_pull_en_5[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_18(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_19 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_ldd_15  {sw_top_pull_en_5[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_19(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_pclk  {sw_top_pull_en_4[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_lck  {sw_top_pull_en_4[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_fck  {sw_top_pull_en_4[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_l_de  {sw_top_pull_en_4[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_4_REG_CLR, SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_4_REG_SET, (((pullopt) << SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT) & SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lcd_gpio_20 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lcd_gpio_20  {sw_top_pull_en_5[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__lcd_gpio_20(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_5_REG_CLR, SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_5_REG_SET, (((pullopt) << SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__SHIFT) & SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_0  {sw_top_pull_en_6[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_1  {sw_top_pull_en_6[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_2  {sw_top_pull_en_6[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_3  {sw_top_pull_en_6[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_4  {sw_top_pull_en_6[9:8] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_5  {sw_top_pull_en_6[11:10] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_6  {sw_top_pull_en_6[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_7  {sw_top_pull_en_6[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_pxclk  {sw_top_pull_en_6[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_hsync  {sw_top_pull_en_6[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.vip_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_vip_vsync  {sw_top_pull_en_6[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__vip_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_6_REG_CLR, SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_6_REG_SET, (((pullopt) << SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT) & SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_clk  {sw_top_pull_en_7[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__gp__sdio3_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_cmd  {sw_top_pull_en_7[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio3_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_0  {sw_top_pull_en_7[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio3_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_1  {sw_top_pull_en_7[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio3_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_2  {sw_top_pull_en_7[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio3_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio3_dat_3  {sw_top_pull_en_7[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio3_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_7_REG_CLR, SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_7_REG_SET, (((pullopt) << SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__SHIFT) & SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_0  {sw_top_pull_en_12[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__sdio3_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_1  {sw_top_pull_en_12[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__sdio3_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_2  {sw_top_pull_en_12[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__sdio3_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio3_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_coex_pio_3  {sw_top_pull_en_12[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__sdio3_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_12_REG_CLR, SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_12_REG_SET, (((pullopt) << SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT) & SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_clk  {sw_top_pull_en_8[1:0] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__gp__sdio5_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_cmd  {sw_top_pull_en_8[5:4] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio5_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_0  {sw_top_pull_en_8[7:6] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio5_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_1  {sw_top_pull_en_8[9:8] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio5_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_2  {sw_top_pull_en_8[11:10] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio5_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.sdio5_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sdio5_dat_3  {sw_top_pull_en_8[13:12] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__sdio5_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_8_REG_CLR, SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_8_REG_SET, (((pullopt) << SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT) & SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_0  {sw_top_pull_en_9[1:0] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_1  {sw_top_pull_en_9[3:2] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_2  {sw_top_pull_en_9[5:4] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txd_3  {sw_top_pull_en_9[7:6] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_txclk  {sw_top_pull_en_9[9:8] <= pullopt , pad_type == zio_pad3v_sdclk_PD}
#define set_pullsel__gp__rgmii_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_tx_ctl  {sw_top_pull_en_9[13:12] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_0  {sw_top_pull_en_9[15:14] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_1  {sw_top_pull_en_9[17:16] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_2  {sw_top_pull_en_9[19:18] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxd_3  {sw_top_pull_en_9[21:20] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_10 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rx_clk  {sw_top_pull_en_9[23:22] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_10(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_11 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_rxc_ctl  {sw_top_pull_en_9[25:24] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_11(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_12 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_mdio  {sw_top_pull_en_9[27:26] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__rgmii_gpio_12(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_13 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_mdc  {sw_top_pull_en_9[29:28] <= pullopt , pad_type == zio_pad3v_4we_PD}
#define set_pullsel__gp__rgmii_gpio_13(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.rgmii_gpio_14 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_rgmii_intr_n  {sw_top_pull_en_9[31:30] <= pullopt , pad_type == zio_pad3v_4we_PU}
#define set_pullsel__gp__rgmii_gpio_14(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_9_REG_CLR, SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_9_REG_SET, (((pullopt) << SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT) & SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_mclk  {sw_top_pull_en_10[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_bclk  {sw_top_pull_en_10[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_ws  {sw_top_pull_en_10[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout0  {sw_top_pull_en_10[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout1  {sw_top_pull_en_10[8] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_dout2  {sw_top_pull_en_10[10] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.i2s_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_i2s_din  {sw_top_pull_en_10[12] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__i2s_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_10_REG_CLR, SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_10_REG_SET, (((pullopt) << SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT) & SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_0  {sw_top_pull_en_11[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_1  {sw_top_pull_en_11[2] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_2  {sw_top_pull_en_11[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_3  {sw_top_pull_en_11[6] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_4  {sw_top_pull_en_11[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_5  {sw_top_pull_en_11[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_6  {sw_top_pull_en_11[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_gpio_7  {sw_top_pull_en_11[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__GPIO_7__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__GPIO_7__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_sda_0  {sw_top_pull_en_11[24] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SDA_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SDA_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SDA_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_scl_0  {sw_top_pull_en_11[26] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_11_REG_CLR, SW_TOP_PULL_EN_11_REG_CLR__SCL_0__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_11_REG_SET, (((pullopt) << SW_TOP_PULL_EN_11_REG_SET__SCL_0__SHIFT) & SW_TOP_PULL_EN_11_REG_SET__SCL_0__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_tx  {sw_top_pull_en_13[0] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart0_rx  {sw_top_pull_en_13[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_tx  {sw_top_pull_en_13[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart1_rx  {sw_top_pull_en_13[10] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART1_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART1_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_tx  {sw_top_pull_en_13[12] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart3_rx  {sw_top_pull_en_13[14] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART3_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART3_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart4_tx  {sw_top_pull_en_13[16] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.uart_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_uart4_rx  {sw_top_pull_en_13[18] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__uart_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_13_REG_CLR, SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_13_REG_SET, (((pullopt) << SW_TOP_PULL_EN_13_REG_SET__UART4_RX__SHIFT) & SW_TOP_PULL_EN_13_REG_SET__UART4_RX__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_jtag_tdo  {sw_top_pull_en_17[0] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__jtag_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_jtag_tms  {sw_top_pull_en_17[2] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__jtag_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_jtag_tck  {sw_top_pull_en_17[4] <= pullopt , pad_type == PRDW0610SDGZ_M311311}
#define set_pullsel__gp__jtag_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_jtag_tdi  {sw_top_pull_en_17[6] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__jtag_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.jtag_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_jtag_trstn  {sw_top_pull_en_17[8] <= pullopt , pad_type == PRUW0610SDGZ_M311311}
#define set_pullsel__gp__jtag_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_17_REG_CLR, SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_17_REG_SET, (((pullopt) << SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT) & SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_0 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d0p  {sw_top_pull_en_16[1:0] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_0(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_16_REG_CLR, SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_16_REG_SET, (((pullopt) << SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__SHIFT) & SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_1 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d0n  {sw_top_pull_en_16[3:2] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_1(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_16_REG_CLR, SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_16_REG_SET, (((pullopt) << SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__SHIFT) & SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_2 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d1p  {sw_top_pull_en_15[13:12] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_2(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_3 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d1n  {sw_top_pull_en_15[15:14] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_3(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_4 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d2p  {sw_top_pull_en_15[9:8] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_4(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_5 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d2n  {sw_top_pull_en_15[11:10] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_5(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_6 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d3p  {sw_top_pull_en_15[5:4] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_6(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_7 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d3n  {sw_top_pull_en_15[7:6] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_7(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_8 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d4p  {sw_top_pull_en_15[1:0] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_8(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// gp.lvds_gpio_9 PullSel
////////////////////////////////////////////////
// padopt: 
//   0 = x_lvds_tx0d4n  {sw_top_pull_en_15[3:2] <= pullopt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_pullsel__gp__lvds_gpio_9(padopt, pullopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_PULL_EN_15_REG_CLR, SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__MASK), \
                         SET_IO_REG(SW_TOP_PULL_EN_15_REG_SET, (((pullopt) << SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__SHIFT) & SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__MASK)), \
                         1) \
    : 0)


////////////////////////////////////////////////
// x_lvds_tx0d0n Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d0n  {sw_top_ad_cntl_0[9] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d0n(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d0p Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d0p  {sw_top_ad_cntl_0[8] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d0p(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d1n Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d1n  {sw_top_ad_cntl_0[7] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d1n(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d1p Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d1p  {sw_top_ad_cntl_0[6] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d1p(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d2n Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d2n  {sw_top_ad_cntl_0[5] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d2n(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d2p Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d2p  {sw_top_ad_cntl_0[4] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d2p(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d3n Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d3n  {sw_top_ad_cntl_0[3] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d3n(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d3p Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d3p  {sw_top_ad_cntl_0[2] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d3p(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d4n Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d4n  {sw_top_ad_cntl_0[1] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d4n(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__MASK)), \
     1)


////////////////////////////////////////////////
// x_lvds_tx0d4p Analog/digital Control
////////////////////////////////////////////////
// x_lvds_tx0d4p  {sw_top_ad_cntl_0[0] <= ad_cntl_opt , pad_type == PRDWUWHW08SCDG_HZ}
#define set_ad_cntl__x_lvds_tx0d4p(ad_cntl_opt) \
    (SET_IO_REG(SW_TOP_AD_CNTL_0_REG_CLR, SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__MASK), \
     SET_IO_REG(SW_TOP_AD_CNTL_0_REG_SET, (((ad_cntl_opt) << SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__SHIFT) & SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__MASK)), \
     1)


////////////////////////////////////////////////
// au.digmic Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__digmic(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__DIGMIC__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__DIGMIC__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__DIGMIC__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__urfs_0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__URFS_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_0__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_0__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__urfs_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__URFS_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__urfs_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__URFS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_2__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__URFS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urxd_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__urxd_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__URXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__URXD_1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__URXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urxd_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__urxd_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__AU__URXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__AU__URXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.usclk_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__usclk_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__USCLK_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__USCLK_1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__USCLK_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.usclk_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__usclk_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__AU__USCLK_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__AU__USCLK_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utfs_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__utfs_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__UTFS_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__UTFS_1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__UTFS_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utfs_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__utfs_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTFS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__AU__UTFS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utxd_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__utxd_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__AU__UTXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__AU__UTXD_1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__AU__UTXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utxd_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__au__utxd_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__AU__UTXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__AU__UTXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__c0__can_rxd_0_trnsv0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__C0__CAN_RXD_0_TRNSV0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0_TRNSV0__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__C0__CAN_RXD_0_TRNSV0__MASK)), \
     1) 


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__c0__can_rxd_0_trnsv1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__C0__CAN_RXD_0_TRNSV1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__C0__CAN_RXD_0_TRNSV1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__C0__CAN_RXD_0_TRNSV1__MASK)), \
     1) 


////////////////////////////////////////////////
// c1.can_rxd_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__c1__can_rxd_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__C1__CAN_RXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__C1__CAN_RXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// ca.spi_func_csb Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__ca__spi_func_csb(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__CA__SPI_FUNC_CSB__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__CA__SPI_FUNC_CSB__MASK)), \
     1) 


////////////////////////////////////////////////
// clkc.trg_ref_clk Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__clkc__trg_ref_clk(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__CLKC__TRG_REF_CLK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__CLKC__TRG_REF_CLK__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_irq1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__gnss_irq1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ1__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_irq2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__gnss_irq2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_IRQ2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_IRQ2__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_m0_porst_b Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__gnss_m0_porst_b(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__GN__GNSS_M0_PORST_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__GN__GNSS_M0_PORST_B__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_clk Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__trg_acq_clk(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_CLK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_CLK__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_d0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__trg_acq_d0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D0__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_d1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__trg_acq_d1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_ACQ_D1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_ACQ_D1__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_irq_b Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__trg_irq_b(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_IRQ_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_IRQ_B__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_spi_di Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__gn__trg_spi_di(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__GN__TRG_SPI_DI__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__GN__TRG_SPI_DI__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_bs Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s0__hs_i2s0_bs(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S0__HS_I2S0_BS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_BS__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_BS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s0__hs_i2s0_rxd0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S0__HS_I2S0_RXD0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_RXD0__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_RXD0__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s0__hs_i2s0_rxd1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S0__HS_I2S0_RXD1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_RXD1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_RXD1__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_ws Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s0__hs_i2s0_ws(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S0__HS_I2S0_WS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_WS__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S0__HS_I2S0_WS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_bs Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s1__hs_i2s1_bs(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S1__HS_I2S1_BS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_BS__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_BS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s1__hs_i2s1_rxd0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S1__HS_I2S1_RXD0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_RXD0__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_RXD0__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s1__hs_i2s1_rxd1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S1__HS_I2S1_RXD1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_RXD1__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_RXD1__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_ws Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__i2s1__hs_i2s1_ws(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__I2S1__HS_I2S1_WS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_WS__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__I2S1__HS_I2S1_WS__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.jt_dbg_nsrst Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__jtag__jt_dbg_nsrst(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__JT_DBG_NSRST__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__JTAG__JT_DBG_NSRST__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.ntrst Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__jtag__ntrst(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__JTAG__NTRST__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__JTAG__NTRST__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.swdiotms Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__jtag__swdiotms(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__JTAG__SWDIOTMS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__JTAG__SWDIOTMS__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__JTAG__SWDIOTMS__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.tck Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__jtag__tck(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__JTAG__TCK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__JTAG__TCK__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__JTAG__TCK__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.tdi Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__jtag__tdi(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__JTAG__TDI__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__JTAG__TDI__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__JTAG__TDI__MASK)), \
     1) 


////////////////////////////////////////////////
// ks.kas_spi_cs_n Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__ks__kas_spi_cs_n(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__KS__KAS_SPI_CS_N__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__KS__KAS_SPI_CS_N__MASK)), \
     1) 


////////////////////////////////////////////////
// pwc.lowbatt_b Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__pwc__lowbatt_b(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__PWC__LOWBATT_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__PWC__LOWBATT_B__MASK)), \
     1) 


////////////////////////////////////////////////
// pwc.on_key_b Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__pwc__on_key_b(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__PWC__ON_KEY_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__PWC__ON_KEY_B__MASK)), \
     1) 


////////////////////////////////////////////////
// rg.gmac_phy_intr_n Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__rg__gmac_phy_intr_n(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__RG__GMAC_PHY_INTR_N__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__RG__GMAC_PHY_INTR_N__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd1__sd_dat_1_0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_0__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd1__sd_dat_1_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_1__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd1__sd_dat_1_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_3 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd1__sd_dat_1_3(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD1__SD_DAT_1_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD1__SD_DAT_1_3__MASK)), \
     1) 


////////////////////////////////////////////////
// sd2.sd_cd_b_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd2__sd_cd_b_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__SD2__SD_CD_B_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__SD2__SD_CD_B_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd2.sd_wp_b_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd2__sd_wp_b_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__SD2__SD_WP_B_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__SD2__SD_WP_B_2__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__SD2__SD_WP_B_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_clk_6 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_clk_6(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CLK_6__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CLK_6__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_cmd_6 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_cmd_6(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_CMD_6__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_CMD_6__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_0 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_dat_6_0(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_0__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_1 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_dat_6_1(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_1__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_dat_6_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_3 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__sd6__sd_dat_6_3(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__SD6__SD_DAT_6_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__SD6__SD_DAT_6_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u2.cts_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__u2__cts_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__U2__CTS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__U2__CTS_2__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__U2__CTS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// u2.rxd_2 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__u2__rxd_2(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_CLR, SW_RTC_IN_DISABLE_2_REG_CLR__U2__RXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_2_REG_SET, (((disable) << SW_RTC_IN_DISABLE_2_REG_SET__U2__RXD_2__SHIFT) & SW_RTC_IN_DISABLE_2_REG_SET__U2__RXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// u3.cts_3 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__u3__cts_3(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__U3__CTS_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__U3__CTS_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u3.rxd_3 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__u3__rxd_3(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_CLR, SW_RTC_IN_DISABLE_0_REG_CLR__U3__RXD_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_0_REG_SET, (((disable) << SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__SHIFT) & SW_RTC_IN_DISABLE_0_REG_SET__U3__RXD_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u4.cts_4 Input Disable
////////////////////////////////////////////////
// disable: 
//   0 = normal mode
//   1 = disable by forcing disable_value to the internal unit
#define set_input_disable__u4__cts_4(disable) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_CLR, SW_RTC_IN_DISABLE_1_REG_CLR__U4__CTS_4__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_1_REG_SET, (((disable) << SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__SHIFT) & SW_RTC_IN_DISABLE_1_REG_SET__U4__CTS_4__MASK)), \
     1) 


////////////////////////////////////////////////
// au.digmic Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__digmic(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__DIGMIC__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__DIGMIC__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__DIGMIC__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__urfs_0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__URFS_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_0__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_0__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__urfs_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__URFS_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urfs_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__urfs_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__URFS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URFS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urxd_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__urxd_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__URXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URXD_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__URXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.urxd_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__urxd_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__URXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__URXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.usclk_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__usclk_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__USCLK_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__USCLK_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__USCLK_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.usclk_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__usclk_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__USCLK_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__USCLK_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utfs_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__utfs_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__UTFS_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__UTFS_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__UTFS_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utfs_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__utfs_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTFS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTFS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utxd_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__utxd_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__AU__UTXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__UTXD_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__AU__UTXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// au.utxd_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__au__utxd_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__AU__UTXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__AU__UTXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__c0__can_rxd_0_trnsv0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__C0__CAN_RXD_0_TRNSV0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0_TRNSV0__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__C0__CAN_RXD_0_TRNSV0__MASK)), \
     1) 


////////////////////////////////////////////////
// c0.can_rxd_0_trnsv1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__c0__can_rxd_0_trnsv1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__C0__CAN_RXD_0_TRNSV1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__C0__CAN_RXD_0_TRNSV1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__C0__CAN_RXD_0_TRNSV1__MASK)), \
     1) 


////////////////////////////////////////////////
// c1.can_rxd_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__c1__can_rxd_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__C1__CAN_RXD_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__C1__CAN_RXD_1__MASK)), \
     1) 


////////////////////////////////////////////////
// ca.spi_func_csb Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__ca__spi_func_csb(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CA__SPI_FUNC_CSB__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__CA__SPI_FUNC_CSB__MASK)), \
     1) 


////////////////////////////////////////////////
// clkc.trg_ref_clk Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__clkc__trg_ref_clk(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__CLKC__TRG_REF_CLK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__CLKC__TRG_REF_CLK__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_irq1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__gnss_irq1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ1__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_irq2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__gnss_irq2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_IRQ2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_IRQ2__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.gnss_m0_porst_b Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__gnss_m0_porst_b(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__GN__GNSS_M0_PORST_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__GN__GNSS_M0_PORST_B__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_clk Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__trg_acq_clk(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_CLK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_CLK__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_d0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__trg_acq_d0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D0__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_acq_d1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__trg_acq_d1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_ACQ_D1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_ACQ_D1__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_irq_b Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__trg_irq_b(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_IRQ_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_IRQ_B__MASK)), \
     1) 


////////////////////////////////////////////////
// gn.trg_spi_di Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__gn__trg_spi_di(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__GN__TRG_SPI_DI__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__GN__TRG_SPI_DI__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_bs Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s0__hs_i2s0_bs(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S0__HS_I2S0_BS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_BS__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_BS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s0__hs_i2s0_rxd0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S0__HS_I2S0_RXD0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_RXD0__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_RXD0__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_rxd1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s0__hs_i2s0_rxd1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S0__HS_I2S0_RXD1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_RXD1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_RXD1__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s0.hs_i2s0_ws Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s0__hs_i2s0_ws(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S0__HS_I2S0_WS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_WS__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S0__HS_I2S0_WS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_bs Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s1__hs_i2s1_bs(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S1__HS_I2S1_BS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_BS__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_BS__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s1__hs_i2s1_rxd0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S1__HS_I2S1_RXD0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_RXD0__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_RXD0__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_rxd1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s1__hs_i2s1_rxd1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S1__HS_I2S1_RXD1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_RXD1__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_RXD1__MASK)), \
     1) 


////////////////////////////////////////////////
// i2s1.hs_i2s1_ws Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__i2s1__hs_i2s1_ws(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__I2S1__HS_I2S1_WS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_WS__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__I2S1__HS_I2S1_WS__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.jt_dbg_nsrst Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__jtag__jt_dbg_nsrst(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__JT_DBG_NSRST__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__JT_DBG_NSRST__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.ntrst Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__jtag__ntrst(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__JTAG__NTRST__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__JTAG__NTRST__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.swdiotms Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__jtag__swdiotms(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__JTAG__SWDIOTMS__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__SWDIOTMS__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__SWDIOTMS__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.tck Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__jtag__tck(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__JTAG__TCK__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__TCK__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__TCK__MASK)), \
     1) 


////////////////////////////////////////////////
// jtag.tdi Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__jtag__tdi(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__JTAG__TDI__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__TDI__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__JTAG__TDI__MASK)), \
     1) 


////////////////////////////////////////////////
// ks.kas_spi_cs_n Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__ks__kas_spi_cs_n(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__KS__KAS_SPI_CS_N__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__KS__KAS_SPI_CS_N__MASK)), \
     1) 


////////////////////////////////////////////////
// pwc.lowbatt_b Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__pwc__lowbatt_b(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__LOWBATT_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__LOWBATT_B__MASK)), \
     1) 


////////////////////////////////////////////////
// pwc.on_key_b Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__pwc__on_key_b(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__PWC__ON_KEY_B__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__PWC__ON_KEY_B__MASK)), \
     1) 


////////////////////////////////////////////////
// rg.gmac_phy_intr_n Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__rg__gmac_phy_intr_n(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__RG__GMAC_PHY_INTR_N__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__RG__GMAC_PHY_INTR_N__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd1__sd_dat_1_0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_0__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd1__sd_dat_1_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_1__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd1__sd_dat_1_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd1.sd_dat_1_3 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd1__sd_dat_1_3(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD1__SD_DAT_1_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD1__SD_DAT_1_3__MASK)), \
     1) 


////////////////////////////////////////////////
// sd2.sd_cd_b_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd2__sd_cd_b_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__SD2__SD_CD_B_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__SD2__SD_CD_B_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd2.sd_wp_b_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd2__sd_wp_b_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__SD2__SD_WP_B_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__SD2__SD_WP_B_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__SD2__SD_WP_B_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_clk_6 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_clk_6(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CLK_6__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CLK_6__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_cmd_6 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_cmd_6(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_CMD_6__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_CMD_6__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_0 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_dat_6_0(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_0__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_0__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_1 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_dat_6_1(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_1__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_1__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_dat_6_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_2__MASK)), \
     1) 


////////////////////////////////////////////////
// sd6.sd_dat_6_3 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__sd6__sd_dat_6_3(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__SD6__SD_DAT_6_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__SD6__SD_DAT_6_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u2.cts_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__u2__cts_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__U2__CTS_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__U2__CTS_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__U2__CTS_2__MASK)), \
     1) 


////////////////////////////////////////////////
// u2.rxd_2 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__u2__rxd_2(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_CLR, SW_RTC_IN_DISABLE_VAL_2_REG_CLR__U2__RXD_2__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_2_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_2_REG_SET__U2__RXD_2__SHIFT) & SW_RTC_IN_DISABLE_VAL_2_REG_SET__U2__RXD_2__MASK)), \
     1) 


////////////////////////////////////////////////
// u3.cts_3 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__u3__cts_3(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U3__CTS_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__U3__CTS_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u3.rxd_3 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__u3__rxd_3(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_CLR, SW_RTC_IN_DISABLE_VAL_0_REG_CLR__U3__RXD_3__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_0_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__SHIFT) & SW_RTC_IN_DISABLE_VAL_0_REG_SET__U3__RXD_3__MASK)), \
     1) 


////////////////////////////////////////////////
// u4.cts_4 Input Disable Value
////////////////////////////////////////////////
// disable Value: 
//   0 = disabled input would receive constant 0
//   1 = disabled input would receive constant 1
#define set_input_disable_value__u4__cts_4(disable_value) \
    (SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_CLR, SW_RTC_IN_DISABLE_VAL_1_REG_CLR__U4__CTS_4__MASK), \
     SET_IO_REG(SW_RTC_IN_DISABLE_VAL_1_REG_SET, (((disable_value) << SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__SHIFT) & SW_RTC_IN_DISABLE_VAL_1_REG_SET__U4__CTS_4__MASK)), \
     1) 


