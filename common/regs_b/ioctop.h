/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __IOCTOP_H__
#define __IOCTOP_H__



/* IOC_TOP */
/* ==================================================================== */

/* SW_TOP_FUNC_SEL_0_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_0_REG register */
#define SW_TOP_FUNC_SEL_0_REG_SET 0x10E40080

/* SW_TOP_FUNC_SEL_0_REG_SET.spi1_en - sets function_select bits of SPI1_EN pad */
/* sets function_select bits of SPI1_EN pad */
/* 0b000 : sp_gpio[0] */
/* 0b001 : sp1.spi_en_1 */
/* 0b010 : au.utfs_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__SHIFT       0
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__MASK        0x00000007
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_EN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_SET.spi1_clk - sets function_select bits of SPI1_CLK pad */
/* sets function_select bits of SPI1_CLK pad */
/* 0b000 : sp_gpio[1] */
/* 0b001 : sp1.spi_clk_1 */
/* 0b010 : au.usclk_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__SHIFT       4
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_SET.spi1_din - sets function_select bits of SPI1_DIN pad */
/* sets function_select bits of SPI1_DIN pad */
/* 0b000 : sp_gpio[2] */
/* 0b001 : sp1.spi_din_1 */
/* 0b010 : au.urxd_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__SHIFT       8
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__MASK        0x00000700
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DIN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_SET.spi1_dout - sets function_select bits of SPI1_DOUT pad */
/* sets function_select bits of SPI1_DOUT pad */
/* 0b000 : sp_gpio[3] */
/* 0b001 : sp1.spi_dout_1 */
/* 0b010 : au.utxd_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__SHIFT       12
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__MASK        0x00007000
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_0_REG_SET__SPI1_DOUT__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_0_REG register */
#define SW_TOP_FUNC_SEL_0_REG_CLR 0x10E40084

/* SW_TOP_FUNC_SEL_0_REG_CLR.spi1_en - clears function_select bits of SPI1_EN pad */
/* clears function_select bits of SPI1_EN pad */
/* 0b000 : sp_gpio[0] */
/* 0b001 : sp1.spi_en_1 */
/* 0b010 : au.utfs_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__SHIFT       0
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__MASK        0x00000007
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_EN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_CLR.spi1_clk - clears function_select bits of SPI1_CLK pad */
/* clears function_select bits of SPI1_CLK pad */
/* 0b000 : sp_gpio[1] */
/* 0b001 : sp1.spi_clk_1 */
/* 0b010 : au.usclk_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__SHIFT       4
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_CLR.spi1_din - clears function_select bits of SPI1_DIN pad */
/* clears function_select bits of SPI1_DIN pad */
/* 0b000 : sp_gpio[2] */
/* 0b001 : sp1.spi_din_1 */
/* 0b010 : au.urxd_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__SHIFT       8
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__MASK        0x00000700
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DIN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_0_REG_CLR.spi1_dout - clears function_select bits of SPI1_DOUT pad */
/* clears function_select bits of SPI1_DOUT pad */
/* 0b000 : sp_gpio[3] */
/* 0b001 : sp1.spi_dout_1 */
/* 0b010 : au.utxd_2_mux0 */
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__SHIFT       12
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__WIDTH       3
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__MASK        0x00007000
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_0_REG_CLR__SPI1_DOUT__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_1_REG register */
#define SW_TOP_FUNC_SEL_1_REG_SET 0x10E40088

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_spi_clk - sets function_select bits of TRG_SPI_CLK pad */
/* sets function_select bits of TRG_SPI_CLK pad */
/* 0b000 : gnss_gpio[0] */
/* 0b001 : gn.trg_spi_clk_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_spi_di - sets function_select bits of TRG_SPI_DI pad */
/* sets function_select bits of TRG_SPI_DI pad */
/* 0b000 : gnss_gpio[1] */
/* 0b001 : gn.trg_spi_di_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__SHIFT       4
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__MASK        0x00000070
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_spi_do - sets function_select bits of TRG_SPI_DO pad */
/* sets function_select bits of TRG_SPI_DO pad */
/* 0b000 : gnss_gpio[2] */
/* 0b001 : gn.trg_spi_do_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__SHIFT       8
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__MASK        0x00000700
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_DO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_spi_cs_b - sets function_select bits of TRG_SPI_CS_B pad */
/* sets function_select bits of TRG_SPI_CS_B pad */
/* 0b000 : gnss_gpio[3] */
/* 0b001 : gn.trg_spi_cs_b_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__SHIFT       12
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__MASK        0x00007000
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_SPI_CS_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_acq_d1 - sets function_select bits of TRG_ACQ_D1 pad */
/* sets function_select bits of TRG_ACQ_D1 pad */
/* 0b000 : gnss_gpio[4] */
/* 0b001 : gn.trg_acq_d1_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__SHIFT       16
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__MASK        0x00070000
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_irq_b - sets function_select bits of TRG_IRQ_B pad */
/* sets function_select bits of TRG_IRQ_B pad */
/* 0b000 : gnss_gpio[5] */
/* 0b001 : gn.trg_irq_b_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__SHIFT       20
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__MASK        0x00700000
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_IRQ_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_acq_d0 - sets function_select bits of TRG_ACQ_D0 pad */
/* sets function_select bits of TRG_ACQ_D0 pad */
/* 0b000 : gnss_gpio[6] */
/* 0b001 : gn.trg_acq_d0_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__SHIFT       24
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__MASK        0x07000000
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_SET.trg_acq_clk - sets function_select bits of TRG_ACQ_CLK pad */
/* sets function_select bits of TRG_ACQ_CLK pad */
/* 0b000 : gnss_gpio[7] */
/* 0b001 : gn.trg_acq_clk_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__SHIFT       28
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__MASK        0x70000000
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_1_REG_SET__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_1_REG register */
#define SW_TOP_FUNC_SEL_1_REG_CLR 0x10E4008C

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_spi_clk - clears function_select bits of TRG_SPI_CLK pad */
/* clears function_select bits of TRG_SPI_CLK pad */
/* 0b000 : gnss_gpio[0] */
/* 0b001 : gn.trg_spi_clk_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_spi_di - clears function_select bits of TRG_SPI_DI pad */
/* clears function_select bits of TRG_SPI_DI pad */
/* 0b000 : gnss_gpio[1] */
/* 0b001 : gn.trg_spi_di_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__SHIFT       4
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__MASK        0x00000070
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_spi_do - clears function_select bits of TRG_SPI_DO pad */
/* clears function_select bits of TRG_SPI_DO pad */
/* 0b000 : gnss_gpio[2] */
/* 0b001 : gn.trg_spi_do_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__SHIFT       8
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__MASK        0x00000700
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_DO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_spi_cs_b - clears function_select bits of TRG_SPI_CS_B pad */
/* clears function_select bits of TRG_SPI_CS_B pad */
/* 0b000 : gnss_gpio[3] */
/* 0b001 : gn.trg_spi_cs_b_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__SHIFT       12
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__MASK        0x00007000
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_SPI_CS_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_acq_d1 - clears function_select bits of TRG_ACQ_D1 pad */
/* clears function_select bits of TRG_ACQ_D1 pad */
/* 0b000 : gnss_gpio[4] */
/* 0b001 : gn.trg_acq_d1_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__SHIFT       16
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__MASK        0x00070000
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_irq_b - clears function_select bits of TRG_IRQ_B pad */
/* clears function_select bits of TRG_IRQ_B pad */
/* 0b000 : gnss_gpio[5] */
/* 0b001 : gn.trg_irq_b_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__SHIFT       20
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__MASK        0x00700000
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_IRQ_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_acq_d0 - clears function_select bits of TRG_ACQ_D0 pad */
/* clears function_select bits of TRG_ACQ_D0 pad */
/* 0b000 : gnss_gpio[6] */
/* 0b001 : gn.trg_acq_d0_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__SHIFT       24
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__MASK        0x07000000
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_1_REG_CLR.trg_acq_clk - clears function_select bits of TRG_ACQ_CLK pad */
/* clears function_select bits of TRG_ACQ_CLK pad */
/* 0b000 : gnss_gpio[7] */
/* 0b001 : gn.trg_acq_clk_mux0 */
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__SHIFT       28
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__MASK        0x70000000
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_1_REG_CLR__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_2_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_2_REG register */
#define SW_TOP_FUNC_SEL_2_REG_SET 0x10E40090

/* SW_TOP_FUNC_SEL_2_REG_SET.trg_shutdown_b_out - sets function_select bits of TRG_SHUTDOWN_B_OUT pad */
/* sets function_select bits of TRG_SHUTDOWN_B_OUT pad */
/* 0b000 : gnss_gpio[8] */
/* 0b001 : gn.trg_shutdown_b_out_mux0 */
/* 0b010 : clkc.trg_ref_clk_mux0 */
#define SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT       0
#define SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__WIDTH       3
#define SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__MASK        0x00000007
#define SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_2_REG_SET__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_2_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_2_REG register */
#define SW_TOP_FUNC_SEL_2_REG_CLR 0x10E40094

/* SW_TOP_FUNC_SEL_2_REG_CLR.trg_shutdown_b_out - clears function_select bits of TRG_SHUTDOWN_B_OUT pad */
/* clears function_select bits of TRG_SHUTDOWN_B_OUT pad */
/* 0b000 : gnss_gpio[8] */
/* 0b001 : gn.trg_shutdown_b_out_mux0 */
/* 0b010 : clkc.trg_ref_clk_mux0 */
#define SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__SHIFT       0
#define SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__WIDTH       3
#define SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK        0x00000007
#define SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_2_REG_CLR__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_3_REG register */
#define SW_TOP_FUNC_SEL_3_REG_SET 0x10E40098

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_clk - sets function_select bits of SDIO2_CLK pad */
/* sets function_select bits of SDIO2_CLK pad */
/* 0b000 : sdio_gpio[0] */
/* 0b001 : sd2.sd_clk_2 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_cmd - sets function_select bits of SDIO2_CMD pad */
/* sets function_select bits of SDIO2_CMD pad */
/* 0b000 : sdio_gpio[1] */
/* 0b001 : sd2.sd_cmd_2 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_dat_0 - sets function_select bits of SDIO2_DAT_0 pad */
/* sets function_select bits of SDIO2_DAT_0 pad */
/* 0b000 : sdio_gpio[2] */
/* 0b001 : sd2.sd_dat_2_0 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_dat_1 - sets function_select bits of SDIO2_DAT_1 pad */
/* sets function_select bits of SDIO2_DAT_1 pad */
/* 0b000 : sdio_gpio[3] */
/* 0b001 : sd2.sd_dat_2_1 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_dat_2 - sets function_select bits of SDIO2_DAT_2 pad */
/* sets function_select bits of SDIO2_DAT_2 pad */
/* 0b000 : sdio_gpio[4] */
/* 0b001 : sd2.sd_dat_2_2 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_SET.sdio2_dat_3 - sets function_select bits of SDIO2_DAT_3 pad */
/* sets function_select bits of SDIO2_DAT_3 pad */
/* 0b000 : sdio_gpio[5] */
/* 0b001 : sd2.sd_dat_2_3 */
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_3_REG_SET__SDIO2_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_3_REG register */
#define SW_TOP_FUNC_SEL_3_REG_CLR 0x10E4009C

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_clk - clears function_select bits of SDIO2_CLK pad */
/* clears function_select bits of SDIO2_CLK pad */
/* 0b000 : sdio_gpio[0] */
/* 0b001 : sd2.sd_clk_2 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_cmd - clears function_select bits of SDIO2_CMD pad */
/* clears function_select bits of SDIO2_CMD pad */
/* 0b000 : sdio_gpio[1] */
/* 0b001 : sd2.sd_cmd_2 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_dat_0 - clears function_select bits of SDIO2_DAT_0 pad */
/* clears function_select bits of SDIO2_DAT_0 pad */
/* 0b000 : sdio_gpio[2] */
/* 0b001 : sd2.sd_dat_2_0 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_dat_1 - clears function_select bits of SDIO2_DAT_1 pad */
/* clears function_select bits of SDIO2_DAT_1 pad */
/* 0b000 : sdio_gpio[3] */
/* 0b001 : sd2.sd_dat_2_1 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_dat_2 - clears function_select bits of SDIO2_DAT_2 pad */
/* clears function_select bits of SDIO2_DAT_2 pad */
/* 0b000 : sdio_gpio[4] */
/* 0b001 : sd2.sd_dat_2_2 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_3_REG_CLR.sdio2_dat_3 - clears function_select bits of SDIO2_DAT_3 pad */
/* clears function_select bits of SDIO2_DAT_3 pad */
/* 0b000 : sdio_gpio[5] */
/* 0b001 : sd2.sd_dat_2_3 */
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_3_REG_CLR__SDIO2_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_4_REG register */
#define SW_TOP_FUNC_SEL_4_REG_SET 0x10E400A0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_7 - sets function_select bits of DF_AD_7 pad */
/* sets function_select bits of DF_AD_7 pad */
/* 0b000 : nand_gpio[7] */
/* 0b001 : nd.df_ad_7 */
/* 0b010 : sd0.sd_dat_0_7 */
/* 0b011 : sd1.sd_dat_1_7 */
/* 0b100 : sd1.sd_dat_1_3_mux1 */
/* 0b101 : ca.pio_9 */
/* 0b110 : au.func_dbg_dconv_7 */
/* 0b111 : gn.io_gnsssys_sw_cfg_7 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__SHIFT       0
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__MASK        0x00000007
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_6 - sets function_select bits of DF_AD_6 pad */
/* sets function_select bits of DF_AD_6 pad */
/* 0b000 : nand_gpio[6] */
/* 0b001 : nd.df_ad_6 */
/* 0b010 : sd0.sd_dat_0_6 */
/* 0b011 : sd1.sd_dat_1_6 */
/* 0b100 : sd1.sd_dat_1_2_mux1 */
/* 0b101 : ca.pio_8 */
/* 0b110 : au.func_dbg_dconv_6 */
/* 0b111 : gn.io_gnsssys_sw_cfg_6 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__SHIFT       4
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__MASK        0x00000070
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_5 - sets function_select bits of DF_AD_5 pad */
/* sets function_select bits of DF_AD_5 pad */
/* 0b000 : nand_gpio[5] */
/* 0b001 : nd.df_ad_5 */
/* 0b010 : sd0.sd_dat_0_5 */
/* 0b011 : sd1.sd_dat_1_5 */
/* 0b100 : sd1.sd_dat_1_1_mux1 */
/* 0b101 : ca.sdio_debug_cmd */
/* 0b110 : au.func_dbg_dconv_5 */
/* 0b111 : gn.io_gnsssys_sw_cfg_5 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__SHIFT       8
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__MASK        0x00000700
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_4 - sets function_select bits of DF_AD_4 pad */
/* sets function_select bits of DF_AD_4 pad */
/* 0b000 : nand_gpio[4] */
/* 0b001 : nd.df_ad_4 */
/* 0b010 : sd0.sd_dat_0_4 */
/* 0b011 : sd1.sd_dat_1_4 */
/* 0b100 : sd1.sd_dat_1_0_mux1 */
/* 0b101 : ca.sdio_debug_clk */
/* 0b110 : au.func_dbg_dconv_4 */
/* 0b111 : gn.io_gnsssys_sw_cfg_4 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__SHIFT       12
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__MASK        0x00007000
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_3 - sets function_select bits of DF_AD_3 pad */
/* sets function_select bits of DF_AD_3 pad */
/* 0b000 : nand_gpio[3] */
/* 0b001 : nd.df_ad_3 */
/* 0b010 : sd0.sd_dat_0_3 */
/* 0b011 : sd1.sd_dat_1_3_mux0 */
/* 0b101 : ca.sdio_debug_data_3 */
/* 0b110 : au.func_dbg_dconv_3 */
/* 0b111 : gn.io_gnsssys_sw_cfg_3 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__SHIFT       16
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__MASK        0x00070000
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_2 - sets function_select bits of DF_AD_2 pad */
/* sets function_select bits of DF_AD_2 pad */
/* 0b000 : nand_gpio[2] */
/* 0b001 : nd.df_ad_2 */
/* 0b010 : sd0.sd_dat_0_2 */
/* 0b011 : sd1.sd_dat_1_2_mux0 */
/* 0b101 : ca.sdio_debug_data_2 */
/* 0b110 : au.func_dbg_dconv_2 */
/* 0b111 : gn.io_gnsssys_sw_cfg_2 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__SHIFT       20
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__MASK        0x00700000
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_1 - sets function_select bits of DF_AD_1 pad */
/* sets function_select bits of DF_AD_1 pad */
/* 0b000 : nand_gpio[1] */
/* 0b001 : nd.df_ad_1 */
/* 0b010 : sd0.sd_dat_0_1 */
/* 0b011 : sd1.sd_dat_1_1_mux0 */
/* 0b101 : ca.sdio_debug_data_1 */
/* 0b110 : au.func_dbg_dconv_1 */
/* 0b111 : gn.io_gnsssys_sw_cfg_1 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__SHIFT       24
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__MASK        0x07000000
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_SET.df_ad_0 - sets function_select bits of DF_AD_0 pad */
/* sets function_select bits of DF_AD_0 pad */
/* 0b000 : nand_gpio[0] */
/* 0b001 : nd.df_ad_0 */
/* 0b010 : sd0.sd_dat_0_0 */
/* 0b011 : sd1.sd_dat_1_0_mux0 */
/* 0b101 : ca.sdio_debug_data_0 */
/* 0b110 : au.func_dbg_dconv_0 */
/* 0b111 : gn.io_gnsssys_sw_cfg_0 */
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__SHIFT       28
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__MASK        0x70000000
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_4_REG_SET__DF_AD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_4_REG register */
#define SW_TOP_FUNC_SEL_4_REG_CLR 0x10E400A4

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_7 - clears function_select bits of DF_AD_7 pad */
/* clears function_select bits of DF_AD_7 pad */
/* 0b000 : nand_gpio[7] */
/* 0b001 : nd.df_ad_7 */
/* 0b010 : sd0.sd_dat_0_7 */
/* 0b011 : sd1.sd_dat_1_7 */
/* 0b100 : sd1.sd_dat_1_3_mux1 */
/* 0b101 : ca.pio_9 */
/* 0b110 : au.func_dbg_dconv_7 */
/* 0b111 : gn.io_gnsssys_sw_cfg_7 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__SHIFT       0
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__MASK        0x00000007
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_6 - clears function_select bits of DF_AD_6 pad */
/* clears function_select bits of DF_AD_6 pad */
/* 0b000 : nand_gpio[6] */
/* 0b001 : nd.df_ad_6 */
/* 0b010 : sd0.sd_dat_0_6 */
/* 0b011 : sd1.sd_dat_1_6 */
/* 0b100 : sd1.sd_dat_1_2_mux1 */
/* 0b101 : ca.pio_8 */
/* 0b110 : au.func_dbg_dconv_6 */
/* 0b111 : gn.io_gnsssys_sw_cfg_6 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__SHIFT       4
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__MASK        0x00000070
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_5 - clears function_select bits of DF_AD_5 pad */
/* clears function_select bits of DF_AD_5 pad */
/* 0b000 : nand_gpio[5] */
/* 0b001 : nd.df_ad_5 */
/* 0b010 : sd0.sd_dat_0_5 */
/* 0b011 : sd1.sd_dat_1_5 */
/* 0b100 : sd1.sd_dat_1_1_mux1 */
/* 0b101 : ca.sdio_debug_cmd */
/* 0b110 : au.func_dbg_dconv_5 */
/* 0b111 : gn.io_gnsssys_sw_cfg_5 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__SHIFT       8
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__MASK        0x00000700
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_4 - clears function_select bits of DF_AD_4 pad */
/* clears function_select bits of DF_AD_4 pad */
/* 0b000 : nand_gpio[4] */
/* 0b001 : nd.df_ad_4 */
/* 0b010 : sd0.sd_dat_0_4 */
/* 0b011 : sd1.sd_dat_1_4 */
/* 0b100 : sd1.sd_dat_1_0_mux1 */
/* 0b101 : ca.sdio_debug_clk */
/* 0b110 : au.func_dbg_dconv_4 */
/* 0b111 : gn.io_gnsssys_sw_cfg_4 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__SHIFT       12
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__MASK        0x00007000
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_3 - clears function_select bits of DF_AD_3 pad */
/* clears function_select bits of DF_AD_3 pad */
/* 0b000 : nand_gpio[3] */
/* 0b001 : nd.df_ad_3 */
/* 0b010 : sd0.sd_dat_0_3 */
/* 0b011 : sd1.sd_dat_1_3_mux0 */
/* 0b101 : ca.sdio_debug_data_3 */
/* 0b110 : au.func_dbg_dconv_3 */
/* 0b111 : gn.io_gnsssys_sw_cfg_3 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__SHIFT       16
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__MASK        0x00070000
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_2 - clears function_select bits of DF_AD_2 pad */
/* clears function_select bits of DF_AD_2 pad */
/* 0b000 : nand_gpio[2] */
/* 0b001 : nd.df_ad_2 */
/* 0b010 : sd0.sd_dat_0_2 */
/* 0b011 : sd1.sd_dat_1_2_mux0 */
/* 0b101 : ca.sdio_debug_data_2 */
/* 0b110 : au.func_dbg_dconv_2 */
/* 0b111 : gn.io_gnsssys_sw_cfg_2 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__SHIFT       20
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__MASK        0x00700000
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_1 - clears function_select bits of DF_AD_1 pad */
/* clears function_select bits of DF_AD_1 pad */
/* 0b000 : nand_gpio[1] */
/* 0b001 : nd.df_ad_1 */
/* 0b010 : sd0.sd_dat_0_1 */
/* 0b011 : sd1.sd_dat_1_1_mux0 */
/* 0b101 : ca.sdio_debug_data_1 */
/* 0b110 : au.func_dbg_dconv_1 */
/* 0b111 : gn.io_gnsssys_sw_cfg_1 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__SHIFT       24
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__MASK        0x07000000
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_4_REG_CLR.df_ad_0 - clears function_select bits of DF_AD_0 pad */
/* clears function_select bits of DF_AD_0 pad */
/* 0b000 : nand_gpio[0] */
/* 0b001 : nd.df_ad_0 */
/* 0b010 : sd0.sd_dat_0_0 */
/* 0b011 : sd1.sd_dat_1_0_mux0 */
/* 0b101 : ca.sdio_debug_data_0 */
/* 0b110 : au.func_dbg_dconv_0 */
/* 0b111 : gn.io_gnsssys_sw_cfg_0 */
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__SHIFT       28
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__MASK        0x70000000
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_4_REG_CLR__DF_AD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_5_REG register */
#define SW_TOP_FUNC_SEL_5_REG_SET 0x10E400A8

/* SW_TOP_FUNC_SEL_5_REG_SET.df_dqs - sets function_select bits of DF_DQS pad */
/* sets function_select bits of DF_DQS pad */
/* 0b000 : nand_gpio[15] */
/* 0b001 : nd.df_dqs */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__SHIFT       0
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__MASK        0x00000007
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_DQS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_cle - sets function_select bits of DF_CLE pad */
/* sets function_select bits of DF_CLE pad */
/* 0b000 : nand_gpio[8] */
/* 0b001 : nd.df_cle */
/* 0b010 : sd0.sd_clk_0 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__SHIFT       4
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__MASK        0x00000070
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CLE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_ale - sets function_select bits of DF_ALE pad */
/* sets function_select bits of DF_ALE pad */
/* 0b000 : nand_gpio[9] */
/* 0b001 : nd.df_ale */
/* 0b010 : sd0.sd_cmd_0 */
/* 0b101 : ca.pio_10 */
/* 0b110 : clkc.tst_clk_mux0 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__SHIFT       8
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__MASK        0x00000700
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_ALE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_we_b - sets function_select bits of DF_WE_B pad */
/* sets function_select bits of DF_WE_B pad */
/* 0b000 : nand_gpio[10] */
/* 0b001 : nd.df_we_b */
/* 0b011 : sd1.sd_clk_1 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__SHIFT       12
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__MASK        0x00007000
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_WE_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_re_b - sets function_select bits of DF_RE_B pad */
/* sets function_select bits of DF_RE_B pad */
/* 0b000 : nand_gpio[11] */
/* 0b001 : nd.df_re_b */
/* 0b011 : sd1.sd_cmd_1 */
/* 0b101 : ca.pio_11 */
/* 0b110 : au.func_dbg_keycomp_out_0 */
/* 0b111 : gn.io_gnsssys_sw_cfg_8 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__SHIFT       16
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__MASK        0x00070000
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RE_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_ry_by - sets function_select bits of DF_RY_BY pad */
/* sets function_select bits of DF_RY_BY pad */
/* 0b000 : nand_gpio[12] */
/* 0b001 : nd.df_ry_by */
/* 0b101 : ca.pio_12 */
/* 0b110 : au.func_dbg_keycomp_out_1 */
/* 0b111 : gn.io_gnsssys_sw_cfg_9 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__SHIFT       20
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__MASK        0x00700000
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_RY_BY__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_cs_b_1 - sets function_select bits of DF_CS_B_1 pad */
/* sets function_select bits of DF_CS_B_1 pad */
/* 0b000 : nand_gpio[14] */
/* 0b001 : nd.df_cs_b_1 */
/* 0b010 : usb0.drvvbus_mux0 */
/* 0b011 : au.digmic_mux0 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__SHIFT       24
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__MASK        0x07000000
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_SET.df_cs_b_0 - sets function_select bits of DF_CS_B_0 pad */
/* sets function_select bits of DF_CS_B_0 pad */
/* 0b000 : nand_gpio[13] */
/* 0b001 : nd.df_cs_b_0 */
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__SHIFT       28
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__MASK        0x70000000
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_5_REG_SET__DF_CS_B_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_5_REG register */
#define SW_TOP_FUNC_SEL_5_REG_CLR 0x10E400AC

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_dqs - clears function_select bits of DF_DQS pad */
/* clears function_select bits of DF_DQS pad */
/* 0b000 : nand_gpio[15] */
/* 0b001 : nd.df_dqs */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__SHIFT       0
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__MASK        0x00000007
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_DQS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_cle - clears function_select bits of DF_CLE pad */
/* clears function_select bits of DF_CLE pad */
/* 0b000 : nand_gpio[8] */
/* 0b001 : nd.df_cle */
/* 0b010 : sd0.sd_clk_0 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__SHIFT       4
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__MASK        0x00000070
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CLE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_ale - clears function_select bits of DF_ALE pad */
/* clears function_select bits of DF_ALE pad */
/* 0b000 : nand_gpio[9] */
/* 0b001 : nd.df_ale */
/* 0b010 : sd0.sd_cmd_0 */
/* 0b101 : ca.pio_10 */
/* 0b110 : clkc.tst_clk_mux0 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__SHIFT       8
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__MASK        0x00000700
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_ALE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_we_b - clears function_select bits of DF_WE_B pad */
/* clears function_select bits of DF_WE_B pad */
/* 0b000 : nand_gpio[10] */
/* 0b001 : nd.df_we_b */
/* 0b011 : sd1.sd_clk_1 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__SHIFT       12
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__MASK        0x00007000
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_WE_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_re_b - clears function_select bits of DF_RE_B pad */
/* clears function_select bits of DF_RE_B pad */
/* 0b000 : nand_gpio[11] */
/* 0b001 : nd.df_re_b */
/* 0b011 : sd1.sd_cmd_1 */
/* 0b101 : ca.pio_11 */
/* 0b110 : au.func_dbg_keycomp_out_0 */
/* 0b111 : gn.io_gnsssys_sw_cfg_8 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__SHIFT       16
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__MASK        0x00070000
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RE_B__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_ry_by - clears function_select bits of DF_RY_BY pad */
/* clears function_select bits of DF_RY_BY pad */
/* 0b000 : nand_gpio[12] */
/* 0b001 : nd.df_ry_by */
/* 0b101 : ca.pio_12 */
/* 0b110 : au.func_dbg_keycomp_out_1 */
/* 0b111 : gn.io_gnsssys_sw_cfg_9 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__SHIFT       20
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__MASK        0x00700000
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_RY_BY__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_cs_b_1 - clears function_select bits of DF_CS_B_1 pad */
/* clears function_select bits of DF_CS_B_1 pad */
/* 0b000 : nand_gpio[14] */
/* 0b001 : nd.df_cs_b_1 */
/* 0b010 : usb0.drvvbus_mux0 */
/* 0b011 : au.digmic_mux0 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__SHIFT       24
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__MASK        0x07000000
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_5_REG_CLR.df_cs_b_0 - clears function_select bits of DF_CS_B_0 pad */
/* clears function_select bits of DF_CS_B_0 pad */
/* 0b000 : nand_gpio[13] */
/* 0b001 : nd.df_cs_b_0 */
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__SHIFT       28
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__WIDTH       3
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__MASK        0x70000000
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_5_REG_CLR__DF_CS_B_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_6_REG register */
#define SW_TOP_FUNC_SEL_6_REG_SET 0x10E400B0

/* SW_TOP_FUNC_SEL_6_REG_SET.l_pclk - sets function_select bits of L_PCLK pad */
/* sets function_select bits of L_PCLK pad */
/* 0b000 : lcd_gpio[0] */
/* 0b001 : ld.l_pclk */
/* 0b010 : lr.lcdrom_frdy */
/* 0b011 : cvbs.cvbsafe_debug_clampup0 */
/* 0b100 : rg.rgmii_phy_ref_clk_mux1 */
/* 0b101 : tpiu.traceclk */
/* 0b110 : visbus.dout_16 */
/* 0b111 : gn.gnss_swclktck */
#define SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_6_REG_SET__L_PCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.l_lck - sets function_select bits of L_LCK pad */
/* sets function_select bits of L_LCK pad */
/* 0b000 : lcd_gpio[1] */
/* 0b001 : ld.l_lck */
/* 0b010 : lr.lcdrom_fce_b */
/* 0b011 : cvbs.cvbsafe_debug_clampdn0 */
/* 0b100 : ca.pio_13 */
/* 0b101 : clkc.tst_clk_mux1 */
/* 0b110 : visbus.dout_17 */
/* 0b111 : gn.gnss_rst_32k_b */
#define SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__SHIFT       4
#define SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_6_REG_SET__L_LCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.l_fck - sets function_select bits of L_FCK pad */
/* sets function_select bits of L_FCK pad */
/* 0b000 : lcd_gpio[2] */
/* 0b001 : ld.l_fck */
/* 0b010 : lr.lcdrom_fwe_b */
/* 0b011 : cvbs.cvbsafe_debug_ext_clk26 */
/* 0b100 : ca.pio_14 */
/* 0b110 : visbus.dout_18 */
/* 0b111 : gn.gnss_swdiotms */
#define SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__SHIFT       8
#define SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_6_REG_SET__L_FCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.l_de - sets function_select bits of L_DE pad */
/* sets function_select bits of L_DE pad */
/* 0b000 : lcd_gpio[3] */
/* 0b001 : ld.l_de */
/* 0b010 : lr.lcdrom_foe_b */
/* 0b011 : cvbs.cvbsafe_debug_testclk */
/* 0b100 : ca.pio_15 */
/* 0b101 : tpiu.tracectl */
/* 0b110 : visbus.dout_19 */
/* 0b111 : gn.gnss_m0_porst_b_mux0 */
#define SW_TOP_FUNC_SEL_6_REG_SET__L_DE__SHIFT       12
#define SW_TOP_FUNC_SEL_6_REG_SET__L_DE__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__L_DE__MASK        0x00007000
#define SW_TOP_FUNC_SEL_6_REG_SET__L_DE__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_6_REG_SET__L_DE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.ldd_0 - sets function_select bits of LDD_0 pad */
/* sets function_select bits of LDD_0 pad */
/* 0b000 : lcd_gpio[4] */
/* 0b001 : ld.ldd_0 */
/* 0b010 : lr.lcdrom_fd_0 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_0 */
/* 0b100 : ca.curator_lpc_func_clk */
/* 0b101 : tpiu.tracedata_0 */
/* 0b110 : visbus.dout_0 */
/* 0b111 : gn.gnss_sw_status_0 */
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__SHIFT       16
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__MASK        0x00070000
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.ldd_1 - sets function_select bits of LDD_1 pad */
/* sets function_select bits of LDD_1 pad */
/* 0b000 : lcd_gpio[5] */
/* 0b001 : ld.ldd_1 */
/* 0b010 : lr.lcdrom_fd_1 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_1 */
/* 0b100 : ca.curator_lpc_func_csb */
/* 0b101 : tpiu.tracedata_1 */
/* 0b110 : visbus.dout_1 */
/* 0b111 : gn.gnss_sw_status_1 */
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__SHIFT       20
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__MASK        0x00700000
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.ldd_2 - sets function_select bits of LDD_2 pad */
/* sets function_select bits of LDD_2 pad */
/* 0b000 : lcd_gpio[6] */
/* 0b001 : ld.ldd_2 */
/* 0b010 : lr.lcdrom_fd_2 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_2 */
/* 0b100 : ca.curator_lpc_func_data_0 */
/* 0b101 : tpiu.tracedata_2 */
/* 0b110 : visbus.dout_2 */
/* 0b111 : gn.gnss_sw_status_2 */
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__SHIFT       24
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__MASK        0x07000000
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_SET.ldd_3 - sets function_select bits of LDD_3 pad */
/* sets function_select bits of LDD_3 pad */
/* 0b000 : lcd_gpio[7] */
/* 0b001 : ld.ldd_3 */
/* 0b010 : lr.lcdrom_fd_3 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_3 */
/* 0b100 : ca.curator_lpc_func_data_1 */
/* 0b101 : tpiu.tracedata_3 */
/* 0b110 : visbus.dout_3 */
/* 0b111 : gn.gnss_sw_status_3 */
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__SHIFT       28
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__MASK        0x70000000
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_6_REG_SET__LDD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_6_REG register */
#define SW_TOP_FUNC_SEL_6_REG_CLR 0x10E400B4

/* SW_TOP_FUNC_SEL_6_REG_CLR.l_pclk - clears function_select bits of L_PCLK pad */
/* clears function_select bits of L_PCLK pad */
/* 0b000 : lcd_gpio[0] */
/* 0b001 : ld.l_pclk */
/* 0b010 : lr.lcdrom_frdy */
/* 0b011 : cvbs.cvbsafe_debug_clampup0 */
/* 0b100 : rg.rgmii_phy_ref_clk_mux1 */
/* 0b101 : tpiu.traceclk */
/* 0b110 : visbus.dout_16 */
/* 0b111 : gn.gnss_swclktck */
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_PCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.l_lck - clears function_select bits of L_LCK pad */
/* clears function_select bits of L_LCK pad */
/* 0b000 : lcd_gpio[1] */
/* 0b001 : ld.l_lck */
/* 0b010 : lr.lcdrom_fce_b */
/* 0b011 : cvbs.cvbsafe_debug_clampdn0 */
/* 0b100 : ca.pio_13 */
/* 0b101 : clkc.tst_clk_mux1 */
/* 0b110 : visbus.dout_17 */
/* 0b111 : gn.gnss_rst_32k_b */
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__SHIFT       4
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_LCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.l_fck - clears function_select bits of L_FCK pad */
/* clears function_select bits of L_FCK pad */
/* 0b000 : lcd_gpio[2] */
/* 0b001 : ld.l_fck */
/* 0b010 : lr.lcdrom_fwe_b */
/* 0b011 : cvbs.cvbsafe_debug_ext_clk26 */
/* 0b100 : ca.pio_14 */
/* 0b110 : visbus.dout_18 */
/* 0b111 : gn.gnss_swdiotms */
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__SHIFT       8
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_FCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.l_de - clears function_select bits of L_DE pad */
/* clears function_select bits of L_DE pad */
/* 0b000 : lcd_gpio[3] */
/* 0b001 : ld.l_de */
/* 0b010 : lr.lcdrom_foe_b */
/* 0b011 : cvbs.cvbsafe_debug_testclk */
/* 0b100 : ca.pio_15 */
/* 0b101 : tpiu.tracectl */
/* 0b110 : visbus.dout_19 */
/* 0b111 : gn.gnss_m0_porst_b_mux0 */
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__SHIFT       12
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__MASK        0x00007000
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_6_REG_CLR__L_DE__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.ldd_0 - clears function_select bits of LDD_0 pad */
/* clears function_select bits of LDD_0 pad */
/* 0b000 : lcd_gpio[4] */
/* 0b001 : ld.ldd_0 */
/* 0b010 : lr.lcdrom_fd_0 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_0 */
/* 0b100 : ca.curator_lpc_func_clk */
/* 0b101 : tpiu.tracedata_0 */
/* 0b110 : visbus.dout_0 */
/* 0b111 : gn.gnss_sw_status_0 */
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__SHIFT       16
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__MASK        0x00070000
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.ldd_1 - clears function_select bits of LDD_1 pad */
/* clears function_select bits of LDD_1 pad */
/* 0b000 : lcd_gpio[5] */
/* 0b001 : ld.ldd_1 */
/* 0b010 : lr.lcdrom_fd_1 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_1 */
/* 0b100 : ca.curator_lpc_func_csb */
/* 0b101 : tpiu.tracedata_1 */
/* 0b110 : visbus.dout_1 */
/* 0b111 : gn.gnss_sw_status_1 */
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__SHIFT       20
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__MASK        0x00700000
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.ldd_2 - clears function_select bits of LDD_2 pad */
/* clears function_select bits of LDD_2 pad */
/* 0b000 : lcd_gpio[6] */
/* 0b001 : ld.ldd_2 */
/* 0b010 : lr.lcdrom_fd_2 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_2 */
/* 0b100 : ca.curator_lpc_func_data_0 */
/* 0b101 : tpiu.tracedata_2 */
/* 0b110 : visbus.dout_2 */
/* 0b111 : gn.gnss_sw_status_2 */
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__SHIFT       24
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__MASK        0x07000000
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_6_REG_CLR.ldd_3 - clears function_select bits of LDD_3 pad */
/* clears function_select bits of LDD_3 pad */
/* 0b000 : lcd_gpio[7] */
/* 0b001 : ld.ldd_3 */
/* 0b010 : lr.lcdrom_fd_3 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_3 */
/* 0b100 : ca.curator_lpc_func_data_1 */
/* 0b101 : tpiu.tracedata_3 */
/* 0b110 : visbus.dout_3 */
/* 0b111 : gn.gnss_sw_status_3 */
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__SHIFT       28
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__MASK        0x70000000
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_6_REG_CLR__LDD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_7_REG register */
#define SW_TOP_FUNC_SEL_7_REG_SET 0x10E400B8

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_4 - sets function_select bits of LDD_4 pad */
/* sets function_select bits of LDD_4 pad */
/* 0b000 : lcd_gpio[8] */
/* 0b001 : ld.ldd_4 */
/* 0b010 : lr.lcdrom_fd_4 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_4 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux0 */
/* 0b101 : tpiu.tracedata_4 */
/* 0b110 : visbus.dout_4 */
/* 0b111 : gn.gnss_sw_status_4 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__SHIFT       0
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__MASK        0x00000007
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_5 - sets function_select bits of LDD_5 pad */
/* sets function_select bits of LDD_5 pad */
/* 0b000 : lcd_gpio[9] */
/* 0b001 : ld.ldd_5 */
/* 0b010 : lr.lcdrom_fd_5 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_5 */
/* 0b100 : ca.audio_lpc_func_clk */
/* 0b101 : tpiu.tracedata_5 */
/* 0b110 : visbus.dout_5 */
/* 0b111 : gn.gnss_sw_status_5 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__SHIFT       4
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__MASK        0x00000070
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_6 - sets function_select bits of LDD_6 pad */
/* sets function_select bits of LDD_6 pad */
/* 0b000 : lcd_gpio[10] */
/* 0b001 : ld.ldd_6 */
/* 0b010 : lr.lcdrom_fd_6 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_6 */
/* 0b100 : ca.audio_lpc_func_csb */
/* 0b101 : tpiu.tracedata_6 */
/* 0b110 : visbus.dout_6 */
/* 0b111 : gn.gnss_sw_status_6 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__SHIFT       8
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__MASK        0x00000700
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_7 - sets function_select bits of LDD_7 pad */
/* sets function_select bits of LDD_7 pad */
/* 0b000 : lcd_gpio[11] */
/* 0b001 : ld.ldd_7 */
/* 0b010 : lr.lcdrom_fd_7 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_7 */
/* 0b100 : ca.audio_lpc_func_data_0 */
/* 0b101 : tpiu.tracedata_7 */
/* 0b110 : visbus.dout_7 */
/* 0b111 : gn.gnss_sw_status_7 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__SHIFT       12
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__MASK        0x00007000
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_8 - sets function_select bits of LDD_8 pad */
/* sets function_select bits of LDD_8 pad */
/* 0b000 : lcd_gpio[12] */
/* 0b001 : ld.ldd_8 */
/* 0b010 : lr.lcdrom_fd_8 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_8 */
/* 0b100 : ca.audio_lpc_func_data_1 */
/* 0b101 : tpiu.tracedata_8 */
/* 0b110 : visbus.dout_8 */
/* 0b111 : gn.gnss_sw_status_8 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__SHIFT       16
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__MASK        0x00070000
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_8__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_9 - sets function_select bits of LDD_9 pad */
/* sets function_select bits of LDD_9 pad */
/* 0b000 : lcd_gpio[13] */
/* 0b001 : ld.ldd_9 */
/* 0b010 : lr.lcdrom_fd_9 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_9 */
/* 0b100 : ca.audio_lpc_func_data_2 */
/* 0b101 : tpiu.tracedata_9 */
/* 0b110 : visbus.dout_9 */
/* 0b111 : gn.gnss_sw_status_9 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__SHIFT       20
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__MASK        0x00700000
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_9__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_10 - sets function_select bits of LDD_10 pad */
/* sets function_select bits of LDD_10 pad */
/* 0b000 : lcd_gpio[14] */
/* 0b001 : ld.ldd_10 */
/* 0b010 : lr.lcdrom_fd_10 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_10 */
/* 0b100 : ca.audio_lpc_func_data_3 */
/* 0b101 : tpiu.tracedata_10 */
/* 0b110 : visbus.dout_10 */
/* 0b111 : gn.gnss_sw_status_10 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__SHIFT       24
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__MASK        0x07000000
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_10__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_SET.ldd_11 - sets function_select bits of LDD_11 pad */
/* sets function_select bits of LDD_11 pad */
/* 0b000 : lcd_gpio[15] */
/* 0b001 : ld.ldd_11 */
/* 0b010 : lr.lcdrom_fd_11 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_11 */
/* 0b100 : ca.audio_lpc_func_data_4 */
/* 0b101 : tpiu.tracedata_11 */
/* 0b110 : visbus.dout_11 */
/* 0b111 : gn.gnss_sw_status_11 */
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__SHIFT       28
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__MASK        0x70000000
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_7_REG_SET__LDD_11__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_7_REG register */
#define SW_TOP_FUNC_SEL_7_REG_CLR 0x10E400BC

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_4 - clears function_select bits of LDD_4 pad */
/* clears function_select bits of LDD_4 pad */
/* 0b000 : lcd_gpio[8] */
/* 0b001 : ld.ldd_4 */
/* 0b010 : lr.lcdrom_fd_4 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_4 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux0 */
/* 0b101 : tpiu.tracedata_4 */
/* 0b110 : visbus.dout_4 */
/* 0b111 : gn.gnss_sw_status_4 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__SHIFT       0
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__MASK        0x00000007
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_5 - clears function_select bits of LDD_5 pad */
/* clears function_select bits of LDD_5 pad */
/* 0b000 : lcd_gpio[9] */
/* 0b001 : ld.ldd_5 */
/* 0b010 : lr.lcdrom_fd_5 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_5 */
/* 0b100 : ca.audio_lpc_func_clk */
/* 0b101 : tpiu.tracedata_5 */
/* 0b110 : visbus.dout_5 */
/* 0b111 : gn.gnss_sw_status_5 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__SHIFT       4
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__MASK        0x00000070
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_6 - clears function_select bits of LDD_6 pad */
/* clears function_select bits of LDD_6 pad */
/* 0b000 : lcd_gpio[10] */
/* 0b001 : ld.ldd_6 */
/* 0b010 : lr.lcdrom_fd_6 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_6 */
/* 0b100 : ca.audio_lpc_func_csb */
/* 0b101 : tpiu.tracedata_6 */
/* 0b110 : visbus.dout_6 */
/* 0b111 : gn.gnss_sw_status_6 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__SHIFT       8
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__MASK        0x00000700
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_7 - clears function_select bits of LDD_7 pad */
/* clears function_select bits of LDD_7 pad */
/* 0b000 : lcd_gpio[11] */
/* 0b001 : ld.ldd_7 */
/* 0b010 : lr.lcdrom_fd_7 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_7 */
/* 0b100 : ca.audio_lpc_func_data_0 */
/* 0b101 : tpiu.tracedata_7 */
/* 0b110 : visbus.dout_7 */
/* 0b111 : gn.gnss_sw_status_7 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__SHIFT       12
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__MASK        0x00007000
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_8 - clears function_select bits of LDD_8 pad */
/* clears function_select bits of LDD_8 pad */
/* 0b000 : lcd_gpio[12] */
/* 0b001 : ld.ldd_8 */
/* 0b010 : lr.lcdrom_fd_8 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_8 */
/* 0b100 : ca.audio_lpc_func_data_1 */
/* 0b101 : tpiu.tracedata_8 */
/* 0b110 : visbus.dout_8 */
/* 0b111 : gn.gnss_sw_status_8 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__SHIFT       16
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__MASK        0x00070000
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_8__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_9 - clears function_select bits of LDD_9 pad */
/* clears function_select bits of LDD_9 pad */
/* 0b000 : lcd_gpio[13] */
/* 0b001 : ld.ldd_9 */
/* 0b010 : lr.lcdrom_fd_9 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_9 */
/* 0b100 : ca.audio_lpc_func_data_2 */
/* 0b101 : tpiu.tracedata_9 */
/* 0b110 : visbus.dout_9 */
/* 0b111 : gn.gnss_sw_status_9 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__SHIFT       20
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__MASK        0x00700000
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_9__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_10 - clears function_select bits of LDD_10 pad */
/* clears function_select bits of LDD_10 pad */
/* 0b000 : lcd_gpio[14] */
/* 0b001 : ld.ldd_10 */
/* 0b010 : lr.lcdrom_fd_10 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_10 */
/* 0b100 : ca.audio_lpc_func_data_3 */
/* 0b101 : tpiu.tracedata_10 */
/* 0b110 : visbus.dout_10 */
/* 0b111 : gn.gnss_sw_status_10 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__SHIFT       24
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__MASK        0x07000000
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_10__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_7_REG_CLR.ldd_11 - clears function_select bits of LDD_11 pad */
/* clears function_select bits of LDD_11 pad */
/* 0b000 : lcd_gpio[15] */
/* 0b001 : ld.ldd_11 */
/* 0b010 : lr.lcdrom_fd_11 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_11 */
/* 0b100 : ca.audio_lpc_func_data_4 */
/* 0b101 : tpiu.tracedata_11 */
/* 0b110 : visbus.dout_11 */
/* 0b111 : gn.gnss_sw_status_11 */
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__SHIFT       28
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__WIDTH       3
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__MASK        0x70000000
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_7_REG_CLR__LDD_11__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_8_REG register */
#define SW_TOP_FUNC_SEL_8_REG_SET 0x10E400C0

/* SW_TOP_FUNC_SEL_8_REG_SET.ldd_12 - sets function_select bits of LDD_12 pad */
/* sets function_select bits of LDD_12 pad */
/* 0b000 : lcd_gpio[16] */
/* 0b001 : ld.ldd_12 */
/* 0b010 : lr.lcdrom_fd_12 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_12 */
/* 0b100 : ca.audio_lpc_func_data_5 */
/* 0b101 : tpiu.tracedata_12 */
/* 0b110 : visbus.dout_12 */
/* 0b111 : gn.gnss_sw_status_12 */
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__SHIFT       0
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__MASK        0x00000007
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_12__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.ldd_13 - sets function_select bits of LDD_13 pad */
/* sets function_select bits of LDD_13 pad */
/* 0b000 : lcd_gpio[17] */
/* 0b001 : ld.ldd_13 */
/* 0b010 : lr.lcdrom_fd_13 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_13 */
/* 0b100 : ca.audio_lpc_func_data_6 */
/* 0b101 : tpiu.tracedata_13 */
/* 0b110 : visbus.dout_13 */
/* 0b111 : gn.gnss_sw_status_13 */
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__SHIFT       4
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__MASK        0x00000070
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_13__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.ldd_14 - sets function_select bits of LDD_14 pad */
/* sets function_select bits of LDD_14 pad */
/* 0b000 : lcd_gpio[18] */
/* 0b001 : ld.ldd_14 */
/* 0b010 : lr.lcdrom_fd_14 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_14 */
/* 0b100 : ca.audio_lpc_func_data_7 */
/* 0b101 : tpiu.tracedata_14 */
/* 0b110 : visbus.dout_14 */
/* 0b111 : gn.gnss_sw_status_14 */
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__SHIFT       8
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__MASK        0x00000700
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_14__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.ldd_15 - sets function_select bits of LDD_15 pad */
/* sets function_select bits of LDD_15 pad */
/* 0b000 : lcd_gpio[19] */
/* 0b001 : ld.ldd_15 */
/* 0b010 : lr.lcdrom_fd_15 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_15 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux0 */
/* 0b101 : tpiu.tracedata_15 */
/* 0b110 : visbus.dout_15 */
/* 0b111 : gn.gnss_sw_status_15 */
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__SHIFT       12
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__MASK        0x00007000
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_8_REG_SET__LDD_15__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_SET.lcd_gpio_20 - sets function_select bits of LCD_GPIO_20 pad */
/* sets function_select bits of LCD_GPIO_20 pad */
/* 0b000 : lcd_gpio[20] */
/* 0b010 : lr.lcdrom_fa_1 */
/* 0b011 : cvbs.cvbsafe_debug_data_valid0 */
/* 0b100 : pw.pwm3_mux1 */
#define SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__SHIFT       16
#define SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__MASK        0x00070000
#define SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_8_REG_SET__LCD_GPIO_20__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_8_REG register */
#define SW_TOP_FUNC_SEL_8_REG_CLR 0x10E400C4

/* SW_TOP_FUNC_SEL_8_REG_CLR.ldd_12 - clears function_select bits of LDD_12 pad */
/* clears function_select bits of LDD_12 pad */
/* 0b000 : lcd_gpio[16] */
/* 0b001 : ld.ldd_12 */
/* 0b010 : lr.lcdrom_fd_12 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_12 */
/* 0b100 : ca.audio_lpc_func_data_5 */
/* 0b101 : tpiu.tracedata_12 */
/* 0b110 : visbus.dout_12 */
/* 0b111 : gn.gnss_sw_status_12 */
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__SHIFT       0
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__MASK        0x00000007
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_12__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.ldd_13 - clears function_select bits of LDD_13 pad */
/* clears function_select bits of LDD_13 pad */
/* 0b000 : lcd_gpio[17] */
/* 0b001 : ld.ldd_13 */
/* 0b010 : lr.lcdrom_fd_13 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_13 */
/* 0b100 : ca.audio_lpc_func_data_6 */
/* 0b101 : tpiu.tracedata_13 */
/* 0b110 : visbus.dout_13 */
/* 0b111 : gn.gnss_sw_status_13 */
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__SHIFT       4
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__MASK        0x00000070
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_13__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.ldd_14 - clears function_select bits of LDD_14 pad */
/* clears function_select bits of LDD_14 pad */
/* 0b000 : lcd_gpio[18] */
/* 0b001 : ld.ldd_14 */
/* 0b010 : lr.lcdrom_fd_14 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_14 */
/* 0b100 : ca.audio_lpc_func_data_7 */
/* 0b101 : tpiu.tracedata_14 */
/* 0b110 : visbus.dout_14 */
/* 0b111 : gn.gnss_sw_status_14 */
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__SHIFT       8
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__MASK        0x00000700
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_14__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.ldd_15 - clears function_select bits of LDD_15 pad */
/* clears function_select bits of LDD_15 pad */
/* 0b000 : lcd_gpio[19] */
/* 0b001 : ld.ldd_15 */
/* 0b010 : lr.lcdrom_fd_15 */
/* 0b011 : cvbs.cvbsafe_debug_test_mux_out_15 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux0 */
/* 0b101 : tpiu.tracedata_15 */
/* 0b110 : visbus.dout_15 */
/* 0b111 : gn.gnss_sw_status_15 */
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__SHIFT       12
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__MASK        0x00007000
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_8_REG_CLR__LDD_15__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_8_REG_CLR.lcd_gpio_20 - clears function_select bits of LCD_GPIO_20 pad */
/* clears function_select bits of LCD_GPIO_20 pad */
/* 0b000 : lcd_gpio[20] */
/* 0b010 : lr.lcdrom_fa_1 */
/* 0b011 : cvbs.cvbsafe_debug_data_valid0 */
/* 0b100 : pw.pwm3_mux1 */
#define SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__SHIFT       16
#define SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__WIDTH       3
#define SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__MASK        0x00070000
#define SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_8_REG_CLR__LCD_GPIO_20__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_9_REG register */
#define SW_TOP_FUNC_SEL_9_REG_SET 0x10E400C8

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_0 - sets function_select bits of VIP_0 pad */
/* sets function_select bits of VIP_0 pad */
/* 0b000 : vip_gpio[0] */
/* 0b001 : vi.vip1_0 */
/* 0b010 : ld.ldd_16 */
/* 0b011 : gn.trg_acq_d1_mux1 */
/* 0b100 : sd6.sd_dat_6_0_mux0 */
/* 0b101 : ca.trb_func_txdata_0 */
/* 0b110 : au.func_dbg_dconv_8 */
/* 0b111 : cvbs.cvbsafe_debug_data0_0 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__SHIFT       0
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_1 - sets function_select bits of VIP_1 pad */
/* sets function_select bits of VIP_1 pad */
/* 0b000 : vip_gpio[1] */
/* 0b001 : vi.vip1_1 */
/* 0b010 : ld.ldd_17 */
/* 0b011 : gn.trg_irq_b_mux1 */
/* 0b100 : sd6.sd_dat_6_1_mux0 */
/* 0b101 : ca.trb_func_txdata_1 */
/* 0b110 : au.func_dbg_dconv_9 */
/* 0b111 : cvbs.cvbsafe_debug_data0_1 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__SHIFT       4
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_2 - sets function_select bits of VIP_2 pad */
/* sets function_select bits of VIP_2 pad */
/* 0b000 : vip_gpio[2] */
/* 0b001 : vi.vip1_2 */
/* 0b010 : ld.ldd_18 */
/* 0b011 : gn.trg_acq_d0_mux1 */
/* 0b100 : sd6.sd_dat_6_2_mux0 */
/* 0b101 : ca.trb_func_txdata_2 */
/* 0b110 : au.func_dbg_dconv_10 */
/* 0b111 : cvbs.cvbsafe_debug_data0_2 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__SHIFT       8
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_3 - sets function_select bits of VIP_3 pad */
/* sets function_select bits of VIP_3 pad */
/* 0b000 : vip_gpio[3] */
/* 0b001 : vi.vip1_3 */
/* 0b010 : ld.ldd_19 */
/* 0b011 : gn.trg_acq_clk_mux1 */
/* 0b100 : sd6.sd_dat_6_3_mux0 */
/* 0b101 : ca.trb_func_txdata_3 */
/* 0b110 : au.func_dbg_dconv_11 */
/* 0b111 : cvbs.cvbsafe_debug_data0_3 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__SHIFT       12
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_4 - sets function_select bits of VIP_4 pad */
/* sets function_select bits of VIP_4 pad */
/* 0b000 : vip_gpio[4] */
/* 0b001 : vi.vip1_4 */
/* 0b010 : ld.ldd_20 */
/* 0b011 : clkc.trg_ref_clk_mux1 */
/* 0b100 : sd6.sd_cmd_6_mux0 */
/* 0b101 : ca.trb_func_txclk */
/* 0b110 : au.func_dbg_dconv_12 */
/* 0b111 : cvbs.cvbsafe_debug_data0_4 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__SHIFT       16
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__MASK        0x00070000
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_5 - sets function_select bits of VIP_5 pad */
/* sets function_select bits of VIP_5 pad */
/* 0b000 : vip_gpio[5] */
/* 0b001 : vi.vip1_5 */
/* 0b010 : ld.ldd_21 */
/* 0b011 : gn.trg_spi_cs_b_mux1 */
/* 0b100 : sd6.sd_clk_6_mux0 */
/* 0b101 : ca.spi_func_csb_mux0 */
/* 0b110 : au.func_dbg_dconv_13 */
/* 0b111 : cvbs.cvbsafe_debug_data0_5 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__SHIFT       20
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__MASK        0x00700000
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_6 - sets function_select bits of VIP_6 pad */
/* sets function_select bits of VIP_6 pad */
/* 0b000 : vip_gpio[6] */
/* 0b001 : vi.vip1_6 */
/* 0b010 : ld.ldd_22 */
/* 0b011 : gn.trg_spi_di_mux1 */
/* 0b101 : ca.spi_func_mosi */
/* 0b110 : au.func_dbg_soc */
/* 0b111 : cvbs.cvbsafe_debug_data0_6 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__SHIFT       24
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__MASK        0x07000000
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_SET.vip_7 - sets function_select bits of VIP_7 pad */
/* sets function_select bits of VIP_7 pad */
/* 0b000 : vip_gpio[7] */
/* 0b001 : vi.vip1_7 */
/* 0b010 : ld.ldd_23 */
/* 0b011 : gn.trg_spi_do_mux1 */
/* 0b101 : ca.spi_miso */
/* 0b110 : au.func_dbg_eoc_out */
/* 0b111 : cvbs.cvbsafe_debug_data0_7 */
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__SHIFT       28
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__MASK        0x70000000
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_9_REG_SET__VIP_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_9_REG register */
#define SW_TOP_FUNC_SEL_9_REG_CLR 0x10E400CC

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_0 - clears function_select bits of VIP_0 pad */
/* clears function_select bits of VIP_0 pad */
/* 0b000 : vip_gpio[0] */
/* 0b001 : vi.vip1_0 */
/* 0b010 : ld.ldd_16 */
/* 0b011 : gn.trg_acq_d1_mux1 */
/* 0b100 : sd6.sd_dat_6_0_mux0 */
/* 0b101 : ca.trb_func_txdata_0 */
/* 0b110 : au.func_dbg_dconv_8 */
/* 0b111 : cvbs.cvbsafe_debug_data0_0 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__SHIFT       0
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_1 - clears function_select bits of VIP_1 pad */
/* clears function_select bits of VIP_1 pad */
/* 0b000 : vip_gpio[1] */
/* 0b001 : vi.vip1_1 */
/* 0b010 : ld.ldd_17 */
/* 0b011 : gn.trg_irq_b_mux1 */
/* 0b100 : sd6.sd_dat_6_1_mux0 */
/* 0b101 : ca.trb_func_txdata_1 */
/* 0b110 : au.func_dbg_dconv_9 */
/* 0b111 : cvbs.cvbsafe_debug_data0_1 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__SHIFT       4
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_2 - clears function_select bits of VIP_2 pad */
/* clears function_select bits of VIP_2 pad */
/* 0b000 : vip_gpio[2] */
/* 0b001 : vi.vip1_2 */
/* 0b010 : ld.ldd_18 */
/* 0b011 : gn.trg_acq_d0_mux1 */
/* 0b100 : sd6.sd_dat_6_2_mux0 */
/* 0b101 : ca.trb_func_txdata_2 */
/* 0b110 : au.func_dbg_dconv_10 */
/* 0b111 : cvbs.cvbsafe_debug_data0_2 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__SHIFT       8
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_3 - clears function_select bits of VIP_3 pad */
/* clears function_select bits of VIP_3 pad */
/* 0b000 : vip_gpio[3] */
/* 0b001 : vi.vip1_3 */
/* 0b010 : ld.ldd_19 */
/* 0b011 : gn.trg_acq_clk_mux1 */
/* 0b100 : sd6.sd_dat_6_3_mux0 */
/* 0b101 : ca.trb_func_txdata_3 */
/* 0b110 : au.func_dbg_dconv_11 */
/* 0b111 : cvbs.cvbsafe_debug_data0_3 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__SHIFT       12
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_4 - clears function_select bits of VIP_4 pad */
/* clears function_select bits of VIP_4 pad */
/* 0b000 : vip_gpio[4] */
/* 0b001 : vi.vip1_4 */
/* 0b010 : ld.ldd_20 */
/* 0b011 : clkc.trg_ref_clk_mux1 */
/* 0b100 : sd6.sd_cmd_6_mux0 */
/* 0b101 : ca.trb_func_txclk */
/* 0b110 : au.func_dbg_dconv_12 */
/* 0b111 : cvbs.cvbsafe_debug_data0_4 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__SHIFT       16
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__MASK        0x00070000
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_5 - clears function_select bits of VIP_5 pad */
/* clears function_select bits of VIP_5 pad */
/* 0b000 : vip_gpio[5] */
/* 0b001 : vi.vip1_5 */
/* 0b010 : ld.ldd_21 */
/* 0b011 : gn.trg_spi_cs_b_mux1 */
/* 0b100 : sd6.sd_clk_6_mux0 */
/* 0b101 : ca.spi_func_csb_mux0 */
/* 0b110 : au.func_dbg_dconv_13 */
/* 0b111 : cvbs.cvbsafe_debug_data0_5 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__SHIFT       20
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__MASK        0x00700000
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_6 - clears function_select bits of VIP_6 pad */
/* clears function_select bits of VIP_6 pad */
/* 0b000 : vip_gpio[6] */
/* 0b001 : vi.vip1_6 */
/* 0b010 : ld.ldd_22 */
/* 0b011 : gn.trg_spi_di_mux1 */
/* 0b101 : ca.spi_func_mosi */
/* 0b110 : au.func_dbg_soc */
/* 0b111 : cvbs.cvbsafe_debug_data0_6 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__SHIFT       24
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__MASK        0x07000000
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_9_REG_CLR.vip_7 - clears function_select bits of VIP_7 pad */
/* clears function_select bits of VIP_7 pad */
/* 0b000 : vip_gpio[7] */
/* 0b001 : vi.vip1_7 */
/* 0b010 : ld.ldd_23 */
/* 0b011 : gn.trg_spi_do_mux1 */
/* 0b101 : ca.spi_miso */
/* 0b110 : au.func_dbg_eoc_out */
/* 0b111 : cvbs.cvbsafe_debug_data0_7 */
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__SHIFT       28
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__WIDTH       3
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__MASK        0x70000000
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_9_REG_CLR__VIP_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_10_REG register */
#define SW_TOP_FUNC_SEL_10_REG_SET 0x10E400D0

/* SW_TOP_FUNC_SEL_10_REG_SET.vip_pxclk - sets function_select bits of VIP_PXCLK pad */
/* sets function_select bits of VIP_PXCLK pad */
/* 0b000 : vip_gpio[8] */
/* 0b001 : vi.vip1_8 */
/* 0b010 : pw.cko_0_mux2 */
/* 0b011 : gn.trg_spi_clk_mux1 */
/* 0b101 : ca.spi_func_clk */
/* 0b111 : cvbs.cvbsafe_debug_clkout0 */
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_PXCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_SET.vip_hsync - sets function_select bits of VIP_HSYNC pad */
/* sets function_select bits of VIP_HSYNC pad */
/* 0b000 : vip_gpio[9] */
/* 0b001 : vi.vip1_9 */
/* 0b010 : u3.txd_3_mux1 */
/* 0b011 : gn.trg_shutdown_b_out_mux1 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux4 */
/* 0b111 : cvbs.cvbsafe_debug_data0_8 */
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__SHIFT       4
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__MASK        0x00000070
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_HSYNC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_SET.vip_vsync - sets function_select bits of VIP_VSYNC pad */
/* sets function_select bits of VIP_VSYNC pad */
/* 0b000 : vip_gpio[10] */
/* 0b001 : vi.vip1_10 */
/* 0b010 : u3.rxd_3_mux1 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux4 */
/* 0b111 : cvbs.cvbsafe_debug_data0_9 */
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__SHIFT       8
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__MASK        0x00000700
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_10_REG_SET__VIP_VSYNC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_10_REG register */
#define SW_TOP_FUNC_SEL_10_REG_CLR 0x10E400D4

/* SW_TOP_FUNC_SEL_10_REG_CLR.vip_pxclk - clears function_select bits of VIP_PXCLK pad */
/* clears function_select bits of VIP_PXCLK pad */
/* 0b000 : vip_gpio[8] */
/* 0b001 : vi.vip1_8 */
/* 0b010 : pw.cko_0_mux2 */
/* 0b011 : gn.trg_spi_clk_mux1 */
/* 0b101 : ca.spi_func_clk */
/* 0b111 : cvbs.cvbsafe_debug_clkout0 */
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_PXCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_CLR.vip_hsync - clears function_select bits of VIP_HSYNC pad */
/* clears function_select bits of VIP_HSYNC pad */
/* 0b000 : vip_gpio[9] */
/* 0b001 : vi.vip1_9 */
/* 0b010 : u3.txd_3_mux1 */
/* 0b011 : gn.trg_shutdown_b_out_mux1 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux4 */
/* 0b111 : cvbs.cvbsafe_debug_data0_8 */
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__SHIFT       4
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__MASK        0x00000070
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_HSYNC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_10_REG_CLR.vip_vsync - clears function_select bits of VIP_VSYNC pad */
/* clears function_select bits of VIP_VSYNC pad */
/* 0b000 : vip_gpio[10] */
/* 0b001 : vi.vip1_10 */
/* 0b010 : u3.rxd_3_mux1 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux4 */
/* 0b111 : cvbs.cvbsafe_debug_data0_9 */
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__SHIFT       8
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__WIDTH       3
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__MASK        0x00000700
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_10_REG_CLR__VIP_VSYNC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_11_REG register */
#define SW_TOP_FUNC_SEL_11_REG_SET 0x10E400D8

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_clk - sets function_select bits of SDIO3_CLK pad */
/* sets function_select bits of SDIO3_CLK pad */
/* 0b000 : sdio3_gpio[0] */
/* 0b001 : sd3.sd_clk_3 */
/* 0b101 : ca.bt_lpc_func_clk */
/* 0b110 : visbus.dout_20 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_cmd - sets function_select bits of SDIO3_CMD pad */
/* sets function_select bits of SDIO3_CMD pad */
/* 0b000 : sdio3_gpio[1] */
/* 0b001 : sd3.sd_cmd_3 */
/* 0b101 : ca.bt_lpc_func_csb */
/* 0b110 : visbus.dout_21 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_dat_0 - sets function_select bits of SDIO3_DAT_0 pad */
/* sets function_select bits of SDIO3_DAT_0 pad */
/* 0b000 : sdio3_gpio[2] */
/* 0b001 : sd3.sd_dat_3_0 */
/* 0b101 : ca.bt_lpc_func_data_0 */
/* 0b110 : visbus.dout_22 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_dat_1 - sets function_select bits of SDIO3_DAT_1 pad */
/* sets function_select bits of SDIO3_DAT_1 pad */
/* 0b000 : sdio3_gpio[3] */
/* 0b001 : sd3.sd_dat_3_1 */
/* 0b101 : ca.bt_lpc_func_data_1 */
/* 0b110 : visbus.dout_23 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_dat_2 - sets function_select bits of SDIO3_DAT_2 pad */
/* sets function_select bits of SDIO3_DAT_2 pad */
/* 0b000 : sdio3_gpio[4] */
/* 0b001 : sd3.sd_dat_3_2 */
/* 0b101 : ca.bt_lpc_func_data_2 */
/* 0b110 : visbus.dout_24 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_SET.sdio3_dat_3 - sets function_select bits of SDIO3_DAT_3 pad */
/* sets function_select bits of SDIO3_DAT_3 pad */
/* 0b000 : sdio3_gpio[5] */
/* 0b001 : sd3.sd_dat_3_3 */
/* 0b101 : ca.bt_lpc_func_data_3 */
/* 0b110 : visbus.dout_25 */
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_11_REG_SET__SDIO3_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_11_REG register */
#define SW_TOP_FUNC_SEL_11_REG_CLR 0x10E400DC

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_clk - clears function_select bits of SDIO3_CLK pad */
/* clears function_select bits of SDIO3_CLK pad */
/* 0b000 : sdio3_gpio[0] */
/* 0b001 : sd3.sd_clk_3 */
/* 0b101 : ca.bt_lpc_func_clk */
/* 0b110 : visbus.dout_20 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_cmd - clears function_select bits of SDIO3_CMD pad */
/* clears function_select bits of SDIO3_CMD pad */
/* 0b000 : sdio3_gpio[1] */
/* 0b001 : sd3.sd_cmd_3 */
/* 0b101 : ca.bt_lpc_func_csb */
/* 0b110 : visbus.dout_21 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_dat_0 - clears function_select bits of SDIO3_DAT_0 pad */
/* clears function_select bits of SDIO3_DAT_0 pad */
/* 0b000 : sdio3_gpio[2] */
/* 0b001 : sd3.sd_dat_3_0 */
/* 0b101 : ca.bt_lpc_func_data_0 */
/* 0b110 : visbus.dout_22 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_dat_1 - clears function_select bits of SDIO3_DAT_1 pad */
/* clears function_select bits of SDIO3_DAT_1 pad */
/* 0b000 : sdio3_gpio[3] */
/* 0b001 : sd3.sd_dat_3_1 */
/* 0b101 : ca.bt_lpc_func_data_1 */
/* 0b110 : visbus.dout_23 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_dat_2 - clears function_select bits of SDIO3_DAT_2 pad */
/* clears function_select bits of SDIO3_DAT_2 pad */
/* 0b000 : sdio3_gpio[4] */
/* 0b001 : sd3.sd_dat_3_2 */
/* 0b101 : ca.bt_lpc_func_data_2 */
/* 0b110 : visbus.dout_24 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_11_REG_CLR.sdio3_dat_3 - clears function_select bits of SDIO3_DAT_3 pad */
/* clears function_select bits of SDIO3_DAT_3 pad */
/* 0b000 : sdio3_gpio[5] */
/* 0b001 : sd3.sd_dat_3_3 */
/* 0b101 : ca.bt_lpc_func_data_3 */
/* 0b110 : visbus.dout_25 */
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_11_REG_CLR__SDIO3_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_12_REG register */
#define SW_TOP_FUNC_SEL_12_REG_SET 0x10E400E0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_clk - sets function_select bits of SDIO5_CLK pad */
/* sets function_select bits of SDIO5_CLK pad */
/* 0b000 : sdio5_gpio[0] */
/* 0b001 : sd5.sd_clk_5 */
/* 0b010 : i2s0.hs_i2s0_bs_mux0 */
/* 0b011 : au.usclk_2_mux2 */
/* 0b100 : ca.trb_func_rxclk */
/* 0b101 : ca.pcm_debug_clk */
/* 0b110 : visbus.dout_26 */
/* 0b111 : gn.io_gnsssys_sw_cfg_10 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_cmd - sets function_select bits of SDIO5_CMD pad */
/* sets function_select bits of SDIO5_CMD pad */
/* 0b000 : sdio5_gpio[1] */
/* 0b001 : sd5.sd_cmd_5 */
/* 0b010 : i2s0.hs_i2s0_ws_mux0 */
/* 0b011 : au.utxd_2_mux2 */
/* 0b101 : ca.pcm_debug_sync */
/* 0b110 : visbus.dout_27 */
/* 0b111 : gn.io_gnsssys_sw_cfg_11 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_dat_0 - sets function_select bits of SDIO5_DAT_0 pad */
/* sets function_select bits of SDIO5_DAT_0 pad */
/* 0b000 : sdio5_gpio[2] */
/* 0b001 : sd5.sd_dat_5_0 */
/* 0b010 : i2s0.hs_i2s0_rxd0_mux0 */
/* 0b011 : au.urxd_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_0 */
/* 0b101 : ca.pcm_debug_data */
/* 0b110 : visbus.dout_28 */
/* 0b111 : gn.io_gnsssys_sw_cfg_12 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_dat_1 - sets function_select bits of SDIO5_DAT_1 pad */
/* sets function_select bits of SDIO5_DAT_1 pad */
/* 0b000 : sdio5_gpio[3] */
/* 0b001 : sd5.sd_dat_5_1 */
/* 0b010 : i2s0.hs_i2s0_rxd1_mux0 */
/* 0b011 : au.utfs_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_1 */
/* 0b101 : ca.pcm_debug_data_out */
/* 0b110 : visbus.dout_29 */
/* 0b111 : gn.io_gnsssys_sw_cfg_13 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_dat_2 - sets function_select bits of SDIO5_DAT_2 pad */
/* sets function_select bits of SDIO5_DAT_2 pad */
/* 0b000 : sdio5_gpio[4] */
/* 0b001 : sd5.sd_dat_5_2 */
/* 0b010 : i2s1.hs_i2s1_bs_mux0 */
/* 0b100 : ca.trb_func_rxdata_2 */
/* 0b110 : visbus.dout_30 */
/* 0b111 : gn.io_gnsssys_sw_cfg_14 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_SET.sdio5_dat_3 - sets function_select bits of SDIO5_DAT_3 pad */
/* sets function_select bits of SDIO5_DAT_3 pad */
/* 0b000 : sdio5_gpio[5] */
/* 0b001 : sd5.sd_dat_5_3 */
/* 0b010 : i2s1.hs_i2s1_ws_mux0 */
/* 0b011 : au.urfs_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_3 */
/* 0b110 : visbus.dout_31 */
/* 0b111 : gn.io_gnsssys_sw_cfg_15 */
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_12_REG_SET__SDIO5_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_12_REG register */
#define SW_TOP_FUNC_SEL_12_REG_CLR 0x10E400E4

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_clk - clears function_select bits of SDIO5_CLK pad */
/* clears function_select bits of SDIO5_CLK pad */
/* 0b000 : sdio5_gpio[0] */
/* 0b001 : sd5.sd_clk_5 */
/* 0b010 : i2s0.hs_i2s0_bs_mux0 */
/* 0b011 : au.usclk_2_mux2 */
/* 0b100 : ca.trb_func_rxclk */
/* 0b101 : ca.pcm_debug_clk */
/* 0b110 : visbus.dout_26 */
/* 0b111 : gn.io_gnsssys_sw_cfg_10 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_cmd - clears function_select bits of SDIO5_CMD pad */
/* clears function_select bits of SDIO5_CMD pad */
/* 0b000 : sdio5_gpio[1] */
/* 0b001 : sd5.sd_cmd_5 */
/* 0b010 : i2s0.hs_i2s0_ws_mux0 */
/* 0b011 : au.utxd_2_mux2 */
/* 0b101 : ca.pcm_debug_sync */
/* 0b110 : visbus.dout_27 */
/* 0b111 : gn.io_gnsssys_sw_cfg_11 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__SHIFT       4
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__MASK        0x00000070
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_CMD__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_dat_0 - clears function_select bits of SDIO5_DAT_0 pad */
/* clears function_select bits of SDIO5_DAT_0 pad */
/* 0b000 : sdio5_gpio[2] */
/* 0b001 : sd5.sd_dat_5_0 */
/* 0b010 : i2s0.hs_i2s0_rxd0_mux0 */
/* 0b011 : au.urxd_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_0 */
/* 0b101 : ca.pcm_debug_data */
/* 0b110 : visbus.dout_28 */
/* 0b111 : gn.io_gnsssys_sw_cfg_12 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__SHIFT       8
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__MASK        0x00000700
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_dat_1 - clears function_select bits of SDIO5_DAT_1 pad */
/* clears function_select bits of SDIO5_DAT_1 pad */
/* 0b000 : sdio5_gpio[3] */
/* 0b001 : sd5.sd_dat_5_1 */
/* 0b010 : i2s0.hs_i2s0_rxd1_mux0 */
/* 0b011 : au.utfs_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_1 */
/* 0b101 : ca.pcm_debug_data_out */
/* 0b110 : visbus.dout_29 */
/* 0b111 : gn.io_gnsssys_sw_cfg_13 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__SHIFT       12
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__MASK        0x00007000
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_dat_2 - clears function_select bits of SDIO5_DAT_2 pad */
/* clears function_select bits of SDIO5_DAT_2 pad */
/* 0b000 : sdio5_gpio[4] */
/* 0b001 : sd5.sd_dat_5_2 */
/* 0b010 : i2s1.hs_i2s1_bs_mux0 */
/* 0b100 : ca.trb_func_rxdata_2 */
/* 0b110 : visbus.dout_30 */
/* 0b111 : gn.io_gnsssys_sw_cfg_14 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__SHIFT       16
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__MASK        0x00070000
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_12_REG_CLR.sdio5_dat_3 - clears function_select bits of SDIO5_DAT_3 pad */
/* clears function_select bits of SDIO5_DAT_3 pad */
/* 0b000 : sdio5_gpio[5] */
/* 0b001 : sd5.sd_dat_5_3 */
/* 0b010 : i2s1.hs_i2s1_ws_mux0 */
/* 0b011 : au.urfs_2_mux2 */
/* 0b100 : ca.trb_func_rxdata_3 */
/* 0b110 : visbus.dout_31 */
/* 0b111 : gn.io_gnsssys_sw_cfg_15 */
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__SHIFT       20
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__WIDTH       3
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__MASK        0x00700000
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_12_REG_CLR__SDIO5_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_13_REG register */
#define SW_TOP_FUNC_SEL_13_REG_SET 0x10E400E8

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_txd_0 - sets function_select bits of RGMII_TXD_0 pad */
/* sets function_select bits of RGMII_TXD_0 pad */
/* 0b000 : rgmii_gpio[0] */
/* 0b001 : rg.eth_mac7 */
/* 0b010 : vi.vip1_18 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__SHIFT       0
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_txd_1 - sets function_select bits of RGMII_TXD_1 pad */
/* sets function_select bits of RGMII_TXD_1 pad */
/* 0b000 : rgmii_gpio[1] */
/* 0b001 : rg.eth_mac8 */
/* 0b010 : vi.vip1_19 */
/* 0b011 : pw.pwm2_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__SHIFT       4
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_txd_2 - sets function_select bits of RGMII_TXD_2 pad */
/* sets function_select bits of RGMII_TXD_2 pad */
/* 0b000 : rgmii_gpio[2] */
/* 0b001 : rg.eth_mac9 */
/* 0b010 : vi.vip1_20 */
/* 0b011 : sd6.sd_cmd_6_mux1 */
/* 0b100 : u4.rts_4_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__SHIFT       8
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_txd_3 - sets function_select bits of RGMII_TXD_3 pad */
/* sets function_select bits of RGMII_TXD_3 pad */
/* 0b000 : rgmii_gpio[3] */
/* 0b001 : rg.eth_mac10 */
/* 0b010 : vi.vip1_21 */
/* 0b011 : sd6.sd_dat_6_0_mux1 */
/* 0b100 : u4.cts_4_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__SHIFT       12
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_txclk - sets function_select bits of RGMII_TXCLK pad */
/* sets function_select bits of RGMII_TXCLK pad */
/* 0b000 : rgmii_gpio[4] */
/* 0b001 : rg.eth_mac11 */
/* 0b010 : au.usclk_2_mux1 */
/* 0b011 : sd6.sd_clk_6_mux1 */
/* 0b100 : pw.cko_0_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__SHIFT       16
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__MASK        0x00070000
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TXCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_tx_ctl - sets function_select bits of RGMII_TX_CTL pad */
/* sets function_select bits of RGMII_TX_CTL pad */
/* 0b000 : rgmii_gpio[5] */
/* 0b001 : rg.eth_mac6 */
/* 0b010 : vi.vip1_17 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__SHIFT       20
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__MASK        0x00700000
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_TX_CTL__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_rxd_0 - sets function_select bits of RGMII_RXD_0 pad */
/* sets function_select bits of RGMII_RXD_0 pad */
/* 0b000 : rgmii_gpio[6] */
/* 0b001 : rg.eth_mac1 */
/* 0b010 : vi.vip1_12 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__SHIFT       24
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__MASK        0x07000000
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_SET.rgmii_rxd_1 - sets function_select bits of RGMII_RXD_1 pad */
/* sets function_select bits of RGMII_RXD_1 pad */
/* 0b000 : rgmii_gpio[7] */
/* 0b001 : rg.eth_mac2 */
/* 0b010 : vi.vip1_13 */
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__SHIFT       28
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__MASK        0x70000000
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_13_REG_SET__RGMII_RXD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_13_REG register */
#define SW_TOP_FUNC_SEL_13_REG_CLR 0x10E400EC

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_txd_0 - clears function_select bits of RGMII_TXD_0 pad */
/* clears function_select bits of RGMII_TXD_0 pad */
/* 0b000 : rgmii_gpio[0] */
/* 0b001 : rg.eth_mac7 */
/* 0b010 : vi.vip1_18 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__SHIFT       0
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_txd_1 - clears function_select bits of RGMII_TXD_1 pad */
/* clears function_select bits of RGMII_TXD_1 pad */
/* 0b000 : rgmii_gpio[1] */
/* 0b001 : rg.eth_mac8 */
/* 0b010 : vi.vip1_19 */
/* 0b011 : pw.pwm2_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__SHIFT       4
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_txd_2 - clears function_select bits of RGMII_TXD_2 pad */
/* clears function_select bits of RGMII_TXD_2 pad */
/* 0b000 : rgmii_gpio[2] */
/* 0b001 : rg.eth_mac9 */
/* 0b010 : vi.vip1_20 */
/* 0b011 : sd6.sd_cmd_6_mux1 */
/* 0b100 : u4.rts_4_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__SHIFT       8
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_txd_3 - clears function_select bits of RGMII_TXD_3 pad */
/* clears function_select bits of RGMII_TXD_3 pad */
/* 0b000 : rgmii_gpio[3] */
/* 0b001 : rg.eth_mac10 */
/* 0b010 : vi.vip1_21 */
/* 0b011 : sd6.sd_dat_6_0_mux1 */
/* 0b100 : u4.cts_4_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__SHIFT       12
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_txclk - clears function_select bits of RGMII_TXCLK pad */
/* clears function_select bits of RGMII_TXCLK pad */
/* 0b000 : rgmii_gpio[4] */
/* 0b001 : rg.eth_mac11 */
/* 0b010 : au.usclk_2_mux1 */
/* 0b011 : sd6.sd_clk_6_mux1 */
/* 0b100 : pw.cko_0_mux1 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__SHIFT       16
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__MASK        0x00070000
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TXCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_tx_ctl - clears function_select bits of RGMII_TX_CTL pad */
/* clears function_select bits of RGMII_TX_CTL pad */
/* 0b000 : rgmii_gpio[5] */
/* 0b001 : rg.eth_mac6 */
/* 0b010 : vi.vip1_17 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__SHIFT       20
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__MASK        0x00700000
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_TX_CTL__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_rxd_0 - clears function_select bits of RGMII_RXD_0 pad */
/* clears function_select bits of RGMII_RXD_0 pad */
/* 0b000 : rgmii_gpio[6] */
/* 0b001 : rg.eth_mac1 */
/* 0b010 : vi.vip1_12 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__SHIFT       24
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__MASK        0x07000000
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_13_REG_CLR.rgmii_rxd_1 - clears function_select bits of RGMII_RXD_1 pad */
/* clears function_select bits of RGMII_RXD_1 pad */
/* 0b000 : rgmii_gpio[7] */
/* 0b001 : rg.eth_mac2 */
/* 0b010 : vi.vip1_13 */
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__SHIFT       28
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__WIDTH       3
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__MASK        0x70000000
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_13_REG_CLR__RGMII_RXD_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_14_REG register */
#define SW_TOP_FUNC_SEL_14_REG_SET 0x10E400F0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_rxd_2 - sets function_select bits of RGMII_RXD_2 pad */
/* sets function_select bits of RGMII_RXD_2 pad */
/* 0b000 : rgmii_gpio[8] */
/* 0b001 : rg.eth_mac3 */
/* 0b010 : vi.vip1_14 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__SHIFT       0
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__MASK        0x00000007
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_rxd_3 - sets function_select bits of RGMII_RXD_3 pad */
/* sets function_select bits of RGMII_RXD_3 pad */
/* 0b000 : rgmii_gpio[9] */
/* 0b001 : rg.eth_mac4 */
/* 0b010 : vi.vip1_15 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__SHIFT       4
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__MASK        0x00000070
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_rx_clk - sets function_select bits of RGMII_RX_CLK pad */
/* sets function_select bits of RGMII_RX_CLK pad */
/* 0b000 : rgmii_gpio[10] */
/* 0b001 : rg.eth_mac5 */
/* 0b010 : vi.vip1_16 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__SHIFT       8
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RX_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_rxc_ctl - sets function_select bits of RGMII_RXC_CTL pad */
/* sets function_select bits of RGMII_RXC_CTL pad */
/* 0b000 : rgmii_gpio[11] */
/* 0b001 : rg.eth_mac0 */
/* 0b010 : vi.vip1_11 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__SHIFT       12
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__MASK        0x00007000
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_RXC_CTL__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_mdio - sets function_select bits of RGMII_MDIO pad */
/* sets function_select bits of RGMII_MDIO pad */
/* 0b000 : rgmii_gpio[12] */
/* 0b001 : rg.rgmii_mac_md */
/* 0b010 : au.urxd_2_mux1 */
/* 0b011 : sd6.sd_dat_6_2_mux1 */
/* 0b100 : u3.rts_3_mux1 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__SHIFT       16
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__MASK        0x00070000
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDIO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_mdc - sets function_select bits of RGMII_MDC pad */
/* sets function_select bits of RGMII_MDC pad */
/* 0b000 : rgmii_gpio[13] */
/* 0b001 : rg.rgmii_mac_mdc */
/* 0b010 : au.utxd_2_mux1 */
/* 0b011 : sd6.sd_dat_6_1_mux1 */
/* 0b100 : pw.cko_1_mux1 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__SHIFT       20
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__MASK        0x00700000
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_MDC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_SET.rgmii_intr_n - sets function_select bits of RGMII_INTR_N pad */
/* sets function_select bits of RGMII_INTR_N pad */
/* 0b000 : rgmii_gpio[14] */
/* 0b001 : rg.gmac_phy_intr_n_mux0 */
/* 0b010 : au.utfs_2_mux1 */
/* 0b011 : sd6.sd_dat_6_3_mux1 */
/* 0b100 : u3.cts_3_mux1 */
/* 0b101 : rg.rgmii_phy_ref_clk_mux0 */
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__SHIFT       24
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__MASK        0x07000000
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_14_REG_SET__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_14_REG register */
#define SW_TOP_FUNC_SEL_14_REG_CLR 0x10E400F4

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_rxd_2 - clears function_select bits of RGMII_RXD_2 pad */
/* clears function_select bits of RGMII_RXD_2 pad */
/* 0b000 : rgmii_gpio[8] */
/* 0b001 : rg.eth_mac3 */
/* 0b010 : vi.vip1_14 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__SHIFT       0
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__MASK        0x00000007
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_rxd_3 - clears function_select bits of RGMII_RXD_3 pad */
/* clears function_select bits of RGMII_RXD_3 pad */
/* 0b000 : rgmii_gpio[9] */
/* 0b001 : rg.eth_mac4 */
/* 0b010 : vi.vip1_15 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__SHIFT       4
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__MASK        0x00000070
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXD_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_rx_clk - clears function_select bits of RGMII_RX_CLK pad */
/* clears function_select bits of RGMII_RX_CLK pad */
/* 0b000 : rgmii_gpio[10] */
/* 0b001 : rg.eth_mac5 */
/* 0b010 : vi.vip1_16 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__SHIFT       8
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RX_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_rxc_ctl - clears function_select bits of RGMII_RXC_CTL pad */
/* clears function_select bits of RGMII_RXC_CTL pad */
/* 0b000 : rgmii_gpio[11] */
/* 0b001 : rg.eth_mac0 */
/* 0b010 : vi.vip1_11 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__SHIFT       12
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__MASK        0x00007000
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_RXC_CTL__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_mdio - clears function_select bits of RGMII_MDIO pad */
/* clears function_select bits of RGMII_MDIO pad */
/* 0b000 : rgmii_gpio[12] */
/* 0b001 : rg.rgmii_mac_md */
/* 0b010 : au.urxd_2_mux1 */
/* 0b011 : sd6.sd_dat_6_2_mux1 */
/* 0b100 : u3.rts_3_mux1 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__SHIFT       16
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__MASK        0x00070000
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDIO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_mdc - clears function_select bits of RGMII_MDC pad */
/* clears function_select bits of RGMII_MDC pad */
/* 0b000 : rgmii_gpio[13] */
/* 0b001 : rg.rgmii_mac_mdc */
/* 0b010 : au.utxd_2_mux1 */
/* 0b011 : sd6.sd_dat_6_1_mux1 */
/* 0b100 : pw.cko_1_mux1 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__SHIFT       20
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__MASK        0x00700000
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_MDC__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_14_REG_CLR.rgmii_intr_n - clears function_select bits of RGMII_INTR_N pad */
/* clears function_select bits of RGMII_INTR_N pad */
/* 0b000 : rgmii_gpio[14] */
/* 0b001 : rg.gmac_phy_intr_n_mux0 */
/* 0b010 : au.utfs_2_mux1 */
/* 0b011 : sd6.sd_dat_6_3_mux1 */
/* 0b100 : u3.cts_3_mux1 */
/* 0b101 : rg.rgmii_phy_ref_clk_mux0 */
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__SHIFT       24
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__WIDTH       3
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__MASK        0x07000000
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_14_REG_CLR__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_15_REG register */
#define SW_TOP_FUNC_SEL_15_REG_SET 0x10E400F8

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_mclk - sets function_select bits of I2S_MCLK pad */
/* sets function_select bits of I2S_MCLK pad */
/* 0b000 : i2s_gpio[0] */
/* 0b001 : au.i2s_mclk_1 */
/* 0b010 : au.i2s_extclk_1 */
/* 0b011 : au.spdif_out_mux0 */
/* 0b100 : gn.gnss_irq1_mux0 */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_MCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_bclk - sets function_select bits of I2S_BCLK pad */
/* sets function_select bits of I2S_BCLK pad */
/* 0b000 : i2s_gpio[1] */
/* 0b001 : au.i2s_sclk_1 */
/* 0b010 : au.ac97_bit_clk */
/* 0b100 : gn.gnss_eclk */
/* 0b110 : au.func_dbg_i2s_bclk_out */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__SHIFT       4
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_BCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_ws - sets function_select bits of I2S_WS pad */
/* sets function_select bits of I2S_WS pad */
/* 0b000 : i2s_gpio[2] */
/* 0b001 : au.i2s_ws_1 */
/* 0b010 : au.ac97_sync */
/* 0b100 : gn.gnss_tsync */
/* 0b110 : au.func_dbg_i2s_fs_out */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__SHIFT       8
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__MASK        0x00000700
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_WS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_dout0 - sets function_select bits of I2S_DOUT0 pad */
/* sets function_select bits of I2S_DOUT0 pad */
/* 0b000 : i2s_gpio[3] */
/* 0b001 : au.i2s_dout0_1 */
/* 0b010 : au.ac97_dout */
/* 0b100 : gn.gnss_tm */
/* 0b110 : au.func_dbg_i2s_tx_out */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__SHIFT       12
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__MASK        0x00007000
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_dout1 - sets function_select bits of I2S_DOUT1 pad */
/* sets function_select bits of I2S_DOUT1 pad */
/* 0b000 : i2s_gpio[4] */
/* 0b001 : au.i2s_dout1_1 */
/* 0b010 : u4.rts_4_mux2 */
/* 0b011 : au.spdif_out_mux1 */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__SHIFT       16
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__MASK        0x00070000
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_dout2 - sets function_select bits of I2S_DOUT2 pad */
/* sets function_select bits of I2S_DOUT2 pad */
/* 0b000 : i2s_gpio[5] */
/* 0b001 : au.i2s_dout2_1 */
/* 0b010 : u4.cts_4_mux2 */
/* 0b011 : pw.i2s01_clk_mux1 */
/* 0b100 : gn.trg_shutdown_b_out_mux2 */
/* 0b101 : au.urfs_0_mux0 */
/* 0b110 : au.urfs_1_mux0 */
/* 0b111 : i2s1.hs_i2s1_rxd0_mux3 */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__SHIFT       20
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__MASK        0x00700000
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DOUT2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_SET.i2s_din - sets function_select bits of I2S_DIN pad */
/* sets function_select bits of I2S_DIN pad */
/* 0b000 : i2s_gpio[6] */
/* 0b001 : au.i2s_din_1 */
/* 0b010 : au.ac97_din */
/* 0b100 : gn.gnss_irq2_mux0 */
/* 0b110 : au.func_dbg_i2s_rx */
/* 0b111 : i2s1.hs_i2s1_rxd1_mux3 */
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__SHIFT       24
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__MASK        0x07000000
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_15_REG_SET__I2S_DIN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_15_REG register */
#define SW_TOP_FUNC_SEL_15_REG_CLR 0x10E400FC

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_mclk - clears function_select bits of I2S_MCLK pad */
/* clears function_select bits of I2S_MCLK pad */
/* 0b000 : i2s_gpio[0] */
/* 0b001 : au.i2s_mclk_1 */
/* 0b010 : au.i2s_extclk_1 */
/* 0b011 : au.spdif_out_mux0 */
/* 0b100 : gn.gnss_irq1_mux0 */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__SHIFT       0
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_MCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_bclk - clears function_select bits of I2S_BCLK pad */
/* clears function_select bits of I2S_BCLK pad */
/* 0b000 : i2s_gpio[1] */
/* 0b001 : au.i2s_sclk_1 */
/* 0b010 : au.ac97_bit_clk */
/* 0b100 : gn.gnss_eclk */
/* 0b110 : au.func_dbg_i2s_bclk_out */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__SHIFT       4
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__MASK        0x00000070
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_BCLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_ws - clears function_select bits of I2S_WS pad */
/* clears function_select bits of I2S_WS pad */
/* 0b000 : i2s_gpio[2] */
/* 0b001 : au.i2s_ws_1 */
/* 0b010 : au.ac97_sync */
/* 0b100 : gn.gnss_tsync */
/* 0b110 : au.func_dbg_i2s_fs_out */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__SHIFT       8
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__MASK        0x00000700
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_WS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_dout0 - clears function_select bits of I2S_DOUT0 pad */
/* clears function_select bits of I2S_DOUT0 pad */
/* 0b000 : i2s_gpio[3] */
/* 0b001 : au.i2s_dout0_1 */
/* 0b010 : au.ac97_dout */
/* 0b100 : gn.gnss_tm */
/* 0b110 : au.func_dbg_i2s_tx_out */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__SHIFT       12
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__MASK        0x00007000
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_dout1 - clears function_select bits of I2S_DOUT1 pad */
/* clears function_select bits of I2S_DOUT1 pad */
/* 0b000 : i2s_gpio[4] */
/* 0b001 : au.i2s_dout1_1 */
/* 0b010 : u4.rts_4_mux2 */
/* 0b011 : au.spdif_out_mux1 */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__SHIFT       16
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__MASK        0x00070000
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_dout2 - clears function_select bits of I2S_DOUT2 pad */
/* clears function_select bits of I2S_DOUT2 pad */
/* 0b000 : i2s_gpio[5] */
/* 0b001 : au.i2s_dout2_1 */
/* 0b010 : u4.cts_4_mux2 */
/* 0b011 : pw.i2s01_clk_mux1 */
/* 0b100 : gn.trg_shutdown_b_out_mux2 */
/* 0b101 : au.urfs_0_mux0 */
/* 0b110 : au.urfs_1_mux0 */
/* 0b111 : i2s1.hs_i2s1_rxd0_mux3 */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__SHIFT       20
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__MASK        0x00700000
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DOUT2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_15_REG_CLR.i2s_din - clears function_select bits of I2S_DIN pad */
/* clears function_select bits of I2S_DIN pad */
/* 0b000 : i2s_gpio[6] */
/* 0b001 : au.i2s_din_1 */
/* 0b010 : au.ac97_din */
/* 0b100 : gn.gnss_irq2_mux0 */
/* 0b110 : au.func_dbg_i2s_rx */
/* 0b111 : i2s1.hs_i2s1_rxd1_mux3 */
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__SHIFT       24
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__WIDTH       3
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__MASK        0x07000000
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_15_REG_CLR__I2S_DIN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_16_REG register */
#define SW_TOP_FUNC_SEL_16_REG_SET 0x10E40100

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_0 - sets function_select bits of GPIO_0 pad */
/* sets function_select bits of GPIO_0 pad */
/* 0b000 : gpio[0] */
/* 0b010 : ps.odo_0 */
/* 0b011 : pw.pwm0_mux0 */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__SHIFT       0
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_1 - sets function_select bits of GPIO_1 pad */
/* sets function_select bits of GPIO_1 pad */
/* 0b000 : gpio[1] */
/* 0b010 : ps.dr_dir */
/* 0b011 : pw.pwm1_mux0 */
/* 0b100 : u0.rts_0 */
/* 0b111 : gn.gnss_pon_req */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__SHIFT       4
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_2 - sets function_select bits of GPIO_2 pad */
/* sets function_select bits of GPIO_2 pad */
/* 0b000 : gpio[2] */
/* 0b010 : ca.pio_4 */
/* 0b011 : pw.pwm2_mux0 */
/* 0b100 : u0.cts_0 */
/* 0b101 : ps.odo_1 */
/* 0b111 : gn.gnss_force_pon */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__SHIFT       8
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_3 - sets function_select bits of GPIO_3 pad */
/* sets function_select bits of GPIO_3 pad */
/* 0b000 : gpio[3] */
/* 0b010 : ca.pio_5 */
/* 0b011 : pw.pwm3_mux0 */
/* 0b100 : u4.cts_4_mux0 */
/* 0b101 : au.digmic_mux1 */
/* 0b111 : gn.gnss_force_pon_ack */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT       12
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_4 - sets function_select bits of GPIO_4 pad */
/* sets function_select bits of GPIO_4 pad */
/* 0b000 : gpio[4] */
/* 0b010 : sd2.sd_wp_b_2_mux0 */
/* 0b011 : pw.cko_0_mux0 */
/* 0b100 : u4.rts_4_mux0 */
/* 0b101 : gn.trg_shutdown_b_out_mux3 */
/* 0b111 : gn.gnss_force_poff */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__SHIFT       16
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__MASK        0x00070000
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_5 - sets function_select bits of GPIO_5 pad */
/* sets function_select bits of GPIO_5 pad */
/* 0b000 : gpio[5] */
/* 0b010 : sd2.sd_cd_b_2_mux0 */
/* 0b011 : pw.cko_1_mux0 */
/* 0b100 : nd.df_wp_b */
/* 0b111 : gn.gnss_force_poff_ack */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__SHIFT       20
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__MASK        0x00700000
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_6 - sets function_select bits of GPIO_6 pad */
/* sets function_select bits of GPIO_6 pad */
/* 0b000 : gpio[6] */
/* 0b010 : u3.cts_3_mux0 */
/* 0b011 : pw.i2s01_clk_mux0 */
/* 0b100 : i2.sda_1 */
/* 0b101 : jtag.jt_dbg_nsrst_mux0 */
/* 0b110 : ca.pio_6 */
/* 0b111 : gn.gnss_poff_req */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__SHIFT       24
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__MASK        0x07000000
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_SET.gpio_7 - sets function_select bits of GPIO_7 pad */
/* sets function_select bits of GPIO_7 pad */
/* 0b000 : gpio[7] */
/* 0b010 : u3.rts_3_mux0 */
/* 0b100 : i2.scl_1 */
/* 0b110 : ca.pio_7 */
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__SHIFT       28
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__MASK        0x70000000
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_16_REG_SET__GPIO_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_16_REG register */
#define SW_TOP_FUNC_SEL_16_REG_CLR 0x10E40104

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_0 - clears function_select bits of GPIO_0 pad */
/* clears function_select bits of GPIO_0 pad */
/* 0b000 : gpio[0] */
/* 0b010 : ps.odo_0 */
/* 0b011 : pw.pwm0_mux0 */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__SHIFT       0
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_1 - clears function_select bits of GPIO_1 pad */
/* clears function_select bits of GPIO_1 pad */
/* 0b000 : gpio[1] */
/* 0b010 : ps.dr_dir */
/* 0b011 : pw.pwm1_mux0 */
/* 0b100 : u0.rts_0 */
/* 0b111 : gn.gnss_pon_req */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__SHIFT       4
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_2 - clears function_select bits of GPIO_2 pad */
/* clears function_select bits of GPIO_2 pad */
/* 0b000 : gpio[2] */
/* 0b010 : ca.pio_4 */
/* 0b011 : pw.pwm2_mux0 */
/* 0b100 : u0.cts_0 */
/* 0b101 : ps.odo_1 */
/* 0b111 : gn.gnss_force_pon */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__SHIFT       8
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_3 - clears function_select bits of GPIO_3 pad */
/* clears function_select bits of GPIO_3 pad */
/* 0b000 : gpio[3] */
/* 0b010 : ca.pio_5 */
/* 0b011 : pw.pwm3_mux0 */
/* 0b100 : u4.cts_4_mux0 */
/* 0b101 : au.digmic_mux1 */
/* 0b111 : gn.gnss_force_pon_ack */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__SHIFT       12
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_4 - clears function_select bits of GPIO_4 pad */
/* clears function_select bits of GPIO_4 pad */
/* 0b000 : gpio[4] */
/* 0b010 : sd2.sd_wp_b_2_mux0 */
/* 0b011 : pw.cko_0_mux0 */
/* 0b100 : u4.rts_4_mux0 */
/* 0b101 : gn.trg_shutdown_b_out_mux3 */
/* 0b111 : gn.gnss_force_poff */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__SHIFT       16
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__MASK        0x00070000
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_4__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_5 - clears function_select bits of GPIO_5 pad */
/* clears function_select bits of GPIO_5 pad */
/* 0b000 : gpio[5] */
/* 0b010 : sd2.sd_cd_b_2_mux0 */
/* 0b011 : pw.cko_1_mux0 */
/* 0b100 : nd.df_wp_b */
/* 0b111 : gn.gnss_force_poff_ack */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__SHIFT       20
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__MASK        0x00700000
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_5__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_6 - clears function_select bits of GPIO_6 pad */
/* clears function_select bits of GPIO_6 pad */
/* 0b000 : gpio[6] */
/* 0b010 : u3.cts_3_mux0 */
/* 0b011 : pw.i2s01_clk_mux0 */
/* 0b100 : i2.sda_1 */
/* 0b101 : jtag.jt_dbg_nsrst_mux0 */
/* 0b110 : ca.pio_6 */
/* 0b111 : gn.gnss_poff_req */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__SHIFT       24
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__MASK        0x07000000
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_6__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_16_REG_CLR.gpio_7 - clears function_select bits of GPIO_7 pad */
/* clears function_select bits of GPIO_7 pad */
/* 0b000 : gpio[7] */
/* 0b010 : u3.rts_3_mux0 */
/* 0b100 : i2.scl_1 */
/* 0b110 : ca.pio_7 */
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__SHIFT       28
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__WIDTH       3
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__MASK        0x70000000
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_7__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_17_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_17_REG register */
#define SW_TOP_FUNC_SEL_17_REG_SET 0x10E40108

/* SW_TOP_FUNC_SEL_17_REG_SET.sda_0 - sets function_select bits of SDA_0 pad */
/* sets function_select bits of SDA_0 pad */
/* 0b000 : gpio[8] */
/* 0b001 : i2.sda_0 */
/* 0b010 : gn.gnss_sda */
#define SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__SHIFT       16
#define SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__WIDTH       3
#define SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__MASK        0x00070000
#define SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_17_REG_SET__SDA_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_17_REG_SET.scl_0 - sets function_select bits of SCL_0 pad */
/* sets function_select bits of SCL_0 pad */
/* 0b000 : gpio[9] */
/* 0b001 : i2.scl_0 */
/* 0b010 : gn.gnss_scl */
#define SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__SHIFT       20
#define SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__WIDTH       3
#define SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__MASK        0x00700000
#define SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_17_REG_SET__SCL_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_17_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_17_REG register */
#define SW_TOP_FUNC_SEL_17_REG_CLR 0x10E4010C

/* SW_TOP_FUNC_SEL_17_REG_CLR.sda_0 - clears function_select bits of SDA_0 pad */
/* clears function_select bits of SDA_0 pad */
/* 0b000 : gpio[8] */
/* 0b001 : i2.sda_0 */
/* 0b010 : gn.gnss_sda */
#define SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__SHIFT       16
#define SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__WIDTH       3
#define SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__MASK        0x00070000
#define SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_17_REG_CLR__SDA_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_17_REG_CLR.scl_0 - clears function_select bits of SCL_0 pad */
/* clears function_select bits of SCL_0 pad */
/* 0b000 : gpio[9] */
/* 0b001 : i2.scl_0 */
/* 0b010 : gn.gnss_scl */
#define SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__SHIFT       20
#define SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__WIDTH       3
#define SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__MASK        0x00700000
#define SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_17_REG_CLR__SCL_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_18_REG register */
#define SW_TOP_FUNC_SEL_18_REG_SET 0x10E40110

/* SW_TOP_FUNC_SEL_18_REG_SET.coex_pio_0 - sets function_select bits of COEX_PIO_0 pad */
/* sets function_select bits of COEX_PIO_0 pad */
/* 0b000 : sdio3_gpio[6] */
/* 0b001 : ca.coex_0 */
/* 0b010 : i2s1.hs_i2s1_rxd0_mux2 */
/* 0b011 : u2.txd_2_mux2 */
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__SHIFT       0
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_SET.coex_pio_1 - sets function_select bits of COEX_PIO_1 pad */
/* sets function_select bits of COEX_PIO_1 pad */
/* 0b000 : sdio3_gpio[7] */
/* 0b001 : ca.coex_1 */
/* 0b010 : i2s1.hs_i2s1_rxd1_mux2 */
/* 0b011 : u2.rxd_2_mux2 */
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__SHIFT       4
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_SET.coex_pio_2 - sets function_select bits of COEX_PIO_2 pad */
/* sets function_select bits of COEX_PIO_2 pad */
/* 0b000 : sdio3_gpio[8] */
/* 0b001 : ca.coex_2 */
/* 0b010 : pw.pwm1_mux2 */
/* 0b011 : u2.rts_2_mux0 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux1 */
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__SHIFT       8
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_SET.coex_pio_3 - sets function_select bits of COEX_PIO_3 pad */
/* sets function_select bits of COEX_PIO_3 pad */
/* 0b000 : sdio3_gpio[9] */
/* 0b001 : ca.coex_3 */
/* 0b010 : pw.i2s01_clk_mux2 */
/* 0b011 : u2.cts_2_mux0 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux1 */
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__SHIFT       12
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_18_REG_SET__COEX_PIO_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_18_REG register */
#define SW_TOP_FUNC_SEL_18_REG_CLR 0x10E40114

/* SW_TOP_FUNC_SEL_18_REG_CLR.coex_pio_0 - clears function_select bits of COEX_PIO_0 pad */
/* clears function_select bits of COEX_PIO_0 pad */
/* 0b000 : sdio3_gpio[6] */
/* 0b001 : ca.coex_0 */
/* 0b010 : i2s1.hs_i2s1_rxd0_mux2 */
/* 0b011 : u2.txd_2_mux2 */
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__SHIFT       0
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__MASK        0x00000007
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_0__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_CLR.coex_pio_1 - clears function_select bits of COEX_PIO_1 pad */
/* clears function_select bits of COEX_PIO_1 pad */
/* 0b000 : sdio3_gpio[7] */
/* 0b001 : ca.coex_1 */
/* 0b010 : i2s1.hs_i2s1_rxd1_mux2 */
/* 0b011 : u2.rxd_2_mux2 */
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__SHIFT       4
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__MASK        0x00000070
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_1__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_CLR.coex_pio_2 - clears function_select bits of COEX_PIO_2 pad */
/* clears function_select bits of COEX_PIO_2 pad */
/* 0b000 : sdio3_gpio[8] */
/* 0b001 : ca.coex_2 */
/* 0b010 : pw.pwm1_mux2 */
/* 0b011 : u2.rts_2_mux0 */
/* 0b100 : i2s1.hs_i2s1_rxd0_mux1 */
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__SHIFT       8
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__MASK        0x00000700
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_2__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_18_REG_CLR.coex_pio_3 - clears function_select bits of COEX_PIO_3 pad */
/* clears function_select bits of COEX_PIO_3 pad */
/* 0b000 : sdio3_gpio[9] */
/* 0b001 : ca.coex_3 */
/* 0b010 : pw.i2s01_clk_mux2 */
/* 0b011 : u2.cts_2_mux0 */
/* 0b100 : i2s1.hs_i2s1_rxd1_mux1 */
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__SHIFT       12
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__WIDTH       3
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__MASK        0x00007000
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_18_REG_CLR__COEX_PIO_3__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_19_REG register */
#define SW_TOP_FUNC_SEL_19_REG_SET 0x10E40118

/* SW_TOP_FUNC_SEL_19_REG_SET.uart0_tx - sets function_select bits of UART0_TX pad */
/* sets function_select bits of UART0_TX pad */
/* 0b000 : uart_gpio[0] */
/* 0b001 : u0.txd_0 */
/* 0b011 : ca.uart_debug_tx */
/* 0b100 : gn.gnss_uart_tx */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__SHIFT       0
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__MASK        0x00000007
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET.uart0_rx - sets function_select bits of UART0_RX pad */
/* sets function_select bits of UART0_RX pad */
/* 0b000 : uart_gpio[1] */
/* 0b001 : u0.rxd_0 */
/* 0b010 : usb1.drvvbus_mux0 */
/* 0b011 : ca.uart_debug_rx */
/* 0b100 : gn.gnss_uart_rx */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__SHIFT       4
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_19_REG_SET__UART0_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET.uart1_tx - sets function_select bits of UART1_TX pad */
/* sets function_select bits of UART1_TX pad */
/* 0b000 : uart_gpio[2] */
/* 0b001 : u1.txd_1 */
/* 0b011 : ca.uart_debug_rts */
/* 0b100 : gn.gnss_rts_b */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__SHIFT       16
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__MASK        0x00070000
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET.uart1_rx - sets function_select bits of UART1_RX pad */
/* sets function_select bits of UART1_RX pad */
/* 0b000 : uart_gpio[3] */
/* 0b001 : u1.rxd_1 */
/* 0b011 : ca.uart_debug_cts */
/* 0b100 : gn.gnss_cts_b */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__SHIFT       20
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__MASK        0x00700000
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_19_REG_SET__UART1_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET.uart3_tx - sets function_select bits of UART3_TX pad */
/* sets function_select bits of UART3_TX pad */
/* 0b000 : uart_gpio[4] */
/* 0b001 : u3.txd_3_mux0 */
/* 0b010 : c1.can_txd_1_mux0 */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__SHIFT       24
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__MASK        0x07000000
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_SET.uart3_rx - sets function_select bits of UART3_RX pad */
/* sets function_select bits of UART3_RX pad */
/* 0b000 : uart_gpio[5] */
/* 0b001 : u3.rxd_3_mux0 */
/* 0b010 : c1.can_rxd_1_mux0 */
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__SHIFT       28
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__MASK        0x70000000
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_19_REG_SET__UART3_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_19_REG register */
#define SW_TOP_FUNC_SEL_19_REG_CLR 0x10E4011C

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart0_tx - clears function_select bits of UART0_TX pad */
/* clears function_select bits of UART0_TX pad */
/* 0b000 : uart_gpio[0] */
/* 0b001 : u0.txd_0 */
/* 0b011 : ca.uart_debug_tx */
/* 0b100 : gn.gnss_uart_tx */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__SHIFT       0
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__MASK        0x00000007
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart0_rx - clears function_select bits of UART0_RX pad */
/* clears function_select bits of UART0_RX pad */
/* 0b000 : uart_gpio[1] */
/* 0b001 : u0.rxd_0 */
/* 0b010 : usb1.drvvbus_mux0 */
/* 0b011 : ca.uart_debug_rx */
/* 0b100 : gn.gnss_uart_rx */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__SHIFT       4
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART0_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart1_tx - clears function_select bits of UART1_TX pad */
/* clears function_select bits of UART1_TX pad */
/* 0b000 : uart_gpio[2] */
/* 0b001 : u1.txd_1 */
/* 0b011 : ca.uart_debug_rts */
/* 0b100 : gn.gnss_rts_b */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__SHIFT       16
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__MASK        0x00070000
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart1_rx - clears function_select bits of UART1_RX pad */
/* clears function_select bits of UART1_RX pad */
/* 0b000 : uart_gpio[3] */
/* 0b001 : u1.rxd_1 */
/* 0b011 : ca.uart_debug_cts */
/* 0b100 : gn.gnss_cts_b */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__SHIFT       20
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__MASK        0x00700000
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART1_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart3_tx - clears function_select bits of UART3_TX pad */
/* clears function_select bits of UART3_TX pad */
/* 0b000 : uart_gpio[4] */
/* 0b001 : u3.txd_3_mux0 */
/* 0b010 : c1.can_txd_1_mux0 */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__SHIFT       24
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__MASK        0x07000000
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_19_REG_CLR.uart3_rx - clears function_select bits of UART3_RX pad */
/* clears function_select bits of UART3_RX pad */
/* 0b000 : uart_gpio[5] */
/* 0b001 : u3.rxd_3_mux0 */
/* 0b010 : c1.can_rxd_1_mux0 */
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__SHIFT       28
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__MASK        0x70000000
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_19_REG_CLR__UART3_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_20_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_20_REG register */
#define SW_TOP_FUNC_SEL_20_REG_SET 0x10E40120

/* SW_TOP_FUNC_SEL_20_REG_SET.uart4_tx - sets function_select bits of UART4_TX pad */
/* sets function_select bits of UART4_TX pad */
/* 0b000 : uart_gpio[6] */
/* 0b001 : u4.txd_4 */
/* 0b010 : u3.rts_3_mux2 */
/* 0b011 : au.urfs_0_mux1 */
/* 0b100 : au.urfs_2_mux0 */
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__SHIFT       0
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__MASK        0x00000007
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_20_REG_SET.uart4_rx - sets function_select bits of UART4_RX pad */
/* sets function_select bits of UART4_RX pad */
/* 0b000 : uart_gpio[7] */
/* 0b001 : u4.rxd_4 */
/* 0b010 : u3.cts_3_mux2 */
/* 0b011 : au.urfs_1_mux1 */
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__SHIFT       4
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_20_REG_SET__UART4_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_20_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_20_REG register */
#define SW_TOP_FUNC_SEL_20_REG_CLR 0x10E40124

/* SW_TOP_FUNC_SEL_20_REG_CLR.uart4_tx - clears function_select bits of UART4_TX pad */
/* clears function_select bits of UART4_TX pad */
/* 0b000 : uart_gpio[6] */
/* 0b001 : u4.txd_4 */
/* 0b010 : u3.rts_3_mux2 */
/* 0b011 : au.urfs_0_mux1 */
/* 0b100 : au.urfs_2_mux0 */
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__SHIFT       0
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__MASK        0x00000007
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_20_REG_CLR.uart4_rx - clears function_select bits of UART4_RX pad */
/* clears function_select bits of UART4_RX pad */
/* 0b000 : uart_gpio[7] */
/* 0b001 : u4.rxd_4 */
/* 0b010 : u3.cts_3_mux2 */
/* 0b011 : au.urfs_1_mux1 */
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__SHIFT       4
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_20_REG_CLR__UART4_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_21_REG register */
#define SW_TOP_FUNC_SEL_21_REG_SET 0x10E40128

/* SW_TOP_FUNC_SEL_21_REG_SET.usp0_clk - sets function_select bits of USP0_CLK pad */
/* sets function_select bits of USP0_CLK pad */
/* 0b000 : sp_gpio[4] */
/* 0b001 : au.usclk_0 */
/* 0b010 : ks.kas_spi_clk */
/* 0b100 : au.func_dbg_clk_audio */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp0_tx - sets function_select bits of USP0_TX pad */
/* sets function_select bits of USP0_TX pad */
/* 0b000 : sp_gpio[5] */
/* 0b001 : au.utxd_0 */
/* 0b010 : ks.kas_spi_do */
/* 0b011 : au.spdif_out_mux2 */
/* 0b100 : au.func_dbg_miso_audio */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__SHIFT       4
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp0_rx - sets function_select bits of USP0_RX pad */
/* sets function_select bits of USP0_RX pad */
/* 0b000 : sp_gpio[6] */
/* 0b001 : au.urxd_0 */
/* 0b010 : ks.kas_spi_di */
/* 0b100 : au.func_dbg_mosi_audio */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__SHIFT       8
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__MASK        0x00000700
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp0_fs - sets function_select bits of USP0_FS pad */
/* sets function_select bits of USP0_FS pad */
/* 0b000 : sp_gpio[7] */
/* 0b001 : au.utfs_0 */
/* 0b010 : ks.kas_spi_cs_n_mux0 */
/* 0b100 : au.func_dbg_cs_audio */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__SHIFT       12
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__MASK        0x00007000
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP0_FS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp1_clk - sets function_select bits of USP1_CLK pad */
/* sets function_select bits of USP1_CLK pad */
/* 0b000 : sp_gpio[8] */
/* 0b001 : au.usclk_1_mux0 */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__SHIFT       16
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__MASK        0x00070000
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp1_tx - sets function_select bits of USP1_TX pad */
/* sets function_select bits of USP1_TX pad */
/* 0b000 : sp_gpio[9] */
/* 0b001 : au.utxd_1_mux0 */
/* 0b010 : c1.can_txd_1_mux1 */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__SHIFT       20
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__MASK        0x00700000
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp1_rx - sets function_select bits of USP1_RX pad */
/* sets function_select bits of USP1_RX pad */
/* 0b000 : sp_gpio[10] */
/* 0b001 : au.urxd_1_mux0 */
/* 0b010 : c1.can_rxd_1_mux1 */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__SHIFT       24
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__MASK        0x07000000
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_SET.usp1_fs - sets function_select bits of USP1_FS pad */
/* sets function_select bits of USP1_FS pad */
/* 0b000 : sp_gpio[11] */
/* 0b001 : au.utfs_1_mux0 */
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__SHIFT       28
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__MASK        0x70000000
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_21_REG_SET__USP1_FS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_21_REG register */
#define SW_TOP_FUNC_SEL_21_REG_CLR 0x10E4012C

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp0_clk - clears function_select bits of USP0_CLK pad */
/* clears function_select bits of USP0_CLK pad */
/* 0b000 : sp_gpio[4] */
/* 0b001 : au.usclk_0 */
/* 0b010 : ks.kas_spi_clk */
/* 0b100 : au.func_dbg_clk_audio */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__SHIFT       0
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__MASK        0x00000007
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp0_tx - clears function_select bits of USP0_TX pad */
/* clears function_select bits of USP0_TX pad */
/* 0b000 : sp_gpio[5] */
/* 0b001 : au.utxd_0 */
/* 0b010 : ks.kas_spi_do */
/* 0b011 : au.spdif_out_mux2 */
/* 0b100 : au.func_dbg_miso_audio */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__SHIFT       4
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__MASK        0x00000070
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp0_rx - clears function_select bits of USP0_RX pad */
/* clears function_select bits of USP0_RX pad */
/* 0b000 : sp_gpio[6] */
/* 0b001 : au.urxd_0 */
/* 0b010 : ks.kas_spi_di */
/* 0b100 : au.func_dbg_mosi_audio */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__SHIFT       8
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__MASK        0x00000700
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp0_fs - clears function_select bits of USP0_FS pad */
/* clears function_select bits of USP0_FS pad */
/* 0b000 : sp_gpio[7] */
/* 0b001 : au.utfs_0 */
/* 0b010 : ks.kas_spi_cs_n_mux0 */
/* 0b100 : au.func_dbg_cs_audio */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__SHIFT       12
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__MASK        0x00007000
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP0_FS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp1_clk - clears function_select bits of USP1_CLK pad */
/* clears function_select bits of USP1_CLK pad */
/* 0b000 : sp_gpio[8] */
/* 0b001 : au.usclk_1_mux0 */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__SHIFT       16
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__MASK        0x00070000
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_CLK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp1_tx - clears function_select bits of USP1_TX pad */
/* clears function_select bits of USP1_TX pad */
/* 0b000 : sp_gpio[9] */
/* 0b001 : au.utxd_1_mux0 */
/* 0b010 : c1.can_txd_1_mux1 */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__SHIFT       20
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__MASK        0x00700000
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_TX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp1_rx - clears function_select bits of USP1_RX pad */
/* clears function_select bits of USP1_RX pad */
/* 0b000 : sp_gpio[10] */
/* 0b001 : au.urxd_1_mux0 */
/* 0b010 : c1.can_rxd_1_mux1 */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__SHIFT       24
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__MASK        0x07000000
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_RX__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_21_REG_CLR.usp1_fs - clears function_select bits of USP1_FS pad */
/* clears function_select bits of USP1_FS pad */
/* 0b000 : sp_gpio[11] */
/* 0b001 : au.utfs_1_mux0 */
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__SHIFT       28
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__WIDTH       3
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__MASK        0x70000000
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_21_REG_CLR__USP1_FS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_22_REG register */
#define SW_TOP_FUNC_SEL_22_REG_SET 0x10E40130

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d4p - sets function_select bits of LVDS_TX0D4P pad */
/* sets function_select bits of LVDS_TX0D4P pad */
/* 0b000 : lvds_gpio[8] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__MASK        0x00000007
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d4n - sets function_select bits of LVDS_TX0D4N pad */
/* sets function_select bits of LVDS_TX0D4N pad */
/* 0b000 : lvds_gpio[9] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__SHIFT       4
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__MASK        0x00000070
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D4N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d3p - sets function_select bits of LVDS_TX0D3P pad */
/* sets function_select bits of LVDS_TX0D3P pad */
/* 0b000 : lvds_gpio[6] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__SHIFT       8
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__MASK        0x00000700
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d3n - sets function_select bits of LVDS_TX0D3N pad */
/* sets function_select bits of LVDS_TX0D3N pad */
/* 0b000 : lvds_gpio[7] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__SHIFT       12
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__MASK        0x00007000
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D3N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d2p - sets function_select bits of LVDS_TX0D2P pad */
/* sets function_select bits of LVDS_TX0D2P pad */
/* 0b000 : lvds_gpio[4] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__SHIFT       16
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__MASK        0x00070000
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d2n - sets function_select bits of LVDS_TX0D2N pad */
/* sets function_select bits of LVDS_TX0D2N pad */
/* 0b000 : lvds_gpio[5] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__SHIFT       20
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__MASK        0x00700000
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D2N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d1p - sets function_select bits of LVDS_TX0D1P pad */
/* sets function_select bits of LVDS_TX0D1P pad */
/* 0b000 : lvds_gpio[2] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__SHIFT       24
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__MASK        0x07000000
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_SET.lvds_tx0d1n - sets function_select bits of LVDS_TX0D1N pad */
/* sets function_select bits of LVDS_TX0D1N pad */
/* 0b000 : lvds_gpio[3] */
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__SHIFT       28
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__MASK        0x70000000
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_22_REG_SET__LVDS_TX0D1N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_22_REG register */
#define SW_TOP_FUNC_SEL_22_REG_CLR 0x10E40134

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d4p - clears function_select bits of LVDS_TX0D4P pad */
/* clears function_select bits of LVDS_TX0D4P pad */
/* 0b000 : lvds_gpio[8] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__MASK        0x00000007
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d4n - clears function_select bits of LVDS_TX0D4N pad */
/* clears function_select bits of LVDS_TX0D4N pad */
/* 0b000 : lvds_gpio[9] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__SHIFT       4
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__MASK        0x00000070
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D4N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d3p - clears function_select bits of LVDS_TX0D3P pad */
/* clears function_select bits of LVDS_TX0D3P pad */
/* 0b000 : lvds_gpio[6] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__SHIFT       8
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__MASK        0x00000700
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d3n - clears function_select bits of LVDS_TX0D3N pad */
/* clears function_select bits of LVDS_TX0D3N pad */
/* 0b000 : lvds_gpio[7] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__SHIFT       12
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__MASK        0x00007000
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D3N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d2p - clears function_select bits of LVDS_TX0D2P pad */
/* clears function_select bits of LVDS_TX0D2P pad */
/* 0b000 : lvds_gpio[4] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__SHIFT       16
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__MASK        0x00070000
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d2n - clears function_select bits of LVDS_TX0D2N pad */
/* clears function_select bits of LVDS_TX0D2N pad */
/* 0b000 : lvds_gpio[5] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__SHIFT       20
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__MASK        0x00700000
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__INV_MASK    0xFF8FFFFF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D2N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d1p - clears function_select bits of LVDS_TX0D1P pad */
/* clears function_select bits of LVDS_TX0D1P pad */
/* 0b000 : lvds_gpio[2] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__SHIFT       24
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__MASK        0x07000000
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__INV_MASK    0xF8FFFFFF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_22_REG_CLR.lvds_tx0d1n - clears function_select bits of LVDS_TX0D1N pad */
/* clears function_select bits of LVDS_TX0D1N pad */
/* 0b000 : lvds_gpio[3] */
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__SHIFT       28
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__WIDTH       3
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__MASK        0x70000000
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__INV_MASK    0x8FFFFFFF
#define SW_TOP_FUNC_SEL_22_REG_CLR__LVDS_TX0D1N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_23_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_23_REG register */
#define SW_TOP_FUNC_SEL_23_REG_SET 0x10E40138

/* SW_TOP_FUNC_SEL_23_REG_SET.lvds_tx0d0p - sets function_select bits of LVDS_TX0D0P pad */
/* sets function_select bits of LVDS_TX0D0P pad */
/* 0b000 : lvds_gpio[0] */
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__SHIFT       0
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__WIDTH       3
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__MASK        0x00000007
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_23_REG_SET.lvds_tx0d0n - sets function_select bits of LVDS_TX0D0N pad */
/* sets function_select bits of LVDS_TX0D0N pad */
/* 0b000 : lvds_gpio[1] */
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__SHIFT       4
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__WIDTH       3
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__MASK        0x00000070
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_23_REG_SET__LVDS_TX0D0N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_23_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_23_REG register */
#define SW_TOP_FUNC_SEL_23_REG_CLR 0x10E4013C

/* SW_TOP_FUNC_SEL_23_REG_CLR.lvds_tx0d0p - clears function_select bits of LVDS_TX0D0P pad */
/* clears function_select bits of LVDS_TX0D0P pad */
/* 0b000 : lvds_gpio[0] */
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__SHIFT       0
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__WIDTH       3
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__MASK        0x00000007
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0P__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_23_REG_CLR.lvds_tx0d0n - clears function_select bits of LVDS_TX0D0N pad */
/* clears function_select bits of LVDS_TX0D0N pad */
/* 0b000 : lvds_gpio[1] */
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__SHIFT       4
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__WIDTH       3
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__MASK        0x00000070
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_23_REG_CLR__LVDS_TX0D0N__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_SET Address */
/* Sets selected bits of SW_TOP_FUNC_SEL_24_REG register */
#define SW_TOP_FUNC_SEL_24_REG_SET 0x10E40140

/* SW_TOP_FUNC_SEL_24_REG_SET.jtag_tdo - sets function_select bits of JTAG_TDO pad */
/* sets function_select bits of JTAG_TDO pad */
/* 0b000 : jtag_gpio[0] */
/* 0b001 : jtag.tdo_mux1 */
/* 0b010 : u2.txd_2_mux1 */
/* 0b101 : pw.pwm0_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__SHIFT       0
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__MASK        0x00000007
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_SET.jtag_tms - sets function_select bits of JTAG_TMS pad */
/* sets function_select bits of JTAG_TMS pad */
/* 0b000 : jtag_gpio[1] */
/* 0b001 : jtag.swdiotms_mux1 */
/* 0b010 : u2.rxd_2_mux1 */
/* 0b101 : pw.pwm1_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__SHIFT       4
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__MASK        0x00000070
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TMS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_SET.jtag_tck - sets function_select bits of JTAG_TCK pad */
/* sets function_select bits of JTAG_TCK pad */
/* 0b000 : jtag_gpio[2] */
/* 0b001 : jtag.tck_mux1 */
/* 0b010 : u2.rts_2_mux1 */
/* 0b011 : u3.txd_3_mux2 */
/* 0b100 : c1.can_txd_1_mux3 */
/* 0b101 : pw.pwm2_mux2 */
/* 0b110 : sd2.sd_cd_b_2_mux1 */
/* 0b111 : au.digmic_mux2 */
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__SHIFT       8
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_SET.jtag_tdi - sets function_select bits of JTAG_TDI pad */
/* sets function_select bits of JTAG_TDI pad */
/* 0b000 : jtag_gpio[3] */
/* 0b001 : jtag.tdi_mux1 */
/* 0b010 : u2.cts_2_mux1 */
/* 0b011 : u3.rxd_3_mux2 */
/* 0b100 : c1.can_rxd_1_mux3 */
/* 0b101 : pw.cko_0_mux3 */
/* 0b110 : au.urfs_0_mux3 */
/* 0b111 : usb0.drvvbus_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__SHIFT       12
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__MASK        0x00007000
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TDI__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_SET.jtag_trstn - sets function_select bits of JTAG_TRSTN pad */
/* sets function_select bits of JTAG_TRSTN pad */
/* 0b000 : jtag_gpio[4] */
/* 0b001 : jtag.ntrst_mux1 */
/* 0b010 : usb1.drvvbus_mux1 */
/* 0b011 : au.urfs_0_mux2 */
/* 0b100 : au.urfs_1_mux2 */
/* 0b101 : pw.cko_1_mux2 */
/* 0b110 : au.urfs_2_mux1 */
/* 0b111 : sd2.sd_wp_b_2_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__SHIFT       16
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__MASK        0x00070000
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_24_REG_SET__JTAG_TRSTN__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_CLR Address */
/* Clears selected bits of SW_TOP_FUNC_SEL_24_REG register */
#define SW_TOP_FUNC_SEL_24_REG_CLR 0x10E40144

/* SW_TOP_FUNC_SEL_24_REG_CLR.jtag_tdo - clears function_select bits of JTAG_TDO pad */
/* clears function_select bits of JTAG_TDO pad */
/* 0b000 : jtag_gpio[0] */
/* 0b001 : jtag.tdo_mux1 */
/* 0b010 : u2.txd_2_mux1 */
/* 0b101 : pw.pwm0_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__SHIFT       0
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__MASK        0x00000007
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__INV_MASK    0xFFFFFFF8
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDO__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_CLR.jtag_tms - clears function_select bits of JTAG_TMS pad */
/* clears function_select bits of JTAG_TMS pad */
/* 0b000 : jtag_gpio[1] */
/* 0b001 : jtag.swdiotms_mux1 */
/* 0b010 : u2.rxd_2_mux1 */
/* 0b101 : pw.pwm1_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__SHIFT       4
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__MASK        0x00000070
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__INV_MASK    0xFFFFFF8F
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TMS__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_CLR.jtag_tck - clears function_select bits of JTAG_TCK pad */
/* clears function_select bits of JTAG_TCK pad */
/* 0b000 : jtag_gpio[2] */
/* 0b001 : jtag.tck_mux1 */
/* 0b010 : u2.rts_2_mux1 */
/* 0b011 : u3.txd_3_mux2 */
/* 0b100 : c1.can_txd_1_mux3 */
/* 0b101 : pw.pwm2_mux2 */
/* 0b110 : sd2.sd_cd_b_2_mux1 */
/* 0b111 : au.digmic_mux2 */
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__SHIFT       8
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__MASK        0x00000700
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__INV_MASK    0xFFFFF8FF
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TCK__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_CLR.jtag_tdi - clears function_select bits of JTAG_TDI pad */
/* clears function_select bits of JTAG_TDI pad */
/* 0b000 : jtag_gpio[3] */
/* 0b001 : jtag.tdi_mux1 */
/* 0b010 : u2.cts_2_mux1 */
/* 0b011 : u3.rxd_3_mux2 */
/* 0b100 : c1.can_rxd_1_mux3 */
/* 0b101 : pw.cko_0_mux3 */
/* 0b110 : au.urfs_0_mux3 */
/* 0b111 : usb0.drvvbus_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__SHIFT       12
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__MASK        0x00007000
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__INV_MASK    0xFFFF8FFF
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TDI__HW_DEFAULT  0x0

/* SW_TOP_FUNC_SEL_24_REG_CLR.jtag_trstn - clears function_select bits of JTAG_TRSTN pad */
/* clears function_select bits of JTAG_TRSTN pad */
/* 0b000 : jtag_gpio[4] */
/* 0b001 : jtag.ntrst_mux1 */
/* 0b010 : usb1.drvvbus_mux1 */
/* 0b011 : au.urfs_0_mux2 */
/* 0b100 : au.urfs_1_mux2 */
/* 0b101 : pw.cko_1_mux2 */
/* 0b110 : au.urfs_2_mux1 */
/* 0b111 : sd2.sd_wp_b_2_mux1 */
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__SHIFT       16
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__WIDTH       3
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__MASK        0x00070000
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__INV_MASK    0xFFF8FFFF
#define SW_TOP_FUNC_SEL_24_REG_CLR__JTAG_TRSTN__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_0_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_0_REG register */
#define SW_TOP_PULL_EN_0_REG_SET  0x10E40180

/* SW_TOP_PULL_EN_0_REG_SET.spi1_en - sets pull_select bits of SPI1_EN pad */
/* sets pull_select bits of SPI1_EN pad */
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__SHIFT       0
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__MASK        0x00000001
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_EN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_SET.spi1_clk - sets pull_select bits of SPI1_CLK pad */
/* sets pull_select bits of SPI1_CLK pad */
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__SHIFT       2
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__MASK        0x00000004
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_SET.spi1_din - sets pull_select bits of SPI1_DIN pad */
/* sets pull_select bits of SPI1_DIN pad */
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__SHIFT       4
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__MASK        0x00000010
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DIN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_SET.spi1_dout - sets pull_select bits of SPI1_DOUT pad */
/* sets pull_select bits of SPI1_DOUT pad */
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__SHIFT       6
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__MASK        0x00000040
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_0_REG_SET__SPI1_DOUT__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_0_REG register */
#define SW_TOP_PULL_EN_0_REG_CLR  0x10E40184

/* SW_TOP_PULL_EN_0_REG_CLR.spi1_en - clears pull_select bits of SPI1_EN pad */
/* clears pull_select bits of SPI1_EN pad */
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__SHIFT       0
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__MASK        0x00000001
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_EN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_CLR.spi1_clk - clears pull_select bits of SPI1_CLK pad */
/* clears pull_select bits of SPI1_CLK pad */
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__SHIFT       2
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__MASK        0x00000004
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_CLR.spi1_din - clears pull_select bits of SPI1_DIN pad */
/* clears pull_select bits of SPI1_DIN pad */
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__SHIFT       4
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__MASK        0x00000010
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DIN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_0_REG_CLR.spi1_dout - clears pull_select bits of SPI1_DOUT pad */
/* clears pull_select bits of SPI1_DOUT pad */
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__SHIFT       6
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__WIDTH       1
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__MASK        0x00000040
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_0_REG_CLR__SPI1_DOUT__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_1_REG register */
#define SW_TOP_PULL_EN_1_REG_SET  0x10E40188

/* SW_TOP_PULL_EN_1_REG_SET.trg_spi_clk - sets pull_select bits of TRG_SPI_CLK pad */
/* sets pull_select bits of TRG_SPI_CLK pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__MASK        0x00000001
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_spi_di - sets pull_select bits of TRG_SPI_DI pad */
/* sets pull_select bits of TRG_SPI_DI pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__SHIFT       2
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__MASK        0x00000004
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DI__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_spi_do - sets pull_select bits of TRG_SPI_DO pad */
/* sets pull_select bits of TRG_SPI_DO pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__SHIFT       4
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__MASK        0x00000010
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_DO__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_spi_cs_b - sets pull_select bits of TRG_SPI_CS_B pad */
/* sets pull_select bits of TRG_SPI_CS_B pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__SHIFT       6
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__MASK        0x00000040
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SPI_CS_B__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_acq_d1 - sets pull_select bits of TRG_ACQ_D1 pad */
/* sets pull_select bits of TRG_ACQ_D1 pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__SHIFT       8
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__MASK        0x00000100
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_irq_b - sets pull_select bits of TRG_IRQ_B pad */
/* sets pull_select bits of TRG_IRQ_B pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__SHIFT       10
#define SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__MASK        0x00000400
#define SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_acq_d0 - sets pull_select bits of TRG_ACQ_D0 pad */
/* sets pull_select bits of TRG_ACQ_D0 pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__SHIFT       12
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__MASK        0x00001000
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_D0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_acq_clk - sets pull_select bits of TRG_ACQ_CLK pad */
/* sets pull_select bits of TRG_ACQ_CLK pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__SHIFT       14
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__MASK        0x00004000
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_ACQ_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_SET.trg_shutdown_b_out - sets pull_select bits of TRG_SHUTDOWN_B_OUT pad */
/* sets pull_select bits of TRG_SHUTDOWN_B_OUT pad */
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT       16
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__MASK        0x00010000
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFEFFFF
#define SW_TOP_PULL_EN_1_REG_SET__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_1_REG register */
#define SW_TOP_PULL_EN_1_REG_CLR  0x10E4018C

/* SW_TOP_PULL_EN_1_REG_CLR.trg_spi_clk - clears pull_select bits of TRG_SPI_CLK pad */
/* clears pull_select bits of TRG_SPI_CLK pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__MASK        0x00000001
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_spi_di - clears pull_select bits of TRG_SPI_DI pad */
/* clears pull_select bits of TRG_SPI_DI pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__SHIFT       2
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__MASK        0x00000004
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DI__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_spi_do - clears pull_select bits of TRG_SPI_DO pad */
/* clears pull_select bits of TRG_SPI_DO pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__SHIFT       4
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__MASK        0x00000010
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_DO__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_spi_cs_b - clears pull_select bits of TRG_SPI_CS_B pad */
/* clears pull_select bits of TRG_SPI_CS_B pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__SHIFT       6
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__MASK        0x00000040
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SPI_CS_B__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_acq_d1 - clears pull_select bits of TRG_ACQ_D1 pad */
/* clears pull_select bits of TRG_ACQ_D1 pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__SHIFT       8
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__MASK        0x00000100
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_irq_b - clears pull_select bits of TRG_IRQ_B pad */
/* clears pull_select bits of TRG_IRQ_B pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__SHIFT       10
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__MASK        0x00000400
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_IRQ_B__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_acq_d0 - clears pull_select bits of TRG_ACQ_D0 pad */
/* clears pull_select bits of TRG_ACQ_D0 pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__SHIFT       12
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__MASK        0x00001000
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_D0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_acq_clk - clears pull_select bits of TRG_ACQ_CLK pad */
/* clears pull_select bits of TRG_ACQ_CLK pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__SHIFT       14
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__MASK        0x00004000
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_ACQ_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_1_REG_CLR.trg_shutdown_b_out - clears pull_select bits of TRG_SHUTDOWN_B_OUT pad */
/* clears pull_select bits of TRG_SHUTDOWN_B_OUT pad */
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__SHIFT       16
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__WIDTH       1
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK        0x00010000
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFEFFFF
#define SW_TOP_PULL_EN_1_REG_CLR__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_2_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_2_REG register */
#define SW_TOP_PULL_EN_2_REG_SET  0x10E40190

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_clk - sets pull_select bits of SDIO2_CLK pad */
/* sets pull_select bits of SDIO2_CLK pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__SHIFT       0
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_cmd - sets pull_select bits of SDIO2_CMD pad */
/* sets pull_select bits of SDIO2_CMD pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__SHIFT       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__MASK        0x0000000C
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_dat_0 - sets pull_select bits of SDIO2_DAT_0 pad */
/* sets pull_select bits of SDIO2_DAT_0 pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__SHIFT       4
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__MASK        0x00000030
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_dat_1 - sets pull_select bits of SDIO2_DAT_1 pad */
/* sets pull_select bits of SDIO2_DAT_1 pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__SHIFT       6
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__MASK        0x000000C0
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_dat_2 - sets pull_select bits of SDIO2_DAT_2 pad */
/* sets pull_select bits of SDIO2_DAT_2 pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__SHIFT       8
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__MASK        0x00000300
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_SET.sdio2_dat_3 - sets pull_select bits of SDIO2_DAT_3 pad */
/* sets pull_select bits of SDIO2_DAT_3 pad */
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__SHIFT       10
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__MASK        0x00000C00
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_2_REG_SET__SDIO2_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_2_REG register */
#define SW_TOP_PULL_EN_2_REG_CLR  0x10E40194

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_clk - clears pull_select bits of SDIO2_CLK pad */
/* clears pull_select bits of SDIO2_CLK pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__SHIFT       0
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_cmd - clears pull_select bits of SDIO2_CMD pad */
/* clears pull_select bits of SDIO2_CMD pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__SHIFT       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__MASK        0x0000000C
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_dat_0 - clears pull_select bits of SDIO2_DAT_0 pad */
/* clears pull_select bits of SDIO2_DAT_0 pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__SHIFT       4
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__MASK        0x00000030
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_dat_1 - clears pull_select bits of SDIO2_DAT_1 pad */
/* clears pull_select bits of SDIO2_DAT_1 pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__SHIFT       6
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__MASK        0x000000C0
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_dat_2 - clears pull_select bits of SDIO2_DAT_2 pad */
/* clears pull_select bits of SDIO2_DAT_2 pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__SHIFT       8
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__MASK        0x00000300
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_2_REG_CLR.sdio2_dat_3 - clears pull_select bits of SDIO2_DAT_3 pad */
/* clears pull_select bits of SDIO2_DAT_3 pad */
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__SHIFT       10
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__MASK        0x00000C00
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_2_REG_CLR__SDIO2_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_3_REG register */
#define SW_TOP_PULL_EN_3_REG_SET  0x10E40198

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_7 - sets pull_select bits of DF_AD_7 pad */
/* sets pull_select bits of DF_AD_7 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__SHIFT       0
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__MASK        0x00000003
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_7__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_6 - sets pull_select bits of DF_AD_6 pad */
/* sets pull_select bits of DF_AD_6 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__SHIFT       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__MASK        0x0000000C
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_6__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_5 - sets pull_select bits of DF_AD_5 pad */
/* sets pull_select bits of DF_AD_5 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__SHIFT       4
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__MASK        0x00000030
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_5__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_4 - sets pull_select bits of DF_AD_4 pad */
/* sets pull_select bits of DF_AD_4 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__SHIFT       6
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__MASK        0x000000C0
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_4__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_3 - sets pull_select bits of DF_AD_3 pad */
/* sets pull_select bits of DF_AD_3 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__SHIFT       8
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__MASK        0x00000300
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_2 - sets pull_select bits of DF_AD_2 pad */
/* sets pull_select bits of DF_AD_2 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__SHIFT       10
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_1 - sets pull_select bits of DF_AD_1 pad */
/* sets pull_select bits of DF_AD_1 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__SHIFT       12
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__MASK        0x00003000
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ad_0 - sets pull_select bits of DF_AD_0 pad */
/* sets pull_select bits of DF_AD_0 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__SHIFT       14
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__MASK        0x0000C000
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_AD_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_dqs - sets pull_select bits of DF_DQS pad */
/* sets pull_select bits of DF_DQS pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_DQS__SHIFT       16
#define SW_TOP_PULL_EN_3_REG_SET__DF_DQS__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_DQS__MASK        0x00030000
#define SW_TOP_PULL_EN_3_REG_SET__DF_DQS__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_DQS__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_3_REG_SET.df_cle - sets pull_select bits of DF_CLE pad */
/* sets pull_select bits of DF_CLE pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_CLE__SHIFT       18
#define SW_TOP_PULL_EN_3_REG_SET__DF_CLE__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_CLE__MASK        0x000C0000
#define SW_TOP_PULL_EN_3_REG_SET__DF_CLE__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_CLE__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_3_REG_SET.df_ale - sets pull_select bits of DF_ALE pad */
/* sets pull_select bits of DF_ALE pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_ALE__SHIFT       20
#define SW_TOP_PULL_EN_3_REG_SET__DF_ALE__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_ALE__MASK        0x00300000
#define SW_TOP_PULL_EN_3_REG_SET__DF_ALE__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_ALE__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_we_b - sets pull_select bits of DF_WE_B pad */
/* sets pull_select bits of DF_WE_B pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__SHIFT       22
#define SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__MASK        0x00C00000
#define SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_WE_B__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_re_b - sets pull_select bits of DF_RE_B pad */
/* sets pull_select bits of DF_RE_B pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__SHIFT       24
#define SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__MASK        0x03000000
#define SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_RE_B__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_ry_by - sets pull_select bits of DF_RY_BY pad */
/* sets pull_select bits of DF_RY_BY pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__SHIFT       26
#define SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__MASK        0x0C000000
#define SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_RY_BY__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_cs_b_1 - sets pull_select bits of DF_CS_B_1 pad */
/* sets pull_select bits of DF_CS_B_1 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__SHIFT       28
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__MASK        0x30000000
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_SET.df_cs_b_0 - sets pull_select bits of DF_CS_B_0 pad */
/* sets pull_select bits of DF_CS_B_0 pad */
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__SHIFT       30
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__MASK        0xC0000000
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_3_REG_SET__DF_CS_B_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_3_REG register */
#define SW_TOP_PULL_EN_3_REG_CLR  0x10E4019C

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_7 - clears pull_select bits of DF_AD_7 pad */
/* clears pull_select bits of DF_AD_7 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__SHIFT       0
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__MASK        0x00000003
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_7__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_6 - clears pull_select bits of DF_AD_6 pad */
/* clears pull_select bits of DF_AD_6 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__SHIFT       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__MASK        0x0000000C
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_6__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_5 - clears pull_select bits of DF_AD_5 pad */
/* clears pull_select bits of DF_AD_5 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__SHIFT       4
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__MASK        0x00000030
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_5__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_4 - clears pull_select bits of DF_AD_4 pad */
/* clears pull_select bits of DF_AD_4 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__SHIFT       6
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__MASK        0x000000C0
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_4__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_3 - clears pull_select bits of DF_AD_3 pad */
/* clears pull_select bits of DF_AD_3 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__SHIFT       8
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__MASK        0x00000300
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_2 - clears pull_select bits of DF_AD_2 pad */
/* clears pull_select bits of DF_AD_2 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__SHIFT       10
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_1 - clears pull_select bits of DF_AD_1 pad */
/* clears pull_select bits of DF_AD_1 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__SHIFT       12
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__MASK        0x00003000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ad_0 - clears pull_select bits of DF_AD_0 pad */
/* clears pull_select bits of DF_AD_0 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__SHIFT       14
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__MASK        0x0000C000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_AD_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_dqs - clears pull_select bits of DF_DQS pad */
/* clears pull_select bits of DF_DQS pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__SHIFT       16
#define SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__MASK        0x00030000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_DQS__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_3_REG_CLR.df_cle - clears pull_select bits of DF_CLE pad */
/* clears pull_select bits of DF_CLE pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__SHIFT       18
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__MASK        0x000C0000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CLE__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_3_REG_CLR.df_ale - clears pull_select bits of DF_ALE pad */
/* clears pull_select bits of DF_ALE pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__SHIFT       20
#define SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__MASK        0x00300000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_ALE__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_we_b - clears pull_select bits of DF_WE_B pad */
/* clears pull_select bits of DF_WE_B pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__SHIFT       22
#define SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__MASK        0x00C00000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_WE_B__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_re_b - clears pull_select bits of DF_RE_B pad */
/* clears pull_select bits of DF_RE_B pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__SHIFT       24
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__MASK        0x03000000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RE_B__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_ry_by - clears pull_select bits of DF_RY_BY pad */
/* clears pull_select bits of DF_RY_BY pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__SHIFT       26
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__MASK        0x0C000000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_RY_BY__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_cs_b_1 - clears pull_select bits of DF_CS_B_1 pad */
/* clears pull_select bits of DF_CS_B_1 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__SHIFT       28
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__MASK        0x30000000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_3_REG_CLR.df_cs_b_0 - clears pull_select bits of DF_CS_B_0 pad */
/* clears pull_select bits of DF_CS_B_0 pad */
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__SHIFT       30
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__WIDTH       2
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__MASK        0xC0000000
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_3_REG_CLR__DF_CS_B_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_4_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_4_REG register */
#define SW_TOP_PULL_EN_4_REG_SET  0x10E401A0

/* SW_TOP_PULL_EN_4_REG_SET.l_pclk - sets pull_select bits of L_PCLK pad */
/* sets pull_select bits of L_PCLK pad */
#define SW_TOP_PULL_EN_4_REG_SET__L_PCLK__SHIFT       0
#define SW_TOP_PULL_EN_4_REG_SET__L_PCLK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__L_PCLK__MASK        0x00000003
#define SW_TOP_PULL_EN_4_REG_SET__L_PCLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_4_REG_SET__L_PCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.l_lck - sets pull_select bits of L_LCK pad */
/* sets pull_select bits of L_LCK pad */
#define SW_TOP_PULL_EN_4_REG_SET__L_LCK__SHIFT       2
#define SW_TOP_PULL_EN_4_REG_SET__L_LCK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__L_LCK__MASK        0x0000000C
#define SW_TOP_PULL_EN_4_REG_SET__L_LCK__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_4_REG_SET__L_LCK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.l_fck - sets pull_select bits of L_FCK pad */
/* sets pull_select bits of L_FCK pad */
#define SW_TOP_PULL_EN_4_REG_SET__L_FCK__SHIFT       4
#define SW_TOP_PULL_EN_4_REG_SET__L_FCK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__L_FCK__MASK        0x00000030
#define SW_TOP_PULL_EN_4_REG_SET__L_FCK__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_4_REG_SET__L_FCK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.l_de - sets pull_select bits of L_DE pad */
/* sets pull_select bits of L_DE pad */
#define SW_TOP_PULL_EN_4_REG_SET__L_DE__SHIFT       6
#define SW_TOP_PULL_EN_4_REG_SET__L_DE__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__L_DE__MASK        0x000000C0
#define SW_TOP_PULL_EN_4_REG_SET__L_DE__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_4_REG_SET__L_DE__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_0 - sets pull_select bits of LDD_0 pad */
/* sets pull_select bits of LDD_0 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_0__SHIFT       8
#define SW_TOP_PULL_EN_4_REG_SET__LDD_0__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_0__MASK        0x00000300
#define SW_TOP_PULL_EN_4_REG_SET__LDD_0__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_1 - sets pull_select bits of LDD_1 pad */
/* sets pull_select bits of LDD_1 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_1__SHIFT       10
#define SW_TOP_PULL_EN_4_REG_SET__LDD_1__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_1__MASK        0x00000C00
#define SW_TOP_PULL_EN_4_REG_SET__LDD_1__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_2 - sets pull_select bits of LDD_2 pad */
/* sets pull_select bits of LDD_2 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_2__SHIFT       12
#define SW_TOP_PULL_EN_4_REG_SET__LDD_2__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_2__MASK        0x00003000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_2__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_3 - sets pull_select bits of LDD_3 pad */
/* sets pull_select bits of LDD_3 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_3__SHIFT       14
#define SW_TOP_PULL_EN_4_REG_SET__LDD_3__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_3__MASK        0x0000C000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_3__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_4 - sets pull_select bits of LDD_4 pad */
/* sets pull_select bits of LDD_4 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_4__SHIFT       16
#define SW_TOP_PULL_EN_4_REG_SET__LDD_4__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_4__MASK        0x00030000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_4__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_4__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_5 - sets pull_select bits of LDD_5 pad */
/* sets pull_select bits of LDD_5 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_5__SHIFT       18
#define SW_TOP_PULL_EN_4_REG_SET__LDD_5__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_5__MASK        0x000C0000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_5__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_5__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_6 - sets pull_select bits of LDD_6 pad */
/* sets pull_select bits of LDD_6 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_6__SHIFT       20
#define SW_TOP_PULL_EN_4_REG_SET__LDD_6__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_6__MASK        0x00300000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_6__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_6__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_7 - sets pull_select bits of LDD_7 pad */
/* sets pull_select bits of LDD_7 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_7__SHIFT       22
#define SW_TOP_PULL_EN_4_REG_SET__LDD_7__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_7__MASK        0x00C00000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_7__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_7__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_8 - sets pull_select bits of LDD_8 pad */
/* sets pull_select bits of LDD_8 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_8__SHIFT       24
#define SW_TOP_PULL_EN_4_REG_SET__LDD_8__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_8__MASK        0x03000000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_8__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_8__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_9 - sets pull_select bits of LDD_9 pad */
/* sets pull_select bits of LDD_9 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_9__SHIFT       26
#define SW_TOP_PULL_EN_4_REG_SET__LDD_9__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_9__MASK        0x0C000000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_9__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_9__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_10 - sets pull_select bits of LDD_10 pad */
/* sets pull_select bits of LDD_10 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_10__SHIFT       28
#define SW_TOP_PULL_EN_4_REG_SET__LDD_10__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_10__MASK        0x30000000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_10__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_10__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_SET.ldd_11 - sets pull_select bits of LDD_11 pad */
/* sets pull_select bits of LDD_11 pad */
#define SW_TOP_PULL_EN_4_REG_SET__LDD_11__SHIFT       30
#define SW_TOP_PULL_EN_4_REG_SET__LDD_11__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_SET__LDD_11__MASK        0xC0000000
#define SW_TOP_PULL_EN_4_REG_SET__LDD_11__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_4_REG_SET__LDD_11__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_4_REG register */
#define SW_TOP_PULL_EN_4_REG_CLR  0x10E401A4

/* SW_TOP_PULL_EN_4_REG_CLR.l_pclk - clears pull_select bits of L_PCLK pad */
/* clears pull_select bits of L_PCLK pad */
#define SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__SHIFT       0
#define SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__MASK        0x00000003
#define SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_4_REG_CLR__L_PCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.l_lck - clears pull_select bits of L_LCK pad */
/* clears pull_select bits of L_LCK pad */
#define SW_TOP_PULL_EN_4_REG_CLR__L_LCK__SHIFT       2
#define SW_TOP_PULL_EN_4_REG_CLR__L_LCK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__L_LCK__MASK        0x0000000C
#define SW_TOP_PULL_EN_4_REG_CLR__L_LCK__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_4_REG_CLR__L_LCK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.l_fck - clears pull_select bits of L_FCK pad */
/* clears pull_select bits of L_FCK pad */
#define SW_TOP_PULL_EN_4_REG_CLR__L_FCK__SHIFT       4
#define SW_TOP_PULL_EN_4_REG_CLR__L_FCK__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__L_FCK__MASK        0x00000030
#define SW_TOP_PULL_EN_4_REG_CLR__L_FCK__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_4_REG_CLR__L_FCK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.l_de - clears pull_select bits of L_DE pad */
/* clears pull_select bits of L_DE pad */
#define SW_TOP_PULL_EN_4_REG_CLR__L_DE__SHIFT       6
#define SW_TOP_PULL_EN_4_REG_CLR__L_DE__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__L_DE__MASK        0x000000C0
#define SW_TOP_PULL_EN_4_REG_CLR__L_DE__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_4_REG_CLR__L_DE__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_0 - clears pull_select bits of LDD_0 pad */
/* clears pull_select bits of LDD_0 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_0__SHIFT       8
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_0__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_0__MASK        0x00000300
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_0__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_1 - clears pull_select bits of LDD_1 pad */
/* clears pull_select bits of LDD_1 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_1__SHIFT       10
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_1__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_1__MASK        0x00000C00
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_1__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_2 - clears pull_select bits of LDD_2 pad */
/* clears pull_select bits of LDD_2 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_2__SHIFT       12
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_2__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_2__MASK        0x00003000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_2__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_3 - clears pull_select bits of LDD_3 pad */
/* clears pull_select bits of LDD_3 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_3__SHIFT       14
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_3__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_3__MASK        0x0000C000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_3__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_4 - clears pull_select bits of LDD_4 pad */
/* clears pull_select bits of LDD_4 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_4__SHIFT       16
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_4__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_4__MASK        0x00030000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_4__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_4__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_5 - clears pull_select bits of LDD_5 pad */
/* clears pull_select bits of LDD_5 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_5__SHIFT       18
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_5__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_5__MASK        0x000C0000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_5__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_5__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_6 - clears pull_select bits of LDD_6 pad */
/* clears pull_select bits of LDD_6 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_6__SHIFT       20
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_6__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_6__MASK        0x00300000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_6__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_6__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_7 - clears pull_select bits of LDD_7 pad */
/* clears pull_select bits of LDD_7 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_7__SHIFT       22
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_7__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_7__MASK        0x00C00000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_7__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_7__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_8 - clears pull_select bits of LDD_8 pad */
/* clears pull_select bits of LDD_8 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_8__SHIFT       24
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_8__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_8__MASK        0x03000000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_8__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_8__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_9 - clears pull_select bits of LDD_9 pad */
/* clears pull_select bits of LDD_9 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_9__SHIFT       26
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_9__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_9__MASK        0x0C000000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_9__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_9__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_10 - clears pull_select bits of LDD_10 pad */
/* clears pull_select bits of LDD_10 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_10__SHIFT       28
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_10__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_10__MASK        0x30000000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_10__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_10__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_4_REG_CLR.ldd_11 - clears pull_select bits of LDD_11 pad */
/* clears pull_select bits of LDD_11 pad */
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_11__SHIFT       30
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_11__WIDTH       2
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_11__MASK        0xC0000000
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_11__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_4_REG_CLR__LDD_11__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_5_REG register */
#define SW_TOP_PULL_EN_5_REG_SET  0x10E401A8

/* SW_TOP_PULL_EN_5_REG_SET.ldd_12 - sets pull_select bits of LDD_12 pad */
/* sets pull_select bits of LDD_12 pad */
#define SW_TOP_PULL_EN_5_REG_SET__LDD_12__SHIFT       0
#define SW_TOP_PULL_EN_5_REG_SET__LDD_12__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_SET__LDD_12__MASK        0x00000003
#define SW_TOP_PULL_EN_5_REG_SET__LDD_12__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_5_REG_SET__LDD_12__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_SET.ldd_13 - sets pull_select bits of LDD_13 pad */
/* sets pull_select bits of LDD_13 pad */
#define SW_TOP_PULL_EN_5_REG_SET__LDD_13__SHIFT       2
#define SW_TOP_PULL_EN_5_REG_SET__LDD_13__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_SET__LDD_13__MASK        0x0000000C
#define SW_TOP_PULL_EN_5_REG_SET__LDD_13__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_5_REG_SET__LDD_13__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_SET.ldd_14 - sets pull_select bits of LDD_14 pad */
/* sets pull_select bits of LDD_14 pad */
#define SW_TOP_PULL_EN_5_REG_SET__LDD_14__SHIFT       4
#define SW_TOP_PULL_EN_5_REG_SET__LDD_14__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_SET__LDD_14__MASK        0x00000030
#define SW_TOP_PULL_EN_5_REG_SET__LDD_14__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_5_REG_SET__LDD_14__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_SET.ldd_15 - sets pull_select bits of LDD_15 pad */
/* sets pull_select bits of LDD_15 pad */
#define SW_TOP_PULL_EN_5_REG_SET__LDD_15__SHIFT       6
#define SW_TOP_PULL_EN_5_REG_SET__LDD_15__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_SET__LDD_15__MASK        0x000000C0
#define SW_TOP_PULL_EN_5_REG_SET__LDD_15__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_5_REG_SET__LDD_15__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_SET.lcd_gpio_20 - sets pull_select bits of LCD_GPIO_20 pad */
/* sets pull_select bits of LCD_GPIO_20 pad */
#define SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__SHIFT       8
#define SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__MASK        0x00000300
#define SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_5_REG_SET__LCD_GPIO_20__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_5_REG register */
#define SW_TOP_PULL_EN_5_REG_CLR  0x10E401AC

/* SW_TOP_PULL_EN_5_REG_CLR.ldd_12 - clears pull_select bits of LDD_12 pad */
/* clears pull_select bits of LDD_12 pad */
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_12__SHIFT       0
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_12__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_12__MASK        0x00000003
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_12__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_12__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_CLR.ldd_13 - clears pull_select bits of LDD_13 pad */
/* clears pull_select bits of LDD_13 pad */
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_13__SHIFT       2
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_13__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_13__MASK        0x0000000C
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_13__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_13__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_CLR.ldd_14 - clears pull_select bits of LDD_14 pad */
/* clears pull_select bits of LDD_14 pad */
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_14__SHIFT       4
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_14__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_14__MASK        0x00000030
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_14__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_14__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_CLR.ldd_15 - clears pull_select bits of LDD_15 pad */
/* clears pull_select bits of LDD_15 pad */
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_15__SHIFT       6
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_15__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_15__MASK        0x000000C0
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_15__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_5_REG_CLR__LDD_15__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_5_REG_CLR.lcd_gpio_20 - clears pull_select bits of LCD_GPIO_20 pad */
/* clears pull_select bits of LCD_GPIO_20 pad */
#define SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__SHIFT       8
#define SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__WIDTH       2
#define SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__MASK        0x00000300
#define SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_5_REG_CLR__LCD_GPIO_20__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_6_REG register */
#define SW_TOP_PULL_EN_6_REG_SET  0x10E401B0

/* SW_TOP_PULL_EN_6_REG_SET.vip_0 - sets pull_select bits of VIP_0 pad */
/* sets pull_select bits of VIP_0 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_0__SHIFT       0
#define SW_TOP_PULL_EN_6_REG_SET__VIP_0__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_0__MASK        0x00000003
#define SW_TOP_PULL_EN_6_REG_SET__VIP_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_6_REG_SET__VIP_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_1 - sets pull_select bits of VIP_1 pad */
/* sets pull_select bits of VIP_1 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_1__SHIFT       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_1__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_6_REG_SET__VIP_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_6_REG_SET__VIP_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_2 - sets pull_select bits of VIP_2 pad */
/* sets pull_select bits of VIP_2 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_2__SHIFT       4
#define SW_TOP_PULL_EN_6_REG_SET__VIP_2__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_2__MASK        0x00000030
#define SW_TOP_PULL_EN_6_REG_SET__VIP_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_3 - sets pull_select bits of VIP_3 pad */
/* sets pull_select bits of VIP_3 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_3__SHIFT       6
#define SW_TOP_PULL_EN_6_REG_SET__VIP_3__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_6_REG_SET__VIP_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_6_REG_SET__VIP_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_4 - sets pull_select bits of VIP_4 pad */
/* sets pull_select bits of VIP_4 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_4__SHIFT       8
#define SW_TOP_PULL_EN_6_REG_SET__VIP_4__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_4__MASK        0x00000300
#define SW_TOP_PULL_EN_6_REG_SET__VIP_4__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_4__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_5 - sets pull_select bits of VIP_5 pad */
/* sets pull_select bits of VIP_5 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_5__SHIFT       10
#define SW_TOP_PULL_EN_6_REG_SET__VIP_5__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_5__MASK        0x00000C00
#define SW_TOP_PULL_EN_6_REG_SET__VIP_5__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_5__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_6 - sets pull_select bits of VIP_6 pad */
/* sets pull_select bits of VIP_6 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_6__SHIFT       12
#define SW_TOP_PULL_EN_6_REG_SET__VIP_6__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_6__MASK        0x00003000
#define SW_TOP_PULL_EN_6_REG_SET__VIP_6__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_6__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_7 - sets pull_select bits of VIP_7 pad */
/* sets pull_select bits of VIP_7 pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_7__SHIFT       14
#define SW_TOP_PULL_EN_6_REG_SET__VIP_7__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_7__MASK        0x0000C000
#define SW_TOP_PULL_EN_6_REG_SET__VIP_7__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_7__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_pxclk - sets pull_select bits of VIP_PXCLK pad */
/* sets pull_select bits of VIP_PXCLK pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__SHIFT       16
#define SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__MASK        0x00030000
#define SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_PXCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_hsync - sets pull_select bits of VIP_HSYNC pad */
/* sets pull_select bits of VIP_HSYNC pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__SHIFT       18
#define SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__MASK        0x000C0000
#define SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_HSYNC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_SET.vip_vsync - sets pull_select bits of VIP_VSYNC pad */
/* sets pull_select bits of VIP_VSYNC pad */
#define SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__SHIFT       20
#define SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__MASK        0x00300000
#define SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_6_REG_SET__VIP_VSYNC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_6_REG register */
#define SW_TOP_PULL_EN_6_REG_CLR  0x10E401B4

/* SW_TOP_PULL_EN_6_REG_CLR.vip_0 - clears pull_select bits of VIP_0 pad */
/* clears pull_select bits of VIP_0 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_0__SHIFT       0
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_0__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_0__MASK        0x00000003
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_1 - clears pull_select bits of VIP_1 pad */
/* clears pull_select bits of VIP_1 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_1__SHIFT       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_1__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_2 - clears pull_select bits of VIP_2 pad */
/* clears pull_select bits of VIP_2 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_2__SHIFT       4
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_2__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_2__MASK        0x00000030
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_3 - clears pull_select bits of VIP_3 pad */
/* clears pull_select bits of VIP_3 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_3__SHIFT       6
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_3__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_4 - clears pull_select bits of VIP_4 pad */
/* clears pull_select bits of VIP_4 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_4__SHIFT       8
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_4__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_4__MASK        0x00000300
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_4__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_4__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_5 - clears pull_select bits of VIP_5 pad */
/* clears pull_select bits of VIP_5 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_5__SHIFT       10
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_5__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_5__MASK        0x00000C00
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_5__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_5__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_6 - clears pull_select bits of VIP_6 pad */
/* clears pull_select bits of VIP_6 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_6__SHIFT       12
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_6__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_6__MASK        0x00003000
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_6__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_6__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_7 - clears pull_select bits of VIP_7 pad */
/* clears pull_select bits of VIP_7 pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_7__SHIFT       14
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_7__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_7__MASK        0x0000C000
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_7__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_7__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_pxclk - clears pull_select bits of VIP_PXCLK pad */
/* clears pull_select bits of VIP_PXCLK pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__SHIFT       16
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__MASK        0x00030000
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_PXCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_hsync - clears pull_select bits of VIP_HSYNC pad */
/* clears pull_select bits of VIP_HSYNC pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__SHIFT       18
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__MASK        0x000C0000
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_HSYNC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_6_REG_CLR.vip_vsync - clears pull_select bits of VIP_VSYNC pad */
/* clears pull_select bits of VIP_VSYNC pad */
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__SHIFT       20
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__WIDTH       2
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__MASK        0x00300000
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_6_REG_CLR__VIP_VSYNC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_7_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_7_REG register */
#define SW_TOP_PULL_EN_7_REG_SET  0x10E401B8

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_clk - sets pull_select bits of SDIO3_CLK pad */
/* sets pull_select bits of SDIO3_CLK pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__SHIFT       0
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_cmd - sets pull_select bits of SDIO3_CMD pad */
/* sets pull_select bits of SDIO3_CMD pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__SHIFT       4
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__MASK        0x00000030
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_dat_0 - sets pull_select bits of SDIO3_DAT_0 pad */
/* sets pull_select bits of SDIO3_DAT_0 pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__SHIFT       6
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__MASK        0x000000C0
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_dat_1 - sets pull_select bits of SDIO3_DAT_1 pad */
/* sets pull_select bits of SDIO3_DAT_1 pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__SHIFT       8
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__MASK        0x00000300
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_dat_2 - sets pull_select bits of SDIO3_DAT_2 pad */
/* sets pull_select bits of SDIO3_DAT_2 pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__SHIFT       10
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_SET.sdio3_dat_3 - sets pull_select bits of SDIO3_DAT_3 pad */
/* sets pull_select bits of SDIO3_DAT_3 pad */
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__SHIFT       12
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__MASK        0x00003000
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_7_REG_SET__SDIO3_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_7_REG register */
#define SW_TOP_PULL_EN_7_REG_CLR  0x10E401BC

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_clk - clears pull_select bits of SDIO3_CLK pad */
/* clears pull_select bits of SDIO3_CLK pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__SHIFT       0
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_cmd - clears pull_select bits of SDIO3_CMD pad */
/* clears pull_select bits of SDIO3_CMD pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__SHIFT       4
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__MASK        0x00000030
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_dat_0 - clears pull_select bits of SDIO3_DAT_0 pad */
/* clears pull_select bits of SDIO3_DAT_0 pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__SHIFT       6
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__MASK        0x000000C0
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_dat_1 - clears pull_select bits of SDIO3_DAT_1 pad */
/* clears pull_select bits of SDIO3_DAT_1 pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__SHIFT       8
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__MASK        0x00000300
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_dat_2 - clears pull_select bits of SDIO3_DAT_2 pad */
/* clears pull_select bits of SDIO3_DAT_2 pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__SHIFT       10
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_7_REG_CLR.sdio3_dat_3 - clears pull_select bits of SDIO3_DAT_3 pad */
/* clears pull_select bits of SDIO3_DAT_3 pad */
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__SHIFT       12
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__MASK        0x00003000
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_7_REG_CLR__SDIO3_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_8_REG register */
#define SW_TOP_PULL_EN_8_REG_SET  0x10E401C0

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_clk - sets pull_select bits of SDIO5_CLK pad */
/* sets pull_select bits of SDIO5_CLK pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__SHIFT       0
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_cmd - sets pull_select bits of SDIO5_CMD pad */
/* sets pull_select bits of SDIO5_CMD pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__SHIFT       4
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__MASK        0x00000030
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_dat_0 - sets pull_select bits of SDIO5_DAT_0 pad */
/* sets pull_select bits of SDIO5_DAT_0 pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__SHIFT       6
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__MASK        0x000000C0
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_dat_1 - sets pull_select bits of SDIO5_DAT_1 pad */
/* sets pull_select bits of SDIO5_DAT_1 pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__SHIFT       8
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__MASK        0x00000300
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_dat_2 - sets pull_select bits of SDIO5_DAT_2 pad */
/* sets pull_select bits of SDIO5_DAT_2 pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__SHIFT       10
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_SET.sdio5_dat_3 - sets pull_select bits of SDIO5_DAT_3 pad */
/* sets pull_select bits of SDIO5_DAT_3 pad */
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__SHIFT       12
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__MASK        0x00003000
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_8_REG_SET__SDIO5_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_8_REG register */
#define SW_TOP_PULL_EN_8_REG_CLR  0x10E401C4

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_clk - clears pull_select bits of SDIO5_CLK pad */
/* clears pull_select bits of SDIO5_CLK pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__SHIFT       0
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__MASK        0x00000003
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_cmd - clears pull_select bits of SDIO5_CMD pad */
/* clears pull_select bits of SDIO5_CMD pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__SHIFT       4
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__MASK        0x00000030
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_CMD__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_dat_0 - clears pull_select bits of SDIO5_DAT_0 pad */
/* clears pull_select bits of SDIO5_DAT_0 pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__SHIFT       6
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__MASK        0x000000C0
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_0__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_dat_1 - clears pull_select bits of SDIO5_DAT_1 pad */
/* clears pull_select bits of SDIO5_DAT_1 pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__SHIFT       8
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__MASK        0x00000300
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_1__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_dat_2 - clears pull_select bits of SDIO5_DAT_2 pad */
/* clears pull_select bits of SDIO5_DAT_2 pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__SHIFT       10
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__MASK        0x00000C00
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_2__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_8_REG_CLR.sdio5_dat_3 - clears pull_select bits of SDIO5_DAT_3 pad */
/* clears pull_select bits of SDIO5_DAT_3 pad */
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__SHIFT       12
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__WIDTH       2
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__MASK        0x00003000
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_8_REG_CLR__SDIO5_DAT_3__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_9_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_9_REG register */
#define SW_TOP_PULL_EN_9_REG_SET  0x10E401C8

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_txd_0 - sets pull_select bits of RGMII_TXD_0 pad */
/* sets pull_select bits of RGMII_TXD_0 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__SHIFT       0
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__MASK        0x00000003
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_txd_1 - sets pull_select bits of RGMII_TXD_1 pad */
/* sets pull_select bits of RGMII_TXD_1 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__SHIFT       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_txd_2 - sets pull_select bits of RGMII_TXD_2 pad */
/* sets pull_select bits of RGMII_TXD_2 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__SHIFT       4
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__MASK        0x00000030
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_txd_3 - sets pull_select bits of RGMII_TXD_3 pad */
/* sets pull_select bits of RGMII_TXD_3 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__SHIFT       6
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_txclk - sets pull_select bits of RGMII_TXCLK pad */
/* sets pull_select bits of RGMII_TXCLK pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__SHIFT       8
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__MASK        0x00000300
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TXCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_tx_ctl - sets pull_select bits of RGMII_TX_CTL pad */
/* sets pull_select bits of RGMII_TX_CTL pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__SHIFT       12
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__MASK        0x00003000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_TX_CTL__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rxd_0 - sets pull_select bits of RGMII_RXD_0 pad */
/* sets pull_select bits of RGMII_RXD_0 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__SHIFT       14
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__MASK        0x0000C000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rxd_1 - sets pull_select bits of RGMII_RXD_1 pad */
/* sets pull_select bits of RGMII_RXD_1 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__SHIFT       16
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__MASK        0x00030000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rxd_2 - sets pull_select bits of RGMII_RXD_2 pad */
/* sets pull_select bits of RGMII_RXD_2 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__SHIFT       18
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__MASK        0x000C0000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rxd_3 - sets pull_select bits of RGMII_RXD_3 pad */
/* sets pull_select bits of RGMII_RXD_3 pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__SHIFT       20
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__MASK        0x00300000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rx_clk - sets pull_select bits of RGMII_RX_CLK pad */
/* sets pull_select bits of RGMII_RX_CLK pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__SHIFT       22
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__MASK        0x00C00000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RX_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_rxc_ctl - sets pull_select bits of RGMII_RXC_CTL pad */
/* sets pull_select bits of RGMII_RXC_CTL pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__SHIFT       24
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__MASK        0x03000000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_RXC_CTL__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_mdio - sets pull_select bits of RGMII_MDIO pad */
/* sets pull_select bits of RGMII_MDIO pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__SHIFT       26
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__MASK        0x0C000000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDIO__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_mdc - sets pull_select bits of RGMII_MDC pad */
/* sets pull_select bits of RGMII_MDC pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__SHIFT       28
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__MASK        0x30000000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_MDC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_SET.rgmii_intr_n - sets pull_select bits of RGMII_INTR_N pad */
/* sets pull_select bits of RGMII_INTR_N pad */
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__SHIFT       30
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__MASK        0xC0000000
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_9_REG_SET__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_9_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_9_REG register */
#define SW_TOP_PULL_EN_9_REG_CLR  0x10E401CC

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_txd_0 - clears pull_select bits of RGMII_TXD_0 pad */
/* clears pull_select bits of RGMII_TXD_0 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__SHIFT       0
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__MASK        0x00000003
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_txd_1 - clears pull_select bits of RGMII_TXD_1 pad */
/* clears pull_select bits of RGMII_TXD_1 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__SHIFT       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_txd_2 - clears pull_select bits of RGMII_TXD_2 pad */
/* clears pull_select bits of RGMII_TXD_2 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__SHIFT       4
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__MASK        0x00000030
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_txd_3 - clears pull_select bits of RGMII_TXD_3 pad */
/* clears pull_select bits of RGMII_TXD_3 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__SHIFT       6
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_txclk - clears pull_select bits of RGMII_TXCLK pad */
/* clears pull_select bits of RGMII_TXCLK pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__SHIFT       8
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__MASK        0x00000300
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TXCLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_tx_ctl - clears pull_select bits of RGMII_TX_CTL pad */
/* clears pull_select bits of RGMII_TX_CTL pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__SHIFT       12
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__MASK        0x00003000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_TX_CTL__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rxd_0 - clears pull_select bits of RGMII_RXD_0 pad */
/* clears pull_select bits of RGMII_RXD_0 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__SHIFT       14
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__MASK        0x0000C000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rxd_1 - clears pull_select bits of RGMII_RXD_1 pad */
/* clears pull_select bits of RGMII_RXD_1 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__SHIFT       16
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__MASK        0x00030000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__INV_MASK    0xFFFCFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rxd_2 - clears pull_select bits of RGMII_RXD_2 pad */
/* clears pull_select bits of RGMII_RXD_2 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__SHIFT       18
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__MASK        0x000C0000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__INV_MASK    0xFFF3FFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rxd_3 - clears pull_select bits of RGMII_RXD_3 pad */
/* clears pull_select bits of RGMII_RXD_3 pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__SHIFT       20
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__MASK        0x00300000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__INV_MASK    0xFFCFFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXD_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rx_clk - clears pull_select bits of RGMII_RX_CLK pad */
/* clears pull_select bits of RGMII_RX_CLK pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__SHIFT       22
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__MASK        0x00C00000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__INV_MASK    0xFF3FFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RX_CLK__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_rxc_ctl - clears pull_select bits of RGMII_RXC_CTL pad */
/* clears pull_select bits of RGMII_RXC_CTL pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__SHIFT       24
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__MASK        0x03000000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__INV_MASK    0xFCFFFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_RXC_CTL__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_mdio - clears pull_select bits of RGMII_MDIO pad */
/* clears pull_select bits of RGMII_MDIO pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__SHIFT       26
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__MASK        0x0C000000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__INV_MASK    0xF3FFFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDIO__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_mdc - clears pull_select bits of RGMII_MDC pad */
/* clears pull_select bits of RGMII_MDC pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__SHIFT       28
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__MASK        0x30000000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__INV_MASK    0xCFFFFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_MDC__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_9_REG_CLR.rgmii_intr_n - clears pull_select bits of RGMII_INTR_N pad */
/* clears pull_select bits of RGMII_INTR_N pad */
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__SHIFT       30
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__WIDTH       2
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__MASK        0xC0000000
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__INV_MASK    0x3FFFFFFF
#define SW_TOP_PULL_EN_9_REG_CLR__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_PULL_EN_10_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_10_REG register */
#define SW_TOP_PULL_EN_10_REG_SET 0x10E40200

/* SW_TOP_PULL_EN_10_REG_SET.i2s_mclk - sets pull_select bits of I2S_MCLK pad */
/* sets pull_select bits of I2S_MCLK pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__SHIFT       0
#define SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__MASK        0x00000001
#define SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_10_REG_SET__I2S_MCLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_bclk - sets pull_select bits of I2S_BCLK pad */
/* sets pull_select bits of I2S_BCLK pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__SHIFT       2
#define SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__MASK        0x00000004
#define SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_10_REG_SET__I2S_BCLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_ws - sets pull_select bits of I2S_WS pad */
/* sets pull_select bits of I2S_WS pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_WS__SHIFT       4
#define SW_TOP_PULL_EN_10_REG_SET__I2S_WS__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_WS__MASK        0x00000010
#define SW_TOP_PULL_EN_10_REG_SET__I2S_WS__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_10_REG_SET__I2S_WS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_dout0 - sets pull_select bits of I2S_DOUT0 pad */
/* sets pull_select bits of I2S_DOUT0 pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__SHIFT       6
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__MASK        0x00000040
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_dout1 - sets pull_select bits of I2S_DOUT1 pad */
/* sets pull_select bits of I2S_DOUT1 pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__SHIFT       8
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__MASK        0x00000100
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_dout2 - sets pull_select bits of I2S_DOUT2 pad */
/* sets pull_select bits of I2S_DOUT2 pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__SHIFT       10
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__MASK        0x00000400
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DOUT2__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_SET.i2s_din - sets pull_select bits of I2S_DIN pad */
/* sets pull_select bits of I2S_DIN pad */
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__SHIFT       12
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__MASK        0x00001000
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_10_REG_SET__I2S_DIN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_10_REG register */
#define SW_TOP_PULL_EN_10_REG_CLR 0x10E40204

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_mclk - clears pull_select bits of I2S_MCLK pad */
/* clears pull_select bits of I2S_MCLK pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__SHIFT       0
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__MASK        0x00000001
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_MCLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_bclk - clears pull_select bits of I2S_BCLK pad */
/* clears pull_select bits of I2S_BCLK pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__SHIFT       2
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__MASK        0x00000004
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_BCLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_ws - clears pull_select bits of I2S_WS pad */
/* clears pull_select bits of I2S_WS pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__SHIFT       4
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__MASK        0x00000010
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_WS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_dout0 - clears pull_select bits of I2S_DOUT0 pad */
/* clears pull_select bits of I2S_DOUT0 pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__SHIFT       6
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__MASK        0x00000040
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_dout1 - clears pull_select bits of I2S_DOUT1 pad */
/* clears pull_select bits of I2S_DOUT1 pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__SHIFT       8
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__MASK        0x00000100
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_dout2 - clears pull_select bits of I2S_DOUT2 pad */
/* clears pull_select bits of I2S_DOUT2 pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__SHIFT       10
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__MASK        0x00000400
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DOUT2__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_10_REG_CLR.i2s_din - clears pull_select bits of I2S_DIN pad */
/* clears pull_select bits of I2S_DIN pad */
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__SHIFT       12
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__WIDTH       1
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__MASK        0x00001000
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_10_REG_CLR__I2S_DIN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_11_REG register */
#define SW_TOP_PULL_EN_11_REG_SET 0x10E40250

/* SW_TOP_PULL_EN_11_REG_SET.gpio_0 - sets pull_select bits of GPIO_0 pad */
/* sets pull_select bits of GPIO_0 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_0__SHIFT       0
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_0__MASK        0x00000001
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_1 - sets pull_select bits of GPIO_1 pad */
/* sets pull_select bits of GPIO_1 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_1__SHIFT       2
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_1__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_1__MASK        0x00000004
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_2 - sets pull_select bits of GPIO_2 pad */
/* sets pull_select bits of GPIO_2 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_2__SHIFT       4
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_2__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_2__MASK        0x00000010
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_2__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_3 - sets pull_select bits of GPIO_3 pad */
/* sets pull_select bits of GPIO_3 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_3__SHIFT       6
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_3__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_3__MASK        0x00000040
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_3__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_4 - sets pull_select bits of GPIO_4 pad */
/* sets pull_select bits of GPIO_4 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_4__SHIFT       8
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_4__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_4__MASK        0x00000100
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_4__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_4__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_5 - sets pull_select bits of GPIO_5 pad */
/* sets pull_select bits of GPIO_5 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_5__SHIFT       10
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_5__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_5__MASK        0x00000400
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_5__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_5__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_6 - sets pull_select bits of GPIO_6 pad */
/* sets pull_select bits of GPIO_6 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_6__SHIFT       12
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_6__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_6__MASK        0x00001000
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_6__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_6__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.gpio_7 - sets pull_select bits of GPIO_7 pad */
/* sets pull_select bits of GPIO_7 pad */
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_7__SHIFT       14
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_7__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_7__MASK        0x00004000
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_7__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_11_REG_SET__GPIO_7__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.sda_0 - sets pull_select bits of SDA_0 pad */
/* sets pull_select bits of SDA_0 pad */
#define SW_TOP_PULL_EN_11_REG_SET__SDA_0__SHIFT       24
#define SW_TOP_PULL_EN_11_REG_SET__SDA_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__SDA_0__MASK        0x01000000
#define SW_TOP_PULL_EN_11_REG_SET__SDA_0__INV_MASK    0xFEFFFFFF
#define SW_TOP_PULL_EN_11_REG_SET__SDA_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_SET.scl_0 - sets pull_select bits of SCL_0 pad */
/* sets pull_select bits of SCL_0 pad */
#define SW_TOP_PULL_EN_11_REG_SET__SCL_0__SHIFT       26
#define SW_TOP_PULL_EN_11_REG_SET__SCL_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_SET__SCL_0__MASK        0x04000000
#define SW_TOP_PULL_EN_11_REG_SET__SCL_0__INV_MASK    0xFBFFFFFF
#define SW_TOP_PULL_EN_11_REG_SET__SCL_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_11_REG register */
#define SW_TOP_PULL_EN_11_REG_CLR 0x10E40254

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_0 - clears pull_select bits of GPIO_0 pad */
/* clears pull_select bits of GPIO_0 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__SHIFT       0
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__MASK        0x00000001
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_1 - clears pull_select bits of GPIO_1 pad */
/* clears pull_select bits of GPIO_1 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__SHIFT       2
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__MASK        0x00000004
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_1__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_2 - clears pull_select bits of GPIO_2 pad */
/* clears pull_select bits of GPIO_2 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__SHIFT       4
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__MASK        0x00000010
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_2__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_3 - clears pull_select bits of GPIO_3 pad */
/* clears pull_select bits of GPIO_3 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__SHIFT       6
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__MASK        0x00000040
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_3__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_4 - clears pull_select bits of GPIO_4 pad */
/* clears pull_select bits of GPIO_4 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__SHIFT       8
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__MASK        0x00000100
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_4__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_5 - clears pull_select bits of GPIO_5 pad */
/* clears pull_select bits of GPIO_5 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__SHIFT       10
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__MASK        0x00000400
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_5__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_6 - clears pull_select bits of GPIO_6 pad */
/* clears pull_select bits of GPIO_6 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__SHIFT       12
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__MASK        0x00001000
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_6__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.gpio_7 - clears pull_select bits of GPIO_7 pad */
/* clears pull_select bits of GPIO_7 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__SHIFT       14
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__MASK        0x00004000
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_11_REG_CLR__GPIO_7__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.sda_0 - clears pull_select bits of SDA_0 pad */
/* clears pull_select bits of SDA_0 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__SDA_0__SHIFT       24
#define SW_TOP_PULL_EN_11_REG_CLR__SDA_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__SDA_0__MASK        0x01000000
#define SW_TOP_PULL_EN_11_REG_CLR__SDA_0__INV_MASK    0xFEFFFFFF
#define SW_TOP_PULL_EN_11_REG_CLR__SDA_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_11_REG_CLR.scl_0 - clears pull_select bits of SCL_0 pad */
/* clears pull_select bits of SCL_0 pad */
#define SW_TOP_PULL_EN_11_REG_CLR__SCL_0__SHIFT       26
#define SW_TOP_PULL_EN_11_REG_CLR__SCL_0__WIDTH       1
#define SW_TOP_PULL_EN_11_REG_CLR__SCL_0__MASK        0x04000000
#define SW_TOP_PULL_EN_11_REG_CLR__SCL_0__INV_MASK    0xFBFFFFFF
#define SW_TOP_PULL_EN_11_REG_CLR__SCL_0__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_12_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_12_REG register */
#define SW_TOP_PULL_EN_12_REG_SET 0x10E40258

/* SW_TOP_PULL_EN_12_REG_SET.coex_pio_0 - sets pull_select bits of COEX_PIO_0 pad */
/* sets pull_select bits of COEX_PIO_0 pad */
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__SHIFT       0
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__MASK        0x00000003
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_SET.coex_pio_1 - sets pull_select bits of COEX_PIO_1 pad */
/* sets pull_select bits of COEX_PIO_1 pad */
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__SHIFT       2
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_SET.coex_pio_2 - sets pull_select bits of COEX_PIO_2 pad */
/* sets pull_select bits of COEX_PIO_2 pad */
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__SHIFT       4
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__MASK        0x00000030
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_SET.coex_pio_3 - sets pull_select bits of COEX_PIO_3 pad */
/* sets pull_select bits of COEX_PIO_3 pad */
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__SHIFT       6
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_12_REG_SET__COEX_PIO_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_12_REG register */
#define SW_TOP_PULL_EN_12_REG_CLR 0x10E4025C

/* SW_TOP_PULL_EN_12_REG_CLR.coex_pio_0 - clears pull_select bits of COEX_PIO_0 pad */
/* clears pull_select bits of COEX_PIO_0 pad */
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__SHIFT       0
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__MASK        0x00000003
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_0__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_CLR.coex_pio_1 - clears pull_select bits of COEX_PIO_1 pad */
/* clears pull_select bits of COEX_PIO_1 pad */
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__SHIFT       2
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__MASK        0x0000000C
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_1__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_CLR.coex_pio_2 - clears pull_select bits of COEX_PIO_2 pad */
/* clears pull_select bits of COEX_PIO_2 pad */
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__SHIFT       4
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__MASK        0x00000030
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_2__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_12_REG_CLR.coex_pio_3 - clears pull_select bits of COEX_PIO_3 pad */
/* clears pull_select bits of COEX_PIO_3 pad */
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__SHIFT       6
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__WIDTH       2
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__MASK        0x000000C0
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_12_REG_CLR__COEX_PIO_3__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_13_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_13_REG register */
#define SW_TOP_PULL_EN_13_REG_SET 0x10E40260

/* SW_TOP_PULL_EN_13_REG_SET.uart0_tx - sets pull_select bits of UART0_TX pad */
/* sets pull_select bits of UART0_TX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART0_TX__SHIFT       0
#define SW_TOP_PULL_EN_13_REG_SET__UART0_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART0_TX__MASK        0x00000001
#define SW_TOP_PULL_EN_13_REG_SET__UART0_TX__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_13_REG_SET__UART0_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart0_rx - sets pull_select bits of UART0_RX pad */
/* sets pull_select bits of UART0_RX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART0_RX__SHIFT       2
#define SW_TOP_PULL_EN_13_REG_SET__UART0_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART0_RX__MASK        0x00000004
#define SW_TOP_PULL_EN_13_REG_SET__UART0_RX__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_13_REG_SET__UART0_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart1_tx - sets pull_select bits of UART1_TX pad */
/* sets pull_select bits of UART1_TX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART1_TX__SHIFT       8
#define SW_TOP_PULL_EN_13_REG_SET__UART1_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART1_TX__MASK        0x00000100
#define SW_TOP_PULL_EN_13_REG_SET__UART1_TX__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_13_REG_SET__UART1_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart1_rx - sets pull_select bits of UART1_RX pad */
/* sets pull_select bits of UART1_RX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART1_RX__SHIFT       10
#define SW_TOP_PULL_EN_13_REG_SET__UART1_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART1_RX__MASK        0x00000400
#define SW_TOP_PULL_EN_13_REG_SET__UART1_RX__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_13_REG_SET__UART1_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart3_tx - sets pull_select bits of UART3_TX pad */
/* sets pull_select bits of UART3_TX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART3_TX__SHIFT       12
#define SW_TOP_PULL_EN_13_REG_SET__UART3_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART3_TX__MASK        0x00001000
#define SW_TOP_PULL_EN_13_REG_SET__UART3_TX__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_13_REG_SET__UART3_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart3_rx - sets pull_select bits of UART3_RX pad */
/* sets pull_select bits of UART3_RX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART3_RX__SHIFT       14
#define SW_TOP_PULL_EN_13_REG_SET__UART3_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART3_RX__MASK        0x00004000
#define SW_TOP_PULL_EN_13_REG_SET__UART3_RX__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_13_REG_SET__UART3_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart4_tx - sets pull_select bits of UART4_TX pad */
/* sets pull_select bits of UART4_TX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART4_TX__SHIFT       16
#define SW_TOP_PULL_EN_13_REG_SET__UART4_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART4_TX__MASK        0x00010000
#define SW_TOP_PULL_EN_13_REG_SET__UART4_TX__INV_MASK    0xFFFEFFFF
#define SW_TOP_PULL_EN_13_REG_SET__UART4_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_SET.uart4_rx - sets pull_select bits of UART4_RX pad */
/* sets pull_select bits of UART4_RX pad */
#define SW_TOP_PULL_EN_13_REG_SET__UART4_RX__SHIFT       18
#define SW_TOP_PULL_EN_13_REG_SET__UART4_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_SET__UART4_RX__MASK        0x00040000
#define SW_TOP_PULL_EN_13_REG_SET__UART4_RX__INV_MASK    0xFFFBFFFF
#define SW_TOP_PULL_EN_13_REG_SET__UART4_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_13_REG register */
#define SW_TOP_PULL_EN_13_REG_CLR 0x10E40264

/* SW_TOP_PULL_EN_13_REG_CLR.uart0_tx - clears pull_select bits of UART0_TX pad */
/* clears pull_select bits of UART0_TX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__SHIFT       0
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__MASK        0x00000001
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart0_rx - clears pull_select bits of UART0_RX pad */
/* clears pull_select bits of UART0_RX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__SHIFT       2
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__MASK        0x00000004
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_13_REG_CLR__UART0_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart1_tx - clears pull_select bits of UART1_TX pad */
/* clears pull_select bits of UART1_TX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__SHIFT       8
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__MASK        0x00000100
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart1_rx - clears pull_select bits of UART1_RX pad */
/* clears pull_select bits of UART1_RX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__SHIFT       10
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__MASK        0x00000400
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART1_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart3_tx - clears pull_select bits of UART3_TX pad */
/* clears pull_select bits of UART3_TX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__SHIFT       12
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__MASK        0x00001000
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart3_rx - clears pull_select bits of UART3_RX pad */
/* clears pull_select bits of UART3_RX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__SHIFT       14
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__MASK        0x00004000
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART3_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart4_tx - clears pull_select bits of UART4_TX pad */
/* clears pull_select bits of UART4_TX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__SHIFT       16
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__MASK        0x00010000
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__INV_MASK    0xFFFEFFFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_13_REG_CLR.uart4_rx - clears pull_select bits of UART4_RX pad */
/* clears pull_select bits of UART4_RX pad */
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__SHIFT       18
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__WIDTH       1
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__MASK        0x00040000
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__INV_MASK    0xFFFBFFFF
#define SW_TOP_PULL_EN_13_REG_CLR__UART4_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_14_REG register */
#define SW_TOP_PULL_EN_14_REG_SET 0x10E40268

/* SW_TOP_PULL_EN_14_REG_SET.usp0_clk - sets pull_select bits of USP0_CLK pad */
/* sets pull_select bits of USP0_CLK pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__SHIFT       0
#define SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__MASK        0x00000001
#define SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_14_REG_SET__USP0_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp0_tx - sets pull_select bits of USP0_TX pad */
/* sets pull_select bits of USP0_TX pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP0_TX__SHIFT       2
#define SW_TOP_PULL_EN_14_REG_SET__USP0_TX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP0_TX__MASK        0x00000004
#define SW_TOP_PULL_EN_14_REG_SET__USP0_TX__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_14_REG_SET__USP0_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp0_rx - sets pull_select bits of USP0_RX pad */
/* sets pull_select bits of USP0_RX pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP0_RX__SHIFT       4
#define SW_TOP_PULL_EN_14_REG_SET__USP0_RX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP0_RX__MASK        0x00000010
#define SW_TOP_PULL_EN_14_REG_SET__USP0_RX__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_14_REG_SET__USP0_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp0_fs - sets pull_select bits of USP0_FS pad */
/* sets pull_select bits of USP0_FS pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP0_FS__SHIFT       6
#define SW_TOP_PULL_EN_14_REG_SET__USP0_FS__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP0_FS__MASK        0x00000040
#define SW_TOP_PULL_EN_14_REG_SET__USP0_FS__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_14_REG_SET__USP0_FS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp1_clk - sets pull_select bits of USP1_CLK pad */
/* sets pull_select bits of USP1_CLK pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__SHIFT       8
#define SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__MASK        0x00000100
#define SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_14_REG_SET__USP1_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp1_tx - sets pull_select bits of USP1_TX pad */
/* sets pull_select bits of USP1_TX pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP1_TX__SHIFT       10
#define SW_TOP_PULL_EN_14_REG_SET__USP1_TX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP1_TX__MASK        0x00000400
#define SW_TOP_PULL_EN_14_REG_SET__USP1_TX__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_14_REG_SET__USP1_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp1_rx - sets pull_select bits of USP1_RX pad */
/* sets pull_select bits of USP1_RX pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP1_RX__SHIFT       12
#define SW_TOP_PULL_EN_14_REG_SET__USP1_RX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP1_RX__MASK        0x00001000
#define SW_TOP_PULL_EN_14_REG_SET__USP1_RX__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_14_REG_SET__USP1_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_SET.usp1_fs - sets pull_select bits of USP1_FS pad */
/* sets pull_select bits of USP1_FS pad */
#define SW_TOP_PULL_EN_14_REG_SET__USP1_FS__SHIFT       14
#define SW_TOP_PULL_EN_14_REG_SET__USP1_FS__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_SET__USP1_FS__MASK        0x00004000
#define SW_TOP_PULL_EN_14_REG_SET__USP1_FS__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_14_REG_SET__USP1_FS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_14_REG register */
#define SW_TOP_PULL_EN_14_REG_CLR 0x10E4026C

/* SW_TOP_PULL_EN_14_REG_CLR.usp0_clk - clears pull_select bits of USP0_CLK pad */
/* clears pull_select bits of USP0_CLK pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__SHIFT       0
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__MASK        0x00000001
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp0_tx - clears pull_select bits of USP0_TX pad */
/* clears pull_select bits of USP0_TX pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__SHIFT       2
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__MASK        0x00000004
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp0_rx - clears pull_select bits of USP0_RX pad */
/* clears pull_select bits of USP0_RX pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__SHIFT       4
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__MASK        0x00000010
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp0_fs - clears pull_select bits of USP0_FS pad */
/* clears pull_select bits of USP0_FS pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__SHIFT       6
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__MASK        0x00000040
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_14_REG_CLR__USP0_FS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp1_clk - clears pull_select bits of USP1_CLK pad */
/* clears pull_select bits of USP1_CLK pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__SHIFT       8
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__MASK        0x00000100
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_CLK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp1_tx - clears pull_select bits of USP1_TX pad */
/* clears pull_select bits of USP1_TX pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__SHIFT       10
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__MASK        0x00000400
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__INV_MASK    0xFFFFFBFF
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_TX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp1_rx - clears pull_select bits of USP1_RX pad */
/* clears pull_select bits of USP1_RX pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__SHIFT       12
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__MASK        0x00001000
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__INV_MASK    0xFFFFEFFF
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_RX__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_14_REG_CLR.usp1_fs - clears pull_select bits of USP1_FS pad */
/* clears pull_select bits of USP1_FS pad */
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__SHIFT       14
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__WIDTH       1
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__MASK        0x00004000
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__INV_MASK    0xFFFFBFFF
#define SW_TOP_PULL_EN_14_REG_CLR__USP1_FS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_15_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_15_REG register */
#define SW_TOP_PULL_EN_15_REG_SET 0x10E40270

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d4p - sets pull_select bits of LVDS_TX0D4P pad */
/* sets pull_select bits of LVDS_TX0D4P pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__MASK        0x00000003
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d4n - sets pull_select bits of LVDS_TX0D4N pad */
/* sets pull_select bits of LVDS_TX0D4N pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__SHIFT       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__MASK        0x0000000C
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D4N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d3p - sets pull_select bits of LVDS_TX0D3P pad */
/* sets pull_select bits of LVDS_TX0D3P pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__SHIFT       4
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__MASK        0x00000030
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d3n - sets pull_select bits of LVDS_TX0D3N pad */
/* sets pull_select bits of LVDS_TX0D3N pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__SHIFT       6
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__MASK        0x000000C0
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D3N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d2p - sets pull_select bits of LVDS_TX0D2P pad */
/* sets pull_select bits of LVDS_TX0D2P pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__SHIFT       8
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__MASK        0x00000300
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d2n - sets pull_select bits of LVDS_TX0D2N pad */
/* sets pull_select bits of LVDS_TX0D2N pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__SHIFT       10
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__MASK        0x00000C00
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D2N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d1p - sets pull_select bits of LVDS_TX0D1P pad */
/* sets pull_select bits of LVDS_TX0D1P pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__SHIFT       12
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__MASK        0x00003000
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_SET.lvds_tx0d1n - sets pull_select bits of LVDS_TX0D1N pad */
/* sets pull_select bits of LVDS_TX0D1N pad */
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__SHIFT       14
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__MASK        0x0000C000
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_15_REG_SET__LVDS_TX0D1N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_15_REG register */
#define SW_TOP_PULL_EN_15_REG_CLR 0x10E40274

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d4p - clears pull_select bits of LVDS_TX0D4P pad */
/* clears pull_select bits of LVDS_TX0D4P pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__MASK        0x00000003
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d4n - clears pull_select bits of LVDS_TX0D4N pad */
/* clears pull_select bits of LVDS_TX0D4N pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__SHIFT       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__MASK        0x0000000C
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D4N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d3p - clears pull_select bits of LVDS_TX0D3P pad */
/* clears pull_select bits of LVDS_TX0D3P pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__SHIFT       4
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__MASK        0x00000030
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__INV_MASK    0xFFFFFFCF
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d3n - clears pull_select bits of LVDS_TX0D3N pad */
/* clears pull_select bits of LVDS_TX0D3N pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__SHIFT       6
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__MASK        0x000000C0
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__INV_MASK    0xFFFFFF3F
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D3N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d2p - clears pull_select bits of LVDS_TX0D2P pad */
/* clears pull_select bits of LVDS_TX0D2P pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__SHIFT       8
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__MASK        0x00000300
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__INV_MASK    0xFFFFFCFF
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d2n - clears pull_select bits of LVDS_TX0D2N pad */
/* clears pull_select bits of LVDS_TX0D2N pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__SHIFT       10
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__MASK        0x00000C00
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__INV_MASK    0xFFFFF3FF
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D2N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d1p - clears pull_select bits of LVDS_TX0D1P pad */
/* clears pull_select bits of LVDS_TX0D1P pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__SHIFT       12
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__MASK        0x00003000
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__INV_MASK    0xFFFFCFFF
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_15_REG_CLR.lvds_tx0d1n - clears pull_select bits of LVDS_TX0D1N pad */
/* clears pull_select bits of LVDS_TX0D1N pad */
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__SHIFT       14
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__WIDTH       2
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__MASK        0x0000C000
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__INV_MASK    0xFFFF3FFF
#define SW_TOP_PULL_EN_15_REG_CLR__LVDS_TX0D1N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_16_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_16_REG register */
#define SW_TOP_PULL_EN_16_REG_SET 0x10E40278

/* SW_TOP_PULL_EN_16_REG_SET.lvds_tx0d0p - sets pull_select bits of LVDS_TX0D0P pad */
/* sets pull_select bits of LVDS_TX0D0P pad */
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__SHIFT       0
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__WIDTH       2
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__MASK        0x00000003
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_16_REG_SET.lvds_tx0d0n - sets pull_select bits of LVDS_TX0D0N pad */
/* sets pull_select bits of LVDS_TX0D0N pad */
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__SHIFT       2
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__WIDTH       2
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__MASK        0x0000000C
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_16_REG_SET__LVDS_TX0D0N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_16_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_16_REG register */
#define SW_TOP_PULL_EN_16_REG_CLR 0x10E4027C

/* SW_TOP_PULL_EN_16_REG_CLR.lvds_tx0d0p - clears pull_select bits of LVDS_TX0D0P pad */
/* clears pull_select bits of LVDS_TX0D0P pad */
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__SHIFT       0
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__WIDTH       2
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__MASK        0x00000003
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__INV_MASK    0xFFFFFFFC
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0P__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_16_REG_CLR.lvds_tx0d0n - clears pull_select bits of LVDS_TX0D0N pad */
/* clears pull_select bits of LVDS_TX0D0N pad */
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__SHIFT       2
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__WIDTH       2
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__MASK        0x0000000C
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__INV_MASK    0xFFFFFFF3
#define SW_TOP_PULL_EN_16_REG_CLR__LVDS_TX0D0N__HW_DEFAULT  0x3

/* SW_TOP_PULL_EN_17_REG_SET Address */
/* Sets selected bits of SW_TOP_PULL_EN_17_REG register */
#define SW_TOP_PULL_EN_17_REG_SET 0x10E40280

/* SW_TOP_PULL_EN_17_REG_SET.jtag_tdo - sets pull_select bits of JTAG_TDO pad */
/* sets pull_select bits of JTAG_TDO pad */
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__SHIFT       0
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__MASK        0x00000001
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDO__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_SET.jtag_tms - sets pull_select bits of JTAG_TMS pad */
/* sets pull_select bits of JTAG_TMS pad */
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__SHIFT       2
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__MASK        0x00000004
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TMS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_SET.jtag_tck - sets pull_select bits of JTAG_TCK pad */
/* sets pull_select bits of JTAG_TCK pad */
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__SHIFT       4
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__MASK        0x00000010
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TCK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_SET.jtag_tdi - sets pull_select bits of JTAG_TDI pad */
/* sets pull_select bits of JTAG_TDI pad */
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__SHIFT       6
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__MASK        0x00000040
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TDI__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_SET.jtag_trstn - sets pull_select bits of JTAG_TRSTN pad */
/* sets pull_select bits of JTAG_TRSTN pad */
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__SHIFT       8
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__MASK        0x00000100
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_17_REG_SET__JTAG_TRSTN__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_CLR Address */
/* Clears selected bits of SW_TOP_PULL_EN_17_REG register */
#define SW_TOP_PULL_EN_17_REG_CLR 0x10E40284

/* SW_TOP_PULL_EN_17_REG_CLR.jtag_tdo - clears pull_select bits of JTAG_TDO pad */
/* clears pull_select bits of JTAG_TDO pad */
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__SHIFT       0
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__MASK        0x00000001
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__INV_MASK    0xFFFFFFFE
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDO__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_CLR.jtag_tms - clears pull_select bits of JTAG_TMS pad */
/* clears pull_select bits of JTAG_TMS pad */
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__SHIFT       2
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__MASK        0x00000004
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__INV_MASK    0xFFFFFFFB
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TMS__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_CLR.jtag_tck - clears pull_select bits of JTAG_TCK pad */
/* clears pull_select bits of JTAG_TCK pad */
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__SHIFT       4
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__MASK        0x00000010
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__INV_MASK    0xFFFFFFEF
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TCK__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_CLR.jtag_tdi - clears pull_select bits of JTAG_TDI pad */
/* clears pull_select bits of JTAG_TDI pad */
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__SHIFT       6
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__MASK        0x00000040
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__INV_MASK    0xFFFFFFBF
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TDI__HW_DEFAULT  0x1

/* SW_TOP_PULL_EN_17_REG_CLR.jtag_trstn - clears pull_select bits of JTAG_TRSTN pad */
/* clears pull_select bits of JTAG_TRSTN pad */
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__SHIFT       8
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__WIDTH       1
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__MASK        0x00000100
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__INV_MASK    0xFFFFFEFF
#define SW_TOP_PULL_EN_17_REG_CLR__JTAG_TRSTN__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_0_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_0_REG register */
#define SW_TOP_STRENGTH_CNTL_0_REG_SET 0x10E40300

/* SW_TOP_STRENGTH_CNTL_0_REG_SET.spi1_en - sets drive_strength bits of SPI1_EN pad */
/* sets drive_strength bits of SPI1_EN pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_EN__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_EN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_EN__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_EN__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_EN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_0_REG_SET.spi1_clk - sets drive_strength bits of SPI1_CLK pad */
/* sets drive_strength bits of SPI1_CLK pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_CLK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_CLK__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_CLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_0_REG_SET.spi1_din - sets drive_strength bits of SPI1_DIN pad */
/* sets drive_strength bits of SPI1_DIN pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DIN__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DIN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DIN__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DIN__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DIN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_0_REG_SET.spi1_dout - sets drive_strength bits of SPI1_DOUT pad */
/* sets drive_strength bits of SPI1_DOUT pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DOUT__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DOUT__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DOUT__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DOUT__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_0_REG_SET__SPI1_DOUT__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_0_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_0_REG register */
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR 0x10E40304

/* SW_TOP_STRENGTH_CNTL_0_REG_CLR.spi1_en - clears drive_strength bits of SPI1_EN pad */
/* clears drive_strength bits of SPI1_EN pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_EN__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_EN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_EN__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_EN__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_EN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_0_REG_CLR.spi1_clk - clears drive_strength bits of SPI1_CLK pad */
/* clears drive_strength bits of SPI1_CLK pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_CLK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_CLK__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_CLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_0_REG_CLR.spi1_din - clears drive_strength bits of SPI1_DIN pad */
/* clears drive_strength bits of SPI1_DIN pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DIN__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DIN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DIN__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DIN__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DIN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_0_REG_CLR.spi1_dout - clears drive_strength bits of SPI1_DOUT pad */
/* clears drive_strength bits of SPI1_DOUT pad */
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DOUT__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DOUT__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DOUT__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DOUT__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_0_REG_CLR__SPI1_DOUT__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_1_REG register */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET 0x10E40308

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_spi_clk - sets drive_strength bits of TRG_SPI_CLK pad */
/* sets drive_strength bits of TRG_SPI_CLK pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_spi_di - sets drive_strength bits of TRG_SPI_DI pad */
/* sets drive_strength bits of TRG_SPI_DI pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DI__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DI__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DI__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DI__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_spi_do - sets drive_strength bits of TRG_SPI_DO pad */
/* sets drive_strength bits of TRG_SPI_DO pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DO__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DO__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DO__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DO__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_DO__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_spi_cs_b - sets drive_strength bits of TRG_SPI_CS_B pad */
/* sets drive_strength bits of TRG_SPI_CS_B pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CS_B__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CS_B__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CS_B__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CS_B__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SPI_CS_B__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_acq_d1 - sets drive_strength bits of TRG_ACQ_D1 pad */
/* sets drive_strength bits of TRG_ACQ_D1 pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D1__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_irq_b - sets drive_strength bits of TRG_IRQ_B pad */
/* sets drive_strength bits of TRG_IRQ_B pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_IRQ_B__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_IRQ_B__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_IRQ_B__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_IRQ_B__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_IRQ_B__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_acq_d0 - sets drive_strength bits of TRG_ACQ_D0 pad */
/* sets drive_strength bits of TRG_ACQ_D0 pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D0__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D0__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D0__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_acq_clk - sets drive_strength bits of TRG_ACQ_CLK pad */
/* sets drive_strength bits of TRG_ACQ_CLK pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_CLK__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_CLK__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_CLK__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_SET.trg_shutdown_b_out - sets drive_strength bits of TRG_SHUTDOWN_B_OUT pad */
/* sets drive_strength bits of TRG_SHUTDOWN_B_OUT pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SHUTDOWN_B_OUT__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SHUTDOWN_B_OUT__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SHUTDOWN_B_OUT__MASK        0x00010000
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFEFFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_SET__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_1_REG register */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR 0x10E4030C

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_spi_clk - clears drive_strength bits of TRG_SPI_CLK pad */
/* clears drive_strength bits of TRG_SPI_CLK pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_spi_di - clears drive_strength bits of TRG_SPI_DI pad */
/* clears drive_strength bits of TRG_SPI_DI pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DI__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DI__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DI__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DI__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DI__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_spi_do - clears drive_strength bits of TRG_SPI_DO pad */
/* clears drive_strength bits of TRG_SPI_DO pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DO__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DO__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DO__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DO__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_DO__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_spi_cs_b - clears drive_strength bits of TRG_SPI_CS_B pad */
/* clears drive_strength bits of TRG_SPI_CS_B pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CS_B__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CS_B__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CS_B__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CS_B__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SPI_CS_B__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_acq_d1 - clears drive_strength bits of TRG_ACQ_D1 pad */
/* clears drive_strength bits of TRG_ACQ_D1 pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D1__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D1__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_irq_b - clears drive_strength bits of TRG_IRQ_B pad */
/* clears drive_strength bits of TRG_IRQ_B pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_IRQ_B__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_IRQ_B__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_IRQ_B__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_IRQ_B__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_IRQ_B__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_acq_d0 - clears drive_strength bits of TRG_ACQ_D0 pad */
/* clears drive_strength bits of TRG_ACQ_D0 pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D0__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D0__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D0__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_D0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_acq_clk - clears drive_strength bits of TRG_ACQ_CLK pad */
/* clears drive_strength bits of TRG_ACQ_CLK pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_CLK__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_CLK__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_CLK__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_ACQ_CLK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_1_REG_CLR.trg_shutdown_b_out - clears drive_strength bits of TRG_SHUTDOWN_B_OUT pad */
/* clears drive_strength bits of TRG_SHUTDOWN_B_OUT pad */
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SHUTDOWN_B_OUT__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SHUTDOWN_B_OUT__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SHUTDOWN_B_OUT__MASK        0x00010000
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SHUTDOWN_B_OUT__INV_MASK    0xFFFEFFFF
#define SW_TOP_STRENGTH_CNTL_1_REG_CLR__TRG_SHUTDOWN_B_OUT__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_2_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_2_REG register */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET 0x10E40310

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_clk - sets drive_strength bits of SDIO2_CLK pad */
/* sets drive_strength bits of SDIO2_CLK pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_cmd - sets drive_strength bits of SDIO2_CMD pad */
/* sets drive_strength bits of SDIO2_CMD pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_dat_0 - sets drive_strength bits of SDIO2_DAT_0 pad */
/* sets drive_strength bits of SDIO2_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_dat_1 - sets drive_strength bits of SDIO2_DAT_1 pad */
/* sets drive_strength bits of SDIO2_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_dat_2 - sets drive_strength bits of SDIO2_DAT_2 pad */
/* sets drive_strength bits of SDIO2_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_SET.sdio2_dat_3 - sets drive_strength bits of SDIO2_DAT_3 pad */
/* sets drive_strength bits of SDIO2_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_2_REG_SET__SDIO2_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_2_REG register */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR 0x10E40314

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_clk - clears drive_strength bits of SDIO2_CLK pad */
/* clears drive_strength bits of SDIO2_CLK pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_cmd - clears drive_strength bits of SDIO2_CMD pad */
/* clears drive_strength bits of SDIO2_CMD pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_dat_0 - clears drive_strength bits of SDIO2_DAT_0 pad */
/* clears drive_strength bits of SDIO2_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_dat_1 - clears drive_strength bits of SDIO2_DAT_1 pad */
/* clears drive_strength bits of SDIO2_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_dat_2 - clears drive_strength bits of SDIO2_DAT_2 pad */
/* clears drive_strength bits of SDIO2_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_2_REG_CLR.sdio2_dat_3 - clears drive_strength bits of SDIO2_DAT_3 pad */
/* clears drive_strength bits of SDIO2_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_2_REG_CLR__SDIO2_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_3_REG register */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET 0x10E40318

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_7 - sets drive_strength bits of DF_AD_7 pad */
/* sets drive_strength bits of DF_AD_7 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_7__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_7__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_7__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_7__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_6 - sets drive_strength bits of DF_AD_6 pad */
/* sets drive_strength bits of DF_AD_6 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_6__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_6__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_6__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_6__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_5 - sets drive_strength bits of DF_AD_5 pad */
/* sets drive_strength bits of DF_AD_5 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_5__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_5__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_5__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_5__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_4 - sets drive_strength bits of DF_AD_4 pad */
/* sets drive_strength bits of DF_AD_4 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_4__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_4__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_4__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_4__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_3 - sets drive_strength bits of DF_AD_3 pad */
/* sets drive_strength bits of DF_AD_3 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_3__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_3__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_3__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_2 - sets drive_strength bits of DF_AD_2 pad */
/* sets drive_strength bits of DF_AD_2 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_1 - sets drive_strength bits of DF_AD_1 pad */
/* sets drive_strength bits of DF_AD_1 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_1__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_1__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_1__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ad_0 - sets drive_strength bits of DF_AD_0 pad */
/* sets drive_strength bits of DF_AD_0 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_0__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_0__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_AD_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_dqs - sets drive_strength bits of DF_DQS pad */
/* sets drive_strength bits of DF_DQS pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_DQS__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_DQS__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_DQS__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_DQS__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_DQS__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_cle - sets drive_strength bits of DF_CLE pad */
/* sets drive_strength bits of DF_CLE pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CLE__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CLE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CLE__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CLE__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CLE__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ale - sets drive_strength bits of DF_ALE pad */
/* sets drive_strength bits of DF_ALE pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_ALE__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_ALE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_ALE__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_ALE__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_ALE__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_we_b - sets drive_strength bits of DF_WE_B pad */
/* sets drive_strength bits of DF_WE_B pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_WE_B__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_WE_B__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_WE_B__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_WE_B__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_WE_B__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_re_b - sets drive_strength bits of DF_RE_B pad */
/* sets drive_strength bits of DF_RE_B pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RE_B__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RE_B__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RE_B__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RE_B__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RE_B__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_ry_by - sets drive_strength bits of DF_RY_BY pad */
/* sets drive_strength bits of DF_RY_BY pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RY_BY__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RY_BY__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RY_BY__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RY_BY__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_RY_BY__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_cs_b_1 - sets drive_strength bits of DF_CS_B_1 pad */
/* sets drive_strength bits of DF_CS_B_1 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_1__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_1__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_1__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_SET.df_cs_b_0 - sets drive_strength bits of DF_CS_B_0 pad */
/* sets drive_strength bits of DF_CS_B_0 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_0__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_0__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_0__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_SET__DF_CS_B_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_3_REG register */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR 0x10E4031C

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_7 - clears drive_strength bits of DF_AD_7 pad */
/* clears drive_strength bits of DF_AD_7 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_7__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_7__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_7__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_7__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_6 - clears drive_strength bits of DF_AD_6 pad */
/* clears drive_strength bits of DF_AD_6 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_6__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_6__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_6__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_6__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_5 - clears drive_strength bits of DF_AD_5 pad */
/* clears drive_strength bits of DF_AD_5 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_5__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_5__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_5__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_5__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_4 - clears drive_strength bits of DF_AD_4 pad */
/* clears drive_strength bits of DF_AD_4 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_4__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_4__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_4__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_4__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_3 - clears drive_strength bits of DF_AD_3 pad */
/* clears drive_strength bits of DF_AD_3 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_3__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_3__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_3__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_2 - clears drive_strength bits of DF_AD_2 pad */
/* clears drive_strength bits of DF_AD_2 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_1 - clears drive_strength bits of DF_AD_1 pad */
/* clears drive_strength bits of DF_AD_1 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_1__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_1__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_1__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ad_0 - clears drive_strength bits of DF_AD_0 pad */
/* clears drive_strength bits of DF_AD_0 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_0__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_0__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_AD_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_dqs - clears drive_strength bits of DF_DQS pad */
/* clears drive_strength bits of DF_DQS pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_DQS__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_DQS__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_DQS__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_DQS__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_DQS__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_cle - clears drive_strength bits of DF_CLE pad */
/* clears drive_strength bits of DF_CLE pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CLE__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CLE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CLE__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CLE__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CLE__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ale - clears drive_strength bits of DF_ALE pad */
/* clears drive_strength bits of DF_ALE pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_ALE__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_ALE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_ALE__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_ALE__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_ALE__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_we_b - clears drive_strength bits of DF_WE_B pad */
/* clears drive_strength bits of DF_WE_B pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_WE_B__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_WE_B__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_WE_B__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_WE_B__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_WE_B__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_re_b - clears drive_strength bits of DF_RE_B pad */
/* clears drive_strength bits of DF_RE_B pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RE_B__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RE_B__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RE_B__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RE_B__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RE_B__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_ry_by - clears drive_strength bits of DF_RY_BY pad */
/* clears drive_strength bits of DF_RY_BY pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RY_BY__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RY_BY__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RY_BY__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RY_BY__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_RY_BY__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_cs_b_1 - clears drive_strength bits of DF_CS_B_1 pad */
/* clears drive_strength bits of DF_CS_B_1 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_1__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_1__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_1__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_3_REG_CLR.df_cs_b_0 - clears drive_strength bits of DF_CS_B_0 pad */
/* clears drive_strength bits of DF_CS_B_0 pad */
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_0__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_0__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_0__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_3_REG_CLR__DF_CS_B_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_4_REG register */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET 0x10E40320

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.l_pclk - sets drive_strength bits of L_PCLK pad */
/* sets drive_strength bits of L_PCLK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_PCLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_PCLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_PCLK__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_PCLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_PCLK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.l_lck - sets drive_strength bits of L_LCK pad */
/* sets drive_strength bits of L_LCK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_LCK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_LCK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_LCK__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_LCK__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_LCK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.l_fck - sets drive_strength bits of L_FCK pad */
/* sets drive_strength bits of L_FCK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_FCK__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_FCK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_FCK__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_FCK__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_FCK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.l_de - sets drive_strength bits of L_DE pad */
/* sets drive_strength bits of L_DE pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_DE__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_DE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_DE__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_DE__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__L_DE__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_0 - sets drive_strength bits of LDD_0 pad */
/* sets drive_strength bits of LDD_0 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_0__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_0__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_0__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_1 - sets drive_strength bits of LDD_1 pad */
/* sets drive_strength bits of LDD_1 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_1__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_1__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_1__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_2 - sets drive_strength bits of LDD_2 pad */
/* sets drive_strength bits of LDD_2 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_2__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_2__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_2__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_3 - sets drive_strength bits of LDD_3 pad */
/* sets drive_strength bits of LDD_3 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_3__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_3__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_3__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_4 - sets drive_strength bits of LDD_4 pad */
/* sets drive_strength bits of LDD_4 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_4__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_4__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_4__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_4__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_5 - sets drive_strength bits of LDD_5 pad */
/* sets drive_strength bits of LDD_5 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_5__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_5__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_5__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_5__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_6 - sets drive_strength bits of LDD_6 pad */
/* sets drive_strength bits of LDD_6 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_6__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_6__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_6__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_6__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_7 - sets drive_strength bits of LDD_7 pad */
/* sets drive_strength bits of LDD_7 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_7__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_7__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_7__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_7__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_8 - sets drive_strength bits of LDD_8 pad */
/* sets drive_strength bits of LDD_8 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_8__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_8__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_8__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_8__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_8__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_9 - sets drive_strength bits of LDD_9 pad */
/* sets drive_strength bits of LDD_9 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_9__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_9__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_9__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_9__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_9__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_10 - sets drive_strength bits of LDD_10 pad */
/* sets drive_strength bits of LDD_10 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_10__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_10__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_10__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_10__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_10__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_SET.ldd_11 - sets drive_strength bits of LDD_11 pad */
/* sets drive_strength bits of LDD_11 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_11__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_11__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_11__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_11__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_SET__LDD_11__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_4_REG register */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR 0x10E40324

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.l_pclk - clears drive_strength bits of L_PCLK pad */
/* clears drive_strength bits of L_PCLK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_PCLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_PCLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_PCLK__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_PCLK__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_PCLK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.l_lck - clears drive_strength bits of L_LCK pad */
/* clears drive_strength bits of L_LCK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_LCK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_LCK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_LCK__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_LCK__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_LCK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.l_fck - clears drive_strength bits of L_FCK pad */
/* clears drive_strength bits of L_FCK pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_FCK__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_FCK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_FCK__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_FCK__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_FCK__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.l_de - clears drive_strength bits of L_DE pad */
/* clears drive_strength bits of L_DE pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_DE__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_DE__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_DE__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_DE__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__L_DE__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_0 - clears drive_strength bits of LDD_0 pad */
/* clears drive_strength bits of LDD_0 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_0__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_0__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_0__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_1 - clears drive_strength bits of LDD_1 pad */
/* clears drive_strength bits of LDD_1 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_1__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_1__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_1__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_2 - clears drive_strength bits of LDD_2 pad */
/* clears drive_strength bits of LDD_2 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_2__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_2__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_2__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_3 - clears drive_strength bits of LDD_3 pad */
/* clears drive_strength bits of LDD_3 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_3__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_3__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_3__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_4 - clears drive_strength bits of LDD_4 pad */
/* clears drive_strength bits of LDD_4 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_4__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_4__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_4__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_4__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_5 - clears drive_strength bits of LDD_5 pad */
/* clears drive_strength bits of LDD_5 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_5__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_5__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_5__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_5__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_6 - clears drive_strength bits of LDD_6 pad */
/* clears drive_strength bits of LDD_6 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_6__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_6__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_6__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_6__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_7 - clears drive_strength bits of LDD_7 pad */
/* clears drive_strength bits of LDD_7 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_7__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_7__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_7__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_7__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_8 - clears drive_strength bits of LDD_8 pad */
/* clears drive_strength bits of LDD_8 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_8__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_8__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_8__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_8__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_8__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_9 - clears drive_strength bits of LDD_9 pad */
/* clears drive_strength bits of LDD_9 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_9__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_9__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_9__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_9__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_9__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_10 - clears drive_strength bits of LDD_10 pad */
/* clears drive_strength bits of LDD_10 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_10__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_10__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_10__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_10__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_10__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_4_REG_CLR.ldd_11 - clears drive_strength bits of LDD_11 pad */
/* clears drive_strength bits of LDD_11 pad */
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_11__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_11__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_11__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_11__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_4_REG_CLR__LDD_11__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_5_REG register */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET 0x10E40328

/* SW_TOP_STRENGTH_CNTL_5_REG_SET.ldd_12 - sets drive_strength bits of LDD_12 pad */
/* sets drive_strength bits of LDD_12 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_12__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_12__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_12__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_12__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_12__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_SET.ldd_13 - sets drive_strength bits of LDD_13 pad */
/* sets drive_strength bits of LDD_13 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_13__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_13__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_13__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_13__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_13__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_SET.ldd_14 - sets drive_strength bits of LDD_14 pad */
/* sets drive_strength bits of LDD_14 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_14__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_14__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_14__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_14__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_14__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_SET.ldd_15 - sets drive_strength bits of LDD_15 pad */
/* sets drive_strength bits of LDD_15 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_15__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_15__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_15__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_15__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LDD_15__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_SET.lcd_gpio_20 - sets drive_strength bits of LCD_GPIO_20 pad */
/* sets drive_strength bits of LCD_GPIO_20 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LCD_GPIO_20__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LCD_GPIO_20__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LCD_GPIO_20__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LCD_GPIO_20__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_5_REG_SET__LCD_GPIO_20__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_5_REG register */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR 0x10E4032C

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR.ldd_12 - clears drive_strength bits of LDD_12 pad */
/* clears drive_strength bits of LDD_12 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_12__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_12__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_12__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_12__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_12__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR.ldd_13 - clears drive_strength bits of LDD_13 pad */
/* clears drive_strength bits of LDD_13 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_13__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_13__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_13__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_13__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_13__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR.ldd_14 - clears drive_strength bits of LDD_14 pad */
/* clears drive_strength bits of LDD_14 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_14__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_14__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_14__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_14__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_14__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR.ldd_15 - clears drive_strength bits of LDD_15 pad */
/* clears drive_strength bits of LDD_15 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_15__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_15__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_15__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_15__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LDD_15__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_5_REG_CLR.lcd_gpio_20 - clears drive_strength bits of LCD_GPIO_20 pad */
/* clears drive_strength bits of LCD_GPIO_20 pad */
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LCD_GPIO_20__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LCD_GPIO_20__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LCD_GPIO_20__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LCD_GPIO_20__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_5_REG_CLR__LCD_GPIO_20__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_6_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_6_REG register */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET 0x10E40330

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_0 - sets drive_strength bits of VIP_0 pad */
/* sets drive_strength bits of VIP_0 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_1 - sets drive_strength bits of VIP_1 pad */
/* sets drive_strength bits of VIP_1 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_2 - sets drive_strength bits of VIP_2 pad */
/* sets drive_strength bits of VIP_2 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_3 - sets drive_strength bits of VIP_3 pad */
/* sets drive_strength bits of VIP_3 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_4 - sets drive_strength bits of VIP_4 pad */
/* sets drive_strength bits of VIP_4 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_4__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_4__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_4__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_4__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_5 - sets drive_strength bits of VIP_5 pad */
/* sets drive_strength bits of VIP_5 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_5__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_5__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_5__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_5__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_6 - sets drive_strength bits of VIP_6 pad */
/* sets drive_strength bits of VIP_6 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_6__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_6__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_6__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_6__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_7 - sets drive_strength bits of VIP_7 pad */
/* sets drive_strength bits of VIP_7 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_7__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_7__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_7__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_7__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_pxclk - sets drive_strength bits of VIP_PXCLK pad */
/* sets drive_strength bits of VIP_PXCLK pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_PXCLK__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_PXCLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_PXCLK__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_PXCLK__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_PXCLK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_hsync - sets drive_strength bits of VIP_HSYNC pad */
/* sets drive_strength bits of VIP_HSYNC pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_HSYNC__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_HSYNC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_HSYNC__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_HSYNC__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_HSYNC__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_SET.vip_vsync - sets drive_strength bits of VIP_VSYNC pad */
/* sets drive_strength bits of VIP_VSYNC pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_VSYNC__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_VSYNC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_VSYNC__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_VSYNC__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_SET__VIP_VSYNC__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_6_REG register */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR 0x10E40334

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_0 - clears drive_strength bits of VIP_0 pad */
/* clears drive_strength bits of VIP_0 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_1 - clears drive_strength bits of VIP_1 pad */
/* clears drive_strength bits of VIP_1 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_2 - clears drive_strength bits of VIP_2 pad */
/* clears drive_strength bits of VIP_2 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_3 - clears drive_strength bits of VIP_3 pad */
/* clears drive_strength bits of VIP_3 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_4 - clears drive_strength bits of VIP_4 pad */
/* clears drive_strength bits of VIP_4 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_4__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_4__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_4__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_4__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_4__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_5 - clears drive_strength bits of VIP_5 pad */
/* clears drive_strength bits of VIP_5 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_5__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_5__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_5__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_5__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_5__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_6 - clears drive_strength bits of VIP_6 pad */
/* clears drive_strength bits of VIP_6 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_6__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_6__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_6__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_6__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_6__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_7 - clears drive_strength bits of VIP_7 pad */
/* clears drive_strength bits of VIP_7 pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_7__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_7__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_7__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_7__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_7__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_pxclk - clears drive_strength bits of VIP_PXCLK pad */
/* clears drive_strength bits of VIP_PXCLK pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_PXCLK__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_PXCLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_PXCLK__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_PXCLK__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_PXCLK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_hsync - clears drive_strength bits of VIP_HSYNC pad */
/* clears drive_strength bits of VIP_HSYNC pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_HSYNC__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_HSYNC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_HSYNC__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_HSYNC__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_HSYNC__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_6_REG_CLR.vip_vsync - clears drive_strength bits of VIP_VSYNC pad */
/* clears drive_strength bits of VIP_VSYNC pad */
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_VSYNC__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_VSYNC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_VSYNC__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_VSYNC__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_6_REG_CLR__VIP_VSYNC__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_7_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_7_REG register */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET 0x10E40338

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_clk - sets drive_strength bits of SDIO3_CLK pad */
/* sets drive_strength bits of SDIO3_CLK pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_cmd - sets drive_strength bits of SDIO3_CMD pad */
/* sets drive_strength bits of SDIO3_CMD pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_dat_0 - sets drive_strength bits of SDIO3_DAT_0 pad */
/* sets drive_strength bits of SDIO3_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_dat_1 - sets drive_strength bits of SDIO3_DAT_1 pad */
/* sets drive_strength bits of SDIO3_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_dat_2 - sets drive_strength bits of SDIO3_DAT_2 pad */
/* sets drive_strength bits of SDIO3_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_SET.sdio3_dat_3 - sets drive_strength bits of SDIO3_DAT_3 pad */
/* sets drive_strength bits of SDIO3_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_7_REG_SET__SDIO3_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_7_REG register */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR 0x10E4033C

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_clk - clears drive_strength bits of SDIO3_CLK pad */
/* clears drive_strength bits of SDIO3_CLK pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_cmd - clears drive_strength bits of SDIO3_CMD pad */
/* clears drive_strength bits of SDIO3_CMD pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_dat_0 - clears drive_strength bits of SDIO3_DAT_0 pad */
/* clears drive_strength bits of SDIO3_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_dat_1 - clears drive_strength bits of SDIO3_DAT_1 pad */
/* clears drive_strength bits of SDIO3_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_dat_2 - clears drive_strength bits of SDIO3_DAT_2 pad */
/* clears drive_strength bits of SDIO3_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_7_REG_CLR.sdio3_dat_3 - clears drive_strength bits of SDIO3_DAT_3 pad */
/* clears drive_strength bits of SDIO3_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_7_REG_CLR__SDIO3_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_8_REG register */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET 0x10E40340

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_clk - sets drive_strength bits of SDIO5_CLK pad */
/* sets drive_strength bits of SDIO5_CLK pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_cmd - sets drive_strength bits of SDIO5_CMD pad */
/* sets drive_strength bits of SDIO5_CMD pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_dat_0 - sets drive_strength bits of SDIO5_DAT_0 pad */
/* sets drive_strength bits of SDIO5_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_dat_1 - sets drive_strength bits of SDIO5_DAT_1 pad */
/* sets drive_strength bits of SDIO5_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_dat_2 - sets drive_strength bits of SDIO5_DAT_2 pad */
/* sets drive_strength bits of SDIO5_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_SET.sdio5_dat_3 - sets drive_strength bits of SDIO5_DAT_3 pad */
/* sets drive_strength bits of SDIO5_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_8_REG_SET__SDIO5_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_8_REG register */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR 0x10E40344

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_clk - clears drive_strength bits of SDIO5_CLK pad */
/* clears drive_strength bits of SDIO5_CLK pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CLK__MASK        0x0000000F
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CLK__INV_MASK    0xFFFFFFF0
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_cmd - clears drive_strength bits of SDIO5_CMD pad */
/* clears drive_strength bits of SDIO5_CMD pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CMD__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CMD__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CMD__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CMD__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_CMD__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_dat_0 - clears drive_strength bits of SDIO5_DAT_0 pad */
/* clears drive_strength bits of SDIO5_DAT_0 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_0__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_0__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_0__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_dat_1 - clears drive_strength bits of SDIO5_DAT_1 pad */
/* clears drive_strength bits of SDIO5_DAT_1 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_1__MASK        0x00000300
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_1__INV_MASK    0xFFFFFCFF
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_1__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_dat_2 - clears drive_strength bits of SDIO5_DAT_2 pad */
/* clears drive_strength bits of SDIO5_DAT_2 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_2__MASK        0x00000C00
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_2__INV_MASK    0xFFFFF3FF
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_2__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_8_REG_CLR.sdio5_dat_3 - clears drive_strength bits of SDIO5_DAT_3 pad */
/* clears drive_strength bits of SDIO5_DAT_3 pad */
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_3__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_3__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_3__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_8_REG_CLR__SDIO5_DAT_3__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_9_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_9_REG register */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET 0x10E40348

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_txd_0 - sets drive_strength bits of RGMII_TXD_0 pad */
/* sets drive_strength bits of RGMII_TXD_0 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_0__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_txd_1 - sets drive_strength bits of RGMII_TXD_1 pad */
/* sets drive_strength bits of RGMII_TXD_1 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_1__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_txd_2 - sets drive_strength bits of RGMII_TXD_2 pad */
/* sets drive_strength bits of RGMII_TXD_2 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_2__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_txd_3 - sets drive_strength bits of RGMII_TXD_3 pad */
/* sets drive_strength bits of RGMII_TXD_3 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXD_3__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_txclk - sets drive_strength bits of RGMII_TXCLK pad */
/* sets drive_strength bits of RGMII_TXCLK pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXCLK__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXCLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXCLK__MASK        0x00000F00
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXCLK__INV_MASK    0xFFFFF0FF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TXCLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_tx_ctl - sets drive_strength bits of RGMII_TX_CTL pad */
/* sets drive_strength bits of RGMII_TX_CTL pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TX_CTL__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TX_CTL__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TX_CTL__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TX_CTL__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_TX_CTL__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rxd_0 - sets drive_strength bits of RGMII_RXD_0 pad */
/* sets drive_strength bits of RGMII_RXD_0 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_0__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_0__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_0__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rxd_1 - sets drive_strength bits of RGMII_RXD_1 pad */
/* sets drive_strength bits of RGMII_RXD_1 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_1__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_1__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_1__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_1__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rxd_2 - sets drive_strength bits of RGMII_RXD_2 pad */
/* sets drive_strength bits of RGMII_RXD_2 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_2__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_2__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_2__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_2__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rxd_3 - sets drive_strength bits of RGMII_RXD_3 pad */
/* sets drive_strength bits of RGMII_RXD_3 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_3__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_3__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_3__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXD_3__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rx_clk - sets drive_strength bits of RGMII_RX_CLK pad */
/* sets drive_strength bits of RGMII_RX_CLK pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RX_CLK__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RX_CLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RX_CLK__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RX_CLK__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RX_CLK__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_rxc_ctl - sets drive_strength bits of RGMII_RXC_CTL pad */
/* sets drive_strength bits of RGMII_RXC_CTL pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXC_CTL__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXC_CTL__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXC_CTL__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXC_CTL__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_RXC_CTL__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_mdio - sets drive_strength bits of RGMII_MDIO pad */
/* sets drive_strength bits of RGMII_MDIO pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDIO__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDIO__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDIO__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDIO__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDIO__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_mdc - sets drive_strength bits of RGMII_MDC pad */
/* sets drive_strength bits of RGMII_MDC pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDC__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDC__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDC__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_MDC__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_9_REG_SET.rgmii_intr_n - sets drive_strength bits of RGMII_INTR_N pad */
/* sets drive_strength bits of RGMII_INTR_N pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_INTR_N__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_INTR_N__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_INTR_N__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_INTR_N__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_SET__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_9_REG register */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR 0x10E4034C

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_txd_0 - clears drive_strength bits of RGMII_TXD_0 pad */
/* clears drive_strength bits of RGMII_TXD_0 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_0__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_txd_1 - clears drive_strength bits of RGMII_TXD_1 pad */
/* clears drive_strength bits of RGMII_TXD_1 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_1__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_txd_2 - clears drive_strength bits of RGMII_TXD_2 pad */
/* clears drive_strength bits of RGMII_TXD_2 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_2__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_txd_3 - clears drive_strength bits of RGMII_TXD_3 pad */
/* clears drive_strength bits of RGMII_TXD_3 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXD_3__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_txclk - clears drive_strength bits of RGMII_TXCLK pad */
/* clears drive_strength bits of RGMII_TXCLK pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXCLK__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXCLK__WIDTH       4
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXCLK__MASK        0x00000F00
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXCLK__INV_MASK    0xFFFFF0FF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TXCLK__HW_DEFAULT  0xE

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_tx_ctl - clears drive_strength bits of RGMII_TX_CTL pad */
/* clears drive_strength bits of RGMII_TX_CTL pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TX_CTL__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TX_CTL__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TX_CTL__MASK        0x00003000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TX_CTL__INV_MASK    0xFFFFCFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_TX_CTL__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rxd_0 - clears drive_strength bits of RGMII_RXD_0 pad */
/* clears drive_strength bits of RGMII_RXD_0 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_0__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_0__MASK        0x0000C000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_0__INV_MASK    0xFFFF3FFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_0__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rxd_1 - clears drive_strength bits of RGMII_RXD_1 pad */
/* clears drive_strength bits of RGMII_RXD_1 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_1__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_1__MASK        0x00030000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_1__INV_MASK    0xFFFCFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_1__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rxd_2 - clears drive_strength bits of RGMII_RXD_2 pad */
/* clears drive_strength bits of RGMII_RXD_2 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_2__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_2__MASK        0x000C0000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_2__INV_MASK    0xFFF3FFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_2__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rxd_3 - clears drive_strength bits of RGMII_RXD_3 pad */
/* clears drive_strength bits of RGMII_RXD_3 pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_3__SHIFT       20
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_3__MASK        0x00300000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_3__INV_MASK    0xFFCFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXD_3__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rx_clk - clears drive_strength bits of RGMII_RX_CLK pad */
/* clears drive_strength bits of RGMII_RX_CLK pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RX_CLK__SHIFT       22
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RX_CLK__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RX_CLK__MASK        0x00C00000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RX_CLK__INV_MASK    0xFF3FFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RX_CLK__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_rxc_ctl - clears drive_strength bits of RGMII_RXC_CTL pad */
/* clears drive_strength bits of RGMII_RXC_CTL pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXC_CTL__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXC_CTL__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXC_CTL__MASK        0x03000000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXC_CTL__INV_MASK    0xFCFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_RXC_CTL__HW_DEFAULT  0x3

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_mdio - clears drive_strength bits of RGMII_MDIO pad */
/* clears drive_strength bits of RGMII_MDIO pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDIO__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDIO__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDIO__MASK        0x0C000000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDIO__INV_MASK    0xF3FFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDIO__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_mdc - clears drive_strength bits of RGMII_MDC pad */
/* clears drive_strength bits of RGMII_MDC pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDC__SHIFT       28
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDC__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDC__MASK        0x30000000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDC__INV_MASK    0xCFFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_MDC__HW_DEFAULT  0x2

/* SW_TOP_STRENGTH_CNTL_9_REG_CLR.rgmii_intr_n - clears drive_strength bits of RGMII_INTR_N pad */
/* clears drive_strength bits of RGMII_INTR_N pad */
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_INTR_N__SHIFT       30
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_INTR_N__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_INTR_N__MASK        0xC0000000
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_INTR_N__INV_MASK    0x3FFFFFFF
#define SW_TOP_STRENGTH_CNTL_9_REG_CLR__RGMII_INTR_N__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_10_REG register */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET 0x10E40350

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_mclk - sets drive_strength bits of I2S_MCLK pad */
/* sets drive_strength bits of I2S_MCLK pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_MCLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_MCLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_MCLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_MCLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_MCLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_bclk - sets drive_strength bits of I2S_BCLK pad */
/* sets drive_strength bits of I2S_BCLK pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_BCLK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_BCLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_BCLK__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_BCLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_BCLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_ws - sets drive_strength bits of I2S_WS pad */
/* sets drive_strength bits of I2S_WS pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_WS__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_WS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_WS__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_WS__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_WS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_dout0 - sets drive_strength bits of I2S_DOUT0 pad */
/* sets drive_strength bits of I2S_DOUT0 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT0__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT0__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_dout1 - sets drive_strength bits of I2S_DOUT1 pad */
/* sets drive_strength bits of I2S_DOUT1 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT1__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT1__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_dout2 - sets drive_strength bits of I2S_DOUT2 pad */
/* sets drive_strength bits of I2S_DOUT2 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT2__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT2__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT2__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DOUT2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_SET.i2s_din - sets drive_strength bits of I2S_DIN pad */
/* sets drive_strength bits of I2S_DIN pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DIN__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DIN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DIN__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DIN__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_10_REG_SET__I2S_DIN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_10_REG register */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR 0x10E40354

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_mclk - clears drive_strength bits of I2S_MCLK pad */
/* clears drive_strength bits of I2S_MCLK pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_MCLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_MCLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_MCLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_MCLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_MCLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_bclk - clears drive_strength bits of I2S_BCLK pad */
/* clears drive_strength bits of I2S_BCLK pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_BCLK__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_BCLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_BCLK__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_BCLK__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_BCLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_ws - clears drive_strength bits of I2S_WS pad */
/* clears drive_strength bits of I2S_WS pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_WS__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_WS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_WS__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_WS__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_WS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_dout0 - clears drive_strength bits of I2S_DOUT0 pad */
/* clears drive_strength bits of I2S_DOUT0 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT0__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT0__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT0__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_dout1 - clears drive_strength bits of I2S_DOUT1 pad */
/* clears drive_strength bits of I2S_DOUT1 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT1__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT1__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT1__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_dout2 - clears drive_strength bits of I2S_DOUT2 pad */
/* clears drive_strength bits of I2S_DOUT2 pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT2__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT2__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT2__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT2__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DOUT2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_10_REG_CLR.i2s_din - clears drive_strength bits of I2S_DIN pad */
/* clears drive_strength bits of I2S_DIN pad */
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DIN__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DIN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DIN__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DIN__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_10_REG_CLR__I2S_DIN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_11_REG register */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET 0x10E40358

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_0 - sets drive_strength bits of GPIO_0 pad */
/* sets drive_strength bits of GPIO_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_0__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_1 - sets drive_strength bits of GPIO_1 pad */
/* sets drive_strength bits of GPIO_1 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_1__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_2 - sets drive_strength bits of GPIO_2 pad */
/* sets drive_strength bits of GPIO_2 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_2__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_2__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_3 - sets drive_strength bits of GPIO_3 pad */
/* sets drive_strength bits of GPIO_3 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_3__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_3__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_4 - sets drive_strength bits of GPIO_4 pad */
/* sets drive_strength bits of GPIO_4 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_4__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_4__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_4__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_4__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_4__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_5 - sets drive_strength bits of GPIO_5 pad */
/* sets drive_strength bits of GPIO_5 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_5__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_5__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_5__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_5__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_5__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_6 - sets drive_strength bits of GPIO_6 pad */
/* sets drive_strength bits of GPIO_6 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_6__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_6__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_6__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_6__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_6__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.gpio_7 - sets drive_strength bits of GPIO_7 pad */
/* sets drive_strength bits of GPIO_7 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_7__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_7__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_7__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_7__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__GPIO_7__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.sda_0 - sets drive_strength bits of SDA_0 pad */
/* sets drive_strength bits of SDA_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SDA_0__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SDA_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SDA_0__MASK        0x01000000
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SDA_0__INV_MASK    0xFEFFFFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SDA_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_SET.scl_0 - sets drive_strength bits of SCL_0 pad */
/* sets drive_strength bits of SCL_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SCL_0__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SCL_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SCL_0__MASK        0x04000000
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SCL_0__INV_MASK    0xFBFFFFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_SET__SCL_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_11_REG register */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR 0x10E4035C

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_0 - clears drive_strength bits of GPIO_0 pad */
/* clears drive_strength bits of GPIO_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_0__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_0__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_1 - clears drive_strength bits of GPIO_1 pad */
/* clears drive_strength bits of GPIO_1 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_1__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_1__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_1__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_2 - clears drive_strength bits of GPIO_2 pad */
/* clears drive_strength bits of GPIO_2 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_2__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_2__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_2__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_3 - clears drive_strength bits of GPIO_3 pad */
/* clears drive_strength bits of GPIO_3 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_3__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_3__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_3__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_4 - clears drive_strength bits of GPIO_4 pad */
/* clears drive_strength bits of GPIO_4 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_4__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_4__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_4__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_4__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_4__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_5 - clears drive_strength bits of GPIO_5 pad */
/* clears drive_strength bits of GPIO_5 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_5__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_5__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_5__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_5__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_5__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_6 - clears drive_strength bits of GPIO_6 pad */
/* clears drive_strength bits of GPIO_6 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_6__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_6__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_6__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_6__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_6__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.gpio_7 - clears drive_strength bits of GPIO_7 pad */
/* clears drive_strength bits of GPIO_7 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_7__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_7__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_7__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_7__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__GPIO_7__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.sda_0 - clears drive_strength bits of SDA_0 pad */
/* clears drive_strength bits of SDA_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SDA_0__SHIFT       24
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SDA_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SDA_0__MASK        0x01000000
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SDA_0__INV_MASK    0xFEFFFFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SDA_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_11_REG_CLR.scl_0 - clears drive_strength bits of SCL_0 pad */
/* clears drive_strength bits of SCL_0 pad */
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SCL_0__SHIFT       26
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SCL_0__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SCL_0__MASK        0x04000000
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SCL_0__INV_MASK    0xFBFFFFFF
#define SW_TOP_STRENGTH_CNTL_11_REG_CLR__SCL_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_12_REG register */
#define SW_TOP_STRENGTH_CNTL_12_REG_SET 0x10E40360

/* SW_TOP_STRENGTH_CNTL_12_REG_SET.coex_pio_0 - sets drive_strength bits of COEX_PIO_0 pad */
/* sets drive_strength bits of COEX_PIO_0 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_SET.coex_pio_1 - sets drive_strength bits of COEX_PIO_1 pad */
/* sets drive_strength bits of COEX_PIO_1 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_SET.coex_pio_2 - sets drive_strength bits of COEX_PIO_2 pad */
/* sets drive_strength bits of COEX_PIO_2 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_SET.coex_pio_3 - sets drive_strength bits of COEX_PIO_3 pad */
/* sets drive_strength bits of COEX_PIO_3 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_12_REG_SET__COEX_PIO_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_12_REG register */
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR 0x10E40364

/* SW_TOP_STRENGTH_CNTL_12_REG_CLR.coex_pio_0 - clears drive_strength bits of COEX_PIO_0 pad */
/* clears drive_strength bits of COEX_PIO_0 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_0__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_0__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_0__MASK        0x00000003
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_0__INV_MASK    0xFFFFFFFC
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_0__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_CLR.coex_pio_1 - clears drive_strength bits of COEX_PIO_1 pad */
/* clears drive_strength bits of COEX_PIO_1 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_1__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_1__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_1__MASK        0x0000000C
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_1__INV_MASK    0xFFFFFFF3
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_1__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_CLR.coex_pio_2 - clears drive_strength bits of COEX_PIO_2 pad */
/* clears drive_strength bits of COEX_PIO_2 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_2__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_2__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_2__MASK        0x00000030
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_2__INV_MASK    0xFFFFFFCF
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_2__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_12_REG_CLR.coex_pio_3 - clears drive_strength bits of COEX_PIO_3 pad */
/* clears drive_strength bits of COEX_PIO_3 pad */
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_3__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_3__WIDTH       2
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_3__MASK        0x000000C0
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_3__INV_MASK    0xFFFFFF3F
#define SW_TOP_STRENGTH_CNTL_12_REG_CLR__COEX_PIO_3__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_13_REG register */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET 0x10E40368

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart0_tx - sets drive_strength bits of UART0_TX pad */
/* sets drive_strength bits of UART0_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_TX__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_TX__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_TX__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart0_rx - sets drive_strength bits of UART0_RX pad */
/* sets drive_strength bits of UART0_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_RX__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_RX__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_RX__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART0_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart1_tx - sets drive_strength bits of UART1_TX pad */
/* sets drive_strength bits of UART1_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_TX__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_TX__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_TX__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart1_rx - sets drive_strength bits of UART1_RX pad */
/* sets drive_strength bits of UART1_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_RX__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_RX__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_RX__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART1_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart3_tx - sets drive_strength bits of UART3_TX pad */
/* sets drive_strength bits of UART3_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_TX__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_TX__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_TX__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart3_rx - sets drive_strength bits of UART3_RX pad */
/* sets drive_strength bits of UART3_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_RX__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_RX__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_RX__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART3_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart4_tx - sets drive_strength bits of UART4_TX pad */
/* sets drive_strength bits of UART4_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_TX__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_TX__MASK        0x00010000
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_TX__INV_MASK    0xFFFEFFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_SET.uart4_rx - sets drive_strength bits of UART4_RX pad */
/* sets drive_strength bits of UART4_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_RX__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_RX__MASK        0x00040000
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_RX__INV_MASK    0xFFFBFFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_SET__UART4_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_13_REG register */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR 0x10E4036C

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart0_tx - clears drive_strength bits of UART0_TX pad */
/* clears drive_strength bits of UART0_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_TX__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_TX__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_TX__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart0_rx - clears drive_strength bits of UART0_RX pad */
/* clears drive_strength bits of UART0_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_RX__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_RX__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_RX__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART0_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart1_tx - clears drive_strength bits of UART1_TX pad */
/* clears drive_strength bits of UART1_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_TX__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_TX__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_TX__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart1_rx - clears drive_strength bits of UART1_RX pad */
/* clears drive_strength bits of UART1_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_RX__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_RX__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_RX__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART1_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart3_tx - clears drive_strength bits of UART3_TX pad */
/* clears drive_strength bits of UART3_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_TX__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_TX__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_TX__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart3_rx - clears drive_strength bits of UART3_RX pad */
/* clears drive_strength bits of UART3_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_RX__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_RX__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_RX__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART3_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart4_tx - clears drive_strength bits of UART4_TX pad */
/* clears drive_strength bits of UART4_TX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_TX__SHIFT       16
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_TX__MASK        0x00010000
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_TX__INV_MASK    0xFFFEFFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_13_REG_CLR.uart4_rx - clears drive_strength bits of UART4_RX pad */
/* clears drive_strength bits of UART4_RX pad */
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_RX__SHIFT       18
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_RX__MASK        0x00040000
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_RX__INV_MASK    0xFFFBFFFF
#define SW_TOP_STRENGTH_CNTL_13_REG_CLR__UART4_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_14_REG register */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET 0x10E40378

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp0_clk - sets drive_strength bits of USP0_CLK pad */
/* sets drive_strength bits of USP0_CLK pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_CLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp0_tx - sets drive_strength bits of USP0_TX pad */
/* sets drive_strength bits of USP0_TX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_TX__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_TX__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_TX__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp0_rx - sets drive_strength bits of USP0_RX pad */
/* sets drive_strength bits of USP0_RX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_RX__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_RX__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_RX__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp0_fs - sets drive_strength bits of USP0_FS pad */
/* sets drive_strength bits of USP0_FS pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_FS__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_FS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_FS__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_FS__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP0_FS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp1_clk - sets drive_strength bits of USP1_CLK pad */
/* sets drive_strength bits of USP1_CLK pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_CLK__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_CLK__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_CLK__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp1_tx - sets drive_strength bits of USP1_TX pad */
/* sets drive_strength bits of USP1_TX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_TX__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_TX__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_TX__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp1_rx - sets drive_strength bits of USP1_RX pad */
/* sets drive_strength bits of USP1_RX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_RX__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_RX__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_RX__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_SET.usp1_fs - sets drive_strength bits of USP1_FS pad */
/* sets drive_strength bits of USP1_FS pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_FS__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_FS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_FS__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_FS__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_14_REG_SET__USP1_FS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_14_REG register */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR 0x10E4037C

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp0_clk - clears drive_strength bits of USP0_CLK pad */
/* clears drive_strength bits of USP0_CLK pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_CLK__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_CLK__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_CLK__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp0_tx - clears drive_strength bits of USP0_TX pad */
/* clears drive_strength bits of USP0_TX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_TX__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_TX__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_TX__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp0_rx - clears drive_strength bits of USP0_RX pad */
/* clears drive_strength bits of USP0_RX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_RX__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_RX__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_RX__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp0_fs - clears drive_strength bits of USP0_FS pad */
/* clears drive_strength bits of USP0_FS pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_FS__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_FS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_FS__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_FS__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP0_FS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp1_clk - clears drive_strength bits of USP1_CLK pad */
/* clears drive_strength bits of USP1_CLK pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_CLK__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_CLK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_CLK__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_CLK__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_CLK__HW_DEFAULT  0x1

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp1_tx - clears drive_strength bits of USP1_TX pad */
/* clears drive_strength bits of USP1_TX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_TX__SHIFT       10
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_TX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_TX__MASK        0x00000400
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_TX__INV_MASK    0xFFFFFBFF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_TX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp1_rx - clears drive_strength bits of USP1_RX pad */
/* clears drive_strength bits of USP1_RX pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_RX__SHIFT       12
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_RX__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_RX__MASK        0x00001000
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_RX__INV_MASK    0xFFFFEFFF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_RX__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_14_REG_CLR.usp1_fs - clears drive_strength bits of USP1_FS pad */
/* clears drive_strength bits of USP1_FS pad */
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_FS__SHIFT       14
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_FS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_FS__MASK        0x00004000
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_FS__INV_MASK    0xFFFFBFFF
#define SW_TOP_STRENGTH_CNTL_14_REG_CLR__USP1_FS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_SET Address */
/* Sets selected bits of SW_TOP_STRENGTH_CNTL_15_REG register */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET 0x10E40380

/* SW_TOP_STRENGTH_CNTL_15_REG_SET.jtag_tdo - sets drive_strength bits of JTAG_TDO pad */
/* sets drive_strength bits of JTAG_TDO pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDO__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDO__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDO__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDO__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDO__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_SET.jtag_tms - sets drive_strength bits of JTAG_TMS pad */
/* sets drive_strength bits of JTAG_TMS pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TMS__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TMS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TMS__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TMS__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TMS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_SET.jtag_tck - sets drive_strength bits of JTAG_TCK pad */
/* sets drive_strength bits of JTAG_TCK pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TCK__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TCK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TCK__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TCK__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TCK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_SET.jtag_tdi - sets drive_strength bits of JTAG_TDI pad */
/* sets drive_strength bits of JTAG_TDI pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDI__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDI__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDI__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDI__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TDI__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_SET.jtag_trstn - sets drive_strength bits of JTAG_TRSTN pad */
/* sets drive_strength bits of JTAG_TRSTN pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TRSTN__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TRSTN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TRSTN__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TRSTN__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_15_REG_SET__JTAG_TRSTN__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR Address */
/* Clears selected bits of SW_TOP_STRENGTH_CNTL_15_REG register */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR 0x10E40384

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR.jtag_tdo - clears drive_strength bits of JTAG_TDO pad */
/* clears drive_strength bits of JTAG_TDO pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDO__SHIFT       0
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDO__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDO__MASK        0x00000001
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDO__INV_MASK    0xFFFFFFFE
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDO__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR.jtag_tms - clears drive_strength bits of JTAG_TMS pad */
/* clears drive_strength bits of JTAG_TMS pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TMS__SHIFT       2
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TMS__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TMS__MASK        0x00000004
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TMS__INV_MASK    0xFFFFFFFB
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TMS__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR.jtag_tck - clears drive_strength bits of JTAG_TCK pad */
/* clears drive_strength bits of JTAG_TCK pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TCK__SHIFT       4
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TCK__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TCK__MASK        0x00000010
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TCK__INV_MASK    0xFFFFFFEF
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TCK__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR.jtag_tdi - clears drive_strength bits of JTAG_TDI pad */
/* clears drive_strength bits of JTAG_TDI pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDI__SHIFT       6
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDI__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDI__MASK        0x00000040
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDI__INV_MASK    0xFFFFFFBF
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TDI__HW_DEFAULT  0x0

/* SW_TOP_STRENGTH_CNTL_15_REG_CLR.jtag_trstn - clears drive_strength bits of JTAG_TRSTN pad */
/* clears drive_strength bits of JTAG_TRSTN pad */
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TRSTN__SHIFT       8
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TRSTN__WIDTH       1
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TRSTN__MASK        0x00000100
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TRSTN__INV_MASK    0xFFFFFEFF
#define SW_TOP_STRENGTH_CNTL_15_REG_CLR__JTAG_TRSTN__HW_DEFAULT  0x0

/* SW_TOP_AD_CNTL_0_REG_SET Address */
/* Sets selected bits of SW_TOP_AD_CNTL_0_REG register */
#define SW_TOP_AD_CNTL_0_REG_SET  0x10E40480

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d4p - sets ad bits of LVDS_TX0D4P pad */
/* sets ad bits of LVDS_TX0D4P pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__MASK        0x00000001
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__INV_MASK    0xFFFFFFFE
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d4n - sets ad bits of LVDS_TX0D4N pad */
/* sets ad bits of LVDS_TX0D4N pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__SHIFT       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__MASK        0x00000002
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__INV_MASK    0xFFFFFFFD
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D4N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d3p - sets ad bits of LVDS_TX0D3P pad */
/* sets ad bits of LVDS_TX0D3P pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__SHIFT       2
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__MASK        0x00000004
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__INV_MASK    0xFFFFFFFB
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d3n - sets ad bits of LVDS_TX0D3N pad */
/* sets ad bits of LVDS_TX0D3N pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__SHIFT       3
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__MASK        0x00000008
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__INV_MASK    0xFFFFFFF7
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D3N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d2p - sets ad bits of LVDS_TX0D2P pad */
/* sets ad bits of LVDS_TX0D2P pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__SHIFT       4
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__MASK        0x00000010
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__INV_MASK    0xFFFFFFEF
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d2n - sets ad bits of LVDS_TX0D2N pad */
/* sets ad bits of LVDS_TX0D2N pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__SHIFT       5
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__MASK        0x00000020
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__INV_MASK    0xFFFFFFDF
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D2N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d1p - sets ad bits of LVDS_TX0D1P pad */
/* sets ad bits of LVDS_TX0D1P pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__SHIFT       6
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__MASK        0x00000040
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__INV_MASK    0xFFFFFFBF
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d1n - sets ad bits of LVDS_TX0D1N pad */
/* sets ad bits of LVDS_TX0D1N pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__SHIFT       7
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__MASK        0x00000080
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__INV_MASK    0xFFFFFF7F
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D1N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d0p - sets ad bits of LVDS_TX0D0P pad */
/* sets ad bits of LVDS_TX0D0P pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__SHIFT       8
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__MASK        0x00000100
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__INV_MASK    0xFFFFFEFF
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_SET.lvds_tx0d0n - sets ad bits of LVDS_TX0D0N pad */
/* sets ad bits of LVDS_TX0D0N pad */
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__SHIFT       9
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__MASK        0x00000200
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__INV_MASK    0xFFFFFDFF
#define SW_TOP_AD_CNTL_0_REG_SET__LVDS_TX0D0N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR Address */
/* Clears selected bits of SW_TOP_AD_CNTL_0_REG register */
#define SW_TOP_AD_CNTL_0_REG_CLR  0x10E40484

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d4p - clears ad bits of LVDS_TX0D4P pad */
/* clears ad bits of LVDS_TX0D4P pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__SHIFT       0
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__MASK        0x00000001
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__INV_MASK    0xFFFFFFFE
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d4n - clears ad bits of LVDS_TX0D4N pad */
/* clears ad bits of LVDS_TX0D4N pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__SHIFT       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__MASK        0x00000002
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__INV_MASK    0xFFFFFFFD
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D4N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d3p - clears ad bits of LVDS_TX0D3P pad */
/* clears ad bits of LVDS_TX0D3P pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__SHIFT       2
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__MASK        0x00000004
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__INV_MASK    0xFFFFFFFB
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d3n - clears ad bits of LVDS_TX0D3N pad */
/* clears ad bits of LVDS_TX0D3N pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__SHIFT       3
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__MASK        0x00000008
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__INV_MASK    0xFFFFFFF7
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D3N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d2p - clears ad bits of LVDS_TX0D2P pad */
/* clears ad bits of LVDS_TX0D2P pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__SHIFT       4
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__MASK        0x00000010
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__INV_MASK    0xFFFFFFEF
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d2n - clears ad bits of LVDS_TX0D2N pad */
/* clears ad bits of LVDS_TX0D2N pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__SHIFT       5
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__MASK        0x00000020
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__INV_MASK    0xFFFFFFDF
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D2N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d1p - clears ad bits of LVDS_TX0D1P pad */
/* clears ad bits of LVDS_TX0D1P pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__SHIFT       6
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__MASK        0x00000040
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__INV_MASK    0xFFFFFFBF
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d1n - clears ad bits of LVDS_TX0D1N pad */
/* clears ad bits of LVDS_TX0D1N pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__SHIFT       7
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__MASK        0x00000080
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__INV_MASK    0xFFFFFF7F
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D1N__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d0p - clears ad bits of LVDS_TX0D0P pad */
/* clears ad bits of LVDS_TX0D0P pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__SHIFT       8
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__MASK        0x00000100
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__INV_MASK    0xFFFFFEFF
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0P__HW_DEFAULT  0x1

/* SW_TOP_AD_CNTL_0_REG_CLR.lvds_tx0d0n - clears ad bits of LVDS_TX0D0N pad */
/* clears ad bits of LVDS_TX0D0N pad */
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__SHIFT       9
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__WIDTH       1
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__MASK        0x00000200
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__INV_MASK    0xFFFFFDFF
#define SW_TOP_AD_CNTL_0_REG_CLR__LVDS_TX0D0N__HW_DEFAULT  0x1


#endif  // __IOCTOP_H__
