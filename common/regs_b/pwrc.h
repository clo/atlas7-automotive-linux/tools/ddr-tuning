/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __PWRC_H__
#define __PWRC_H__



/* PWRC */
/* ==================================================================== */

/* PWRC Power Down Control SET Register */
/* For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input) */
#define PWRC_PDN_CTRL_SET         0x18843000

/* PWRC_PDN_CTRL_SET.START_PSAVING - Power Saving control */
/* 0 : Reserved */
/* 1 : Begin to go into power saving mode. 
 Note: when software writes 1, the PWRC will begin entering into power saving mode and the bit will be cleared by hardware itself. */
#define PWRC_PDN_CTRL_SET__START_PSAVING__SHIFT       0
#define PWRC_PDN_CTRL_SET__START_PSAVING__WIDTH       1
#define PWRC_PDN_CTRL_SET__START_PSAVING__MASK        0x00000001
#define PWRC_PDN_CTRL_SET__START_PSAVING__INV_MASK    0xFFFFFFFE
#define PWRC_PDN_CTRL_SET__START_PSAVING__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.PSAVING_CONFG - Power saving mode configuration */
/* 0 : Hibernation mode */
/* 1 : Deep sleep mode */
#define PWRC_PDN_CTRL_SET__PSAVING_CONFG__SHIFT       1
#define PWRC_PDN_CTRL_SET__PSAVING_CONFG__WIDTH       2
#define PWRC_PDN_CTRL_SET__PSAVING_CONFG__MASK        0x00000006
#define PWRC_PDN_CTRL_SET__PSAVING_CONFG__INV_MASK    0xFFFFFFF9
#define PWRC_PDN_CTRL_SET__PSAVING_CONFG__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.DRAM_HOLD - Memory pad retention control. 
 When entering into power saving mode, the hardware will automatically set this bit and let memory pads keep a program status in power saving mode. 
 After wakeup, software should clear it before using memory. After clear it, software should wait for at least 10us to ensure memory pads stable. */
/* 0 : De-activate memory pad retention */
/* 1 : Activate memory pad retention. 
 It is recommended that software will not set this bit to 1, and let hardware to set it automatically, according to dram_hold_set_req */
#define PWRC_PDN_CTRL_SET__DRAM_HOLD__SHIFT       3
#define PWRC_PDN_CTRL_SET__DRAM_HOLD__WIDTH       1
#define PWRC_PDN_CTRL_SET__DRAM_HOLD__MASK        0x00000008
#define PWRC_PDN_CTRL_SET__DRAM_HOLD__INV_MASK    0xFFFFFFF7
#define PWRC_PDN_CTRL_SET__DRAM_HOLD__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.SWITCHMODE_EN - Switch mode enable from the Power Saving mode to the Cold Boot mode when battery/adapter voltage is low */
/* 0 : Switch mode is disabled */
/* 1 : Switch mode is enabled */
#define PWRC_PDN_CTRL_SET__SWITCHMODE_EN__SHIFT       6
#define PWRC_PDN_CTRL_SET__SWITCHMODE_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__SWITCHMODE_EN__MASK        0x00000040
#define PWRC_PDN_CTRL_SET__SWITCHMODE_EN__INV_MASK    0xFFFFFFBF
#define PWRC_PDN_CTRL_SET__SWITCHMODE_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.FORCE_SHUTDOWN_TIME - Program the threshold time for X_ON_KEY_B or X_EXT_ON force shutdown. 
 This threshold time is used in following two cases: 
 Case 1: If the system is boot through the 320ms X_ON_KEY_B or X_EXT_ON event from the Cold Boot Mode or Power Saving Mode, and within this threshold time the software does not set the WATCHDOG_STOP register, the system will force shut-down. 
 Case 2: If the X_ON_KEY_B valid duration time exceeds this threshold during either the Normal Mode or Power Saving Mode, the system will force shut-down. 
 Note that this feature also requires PWRC_RTC_DCOG.SHUTDOWN_MODE to be 1 (disabled by default). */
/* 0 :  5 seconds */
/* 1 :  8 seconds */
/* 2 : 12 seconds */
/* 3 : 15 seconds */
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_TIME__SHIFT       7
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_TIME__WIDTH       2
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_TIME__MASK        0x00000180
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_TIME__INV_MASK    0xFFFFFE7F
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_TIME__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.WATCHDOG_STOP - This bit is used for stopping the counter related with X_ON_KEY_B or X_EXT_ON force shut-down, which is happened when the system is boot from X_ON_KEY_B or X_EXT_ON. 
 HW will clear this bit when entering into either Power Saving Mode or Cold Boot Mode automatically. */
/* 0 : Enable the counter related with X_ON_KEY_B or X_EXT_ON force shut-down */
/* 1 : Disable the counter related with X_ON_KEY_B or X_EXT_ON force shut-down */
#define PWRC_PDN_CTRL_SET__WATCHDOG_STOP__SHIFT       9
#define PWRC_PDN_CTRL_SET__WATCHDOG_STOP__WIDTH       1
#define PWRC_PDN_CTRL_SET__WATCHDOG_STOP__MASK        0x00000200
#define PWRC_PDN_CTRL_SET__WATCHDOG_STOP__INV_MASK    0xFFFFFDFF
#define PWRC_PDN_CTRL_SET__WATCHDOG_STOP__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.ONKEY_WATCHDOG_EN - Enable the X_ON_KEY_B force shut-down happened when the system is boot from X_ON_KEY_B. */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_SET__ONKEY_WATCHDOG_EN__SHIFT       10
#define PWRC_PDN_CTRL_SET__ONKEY_WATCHDOG_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__ONKEY_WATCHDOG_EN__MASK        0x00000400
#define PWRC_PDN_CTRL_SET__ONKEY_WATCHDOG_EN__INV_MASK    0xFFFFFBFF
#define PWRC_PDN_CTRL_SET__ONKEY_WATCHDOG_EN__HW_DEFAULT  0x1

/* PWRC_PDN_CTRL_SET.EXTON_WATCHDOG_EN - Enable the X_EXT_ON force shut-down happened when the system is boot from X_EXT_ON. */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_SET__EXTON_WATCHDOG_EN__SHIFT       11
#define PWRC_PDN_CTRL_SET__EXTON_WATCHDOG_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__EXTON_WATCHDOG_EN__MASK        0x00000800
#define PWRC_PDN_CTRL_SET__EXTON_WATCHDOG_EN__INV_MASK    0xFFFFF7FF
#define PWRC_PDN_CTRL_SET__EXTON_WATCHDOG_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.FORCE_SHUTDOWN_EN - Enable the X_ON_KEY_B force shut-down happened when X_ON_KEY_B valid duration time exceeds the FORCE_SHUTDOWN_TIME. 
 Note that this feature also requires PWRC_RTC_DCOG.SHUTDOWN_MODE to be 1 (disabled by default). */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_EN__SHIFT       12
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_EN__MASK        0x00001000
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_EN__INV_MASK    0xFFFFEFFF
#define PWRC_PDN_CTRL_SET__FORCE_SHUTDOWN_EN__HW_DEFAULT  0x1

/* PWRC_PDN_CTRL_SET.RTCRST_SYSRTC_EN - X_REBOOT_B reset the system RTC controlling. */
/* 0 : X_REBOOT_B cannot be used to reset the system RTC block */
/* 1 : X_REBOOT_B can be used to reset the system RTC block */
#define PWRC_PDN_CTRL_SET__RTCRST_SYSRTC_EN__SHIFT       13
#define PWRC_PDN_CTRL_SET__RTCRST_SYSRTC_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__RTCRST_SYSRTC_EN__MASK        0x00002000
#define PWRC_PDN_CTRL_SET__RTCRST_SYSRTC_EN__INV_MASK    0xFFFFDFFF
#define PWRC_PDN_CTRL_SET__RTCRST_SYSRTC_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.LPM_AUTO_EN - Automatic controlling for VDIGPMU low power mode */
/* 0 : Low power mode of VDIGPMU is under the controlling of LMP_REG_EN */
/* 1 : VDIGPMU will enter into low power mode when system entering into Power Saving Mode */
#define PWRC_PDN_CTRL_SET__LPM_AUTO_EN__SHIFT       20
#define PWRC_PDN_CTRL_SET__LPM_AUTO_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__LPM_AUTO_EN__MASK        0x00100000
#define PWRC_PDN_CTRL_SET__LPM_AUTO_EN__INV_MASK    0xFFEFFFFF
#define PWRC_PDN_CTRL_SET__LPM_AUTO_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.LPM_REG_EN - low power mode of VDIGPMU controlling when LPM_AUTO_EN is disabled */
/* 0 : Low power mode is disabled */
/* 1 : Low power mode is enabled */
#define PWRC_PDN_CTRL_SET__LPM_REG_EN__SHIFT       21
#define PWRC_PDN_CTRL_SET__LPM_REG_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__LPM_REG_EN__MASK        0x00200000
#define PWRC_PDN_CTRL_SET__LPM_REG_EN__INV_MASK    0xFFDFFFFF
#define PWRC_PDN_CTRL_SET__LPM_REG_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_SET.TSADC_POWER_EN - Enable Touch Screen ADC power and its digital power. */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_PDN_CTRL_SET__TSADC_POWER_EN__SHIFT       22
#define PWRC_PDN_CTRL_SET__TSADC_POWER_EN__WIDTH       1
#define PWRC_PDN_CTRL_SET__TSADC_POWER_EN__MASK        0x00400000
#define PWRC_PDN_CTRL_SET__TSADC_POWER_EN__INV_MASK    0xFFBFFFFF
#define PWRC_PDN_CTRL_SET__TSADC_POWER_EN__HW_DEFAULT  0x1

/* PWRC Power Down Control CLR Register */
/* For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input) */
#define PWRC_PDN_CTRL_CLR         0x18843004

/* PWRC_PDN_CTRL_CLR.START_PSAVING - Power Saving control */
/* 0 : Reserved */
/* 1 : Begin to go into power saving mode. 
 Note: when software writes 1, the PWRC will begin entering into power saving mode and the bit will be cleared by hardware itself. */
#define PWRC_PDN_CTRL_CLR__START_PSAVING__SHIFT       0
#define PWRC_PDN_CTRL_CLR__START_PSAVING__WIDTH       1
#define PWRC_PDN_CTRL_CLR__START_PSAVING__MASK        0x00000001
#define PWRC_PDN_CTRL_CLR__START_PSAVING__INV_MASK    0xFFFFFFFE
#define PWRC_PDN_CTRL_CLR__START_PSAVING__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.PSAVING_CONFG - Power saving mode configuration */
/* 0 : Hibernation mode */
/* 1 : Deep sleep mode */
#define PWRC_PDN_CTRL_CLR__PSAVING_CONFG__SHIFT       1
#define PWRC_PDN_CTRL_CLR__PSAVING_CONFG__WIDTH       2
#define PWRC_PDN_CTRL_CLR__PSAVING_CONFG__MASK        0x00000006
#define PWRC_PDN_CTRL_CLR__PSAVING_CONFG__INV_MASK    0xFFFFFFF9
#define PWRC_PDN_CTRL_CLR__PSAVING_CONFG__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.DRAM_HOLD - Memory pad retention control. 
 When entering into power saving mode, the hardware will automatically set this bit and let memory pads keep a program status in power saving mode. 
 After wakeup, software should clear it before using memory. After clear it, software should wait for at least 10us to ensure memory pads stable. */
/* 0 : De-activate memory pad retention */
/* 1 : Activate memory pad retention. 
 It is recommended that software will not set this bit to 1, and let hardware to set it automatically, according to dram_hold_set_req */
#define PWRC_PDN_CTRL_CLR__DRAM_HOLD__SHIFT       3
#define PWRC_PDN_CTRL_CLR__DRAM_HOLD__WIDTH       1
#define PWRC_PDN_CTRL_CLR__DRAM_HOLD__MASK        0x00000008
#define PWRC_PDN_CTRL_CLR__DRAM_HOLD__INV_MASK    0xFFFFFFF7
#define PWRC_PDN_CTRL_CLR__DRAM_HOLD__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.SWITCHMODE_EN - Switch mode enable from the Power Saving mode to the Cold Boot mode when battery/adapter voltage is low */
/* 0 : Switch mode is disabled */
/* 1 : Switch mode is enabled */
#define PWRC_PDN_CTRL_CLR__SWITCHMODE_EN__SHIFT       6
#define PWRC_PDN_CTRL_CLR__SWITCHMODE_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__SWITCHMODE_EN__MASK        0x00000040
#define PWRC_PDN_CTRL_CLR__SWITCHMODE_EN__INV_MASK    0xFFFFFFBF
#define PWRC_PDN_CTRL_CLR__SWITCHMODE_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.FORCE_SHUTDOWN_TIME - Program the threshold time for X_ON_KEY_B or X_EXT_ON force shutdown. 
 This threshold time is used in following two cases: 
 Case 1: If the system is boot through the 320ms X_ON_KEY_B or X_EXT_ON event from the Cold Boot Mode or Power Saving Mode, and within this threshold time the software does not set the WATCHDOG_STOP register, the system will force shut-down. 
 Case 2: If the X_ON_KEY_B valid duration time exceeds this threshold during either the Normal Mode or Power Saving Mode, the system will force shut-down. 
 Note that this feature also requires PWRC_RTC_DCOG.SHUTDOWN_MODE to be 1 (disabled by default). */
/* 0 :  5 seconds */
/* 1 :  8 seconds */
/* 2 : 12 seconds */
/* 3 : 15 seconds */
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_TIME__SHIFT       7
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_TIME__WIDTH       2
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_TIME__MASK        0x00000180
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_TIME__INV_MASK    0xFFFFFE7F
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_TIME__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.WATCHDOG_STOP - This bit is used for stopping the counter related with X_ON_KEY_B or X_EXT_ON force shut-down, which is happened when the system is boot from X_ON_KEY_B or X_EXT_ON. 
 HW will clear this bit when entering into either Power Saving Mode or Cold Boot Mode automatically. */
/* 0 : Enable the counter related with X_ON_KEY_B or X_EXT_ON force shut-down */
/* 1 : Disable the counter related with X_ON_KEY_B or X_EXT_ON force shut-down */
#define PWRC_PDN_CTRL_CLR__WATCHDOG_STOP__SHIFT       9
#define PWRC_PDN_CTRL_CLR__WATCHDOG_STOP__WIDTH       1
#define PWRC_PDN_CTRL_CLR__WATCHDOG_STOP__MASK        0x00000200
#define PWRC_PDN_CTRL_CLR__WATCHDOG_STOP__INV_MASK    0xFFFFFDFF
#define PWRC_PDN_CTRL_CLR__WATCHDOG_STOP__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.ONKEY_WATCHDOG_EN - Enable the X_ON_KEY_B force shut-down happened when the system is boot from X_ON_KEY_B. */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_CLR__ONKEY_WATCHDOG_EN__SHIFT       10
#define PWRC_PDN_CTRL_CLR__ONKEY_WATCHDOG_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__ONKEY_WATCHDOG_EN__MASK        0x00000400
#define PWRC_PDN_CTRL_CLR__ONKEY_WATCHDOG_EN__INV_MASK    0xFFFFFBFF
#define PWRC_PDN_CTRL_CLR__ONKEY_WATCHDOG_EN__HW_DEFAULT  0x1

/* PWRC_PDN_CTRL_CLR.EXTON_WATCHDOG_EN - Enable the X_EXT_ON force shut-down happened when the system is boot from X_EXT_ON. */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_CLR__EXTON_WATCHDOG_EN__SHIFT       11
#define PWRC_PDN_CTRL_CLR__EXTON_WATCHDOG_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__EXTON_WATCHDOG_EN__MASK        0x00000800
#define PWRC_PDN_CTRL_CLR__EXTON_WATCHDOG_EN__INV_MASK    0xFFFFF7FF
#define PWRC_PDN_CTRL_CLR__EXTON_WATCHDOG_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.FORCE_SHUTDOWN_EN - Enable the X_ON_KEY_B force shut-down happened when X_ON_KEY_B valid duration time exceeds the FORCE_SHUTDOWN_TIME. 
 Note that this feature also requires PWRC_RTC_DCOG.SHUTDOWN_MODE to be 1 (disabled by default). */
/* 0 : Disable this feature */
/* 1 : Enable this feature */
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_EN__SHIFT       12
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_EN__MASK        0x00001000
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_EN__INV_MASK    0xFFFFEFFF
#define PWRC_PDN_CTRL_CLR__FORCE_SHUTDOWN_EN__HW_DEFAULT  0x1

/* PWRC_PDN_CTRL_CLR.RTCRST_SYSRTC_EN - X_REBOOT_B reset the system RTC controlling. */
/* 0 : X_REBOOT_B cannot be used to reset the system RTC block */
/* 1 : X_REBOOT_B can be used to reset the system RTC block */
#define PWRC_PDN_CTRL_CLR__RTCRST_SYSRTC_EN__SHIFT       13
#define PWRC_PDN_CTRL_CLR__RTCRST_SYSRTC_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__RTCRST_SYSRTC_EN__MASK        0x00002000
#define PWRC_PDN_CTRL_CLR__RTCRST_SYSRTC_EN__INV_MASK    0xFFFFDFFF
#define PWRC_PDN_CTRL_CLR__RTCRST_SYSRTC_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.LPM_AUTO_EN - Automatic controlling for VDIGPMU low power mode */
/* 0 : Low power mode of VDIGPMU is under the controlling of LMP_REG_EN */
/* 1 : VDIGPMU will enter into low power mode when system entering into Power Saving Mode */
#define PWRC_PDN_CTRL_CLR__LPM_AUTO_EN__SHIFT       20
#define PWRC_PDN_CTRL_CLR__LPM_AUTO_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__LPM_AUTO_EN__MASK        0x00100000
#define PWRC_PDN_CTRL_CLR__LPM_AUTO_EN__INV_MASK    0xFFEFFFFF
#define PWRC_PDN_CTRL_CLR__LPM_AUTO_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.LPM_REG_EN - low power mode of VDIGPMU controlling when LPM_AUTO_EN is disabled */
/* 0 : Low power mode is disabled */
/* 1 : Low power mode is enabled */
#define PWRC_PDN_CTRL_CLR__LPM_REG_EN__SHIFT       21
#define PWRC_PDN_CTRL_CLR__LPM_REG_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__LPM_REG_EN__MASK        0x00200000
#define PWRC_PDN_CTRL_CLR__LPM_REG_EN__INV_MASK    0xFFDFFFFF
#define PWRC_PDN_CTRL_CLR__LPM_REG_EN__HW_DEFAULT  0x0

/* PWRC_PDN_CTRL_CLR.TSADC_POWER_EN - Enable Touch Screen ADC power and its digital power. */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_PDN_CTRL_CLR__TSADC_POWER_EN__SHIFT       22
#define PWRC_PDN_CTRL_CLR__TSADC_POWER_EN__WIDTH       1
#define PWRC_PDN_CTRL_CLR__TSADC_POWER_EN__MASK        0x00400000
#define PWRC_PDN_CTRL_CLR__TSADC_POWER_EN__INV_MASK    0xFFBFFFFF
#define PWRC_PDN_CTRL_CLR__TSADC_POWER_EN__HW_DEFAULT  0x1

/* PWRC Power On Status Register */
/* This register is used by SW for determination of boot cause. */
#define PWRC_PON_STATUS           0x18843008

/* PWRC_PON_STATUS.WARM_BOOT - Warm boot indication. Writing 0 will clear it. */
/* 0 : Power-up from cold boot mode */
/* 1 : Wakeup from power saving mode */
#define PWRC_PON_STATUS__WARM_BOOT__SHIFT       0
#define PWRC_PON_STATUS__WARM_BOOT__WIDTH       1
#define PWRC_PON_STATUS__WARM_BOOT__MASK        0x00000001
#define PWRC_PON_STATUS__WARM_BOOT__INV_MASK    0xFFFFFFFE
#define PWRC_PON_STATUS__WARM_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.SYSRTC_BEEN_RST - This bit is used to indicate whether the system RTC has been reset when the system is boot from Cold Boot Mode. 
 If the system RTC is reset, the software should write this bit to 1 after the system is boot. 
 Then for the next cold boot, the software can use this bit to judge whether the system RTC has been reset. */
/* 0 : System RTC has been reset when the cold boot event happened. */
/* 1 : Since the software writes this bit to 1, the system RTC is not reset. */
#define PWRC_PON_STATUS__SYSRTC_BEEN_RST__SHIFT       1
#define PWRC_PON_STATUS__SYSRTC_BEEN_RST__WIDTH       1
#define PWRC_PON_STATUS__SYSRTC_BEEN_RST__MASK        0x00000002
#define PWRC_PON_STATUS__SYSRTC_BEEN_RST__INV_MASK    0xFFFFFFFD
#define PWRC_PON_STATUS__SYSRTC_BEEN_RST__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.ON_KEY_BOOT - Boot was triggered by ON_KEY event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__ON_KEY_BOOT__SHIFT       2
#define PWRC_PON_STATUS__ON_KEY_BOOT__WIDTH       1
#define PWRC_PON_STATUS__ON_KEY_BOOT__MASK        0x00000004
#define PWRC_PON_STATUS__ON_KEY_BOOT__INV_MASK    0xFFFFFFFB
#define PWRC_PON_STATUS__ON_KEY_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.EXT_ON_BOOT - Boot was triggered by EXT_ON event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__EXT_ON_BOOT__SHIFT       3
#define PWRC_PON_STATUS__EXT_ON_BOOT__WIDTH       1
#define PWRC_PON_STATUS__EXT_ON_BOOT__MASK        0x00000008
#define PWRC_PON_STATUS__EXT_ON_BOOT__INV_MASK    0xFFFFFFF7
#define PWRC_PON_STATUS__EXT_ON_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.RTC_ALARM0_BOOT - Boot was triggered by RTC alarm0 event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__RTC_ALARM0_BOOT__SHIFT       4
#define PWRC_PON_STATUS__RTC_ALARM0_BOOT__WIDTH       1
#define PWRC_PON_STATUS__RTC_ALARM0_BOOT__MASK        0x00000010
#define PWRC_PON_STATUS__RTC_ALARM0_BOOT__INV_MASK    0xFFFFFFEF
#define PWRC_PON_STATUS__RTC_ALARM0_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.RTC_ALARM1_BOOT - Boot was triggered by RTC alarm1 event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__RTC_ALARM1_BOOT__SHIFT       5
#define PWRC_PON_STATUS__RTC_ALARM1_BOOT__WIDTH       1
#define PWRC_PON_STATUS__RTC_ALARM1_BOOT__MASK        0x00000020
#define PWRC_PON_STATUS__RTC_ALARM1_BOOT__INV_MASK    0xFFFFFFDF
#define PWRC_PON_STATUS__RTC_ALARM1_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.GPIO_BOOT - Boot was triggered by X_GPIO[3:0] event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__GPIO_BOOT__SHIFT       7
#define PWRC_PON_STATUS__GPIO_BOOT__WIDTH       4
#define PWRC_PON_STATUS__GPIO_BOOT__MASK        0x00000780
#define PWRC_PON_STATUS__GPIO_BOOT__INV_MASK    0xFFFFF87F
#define PWRC_PON_STATUS__GPIO_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.RTC_RST_BOOT - Boot was triggered by X_REBOOT_B event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__RTC_RST_BOOT__SHIFT       11
#define PWRC_PON_STATUS__RTC_RST_BOOT__WIDTH       1
#define PWRC_PON_STATUS__RTC_RST_BOOT__MASK        0x00000800
#define PWRC_PON_STATUS__RTC_RST_BOOT__INV_MASK    0xFFFFF7FF
#define PWRC_PON_STATUS__RTC_RST_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.GNSS_BOOT - Boot was triggered by the GNSS request power ON event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__GNSS_BOOT__SHIFT       14
#define PWRC_PON_STATUS__GNSS_BOOT__WIDTH       1
#define PWRC_PON_STATUS__GNSS_BOOT__MASK        0x00004000
#define PWRC_PON_STATUS__GNSS_BOOT__INV_MASK    0xFFFFBFFF
#define PWRC_PON_STATUS__GNSS_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.CAN_BOOT - Boot was triggered by the CAN request power ON event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__CAN_BOOT__SHIFT       15
#define PWRC_PON_STATUS__CAN_BOOT__WIDTH       1
#define PWRC_PON_STATUS__CAN_BOOT__MASK        0x00008000
#define PWRC_PON_STATUS__CAN_BOOT__INV_MASK    0xFFFF7FFF
#define PWRC_PON_STATUS__CAN_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.A7_WDOG_SW_BOOT - Boot was triggered by a7_wd_sw_rst_b event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__A7_WDOG_SW_BOOT__SHIFT       16
#define PWRC_PON_STATUS__A7_WDOG_SW_BOOT__WIDTH       1
#define PWRC_PON_STATUS__A7_WDOG_SW_BOOT__MASK        0x00010000
#define PWRC_PON_STATUS__A7_WDOG_SW_BOOT__INV_MASK    0xFFFEFFFF
#define PWRC_PON_STATUS__A7_WDOG_SW_BOOT__HW_DEFAULT  0x0

/* PWRC_PON_STATUS.A7_WARM_RST_BOOT - Boot was triggered by a7_warm_reset_b event. Writing 0 will clear it. */
/* 0 : Not happen */
/* 1 : Happen */
#define PWRC_PON_STATUS__A7_WARM_RST_BOOT__SHIFT       17
#define PWRC_PON_STATUS__A7_WARM_RST_BOOT__WIDTH       1
#define PWRC_PON_STATUS__A7_WARM_RST_BOOT__MASK        0x00020000
#define PWRC_PON_STATUS__A7_WARM_RST_BOOT__INV_MASK    0xFFFDFFFF
#define PWRC_PON_STATUS__A7_WARM_RST_BOOT__HW_DEFAULT  0x0

/* PWRC Trigger Enable SET Register */
/* For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input) */
#define PWRC_TRIGGER_EN_SET       0x1884300C

/* PWRC_TRIGGER_EN_SET.ON_KEY_EN - Enable X_ON_KEY_B to trigger to wakeup from power saving or cold boot mode or generate interrupt in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__ON_KEY_EN__SHIFT       0
#define PWRC_TRIGGER_EN_SET__ON_KEY_EN__WIDTH       1
#define PWRC_TRIGGER_EN_SET__ON_KEY_EN__MASK        0x00000001
#define PWRC_TRIGGER_EN_SET__ON_KEY_EN__INV_MASK    0xFFFFFFFE
#define PWRC_TRIGGER_EN_SET__ON_KEY_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_SET.EXT_ON_EN - X_EXT_ON to trigger to wakeup from power saving or cold boot mode or generate interrupt in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__EXT_ON_EN__SHIFT       1
#define PWRC_TRIGGER_EN_SET__EXT_ON_EN__WIDTH       1
#define PWRC_TRIGGER_EN_SET__EXT_ON_EN__MASK        0x00000002
#define PWRC_TRIGGER_EN_SET__EXT_ON_EN__INV_MASK    0xFFFFFFFD
#define PWRC_TRIGGER_EN_SET__EXT_ON_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_SET.LOW_BAT_EN - Enable X_LOWBATT_B to trigger to both generate interrupt and set the interrupt status in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__LOW_BAT_EN__SHIFT       2
#define PWRC_TRIGGER_EN_SET__LOW_BAT_EN__WIDTH       1
#define PWRC_TRIGGER_EN_SET__LOW_BAT_EN__MASK        0x00000004
#define PWRC_TRIGGER_EN_SET__LOW_BAT_EN__INV_MASK    0xFFFFFFFB
#define PWRC_TRIGGER_EN_SET__LOW_BAT_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_SET.MULT_BUTTON_EN - Enable MULT_BUTTON1 and MULT_BUTTON2 to trigger interrupt */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__MULT_BUTTON_EN__SHIFT       3
#define PWRC_TRIGGER_EN_SET__MULT_BUTTON_EN__WIDTH       2
#define PWRC_TRIGGER_EN_SET__MULT_BUTTON_EN__MASK        0x00000018
#define PWRC_TRIGGER_EN_SET__MULT_BUTTON_EN__INV_MASK    0xFFFFFFE7
#define PWRC_TRIGGER_EN_SET__MULT_BUTTON_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.RTC_ALARM0_EN - Enable RTC alarm0 to trigger to wakeup */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__RTC_ALARM0_EN__SHIFT       5
#define PWRC_TRIGGER_EN_SET__RTC_ALARM0_EN__WIDTH       1
#define PWRC_TRIGGER_EN_SET__RTC_ALARM0_EN__MASK        0x00000020
#define PWRC_TRIGGER_EN_SET__RTC_ALARM0_EN__INV_MASK    0xFFFFFFDF
#define PWRC_TRIGGER_EN_SET__RTC_ALARM0_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.RTC_ALARM1_EN - Enable RTC alarm1 to trigger to wakeup */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__RTC_ALARM1_EN__SHIFT       6
#define PWRC_TRIGGER_EN_SET__RTC_ALARM1_EN__WIDTH       1
#define PWRC_TRIGGER_EN_SET__RTC_ALARM1_EN__MASK        0x00000040
#define PWRC_TRIGGER_EN_SET__RTC_ALARM1_EN__INV_MASK    0xFFFFFFBF
#define PWRC_TRIGGER_EN_SET__RTC_ALARM1_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.GPIO_WAKEUP_EN - Wake-up enable from power saving mode by X_GPIO[3:0]. GPIO_ WAKEUP_EN[0] is corresponding to the X_GPIO[0], GPIO_ WAKEUP_EN[1] is corresponding to the X_GPIO[1], and so on */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_EN__SHIFT       8
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_EN__WIDTH       4
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_EN__MASK        0x00000F00
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_EN__INV_MASK    0xFFFFF0FF
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.GPIO_WAKEUP_MODE1 - It is used to control X_GPIO[3:0] wake-up mode */
/* 0 : Wake up event is one event according to the setting of GPIO_WAKEUP_MODE2 and GPIO_WAKEUP_LEVEL */
/* 1 : Wake up event is two events: either positive edge or negative edges */
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE1__SHIFT       12
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE1__WIDTH       4
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE1__MASK        0x0000F000
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE1__INV_MASK    0xFFFF0FFF
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE1__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.GPIO_WAKEUP_MODE2 - It is used to control X_GPIO[3:0] wake-up mode */
/* 0 : Wake up event is one of edge triggered */
/* 1 : Wake up event is one of level triggered */
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE2__SHIFT       16
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE2__WIDTH       4
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE2__MASK        0x000F0000
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE2__INV_MASK    0xFFF0FFFF
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_MODE2__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_SET.GPIO_WAKEUP_LEVEL - When the GPIO_WAKEUP_MODE2 is level trigger */
/* 0 : Low active, or Negative edge if the GPIO_WAKEUP_MODE2 is edge trigger */
/* 1 : High active or Positive edge if the GPIO_WAKEUP_MODE2 is edge trigger */
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_LEVEL__SHIFT       20
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_LEVEL__WIDTH       4
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_LEVEL__MASK        0x00F00000
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_LEVEL__INV_MASK    0xFF0FFFFF
#define PWRC_TRIGGER_EN_SET__GPIO_WAKEUP_LEVEL__HW_DEFAULT  0x0

/* PWRC Trigger Enable CLR Register */
/* For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input) */
#define PWRC_TRIGGER_EN_CLR       0x18843010

/* PWRC_TRIGGER_EN_CLR.ON_KEY_EN - Enable X_ON_KEY_B to trigger to wakeup from power saving or cold boot mode or generate interrupt in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__ON_KEY_EN__SHIFT       0
#define PWRC_TRIGGER_EN_CLR__ON_KEY_EN__WIDTH       1
#define PWRC_TRIGGER_EN_CLR__ON_KEY_EN__MASK        0x00000001
#define PWRC_TRIGGER_EN_CLR__ON_KEY_EN__INV_MASK    0xFFFFFFFE
#define PWRC_TRIGGER_EN_CLR__ON_KEY_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_CLR.EXT_ON_EN - X_EXT_ON to trigger to wakeup from power saving or cold boot mode or generate interrupt in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__EXT_ON_EN__SHIFT       1
#define PWRC_TRIGGER_EN_CLR__EXT_ON_EN__WIDTH       1
#define PWRC_TRIGGER_EN_CLR__EXT_ON_EN__MASK        0x00000002
#define PWRC_TRIGGER_EN_CLR__EXT_ON_EN__INV_MASK    0xFFFFFFFD
#define PWRC_TRIGGER_EN_CLR__EXT_ON_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_CLR.LOW_BAT_EN - Enable X_LOWBATT_B to trigger to both generate interrupt and set the interrupt status in normal mode */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__LOW_BAT_EN__SHIFT       2
#define PWRC_TRIGGER_EN_CLR__LOW_BAT_EN__WIDTH       1
#define PWRC_TRIGGER_EN_CLR__LOW_BAT_EN__MASK        0x00000004
#define PWRC_TRIGGER_EN_CLR__LOW_BAT_EN__INV_MASK    0xFFFFFFFB
#define PWRC_TRIGGER_EN_CLR__LOW_BAT_EN__HW_DEFAULT  0x1

/* PWRC_TRIGGER_EN_CLR.MULT_BUTTON_EN - Enable MULT_BUTTON1 and MULT_BUTTON2 to trigger interrupt */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__MULT_BUTTON_EN__SHIFT       3
#define PWRC_TRIGGER_EN_CLR__MULT_BUTTON_EN__WIDTH       2
#define PWRC_TRIGGER_EN_CLR__MULT_BUTTON_EN__MASK        0x00000018
#define PWRC_TRIGGER_EN_CLR__MULT_BUTTON_EN__INV_MASK    0xFFFFFFE7
#define PWRC_TRIGGER_EN_CLR__MULT_BUTTON_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.RTC_ALARM0_EN - Enable RTC alarm0 to trigger to wakeup */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM0_EN__SHIFT       5
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM0_EN__WIDTH       1
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM0_EN__MASK        0x00000020
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM0_EN__INV_MASK    0xFFFFFFDF
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM0_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.RTC_ALARM1_EN - Enable RTC alarm1 to trigger to wakeup */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM1_EN__SHIFT       6
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM1_EN__WIDTH       1
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM1_EN__MASK        0x00000040
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM1_EN__INV_MASK    0xFFFFFFBF
#define PWRC_TRIGGER_EN_CLR__RTC_ALARM1_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.GPIO_WAKEUP_EN - Wake-up enable from power saving mode by X_GPIO[3:0]. GPIO_ WAKEUP_EN[0] is corresponding to the X_GPIO[0], GPIO_ WAKEUP_EN[1] is corresponding to the X_GPIO[1], and so on */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_EN__SHIFT       8
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_EN__WIDTH       4
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_EN__MASK        0x00000F00
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_EN__INV_MASK    0xFFFFF0FF
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_EN__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.GPIO_WAKEUP_MODE1 - It is used to control X_GPIO[3:0] wake-up mode */
/* 0 : Wake up event is one event according to the setting of GPIO_WAKEUP_MODE2 and GPIO_WAKEUP_LEVEL */
/* 1 : Wake up event is two events: either positive edge or negative edges */
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE1__SHIFT       12
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE1__WIDTH       4
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE1__MASK        0x0000F000
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE1__INV_MASK    0xFFFF0FFF
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE1__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.GPIO_WAKEUP_MODE2 - It is used to control X_GPIO[3:0] wake-up mode */
/* 0 : Wake up event is one of edge triggered */
/* 1 : Wake up event is one of level triggered */
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE2__SHIFT       16
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE2__WIDTH       4
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE2__MASK        0x000F0000
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE2__INV_MASK    0xFFF0FFFF
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_MODE2__HW_DEFAULT  0x0

/* PWRC_TRIGGER_EN_CLR.GPIO_WAKEUP_LEVEL - When the GPIO_WAKEUP_MODE2 is level trigger */
/* 0 : Low active, or Negative edge if the GPIO_WAKEUP_MODE2 is edge trigger */
/* 1 : High active or Positive edge if the GPIO_WAKEUP_MODE2 is edge trigger */
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_LEVEL__SHIFT       20
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_LEVEL__WIDTH       4
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_LEVEL__MASK        0x00F00000
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_LEVEL__INV_MASK    0xFF0FFFFF
#define PWRC_TRIGGER_EN_CLR__GPIO_WAKEUP_LEVEL__HW_DEFAULT  0x0

/* PWRC Interrupt Mask SET Register */
/* For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input) */
#define PWRC_INT_MASK_SET         0x18843014

/* PWRC_INT_MASK_SET.ON_KEY_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__ON_KEY_MASK__SHIFT       0
#define PWRC_INT_MASK_SET__ON_KEY_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__ON_KEY_MASK__MASK        0x00000001
#define PWRC_INT_MASK_SET__ON_KEY_MASK__INV_MASK    0xFFFFFFFE
#define PWRC_INT_MASK_SET__ON_KEY_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.EXT_ON_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__EXT_ON_MASK__SHIFT       1
#define PWRC_INT_MASK_SET__EXT_ON_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__EXT_ON_MASK__MASK        0x00000002
#define PWRC_INT_MASK_SET__EXT_ON_MASK__INV_MASK    0xFFFFFFFD
#define PWRC_INT_MASK_SET__EXT_ON_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.LOW_BAT_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__LOW_BAT_MASK__SHIFT       2
#define PWRC_INT_MASK_SET__LOW_BAT_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__LOW_BAT_MASK__MASK        0x00000004
#define PWRC_INT_MASK_SET__LOW_BAT_MASK__INV_MASK    0xFFFFFFFB
#define PWRC_INT_MASK_SET__LOW_BAT_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.MULT_BUTTON_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__MULT_BUTTON_MASK__SHIFT       3
#define PWRC_INT_MASK_SET__MULT_BUTTON_MASK__WIDTH       2
#define PWRC_INT_MASK_SET__MULT_BUTTON_MASK__MASK        0x00000018
#define PWRC_INT_MASK_SET__MULT_BUTTON_MASK__INV_MASK    0xFFFFFFE7
#define PWRC_INT_MASK_SET__MULT_BUTTON_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.GNSS_PON_REQ_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__GNSS_PON_REQ_MASK__SHIFT       5
#define PWRC_INT_MASK_SET__GNSS_PON_REQ_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__GNSS_PON_REQ_MASK__MASK        0x00000020
#define PWRC_INT_MASK_SET__GNSS_PON_REQ_MASK__INV_MASK    0xFFFFFFDF
#define PWRC_INT_MASK_SET__GNSS_PON_REQ_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.GNSS_POFF_REQ_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__GNSS_POFF_REQ_MASK__SHIFT       6
#define PWRC_INT_MASK_SET__GNSS_POFF_REQ_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__GNSS_POFF_REQ_MASK__MASK        0x00000040
#define PWRC_INT_MASK_SET__GNSS_POFF_REQ_MASK__INV_MASK    0xFFFFFFBF
#define PWRC_INT_MASK_SET__GNSS_POFF_REQ_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.GNSS_PON_ACK_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__GNSS_PON_ACK_MASK__SHIFT       7
#define PWRC_INT_MASK_SET__GNSS_PON_ACK_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__GNSS_PON_ACK_MASK__MASK        0x00000080
#define PWRC_INT_MASK_SET__GNSS_PON_ACK_MASK__INV_MASK    0xFFFFFF7F
#define PWRC_INT_MASK_SET__GNSS_PON_ACK_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_SET.GNSS_POFF_ACK_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_SET__GNSS_POFF_ACK_MASK__SHIFT       8
#define PWRC_INT_MASK_SET__GNSS_POFF_ACK_MASK__WIDTH       1
#define PWRC_INT_MASK_SET__GNSS_POFF_ACK_MASK__MASK        0x00000100
#define PWRC_INT_MASK_SET__GNSS_POFF_ACK_MASK__INV_MASK    0xFFFFFEFF
#define PWRC_INT_MASK_SET__GNSS_POFF_ACK_MASK__HW_DEFAULT  0x0

/* PWRC Interrupt Mask CLR Register */
/* For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input) */
#define PWRC_INT_MASK_CLR         0x18843018

/* PWRC_INT_MASK_CLR.ON_KEY_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__ON_KEY_MASK__SHIFT       0
#define PWRC_INT_MASK_CLR__ON_KEY_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__ON_KEY_MASK__MASK        0x00000001
#define PWRC_INT_MASK_CLR__ON_KEY_MASK__INV_MASK    0xFFFFFFFE
#define PWRC_INT_MASK_CLR__ON_KEY_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.EXT_ON_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__EXT_ON_MASK__SHIFT       1
#define PWRC_INT_MASK_CLR__EXT_ON_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__EXT_ON_MASK__MASK        0x00000002
#define PWRC_INT_MASK_CLR__EXT_ON_MASK__INV_MASK    0xFFFFFFFD
#define PWRC_INT_MASK_CLR__EXT_ON_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.LOW_BAT_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__LOW_BAT_MASK__SHIFT       2
#define PWRC_INT_MASK_CLR__LOW_BAT_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__LOW_BAT_MASK__MASK        0x00000004
#define PWRC_INT_MASK_CLR__LOW_BAT_MASK__INV_MASK    0xFFFFFFFB
#define PWRC_INT_MASK_CLR__LOW_BAT_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.MULT_BUTTON_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__MULT_BUTTON_MASK__SHIFT       3
#define PWRC_INT_MASK_CLR__MULT_BUTTON_MASK__WIDTH       2
#define PWRC_INT_MASK_CLR__MULT_BUTTON_MASK__MASK        0x00000018
#define PWRC_INT_MASK_CLR__MULT_BUTTON_MASK__INV_MASK    0xFFFFFFE7
#define PWRC_INT_MASK_CLR__MULT_BUTTON_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.GNSS_PON_REQ_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__GNSS_PON_REQ_MASK__SHIFT       5
#define PWRC_INT_MASK_CLR__GNSS_PON_REQ_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__GNSS_PON_REQ_MASK__MASK        0x00000020
#define PWRC_INT_MASK_CLR__GNSS_PON_REQ_MASK__INV_MASK    0xFFFFFFDF
#define PWRC_INT_MASK_CLR__GNSS_PON_REQ_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.GNSS_POFF_REQ_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__GNSS_POFF_REQ_MASK__SHIFT       6
#define PWRC_INT_MASK_CLR__GNSS_POFF_REQ_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__GNSS_POFF_REQ_MASK__MASK        0x00000040
#define PWRC_INT_MASK_CLR__GNSS_POFF_REQ_MASK__INV_MASK    0xFFFFFFBF
#define PWRC_INT_MASK_CLR__GNSS_POFF_REQ_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.GNSS_PON_ACK_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__GNSS_PON_ACK_MASK__SHIFT       7
#define PWRC_INT_MASK_CLR__GNSS_PON_ACK_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__GNSS_PON_ACK_MASK__MASK        0x00000080
#define PWRC_INT_MASK_CLR__GNSS_PON_ACK_MASK__INV_MASK    0xFFFFFF7F
#define PWRC_INT_MASK_CLR__GNSS_PON_ACK_MASK__HW_DEFAULT  0x0

/* PWRC_INT_MASK_CLR.GNSS_POFF_ACK_MASK - Mask the generated interrupt to the interrupt controller, while the interrupt status is not affected by this bit. */
/* 0 : Mask the interrupt */
/* 1 : Do not mask the interrupt */
#define PWRC_INT_MASK_CLR__GNSS_POFF_ACK_MASK__SHIFT       8
#define PWRC_INT_MASK_CLR__GNSS_POFF_ACK_MASK__WIDTH       1
#define PWRC_INT_MASK_CLR__GNSS_POFF_ACK_MASK__MASK        0x00000100
#define PWRC_INT_MASK_CLR__GNSS_POFF_ACK_MASK__INV_MASK    0xFFFFFEFF
#define PWRC_INT_MASK_CLR__GNSS_POFF_ACK_MASK__HW_DEFAULT  0x0

/* PWRC Interrupt Status Register */
/* Each status will be asserted regardless to the corresponding interrupt mask. 
 Each mask only affects the interrupt event, but not the status. For each field, write 0 will clear the interrupt if occured, while write 1 will be discarded. */
#define PWRC_INT_STATUS           0x1884301C

/* PWRC_INT_STATUS.ON_KEY_STATUS - ON_KEY Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__ON_KEY_STATUS__SHIFT       0
#define PWRC_INT_STATUS__ON_KEY_STATUS__WIDTH       1
#define PWRC_INT_STATUS__ON_KEY_STATUS__MASK        0x00000001
#define PWRC_INT_STATUS__ON_KEY_STATUS__INV_MASK    0xFFFFFFFE
#define PWRC_INT_STATUS__ON_KEY_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.EXT_ON_STATUS - EXT_ON Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__EXT_ON_STATUS__SHIFT       1
#define PWRC_INT_STATUS__EXT_ON_STATUS__WIDTH       1
#define PWRC_INT_STATUS__EXT_ON_STATUS__MASK        0x00000002
#define PWRC_INT_STATUS__EXT_ON_STATUS__INV_MASK    0xFFFFFFFD
#define PWRC_INT_STATUS__EXT_ON_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.LOW_BAT_STATUS - LOW_BAT Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__LOW_BAT_STATUS__SHIFT       2
#define PWRC_INT_STATUS__LOW_BAT_STATUS__WIDTH       1
#define PWRC_INT_STATUS__LOW_BAT_STATUS__MASK        0x00000004
#define PWRC_INT_STATUS__LOW_BAT_STATUS__INV_MASK    0xFFFFFFFB
#define PWRC_INT_STATUS__LOW_BAT_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.MULT_BUTTON_STATUS - MULT_BUTTON Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__MULT_BUTTON_STATUS__SHIFT       3
#define PWRC_INT_STATUS__MULT_BUTTON_STATUS__WIDTH       2
#define PWRC_INT_STATUS__MULT_BUTTON_STATUS__MASK        0x00000018
#define PWRC_INT_STATUS__MULT_BUTTON_STATUS__INV_MASK    0xFFFFFFE7
#define PWRC_INT_STATUS__MULT_BUTTON_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.GNSS_PON_REQ_STATUS - GNSS_PON_REQ Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__GNSS_PON_REQ_STATUS__SHIFT       5
#define PWRC_INT_STATUS__GNSS_PON_REQ_STATUS__WIDTH       1
#define PWRC_INT_STATUS__GNSS_PON_REQ_STATUS__MASK        0x00000020
#define PWRC_INT_STATUS__GNSS_PON_REQ_STATUS__INV_MASK    0xFFFFFFDF
#define PWRC_INT_STATUS__GNSS_PON_REQ_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.GNSS_POFF_REQ_STATUS - GNSS_POFF_REQ_STATUS Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__GNSS_POFF_REQ_STATUS__SHIFT       6
#define PWRC_INT_STATUS__GNSS_POFF_REQ_STATUS__WIDTH       1
#define PWRC_INT_STATUS__GNSS_POFF_REQ_STATUS__MASK        0x00000040
#define PWRC_INT_STATUS__GNSS_POFF_REQ_STATUS__INV_MASK    0xFFFFFFBF
#define PWRC_INT_STATUS__GNSS_POFF_REQ_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.GNSS_PON_ACK_STATUS - GNSS_PON_ACK Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__GNSS_PON_ACK_STATUS__SHIFT       7
#define PWRC_INT_STATUS__GNSS_PON_ACK_STATUS__WIDTH       1
#define PWRC_INT_STATUS__GNSS_PON_ACK_STATUS__MASK        0x00000080
#define PWRC_INT_STATUS__GNSS_PON_ACK_STATUS__INV_MASK    0xFFFFFF7F
#define PWRC_INT_STATUS__GNSS_PON_ACK_STATUS__HW_DEFAULT  0x0

/* PWRC_INT_STATUS.GNSS_POFF_ACK_STATUS - GNSS_POFF_ACK Interrupt status */
/* 0 : Interrupt did not happen (R) or interrupt clearance (W) */
/* 1 : Interrupt happened */
#define PWRC_INT_STATUS__GNSS_POFF_ACK_STATUS__SHIFT       8
#define PWRC_INT_STATUS__GNSS_POFF_ACK_STATUS__WIDTH       1
#define PWRC_INT_STATUS__GNSS_POFF_ACK_STATUS__MASK        0x00000100
#define PWRC_INT_STATUS__GNSS_POFF_ACK_STATUS__INV_MASK    0xFFFFFEFF
#define PWRC_INT_STATUS__GNSS_POFF_ACK_STATUS__HW_DEFAULT  0x0

/* PWRC Pin Status Register */
/* This register reflects several PWRC inputs, and might be useful for debug. */
#define PWRC_PIN_STATUS           0x18843020

/* PWRC_PIN_STATUS.ON_KEY_B_PIN - directly read X_ON_KEY_B pin status (active low) */
#define PWRC_PIN_STATUS__ON_KEY_B_PIN__SHIFT       0
#define PWRC_PIN_STATUS__ON_KEY_B_PIN__WIDTH       1
#define PWRC_PIN_STATUS__ON_KEY_B_PIN__MASK        0x00000001
#define PWRC_PIN_STATUS__ON_KEY_B_PIN__INV_MASK    0xFFFFFFFE
#define PWRC_PIN_STATUS__ON_KEY_B_PIN__HW_DEFAULT  0x1

/* PWRC_PIN_STATUS.EXT_ON_PIN - directly read X_EXT_ON pin status */
#define PWRC_PIN_STATUS__EXT_ON_PIN__SHIFT       1
#define PWRC_PIN_STATUS__EXT_ON_PIN__WIDTH       1
#define PWRC_PIN_STATUS__EXT_ON_PIN__MASK        0x00000002
#define PWRC_PIN_STATUS__EXT_ON_PIN__INV_MASK    0xFFFFFFFD
#define PWRC_PIN_STATUS__EXT_ON_PIN__HW_DEFAULT  0x0

/* PWRC_PIN_STATUS.LOW_BAT_B_PIN - directly read X_LOWBATT_B pin status (active low) */
#define PWRC_PIN_STATUS__LOW_BAT_B_PIN__SHIFT       2
#define PWRC_PIN_STATUS__LOW_BAT_B_PIN__WIDTH       1
#define PWRC_PIN_STATUS__LOW_BAT_B_PIN__MASK        0x00000004
#define PWRC_PIN_STATUS__LOW_BAT_B_PIN__INV_MASK    0xFFFFFFFB
#define PWRC_PIN_STATUS__LOW_BAT_B_PIN__HW_DEFAULT  0x1

/* PWRC_PIN_STATUS.MULT_BUTTON1 - directly read MULT_BUTTON1 pin status */
#define PWRC_PIN_STATUS__MULT_BUTTON1__SHIFT       3
#define PWRC_PIN_STATUS__MULT_BUTTON1__WIDTH       1
#define PWRC_PIN_STATUS__MULT_BUTTON1__MASK        0x00000008
#define PWRC_PIN_STATUS__MULT_BUTTON1__INV_MASK    0xFFFFFFF7
#define PWRC_PIN_STATUS__MULT_BUTTON1__HW_DEFAULT  0x0

/* PWRC_PIN_STATUS.MULT_BUTTON2 - directly read MULT_BUTTON2 pin status */
#define PWRC_PIN_STATUS__MULT_BUTTON2__SHIFT       4
#define PWRC_PIN_STATUS__MULT_BUTTON2__WIDTH       1
#define PWRC_PIN_STATUS__MULT_BUTTON2__MASK        0x00000010
#define PWRC_PIN_STATUS__MULT_BUTTON2__INV_MASK    0xFFFFFFEF
#define PWRC_PIN_STATUS__MULT_BUTTON2__HW_DEFAULT  0x0

/* PWRC_PIN_STATUS.DRAM_HOLD_SET_REQ - directly from RSTC */
#define PWRC_PIN_STATUS__DRAM_HOLD_SET_REQ__SHIFT       5
#define PWRC_PIN_STATUS__DRAM_HOLD_SET_REQ__WIDTH       1
#define PWRC_PIN_STATUS__DRAM_HOLD_SET_REQ__MASK        0x00000020
#define PWRC_PIN_STATUS__DRAM_HOLD_SET_REQ__INV_MASK    0xFFFFFFDF
#define PWRC_PIN_STATUS__DRAM_HOLD_SET_REQ__HW_DEFAULT  0x0

/* PWRC_PIN_STATUS.NOC_RECONNECT_REQ - directly from RSTC */
#define PWRC_PIN_STATUS__NOC_RECONNECT_REQ__SHIFT       6
#define PWRC_PIN_STATUS__NOC_RECONNECT_REQ__WIDTH       1
#define PWRC_PIN_STATUS__NOC_RECONNECT_REQ__MASK        0x00000040
#define PWRC_PIN_STATUS__NOC_RECONNECT_REQ__INV_MASK    0xFFFFFFBF
#define PWRC_PIN_STATUS__NOC_RECONNECT_REQ__HW_DEFAULT  0x0

/* PWRC SPARE Controlling Register */
/* PWRC Misc. */
#define PWRC_SPARE                0x18843024

/* PWRC_SPARE.RTCLDO_TMUXCTRL - RTC LDO test multiplex controlling */
#define PWRC_SPARE__RTCLDO_TMUXCTRL__SHIFT       1
#define PWRC_SPARE__RTCLDO_TMUXCTRL__WIDTH       4
#define PWRC_SPARE__RTCLDO_TMUXCTRL__MASK        0x0000001E
#define PWRC_SPARE__RTCLDO_TMUXCTRL__INV_MASK    0xFFFFFFE1
#define PWRC_SPARE__RTCLDO_TMUXCTRL__HW_DEFAULT  0x0

/* PWRC Control over RTC-PLL Register */
/* PLL is ready and locked at VCO=160MHz by default, when CPUs (M3/A7) are active. 
 By default, FAST clock is 160MHz (div=1) and SLOW clock is 40MHz (div=4). 
 F_VCO [KHz] = FREF * FBDIV = 32.768KHz * FBDIV , F_OUT = F_VCO / (POSTDIV+1) */
#define PWRC_RTC_PLL_CTRL         0x18843028

/* PWRC_RTC_PLL_CTRL.BYPASS - FREF internal bypass, i.e. bypass is done within the RTCPLL. */
/* 0 : Bypass disabled */
/* 1 : Bypass enabled, i.e. FREF is bypassed to FOUT1 and FOUT2 */
#define PWRC_RTC_PLL_CTRL__BYPASS__SHIFT       0
#define PWRC_RTC_PLL_CTRL__BYPASS__WIDTH       1
#define PWRC_RTC_PLL_CTRL__BYPASS__MASK        0x00000001
#define PWRC_RTC_PLL_CTRL__BYPASS__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_PLL_CTRL__BYPASS__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.FBDIV - PLL Feedback division value (256 to 12800). 
 Actual maximum is 16383, but this value is outside of the useful functional range of the VCO and PFD */
#define PWRC_RTC_PLL_CTRL__FBDIV__SHIFT       1
#define PWRC_RTC_PLL_CTRL__FBDIV__WIDTH       14
#define PWRC_RTC_PLL_CTRL__FBDIV__MASK        0x00007FFE
#define PWRC_RTC_PLL_CTRL__FBDIV__INV_MASK    0xFFFF8001
#define PWRC_RTC_PLL_CTRL__FBDIV__HW_DEFAULT  0x1313

/* PWRC_RTC_PLL_CTRL.FOUTPOSTDIV1PD - First post divide power down */
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV1PD__SHIFT       15
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV1PD__WIDTH       1
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV1PD__MASK        0x00008000
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV1PD__INV_MASK    0xFFFF7FFF
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV1PD__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.FOUTPOSTDIV2PD - Second post divide power down */
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV2PD__SHIFT       16
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV2PD__WIDTH       1
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV2PD__MASK        0x00010000
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV2PD__INV_MASK    0xFFFEFFFF
#define PWRC_RTC_PLL_CTRL__FOUTPOSTDIV2PD__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.FOUTVCOPD - VCO rate output clock power down */
#define PWRC_RTC_PLL_CTRL__FOUTVCOPD__SHIFT       17
#define PWRC_RTC_PLL_CTRL__FOUTVCOPD__WIDTH       1
#define PWRC_RTC_PLL_CTRL__FOUTVCOPD__MASK        0x00020000
#define PWRC_RTC_PLL_CTRL__FOUTVCOPD__INV_MASK    0xFFFDFFFF
#define PWRC_RTC_PLL_CTRL__FOUTVCOPD__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.PD - Power down for PLL */
#define PWRC_RTC_PLL_CTRL__PD__SHIFT       18
#define PWRC_RTC_PLL_CTRL__PD__WIDTH       1
#define PWRC_RTC_PLL_CTRL__PD__MASK        0x00040000
#define PWRC_RTC_PLL_CTRL__PD__INV_MASK    0xFFFBFFFF
#define PWRC_RTC_PLL_CTRL__PD__HW_DEFAULT  0x1

/* PWRC_RTC_PLL_CTRL.POSTDIV1 - First post divide value (for FOUT1), 0 to 15 */
#define PWRC_RTC_PLL_CTRL__POSTDIV1__SHIFT       19
#define PWRC_RTC_PLL_CTRL__POSTDIV1__WIDTH       4
#define PWRC_RTC_PLL_CTRL__POSTDIV1__MASK        0x00780000
#define PWRC_RTC_PLL_CTRL__POSTDIV1__INV_MASK    0xFF87FFFF
#define PWRC_RTC_PLL_CTRL__POSTDIV1__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.POSTDIV2 - Second post divide value (for FOUT2), 0 to 15 */
#define PWRC_RTC_PLL_CTRL__POSTDIV2__SHIFT       23
#define PWRC_RTC_PLL_CTRL__POSTDIV2__WIDTH       4
#define PWRC_RTC_PLL_CTRL__POSTDIV2__MASK        0x07800000
#define PWRC_RTC_PLL_CTRL__POSTDIV2__INV_MASK    0xF87FFFFF
#define PWRC_RTC_PLL_CTRL__POSTDIV2__HW_DEFAULT  0x3

/* PWRC_RTC_PLL_CTRL.LOCK - Lock signal, indicates no cycle slips between the feedback clock and FPFD for 96 consecutive cycles. 
 SW may write to this bit in order to generate a pseudo lock, when PLL is bypassed. */
#define PWRC_RTC_PLL_CTRL__LOCK__SHIFT       27
#define PWRC_RTC_PLL_CTRL__LOCK__WIDTH       1
#define PWRC_RTC_PLL_CTRL__LOCK__MASK        0x08000000
#define PWRC_RTC_PLL_CTRL__LOCK__INV_MASK    0xF7FFFFFF
#define PWRC_RTC_PLL_CTRL__LOCK__HW_DEFAULT  0x0

/* PWRC_RTC_PLL_CTRL.RL_BYPASS - Bypass RTCPLL due to reset-latch (bypass is done external to the RTCPLL) */
/* 0 : RTCPLL bypass is determined according to PWRC_BCRL_VAL.PLL_BYPASS state. */
/* 1 : RTCPLL is bypassed, regardless to reset-latch value. */
/* 2 : RTCPLL is not bypassed, regardless to reset-latch value. */
#define PWRC_RTC_PLL_CTRL__RL_BYPASS__SHIFT       30
#define PWRC_RTC_PLL_CTRL__RL_BYPASS__WIDTH       2
#define PWRC_RTC_PLL_CTRL__RL_BYPASS__MASK        0xC0000000
#define PWRC_RTC_PLL_CTRL__RL_BYPASS__INV_MASK    0x3FFFFFFF
#define PWRC_RTC_PLL_CTRL__RL_BYPASS__HW_DEFAULT  0x0

/* PWRC XINW FMODE Controlling Register */
/* This register controls the Fast-Mode logic which affects the XINW crystal */
#define PWRC_XINW_FMODE_CTRL      0x1884302C

/* PWRC_XINW_FMODE_CTRL.XINW_FMODE - XINW fast mode controlling */
/* 0 : Automatic mode. FAST_MODE will be dropped-down to 0 when the new counter reaches its max value (0x7FFF --> ~1sec). */
/* 1 : Manual mode. FAST_MODE will be dropped-down to 0 when SW manually writes 1 to XINW_FMODE_CLR. */
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE__SHIFT       0
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE__WIDTH       1
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE__MASK        0x00000001
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE__INV_MASK    0xFFFFFFFE
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE__HW_DEFAULT  0x0

/* PWRC_XINW_FMODE_CTRL.XINW_FMODE_CLR - XINW fast mode clear, only relevant when XINW_FMODE is set to 1. Auto-cleared. */
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_CLR__SHIFT       1
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_CLR__WIDTH       1
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_CLR__MASK        0x00000002
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_CLR__INV_MASK    0xFFFFFFFD
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_CLR__HW_DEFAULT  0x0

/* PWRC_XINW_FMODE_CTRL.XINW_FMODE_VAL - Visibility of FMODE output from PWRC to XINW */
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_VAL__SHIFT       31
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_VAL__WIDTH       1
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_VAL__MASK        0x80000000
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_VAL__INV_MASK    0x7FFFFFFF
#define PWRC_XINW_FMODE_CTRL__XINW_FMODE_VAL__HW_DEFAULT  0x0

/* PWRC DDR LDO Configuration Register */
/* This register Controls the DDR LDO configurations, during functional mode 
 The DDR LDO was added in step-B and generates DRAM_CORE voltage (1.2V) from the incoming DRAM_IO input (1.5V), in addition it also uses CORE voltage (1.1V) for control. */
#define PWRC_DDR_LDO_CONFIG       0x18843030

/* PWRC_DDR_LDO_CONFIG.ENABLE - Controls the ddr_ldo_enable pin of the LDO, conditioned as following: */
/* 0 : core_on &amp; hw_ddr_ldo_sticky_en 
 hw_ddr_ldo_sticky_en = 0 by default 
 hw_ddr_ldo_sticky_en = 0 upon !mix_reset_b or COLDBOOT entrance 
 hw_ddr_ldo_sticky_en = 1 if ddr_ldo_poc and ANALOG_ON entrance */
/* 1 : 1'b1 */
/* 2 : 1'b0 */
/* 3 : Reserved */
#define PWRC_DDR_LDO_CONFIG__ENABLE__SHIFT       0
#define PWRC_DDR_LDO_CONFIG__ENABLE__WIDTH       2
#define PWRC_DDR_LDO_CONFIG__ENABLE__MASK        0x00000003
#define PWRC_DDR_LDO_CONFIG__ENABLE__INV_MASK    0xFFFFFFFC
#define PWRC_DDR_LDO_CONFIG__ENABLE__HW_DEFAULT  0x0

/* PWRC_DDR_LDO_CONFIG.POC_ENABLE - Controls the ddr_ldo_poc_enable pin of the LDO. */
#define PWRC_DDR_LDO_CONFIG__POC_ENABLE__SHIFT       2
#define PWRC_DDR_LDO_CONFIG__POC_ENABLE__WIDTH       1
#define PWRC_DDR_LDO_CONFIG__POC_ENABLE__MASK        0x00000004
#define PWRC_DDR_LDO_CONFIG__POC_ENABLE__INV_MASK    0xFFFFFFFB
#define PWRC_DDR_LDO_CONFIG__POC_ENABLE__HW_DEFAULT  0x1

/* PWRC_DDR_LDO_CONFIG.SNS_SEL - Controls the ddr_ldo_sns_sel pin of the LDO. */
#define PWRC_DDR_LDO_CONFIG__SNS_SEL__SHIFT       3
#define PWRC_DDR_LDO_CONFIG__SNS_SEL__WIDTH       1
#define PWRC_DDR_LDO_CONFIG__SNS_SEL__MASK        0x00000008
#define PWRC_DDR_LDO_CONFIG__SNS_SEL__INV_MASK    0xFFFFFFF7
#define PWRC_DDR_LDO_CONFIG__SNS_SEL__HW_DEFAULT  0x0

/* PWRC_DDR_LDO_CONFIG.TUNE_1P2 - Controls the ddr_ldo_tune_1p2 pin of the LDO (1.2V). */
#define PWRC_DDR_LDO_CONFIG__TUNE_1P2__SHIFT       4
#define PWRC_DDR_LDO_CONFIG__TUNE_1P2__WIDTH       3
#define PWRC_DDR_LDO_CONFIG__TUNE_1P2__MASK        0x00000070
#define PWRC_DDR_LDO_CONFIG__TUNE_1P2__INV_MASK    0xFFFFFF8F
#define PWRC_DDR_LDO_CONFIG__TUNE_1P2__HW_DEFAULT  0x7

/* PWRC_DDR_LDO_CONFIG.IBIAS - Controls the ddr_ldo_tst_bit pin of the LDO. */
/* 0x0 : Ibias is 1.005uA */
/* 0x1 : Ibias is 1.089uA */
/* 0x2 : Ibias is 1.188uA */
/* 0x3 : Ibias is 1.307uA */
/* 0x4 : Ibias is 768.8nA */
/* 0x5 : Ibias is 816.8nA */
/* 0x6 : Ibias is 871.3nA */
/* 0x7 : Ibias is 933.5nA */
#define PWRC_DDR_LDO_CONFIG__IBIAS__SHIFT       7
#define PWRC_DDR_LDO_CONFIG__IBIAS__WIDTH       3
#define PWRC_DDR_LDO_CONFIG__IBIAS__MASK        0x00000380
#define PWRC_DDR_LDO_CONFIG__IBIAS__INV_MASK    0xFFFFFC7F
#define PWRC_DDR_LDO_CONFIG__IBIAS__HW_DEFAULT  0x0

/* PWRC_DDR_LDO_CONFIG.POC_IGNORE - Controls the isolation logic between DDR and CORE (pwrc_ddr2core_iso_valid signal driving logic). */
/* 0 : rtccore_iso_en &amp; ddr_ldo_poc */
/* 1 : rtccore_iso_en (legacy) */
/* 2 : 1'b0 */
/* 3 : 1'b1 */
#define PWRC_DDR_LDO_CONFIG__POC_IGNORE__SHIFT       10
#define PWRC_DDR_LDO_CONFIG__POC_IGNORE__WIDTH       2
#define PWRC_DDR_LDO_CONFIG__POC_IGNORE__MASK        0x00000C00
#define PWRC_DDR_LDO_CONFIG__POC_IGNORE__INV_MASK    0xFFFFF3FF
#define PWRC_DDR_LDO_CONFIG__POC_IGNORE__HW_DEFAULT  0x0

/* PWRC_DDR_LDO_CONFIG.BGTRIM - Controls the ddr_ldo_trim pin of the LDO. */
/* 0x0 : VBG is 1.205V */
/* 0x1 : VBG is 1.211V */
/* 0x2 : VBG is 1.218V */
/* 0x3 : VBG is 1.224V */
/* 0x4 : VBG is 1.229V */
/* 0x5 : VBG is 1.234V */
/* 0x6 : VBG is 1.239V */
/* 0x7 : VBG is 1.244V */
/* 0x8 : VBG is 1.135V */
/* 0x9 : VBG is 1.146V */
/* 0xA : VBG is 1.156V */
/* 0xB : VBG is 1.166V */
/* 0xC : VBG is 1.175V */
/* 0xD : VBG is 1.183V */
/* 0xE : VBG is 1.191V */
/* 0xF : VBG is 1.198V */
#define PWRC_DDR_LDO_CONFIG__BGTRIM__SHIFT       12
#define PWRC_DDR_LDO_CONFIG__BGTRIM__WIDTH       4
#define PWRC_DDR_LDO_CONFIG__BGTRIM__MASK        0x0000F000
#define PWRC_DDR_LDO_CONFIG__BGTRIM__INV_MASK    0xFFFF0FFF
#define PWRC_DDR_LDO_CONFIG__BGTRIM__HW_DEFAULT  0xF

/* PWRC_DDR_LDO_CONFIG.POC_STICKY - Monitors the hw_ddr_ldo_sticky_en signal, which affect the ENABLE field. */
#define PWRC_DDR_LDO_CONFIG__POC_STICKY__SHIFT       30
#define PWRC_DDR_LDO_CONFIG__POC_STICKY__WIDTH       1
#define PWRC_DDR_LDO_CONFIG__POC_STICKY__MASK        0x40000000
#define PWRC_DDR_LDO_CONFIG__POC_STICKY__INV_MASK    0xBFFFFFFF
#define PWRC_DDR_LDO_CONFIG__POC_STICKY__HW_DEFAULT  0x1

/* PWRC_DDR_LDO_CONFIG.POC_STATUS - Monitors the ddr_ldo_poc pin of the LDO, which should assert high if both input voltages are sensed (CORE and DRAM_IO). 
 If POC_STATUS is high, it means DDR LDO is not required since DRAM CORE voltage is already being supplied from external resource/board. */
#define PWRC_DDR_LDO_CONFIG__POC_STATUS__SHIFT       31
#define PWRC_DDR_LDO_CONFIG__POC_STATUS__WIDTH       1
#define PWRC_DDR_LDO_CONFIG__POC_STATUS__MASK        0x80000000
#define PWRC_DDR_LDO_CONFIG__POC_STATUS__INV_MASK    0x7FFFFFFF
#define PWRC_DDR_LDO_CONFIG__POC_STATUS__HW_DEFAULT  0x0

/* PWRC GPIO3 Debug Controlling Register */
/* This register allows debug visibility, and requires that IO must be configured and JTAG must be disabled (reset-latch). */
#define PWRC_GPIO3_DEBUG          0x18843034

/* PWRC_GPIO3_DEBUG.PWRC_GPIO3_DEBUG_SEL - X_GPIO[3] output, used for debug */
/* 0 : disable output */
/* 1 : XINW 32.668KHz clock */
/* 2 : RTC_PLL Fast-Clock */
/* 3 : RTC_PLL Slow-Clock */
/* 4 : PSAVING_MODE indication */
/* 5 : CAN_ACTIVE indication (current state) */
/* 6 : RTC_PLL lock */
/* 7 : CAN0 Memory - Light-Sleep (LS) */
/* 8 : CAN0 Memory - Deep-Sleep (DS) */
/* 9 : CAN0 Memory - Shut-Down (SD) */
/* 10 : CAN0 Listener interrupt (can_pre_wake_up) */
/* 11 : CAN1 Listener interrupt (can_pre_wake_up) */
/* 12 : CAN External transceiver interrupt */
/* 13 : CAN timeout (CAN_ACTIVE-->PSAVING_MODE) */
/* 14 : CAN0 Controller interrupt (can_wake_up) */
/* 15 : CAN0 reset */
/* 16 : Boot-Lock indication */
/* 17 : SYSRTC Alarm0 wake-up event */
/* 18 : SYSRTC Alarm1 wake-up event */
/* 19 : ON_KEY_B wake-up event */
/* 20 : EXT_ON wake-up event */
/* 21 : GPIO wake-up event (ORed) */
/* 22 : GNSS Request wake-up event */
/* 23 : CAN0_RX_TRNSV0 (internal usage in PWRC) */
/* 24 : CAN1_RX (internal usage in PWRC) */
/* 25 : CAN0_RX_TRNSV1 (internal usage in PWRC) */
/* 26 : GPIO wake-up event bit 0 */
/* 27 : GPIO wake-up event bit 1 */
/* 28 : GPIO wake-up event bit 2 */
/* 29 : GPIO wake-up event bit 3 */
#define PWRC_GPIO3_DEBUG__PWRC_GPIO3_DEBUG_SEL__SHIFT       0
#define PWRC_GPIO3_DEBUG__PWRC_GPIO3_DEBUG_SEL__WIDTH       5
#define PWRC_GPIO3_DEBUG__PWRC_GPIO3_DEBUG_SEL__MASK        0x0000001F
#define PWRC_GPIO3_DEBUG__PWRC_GPIO3_DEBUG_SEL__INV_MASK    0xFFFFFFE0
#define PWRC_GPIO3_DEBUG__PWRC_GPIO3_DEBUG_SEL__HW_DEFAULT  0x0

/* PWRC Control over RTC NoC PwrCtl SET Register */
/* For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input). </br> RTCM contains 3 NoC networks/partitions - Data network (target --> DRAMT), Parameters network (target --> REGT) and Parameters network (initiator --> REGI). */
#define PWRC_RTC_NOC_PWRCTL_SET   0x18843038

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGI_IDLE_REQ - IDLE request for NoC REGI target */
/* Should be used before disabling NoC REGI clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_REQ__SHIFT       0
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_REQ__MASK        0x00000001
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_REQ__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGI_IDLE_ACK - IDLE acknowledge for NoC REGI target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_ACK__SHIFT       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_ACK__MASK        0x00000002
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_ACK__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGI_IDLE - IDLE state for NoC REGI target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE__SHIFT       2
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE__MASK        0x00000004
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE__INV_MASK    0xFFFFFFFB
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGI_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGT_IDLE_REQ - IDLE request for NoC REGT target. */
/* Should be used before disabling NoC REGT clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_REQ__SHIFT       3
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_REQ__MASK        0x00000008
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_REQ__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGT_IDLE_ACK - IDLE acknowledge for NoC REGT target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_ACK__SHIFT       4
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_ACK__MASK        0x00000010
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_ACK__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_REGT_IDLE - IDLE state for NoC REGT target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE__SHIFT       5
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE__MASK        0x00000020
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_REGT_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_DRAMT_IDLE_REQ - IDLE request for NoC DRAMT target */
/* Should be used before disabling NoC DRAMT clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_REQ__SHIFT       6
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_REQ__MASK        0x00000040
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_REQ__INV_MASK    0xFFFFFFBF
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_DRAMT_IDLE_ACK - IDLE acknowledge for NoC DRAMT target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_ACK__SHIFT       7
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_ACK__MASK        0x00000080
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_ACK__INV_MASK    0xFFFFFF7F
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.NOC_DRAMT_IDLE - IDLE state for NoC DRAMT target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE__SHIFT       8
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE__MASK        0x00000100
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_NOC_PWRCTL_SET__NOC_DRAMT_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_IDLE_REQ - IDLE request for Security taget */
/* Should be used before disabling Security clock. </br> Setting to low (0) aborts any Idle request in progress. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_REQ__SHIFT       9
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_REQ__MASK        0x00000200
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_REQ__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_IDLE_ACK - IDLE acknowledge for Security target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_ACK__SHIFT       10
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_ACK__MASK        0x00000400
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_ACK__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_IDLE - IDLE state for Security target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE__SHIFT       11
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE__MASK        0x00000800
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE__INV_MASK    0xFFFFF7FF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_IDLE_REQ - IDLE request for SPI0 taget */
/* Should be used before disabling SPI0 clock. </br> Setting to low (0) aborts any Idle request in progress. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_REQ__SHIFT       12
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_REQ__MASK        0x00001000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_REQ__INV_MASK    0xFFFFEFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_IDLE_ACK - IDLE acknowledge for SPI0 target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_ACK__SHIFT       13
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_ACK__MASK        0x00002000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_ACK__INV_MASK    0xFFFFDFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_IDLE - IDLE state for SPI0 target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE__SHIFT       14
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE__MASK        0x00004000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE__INV_MASK    0xFFFFBFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.CAN0_SLV_RDY - SLV_RDY request for CAN0 target */
/* Should be used before disabling CAN0 clock. */
/* 0 : Disable slave */
/* 1 : Enable slave */
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SLV_RDY__SHIFT       15
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SLV_RDY__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SLV_RDY__MASK        0x00008000
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SLV_RDY__INV_MASK    0xFFFF7FFF
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SLV_RDY__HW_DEFAULT  0x1

/* PWRC_RTC_NOC_PWRCTL_SET.CAN0_SCKT_CON - SCKT_CON acknowledge for CAN0 target. */
/* 0 : Socket disconnected (clock may be disabled) */
/* 1 : Socket connected */
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SCKT_CON__SHIFT       16
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SCKT_CON__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SCKT_CON__MASK        0x00010000
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SCKT_CON__INV_MASK    0xFFFEFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__CAN0_SCKT_CON__HW_DEFAULT  0x1

/* PWRC_RTC_NOC_PWRCTL_SET.ARMM3_NFU_ENABLE - Enable ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_ENABLE__SHIFT       17
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_ENABLE__MASK        0x00020000
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_ENABLE__INV_MASK    0xFFFDFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.ARMM3_NFU_FLUSH_REQ - Flush request for ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_REQ__SHIFT       18
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_REQ__MASK        0x00040000
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_REQ__INV_MASK    0xFFFBFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.ARMM3_NFU_FLUSH_DONE - Flush-done indication from ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_DONE__SHIFT       19
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_DONE__MASK        0x00080000
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_DONE__INV_MASK    0xFFF7FFFF
#define PWRC_RTC_NOC_PWRCTL_SET__ARMM3_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_NFU_ENABLE - Enable SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_ENABLE__SHIFT       20
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_ENABLE__MASK        0x00100000
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_ENABLE__INV_MASK    0xFFEFFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_NFU_FLUSH_REQ - Flush request for SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_REQ__SHIFT       21
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_REQ__MASK        0x00200000
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_REQ__INV_MASK    0xFFDFFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SECURITY_NFU_FLUSH_DONE - Flush-done indication from SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_DONE__SHIFT       22
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_DONE__MASK        0x00400000
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_DONE__INV_MASK    0xFFBFFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SECURITY_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_NFU_ENABLE - Enable SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_ENABLE__SHIFT       23
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_ENABLE__MASK        0x00800000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_ENABLE__INV_MASK    0xFF7FFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_NFU_FLUSH_REQ - Flush request for SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_REQ__SHIFT       24
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_REQ__MASK        0x01000000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_REQ__INV_MASK    0xFEFFFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_SET.SPI0_NFU_FLUSH_DONE - Flush-done indication from SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_DONE__SHIFT       25
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_DONE__MASK        0x02000000
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_DONE__INV_MASK    0xFDFFFFFF
#define PWRC_RTC_NOC_PWRCTL_SET__SPI0_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC Control over RTC NoC PwrCtl SET Register */
/* For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input).</br> RTCM contains 3 NoC networks/partitions - Data network (target --> DRAMT), Parameters network (target --> REGT) and Parameters network (initiator --> REGI). */
#define PWRC_RTC_NOC_PWRCTL_CLR   0x1884303C

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGI_IDLE_REQ - IDLE request for NoC REGI target */
/* Should be used before disabling NoC REGI clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_REQ__SHIFT       0
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_REQ__MASK        0x00000001
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_REQ__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGI_IDLE_ACK - IDLE acknowledge for NoC REGI target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_ACK__SHIFT       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_ACK__MASK        0x00000002
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_ACK__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGI_IDLE - IDLE state for NoC REGI target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE__SHIFT       2
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE__MASK        0x00000004
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE__INV_MASK    0xFFFFFFFB
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGI_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGT_IDLE_REQ - IDLE request for NoC REGT target */
/* Should be used before disabling NoC REGT clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_REQ__SHIFT       3
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_REQ__MASK        0x00000008
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_REQ__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGT_IDLE_ACK - IDLE acknowledge for NoC REGT target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_ACK__SHIFT       4
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_ACK__MASK        0x00000010
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_ACK__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_REGT_IDLE - IDLE state for NoC REGT target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE__SHIFT       5
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE__MASK        0x00000020
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_REGT_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_DRAMT_IDLE_REQ - IDLE request for NoC DRAMT target */
/* Should be used before disabling NoC DRAMT clock. </br> Setting to low (0) aborts any Idle request in progress.</br>This field may also be set to low (0) by HW when reconnecting according to the noc_reconnect_req signal. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_REQ__SHIFT       6
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_REQ__MASK        0x00000040
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_REQ__INV_MASK    0xFFFFFFBF
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_DRAMT_IDLE_ACK - IDLE acknowledge for NoC DRAMT target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_ACK__SHIFT       7
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_ACK__MASK        0x00000080
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_ACK__INV_MASK    0xFFFFFF7F
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.NOC_DRAMT_IDLE - IDLE state for NoC DRAMT target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE__SHIFT       8
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE__MASK        0x00000100
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_NOC_PWRCTL_CLR__NOC_DRAMT_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_IDLE_REQ - IDLE request for Security taget */
/* Should be used before disabling Security clock. </br> Setting to low (0) aborts any Idle request in progress. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_REQ__SHIFT       9
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_REQ__MASK        0x00000200
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_REQ__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_IDLE_ACK - IDLE acknowledge for Security target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_ACK__SHIFT       10
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_ACK__MASK        0x00000400
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_ACK__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_IDLE - IDLE state for Security target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE__SHIFT       11
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE__MASK        0x00000800
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE__INV_MASK    0xFFFFF7FF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_IDLE_REQ - IDLE request for SPI0 taget. Should be used before disabling SPI0 clock. */
/* Should be used before disabling SPI0 clock. </br> Setting to low (0) aborts any Idle request in progress. */
/* 0 : No request */
/* 1 : Request active */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_REQ__SHIFT       12
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_REQ__MASK        0x00001000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_REQ__INV_MASK    0xFFFFEFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_IDLE_ACK - IDLE acknowledge for SPI0 target. */
/* 0 : Idle request not acknowledged */
/* 1 : Idle request acknowledged (but may not be completed) */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_ACK__SHIFT       13
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_ACK__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_ACK__MASK        0x00002000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_ACK__INV_MASK    0xFFFFDFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE_ACK__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_IDLE - IDLE state for SPI0 target. */
/* 0 : Active (connected) */
/* 1 : Idle (disconnected) */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE__SHIFT       14
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE__MASK        0x00004000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE__INV_MASK    0xFFFFBFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_IDLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.CAN0_SLV_RDY - SLV_RDY request for CAN0 target. Should be used before disabling CAN0 clock. */
/* Should be used before disabling CAN0 clock. */
/* 0 : Disable slave */
/* 1 : Enable slave */
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SLV_RDY__SHIFT       15
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SLV_RDY__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SLV_RDY__MASK        0x00008000
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SLV_RDY__INV_MASK    0xFFFF7FFF
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SLV_RDY__HW_DEFAULT  0x1

/* PWRC_RTC_NOC_PWRCTL_CLR.CAN0_SCKT_CON - SCKT_CON acknowledge for CAN0 target. */
/* 0 : Socket disconnected (clock may be disabled) */
/* 1 : Socket connected */
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SCKT_CON__SHIFT       16
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SCKT_CON__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SCKT_CON__MASK        0x00010000
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SCKT_CON__INV_MASK    0xFFFEFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__CAN0_SCKT_CON__HW_DEFAULT  0x1

/* PWRC_RTC_NOC_PWRCTL_CLR.ARMM3_NFU_ENABLE - Enable ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_ENABLE__SHIFT       17
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_ENABLE__MASK        0x00020000
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_ENABLE__INV_MASK    0xFFFDFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.ARMM3_NFU_FLUSH_REQ - Flush request for ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_REQ__SHIFT       18
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_REQ__MASK        0x00040000
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_REQ__INV_MASK    0xFFFBFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.ARMM3_NFU_FLUSH_DONE - Flush-done indication from ARMM3 NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_DONE__SHIFT       19
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_DONE__MASK        0x00080000
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_DONE__INV_MASK    0xFFF7FFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__ARMM3_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_NFU_ENABLE - Enable SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_ENABLE__SHIFT       20
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_ENABLE__MASK        0x00100000
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_ENABLE__INV_MASK    0xFFEFFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_NFU_FLUSH_REQ - Flush request for SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_REQ__SHIFT       21
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_REQ__MASK        0x00200000
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_REQ__INV_MASK    0xFFDFFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SECURITY_NFU_FLUSH_DONE - Flush-done indication from SECURITY NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_DONE__SHIFT       22
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_DONE__MASK        0x00400000
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_DONE__INV_MASK    0xFFBFFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SECURITY_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_NFU_ENABLE - Enable SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Disabled */
/* 1 : Enabled */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_ENABLE__SHIFT       23
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_ENABLE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_ENABLE__MASK        0x00800000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_ENABLE__INV_MASK    0xFF7FFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_ENABLE__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_NFU_FLUSH_REQ - Flush request for SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Request active */
/* 1 : No request */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_REQ__SHIFT       24
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_REQ__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_REQ__MASK        0x01000000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_REQ__INV_MASK    0xFEFFFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_REQ__HW_DEFAULT  0x0

/* PWRC_RTC_NOC_PWRCTL_CLR.SPI0_NFU_FLUSH_DONE - Flush-done indication from SPI0 NoC-Flush-Unit (NFU) */
/* 0 : Flush not done */
/* 1 : Flush complete */
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_DONE__SHIFT       25
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_DONE__WIDTH       1
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_DONE__MASK        0x02000000
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_DONE__INV_MASK    0xFDFFFFFF
#define PWRC_RTC_NOC_PWRCTL_CLR__SPI0_NFU_FLUSH_DONE__HW_DEFAULT  0x0

/* PWRC XTAL Controlling Register 2 */
/* XTAL configuration can be done according to either Bootstrap (default) or PWRC_XTAL_REG2 values. 
 The selection between the two options is done through PWRC_XTAL_LDO_MUX_SEL2 register. 
 In case of Bootstrap, the XTAL parameters will be injected from PWRC_RTC_BOOTSTRAP registers, bits 80-107. 
 In case of PWRC_XTAL_REG2, XTAL will be configured according to these register settings. */
#define PWRC_XTAL_REG2            0x18843040

/* PWRC_XTAL_REG2.MIDLOAD_TRM - Midrange capacitance values. 
 Used to fill the whole tuning range. 
 Nominal value should be selected according to crystal model specifications (and PCB and package spec). 
 Nominal value is 0. */
#define PWRC_XTAL_REG2__MIDLOAD_TRM__SHIFT       0
#define PWRC_XTAL_REG2__MIDLOAD_TRM__WIDTH       4
#define PWRC_XTAL_REG2__MIDLOAD_TRM__MASK        0x0000000F
#define PWRC_XTAL_REG2__MIDLOAD_TRM__INV_MASK    0xFFFFFFF0
#define PWRC_XTAL_REG2__MIDLOAD_TRM__HW_DEFAULT  0x0

/* PWRC XTAL Mux Selection Register 2 */
/* Affects PWRC_XTAL_REG2, used as data-source selection. 
 Note: This register is reset by HW when entering PSAVING_MODE. */
#define PWRC_XTAL_MUX_SEL2        0x18843044

/* PWRC_XTAL_MUX_SEL2.MIDLOAD_TRM - MIDLOAD_TRM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG2 ('1'). */
#define PWRC_XTAL_MUX_SEL2__MIDLOAD_TRM__SHIFT       0
#define PWRC_XTAL_MUX_SEL2__MIDLOAD_TRM__WIDTH       4
#define PWRC_XTAL_MUX_SEL2__MIDLOAD_TRM__MASK        0x0000000F
#define PWRC_XTAL_MUX_SEL2__MIDLOAD_TRM__INV_MASK    0xFFFFFFF0
#define PWRC_XTAL_MUX_SEL2__MIDLOAD_TRM__HW_DEFAULT  0x0

/* PWRC Control over CAN Register */
/* Each CAN Listener has 8 cores which are concatenated, and SW can thereby configure threshold for CAN_RX toggling amount, for waking up the SoC. 
 SW must configure IO-Handler in order to use CAN #1 on X_RTC_GPIO[3:2]. The correct configuration is setting X_RTC_GPIO[2] on its 2nd digital function and setting X_RTC_GPIO[3] on its 5th digital function. 
 SW flow before entering hibernation (for initializing CAN listeners): 
 1. CAN_LIST0/1_EN = 0 
 2. CAN_LIST0/1_RESET_B = 0 
 3. CAN_LIST0/1_RESET_B = 1 
 4. CAN_LIST0/1_EN = 1 */
#define PWRC_RTC_CAN_CTRL         0x18843048

/* PWRC_RTC_CAN_CTRL.CAN_LIST0_EN - CAN Listener #0 enable */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_EN__SHIFT       0
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_EN__WIDTH       1
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_EN__MASK        0x00000001
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_EN__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_EN__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_CTRL.CAN_LIST1_EN - CAN Listener #1 enable */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_EN__SHIFT       1
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_EN__WIDTH       1
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_EN__MASK        0x00000002
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_EN__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_EN__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_CTRL.CAN_LIST0_SEL - Number of concatenated cores in listener #0 */
/* 0 : 1 core */
/* 1 : 2 core */
/* 2 : 3 core */
/* 3 : 4 core */
/* 4 : 5 core */
/* 5 : 6 core */
/* 6 : 7 core */
/* 7 : 8 core */
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_SEL__SHIFT       2
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_SEL__WIDTH       3
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_SEL__MASK        0x0000001C
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_SEL__INV_MASK    0xFFFFFFE3
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_SEL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_CTRL.CAN_LIST1_SEL - Number of concatenated cores in listener #1 */
/* 0 : 1 core */
/* 1 : 2 core */
/* 2 : 3 core */
/* 3 : 4 core */
/* 4 : 5 core */
/* 5 : 6 core */
/* 6 : 7 core */
/* 7 : 8 core */
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_SEL__SHIFT       5
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_SEL__WIDTH       3
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_SEL__MASK        0x000000E0
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_SEL__INV_MASK    0xFFFFFF1F
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_SEL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_CTRL.CAN_LIST0_DELAY - Delay line calibration for listener #0, to be used for edge derivation. 
 Maximal delay (0xff) obtains 2ns-6ns delay, depends on silicon characteristics. */
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_DELAY__SHIFT       8
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_DELAY__WIDTH       7
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_DELAY__MASK        0x00007F00
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_DELAY__INV_MASK    0xFFFF80FF
#define PWRC_RTC_CAN_CTRL__CAN_LIST0_DELAY__HW_DEFAULT  0x7F

/* PWRC_RTC_CAN_CTRL.CAN_LIST1_DELAY - Delay line calibration for listener #1, to be used for edge derivation. 
 Maximal delay (0xff) obtains 2ns-6ns delay, depends on silicon characteristics. */
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_DELAY__SHIFT       15
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_DELAY__WIDTH       7
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_DELAY__MASK        0x003F8000
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_DELAY__INV_MASK    0xFFC07FFF
#define PWRC_RTC_CAN_CTRL__CAN_LIST1_DELAY__HW_DEFAULT  0x7F

/* PWRC_RTC_CAN_CTRL.CAN_PRE_WAKEUP_CTRL - CAN Pre wakeup assignment options. 
 Note: Any other value in interpreted as an OR combination of the below, e.g. 0x3 means OR of both CAN listeners */
/* 0x0 : Always high (Partial-Connect mode) */
/* 0x1 : CAN Listener #0 interrupt */
/* 0x2 : CAN Listener #1 interrupt */
/* 0x4 : Transceiver interrupt (mapped on GPIO 1) */
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_CTRL__SHIFT       22
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_CTRL__WIDTH       3
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_CTRL__MASK        0x01C00000
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_CTRL__INV_MASK    0xFE3FFFFF
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_CTRL__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_CTRL.EXT_CAN_IRQ_SEL - Transceiver interrupt connection selection */
/* 0 : Ext. Transceiver is connected to CAN network #0 */
/* 1 : Ext. Transceiver is connected to CAN network #1 */
#define PWRC_RTC_CAN_CTRL__EXT_CAN_IRQ_SEL__SHIFT       25
#define PWRC_RTC_CAN_CTRL__EXT_CAN_IRQ_SEL__WIDTH       1
#define PWRC_RTC_CAN_CTRL__EXT_CAN_IRQ_SEL__MASK        0x02000000
#define PWRC_RTC_CAN_CTRL__EXT_CAN_IRQ_SEL__INV_MASK    0xFDFFFFFF
#define PWRC_RTC_CAN_CTRL__EXT_CAN_IRQ_SEL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_CTRL.CAN_PRE_WAKEUP_MODE - CAN pre-wakeup effect */
/* 0 : CAN pre-wakeup causes CAN0 wake-up */
/* 1 : CAN pre-wakeup causes M3PD wake-up */
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_MODE__SHIFT       26
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_MODE__WIDTH       1
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_MODE__MASK        0x04000000
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_MODE__INV_MASK    0xFBFFFFFF
#define PWRC_RTC_CAN_CTRL__CAN_PRE_WAKEUP_MODE__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_CTRL.CAN0_WAKEUP_EN - Wake-up enable from CANBus Controller #0 */
/* 0 : Wakeup from CAN #0 is disabled */
/* 1 : Wakeup from CAN #0 is enabled */
#define PWRC_RTC_CAN_CTRL__CAN0_WAKEUP_EN__SHIFT       27
#define PWRC_RTC_CAN_CTRL__CAN0_WAKEUP_EN__WIDTH       1
#define PWRC_RTC_CAN_CTRL__CAN0_WAKEUP_EN__MASK        0x08000000
#define PWRC_RTC_CAN_CTRL__CAN0_WAKEUP_EN__INV_MASK    0xF7FFFFFF
#define PWRC_RTC_CAN_CTRL__CAN0_WAKEUP_EN__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_CTRL.CAN0_SEL - CAN0 input selection */
/* 0 : Transceiver #0 */
/* 1 : Transceiver #1 (mapped on GPIOs 2,3) */
/* 2 : Auto (by HW) - according to CAN_LIST0_IRQ and CAN_LIST1_IRQ. 
 Note that the CAN0 connectivity mux will automatically be attached to either CAN #0 or CAN #1 only when SW is not available, i.e. in states lower than PSAVING_MODE_M3. */
/* 3 : Same as option 2, except the connectivity mux is locked after the first CAN_LISTx_IRQ. 
 The mux is unlocked upon re-setting of this field (writing to CAN0_SEL). */
#define PWRC_RTC_CAN_CTRL__CAN0_SEL__SHIFT       28
#define PWRC_RTC_CAN_CTRL__CAN0_SEL__WIDTH       2
#define PWRC_RTC_CAN_CTRL__CAN0_SEL__MASK        0x30000000
#define PWRC_RTC_CAN_CTRL__CAN0_SEL__INV_MASK    0xCFFFFFFF
#define PWRC_RTC_CAN_CTRL__CAN0_SEL__HW_DEFAULT  0x2

/* PWRC CAN Status Register */
/* This registers is a mix of both CAN monitoring/visibility and CAN interrupt-status bits. 
 Each field is latched by HW. */
#define PWRC_RTC_CAN_STATUS       0x1884304C

/* PWRC_RTC_CAN_STATUS.CAN_LIST0_IRQ - CAN Listener #0 interrupt. 
 Sticky – cleared by SW write '0'. */
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_IRQ__SHIFT       0
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_IRQ__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_IRQ__MASK        0x00000001
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_IRQ__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_IRQ__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN_LIST1_IRQ - CAN Listener #1 interrupt. 
 Sticky – cleared by SW write '0'. */
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_IRQ__SHIFT       1
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_IRQ__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_IRQ__MASK        0x00000002
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_IRQ__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_IRQ__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN_TRANS_IRQ - CAN Transceiver interrupt (mapped on GPIO 1). 
 Sticky – cleared by SW write '0'. */
#define PWRC_RTC_CAN_STATUS__CAN_TRANS_IRQ__SHIFT       2
#define PWRC_RTC_CAN_STATUS__CAN_TRANS_IRQ__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN_TRANS_IRQ__MASK        0x00000004
#define PWRC_RTC_CAN_STATUS__CAN_TRANS_IRQ__INV_MASK    0xFFFFFFFB
#define PWRC_RTC_CAN_STATUS__CAN_TRANS_IRQ__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN0_WAKEUP_IRQ - Wake-up alert from CAN0. 
 Sticky – cleared by SW write '0'. */
#define PWRC_RTC_CAN_STATUS__CAN0_WAKEUP_IRQ__SHIFT       3
#define PWRC_RTC_CAN_STATUS__CAN0_WAKEUP_IRQ__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN0_WAKEUP_IRQ__MASK        0x00000008
#define PWRC_RTC_CAN_STATUS__CAN0_WAKEUP_IRQ__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_CAN_STATUS__CAN0_WAKEUP_IRQ__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN_LIST0_VAL - CAN Listener #0 value (shift-registers). 
 Auto-cleared by HW (mainly affected by PWRC_RTC_CAN_CTRL register). */
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_VAL__SHIFT       4
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_VAL__WIDTH       8
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_VAL__MASK        0x00000FF0
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_VAL__INV_MASK    0xFFFFF00F
#define PWRC_RTC_CAN_STATUS__CAN_LIST0_VAL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN_LIST1_VAL - CAN Listener #1 value (shift-registers). 
 Auto-cleared by HW (mainly affected by PWRC_RTC_CAN_CTRL register). */
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_VAL__SHIFT       12
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_VAL__WIDTH       8
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_VAL__MASK        0x000FF000
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_VAL__INV_MASK    0xFFF00FFF
#define PWRC_RTC_CAN_STATUS__CAN_LIST1_VAL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN0_SEL_MUX_VAL - CAN0 mux selection value, mostly valuable when CAN0_SEL = 0x2 (SW may conclude which CAN network was automatically connected to CAN0). 
 Auto-cleared by HW (mainly affected by PWRC_RTC_CAN_CTRL register). */
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_MUX_VAL__SHIFT       20
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_MUX_VAL__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_MUX_VAL__MASK        0x00100000
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_MUX_VAL__INV_MASK    0xFFEFFFFF
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_MUX_VAL__HW_DEFAULT  0x0

/* PWRC_RTC_CAN_STATUS.CAN0_SEL_LOCK - CAN0 mux selection lock value, only relevant if CAN0_SEL = 0x3. 
 Auto-cleared by HW (mainly affected by PWRC_RTC_CAN_CTRL register). */
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_LOCK__SHIFT       21
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_LOCK__WIDTH       1
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_LOCK__MASK        0x00200000
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_LOCK__INV_MASK    0xFFDFFFFF
#define PWRC_RTC_CAN_STATUS__CAN0_SEL_LOCK__HW_DEFAULT  0x0

/* PWRC FSM Control Register */
/* PWRC FSM SW Control */
#define PWRC_FSM_M3_CTRL          0x18843050

/* PWRC_FSM_M3_CTRL.CAN_TIMEOUT - Timeout period for CAN_ACTIVE to PSAVING_MODE auto-transition */
/* 0 : Timeout is disabled (i.e. infinite) */
/* 1 :  ~5ms ( 164 xtal cycles) */
/* 2 :  ~10ms ( 328 xtal cycles) */
/* 3 :  ~20ms ( 655 xtal cycles) */
/* 4 :  ~40ms ( 1311 xtal cycles) */
/* 5 :  ~80ms ( 2621 xtal cycles) */
/* 6 : ~160ms ( 5243 xtal cycles) */
/* 7 : ~320ms (10486 xtal cycles) */
#define PWRC_FSM_M3_CTRL__CAN_TIMEOUT__SHIFT       0
#define PWRC_FSM_M3_CTRL__CAN_TIMEOUT__WIDTH       3
#define PWRC_FSM_M3_CTRL__CAN_TIMEOUT__MASK        0x00000007
#define PWRC_FSM_M3_CTRL__CAN_TIMEOUT__INV_MASK    0xFFFFFFF8
#define PWRC_FSM_M3_CTRL__CAN_TIMEOUT__HW_DEFAULT  0x0

/* PWRC_FSM_M3_CTRL.SW_M3_POWER_OFF - Writing 1 to this field will invoke transition from PSAVING_MODE_M3 state to PSAVING_MODE state, i.e. SW power-off. 
 This bit will be cleared by hardware itself. */
#define PWRC_FSM_M3_CTRL__SW_M3_POWER_OFF__SHIFT       30
#define PWRC_FSM_M3_CTRL__SW_M3_POWER_OFF__WIDTH       1
#define PWRC_FSM_M3_CTRL__SW_M3_POWER_OFF__MASK        0x40000000
#define PWRC_FSM_M3_CTRL__SW_M3_POWER_OFF__INV_MASK    0xBFFFFFFF
#define PWRC_FSM_M3_CTRL__SW_M3_POWER_OFF__HW_DEFAULT  0x0

/* PWRC_FSM_M3_CTRL.SW_M3_WAKE_UP - Writing 1 to this field will invoke transition from PSAVING_MODE_M3 state to NORMAL_MODE state, i.e. SW wakeup */
#define PWRC_FSM_M3_CTRL__SW_M3_WAKE_UP__SHIFT       31
#define PWRC_FSM_M3_CTRL__SW_M3_WAKE_UP__WIDTH       1
#define PWRC_FSM_M3_CTRL__SW_M3_WAKE_UP__MASK        0x80000000
#define PWRC_FSM_M3_CTRL__SW_M3_WAKE_UP__INV_MASK    0x7FFFFFFF
#define PWRC_FSM_M3_CTRL__SW_M3_WAKE_UP__HW_DEFAULT  0x0

/* PWRC FSM State Register */
/* PWRC main FSM current-state */
#define PWRC_FSM_STATE            0x18843054

/* PWRC_FSM_STATE.FSM_STATE - PWRC FSM state */
/* 0x00 : COLDBOOT_MODE */
/* 0x01 : BEGIN_POWER_ON */
/* 0x02 : MEM_ON */
/* 0x03 : IO_ON */
/* 0x04 : ANALOG_ON */
/* 0x05 : CORE_ON */
/* 0x06 : BOOT_RET_OFF */
/* 0x07 : BOOT_RESET_OFF */
/* 0x08 : NORMAL_MODE */
/* 0x09 : PREPARE_RET */
/* 0x0A : BEGIN_RET */
/* 0x0B : RET_DONE */
/* 0x0C : BEGIN_POWER_OFF */
/* 0x0D : ONE_OFF */
/* 0x0E : ANALOG_OFF */
/* 0x0F : IO_OFF */
/* 0x10 : MEM_OFF */
/* 0x11 : CORE_OFF */
/* 0x12 : BOOT_RESET_ON */
/* 0x13 : PSAVING_MODE */
/* 0x14 : PSAVING_MODE_M3 */
/* 0x15 : M3_RESET_OFF */
/* 0x16 : M3_RESET_ON */
/* 0x17 : CAN_ACTIVE */
/* 0x18 : M3_ON */
/* 0x19 : M3_OFF */
#define PWRC_FSM_STATE__FSM_STATE__SHIFT       0
#define PWRC_FSM_STATE__FSM_STATE__WIDTH       5
#define PWRC_FSM_STATE__FSM_STATE__MASK        0x0000001F
#define PWRC_FSM_STATE__FSM_STATE__INV_MASK    0xFFFFFFE0
#define PWRC_FSM_STATE__FSM_STATE__HW_DEFAULT  0x0

/* PWRC RTC RTCLDO Controlling Register */
/* RTC LDO configuration can be done according to either Bootstrap (default) or PWRC_RTCLDO_REG values. 
 The selection between the two options is done through PWRC_XTAL_LDO_MUX_SEL register. 
 In case of Bootstrap, the RTCLDO parameters will be injected from PWRC_RTC_BOOTSTRAP registers, bits 80-107. 
 In case of PWRC_RTCLDOL_REG, the RTCLDO will be configured according to these register settings. */
#define PWRC_RTCLDO_REG           0x18843058

/* PWRC_RTCLDO_REG.VBG_TRIM - VBGTRIM, Band Gap voltage trimming, overwrites bootstrap data (bits [44:47]) */
#define PWRC_RTCLDO_REG__VBG_TRIM__SHIFT       0
#define PWRC_RTCLDO_REG__VBG_TRIM__WIDTH       4
#define PWRC_RTCLDO_REG__VBG_TRIM__MASK        0x0000000F
#define PWRC_RTCLDO_REG__VBG_TRIM__INV_MASK    0xFFFFFFF0
#define PWRC_RTCLDO_REG__VBG_TRIM__HW_DEFAULT  0x0

/* PWRC_RTCLDO_REG.IREF_TRIM - Overwrites bootstrap data (bits [48:50]) */
#define PWRC_RTCLDO_REG__IREF_TRIM__SHIFT       4
#define PWRC_RTCLDO_REG__IREF_TRIM__WIDTH       3
#define PWRC_RTCLDO_REG__IREF_TRIM__MASK        0x00000070
#define PWRC_RTCLDO_REG__IREF_TRIM__INV_MASK    0xFFFFFF8F
#define PWRC_RTCLDO_REG__IREF_TRIM__HW_DEFAULT  0x0

/* PWRC_RTCLDO_REG.PORB - Power-On Reset output */
#define PWRC_RTCLDO_REG__PORB__SHIFT       31
#define PWRC_RTCLDO_REG__PORB__WIDTH       1
#define PWRC_RTCLDO_REG__PORB__MASK        0x80000000
#define PWRC_RTCLDO_REG__PORB__INV_MASK    0x7FFFFFFF
#define PWRC_RTCLDO_REG__PORB__HW_DEFAULT  0x1

/* PWRC GNSS Controlling Register */
/* GNSS_CFG is a write-once field, i.e. can be written by SW only once after Coldboot. 
 PWRC_GNSS_CTRL register, except GNSS_CFG field (W1), should be initiated to reset/default value upon either a7_wd_hw_rst_b, m3_wd_rst_b or nsrst_reset_b events. */
#define PWRC_GNSS_CTRL            0x1884305C

/* PWRC_GNSS_CTRL.GNSS_FORCE_PON - Force power ON signal to GNSS_RTC block. 
 It will be auto-cleared by the HW after receives acknowledge from the GNSS RTC. */
/* 0 : No use */
/* 1 : Force power ON GNSS RTC */
#define PWRC_GNSS_CTRL__GNSS_FORCE_PON__SHIFT       0
#define PWRC_GNSS_CTRL__GNSS_FORCE_PON__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_FORCE_PON__MASK        0x00000001
#define PWRC_GNSS_CTRL__GNSS_FORCE_PON__INV_MASK    0xFFFFFFFE
#define PWRC_GNSS_CTRL__GNSS_FORCE_PON__HW_DEFAULT  0x0

/* PWRC_GNSS_CTRL.GNSS_FORCE_POFF - Force power OFF signal to GNSS_RTC block. 
 It will be auto-cleared by the HW after receives acknowledge from the GNSS RTC. */
/* 0 : No use */
/* 1 : Force power OFF GNSS RTC */
#define PWRC_GNSS_CTRL__GNSS_FORCE_POFF__SHIFT       1
#define PWRC_GNSS_CTRL__GNSS_FORCE_POFF__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_FORCE_POFF__MASK        0x00000002
#define PWRC_GNSS_CTRL__GNSS_FORCE_POFF__INV_MASK    0xFFFFFFFD
#define PWRC_GNSS_CTRL__GNSS_FORCE_POFF__HW_DEFAULT  0x0

/* PWRC_GNSS_CTRL.GNSS_SW_RST_B - Software reset to GNSS block. */
/* 0 : GNSS block reset valid */
/* 1 : GNSS block reset invalid */
#define PWRC_GNSS_CTRL__GNSS_SW_RST_B__SHIFT       2
#define PWRC_GNSS_CTRL__GNSS_SW_RST_B__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_SW_RST_B__MASK        0x00000004
#define PWRC_GNSS_CTRL__GNSS_SW_RST_B__INV_MASK    0xFFFFFFFB
#define PWRC_GNSS_CTRL__GNSS_SW_RST_B__HW_DEFAULT  0x1

/* PWRC_GNSS_CTRL.GNSS_WAKEUP_EN - Enable GNSS request power ON trigger to wakeup */
/* 0 : Disable */
/* 1 : Enable */
#define PWRC_GNSS_CTRL__GNSS_WAKEUP_EN__SHIFT       3
#define PWRC_GNSS_CTRL__GNSS_WAKEUP_EN__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_WAKEUP_EN__MASK        0x00000008
#define PWRC_GNSS_CTRL__GNSS_WAKEUP_EN__INV_MASK    0xFFFFFFF7
#define PWRC_GNSS_CTRL__GNSS_WAKEUP_EN__HW_DEFAULT  0x1

/* PWRC_GNSS_CTRL.GNSS_FORCE_CLR - Force power ON or OFF signal clear. */
/* 0 : No use */
/* 1 : Clear the force signal of power ON or OFF to GNSS RTC */
#define PWRC_GNSS_CTRL__GNSS_FORCE_CLR__SHIFT       14
#define PWRC_GNSS_CTRL__GNSS_FORCE_CLR__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_FORCE_CLR__MASK        0x00004000
#define PWRC_GNSS_CTRL__GNSS_FORCE_CLR__INV_MASK    0xFFFFBFFF
#define PWRC_GNSS_CTRL__GNSS_FORCE_CLR__HW_DEFAULT  0x0

/* PWRC_GNSS_CTRL.GNSS_STANDALONE - GNSS stand-alone mode */
#define PWRC_GNSS_CTRL__GNSS_STANDALONE__SHIFT       15
#define PWRC_GNSS_CTRL__GNSS_STANDALONE__WIDTH       1
#define PWRC_GNSS_CTRL__GNSS_STANDALONE__MASK        0x00008000
#define PWRC_GNSS_CTRL__GNSS_STANDALONE__INV_MASK    0xFFFF7FFF
#define PWRC_GNSS_CTRL__GNSS_STANDALONE__HW_DEFAULT  0x0

/* PWRC_GNSS_CTRL.GNSS_CFG - GNSS configuration bits, W1 (formerly implemented by EFUSE). */
#define PWRC_GNSS_CTRL__GNSS_CFG__SHIFT       16
#define PWRC_GNSS_CTRL__GNSS_CFG__WIDTH       16
#define PWRC_GNSS_CTRL__GNSS_CFG__MASK        0xFFFF0000
#define PWRC_GNSS_CTRL__GNSS_CFG__INV_MASK    0x0000FFFF
#define PWRC_GNSS_CTRL__GNSS_CFG__HW_DEFAULT  0x0

/* PWRC GNSS Status Register */
/* GNSS Status register */
#define PWRC_GNSS_STATUS          0x18843060

/* PWRC_GNSS_STATUS.GNSS_PON_REQ_PIN - Directly read GNSS_PON_REQ PIN status from GNSS RTC */
#define PWRC_GNSS_STATUS__GNSS_PON_REQ_PIN__SHIFT       0
#define PWRC_GNSS_STATUS__GNSS_PON_REQ_PIN__WIDTH       1
#define PWRC_GNSS_STATUS__GNSS_PON_REQ_PIN__MASK        0x00000001
#define PWRC_GNSS_STATUS__GNSS_PON_REQ_PIN__INV_MASK    0xFFFFFFFE
#define PWRC_GNSS_STATUS__GNSS_PON_REQ_PIN__HW_DEFAULT  0x0

/* PWRC_GNSS_STATUS.GNSS_POFF_REQ_PIN - Directly read GNSS_POFF_REQ PIN status from GNSS RTC */
#define PWRC_GNSS_STATUS__GNSS_POFF_REQ_PIN__SHIFT       1
#define PWRC_GNSS_STATUS__GNSS_POFF_REQ_PIN__WIDTH       1
#define PWRC_GNSS_STATUS__GNSS_POFF_REQ_PIN__MASK        0x00000002
#define PWRC_GNSS_STATUS__GNSS_POFF_REQ_PIN__INV_MASK    0xFFFFFFFD
#define PWRC_GNSS_STATUS__GNSS_POFF_REQ_PIN__HW_DEFAULT  0x0

/* PWRC_GNSS_STATUS.GNSS_PON_ACK_PIN - Directly read GNSS_FORCE_PON_ACK PIN status from GNSS RTC */
#define PWRC_GNSS_STATUS__GNSS_PON_ACK_PIN__SHIFT       2
#define PWRC_GNSS_STATUS__GNSS_PON_ACK_PIN__WIDTH       1
#define PWRC_GNSS_STATUS__GNSS_PON_ACK_PIN__MASK        0x00000004
#define PWRC_GNSS_STATUS__GNSS_PON_ACK_PIN__INV_MASK    0xFFFFFFFB
#define PWRC_GNSS_STATUS__GNSS_PON_ACK_PIN__HW_DEFAULT  0x0

/* PWRC_GNSS_STATUS.GNSS_POFF_ACK_PIN - Directly read GNSS_FORCE_POFF_ACK PIN status from GNSS RTC */
#define PWRC_GNSS_STATUS__GNSS_POFF_ACK_PIN__SHIFT       3
#define PWRC_GNSS_STATUS__GNSS_POFF_ACK_PIN__WIDTH       1
#define PWRC_GNSS_STATUS__GNSS_POFF_ACK_PIN__MASK        0x00000008
#define PWRC_GNSS_STATUS__GNSS_POFF_ACK_PIN__INV_MASK    0xFFFFFFF7
#define PWRC_GNSS_STATUS__GNSS_POFF_ACK_PIN__HW_DEFAULT  0x0

/* PWRC_GNSS_STATUS.GNSS_SW_STATUS - Directly read GNSS_SW_STATUS PIN status from GNSS RTC */
#define PWRC_GNSS_STATUS__GNSS_SW_STATUS__SHIFT       4
#define PWRC_GNSS_STATUS__GNSS_SW_STATUS__WIDTH       16
#define PWRC_GNSS_STATUS__GNSS_SW_STATUS__MASK        0x000FFFF0
#define PWRC_GNSS_STATUS__GNSS_SW_STATUS__INV_MASK    0xFFF0000F
#define PWRC_GNSS_STATUS__GNSS_SW_STATUS__HW_DEFAULT  0x0

/* PWRC XTAL Controlling Register */
/* XTAL configuration can be done according to either Bootstrap (default) or PWRC_XTAL_REG values. 
 The selection between the two options is done through PWRC_XTAL_LDO_MUX_SEL register. 
 In any case, the following bits of the muxed vector should be inverted: 0, 1, 6, 13 and 21. 
 In case of Bootstrap, the XTAL parameters will be injected from PWRC_RTC_BOOTSTRAP registers, bits 80-107. 
 In case of PWRC_XTAL_REG, XTAL will be configured according to these register settings. */
#define PWRC_XTAL_REG             0x18843064

/* PWRC_XTAL_REG.XTAL_EN - Enable xtal core */
#define PWRC_XTAL_REG__XTAL_EN__SHIFT       0
#define PWRC_XTAL_REG__XTAL_EN__WIDTH       1
#define PWRC_XTAL_REG__XTAL_EN__MASK        0x00000001
#define PWRC_XTAL_REG__XTAL_EN__INV_MASK    0xFFFFFFFE
#define PWRC_XTAL_REG__XTAL_EN__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_EN_LDO - Enable internal 1.2V sub-regulator and the Bandgap reference */
#define PWRC_XTAL_REG__XTAL_EN_LDO__SHIFT       1
#define PWRC_XTAL_REG__XTAL_EN_LDO__WIDTH       1
#define PWRC_XTAL_REG__XTAL_EN_LDO__MASK        0x00000002
#define PWRC_XTAL_REG__XTAL_EN_LDO__INV_MASK    0xFFFFFFFD
#define PWRC_XTAL_REG__XTAL_EN_LDO__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_EN_CLKDET - Enable the clock detector circuit */
#define PWRC_XTAL_REG__XTAL_EN_CLKDET__SHIFT       2
#define PWRC_XTAL_REG__XTAL_EN_CLKDET__WIDTH       1
#define PWRC_XTAL_REG__XTAL_EN_CLKDET__MASK        0x00000004
#define PWRC_XTAL_REG__XTAL_EN_CLKDET__INV_MASK    0xFFFFFFFB
#define PWRC_XTAL_REG__XTAL_EN_CLKDET__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_SPARE - Spare dynamic controls */
#define PWRC_XTAL_REG__XTAL_SPARE__SHIFT       3
#define PWRC_XTAL_REG__XTAL_SPARE__WIDTH       2
#define PWRC_XTAL_REG__XTAL_SPARE__MASK        0x00000018
#define PWRC_XTAL_REG__XTAL_SPARE__INV_MASK    0xFFFFFFE7
#define PWRC_XTAL_REG__XTAL_SPARE__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_LPM - Enable LDO low power mode. 
 Affects CAN_ACTIVE state only (might be useful for Partial-Connect mode). */
#define PWRC_XTAL_REG__XTAL_CTRL_LPM__SHIFT       5
#define PWRC_XTAL_REG__XTAL_CTRL_LPM__WIDTH       1
#define PWRC_XTAL_REG__XTAL_CTRL_LPM__MASK        0x00000020
#define PWRC_XTAL_REG__XTAL_CTRL_LPM__INV_MASK    0xFFFFFFDF
#define PWRC_XTAL_REG__XTAL_CTRL_LPM__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_SEL_XTAL - Enable the crystal driver amplifier. 
 This bit is only valid in the 26MHz clock oscillator mode */
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_XTAL__SHIFT       6
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_XTAL__WIDTH       1
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_XTAL__MASK        0x00000040
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_XTAL__INV_MASK    0xFFFFFFBF
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_XTAL__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_LOAD_FINETRM - XTAL_IN load capacitance fine trimming */
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_FINETRM__SHIFT       7
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_FINETRM__WIDTH       7
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_FINETRM__MASK        0x00003F80
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_FINETRM__INV_MASK    0xFFFFC07F
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_FINETRM__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_SEL_LP - Set low power mode */
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_LP__SHIFT       14
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_LP__WIDTH       1
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_LP__MASK        0x00004000
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_LP__INV_MASK    0xFFFFBFFF
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_LP__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_SEL_ULP - Set ultra low power mode */
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_ULP__SHIFT       15
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_ULP__WIDTH       1
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_ULP__MASK        0x00008000
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_ULP__INV_MASK    0xFFFF7FFF
#define PWRC_XTAL_REG__XTAL_CTRL_SEL_ULP__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_LVL - Controls the gain of the crystal driver amplifier */
#define PWRC_XTAL_REG__XTAL_CTRL_LVL__SHIFT       16
#define PWRC_XTAL_REG__XTAL_CTRL_LVL__WIDTH       4
#define PWRC_XTAL_REG__XTAL_CTRL_LVL__MASK        0x000F0000
#define PWRC_XTAL_REG__XTAL_CTRL_LVL__INV_MASK    0xFFF0FFFF
#define PWRC_XTAL_REG__XTAL_CTRL_LVL__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_LOAD_TRM - Load capacitance coarse trimming */
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_TRM__SHIFT       20
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_TRM__WIDTH       2
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_TRM__MASK        0x00300000
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_TRM__INV_MASK    0xFFCFFFFF
#define PWRC_XTAL_REG__XTAL_CTRL_LOAD_TRM__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_FILT_TRM - RF filter capacitance trimming */
#define PWRC_XTAL_REG__XTAL_CTRL_FILT_TRM__SHIFT       22
#define PWRC_XTAL_REG__XTAL_CTRL_FILT_TRM__WIDTH       2
#define PWRC_XTAL_REG__XTAL_CTRL_FILT_TRM__MASK        0x00C00000
#define PWRC_XTAL_REG__XTAL_CTRL_FILT_TRM__INV_MASK    0xFF3FFFFF
#define PWRC_XTAL_REG__XTAL_CTRL_FILT_TRM__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_CTRL_HQREF_TRIM - Bandgap reference voltage trimming */
#define PWRC_XTAL_REG__XTAL_CTRL_HQREF_TRIM__SHIFT       24
#define PWRC_XTAL_REG__XTAL_CTRL_HQREF_TRIM__WIDTH       4
#define PWRC_XTAL_REG__XTAL_CTRL_HQREF_TRIM__MASK        0x0F000000
#define PWRC_XTAL_REG__XTAL_CTRL_HQREF_TRIM__INV_MASK    0xF0FFFFFF
#define PWRC_XTAL_REG__XTAL_CTRL_HQREF_TRIM__HW_DEFAULT  0x0

/* PWRC_XTAL_REG.XTAL_OUT_SPARE - XTAL spare outputs */
#define PWRC_XTAL_REG__XTAL_OUT_SPARE__SHIFT       28
#define PWRC_XTAL_REG__XTAL_OUT_SPARE__WIDTH       4
#define PWRC_XTAL_REG__XTAL_OUT_SPARE__MASK        0xF0000000
#define PWRC_XTAL_REG__XTAL_OUT_SPARE__INV_MASK    0x0FFFFFFF
#define PWRC_XTAL_REG__XTAL_OUT_SPARE__HW_DEFAULT  0x0

/* PWRC XTAL and RTCLDO Mux Selection Register */
/* Affects both PWRC_RTCLDO_REG and PWRC_XTAL_REG, used as data-source selection. 
 Note: This register is reset by HW when entering PSAVING_MODE. */
#define PWRC_XTAL_LDO_MUX_SEL     0x18843068

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_EN - XTAL_EN, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN__SHIFT       0
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN__MASK        0x00000001
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN__INV_MASK    0xFFFFFFFE
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_EN_LDO - XTAL_EN_LDO, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_LDO__SHIFT       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_LDO__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_LDO__MASK        0x00000002
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_LDO__INV_MASK    0xFFFFFFFD
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_LDO__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_EN_CLKDET - XTAL_EN_CLKDET, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_CLKDET__SHIFT       2
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_CLKDET__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_CLKDET__MASK        0x00000004
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_CLKDET__INV_MASK    0xFFFFFFFB
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_EN_CLKDET__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_SPARE - XTAL_SPARE[1:0], bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_SPARE__SHIFT       3
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_SPARE__WIDTH       2
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_SPARE__MASK        0x00000018
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_SPARE__INV_MASK    0xFFFFFFE7
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_SPARE__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_LPM - XTAL_CTRL_LPM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LPM__SHIFT       5
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LPM__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LPM__MASK        0x00000020
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LPM__INV_MASK    0xFFFFFFDF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LPM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_SEL_XTAL - XTAL_CTRL_SEL_XTAL, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_XTAL__SHIFT       6
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_XTAL__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_XTAL__MASK        0x00000040
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_XTAL__INV_MASK    0xFFFFFFBF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_XTAL__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_LOAD_FINETRM - XTAL_CTRL_LOAD_FINETRM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_FINETRM__SHIFT       7
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_FINETRM__WIDTH       7
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_FINETRM__MASK        0x00003F80
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_FINETRM__INV_MASK    0xFFFFC07F
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_FINETRM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_SEL_LP - XTAL_CTRL_SEL_LP, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_LP__SHIFT       14
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_LP__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_LP__MASK        0x00004000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_LP__INV_MASK    0xFFFFBFFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_LP__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_SEL_ULP - XTAL_CTRL_SEL_ULP, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_ULP__SHIFT       15
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_ULP__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_ULP__MASK        0x00008000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_ULP__INV_MASK    0xFFFF7FFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_SEL_ULP__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_LVL - XTAL_CTRL_LVL, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LVL__SHIFT       16
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LVL__WIDTH       4
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LVL__MASK        0x000F0000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LVL__INV_MASK    0xFFF0FFFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LVL__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_LOAD_TRM - XTAL_CTRL_LOAD_TRM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_TRM__SHIFT       20
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_TRM__WIDTH       2
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_TRM__MASK        0x00300000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_TRM__INV_MASK    0xFFCFFFFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_LOAD_TRM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_FILT_TRM - XTAL_CTRL_FILT_TRM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_FILT_TRM__SHIFT       22
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_FILT_TRM__WIDTH       2
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_FILT_TRM__MASK        0x00C00000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_FILT_TRM__INV_MASK    0xFF3FFFFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_FILT_TRM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.XTAL_CTRL_HQREF_TRIM - XTAL_CTRL_HQREF_TRIM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_HQREF_TRIM__SHIFT       24
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_HQREF_TRIM__WIDTH       4
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_HQREF_TRIM__MASK        0x0F000000
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_HQREF_TRIM__INV_MASK    0xF0FFFFFF
#define PWRC_XTAL_LDO_MUX_SEL__XTAL_CTRL_HQREF_TRIM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.RTCLDO_VBG_TRIM - RTCLDO_VBG_TRIM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_VBG_TRIM__SHIFT       28
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_VBG_TRIM__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_VBG_TRIM__MASK        0x10000000
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_VBG_TRIM__INV_MASK    0xEFFFFFFF
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_VBG_TRIM__HW_DEFAULT  0x0

/* PWRC_XTAL_LDO_MUX_SEL.RTCLDO_IREF_TRIM - RTCLDO_IREF_TRIM, bitwise mask - data can be taken from either Bootstrap ('0') or PWRC_XTAL_REG ('1'). */
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_IREF_TRIM__SHIFT       29
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_IREF_TRIM__WIDTH       1
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_IREF_TRIM__MASK        0x20000000
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_IREF_TRIM__INV_MASK    0xDFFFFFFF
#define PWRC_XTAL_LDO_MUX_SEL__RTCLDO_IREF_TRIM__HW_DEFAULT  0x0

/* PWRC Resets control for RTC SET Register */
/* 1. SW values are ANDed with HW signals. 
 2. For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input). 
 3. ds_bcrl_rtc_reset_b is not controlled by SW and is assigned to mix_reset_b_hw, i.e. assign ds_bcrl_rtc_reset_b = mix_reset_b_hw. 
 4. Watchdog flow and well described in the Watchdog Application-Note. 
 5. MIX_RESET_B and RTC_NOC_IOB_RESET_B will be cleared automatically by HW after 2 cycles (XINW) - pulse signals. */
#define PWRC_RTC_SW_RSTC_SET      0x1884306C

/* PWRC_RTC_SW_RSTC_SET.CORE_RESET_B - Core reset output for CLKC */
#define PWRC_RTC_SW_RSTC_SET__CORE_RESET_B__SHIFT       0
#define PWRC_RTC_SW_RSTC_SET__CORE_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__CORE_RESET_B__MASK        0x00000001
#define PWRC_RTC_SW_RSTC_SET__CORE_RESET_B__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_SW_RSTC_SET__CORE_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.A7_WARM_RESET_B - Warm Reset for A7. 
 M3 can read RST_STS register within the CLKC, in order to know that reset (+dram refresh procedure) has been completed. 
 Note that A7_WARM_RESET_B and A7_WD_SW_RST_B are reflected in different PON_STATUS bits, but are OR-ed into a same single output towards CLKC (i.e. they have exact same affect) */
#define PWRC_RTC_SW_RSTC_SET__A7_WARM_RESET_B__SHIFT       1
#define PWRC_RTC_SW_RSTC_SET__A7_WARM_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__A7_WARM_RESET_B__MASK        0x00000002
#define PWRC_RTC_SW_RSTC_SET__A7_WARM_RESET_B__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_SW_RSTC_SET__A7_WARM_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.MIX_RESET_B - Reset for internal PWRC block. 
 Reset will be cleared automatically by HW after 2 cycles (XINW) - pulse signal. */
#define PWRC_RTC_SW_RSTC_SET__MIX_RESET_B__SHIFT       2
#define PWRC_RTC_SW_RSTC_SET__MIX_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__MIX_RESET_B__MASK        0x00000004
#define PWRC_RTC_SW_RSTC_SET__MIX_RESET_B__INV_MASK    0xFFFFFFFB
#define PWRC_RTC_SW_RSTC_SET__MIX_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.SYSRTC_RESET_B - Reset for System RTC */
#define PWRC_RTC_SW_RSTC_SET__SYSRTC_RESET_B__SHIFT       3
#define PWRC_RTC_SW_RSTC_SET__SYSRTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__SYSRTC_RESET_B__MASK        0x00000008
#define PWRC_RTC_SW_RSTC_SET__SYSRTC_RESET_B__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_SW_RSTC_SET__SYSRTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_POR_RESET_B - Reset for Cortex-M3 (PORRESETn) 
 Reset at power-up, full system reset (Cold reset). */
#define PWRC_RTC_SW_RSTC_SET__M3_POR_RESET_B__SHIFT       4
#define PWRC_RTC_SW_RSTC_SET__M3_POR_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_POR_RESET_B__MASK        0x00000010
#define PWRC_RTC_SW_RSTC_SET__M3_POR_RESET_B__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_SW_RSTC_SET__M3_POR_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_SYS_RESET_B - Reset for Cortex-M3 (SYSRESETn) 
 Resets the processor only, excluding debug logic (Warm reset). */
#define PWRC_RTC_SW_RSTC_SET__M3_SYS_RESET_B__SHIFT       5
#define PWRC_RTC_SW_RSTC_SET__M3_SYS_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_SYS_RESET_B__MASK        0x00000020
#define PWRC_RTC_SW_RSTC_SET__M3_SYS_RESET_B__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_SW_RSTC_SET__M3_SYS_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_DAP_RESET_B - Reset for Cortex-M3 (DAPRESETn) 
 Resets the bus interface connecting between DP and AP (AHB-AP) */
#define PWRC_RTC_SW_RSTC_SET__M3_DAP_RESET_B__SHIFT       6
#define PWRC_RTC_SW_RSTC_SET__M3_DAP_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_DAP_RESET_B__MASK        0x00000040
#define PWRC_RTC_SW_RSTC_SET__M3_DAP_RESET_B__INV_MASK    0xFFFFFFBF
#define PWRC_RTC_SW_RSTC_SET__M3_DAP_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_CTI_RESET_B - Reset for Cortex-M3 (CTIRESETn) 
 Resets CTI and CTI wrapper */
#define PWRC_RTC_SW_RSTC_SET__M3_CTI_RESET_B__SHIFT       7
#define PWRC_RTC_SW_RSTC_SET__M3_CTI_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_CTI_RESET_B__MASK        0x00000080
#define PWRC_RTC_SW_RSTC_SET__M3_CTI_RESET_B__INV_MASK    0xFFFFFF7F
#define PWRC_RTC_SW_RSTC_SET__M3_CTI_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.CAN0_RESET_B - Reset for CAN0 */
#define PWRC_RTC_SW_RSTC_SET__CAN0_RESET_B__SHIFT       8
#define PWRC_RTC_SW_RSTC_SET__CAN0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__CAN0_RESET_B__MASK        0x00000100
#define PWRC_RTC_SW_RSTC_SET__CAN0_RESET_B__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_SW_RSTC_SET__CAN0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.SPI0_RESET_B - Reset for SPI0 (QSPI) */
#define PWRC_RTC_SW_RSTC_SET__SPI0_RESET_B__SHIFT       9
#define PWRC_RTC_SW_RSTC_SET__SPI0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__SPI0_RESET_B__MASK        0x00000200
#define PWRC_RTC_SW_RSTC_SET__SPI0_RESET_B__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_SW_RSTC_SET__SPI0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.RTC_SECURITY_RESET_B - Reset for RTC_SECURITY */
#define PWRC_RTC_SW_RSTC_SET__RTC_SECURITY_RESET_B__SHIFT       10
#define PWRC_RTC_SW_RSTC_SET__RTC_SECURITY_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__RTC_SECURITY_RESET_B__MASK        0x00000400
#define PWRC_RTC_SW_RSTC_SET__RTC_SECURITY_RESET_B__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_SW_RSTC_SET__RTC_SECURITY_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.IO_RTC_RESET_B - Reset for IO_RTC */
#define PWRC_RTC_SW_RSTC_SET__IO_RTC_RESET_B__SHIFT       11
#define PWRC_RTC_SW_RSTC_SET__IO_RTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__IO_RTC_RESET_B__MASK        0x00000800
#define PWRC_RTC_SW_RSTC_SET__IO_RTC_RESET_B__INV_MASK    0xFFFFF7FF
#define PWRC_RTC_SW_RSTC_SET__IO_RTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.GPIO_RTC_RESET_B - Reset for GPIO_RTC */
#define PWRC_RTC_SW_RSTC_SET__GPIO_RTC_RESET_B__SHIFT       12
#define PWRC_RTC_SW_RSTC_SET__GPIO_RTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__GPIO_RTC_RESET_B__MASK        0x00001000
#define PWRC_RTC_SW_RSTC_SET__GPIO_RTC_RESET_B__INV_MASK    0xFFFFEFFF
#define PWRC_RTC_SW_RSTC_SET__GPIO_RTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.RTC_NOC_IOB_RESET_B - Reset for RTC NoC and IO-Bridge. 
 Reset will be cleared automatically by HW after 2 cycles (XINW) - pulse signal. */
#define PWRC_RTC_SW_RSTC_SET__RTC_NOC_IOB_RESET_B__SHIFT       13
#define PWRC_RTC_SW_RSTC_SET__RTC_NOC_IOB_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__RTC_NOC_IOB_RESET_B__MASK        0x00002000
#define PWRC_RTC_SW_RSTC_SET__RTC_NOC_IOB_RESET_B__INV_MASK    0xFFFFDFFF
#define PWRC_RTC_SW_RSTC_SET__RTC_NOC_IOB_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.CSSI_RST_RTCM_B - Reset for CSSI_RTC */
#define PWRC_RTC_SW_RSTC_SET__CSSI_RST_RTCM_B__SHIFT       14
#define PWRC_RTC_SW_RSTC_SET__CSSI_RST_RTCM_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__CSSI_RST_RTCM_B__MASK        0x00004000
#define PWRC_RTC_SW_RSTC_SET__CSSI_RST_RTCM_B__INV_MASK    0xFFFFBFFF
#define PWRC_RTC_SW_RSTC_SET__CSSI_RST_RTCM_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.CAN_LIST0_RESET_B - Reset for CAN Listener #0 */
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST0_RESET_B__SHIFT       15
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST0_RESET_B__MASK        0x00008000
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST0_RESET_B__INV_MASK    0xFFFF7FFF
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.CAN_LIST1_RESET_B - Reset for CAN Listener #1 */
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST1_RESET_B__SHIFT       16
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST1_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST1_RESET_B__MASK        0x00010000
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST1_RESET_B__INV_MASK    0xFFFEFFFF
#define PWRC_RTC_SW_RSTC_SET__CAN_LIST1_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.REBOOT_B - X_REBOOT_B input */
#define PWRC_RTC_SW_RSTC_SET__REBOOT_B__SHIFT       17
#define PWRC_RTC_SW_RSTC_SET__REBOOT_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__REBOOT_B__MASK        0x00020000
#define PWRC_RTC_SW_RSTC_SET__REBOOT_B__INV_MASK    0xFFFDFFFF
#define PWRC_RTC_SW_RSTC_SET__REBOOT_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.RTC_RST_B - X_RTC_RST_B input */
#define PWRC_RTC_SW_RSTC_SET__RTC_RST_B__SHIFT       18
#define PWRC_RTC_SW_RSTC_SET__RTC_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__RTC_RST_B__MASK        0x00040000
#define PWRC_RTC_SW_RSTC_SET__RTC_RST_B__INV_MASK    0xFFFBFFFF
#define PWRC_RTC_SW_RSTC_SET__RTC_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.RESET_B - X_RESET_B input */
#define PWRC_RTC_SW_RSTC_SET__RESET_B__SHIFT       19
#define PWRC_RTC_SW_RSTC_SET__RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__RESET_B__MASK        0x00080000
#define PWRC_RTC_SW_RSTC_SET__RESET_B__INV_MASK    0xFFF7FFFF
#define PWRC_RTC_SW_RSTC_SET__RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_TIMER_RESET_B - M3 OsTimer reset */
#define PWRC_RTC_SW_RSTC_SET__M3_TIMER_RESET_B__SHIFT       20
#define PWRC_RTC_SW_RSTC_SET__M3_TIMER_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_TIMER_RESET_B__MASK        0x00100000
#define PWRC_RTC_SW_RSTC_SET__M3_TIMER_RESET_B__INV_MASK    0xFFEFFFFF
#define PWRC_RTC_SW_RSTC_SET__M3_TIMER_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.M3_WD_RST_B - M3 Watchdog reset, OsTimer based */
#define PWRC_RTC_SW_RSTC_SET__M3_WD_RST_B__SHIFT       21
#define PWRC_RTC_SW_RSTC_SET__M3_WD_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__M3_WD_RST_B__MASK        0x00200000
#define PWRC_RTC_SW_RSTC_SET__M3_WD_RST_B__INV_MASK    0xFFDFFFFF
#define PWRC_RTC_SW_RSTC_SET__M3_WD_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.A7_WD_HW_RST_B - A7 Watchdog reset, OsTimer based */
#define PWRC_RTC_SW_RSTC_SET__A7_WD_HW_RST_B__SHIFT       22
#define PWRC_RTC_SW_RSTC_SET__A7_WD_HW_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__A7_WD_HW_RST_B__MASK        0x00400000
#define PWRC_RTC_SW_RSTC_SET__A7_WD_HW_RST_B__INV_MASK    0xFFBFFFFF
#define PWRC_RTC_SW_RSTC_SET__A7_WD_HW_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_SET.A7_WD_SW_RST_B - A7 Watchdog reset, triggered by SW. 
 Note that A7_WARM_RESET_B and A7_WD_SW_RST_B are reflected in different PON_STATUS bits, but are OR-ed into a same single output towards CLKC (i.e. they have exact same affect) */
#define PWRC_RTC_SW_RSTC_SET__A7_WD_SW_RST_B__SHIFT       23
#define PWRC_RTC_SW_RSTC_SET__A7_WD_SW_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_SET__A7_WD_SW_RST_B__MASK        0x00800000
#define PWRC_RTC_SW_RSTC_SET__A7_WD_SW_RST_B__INV_MASK    0xFF7FFFFF
#define PWRC_RTC_SW_RSTC_SET__A7_WD_SW_RST_B__HW_DEFAULT  0x1

/* PWRC Resets control for RTC CLR Register */
/* 1. SW values are ANDed with HW signals. 
 2. For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input). 
 3. ds_bcrl_rtc_reset_b is not controlled by SW and is assigned to mix_reset_b_hw, i.e. assign ds_bcrl_rtc_reset_b = mix_reset_b_hw. 
 4. Watchdog flow and well described in the Watchdog Application-Note. 
 5. MIX_RESET_B and RTC_NOC_IOB_RESET_B will be cleared automatically by HW after 2 cycles (XINW) - pulse signals. */
#define PWRC_RTC_SW_RSTC_CLR      0x18843070

/* PWRC_RTC_SW_RSTC_CLR.CORE_RESET_B - Core reset output for CLKC */
#define PWRC_RTC_SW_RSTC_CLR__CORE_RESET_B__SHIFT       0
#define PWRC_RTC_SW_RSTC_CLR__CORE_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__CORE_RESET_B__MASK        0x00000001
#define PWRC_RTC_SW_RSTC_CLR__CORE_RESET_B__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_SW_RSTC_CLR__CORE_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.A7_WARM_RESET_B - Warm Reset for A7. 
 M3 can read RST_STS register within the CLKC, in order to know that reset (+dram refresh procedure) has been completed. */
#define PWRC_RTC_SW_RSTC_CLR__A7_WARM_RESET_B__SHIFT       1
#define PWRC_RTC_SW_RSTC_CLR__A7_WARM_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__A7_WARM_RESET_B__MASK        0x00000002
#define PWRC_RTC_SW_RSTC_CLR__A7_WARM_RESET_B__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_SW_RSTC_CLR__A7_WARM_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.MIX_RESET_B - Reset for internal PWRC block. 
 Reset will be cleared automatically by HW after 2 cycles (XINW) - pulse signal. */
#define PWRC_RTC_SW_RSTC_CLR__MIX_RESET_B__SHIFT       2
#define PWRC_RTC_SW_RSTC_CLR__MIX_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__MIX_RESET_B__MASK        0x00000004
#define PWRC_RTC_SW_RSTC_CLR__MIX_RESET_B__INV_MASK    0xFFFFFFFB
#define PWRC_RTC_SW_RSTC_CLR__MIX_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.SYSRTC_RESET_B - Reset for System RTC */
#define PWRC_RTC_SW_RSTC_CLR__SYSRTC_RESET_B__SHIFT       3
#define PWRC_RTC_SW_RSTC_CLR__SYSRTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__SYSRTC_RESET_B__MASK        0x00000008
#define PWRC_RTC_SW_RSTC_CLR__SYSRTC_RESET_B__INV_MASK    0xFFFFFFF7
#define PWRC_RTC_SW_RSTC_CLR__SYSRTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_POR_RESET_B - Reset for Cortex-M3 (PORRESETn) 
 Reset at power-up, full system reset (Cold reset). */
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B__SHIFT       4
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B__MASK        0x00000010
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_SYS_RESET_B - Reset for Cortex-M3 (SYSRESETn) 
 Resets the processor only, excluding debug logic (Warm reset). */
#define PWRC_RTC_SW_RSTC_CLR__M3_SYS_RESET_B__SHIFT       5
#define PWRC_RTC_SW_RSTC_CLR__M3_SYS_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_SYS_RESET_B__MASK        0x00000020
#define PWRC_RTC_SW_RSTC_CLR__M3_SYS_RESET_B__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_SW_RSTC_CLR__M3_SYS_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_DAP_RESET_B - Reset for Cortex-M3 (DAPRESETn) 
 Resets the bus interface connecting between DP and AP (AHB-AP) */
#define PWRC_RTC_SW_RSTC_CLR__M3_DAP_RESET_B__SHIFT       6
#define PWRC_RTC_SW_RSTC_CLR__M3_DAP_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_DAP_RESET_B__MASK        0x00000040
#define PWRC_RTC_SW_RSTC_CLR__M3_DAP_RESET_B__INV_MASK    0xFFFFFFBF
#define PWRC_RTC_SW_RSTC_CLR__M3_DAP_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_CTI_RESET_B - Reset for Cortex-M3 (CTIRESETn) 
 Resets CTI and CTI wrapper */
#define PWRC_RTC_SW_RSTC_CLR__M3_CTI_RESET_B__SHIFT       7
#define PWRC_RTC_SW_RSTC_CLR__M3_CTI_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_CTI_RESET_B__MASK        0x00000080
#define PWRC_RTC_SW_RSTC_CLR__M3_CTI_RESET_B__INV_MASK    0xFFFFFF7F
#define PWRC_RTC_SW_RSTC_CLR__M3_CTI_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.CAN0_RESET_B - Reset for CAN0 */
#define PWRC_RTC_SW_RSTC_CLR__CAN0_RESET_B__SHIFT       8
#define PWRC_RTC_SW_RSTC_CLR__CAN0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__CAN0_RESET_B__MASK        0x00000100
#define PWRC_RTC_SW_RSTC_CLR__CAN0_RESET_B__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_SW_RSTC_CLR__CAN0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.SPI0_RESET_B - Reset for SPI0 (QSPI) */
#define PWRC_RTC_SW_RSTC_CLR__SPI0_RESET_B__SHIFT       9
#define PWRC_RTC_SW_RSTC_CLR__SPI0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__SPI0_RESET_B__MASK        0x00000200
#define PWRC_RTC_SW_RSTC_CLR__SPI0_RESET_B__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_SW_RSTC_CLR__SPI0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.RTC_SECURITY_RESET_B - Reset for RTC_SECURITY */
#define PWRC_RTC_SW_RSTC_CLR__RTC_SECURITY_RESET_B__SHIFT       10
#define PWRC_RTC_SW_RSTC_CLR__RTC_SECURITY_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__RTC_SECURITY_RESET_B__MASK        0x00000400
#define PWRC_RTC_SW_RSTC_CLR__RTC_SECURITY_RESET_B__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_SW_RSTC_CLR__RTC_SECURITY_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.IO_RTC_RESET_B - Reset for IO_RTC */
#define PWRC_RTC_SW_RSTC_CLR__IO_RTC_RESET_B__SHIFT       11
#define PWRC_RTC_SW_RSTC_CLR__IO_RTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__IO_RTC_RESET_B__MASK        0x00000800
#define PWRC_RTC_SW_RSTC_CLR__IO_RTC_RESET_B__INV_MASK    0xFFFFF7FF
#define PWRC_RTC_SW_RSTC_CLR__IO_RTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.GPIO_RTC_RESET_B - Reset for GPIO_RTC */
#define PWRC_RTC_SW_RSTC_CLR__GPIO_RTC_RESET_B__SHIFT       12
#define PWRC_RTC_SW_RSTC_CLR__GPIO_RTC_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__GPIO_RTC_RESET_B__MASK        0x00001000
#define PWRC_RTC_SW_RSTC_CLR__GPIO_RTC_RESET_B__INV_MASK    0xFFFFEFFF
#define PWRC_RTC_SW_RSTC_CLR__GPIO_RTC_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.RTC_NOC_IOB_RESET_B - Reset for RTC NoC and IO-Bridge. 
 Reset will be cleared automatically by HW after 2 cycles (XINW) - pulse signal. */
#define PWRC_RTC_SW_RSTC_CLR__RTC_NOC_IOB_RESET_B__SHIFT       13
#define PWRC_RTC_SW_RSTC_CLR__RTC_NOC_IOB_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__RTC_NOC_IOB_RESET_B__MASK        0x00002000
#define PWRC_RTC_SW_RSTC_CLR__RTC_NOC_IOB_RESET_B__INV_MASK    0xFFFFDFFF
#define PWRC_RTC_SW_RSTC_CLR__RTC_NOC_IOB_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.CSSI_RST_RTCM_B - Reset for CSSI_RTC */
#define PWRC_RTC_SW_RSTC_CLR__CSSI_RST_RTCM_B__SHIFT       14
#define PWRC_RTC_SW_RSTC_CLR__CSSI_RST_RTCM_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__CSSI_RST_RTCM_B__MASK        0x00004000
#define PWRC_RTC_SW_RSTC_CLR__CSSI_RST_RTCM_B__INV_MASK    0xFFFFBFFF
#define PWRC_RTC_SW_RSTC_CLR__CSSI_RST_RTCM_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.CAN_LIST0_RESET_B - Reset for CAN Listener #0 */
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST0_RESET_B__SHIFT       15
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST0_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST0_RESET_B__MASK        0x00008000
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST0_RESET_B__INV_MASK    0xFFFF7FFF
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST0_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.CAN_LIST1_RESET_B - Reset for CAN Listener #1 */
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST1_RESET_B__SHIFT       16
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST1_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST1_RESET_B__MASK        0x00010000
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST1_RESET_B__INV_MASK    0xFFFEFFFF
#define PWRC_RTC_SW_RSTC_CLR__CAN_LIST1_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.REBOOT_B - X_REBOOT_B input */
#define PWRC_RTC_SW_RSTC_CLR__REBOOT_B__SHIFT       17
#define PWRC_RTC_SW_RSTC_CLR__REBOOT_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__REBOOT_B__MASK        0x00020000
#define PWRC_RTC_SW_RSTC_CLR__REBOOT_B__INV_MASK    0xFFFDFFFF
#define PWRC_RTC_SW_RSTC_CLR__REBOOT_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.RTC_RST_B - X_RTC_RST_B input */
#define PWRC_RTC_SW_RSTC_CLR__RTC_RST_B__SHIFT       18
#define PWRC_RTC_SW_RSTC_CLR__RTC_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__RTC_RST_B__MASK        0x00040000
#define PWRC_RTC_SW_RSTC_CLR__RTC_RST_B__INV_MASK    0xFFFBFFFF
#define PWRC_RTC_SW_RSTC_CLR__RTC_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.RESET_B - X_RESET_B input */
#define PWRC_RTC_SW_RSTC_CLR__RESET_B__SHIFT       19
#define PWRC_RTC_SW_RSTC_CLR__RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__RESET_B__MASK        0x00080000
#define PWRC_RTC_SW_RSTC_CLR__RESET_B__INV_MASK    0xFFF7FFFF
#define PWRC_RTC_SW_RSTC_CLR__RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_TIMER_RESET_B - M3 OsTimer reset */
#define PWRC_RTC_SW_RSTC_CLR__M3_TIMER_RESET_B__SHIFT       20
#define PWRC_RTC_SW_RSTC_CLR__M3_TIMER_RESET_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_TIMER_RESET_B__MASK        0x00100000
#define PWRC_RTC_SW_RSTC_CLR__M3_TIMER_RESET_B__INV_MASK    0xFFEFFFFF
#define PWRC_RTC_SW_RSTC_CLR__M3_TIMER_RESET_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_WD_RST_B - M3 Watchdog reset, OsTimer based */
#define PWRC_RTC_SW_RSTC_CLR__M3_WD_RST_B__SHIFT       21
#define PWRC_RTC_SW_RSTC_CLR__M3_WD_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_WD_RST_B__MASK        0x00200000
#define PWRC_RTC_SW_RSTC_CLR__M3_WD_RST_B__INV_MASK    0xFFDFFFFF
#define PWRC_RTC_SW_RSTC_CLR__M3_WD_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.A7_WD_HW_RST_B - A7 Watchdog reset, OsTimer based */
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_HW_RST_B__SHIFT       22
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_HW_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_HW_RST_B__MASK        0x00400000
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_HW_RST_B__INV_MASK    0xFFBFFFFF
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_HW_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.A7_WD_SW_RST_B - A7 Watchdog reset, triggered by SW */
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_SW_RST_B__SHIFT       23
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_SW_RST_B__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_SW_RST_B__MASK        0x00800000
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_SW_RST_B__INV_MASK    0xFF7FFFFF
#define PWRC_RTC_SW_RSTC_CLR__A7_WD_SW_RST_B__HW_DEFAULT  0x1

/* PWRC_RTC_SW_RSTC_CLR.M3_POR_RESET_B_AS - Reset for Cortex-M3 (PORRESETn) with auto-SET after 2 cycles of XINW. 
 Reset at power-up, full system reset (Cold reset). */
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B_AS__SHIFT       31
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B_AS__WIDTH       1
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B_AS__MASK        0x80000000
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B_AS__INV_MASK    0x7FFFFFFF
#define PWRC_RTC_SW_RSTC_CLR__M3_POR_RESET_B_AS__HW_DEFAULT  0x1

/* PWRC SW control for power supplies SET Register */
/* SW values are ORed with HW signals. 
 For each bit, writing 0 has no impact and writing 1 will set this register correspondingly (reg_value = reg_value | input) */
#define PWRC_POWER_SW_CTRL_SET    0x18843074

/* PWRC_POWER_SW_CTRL_SET.CORE_ON - X_CORE_ON output */
#define PWRC_POWER_SW_CTRL_SET__CORE_ON__SHIFT       0
#define PWRC_POWER_SW_CTRL_SET__CORE_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_SET__CORE_ON__MASK        0x00000001
#define PWRC_POWER_SW_CTRL_SET__CORE_ON__INV_MASK    0xFFFFFFFE
#define PWRC_POWER_SW_CTRL_SET__CORE_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_SET.MEM_ON - X_MEM_ON output */
#define PWRC_POWER_SW_CTRL_SET__MEM_ON__SHIFT       1
#define PWRC_POWER_SW_CTRL_SET__MEM_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_SET__MEM_ON__MASK        0x00000002
#define PWRC_POWER_SW_CTRL_SET__MEM_ON__INV_MASK    0xFFFFFFFD
#define PWRC_POWER_SW_CTRL_SET__MEM_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_SET.IO_ON - X_IO_ON output */
#define PWRC_POWER_SW_CTRL_SET__IO_ON__SHIFT       2
#define PWRC_POWER_SW_CTRL_SET__IO_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_SET__IO_ON__MASK        0x00000004
#define PWRC_POWER_SW_CTRL_SET__IO_ON__INV_MASK    0xFFFFFFFB
#define PWRC_POWER_SW_CTRL_SET__IO_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_SET.M3_ON - Output to internal M3 power switch, and to external SPI NOR device (Automotive) */
#define PWRC_POWER_SW_CTRL_SET__M3_ON__SHIFT       3
#define PWRC_POWER_SW_CTRL_SET__M3_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_SET__M3_ON__MASK        0x00000008
#define PWRC_POWER_SW_CTRL_SET__M3_ON__INV_MASK    0xFFFFFFF7
#define PWRC_POWER_SW_CTRL_SET__M3_ON__HW_DEFAULT  0x0

/* PWRC SW control for power supplies CLR Register */
/* SW values are ORed with HW signals. 
 For each bit, writing 0 has no impact and writing 1 will clear this register correspondingly (reg_value = reg_value &amp; ~input) */
#define PWRC_POWER_SW_CTRL_CLR    0x18843078

/* PWRC_POWER_SW_CTRL_CLR.CORE_ON - X_CORE_ON output */
#define PWRC_POWER_SW_CTRL_CLR__CORE_ON__SHIFT       0
#define PWRC_POWER_SW_CTRL_CLR__CORE_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_CLR__CORE_ON__MASK        0x00000001
#define PWRC_POWER_SW_CTRL_CLR__CORE_ON__INV_MASK    0xFFFFFFFE
#define PWRC_POWER_SW_CTRL_CLR__CORE_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_CLR.MEM_ON - X_MEM_ON output */
#define PWRC_POWER_SW_CTRL_CLR__MEM_ON__SHIFT       1
#define PWRC_POWER_SW_CTRL_CLR__MEM_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_CLR__MEM_ON__MASK        0x00000002
#define PWRC_POWER_SW_CTRL_CLR__MEM_ON__INV_MASK    0xFFFFFFFD
#define PWRC_POWER_SW_CTRL_CLR__MEM_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_CLR.IO_ON - X_IO_ON output */
#define PWRC_POWER_SW_CTRL_CLR__IO_ON__SHIFT       2
#define PWRC_POWER_SW_CTRL_CLR__IO_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_CLR__IO_ON__MASK        0x00000004
#define PWRC_POWER_SW_CTRL_CLR__IO_ON__INV_MASK    0xFFFFFFFB
#define PWRC_POWER_SW_CTRL_CLR__IO_ON__HW_DEFAULT  0x0

/* PWRC_POWER_SW_CTRL_CLR.M3_ON - Output to internal M3 power switch, and to external SPI NOR device (Automotive) */
#define PWRC_POWER_SW_CTRL_CLR__M3_ON__SHIFT       3
#define PWRC_POWER_SW_CTRL_CLR__M3_ON__WIDTH       1
#define PWRC_POWER_SW_CTRL_CLR__M3_ON__MASK        0x00000008
#define PWRC_POWER_SW_CTRL_CLR__M3_ON__INV_MASK    0xFFFFFFF7
#define PWRC_POWER_SW_CTRL_CLR__M3_ON__HW_DEFAULT  0x0

/* PWRC RTC DCOG Value Register */
/* RTC Delay-Configurations register, affects power-on and power-off sequences. 
 This register is W1, i.e. can be written by SW only once after Coldboot. */
#define PWRC_RTC_DCOG             0x1884307C

/* PWRC_RTC_DCOG.T1_DELAY - T1 delay time */
/* 0x0 :  4 ms */
/* 0x1 :  5 ms */
/* 0x2 :  6 ms */
/* 0x3 :  7 ms */
/* 0x4 :  8 ms */
/* 0x5 :  9 ms */
/* 0x6 : 10 ms */
/* 0x7 : 11 ms */
/* 0x8 : 12 ms */
/* 0x9 : 13 ms */
/* 0xa : 14 ms */
/* 0xb : 15 ms */
/* 0xc : 16 ms */
/* 0xd :  1 ms */
/* 0xe :  2 ms */
/* 0xf :  3 ms */
#define PWRC_RTC_DCOG__T1_DELAY__SHIFT       0
#define PWRC_RTC_DCOG__T1_DELAY__WIDTH       4
#define PWRC_RTC_DCOG__T1_DELAY__MASK        0x0000000F
#define PWRC_RTC_DCOG__T1_DELAY__INV_MASK    0xFFFFFFF0
#define PWRC_RTC_DCOG__T1_DELAY__HW_DEFAULT  0x0

/* PWRC_RTC_DCOG.T2_DELAY - T2 delay time */
/* 0x0 : 12 ms */
/* 0x1 : 14 ms */
/* 0x2 : 16 ms */
/* 0x3 : 18 ms */
/* 0x4 : 20 ms */
/* 0x5 : 22 ms */
/* 0x6 : 24 ms */
/* 0x7 : 26 ms */
/* 0x8 : 28 ms */
/* 0x9 : 30 ms */
/* 0xa : 32 ms */
/* 0xb :  2 ms */
/* 0xc :  4 ms */
/* 0xd :  6 ms */
/* 0xe :  8 ms */
/* 0xf : 10 ms */
#define PWRC_RTC_DCOG__T2_DELAY__SHIFT       4
#define PWRC_RTC_DCOG__T2_DELAY__WIDTH       4
#define PWRC_RTC_DCOG__T2_DELAY__MASK        0x000000F0
#define PWRC_RTC_DCOG__T2_DELAY__INV_MASK    0xFFFFFF0F
#define PWRC_RTC_DCOG__T2_DELAY__HW_DEFAULT  0x0

/* PWRC_RTC_DCOG.SHUTDOWN_MODE - System shutdown feature controlling for X_ON_KEY_B/X_EXT_ON boot watch dog and X_ON_KEY_B force shutdown */
/* 0 : Feature of X_ON_KEY_B/X_EXT_ON boot watch dog and X_ON_KEY_B force shutdown is disabled */
/* 1 : Feature of X_ON_KEY_B/X_EXT_ON boot watch dog and X_ON_KEY_B force shutdown is enabled */
#define PWRC_RTC_DCOG__SHUTDOWN_MODE__SHIFT       10
#define PWRC_RTC_DCOG__SHUTDOWN_MODE__WIDTH       1
#define PWRC_RTC_DCOG__SHUTDOWN_MODE__MASK        0x00000400
#define PWRC_RTC_DCOG__SHUTDOWN_MODE__INV_MASK    0xFFFFFBFF
#define PWRC_RTC_DCOG__SHUTDOWN_MODE__HW_DEFAULT  0x0

/* PWRC Control over M3 memories */
/* This register allows SW to control the M3 memories power-state. 
 Memory can only be accesses (R/W) upon Normal state. Data is retained in all power-states, except Shut-down. Wake-up time: Light-Sleep < Deep-Sleep < Shut-Down */
#define PWRC_M3_MEMORIES          0x18843080

/* PWRC_M3_MEMORIES.M3_BANK0_ACT - Cortex-M3 Bank 0 mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK0_ACT__SHIFT       0
#define PWRC_M3_MEMORIES__M3_BANK0_ACT__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK0_ACT__MASK        0x00000003
#define PWRC_M3_MEMORIES__M3_BANK0_ACT__INV_MASK    0xFFFFFFFC
#define PWRC_M3_MEMORIES__M3_BANK0_ACT__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK0_HIB - Cortex-M3 Bank 0 mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK0_HIB__SHIFT       2
#define PWRC_M3_MEMORIES__M3_BANK0_HIB__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK0_HIB__MASK        0x0000000C
#define PWRC_M3_MEMORIES__M3_BANK0_HIB__INV_MASK    0xFFFFFFF3
#define PWRC_M3_MEMORIES__M3_BANK0_HIB__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK1_ACT - Cortex-M3 Bank 1 mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK1_ACT__SHIFT       4
#define PWRC_M3_MEMORIES__M3_BANK1_ACT__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK1_ACT__MASK        0x00000030
#define PWRC_M3_MEMORIES__M3_BANK1_ACT__INV_MASK    0xFFFFFFCF
#define PWRC_M3_MEMORIES__M3_BANK1_ACT__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK1_HIB - Cortex-M3 Bank 1 mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK1_HIB__SHIFT       6
#define PWRC_M3_MEMORIES__M3_BANK1_HIB__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK1_HIB__MASK        0x000000C0
#define PWRC_M3_MEMORIES__M3_BANK1_HIB__INV_MASK    0xFFFFFF3F
#define PWRC_M3_MEMORIES__M3_BANK1_HIB__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK2_ACT - Cortex-M3 Bank 2 mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK2_ACT__SHIFT       8
#define PWRC_M3_MEMORIES__M3_BANK2_ACT__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK2_ACT__MASK        0x00000300
#define PWRC_M3_MEMORIES__M3_BANK2_ACT__INV_MASK    0xFFFFFCFF
#define PWRC_M3_MEMORIES__M3_BANK2_ACT__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK2_HIB - Cortex-M3 Bank 2 mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK2_HIB__SHIFT       10
#define PWRC_M3_MEMORIES__M3_BANK2_HIB__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK2_HIB__MASK        0x00000C00
#define PWRC_M3_MEMORIES__M3_BANK2_HIB__INV_MASK    0xFFFFF3FF
#define PWRC_M3_MEMORIES__M3_BANK2_HIB__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK3_ACT - Cortex-M3 Bank 3 mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK3_ACT__SHIFT       12
#define PWRC_M3_MEMORIES__M3_BANK3_ACT__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK3_ACT__MASK        0x00003000
#define PWRC_M3_MEMORIES__M3_BANK3_ACT__INV_MASK    0xFFFFCFFF
#define PWRC_M3_MEMORIES__M3_BANK3_ACT__HW_DEFAULT  0x0

/* PWRC_M3_MEMORIES.M3_BANK3_HIB - Cortex-M3 Bank 3 mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_M3_MEMORIES__M3_BANK3_HIB__SHIFT       14
#define PWRC_M3_MEMORIES__M3_BANK3_HIB__WIDTH       2
#define PWRC_M3_MEMORIES__M3_BANK3_HIB__MASK        0x0000C000
#define PWRC_M3_MEMORIES__M3_BANK3_HIB__INV_MASK    0xFFFF3FFF
#define PWRC_M3_MEMORIES__M3_BANK3_HIB__HW_DEFAULT  0x0

/* PWRC Control over CAN0 memory */
/* This register allows SW to control the CAN0 memory power-state. 
 Memory can only be accesses (R/W) upon Normal state. Data is retained in all power-states, except Shut-down. Wake-up time: Light-Sleep < Deep-Sleep < Shut-Down */
#define PWRC_CAN_MEMORY           0x18843084

/* PWRC_CAN_MEMORY.CAN0_ACT - CAN0 memory mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_CAN_MEMORY__CAN0_ACT__SHIFT       0
#define PWRC_CAN_MEMORY__CAN0_ACT__WIDTH       2
#define PWRC_CAN_MEMORY__CAN0_ACT__MASK        0x00000003
#define PWRC_CAN_MEMORY__CAN0_ACT__INV_MASK    0xFFFFFFFC
#define PWRC_CAN_MEMORY__CAN0_ACT__HW_DEFAULT  0x0

/* PWRC_CAN_MEMORY.CAN0_HIB - CAN0 memory mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_CAN_MEMORY__CAN0_HIB__SHIFT       2
#define PWRC_CAN_MEMORY__CAN0_HIB__WIDTH       2
#define PWRC_CAN_MEMORY__CAN0_HIB__MASK        0x0000000C
#define PWRC_CAN_MEMORY__CAN0_HIB__INV_MASK    0xFFFFFFF3
#define PWRC_CAN_MEMORY__CAN0_HIB__HW_DEFAULT  0x0

/* PWRC Control over RTC GNSS memory */
/* This register allows SW to control the RTC GNSS memory power-state. 
 Memory can only be accesses (R/W) upon Normal state. Data is retained in all power-states, except Shut-down. Wake-up time: Light-Sleep < Deep-Sleep < Shut-Down */
#define PWRC_RTC_GNSS_MEMORY      0x18843088

/* PWRC_RTC_GNSS_MEMORY.RTC_GNSS_ACT - RTC GNSS memory mode, when systen is NOT in PSAVING_MODE (i.e. SW is available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_ACT__SHIFT       0
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_ACT__WIDTH       2
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_ACT__MASK        0x00000003
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_ACT__INV_MASK    0xFFFFFFFC
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_ACT__HW_DEFAULT  0x0

/* PWRC_RTC_GNSS_MEMORY.RTC_GNSS_HIB - RTC GNSS memory mode, when systen is in PSAVING_MODE (i.e. SW is not available) */
/* 0 : Normal mode */
/* 1 : Light sleep mode */
/* 2 : Deep sleep mode */
/* 3 : Shut Down mode */
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_HIB__SHIFT       2
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_HIB__WIDTH       2
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_HIB__MASK        0x0000000C
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_HIB__INV_MASK    0xFFFFFFF3
#define PWRC_RTC_GNSS_MEMORY__RTC_GNSS_HIB__HW_DEFAULT  0x0

/* PWRC clock control register for M3 */
/* Clock control over M3 clocks */
#define PWRC_M3_CLK_EN            0x1884308C

/* PWRC_M3_CLK_EN.M3_FCLK_CLK_SEL - Clock selection for M3 FCLK. 
 FCLK is a free-funning clock, used by both the processor and its satellite debug features */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_M3_CLK_EN__M3_FCLK_CLK_SEL__SHIFT       3
#define PWRC_M3_CLK_EN__M3_FCLK_CLK_SEL__WIDTH       1
#define PWRC_M3_CLK_EN__M3_FCLK_CLK_SEL__MASK        0x00000008
#define PWRC_M3_CLK_EN__M3_FCLK_CLK_SEL__INV_MASK    0xFFFFFFF7
#define PWRC_M3_CLK_EN__M3_FCLK_CLK_SEL__HW_DEFAULT  0x0

/* PWRC_M3_CLK_EN.M3_HCLK_EN - Clock mux for M3 HCLK. 
 HCLK is a system clock, and must be the same as FCLK when active. 
 It might be gated-off when processor is in sleep mode, unless debugger is on.  */
/* 0 : M3_HCLK in non-gated (equals M3_FCLK) */
/* 1 : M3_HCLK is gated according to SLEEPING */
/* 2 : M3_HCLK is gated according to SLEEPDEEP */
/* 3 : M3_HCLK is gated according to SLEEPING or SLEEPDEEP */
#define PWRC_M3_CLK_EN__M3_HCLK_EN__SHIFT       4
#define PWRC_M3_CLK_EN__M3_HCLK_EN__WIDTH       2
#define PWRC_M3_CLK_EN__M3_HCLK_EN__MASK        0x00000030
#define PWRC_M3_CLK_EN__M3_HCLK_EN__INV_MASK    0xFFFFFFCF
#define PWRC_M3_CLK_EN__M3_HCLK_EN__HW_DEFAULT  0x0

/* PWRC_M3_CLK_EN.M3_DAP_CLK_EN - Clock enable for M3 DAPCLK */
/* 0 : Clock is disabled */
/* 1 : Clock is enabled */
#define PWRC_M3_CLK_EN__M3_DAP_CLK_EN__SHIFT       8
#define PWRC_M3_CLK_EN__M3_DAP_CLK_EN__WIDTH       1
#define PWRC_M3_CLK_EN__M3_DAP_CLK_EN__MASK        0x00000100
#define PWRC_M3_CLK_EN__M3_DAP_CLK_EN__INV_MASK    0xFFFFFEFF
#define PWRC_M3_CLK_EN__M3_DAP_CLK_EN__HW_DEFAULT  0x1

/* PWRC_M3_CLK_EN.M3_DAP_CLK_SEL - Clock mux for M3 DAPCLK. 
 DAP_CLK is a debug clock, used for the debug-bus from the DP (e.g. SWJ-DP). */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_M3_CLK_EN__M3_DAP_CLK_SEL__SHIFT       11
#define PWRC_M3_CLK_EN__M3_DAP_CLK_SEL__WIDTH       1
#define PWRC_M3_CLK_EN__M3_DAP_CLK_SEL__MASK        0x00000800
#define PWRC_M3_CLK_EN__M3_DAP_CLK_SEL__INV_MASK    0xFFFFF7FF
#define PWRC_M3_CLK_EN__M3_DAP_CLK_SEL__HW_DEFAULT  0x0

/* PWRC_M3_CLK_EN.M3_CTI_CLK_EN - Clock enable for M3 CTICLK */
/* 0 : Clock is disabled */
/* 1 : Clock is enabled */
#define PWRC_M3_CLK_EN__M3_CTI_CLK_EN__SHIFT       12
#define PWRC_M3_CLK_EN__M3_CTI_CLK_EN__WIDTH       1
#define PWRC_M3_CLK_EN__M3_CTI_CLK_EN__MASK        0x00001000
#define PWRC_M3_CLK_EN__M3_CTI_CLK_EN__INV_MASK    0xFFFFEFFF
#define PWRC_M3_CLK_EN__M3_CTI_CLK_EN__HW_DEFAULT  0x1

/* PWRC_M3_CLK_EN.M3_CTI_CLK_SEL - Clock mux for M3_CTICLK/ 
 CTI_CLK is a clock for CTI, and must be the same as FCLK when active. 
 It might be gated-off when CTI is not in use (i.e. when debugger is off). */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_M3_CLK_EN__M3_CTI_CLK_SEL__SHIFT       15
#define PWRC_M3_CLK_EN__M3_CTI_CLK_SEL__WIDTH       1
#define PWRC_M3_CLK_EN__M3_CTI_CLK_SEL__MASK        0x00008000
#define PWRC_M3_CLK_EN__M3_CTI_CLK_SEL__INV_MASK    0xFFFF7FFF
#define PWRC_M3_CLK_EN__M3_CTI_CLK_SEL__HW_DEFAULT  0x0

/* PWRC clock control register for CAN0 */
/* Clock control over CAN0 clock */
#define PWRC_CAN0_CLK_EN          0x18843090

/* PWRC_CAN0_CLK_EN.CAN0_CLK_EN - Clock enable for CAN0 */
/* 0 : Clock is disabled */
/* 1 : Clock is enabled */
#define PWRC_CAN0_CLK_EN__CAN0_CLK_EN__SHIFT       0
#define PWRC_CAN0_CLK_EN__CAN0_CLK_EN__WIDTH       1
#define PWRC_CAN0_CLK_EN__CAN0_CLK_EN__MASK        0x00000001
#define PWRC_CAN0_CLK_EN__CAN0_CLK_EN__INV_MASK    0xFFFFFFFE
#define PWRC_CAN0_CLK_EN__CAN0_CLK_EN__HW_DEFAULT  0x1

/* PWRC_CAN0_CLK_EN.CAN0_CLK_SEL - Clock mux for CAN0 */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_CAN0_CLK_EN__CAN0_CLK_SEL__SHIFT       1
#define PWRC_CAN0_CLK_EN__CAN0_CLK_SEL__WIDTH       1
#define PWRC_CAN0_CLK_EN__CAN0_CLK_SEL__MASK        0x00000002
#define PWRC_CAN0_CLK_EN__CAN0_CLK_SEL__INV_MASK    0xFFFFFFFD
#define PWRC_CAN0_CLK_EN__CAN0_CLK_SEL__HW_DEFAULT  0x1

/* PWRC clock control register for SPI0 */
/* Clock control over SPI0 clock */
#define PWRC_SPI0_CLK_EN          0x18843094

/* PWRC_SPI0_CLK_EN.SPI0_CLK_EN - Clock enable for SPI0 */
/* 0 : Clock is disabled */
/* 1 : Clock is enabled */
#define PWRC_SPI0_CLK_EN__SPI0_CLK_EN__SHIFT       0
#define PWRC_SPI0_CLK_EN__SPI0_CLK_EN__WIDTH       1
#define PWRC_SPI0_CLK_EN__SPI0_CLK_EN__MASK        0x00000001
#define PWRC_SPI0_CLK_EN__SPI0_CLK_EN__INV_MASK    0xFFFFFFFE
#define PWRC_SPI0_CLK_EN__SPI0_CLK_EN__HW_DEFAULT  0x1

/* PWRC_SPI0_CLK_EN.SPI0_CLK_SEL - Clock mux for SPI0 */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_SPI0_CLK_EN__SPI0_CLK_SEL__SHIFT       1
#define PWRC_SPI0_CLK_EN__SPI0_CLK_SEL__WIDTH       1
#define PWRC_SPI0_CLK_EN__SPI0_CLK_SEL__MASK        0x00000002
#define PWRC_SPI0_CLK_EN__SPI0_CLK_SEL__INV_MASK    0xFFFFFFFD
#define PWRC_SPI0_CLK_EN__SPI0_CLK_SEL__HW_DEFAULT  0x0

/* PWRC clock control register for RTC_SECURITY */
/* Clock control over RTC_SEC clock */
#define PWRC_RTC_SEC_CLK_EN       0x18843098

/* PWRC_RTC_SEC_CLK_EN.RTC_SEC_CLK_EN - Clock enable for RTC-Security */
/* 0 : Clock is disabled */
/* 1 : Clock is enabled */
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_EN__SHIFT       0
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_EN__WIDTH       1
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_EN__MASK        0x00000001
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_EN__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_EN__HW_DEFAULT  0x1

/* PWRC_RTC_SEC_CLK_EN.RTC_SEC_CLK_SEL - Clock mux for RTC-Security */
/* 0 : Fast clock is enabled */
/* 1 : Slow clock is enabled */
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_SEL__SHIFT       1
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_SEL__WIDTH       1
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_SEL__MASK        0x00000002
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_SEL__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_SEC_CLK_EN__RTC_SEC_CLK_SEL__HW_DEFAULT  0x0

/* PWRC Control over CAN Register */
/* This register allows controling the power-states of the external CAN transceivers. */
#define PWRC_RTC_CAN_TRANS_CTRL   0x1884309C

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_EN_ACT - Control external CAN transceiver EN pin (mapped on GPIO 2) when SW is up. */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_ACT__SHIFT       0
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_ACT__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_ACT__MASK        0x00000001
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_ACT__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_ACT__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_STB_N_ACT - Control external CAN transceiver STB_N pin (mapped on GPIO 3) when SW is up. */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_ACT__SHIFT       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_ACT__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_ACT__MASK        0x00000002
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_ACT__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_ACT__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_EN_HIB_PS - Control external CAN transceiver EN pin (mapped on GPIO 2) upon PSAVING_MODE state (hibernation). */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_PS__SHIFT       4
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_PS__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_PS__MASK        0x00000010
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_PS__INV_MASK    0xFFFFFFEF
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_PS__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_STB_N_HIB_PS - Control external CAN transceiver STB_N pin (mapped on GPIO 3) upon PSAVING_MODE state (hibernation). */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_PS__SHIFT       5
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_PS__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_PS__MASK        0x00000020
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_PS__INV_MASK    0xFFFFFFDF
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_PS__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_EN_HIB_CA - Control external CAN transceiver EN pin (mapped on GPIO 2) upon CAN_ACTIVE state (can activity has been detected, CAN0 and RTCPLL are up, no SW yet). */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_CA__SHIFT       8
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_CA__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_CA__MASK        0x00000100
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_CA__INV_MASK    0xFFFFFEFF
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_EN_HIB_CA__HW_DEFAULT  0x1

/* PWRC_RTC_CAN_TRANS_CTRL.EXT_CAN_STB_N_HIB_CA - Control external CAN transceiver STB_N pin (mapped on GPIO 3) upon CAN_ACTIVE state (can activity has been detected, CAN0 and RTCPLL are up, no SW yet). */
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_CA__SHIFT       9
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_CA__WIDTH       1
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_CA__MASK        0x00000200
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_CA__INV_MASK    0xFFFFFDFF
#define PWRC_RTC_CAN_TRANS_CTRL__EXT_CAN_STB_N_HIB_CA__HW_DEFAULT  0x1

/* PWRC M3 Watchdog Counter Register */
/* This register collects M3 watchdog statistics for software usage. 
 Note 1: this register is only resets upon hardware triggered mix_reset_b, and is not reset upon watchdog nor software-triggred mix_reset_b. 
 Note 2: Counter in this register does not wrap-around, i.e. it persists its maximal value when saturated. */
#define PWRC_M3_WD_CNTR           0x188430A0

/* PWRC_M3_WD_CNTR.WD_CNTR - M3 Watchdog counter, auto-increments (+1) by PWRC hardware upon M3 Hardware Watchdog event (m3_wd_rst_b assertion), might be cleared/set for any other value by software. */
#define PWRC_M3_WD_CNTR__WD_CNTR__SHIFT       0
#define PWRC_M3_WD_CNTR__WD_CNTR__WIDTH       4
#define PWRC_M3_WD_CNTR__WD_CNTR__MASK        0x0000000F
#define PWRC_M3_WD_CNTR__WD_CNTR__INV_MASK    0xFFFFFFF0
#define PWRC_M3_WD_CNTR__WD_CNTR__HW_DEFAULT  0x0

/* PWRC_M3_WD_CNTR.VLD - M3 Watchdog counter valid flag, auto-set by PWRC hardware upon M3 Hardware Watchdog event (m3_wd_rst_b assertion), might be cleared by software through writing zero value. */
#define PWRC_M3_WD_CNTR__VLD__SHIFT       4
#define PWRC_M3_WD_CNTR__VLD__WIDTH       1
#define PWRC_M3_WD_CNTR__VLD__MASK        0x00000010
#define PWRC_M3_WD_CNTR__VLD__INV_MASK    0xFFFFFFEF
#define PWRC_M3_WD_CNTR__VLD__HW_DEFAULT  0x0

/* PWRC A7 Watchdog Counter Register */
/* This register collects A7 watchdog statistics for software usage. 
 Note 1: this register is only resets upon hardware triggered mix_reset_b, and is not reset upon watchdog nor software-triggred mix_reset_b. 
 Note 2: Counter in this register does not wrap-around, i.e. it persists its maximal value when saturated. */
#define PWRC_A7_WD_CNTR           0x188430A4

/* PWRC_A7_WD_CNTR.WD_CNTR - A7 Watchdog counter, auto-increments (+1) by PWRC hardware upon A7 Hardware Watchdog event (a7_wd_hw_rst_b assertion), might be cleared/set for any other value by software. */
#define PWRC_A7_WD_CNTR__WD_CNTR__SHIFT       0
#define PWRC_A7_WD_CNTR__WD_CNTR__WIDTH       4
#define PWRC_A7_WD_CNTR__WD_CNTR__MASK        0x0000000F
#define PWRC_A7_WD_CNTR__WD_CNTR__INV_MASK    0xFFFFFFF0
#define PWRC_A7_WD_CNTR__WD_CNTR__HW_DEFAULT  0x0

/* PWRC_A7_WD_CNTR.VLD - A7 Watchdog counter valid flag, auto-set by PWRC hardware upon A7 Hardware Watchdog event (a7_wd_hw_rst_b assertion), might be cleared by software through writing zero value. */
#define PWRC_A7_WD_CNTR__VLD__SHIFT       4
#define PWRC_A7_WD_CNTR__VLD__WIDTH       1
#define PWRC_A7_WD_CNTR__VLD__MASK        0x00000010
#define PWRC_A7_WD_CNTR__VLD__INV_MASK    0xFFFFFFEF
#define PWRC_A7_WD_CNTR__VLD__HW_DEFAULT  0x0

/* PWRC RTC Write-Once Register */
/* This register monitors the status of the write-once (W1) registers in the PWRC XINW domain. */
#define PWRC_RTC_WRITE_ONE        0x188430A8

/* PWRC_RTC_WRITE_ONE.RTC_DCOG - Write-once status for register PWRC_RTC_DCOG */
/* 0 : Register has already been written (R/O now) */
/* 1 : Register hasn not been written yet (still open) */
#define PWRC_RTC_WRITE_ONE__RTC_DCOG__SHIFT       0
#define PWRC_RTC_WRITE_ONE__RTC_DCOG__WIDTH       1
#define PWRC_RTC_WRITE_ONE__RTC_DCOG__MASK        0x00000001
#define PWRC_RTC_WRITE_ONE__RTC_DCOG__INV_MASK    0xFFFFFFFE
#define PWRC_RTC_WRITE_ONE__RTC_DCOG__HW_DEFAULT  0x1

/* PWRC_RTC_WRITE_ONE.GNSS_CTRL_CFG - Write-once status for field GNSS_CFG in register PWRC_GNSS_CTRL */
/* 0 : Register has already been written (R/O now) */
/* 1 : Register hasn not been written yet (still open) */
#define PWRC_RTC_WRITE_ONE__GNSS_CTRL_CFG__SHIFT       1
#define PWRC_RTC_WRITE_ONE__GNSS_CTRL_CFG__WIDTH       1
#define PWRC_RTC_WRITE_ONE__GNSS_CTRL_CFG__MASK        0x00000002
#define PWRC_RTC_WRITE_ONE__GNSS_CTRL_CFG__INV_MASK    0xFFFFFFFD
#define PWRC_RTC_WRITE_ONE__GNSS_CTRL_CFG__HW_DEFAULT  0x1


#endif  // __PWRC_H__
