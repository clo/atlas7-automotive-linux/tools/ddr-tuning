/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_FC__GPIO_MEDIAM
#define _SOC_FC__GPIO_MEDIAM

#define _GPIO_MEDIAM_MODULE_BASE                 0x17040000

#define rGPIO_MEDIAM_CFG_LVDS_0                   (*(volatile unsigned *) 0x17040000U)
#define rGPIO_MEDIAM_CFG_LVDS_1                   (*(volatile unsigned *) 0x17040004U)
#define rGPIO_MEDIAM_CFG_LVDS_2                   (*(volatile unsigned *) 0x17040008U)
#define rGPIO_MEDIAM_CFG_LVDS_3                   (*(volatile unsigned *) 0x1704000CU)
#define rGPIO_MEDIAM_CFG_LVDS_4                   (*(volatile unsigned *) 0x17040010U)
#define rGPIO_MEDIAM_CFG_LVDS_5                   (*(volatile unsigned *) 0x17040014U)
#define rGPIO_MEDIAM_CFG_LVDS_6                   (*(volatile unsigned *) 0x17040018U)
#define rGPIO_MEDIAM_CFG_LVDS_7                   (*(volatile unsigned *) 0x1704001CU)
#define rGPIO_MEDIAM_CFG_LVDS_8                   (*(volatile unsigned *) 0x17040020U)
#define rGPIO_MEDIAM_CFG_LVDS_9                   (*(volatile unsigned *) 0x17040024U)
#define rGPIO_MEDIAM_STATUS_LVDS                  (*(volatile unsigned *) 0x1704008CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_0         (*(volatile unsigned *) 0x17040100U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_1         (*(volatile unsigned *) 0x17040104U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_2         (*(volatile unsigned *) 0x17040108U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_3         (*(volatile unsigned *) 0x1704010CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_4         (*(volatile unsigned *) 0x17040110U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_5         (*(volatile unsigned *) 0x17040114U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_6         (*(volatile unsigned *) 0x17040118U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_7         (*(volatile unsigned *) 0x1704011CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_8         (*(volatile unsigned *) 0x17040120U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_9         (*(volatile unsigned *) 0x17040124U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_10        (*(volatile unsigned *) 0x17040128U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_11        (*(volatile unsigned *) 0x1704012CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_12        (*(volatile unsigned *) 0x17040130U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_13        (*(volatile unsigned *) 0x17040134U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_14        (*(volatile unsigned *) 0x17040138U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_15        (*(volatile unsigned *) 0x1704013CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_16        (*(volatile unsigned *) 0x17040140U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_17        (*(volatile unsigned *) 0x17040144U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_18        (*(volatile unsigned *) 0x17040148U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_19        (*(volatile unsigned *) 0x1704014CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_20        (*(volatile unsigned *) 0x17040150U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_21        (*(volatile unsigned *) 0x17040154U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_22        (*(volatile unsigned *) 0x17040158U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_23        (*(volatile unsigned *) 0x1704015CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_24        (*(volatile unsigned *) 0x17040160U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_25        (*(volatile unsigned *) 0x17040164U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_26        (*(volatile unsigned *) 0x17040168U)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_27        (*(volatile unsigned *) 0x1704016CU)
#define rGPIO_MEDIAM_CFG_JTAG_UART_NAND_28        (*(volatile unsigned *) 0x17040170U)
#define rGPIO_MEDIAM_STATUS_JTAG_UART_NAND        (*(volatile unsigned *) 0x1704018CU)
#endif // _SOC_FC__GPIO_MEDIAM 

