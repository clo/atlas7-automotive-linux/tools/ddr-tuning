/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __NOCDDRM_H__
#define __NOCDDRM_H__



/* NOC_DDRM */
/* ==================================================================== */

/* upctl_data_T_main_Scheduler_Id_CoreId */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID 0x10820000

/* upctl_data_T_main_Scheduler_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_TYPE_ID__SHIFT    0
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_TYPE_ID__WIDTH    8
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_TYPE_ID__MASK     0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0x2

/* upctl_data_T_main_Scheduler_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_CHECKSUM__SHIFT     8
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_CHECKSUM__WIDTH     24
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_CHECKSUM__MASK      0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_CHECKSUM__INV_MASK  0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0x3A5035

/* upctl_data_T_main_Scheduler_Id_RevisionId */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID 0x10820004

/* upctl_data_T_main_Scheduler_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__USER_ID__SHIFT     0
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__USER_ID__WIDTH     8
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__USER_ID__MASK      0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__USER_ID__INV_MASK  0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__FLEX_NOC_ID__SHIFT    8
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__FLEX_NOC_ID__WIDTH    24
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__FLEX_NOC_ID__MASK     0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* upctl_data_T_main_Scheduler_DdrConf */
/* ddr configuration definition. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF 0x10820008

/* upctl_data_T_main_Scheduler_DdrConf.DdrConf - Selection of a configuration of mappings of address bits to memory device, bank, row, and column. <See SoC-specific DDR Conf documentation> */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF__DDR_CONF__SHIFT     0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF__DDR_CONF__WIDTH     3
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF__DDR_CONF__MASK      0x00000007
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF__DDR_CONF__INV_MASK  0xFFFFFFF8
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF__DDR_CONF__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_DdrTiming */
/* ddr timing definition. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING 0x1082000C

/* upctl_data_T_main_Scheduler_DdrTiming.ActToAct - The minimum number of scheduler clock cycles between two consecutive DRAM Activate commands on the same bank (tRC/ tCkG). tCkG is the clock period of the SoC DRAM scheduler. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__ACT_TO_ACT__SHIFT    0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__ACT_TO_ACT__WIDTH    6
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__ACT_TO_ACT__MASK     0x0000003F
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__ACT_TO_ACT__INV_MASK 0xFFFFFFC0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__ACT_TO_ACT__HW_DEFAULT 0x14

/* upctl_data_T_main_Scheduler_DdrTiming.RdToMiss - The minimum number of scheduler clock cycles between the last DRAM Read command and a new Read or Write command in another page of the same bank (tRTP + tRP + tRCD - BL x tCkD / 2). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_MISS__SHIFT    6
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_MISS__WIDTH    6
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_MISS__MASK     0x00000FC0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_MISS__INV_MASK 0xFFFFF03F
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_MISS__HW_DEFAULT 0xC

/* upctl_data_T_main_Scheduler_DdrTiming.WrToMiss - The minimum number of scheduler clock cycles between the last DRAM Write command and a new Read or Write command in another page of the same bank (WL x tCkD + tWR + tRP + tRCD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_MISS__SHIFT    12
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_MISS__WIDTH    6
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_MISS__MASK     0x0003F000
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_MISS__INV_MASK 0xFFFC0FFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_MISS__HW_DEFAULT 0x15

/* upctl_data_T_main_Scheduler_DdrTiming.BurstLen - The DRAM burst duration on the DRAM data bus in scheduler clock cycles. Also equal to scheduler clock cycles between two DRAM commands (BL / 2 x tCkD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BURST_LEN__SHIFT     18
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BURST_LEN__WIDTH     3
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BURST_LEN__MASK      0x001C0000
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BURST_LEN__INV_MASK  0xFFE3FFFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BURST_LEN__HW_DEFAULT 0x2

/* upctl_data_T_main_Scheduler_DdrTiming.RdToWr - The minimum number of scheduler clock cycles between the last DRAM Read command and a Write command (DDR2: 2 x tCkD, DDR3: (RL - WL + 2) x tCkD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_WR__SHIFT    21
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_WR__WIDTH    5
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_WR__MASK     0x03E00000
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_WR__INV_MASK 0xFC1FFFFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__RD_TO_WR__HW_DEFAULT 0x3

/* upctl_data_T_main_Scheduler_DdrTiming.WrToRd - The minimum number of scheduler clock cycles between the last DRAM Write command and a Read command (WL x tCkD + tWTR). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_RD__SHIFT    26
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_RD__WIDTH    5
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_RD__MASK     0x7C000000
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_RD__INV_MASK 0x83FFFFFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__WR_TO_RD__HW_DEFAULT 0xB

/* upctl_data_T_main_Scheduler_DdrTiming.BwRatio - When set to zero, one DRAM clock cycle (two DDR transfers) is used to transfer each word of data. When set to one, two DRAM clock cycles (four DDR transfers) are used to transfer each word of data. This is applicable when half of a DRAM data bus width is used. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BW_RATIO__SHIFT     31
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BW_RATIO__WIDTH     1
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BW_RATIO__MASK      0x80000000
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BW_RATIO__INV_MASK  0x7FFFFFFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_TIMING__BW_RATIO__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_DdrMode */
/* ddr mode definition. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE 0x10820010

/* upctl_data_T_main_Scheduler_DdrMode.AutoPrecharge - When set to one, pages are automatically closed after each access, when set to zero, pages are left opened until an access in a different page occurs */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__AUTO_PRECHARGE__SHIFT     0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__AUTO_PRECHARGE__WIDTH     1
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__AUTO_PRECHARGE__MASK      0x00000001
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__AUTO_PRECHARGE__INV_MASK  0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__AUTO_PRECHARGE__HW_DEFAULT 0x1

/* upctl_data_T_main_Scheduler_DdrMode.BwRatioExtended - When set to 1, support for 4x Bwratio. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__BW_RATIO_EXTENDED__SHIFT    1
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__BW_RATIO_EXTENDED__WIDTH    1
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__BW_RATIO_EXTENDED__MASK     0x00000002
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__BW_RATIO_EXTENDED__INV_MASK 0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_SCHEDULER_DDR_MODE__BW_RATIO_EXTENDED__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_ReadLatency */
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY 0x10820014

/* upctl_data_T_main_Scheduler_ReadLatency.ReadLatency - The DRAM type-specific number of cycles from a scheduler request to a protocol controller response. This is a fixed value depending on the type of DRAM memory. <See SoC-specific memory controller documentation>. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY__READ_LATENCY__SHIFT     0
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY__READ_LATENCY__WIDTH     8
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY__READ_LATENCY__MASK      0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY__READ_LATENCY__INV_MASK  0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_READ_LATENCY__READ_LATENCY__HW_DEFAULT 0xFF

/* upctl_data_T_main_Scheduler_Aging0 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0 0x10820018

/* upctl_data_T_main_Scheduler_Aging0.Aging0 - AGING slice threshold for port 0 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0__AGING0__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0__AGING0__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0__AGING0__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0__AGING0__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING0__AGING0__HW_DEFAULT  0x25

/* upctl_data_T_main_Scheduler_Aging1 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1 0x1082001C

/* upctl_data_T_main_Scheduler_Aging1.Aging1 - AGING slice threshold for port 1 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1__AGING1__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1__AGING1__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1__AGING1__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1__AGING1__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING1__AGING1__HW_DEFAULT  0x60

/* upctl_data_T_main_Scheduler_Aging2 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2 0x10820020

/* upctl_data_T_main_Scheduler_Aging2.Aging2 - AGING slice threshold for port 2 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2__AGING2__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2__AGING2__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2__AGING2__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2__AGING2__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING2__AGING2__HW_DEFAULT  0x20

/* upctl_data_T_main_Scheduler_Aging3 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3 0x10820024

/* upctl_data_T_main_Scheduler_Aging3.Aging3 - AGING slice threshold for port 3 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3__AGING3__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3__AGING3__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3__AGING3__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3__AGING3__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING3__AGING3__HW_DEFAULT  0x20

/* upctl_data_T_main_Scheduler_Aging4 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4 0x10820028

/* upctl_data_T_main_Scheduler_Aging4.Aging4 - AGING slice threshold for port 4 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4__AGING4__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4__AGING4__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4__AGING4__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4__AGING4__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING4__AGING4__HW_DEFAULT  0x30

/* upctl_data_T_main_Scheduler_Aging5 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5 0x1082002C

/* upctl_data_T_main_Scheduler_Aging5.Aging5 - AGING slice threshold for port 5 */
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5__AGING5__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5__AGING5__WIDTH       8
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5__AGING5__MASK        0x000000FF
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5__AGING5__INV_MASK    0xFFFFFF00
#define UPCTL_DATA_T_MAIN_SCHEDULER_AGING5__AGING5__HW_DEFAULT  0x40

/* upctl_data_T_main_Scheduler_Activate */
/* timing values concerning Activate commands, in Generic clock unit. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE 0x10820038

/* upctl_data_T_main_Scheduler_Activate.Rrd - 'The number of cycles between two consecutive Activate commands on different Banks of the same device (tRRD). */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__RRD__SHIFT       0
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__RRD__WIDTH       4
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__RRD__MASK        0x0000000F
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__RRD__INV_MASK    0xFFFFFFF0
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__RRD__HW_DEFAULT  0x3

/* upctl_data_T_main_Scheduler_Activate.Faw - The number of cycles for the four bank activate (FAW) period (tFAW). */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW__SHIFT       4
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW__WIDTH       6
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW__MASK        0x000003F0
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW__INV_MASK    0xFFFFFC0F
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW__HW_DEFAULT  0xC

/* upctl_data_T_main_Scheduler_Activate.FawBank - The number of Banks of a given device involved in the FAW period. Set to zero for 2-bank memories (WideIO). Set to one for memories with 4 banks or more (DDR). */
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW_BANK__SHIFT      10
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW_BANK__WIDTH      1
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW_BANK__MASK       0x00000400
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW_BANK__INV_MASK   0xFFFFFBFF
#define UPCTL_DATA_T_MAIN_SCHEDULER_ACTIVATE__FAW_BANK__HW_DEFAULT 0x1

/* upctl_data_T_main_Scheduler_DevToDev */
/* timing values concerning device to device data bus ownership change, in Generic clock unit. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV 0x1082003C

/* upctl_data_T_main_Scheduler_DevToDev.BusRdToRd - The number of cycles between the last read data of a device and the first read data of another device of a memory array with multiple ranks (tCkD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_RD__SHIFT  0
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_RD__WIDTH  2
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_RD__MASK   0x00000003
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_RD__INV_MASK 0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_RD__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_DevToDev.BusRdToWr - The number of cycles between the last read data of a device and the first write data to another device of a memory array with multiple ranks (2 x tCkD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_WR__SHIFT  2
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_WR__WIDTH  2
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_WR__MASK   0x0000000C
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_WR__INV_MASK 0xFFFFFFF3
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_RD_TO_WR__HW_DEFAULT 0x0

/* upctl_data_T_main_Scheduler_DevToDev.BusWrToRd - The number of cycles between the last write data to a device and the first read data of another device of a memory array with multiple ranks (2 x tCkD). tCkD is the DRAM clock period. */
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_WR_TO_RD__SHIFT  4
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_WR_TO_RD__WIDTH  2
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_WR_TO_RD__MASK   0x00000030
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_WR_TO_RD__INV_MASK 0xFFFFFFCF
#define UPCTL_DATA_T_MAIN_SCHEDULER_DEV_TO_DEV__BUS_WR_TO_RD__HW_DEFAULT 0x0

/* ddrm_observer_main_STPv2Converter_Id_CoreId */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID 0x10820080

/* ddrm_observer_main_STPv2Converter_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_TYPE_ID__SHIFT    0
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_TYPE_ID__WIDTH    8
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_TYPE_ID__MASK     0x000000FF
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0xE

/* ddrm_observer_main_STPv2Converter_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_CHECKSUM__SHIFT     8
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_CHECKSUM__WIDTH     24
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_CHECKSUM__MASK      0xFFFFFF00
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_CHECKSUM__INV_MASK  0x000000FF
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0x4C4023

/* ddrm_observer_main_STPv2Converter_Id_RevisionId */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID 0x10820084

/* ddrm_observer_main_STPv2Converter_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__USER_ID__SHIFT     0
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__USER_ID__WIDTH     8
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__USER_ID__MASK      0x000000FF
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__USER_ID__INV_MASK  0xFFFFFF00
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_observer_main_STPv2Converter_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__FLEX_NOC_ID__SHIFT    8
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__FLEX_NOC_ID__WIDTH    24
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__FLEX_NOC_ID__MASK     0xFFFFFF00
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_observer_main_STPv2Converter_AsyncPeriod */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD 0x10820088

/* ddrm_observer_main_STPv2Converter_AsyncPeriod.AsyncPeriod - Defines the period of ASYNC sequences. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD__ASYNC_PERIOD__SHIFT     0
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD__ASYNC_PERIOD__WIDTH     5
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD__ASYNC_PERIOD__MASK      0x0000001F
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD__ASYNC_PERIOD__INV_MASK  0xFFFFFFE0
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_ASYNC_PERIOD__ASYNC_PERIOD__HW_DEFAULT 0x0

/* ddrm_observer_main_STPv2Converter_STPV2En */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN 0x1082008C

/* ddrm_observer_main_STPv2Converter_STPV2En.STPV2En - Enables the output to STP, rstVal=1. */
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN__STPV2EN__SHIFT       0
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN__STPV2EN__WIDTH       1
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN__STPV2EN__MASK        0x00000001
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN__STPV2EN__INV_MASK    0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_STPV2CONVERTER_STPV2EN__STPV2EN__HW_DEFAULT  0x1

/* ddrm_observer_main_AtbEndPoint_Id_CoreId */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID 0x10820100

/* ddrm_observer_main_AtbEndPoint_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_TYPE_ID__SHIFT  0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_TYPE_ID__WIDTH  8
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_TYPE_ID__MASK   0x000000FF
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0x7

/* ddrm_observer_main_AtbEndPoint_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_CHECKSUM__SHIFT   8
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_CHECKSUM__WIDTH   24
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_CHECKSUM__MASK    0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0x2234CC

/* ddrm_observer_main_AtbEndPoint_Id_RevisionId */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID 0x10820104

/* ddrm_observer_main_AtbEndPoint_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__USER_ID__SHIFT   0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__USER_ID__WIDTH   8
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__USER_ID__MASK    0x000000FF
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_observer_main_AtbEndPoint_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__FLEX_NOC_ID__SHIFT  8
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__FLEX_NOC_ID__WIDTH  24
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__FLEX_NOC_ID__MASK   0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_observer_main_AtbEndPoint_AtbId */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID 0x10820108

/* ddrm_observer_main_AtbEndPoint_AtbId.AtbId - ATB AtId */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID__ATB_ID__SHIFT   0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID__ATB_ID__WIDTH   7
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID__ATB_ID__MASK    0x0000007F
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID__ATB_ID__INV_MASK 0xFFFFFF80
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_ID__ATB_ID__HW_DEFAULT 0x0

/* ddrm_observer_main_AtbEndPoint_AtbEn */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN 0x1082010C

/* ddrm_observer_main_AtbEndPoint_AtbEn.AtbEn - ATB Unit Enable */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN__ATB_EN__SHIFT   0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN__ATB_EN__WIDTH   1
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN__ATB_EN__MASK    0x00000001
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN__ATB_EN__INV_MASK 0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_ATB_EN__ATB_EN__HW_DEFAULT 0x0

/* ddrm_observer_main_AtbEndPoint_SyncPeriod */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD 0x10820110

/* ddrm_observer_main_AtbEndPoint_SyncPeriod.SyncPeriod - ATB Synchro Period */
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD__SYNC_PERIOD__SHIFT   0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD__SYNC_PERIOD__WIDTH   5
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD__SYNC_PERIOD__MASK    0x0000001F
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD__SYNC_PERIOD__INV_MASK 0xFFFFFFE0
#define DDRM_OBSERVER_MAIN_ATB_END_POINT_SYNC_PERIOD__SYNC_PERIOD__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_Id_CoreId */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID 0x10820180

/* ddrm_observer_main_ErrorLogger_0_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_TYPE_ID__SHIFT   0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_TYPE_ID__WIDTH   8
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_TYPE_ID__MASK    0x000000FF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0xD

/* ddrm_observer_main_ErrorLogger_0_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_CHECKSUM__SHIFT    8
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_CHECKSUM__WIDTH    24
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_CHECKSUM__MASK     0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0xC8546D

/* ddrm_observer_main_ErrorLogger_0_Id_RevisionId */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID 0x10820184

/* ddrm_observer_main_ErrorLogger_0_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__USER_ID__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__USER_ID__WIDTH    8
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__USER_ID__MASK     0x000000FF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__FLEX_NOC_ID__SHIFT   8
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__FLEX_NOC_ID__WIDTH   24
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__FLEX_NOC_ID__MASK    0xFFFFFF00
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_observer_main_ErrorLogger_0_FaultEn */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN 0x10820188

/* ddrm_observer_main_ErrorLogger_0_FaultEn.FaultEn - Set to 1 to enable output signal Fault. Fault asserted when ErrVld is 1. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN__FAULT_EN__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN__FAULT_EN__WIDTH    1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN__FAULT_EN__MASK     0x00000001
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN__FAULT_EN__INV_MASK 0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_FAULT_EN__FAULT_EN__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrVld */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD 0x1082018C

/* ddrm_observer_main_ErrorLogger_0_ErrVld.ErrVld - 1 indicates an error has been logged */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD__ERR_VLD__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD__ERR_VLD__WIDTH    1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD__ERR_VLD__MASK     0x00000001
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD__ERR_VLD__INV_MASK 0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_VLD__ERR_VLD__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrClr */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR 0x10820190

/* ddrm_observer_main_ErrorLogger_0_ErrClr.ErrClr - Set to 1 to clear ErrVld. NOTE The written value is not stored in ErrVld. A read always returns 0. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR__ERR_CLR__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR__ERR_CLR__WIDTH    1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR__ERR_CLR__MASK     0x00000001
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR__ERR_CLR__INV_MASK 0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_CLR__ERR_CLR__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog0 */
/* Stores NTTP packet header fields Lock, Opc, ErrCode, Len1 and indicates version of NTTP transport protocol */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0 0x10820194

/* ddrm_observer_main_ErrorLogger_0_ErrLog0.Lock - Lock */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LOCK__SHIFT     0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LOCK__WIDTH     1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LOCK__MASK      0x00000001
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LOCK__INV_MASK  0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LOCK__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog0.Opc - Opc */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__OPC__SHIFT     1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__OPC__WIDTH     4
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__OPC__MASK      0x0000001E
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__OPC__INV_MASK  0xFFFFFFE1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__OPC__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog0.ErrCode - ErrCode */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__ERR_CODE__SHIFT    8
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__ERR_CODE__WIDTH    3
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__ERR_CODE__MASK     0x00000700
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__ERR_CODE__INV_MASK 0xFFFFF8FF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__ERR_CODE__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog0.Len1 - Len1 */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LEN1__SHIFT     16
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LEN1__WIDTH     7
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LEN1__MASK      0x007F0000
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LEN1__INV_MASK  0xFF80FFFF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__LEN1__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog0.Format - NTTP transport protocol version */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__FORMAT__SHIFT     31
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__FORMAT__WIDTH     1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__FORMAT__MASK      0x80000000
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__FORMAT__INV_MASK  0x7FFFFFFF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG0__FORMAT__HW_DEFAULT 0x1

/* ddrm_observer_main_ErrorLogger_0_ErrLog1 */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1 0x10820198

/* ddrm_observer_main_ErrorLogger_0_ErrLog1.ErrLog1 - Stores NTTP packet header field RouteId (LSBs) of the logged error */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1__ERR_LOG1__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1__ERR_LOG1__WIDTH    15
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1__ERR_LOG1__MASK     0x00007FFF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1__ERR_LOG1__INV_MASK 0xFFFF8000
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG1__ERR_LOG1__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog3 */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3 0x108201A0

/* ddrm_observer_main_ErrorLogger_0_ErrLog3.ErrLog3 - Stores NTTP packet header field Addr (LSBs) of the logged error */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3__ERR_LOG3__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3__ERR_LOG3__WIDTH    32
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3__ERR_LOG3__MASK     0xFFFFFFFF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3__ERR_LOG3__INV_MASK 0x00000000
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG3__ERR_LOG3__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_ErrLog5 */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5 0x108201A8

/* ddrm_observer_main_ErrorLogger_0_ErrLog5.ErrLog5 - Stores NTTP packet header field User (LSBs) of the logged error */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5__ERR_LOG5__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5__ERR_LOG5__WIDTH    12
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5__ERR_LOG5__MASK     0x00000FFF
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5__ERR_LOG5__INV_MASK 0xFFFFF000
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_ERR_LOG5__ERR_LOG5__HW_DEFAULT 0x0

/* ddrm_observer_main_ErrorLogger_0_StallEn */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN 0x108201B8

/* ddrm_observer_main_ErrorLogger_0_StallEn.StallEn - Set to 1 to enable stall mode behavior. */
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN__STALL_EN__SHIFT    0
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN__STALL_EN__WIDTH    1
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN__STALL_EN__MASK     0x00000001
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN__STALL_EN__INV_MASK 0xFFFFFFFE
#define DDRM_OBSERVER_MAIN_ERROR_LOGGER_0_STALL_EN__STALL_EN__HW_DEFAULT 0x0

/* ddr_bist_I_main_QosGenerator_Id_CoreId */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID 0x10820200

/* ddr_bist_I_main_QosGenerator_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_TYPE_ID__SHIFT   0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_TYPE_ID__WIDTH   8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_TYPE_ID__MASK    0x000000FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0x4

/* ddr_bist_I_main_QosGenerator_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_CHECKSUM__SHIFT    8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_CHECKSUM__WIDTH    24
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_CHECKSUM__MASK     0xFFFFFF00
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0xBAC0EA

/* ddr_bist_I_main_QosGenerator_Id_RevisionId */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID 0x10820204

/* ddr_bist_I_main_QosGenerator_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__USER_ID__SHIFT    0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__USER_ID__WIDTH    8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__USER_ID__MASK     0x000000FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddr_bist_I_main_QosGenerator_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__FLEX_NOC_ID__SHIFT   8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__FLEX_NOC_ID__WIDTH   24
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__FLEX_NOC_ID__MASK    0xFFFFFF00
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddr_bist_I_main_QosGenerator_Priority */
/* Priority register. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY 0x10820208

/* ddr_bist_I_main_QosGenerator_Priority.P0 - In Programmable or Bandwidth Limiter mode, the priority level for write transactions. In Bandwidth Regulator mode, the priority level when the used throughput is above the threshold. In Bandwidth Regulator mode, P0 should have a value equal or lower than P1. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P0__SHIFT      0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P0__WIDTH      3
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P0__MASK       0x00000007
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P0__INV_MASK   0xFFFFFFF8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P0__HW_DEFAULT 0x4

/* ddr_bist_I_main_QosGenerator_Priority.P1 - In Programmable or Bandwidth Limiter mode, the priority level for read transactions. In Bandwidth regulator mode, the priority level when the used throughput is below the threshold. In Bandwidth Regulator mode, P1 should have a value equal or greater than P0. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P1__SHIFT      8
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P1__WIDTH      3
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P1__MASK       0x00000700
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P1__INV_MASK   0xFFFFF8FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__P1__HW_DEFAULT 0x4

/* ddr_bist_I_main_QosGenerator_Priority.Mark - Backward compatibility marker when 0. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__MARK__SHIFT      31
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__MARK__WIDTH      1
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__MARK__MASK       0x80000000
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__MARK__INV_MASK   0x7FFFFFFF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_PRIORITY__MARK__HW_DEFAULT 0x1

/* ddr_bist_I_main_QosGenerator_Mode */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE 0x1082020C

/* ddr_bist_I_main_QosGenerator_Mode.Mode - 0 = Programmable mode: a programmed priority is assigned to each read or write, 1 = Bandwidth Limiter Mode: a hard limit restricts throughput, 2 = Bypass mode: (<See SoC-specific QoS generator documentation>), 3 = Bandwidth Regulator mode: priority decreases when throughput exceeds a threshold. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE__MODE__SHIFT      0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE__MODE__WIDTH      2
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE__MODE__MASK       0x00000003
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE__MODE__INV_MASK   0xFFFFFFFC
#define DDR_BIST_I_MAIN_QOS_GENERATOR_MODE__MODE__HW_DEFAULT 0x0

/* ddr_bist_I_main_QosGenerator_Bandwidth */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH 0x10820210

/* ddr_bist_I_main_QosGenerator_Bandwidth.Bandwidth - In Bandwidth Limiter or Bandwidth Regulator mode, the bandwidth threshold in units of 1/256th bytes per cycle. For example, 80 MBps on a 250 MHz interface is value 0x0052. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH__BANDWIDTH__SHIFT      0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH__BANDWIDTH__WIDTH      12
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH__BANDWIDTH__MASK       0x00000FFF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH__BANDWIDTH__INV_MASK   0xFFFFF000
#define DDR_BIST_I_MAIN_QOS_GENERATOR_BANDWIDTH__BANDWIDTH__HW_DEFAULT 0x40

/* ddr_bist_I_main_QosGenerator_Saturation */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION 0x10820214

/* ddr_bist_I_main_QosGenerator_Saturation.Saturation - In Bandwidth Limiter or Bandwidth Regulator mode, the maximum data count value, in units of 16 bytes. This determines the window of time over which bandwidth is measured. For example, to measure bandwidth within a 1000 cycle window on a 64-bit interface is value 0x1F4. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION__SATURATION__SHIFT      0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION__SATURATION__WIDTH      10
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION__SATURATION__MASK       0x000003FF
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION__SATURATION__INV_MASK   0xFFFFFC00
#define DDR_BIST_I_MAIN_QOS_GENERATOR_SATURATION__SATURATION__HW_DEFAULT 0x40

/* ddr_bist_I_main_QosGenerator_ExtControl */
/* External inputs control. */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL 0x10820218

/* ddr_bist_I_main_QosGenerator_ExtControl.SocketQosEn - n/a */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__SOCKET_QOS_EN__SHIFT   0
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__SOCKET_QOS_EN__WIDTH   1
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__SOCKET_QOS_EN__MASK    0x00000001
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__SOCKET_QOS_EN__INV_MASK 0xFFFFFFFE
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__SOCKET_QOS_EN__HW_DEFAULT 0x0

/* ddr_bist_I_main_QosGenerator_ExtControl.ExtThrEn - n/a */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__EXT_THR_EN__SHIFT   1
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__EXT_THR_EN__WIDTH   1
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__EXT_THR_EN__MASK    0x00000002
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__EXT_THR_EN__INV_MASK 0xFFFFFFFD
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__EXT_THR_EN__HW_DEFAULT 0x0

/* ddr_bist_I_main_QosGenerator_ExtControl.IntClkEn - n/a */
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__INT_CLK_EN__SHIFT   2
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__INT_CLK_EN__WIDTH   1
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__INT_CLK_EN__MASK    0x00000004
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__INT_CLK_EN__INV_MASK 0xFFFFFFFB
#define DDR_BIST_I_MAIN_QOS_GENERATOR_EXT_CONTROL__INT_CLK_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Id_CoreId */
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID 0x10820400

/* upctl_data_T_main_Probe_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_TYPE_ID__SHIFT    0
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_TYPE_ID__WIDTH    8
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_TYPE_ID__MASK     0x000000FF
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0x6

/* upctl_data_T_main_Probe_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_CHECKSUM__SHIFT     8
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_CHECKSUM__WIDTH     24
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_CHECKSUM__MASK      0xFFFFFF00
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_CHECKSUM__INV_MASK  0x000000FF
#define UPCTL_DATA_T_MAIN_PROBE_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0x1F782C

/* upctl_data_T_main_Probe_Id_RevisionId */
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID 0x10820404

/* upctl_data_T_main_Probe_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__USER_ID__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__USER_ID__WIDTH     8
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__USER_ID__MASK      0x000000FF
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__USER_ID__INV_MASK  0xFFFFFF00
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__FLEX_NOC_ID__SHIFT    8
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__FLEX_NOC_ID__WIDTH    24
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__FLEX_NOC_ID__MASK     0xFFFFFF00
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define UPCTL_DATA_T_MAIN_PROBE_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* upctl_data_T_main_Probe_MainCtl */
/* Register MainCtl contains probe global control bits. The register has seven bit fields: */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL 0x10820408

/* upctl_data_T_main_Probe_MainCtl.ErrEn - Register field ErrEn enables the probe to send on the ObsTx output any packet with Error status, independently of filtering mechanisms, thus constituting a simple supplementary global filter. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ERR_EN__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ERR_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ERR_EN__MASK      0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ERR_EN__INV_MASK  0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ERR_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.TraceEn - Register field TraceEn enables the probe to send filtered packets (Trace) on the ObsTx observation output. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__TRACE_EN__SHIFT     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__TRACE_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__TRACE_EN__MASK      0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__TRACE_EN__INV_MASK  0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__TRACE_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.PayloadEn - Register field PayloadEn, when set to 1, enables traces to contain headers and payload. When set ot 0, only headers are reported. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__PAYLOAD_EN__SHIFT     2
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__PAYLOAD_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__PAYLOAD_EN__MASK      0x00000004
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__PAYLOAD_EN__INV_MASK  0xFFFFFFFB
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__PAYLOAD_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.StatEn - When set to 1, register field StatEn enables statistics profiling. The probe sendS statistics results to the output for signal ObsTx. All statistics counters are cleared when the StatEn bit goes from 0 to 1. When set to 0, counters are disabled. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_EN__SHIFT     3
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_EN__MASK      0x00000008
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_EN__INV_MASK  0xFFFFFFF7
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.AlarmEn - When set, register field AlarmEn enables the probe to collect alarm-related information. When the register field bit is null, both TraceAlarm and StatAlarm outputs are driven to 0. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ALARM_EN__SHIFT     4
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ALARM_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ALARM_EN__MASK      0x00000010
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ALARM_EN__INV_MASK  0xFFFFFFEF
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__ALARM_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.StatCondDump - When set, register field StatCondDump enables the dump of a statistics frame to the range of counter values set for registers StatAlarmMin, StatAlarmMax, and AlarmMode. This field also renders register StatAlarmStatus inoperative. When parameter statisticsCounterAlarm is set to False, the StatCondDump register bit is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_COND_DUMP__SHIFT    5
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_COND_DUMP__WIDTH    1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_COND_DUMP__MASK     0x00000020
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_COND_DUMP__INV_MASK 0xFFFFFFDF
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__STAT_COND_DUMP__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.IntrusiveMode - When set to 1, register field IntrusiveMode enables trace operation in Intrusive flow-control mode. When set to 0, the register enables trace operation in Overflow flow-control mode */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__INTRUSIVE_MODE__SHIFT     6
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__INTRUSIVE_MODE__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__INTRUSIVE_MODE__MASK      0x00000040
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__INTRUSIVE_MODE__INV_MASK  0xFFFFFFBF
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__INTRUSIVE_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_MainCtl.FiltByteAlwaysChainableEn - When set to 0, filters are mapped to all statistic counters when counting bytes or enabled bytes. Therefore, only filter events mapped to even counters can be counted using a pair of chained counters.When set to 1, filters are mapped only to even statistic counters when counting bytes or enabled bytes. Thus events from any filter can be counted using a pair of chained counters. */
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__FILT_BYTE_ALWAYS_CHAINABLE_EN__SHIFT  7
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__FILT_BYTE_ALWAYS_CHAINABLE_EN__WIDTH  1
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__FILT_BYTE_ALWAYS_CHAINABLE_EN__MASK   0x00000080
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__FILT_BYTE_ALWAYS_CHAINABLE_EN__INV_MASK 0xFFFFFF7F
#define UPCTL_DATA_T_MAIN_PROBE_MAIN_CTL__FILT_BYTE_ALWAYS_CHAINABLE_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_CfgCtl */
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL 0x1082040C

/* upctl_data_T_main_Probe_CfgCtl.GlobalEn -  */
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__GLOBAL_EN__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__GLOBAL_EN__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__GLOBAL_EN__MASK      0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__GLOBAL_EN__INV_MASK  0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__GLOBAL_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_CfgCtl.Active -  */
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__ACTIVE__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__ACTIVE__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__ACTIVE__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__ACTIVE__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_CFG_CTL__ACTIVE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_FilterLut */
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT 0x10820414

/* upctl_data_T_main_Probe_FilterLut.FilterLut - Register FilterLut contains a look-up table that is used to combine filter outputs in order to trace packets. Packet tracing is enabled when the FilterLut bit of index (FNout ... F0out) is equal to 1.The number of bits in register FilterLut is determined by the setting for parameter nFilter, calculated as 2**nFilter.When parameter nFilter is set to None, FilterLut is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT__FILTER_LUT__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT__FILTER_LUT__WIDTH     16
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT__FILTER_LUT__MASK      0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT__FILTER_LUT__INV_MASK  0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_FILTER_LUT__FILTER_LUT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_TraceAlarmEn */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN 0x10820418

/* upctl_data_T_main_Probe_TraceAlarmEn.TraceAlarmEn - Register TraceAlarmEn controls which lookup table or filter can set the TraceAlarm signal output once the trace alarm status is set. The number of bits in register TraceAlarmEn is determined by the value set for parameter nFilter + 1.Bit nFilter controls the lookup table output, and bits nFilter:0 control the corresponding filter output. When parameter nFilter is set to None, TraceAlarmEn is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN__TRACE_ALARM_EN__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN__TRACE_ALARM_EN__WIDTH   5
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN__TRACE_ALARM_EN__MASK    0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN__TRACE_ALARM_EN__INV_MASK 0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_EN__TRACE_ALARM_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_TraceAlarmStatus */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS 0x1082041C

/* upctl_data_T_main_Probe_TraceAlarmStatus.TraceAlarmStatus - Register TraceAlarmStatus is a read-only register that indicates which lookup table or filter has been matched by a packet, independently of register TraceAlarmEn bit configuration. The number of bits in TraceAlarmStatus is determined by the value set for parameter nFilter + 1.When nFilter is set to None, TraceAlarmStatus is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS__TRACE_ALARM_STATUS__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS__TRACE_ALARM_STATUS__WIDTH   5
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS__TRACE_ALARM_STATUS__MASK    0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS__TRACE_ALARM_STATUS__INV_MASK 0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_STATUS__TRACE_ALARM_STATUS__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_TraceAlarmClr */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR 0x10820420

/* upctl_data_T_main_Probe_TraceAlarmClr.TraceAlarmClr - Setting a bit to 1 in register TraceAlarmClr clears the corresponding bit in register TraceAlarmStatus.The number of bits in register TraceAlarmClr is equal to (nFilter + 1). When nFilter is set to 0, TraceAlarmClr is reserved.NOTE The written value is not stored in TraceAlarmClr. A read always returns 0. */
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR__TRACE_ALARM_CLR__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR__TRACE_ALARM_CLR__WIDTH   5
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR__TRACE_ALARM_CLR__MASK    0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR__TRACE_ALARM_CLR__INV_MASK 0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_TRACE_ALARM_CLR__TRACE_ALARM_CLR__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatPeriod */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD 0x10820424

/* upctl_data_T_main_Probe_StatPeriod.StatPeriod - Register StatPeriod is a 5-bit register that sets a period, within a range of 2 cycles to 2 gigacycles, during which statistics are collected before being dumped automatically. Setting the register implicitly enables automatic mode operation for statistics collection. The period is calculated with the formula: N_Cycle = 2**StatPeriodWhen register StatPeriod is set to its default value 0, automatic dump mode is disabled, and register StatGo is activated for manual mode operation. Note: When parameter statisticsCollection is set to False, StatPeriod is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD__STAT_PERIOD__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD__STAT_PERIOD__WIDTH     5
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD__STAT_PERIOD__MASK      0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD__STAT_PERIOD__INV_MASK  0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_PERIOD__STAT_PERIOD__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatGo */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO 0x10820428

/* upctl_data_T_main_Probe_StatGo.StatGo - Writing a 1 to the 1-bit pulse register StatGo generates a statistics dump.The register is active when statistics collection operates in manual mode, that is, when register StatPeriod is set to 0.NOTE The written value is not stored in StatGo. A read always returns 0. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO__STAT_GO__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO__STAT_GO__WIDTH     1
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO__STAT_GO__MASK      0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO__STAT_GO__INV_MASK  0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_STAT_GO__STAT_GO__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatAlarmMin */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN 0x1082042C

/* upctl_data_T_main_Probe_StatAlarmMin.StatAlarmMin - Register StatAlarmMin contains the minimum count value used in statistics alarm comparisons. The number of bits is equal to twice the value set forparameter wStatisticsCounter. When parameter statisticsCounterAlarm is set to False, StatAlarmMin is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN__STAT_ALARM_MIN__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN__STAT_ALARM_MIN__WIDTH   32
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN__STAT_ALARM_MIN__MASK    0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN__STAT_ALARM_MIN__INV_MASK 0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MIN__STAT_ALARM_MIN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatAlarmMax */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX 0x10820430

/* upctl_data_T_main_Probe_StatAlarmMax.StatAlarmMax - Register StatAlarmMax contains the maximum count value used in statistics alarm comparisons.The number of bits is equal to twice the value set for parameter wStatisticsCounter. When parameter statisticsCounterAlarm is set to False, StatAlarmMax is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX__STAT_ALARM_MAX__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX__STAT_ALARM_MAX__WIDTH   32
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX__STAT_ALARM_MAX__MASK    0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX__STAT_ALARM_MAX__INV_MASK 0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_MAX__STAT_ALARM_MAX__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatAlarmStatus */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS 0x10820434

/* upctl_data_T_main_Probe_StatAlarmStatus.StatAlarmStatus - Register StatAlarmStatus is a read-only 1-bit register indicating that at least one statistics counter has exceeded the programmed values for registers StatAlarmMin or StatAlarmMax. Output signal StatAlarm is equal to the values stored in register MainCtl fields StatAlarmStatus and AlarmEn. When parameter statisticsCounterAlarm is set to False, StatAlarmStatus is reserved. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS__STAT_ALARM_STATUS__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS__STAT_ALARM_STATUS__WIDTH   1
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS__STAT_ALARM_STATUS__MASK    0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS__STAT_ALARM_STATUS__INV_MASK 0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_STATUS__STAT_ALARM_STATUS__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatAlarmClr */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR 0x10820438

/* upctl_data_T_main_Probe_StatAlarmClr.StatAlarmClr - Register StatAlarmClr is a 1-bit register. Writing a 1 to this register clears the StatAlarmStatus register bit.When parameter statisticsCounterAlarm is set to False, StatAlarmClr is reserved.NOTE The written value is not stored in StatAlarmClr. A read always returns 0. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR__STAT_ALARM_CLR__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR__STAT_ALARM_CLR__WIDTH   1
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR__STAT_ALARM_CLR__MASK    0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR__STAT_ALARM_CLR__INV_MASK 0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_CLR__STAT_ALARM_CLR__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_StatAlarmEn */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN 0x1082043C

/* upctl_data_T_main_Probe_StatAlarmEn.StatAlarmEn - Register StatAlarmEn is a 1-bit register. When set to 0 it masks StatAlarm and CtiTrigOut(1) signal interrupts. */
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN__STAT_ALARM_EN__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN__STAT_ALARM_EN__WIDTH   1
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN__STAT_ALARM_EN__MASK    0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN__STAT_ALARM_EN__INV_MASK 0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_STAT_ALARM_EN__STAT_ALARM_EN__HW_DEFAULT 0x1

/* upctl_data_T_main_Probe_Filters_0_RouteIdBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE 0x10820444

/* upctl_data_T_main_Probe_Filters_0_RouteIdBase.Filters_0_RouteIdBase - Register RouteIdBase contains the RouteId-lsbFilterRouteId bits base used to filter packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE__FILTERS_0_ROUTE_ID_BASE__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE__FILTERS_0_ROUTE_ID_BASE__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE__FILTERS_0_ROUTE_ID_BASE__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE__FILTERS_0_ROUTE_ID_BASE__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_BASE__FILTERS_0_ROUTE_ID_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_RouteIdMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK 0x10820448

/* upctl_data_T_main_Probe_Filters_0_RouteIdMask.Filters_0_RouteIdMask - Register RouteIdMask contains the RouteId-lsbFilterRouteId mask used to filter packets. A packet is a candidate when packet.RouteId>>lsbFilterRouteId &amp; RouteIdMask = RouteIdBase &amp; RouteIdMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK__FILTERS_0_ROUTE_ID_MASK__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK__FILTERS_0_ROUTE_ID_MASK__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK__FILTERS_0_ROUTE_ID_MASK__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK__FILTERS_0_ROUTE_ID_MASK__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ROUTE_ID_MASK__FILTERS_0_ROUTE_ID_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_AddrBase_Low */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW 0x1082044C

/* upctl_data_T_main_Probe_Filters_0_AddrBase_Low.Filters_0_AddrBase_Low - Address LSB register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW__FILTERS_0_ADDR_BASE_LOW__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW__FILTERS_0_ADDR_BASE_LOW__WIDTH     32
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW__FILTERS_0_ADDR_BASE_LOW__MASK      0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW__FILTERS_0_ADDR_BASE_LOW__INV_MASK  0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_ADDR_BASE_LOW__FILTERS_0_ADDR_BASE_LOW__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_WindowSize */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE 0x10820454

/* upctl_data_T_main_Probe_Filters_0_WindowSize.Filters_0_WindowSize - Register WindowSize contains the encoded address mask used to filter packets. The effective Mask value is equal to ~(2max(WindowSize, packet.Len) - 1). A packet is a candidate when packet.Addr &amp; Mask = AddrBase &amp; Mask. This allows filteringof packets having an intersection with the AddrBase/WindowSize burst aligned region, even if the region is smaller than the packet. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE__FILTERS_0_WINDOW_SIZE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE__FILTERS_0_WINDOW_SIZE__WIDTH     6
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE__FILTERS_0_WINDOW_SIZE__MASK      0x0000003F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE__FILTERS_0_WINDOW_SIZE__INV_MASK  0xFFFFFFC0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_WINDOW_SIZE__FILTERS_0_WINDOW_SIZE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Opcode */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE 0x10820460

/* upctl_data_T_main_Probe_Filters_0_Opcode.RdEn - Selects RD packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__RD_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__RD_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__RD_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__RD_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__RD_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Opcode.WrEn - Selects WR packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__WR_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__WR_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__WR_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__WR_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__WR_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Opcode.LockEn - Selects RDX-WR, RDL, WRC and Linked sequence. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__LOCK_EN__SHIFT      2
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__LOCK_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__LOCK_EN__MASK       0x00000004
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__LOCK_EN__INV_MASK   0xFFFFFFFB
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__LOCK_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Opcode.UrgEn - Selects URG packets (urgency). */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__URG_EN__SHIFT      3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__URG_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__URG_EN__MASK       0x00000008
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__URG_EN__INV_MASK   0xFFFFFFF7
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_OPCODE__URG_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Status */
/* Register Status is 2-bit register that selects candidate packets based on packet status. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS 0x10820464

/* upctl_data_T_main_Probe_Filters_0_Status.ReqEn - Selects REQ status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__REQ_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__REQ_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__REQ_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__REQ_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__REQ_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Status.RspEn - Selects RSP and FAIL-CONT status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__RSP_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__RSP_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__RSP_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__RSP_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_STATUS__RSP_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_Length */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH 0x10820468

/* upctl_data_T_main_Probe_Filters_0_Length.Filters_0_Length - Register Length is 4-bit register that selects candidate packets if their number of bytes is less than or equal to 2**Length. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH__FILTERS_0_LENGTH__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH__FILTERS_0_LENGTH__WIDTH       4
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH__FILTERS_0_LENGTH__MASK        0x0000000F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH__FILTERS_0_LENGTH__INV_MASK    0xFFFFFFF0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_LENGTH__FILTERS_0_LENGTH__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_0_Urgency */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY 0x1082046C

/* upctl_data_T_main_Probe_Filters_0_Urgency.Filters_0_Urgency - Register Urgency contains the minimum urgency level used to filter packets. A packet is a candidate when its socket urgency is greater than or equal to the urgency specified in the register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY__FILTERS_0_URGENCY__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY__FILTERS_0_URGENCY__WIDTH       3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY__FILTERS_0_URGENCY__MASK        0x00000007
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY__FILTERS_0_URGENCY__INV_MASK    0xFFFFFFF8
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_URGENCY__FILTERS_0_URGENCY__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_0_UserBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE 0x10820470

/* upctl_data_T_main_Probe_Filters_0_UserBase.Filters_0_UserBase - Register UserBase is available when parameter useUserFilter is set to True. Register size is determined by parameter wUser. The register stores a user base value that is employed in filtering packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE__FILTERS_0_USER_BASE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE__FILTERS_0_USER_BASE__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE__FILTERS_0_USER_BASE__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE__FILTERS_0_USER_BASE__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_BASE__FILTERS_0_USER_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_0_UserMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK 0x10820474

/* upctl_data_T_main_Probe_Filters_0_UserMask.Filters_0_UserMask - Register UserMask is available when parameter useUserFilter is set to True.Register size is determined by parameter wUser. The register stores a user mask that is employed in filtering packets. A packet is a candidate for trace or statistic collection when packet.User &amp; UserMask = UserBase &amp; UserMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK__FILTERS_0_USER_MASK__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK__FILTERS_0_USER_MASK__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK__FILTERS_0_USER_MASK__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK__FILTERS_0_USER_MASK__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_0_USER_MASK__FILTERS_0_USER_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_RouteIdBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE 0x10820480

/* upctl_data_T_main_Probe_Filters_1_RouteIdBase.Filters_1_RouteIdBase - Register RouteIdBase contains the RouteId-lsbFilterRouteId bits base used to filter packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE__FILTERS_1_ROUTE_ID_BASE__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE__FILTERS_1_ROUTE_ID_BASE__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE__FILTERS_1_ROUTE_ID_BASE__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE__FILTERS_1_ROUTE_ID_BASE__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_BASE__FILTERS_1_ROUTE_ID_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_RouteIdMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK 0x10820484

/* upctl_data_T_main_Probe_Filters_1_RouteIdMask.Filters_1_RouteIdMask - Register RouteIdMask contains the RouteId-lsbFilterRouteId mask used to filter packets. A packet is a candidate when packet.RouteId>>lsbFilterRouteId &amp; RouteIdMask = RouteIdBase &amp; RouteIdMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK__FILTERS_1_ROUTE_ID_MASK__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK__FILTERS_1_ROUTE_ID_MASK__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK__FILTERS_1_ROUTE_ID_MASK__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK__FILTERS_1_ROUTE_ID_MASK__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ROUTE_ID_MASK__FILTERS_1_ROUTE_ID_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_AddrBase_Low */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW 0x10820488

/* upctl_data_T_main_Probe_Filters_1_AddrBase_Low.Filters_1_AddrBase_Low - Address LSB register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW__FILTERS_1_ADDR_BASE_LOW__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW__FILTERS_1_ADDR_BASE_LOW__WIDTH     32
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW__FILTERS_1_ADDR_BASE_LOW__MASK      0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW__FILTERS_1_ADDR_BASE_LOW__INV_MASK  0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_ADDR_BASE_LOW__FILTERS_1_ADDR_BASE_LOW__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_WindowSize */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE 0x10820490

/* upctl_data_T_main_Probe_Filters_1_WindowSize.Filters_1_WindowSize - Register WindowSize contains the encoded address mask used to filter packets. The effective Mask value is equal to ~(2max(WindowSize, packet.Len) - 1). A packet is a candidate when packet.Addr &amp; Mask = AddrBase &amp; Mask. This allows filteringof packets having an intersection with the AddrBase/WindowSize burst aligned region, even if the region is smaller than the packet. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE__FILTERS_1_WINDOW_SIZE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE__FILTERS_1_WINDOW_SIZE__WIDTH     6
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE__FILTERS_1_WINDOW_SIZE__MASK      0x0000003F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE__FILTERS_1_WINDOW_SIZE__INV_MASK  0xFFFFFFC0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_WINDOW_SIZE__FILTERS_1_WINDOW_SIZE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Opcode */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE 0x1082049C

/* upctl_data_T_main_Probe_Filters_1_Opcode.RdEn - Selects RD packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__RD_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__RD_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__RD_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__RD_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__RD_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Opcode.WrEn - Selects WR packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__WR_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__WR_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__WR_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__WR_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__WR_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Opcode.LockEn - Selects RDX-WR, RDL, WRC and Linked sequence. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__LOCK_EN__SHIFT      2
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__LOCK_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__LOCK_EN__MASK       0x00000004
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__LOCK_EN__INV_MASK   0xFFFFFFFB
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__LOCK_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Opcode.UrgEn - Selects URG packets (urgency). */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__URG_EN__SHIFT      3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__URG_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__URG_EN__MASK       0x00000008
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__URG_EN__INV_MASK   0xFFFFFFF7
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_OPCODE__URG_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Status */
/* Register Status is 2-bit register that selects candidate packets based on packet status. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS 0x108204A0

/* upctl_data_T_main_Probe_Filters_1_Status.ReqEn - Selects REQ status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__REQ_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__REQ_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__REQ_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__REQ_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__REQ_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Status.RspEn - Selects RSP and FAIL-CONT status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__RSP_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__RSP_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__RSP_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__RSP_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_STATUS__RSP_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_Length */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH 0x108204A4

/* upctl_data_T_main_Probe_Filters_1_Length.Filters_1_Length - Register Length is 4-bit register that selects candidate packets if their number of bytes is less than or equal to 2**Length. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH__FILTERS_1_LENGTH__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH__FILTERS_1_LENGTH__WIDTH       4
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH__FILTERS_1_LENGTH__MASK        0x0000000F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH__FILTERS_1_LENGTH__INV_MASK    0xFFFFFFF0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_LENGTH__FILTERS_1_LENGTH__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_1_Urgency */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY 0x108204A8

/* upctl_data_T_main_Probe_Filters_1_Urgency.Filters_1_Urgency - Register Urgency contains the minimum urgency level used to filter packets. A packet is a candidate when its socket urgency is greater than or equal to the urgency specified in the register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY__FILTERS_1_URGENCY__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY__FILTERS_1_URGENCY__WIDTH       3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY__FILTERS_1_URGENCY__MASK        0x00000007
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY__FILTERS_1_URGENCY__INV_MASK    0xFFFFFFF8
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_URGENCY__FILTERS_1_URGENCY__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_1_UserBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE 0x108204AC

/* upctl_data_T_main_Probe_Filters_1_UserBase.Filters_1_UserBase - Register UserBase is available when parameter useUserFilter is set to True. Register size is determined by parameter wUser. The register stores a user base value that is employed in filtering packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE__FILTERS_1_USER_BASE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE__FILTERS_1_USER_BASE__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE__FILTERS_1_USER_BASE__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE__FILTERS_1_USER_BASE__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_BASE__FILTERS_1_USER_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_1_UserMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK 0x108204B0

/* upctl_data_T_main_Probe_Filters_1_UserMask.Filters_1_UserMask - Register UserMask is available when parameter useUserFilter is set to True.Register size is determined by parameter wUser. The register stores a user mask that is employed in filtering packets. A packet is a candidate for trace or statistic collection when packet.User &amp; UserMask = UserBase &amp; UserMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK__FILTERS_1_USER_MASK__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK__FILTERS_1_USER_MASK__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK__FILTERS_1_USER_MASK__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK__FILTERS_1_USER_MASK__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_1_USER_MASK__FILTERS_1_USER_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_RouteIdBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE 0x108204BC

/* upctl_data_T_main_Probe_Filters_2_RouteIdBase.Filters_2_RouteIdBase - Register RouteIdBase contains the RouteId-lsbFilterRouteId bits base used to filter packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE__FILTERS_2_ROUTE_ID_BASE__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE__FILTERS_2_ROUTE_ID_BASE__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE__FILTERS_2_ROUTE_ID_BASE__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE__FILTERS_2_ROUTE_ID_BASE__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_BASE__FILTERS_2_ROUTE_ID_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_RouteIdMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK 0x108204C0

/* upctl_data_T_main_Probe_Filters_2_RouteIdMask.Filters_2_RouteIdMask - Register RouteIdMask contains the RouteId-lsbFilterRouteId mask used to filter packets. A packet is a candidate when packet.RouteId>>lsbFilterRouteId &amp; RouteIdMask = RouteIdBase &amp; RouteIdMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK__FILTERS_2_ROUTE_ID_MASK__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK__FILTERS_2_ROUTE_ID_MASK__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK__FILTERS_2_ROUTE_ID_MASK__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK__FILTERS_2_ROUTE_ID_MASK__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ROUTE_ID_MASK__FILTERS_2_ROUTE_ID_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_AddrBase_Low */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW 0x108204C4

/* upctl_data_T_main_Probe_Filters_2_AddrBase_Low.Filters_2_AddrBase_Low - Address LSB register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW__FILTERS_2_ADDR_BASE_LOW__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW__FILTERS_2_ADDR_BASE_LOW__WIDTH     32
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW__FILTERS_2_ADDR_BASE_LOW__MASK      0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW__FILTERS_2_ADDR_BASE_LOW__INV_MASK  0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_ADDR_BASE_LOW__FILTERS_2_ADDR_BASE_LOW__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_WindowSize */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE 0x108204CC

/* upctl_data_T_main_Probe_Filters_2_WindowSize.Filters_2_WindowSize - Register WindowSize contains the encoded address mask used to filter packets. The effective Mask value is equal to ~(2max(WindowSize, packet.Len) - 1). A packet is a candidate when packet.Addr &amp; Mask = AddrBase &amp; Mask. This allows filteringof packets having an intersection with the AddrBase/WindowSize burst aligned region, even if the region is smaller than the packet. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE__FILTERS_2_WINDOW_SIZE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE__FILTERS_2_WINDOW_SIZE__WIDTH     6
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE__FILTERS_2_WINDOW_SIZE__MASK      0x0000003F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE__FILTERS_2_WINDOW_SIZE__INV_MASK  0xFFFFFFC0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_WINDOW_SIZE__FILTERS_2_WINDOW_SIZE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Opcode */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE 0x108204D8

/* upctl_data_T_main_Probe_Filters_2_Opcode.RdEn - Selects RD packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__RD_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__RD_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__RD_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__RD_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__RD_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Opcode.WrEn - Selects WR packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__WR_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__WR_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__WR_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__WR_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__WR_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Opcode.LockEn - Selects RDX-WR, RDL, WRC and Linked sequence. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__LOCK_EN__SHIFT      2
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__LOCK_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__LOCK_EN__MASK       0x00000004
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__LOCK_EN__INV_MASK   0xFFFFFFFB
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__LOCK_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Opcode.UrgEn - Selects URG packets (urgency). */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__URG_EN__SHIFT      3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__URG_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__URG_EN__MASK       0x00000008
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__URG_EN__INV_MASK   0xFFFFFFF7
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_OPCODE__URG_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Status */
/* Register Status is 2-bit register that selects candidate packets based on packet status. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS 0x108204DC

/* upctl_data_T_main_Probe_Filters_2_Status.ReqEn - Selects REQ status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__REQ_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__REQ_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__REQ_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__REQ_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__REQ_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Status.RspEn - Selects RSP and FAIL-CONT status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__RSP_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__RSP_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__RSP_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__RSP_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_STATUS__RSP_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_Length */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH 0x108204E0

/* upctl_data_T_main_Probe_Filters_2_Length.Filters_2_Length - Register Length is 4-bit register that selects candidate packets if their number of bytes is less than or equal to 2**Length. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH__FILTERS_2_LENGTH__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH__FILTERS_2_LENGTH__WIDTH       4
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH__FILTERS_2_LENGTH__MASK        0x0000000F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH__FILTERS_2_LENGTH__INV_MASK    0xFFFFFFF0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_LENGTH__FILTERS_2_LENGTH__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_2_Urgency */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY 0x108204E4

/* upctl_data_T_main_Probe_Filters_2_Urgency.Filters_2_Urgency - Register Urgency contains the minimum urgency level used to filter packets. A packet is a candidate when its socket urgency is greater than or equal to the urgency specified in the register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY__FILTERS_2_URGENCY__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY__FILTERS_2_URGENCY__WIDTH       3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY__FILTERS_2_URGENCY__MASK        0x00000007
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY__FILTERS_2_URGENCY__INV_MASK    0xFFFFFFF8
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_URGENCY__FILTERS_2_URGENCY__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_2_UserBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE 0x108204E8

/* upctl_data_T_main_Probe_Filters_2_UserBase.Filters_2_UserBase - Register UserBase is available when parameter useUserFilter is set to True. Register size is determined by parameter wUser. The register stores a user base value that is employed in filtering packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE__FILTERS_2_USER_BASE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE__FILTERS_2_USER_BASE__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE__FILTERS_2_USER_BASE__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE__FILTERS_2_USER_BASE__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_BASE__FILTERS_2_USER_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_2_UserMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK 0x108204EC

/* upctl_data_T_main_Probe_Filters_2_UserMask.Filters_2_UserMask - Register UserMask is available when parameter useUserFilter is set to True.Register size is determined by parameter wUser. The register stores a user mask that is employed in filtering packets. A packet is a candidate for trace or statistic collection when packet.User &amp; UserMask = UserBase &amp; UserMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK__FILTERS_2_USER_MASK__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK__FILTERS_2_USER_MASK__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK__FILTERS_2_USER_MASK__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK__FILTERS_2_USER_MASK__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_2_USER_MASK__FILTERS_2_USER_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_RouteIdBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE 0x108204F8

/* upctl_data_T_main_Probe_Filters_3_RouteIdBase.Filters_3_RouteIdBase - Register RouteIdBase contains the RouteId-lsbFilterRouteId bits base used to filter packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE__FILTERS_3_ROUTE_ID_BASE__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE__FILTERS_3_ROUTE_ID_BASE__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE__FILTERS_3_ROUTE_ID_BASE__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE__FILTERS_3_ROUTE_ID_BASE__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_BASE__FILTERS_3_ROUTE_ID_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_RouteIdMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK 0x108204FC

/* upctl_data_T_main_Probe_Filters_3_RouteIdMask.Filters_3_RouteIdMask - Register RouteIdMask contains the RouteId-lsbFilterRouteId mask used to filter packets. A packet is a candidate when packet.RouteId>>lsbFilterRouteId &amp; RouteIdMask = RouteIdBase &amp; RouteIdMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK__FILTERS_3_ROUTE_ID_MASK__SHIFT   0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK__FILTERS_3_ROUTE_ID_MASK__WIDTH   15
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK__FILTERS_3_ROUTE_ID_MASK__MASK    0x00007FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK__FILTERS_3_ROUTE_ID_MASK__INV_MASK 0xFFFF8000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ROUTE_ID_MASK__FILTERS_3_ROUTE_ID_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_AddrBase_Low */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW 0x10820500

/* upctl_data_T_main_Probe_Filters_3_AddrBase_Low.Filters_3_AddrBase_Low - Address LSB register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW__FILTERS_3_ADDR_BASE_LOW__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW__FILTERS_3_ADDR_BASE_LOW__WIDTH     32
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW__FILTERS_3_ADDR_BASE_LOW__MASK      0xFFFFFFFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW__FILTERS_3_ADDR_BASE_LOW__INV_MASK  0x00000000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_ADDR_BASE_LOW__FILTERS_3_ADDR_BASE_LOW__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_WindowSize */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE 0x10820508

/* upctl_data_T_main_Probe_Filters_3_WindowSize.Filters_3_WindowSize - Register WindowSize contains the encoded address mask used to filter packets. The effective Mask value is equal to ~(2max(WindowSize, packet.Len) - 1). A packet is a candidate when packet.Addr &amp; Mask = AddrBase &amp; Mask. This allows filteringof packets having an intersection with the AddrBase/WindowSize burst aligned region, even if the region is smaller than the packet. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE__FILTERS_3_WINDOW_SIZE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE__FILTERS_3_WINDOW_SIZE__WIDTH     6
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE__FILTERS_3_WINDOW_SIZE__MASK      0x0000003F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE__FILTERS_3_WINDOW_SIZE__INV_MASK  0xFFFFFFC0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_WINDOW_SIZE__FILTERS_3_WINDOW_SIZE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Opcode */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE 0x10820514

/* upctl_data_T_main_Probe_Filters_3_Opcode.RdEn - Selects RD packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__RD_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__RD_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__RD_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__RD_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__RD_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Opcode.WrEn - Selects WR packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__WR_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__WR_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__WR_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__WR_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__WR_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Opcode.LockEn - Selects RDX-WR, RDL, WRC and Linked sequence. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__LOCK_EN__SHIFT      2
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__LOCK_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__LOCK_EN__MASK       0x00000004
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__LOCK_EN__INV_MASK   0xFFFFFFFB
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__LOCK_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Opcode.UrgEn - Selects URG packets (urgency). */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__URG_EN__SHIFT      3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__URG_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__URG_EN__MASK       0x00000008
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__URG_EN__INV_MASK   0xFFFFFFF7
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_OPCODE__URG_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Status */
/* Register Status is 2-bit register that selects candidate packets based on packet status. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS 0x10820518

/* upctl_data_T_main_Probe_Filters_3_Status.ReqEn - Selects REQ status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__REQ_EN__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__REQ_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__REQ_EN__MASK       0x00000001
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__REQ_EN__INV_MASK   0xFFFFFFFE
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__REQ_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Status.RspEn - Selects RSP and FAIL-CONT status packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__RSP_EN__SHIFT      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__RSP_EN__WIDTH      1
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__RSP_EN__MASK       0x00000002
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__RSP_EN__INV_MASK   0xFFFFFFFD
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_STATUS__RSP_EN__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_Length */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH 0x1082051C

/* upctl_data_T_main_Probe_Filters_3_Length.Filters_3_Length - Register Length is 4-bit register that selects candidate packets if their number of bytes is less than or equal to 2**Length. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH__FILTERS_3_LENGTH__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH__FILTERS_3_LENGTH__WIDTH       4
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH__FILTERS_3_LENGTH__MASK        0x0000000F
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH__FILTERS_3_LENGTH__INV_MASK    0xFFFFFFF0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_LENGTH__FILTERS_3_LENGTH__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_3_Urgency */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY 0x10820520

/* upctl_data_T_main_Probe_Filters_3_Urgency.Filters_3_Urgency - Register Urgency contains the minimum urgency level used to filter packets. A packet is a candidate when its socket urgency is greater than or equal to the urgency specified in the register. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY__FILTERS_3_URGENCY__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY__FILTERS_3_URGENCY__WIDTH       3
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY__FILTERS_3_URGENCY__MASK        0x00000007
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY__FILTERS_3_URGENCY__INV_MASK    0xFFFFFFF8
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_URGENCY__FILTERS_3_URGENCY__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Filters_3_UserBase */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE 0x10820524

/* upctl_data_T_main_Probe_Filters_3_UserBase.Filters_3_UserBase - Register UserBase is available when parameter useUserFilter is set to True. Register size is determined by parameter wUser. The register stores a user base value that is employed in filtering packets. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE__FILTERS_3_USER_BASE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE__FILTERS_3_USER_BASE__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE__FILTERS_3_USER_BASE__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE__FILTERS_3_USER_BASE__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_BASE__FILTERS_3_USER_BASE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Filters_3_UserMask */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK 0x10820528

/* upctl_data_T_main_Probe_Filters_3_UserMask.Filters_3_UserMask - Register UserMask is available when parameter useUserFilter is set to True.Register size is determined by parameter wUser. The register stores a user mask that is employed in filtering packets. A packet is a candidate for trace or statistic collection when packet.User &amp; UserMask = UserBase &amp; UserMask. */
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK__FILTERS_3_USER_MASK__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK__FILTERS_3_USER_MASK__WIDTH     12
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK__FILTERS_3_USER_MASK__MASK      0x00000FFF
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK__FILTERS_3_USER_MASK__INV_MASK  0xFFFFF000
#define UPCTL_DATA_T_MAIN_PROBE_FILTERS_3_USER_MASK__FILTERS_3_USER_MASK__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_0_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC 0x10820538

/* upctl_data_T_main_Probe_Counters_0_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_0_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE 0x1082053C

/* upctl_data_T_main_Probe_Counters_0_AlarmMode.Counters_0_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE__COUNTERS_0_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE__COUNTERS_0_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE__COUNTERS_0_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE__COUNTERS_0_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_ALARM_MODE__COUNTERS_0_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_0_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL 0x10820540

/* upctl_data_T_main_Probe_Counters_0_Val.Counters_0_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL__COUNTERS_0_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL__COUNTERS_0_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL__COUNTERS_0_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL__COUNTERS_0_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_0_VAL__COUNTERS_0_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_1_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC 0x1082054C

/* upctl_data_T_main_Probe_Counters_1_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_1_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE 0x10820550

/* upctl_data_T_main_Probe_Counters_1_AlarmMode.Counters_1_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE__COUNTERS_1_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE__COUNTERS_1_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE__COUNTERS_1_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE__COUNTERS_1_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_ALARM_MODE__COUNTERS_1_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_1_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL 0x10820554

/* upctl_data_T_main_Probe_Counters_1_Val.Counters_1_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL__COUNTERS_1_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL__COUNTERS_1_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL__COUNTERS_1_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL__COUNTERS_1_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_1_VAL__COUNTERS_1_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_2_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC 0x10820560

/* upctl_data_T_main_Probe_Counters_2_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_2_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE 0x10820564

/* upctl_data_T_main_Probe_Counters_2_AlarmMode.Counters_2_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE__COUNTERS_2_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE__COUNTERS_2_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE__COUNTERS_2_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE__COUNTERS_2_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_ALARM_MODE__COUNTERS_2_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_2_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL 0x10820568

/* upctl_data_T_main_Probe_Counters_2_Val.Counters_2_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL__COUNTERS_2_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL__COUNTERS_2_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL__COUNTERS_2_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL__COUNTERS_2_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_2_VAL__COUNTERS_2_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_3_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC 0x10820574

/* upctl_data_T_main_Probe_Counters_3_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_3_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE 0x10820578

/* upctl_data_T_main_Probe_Counters_3_AlarmMode.Counters_3_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE__COUNTERS_3_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE__COUNTERS_3_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE__COUNTERS_3_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE__COUNTERS_3_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_ALARM_MODE__COUNTERS_3_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_3_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL 0x1082057C

/* upctl_data_T_main_Probe_Counters_3_Val.Counters_3_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL__COUNTERS_3_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL__COUNTERS_3_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL__COUNTERS_3_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL__COUNTERS_3_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_3_VAL__COUNTERS_3_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_4_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC 0x10820588

/* upctl_data_T_main_Probe_Counters_4_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_4_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE 0x1082058C

/* upctl_data_T_main_Probe_Counters_4_AlarmMode.Counters_4_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE__COUNTERS_4_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE__COUNTERS_4_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE__COUNTERS_4_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE__COUNTERS_4_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_ALARM_MODE__COUNTERS_4_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_4_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL 0x10820590

/* upctl_data_T_main_Probe_Counters_4_Val.Counters_4_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL__COUNTERS_4_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL__COUNTERS_4_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL__COUNTERS_4_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL__COUNTERS_4_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_4_VAL__COUNTERS_4_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_5_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC 0x1082059C

/* upctl_data_T_main_Probe_Counters_5_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_5_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE 0x108205A0

/* upctl_data_T_main_Probe_Counters_5_AlarmMode.Counters_5_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE__COUNTERS_5_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE__COUNTERS_5_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE__COUNTERS_5_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE__COUNTERS_5_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_ALARM_MODE__COUNTERS_5_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_5_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL 0x108205A4

/* upctl_data_T_main_Probe_Counters_5_Val.Counters_5_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL__COUNTERS_5_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL__COUNTERS_5_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL__COUNTERS_5_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL__COUNTERS_5_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_5_VAL__COUNTERS_5_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_6_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC 0x108205B0

/* upctl_data_T_main_Probe_Counters_6_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_6_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE 0x108205B4

/* upctl_data_T_main_Probe_Counters_6_AlarmMode.Counters_6_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE__COUNTERS_6_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE__COUNTERS_6_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE__COUNTERS_6_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE__COUNTERS_6_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_ALARM_MODE__COUNTERS_6_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_6_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL 0x108205B8

/* upctl_data_T_main_Probe_Counters_6_Val.Counters_6_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL__COUNTERS_6_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL__COUNTERS_6_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL__COUNTERS_6_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL__COUNTERS_6_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_6_VAL__COUNTERS_6_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_7_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC 0x108205C4

/* upctl_data_T_main_Probe_Counters_7_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_7_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE 0x108205C8

/* upctl_data_T_main_Probe_Counters_7_AlarmMode.Counters_7_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE__COUNTERS_7_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE__COUNTERS_7_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE__COUNTERS_7_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE__COUNTERS_7_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_ALARM_MODE__COUNTERS_7_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_7_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL 0x108205CC

/* upctl_data_T_main_Probe_Counters_7_Val.Counters_7_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL__COUNTERS_7_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL__COUNTERS_7_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL__COUNTERS_7_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL__COUNTERS_7_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_7_VAL__COUNTERS_7_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_8_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC 0x108205D8

/* upctl_data_T_main_Probe_Counters_8_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_8_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE 0x108205DC

/* upctl_data_T_main_Probe_Counters_8_AlarmMode.Counters_8_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE__COUNTERS_8_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE__COUNTERS_8_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE__COUNTERS_8_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE__COUNTERS_8_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_ALARM_MODE__COUNTERS_8_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_8_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL 0x108205E0

/* upctl_data_T_main_Probe_Counters_8_Val.Counters_8_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL__COUNTERS_8_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL__COUNTERS_8_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL__COUNTERS_8_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL__COUNTERS_8_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_8_VAL__COUNTERS_8_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_9_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC 0x108205EC

/* upctl_data_T_main_Probe_Counters_9_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_9_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE 0x108205F0

/* upctl_data_T_main_Probe_Counters_9_AlarmMode.Counters_9_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE__COUNTERS_9_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE__COUNTERS_9_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE__COUNTERS_9_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE__COUNTERS_9_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_ALARM_MODE__COUNTERS_9_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_9_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL 0x108205F4

/* upctl_data_T_main_Probe_Counters_9_Val.Counters_9_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL__COUNTERS_9_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL__COUNTERS_9_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL__COUNTERS_9_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL__COUNTERS_9_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_9_VAL__COUNTERS_9_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_10_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC 0x10820600

/* upctl_data_T_main_Probe_Counters_10_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_10_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE 0x10820604

/* upctl_data_T_main_Probe_Counters_10_AlarmMode.Counters_10_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE__COUNTERS_10_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE__COUNTERS_10_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE__COUNTERS_10_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE__COUNTERS_10_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_ALARM_MODE__COUNTERS_10_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_10_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL 0x10820608

/* upctl_data_T_main_Probe_Counters_10_Val.Counters_10_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL__COUNTERS_10_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL__COUNTERS_10_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL__COUNTERS_10_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL__COUNTERS_10_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_10_VAL__COUNTERS_10_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_11_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC 0x10820614

/* upctl_data_T_main_Probe_Counters_11_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_11_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE 0x10820618

/* upctl_data_T_main_Probe_Counters_11_AlarmMode.Counters_11_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE__COUNTERS_11_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE__COUNTERS_11_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE__COUNTERS_11_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE__COUNTERS_11_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_ALARM_MODE__COUNTERS_11_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_11_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL 0x1082061C

/* upctl_data_T_main_Probe_Counters_11_Val.Counters_11_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL__COUNTERS_11_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL__COUNTERS_11_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL__COUNTERS_11_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL__COUNTERS_11_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_11_VAL__COUNTERS_11_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_12_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC 0x10820628

/* upctl_data_T_main_Probe_Counters_12_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_12_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE 0x1082062C

/* upctl_data_T_main_Probe_Counters_12_AlarmMode.Counters_12_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE__COUNTERS_12_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE__COUNTERS_12_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE__COUNTERS_12_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE__COUNTERS_12_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_ALARM_MODE__COUNTERS_12_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_12_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL 0x10820630

/* upctl_data_T_main_Probe_Counters_12_Val.Counters_12_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL__COUNTERS_12_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL__COUNTERS_12_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL__COUNTERS_12_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL__COUNTERS_12_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_12_VAL__COUNTERS_12_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_13_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC 0x1082063C

/* upctl_data_T_main_Probe_Counters_13_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_13_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE 0x10820640

/* upctl_data_T_main_Probe_Counters_13_AlarmMode.Counters_13_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE__COUNTERS_13_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE__COUNTERS_13_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE__COUNTERS_13_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE__COUNTERS_13_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_ALARM_MODE__COUNTERS_13_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_13_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL 0x10820644

/* upctl_data_T_main_Probe_Counters_13_Val.Counters_13_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL__COUNTERS_13_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL__COUNTERS_13_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL__COUNTERS_13_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL__COUNTERS_13_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_13_VAL__COUNTERS_13_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_14_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC 0x10820650

/* upctl_data_T_main_Probe_Counters_14_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_14_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE 0x10820654

/* upctl_data_T_main_Probe_Counters_14_AlarmMode.Counters_14_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE__COUNTERS_14_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE__COUNTERS_14_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE__COUNTERS_14_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE__COUNTERS_14_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_ALARM_MODE__COUNTERS_14_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_14_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL 0x10820658

/* upctl_data_T_main_Probe_Counters_14_Val.Counters_14_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL__COUNTERS_14_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL__COUNTERS_14_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL__COUNTERS_14_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL__COUNTERS_14_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_14_VAL__COUNTERS_14_VAL__HW_DEFAULT  0x0

/* upctl_data_T_main_Probe_Counters_15_Src */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC 0x10820664

/* upctl_data_T_main_Probe_Counters_15_Src.IntEvent - Internal packet event */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC__INT_EVENT__SHIFT      0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC__INT_EVENT__WIDTH      5
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC__INT_EVENT__MASK       0x0000001F
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC__INT_EVENT__INV_MASK   0xFFFFFFE0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_SRC__INT_EVENT__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_15_AlarmMode */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE 0x10820668

/* upctl_data_T_main_Probe_Counters_15_AlarmMode.Counters_15_AlarmMode - Register AlarmMode is a 2-bit register that is present when parameter statisticsCounterAlarm is set to True. The register defines the statistics-alarm behavior of the counter. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE__COUNTERS_15_ALARM_MODE__SHIFT     0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE__COUNTERS_15_ALARM_MODE__WIDTH     2
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE__COUNTERS_15_ALARM_MODE__MASK      0x00000003
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE__COUNTERS_15_ALARM_MODE__INV_MASK  0xFFFFFFFC
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_ALARM_MODE__COUNTERS_15_ALARM_MODE__HW_DEFAULT 0x0

/* upctl_data_T_main_Probe_Counters_15_Val */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL 0x1082066C

/* upctl_data_T_main_Probe_Counters_15_Val.Counters_15_Val - Register Val is a read-only register that is always present. The register containsthe statistics counter value either pending StatAlarm output, or when statisticscollection is suspended subsequent to triggers or signal statSuspend. */
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL__COUNTERS_15_VAL__SHIFT       0
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL__COUNTERS_15_VAL__WIDTH       16
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL__COUNTERS_15_VAL__MASK        0x0000FFFF
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL__COUNTERS_15_VAL__INV_MASK    0xFFFF0000
#define UPCTL_DATA_T_MAIN_PROBE_COUNTERS_15_VAL__COUNTERS_15_VAL__HW_DEFAULT  0x0

/* ddrm_sb_main_SidebandManager_Id_CoreId */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID 0x10820800

/* ddrm_sb_main_SidebandManager_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__SHIFT   0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__WIDTH   8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__MASK    0x000000FF
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0xB

/* ddrm_sb_main_SidebandManager_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__SHIFT    8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__WIDTH    24
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__MASK     0xFFFFFF00
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0x412E0E

/* ddrm_sb_main_SidebandManager_Id_RevisionId */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID 0x10820804

/* ddrm_sb_main_SidebandManager_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__SHIFT    0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__WIDTH    8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__MASK     0x000000FF
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__SHIFT   8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__WIDTH   24
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__MASK    0xFFFFFF00
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_sb_main_SidebandManager_FaultEn */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN 0x10820808

/* ddrm_sb_main_SidebandManager_FaultEn.FaultEn - Global Fault Enable register */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN__FAULT_EN__SHIFT    0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN__FAULT_EN__WIDTH    1
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN__FAULT_EN__MASK     0x00000001
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN__FAULT_EN__INV_MASK 0xFFFFFFFE
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_EN__FAULT_EN__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FaultStatus */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS 0x1082080C

/* ddrm_sb_main_SidebandManager_FaultStatus.FaultStatus - Global Fault Status register */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS__FAULT_STATUS__SHIFT    0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS__FAULT_STATUS__WIDTH    1
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS__FAULT_STATUS__MASK     0x00000001
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS__FAULT_STATUS__INV_MASK 0xFFFFFFFE
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FAULT_STATUS__FAULT_STATUS__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FlagInEn0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0 0x10820810

/* ddrm_sb_main_SidebandManager_FlagInEn0.FlagInEn0 - FlagIn Enable register #0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0__FLAG_IN_EN0__SHIFT  0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0__FLAG_IN_EN0__WIDTH  3
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0__FLAG_IN_EN0__MASK   0x00000007
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0__FLAG_IN_EN0__INV_MASK 0xFFFFFFF8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_EN0__FLAG_IN_EN0__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FlagInStatus0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0 0x10820814

/* ddrm_sb_main_SidebandManager_FlagInStatus0.FlagInStatus0 - FlagIn Status register #0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0__FLAG_IN_STATUS0__SHIFT  0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0__FLAG_IN_STATUS0__WIDTH  3
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0__FLAG_IN_STATUS0__MASK   0x00000007
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0__FLAG_IN_STATUS0__INV_MASK 0xFFFFFFF8
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_IN_STATUS0__FLAG_IN_STATUS0__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FlagOutSet0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0 0x10820850

/* ddrm_sb_main_SidebandManager_FlagOutSet0.FlagOutSet0 - FlagOut Set register #0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__SHIFT  0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__WIDTH  5
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__MASK   0x0000001F
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__INV_MASK 0xFFFFFFE0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FlagOutClr0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0 0x10820854

/* ddrm_sb_main_SidebandManager_FlagOutClr0.FlagOutClr0 - FlagOut Clr register #0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__SHIFT  0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__WIDTH  5
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__MASK   0x0000001F
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__INV_MASK 0xFFFFFFE0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__HW_DEFAULT 0x0

/* ddrm_sb_main_SidebandManager_FlagOutStatus0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0 0x10820858

/* ddrm_sb_main_SidebandManager_FlagOutStatus0.FlagOutStatus0 - FlagOut Status register #0 */
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__SHIFT  0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__WIDTH  5
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__MASK   0x0000001F
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__INV_MASK 0xFFFFFFE0
#define DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__HW_DEFAULT 0x8

/* ddrm_sb_dram_main_SidebandManager_Id_CoreId */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID 0x10821000

/* ddrm_sb_dram_main_SidebandManager_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__SHIFT   0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__WIDTH   8
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__MASK    0x000000FF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0xB

/* ddrm_sb_dram_main_SidebandManager_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__SHIFT    8
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__WIDTH    24
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__MASK     0xFFFFFF00
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0xFABC6D

/* ddrm_sb_dram_main_SidebandManager_Id_RevisionId */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID 0x10821004

/* ddrm_sb_dram_main_SidebandManager_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__SHIFT    0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__WIDTH    8
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__MASK     0x000000FF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__SHIFT   8
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__WIDTH   24
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__MASK    0xFFFFFF00
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0 0x10821050

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet0.FlagOutSet0 - FlagOut Set register #0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0 0x10821054

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr0.FlagOutClr0 - FlagOut Clr register #0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0 0x10821058

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus0.FlagOutStatus0 - FlagOut Status register #0 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1 0x1082105C

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet1.FlagOutSet1 - FlagOut Set register #1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1 0x10821060

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr1.FlagOutClr1 - FlagOut Clr register #1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1 0x10821064

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus1.FlagOutStatus1 - FlagOut Status register #1 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2 0x10821068

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet2.FlagOutSet2 - FlagOut Set register #2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2 0x1082106C

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr2.FlagOutClr2 - FlagOut Clr register #2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2 0x10821070

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus2.FlagOutStatus2 - FlagOut Status register #2 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3 0x10821074

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet3.FlagOutSet3 - FlagOut Set register #3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3 0x10821078

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr3.FlagOutClr3 - FlagOut Clr register #3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3 0x1082107C

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus3.FlagOutStatus3 - FlagOut Status register #3 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4 0x10821080

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet4.FlagOutSet4 - FlagOut Set register #4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4 0x10821084

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr4.FlagOutClr4 - FlagOut Clr register #4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4 0x10821088

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus4.FlagOutStatus4 - FlagOut Status register #4 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5 0x1082108C

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet5.FlagOutSet5 - FlagOut Set register #5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5__FLAG_OUT_SET5__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5__FLAG_OUT_SET5__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5__FLAG_OUT_SET5__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5__FLAG_OUT_SET5__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET5__FLAG_OUT_SET5__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5 0x10821090

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr5.FlagOutClr5 - FlagOut Clr register #5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5__FLAG_OUT_CLR5__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5__FLAG_OUT_CLR5__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5__FLAG_OUT_CLR5__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5__FLAG_OUT_CLR5__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR5__FLAG_OUT_CLR5__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5 0x10821094

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus5.FlagOutStatus5 - FlagOut Status register #5 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5__FLAG_OUT_STATUS5__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5__FLAG_OUT_STATUS5__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5__FLAG_OUT_STATUS5__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5__FLAG_OUT_STATUS5__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS5__FLAG_OUT_STATUS5__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6 0x10821098

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet6.FlagOutSet6 - FlagOut Set register #6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6__FLAG_OUT_SET6__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6__FLAG_OUT_SET6__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6__FLAG_OUT_SET6__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6__FLAG_OUT_SET6__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET6__FLAG_OUT_SET6__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6 0x1082109C

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr6.FlagOutClr6 - FlagOut Clr register #6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6__FLAG_OUT_CLR6__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6__FLAG_OUT_CLR6__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6__FLAG_OUT_CLR6__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6__FLAG_OUT_CLR6__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR6__FLAG_OUT_CLR6__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6 0x108210A0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus6.FlagOutStatus6 - FlagOut Status register #6 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6__FLAG_OUT_STATUS6__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6__FLAG_OUT_STATUS6__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6__FLAG_OUT_STATUS6__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6__FLAG_OUT_STATUS6__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS6__FLAG_OUT_STATUS6__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7 0x108210A4

/* ddrm_sb_dram_main_SidebandManager_FlagOutSet7.FlagOutSet7 - FlagOut Set register #7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7__FLAG_OUT_SET7__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7__FLAG_OUT_SET7__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7__FLAG_OUT_SET7__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7__FLAG_OUT_SET7__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET7__FLAG_OUT_SET7__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7 0x108210A8

/* ddrm_sb_dram_main_SidebandManager_FlagOutClr7.FlagOutClr7 - FlagOut Clr register #7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7__FLAG_OUT_CLR7__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7__FLAG_OUT_CLR7__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7__FLAG_OUT_CLR7__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7__FLAG_OUT_CLR7__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR7__FLAG_OUT_CLR7__HW_DEFAULT 0x0

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7 0x108210AC

/* ddrm_sb_dram_main_SidebandManager_FlagOutStatus7.FlagOutStatus7 - FlagOut Status register #7 */
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7__FLAG_OUT_STATUS7__SHIFT  0
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7__FLAG_OUT_STATUS7__WIDTH  32
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7__FLAG_OUT_STATUS7__MASK   0xFFFFFFFF
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7__FLAG_OUT_STATUS7__INV_MASK 0x00000000
#define DDRM_SB_DRAM_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS7__FLAG_OUT_STATUS7__HW_DEFAULT 0xFFFFFFFF

/* ddrm_sb_reg_main_SidebandManager_Id_CoreId */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID 0x10822000

/* ddrm_sb_reg_main_SidebandManager_Id_CoreId.CoreTypeId - Field identifying the type of IP. */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__SHIFT   0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__WIDTH   8
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__MASK    0x000000FF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_TYPE_ID__HW_DEFAULT 0xB

/* ddrm_sb_reg_main_SidebandManager_Id_CoreId.CoreChecksum - Field containing a checksum of the parameters of the IP. */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__SHIFT    8
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__WIDTH    24
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__MASK     0xFFFFFF00
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__INV_MASK 0x000000FF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_CORE_ID__CORE_CHECKSUM__HW_DEFAULT 0xBF2274

/* ddrm_sb_reg_main_SidebandManager_Id_RevisionId */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID 0x10822004

/* ddrm_sb_reg_main_SidebandManager_Id_RevisionId.UserId - Field containing a user defined value, not used anywhere inside the IP itself. */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__SHIFT    0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__WIDTH    8
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__MASK     0x000000FF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__INV_MASK 0xFFFFFF00
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__USER_ID__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_Id_RevisionId.FlexNocId - Field containing the build revision of the software used to generate the IP HDL code. */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__SHIFT   8
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__WIDTH   24
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__MASK    0xFFFFFF00
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__INV_MASK 0x000000FF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_ID_REVISION_ID__FLEX_NOC_ID__HW_DEFAULT 0x148

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0 0x10822050

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet0.FlagOutSet0 - FlagOut Set register #0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0__FLAG_OUT_SET0__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0 0x10822054

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr0.FlagOutClr0 - FlagOut Clr register #0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR0__FLAG_OUT_CLR0__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0 0x10822058

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus0.FlagOutStatus0 - FlagOut Status register #0 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS0__FLAG_OUT_STATUS0__HW_DEFAULT 0xFFFF

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1 0x1082205C

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet1.FlagOutSet1 - FlagOut Set register #1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET1__FLAG_OUT_SET1__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1 0x10822060

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr1.FlagOutClr1 - FlagOut Clr register #1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR1__FLAG_OUT_CLR1__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1 0x10822064

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus1.FlagOutStatus1 - FlagOut Status register #1 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS1__FLAG_OUT_STATUS1__HW_DEFAULT 0xFFFF

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2 0x10822068

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet2.FlagOutSet2 - FlagOut Set register #2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET2__FLAG_OUT_SET2__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2 0x1082206C

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr2.FlagOutClr2 - FlagOut Clr register #2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR2__FLAG_OUT_CLR2__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2 0x10822070

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus2.FlagOutStatus2 - FlagOut Status register #2 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS2__FLAG_OUT_STATUS2__HW_DEFAULT 0xFFFF

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3 0x10822074

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet3.FlagOutSet3 - FlagOut Set register #3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET3__FLAG_OUT_SET3__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3 0x10822078

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr3.FlagOutClr3 - FlagOut Clr register #3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR3__FLAG_OUT_CLR3__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3 0x1082207C

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus3.FlagOutStatus3 - FlagOut Status register #3 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS3__FLAG_OUT_STATUS3__HW_DEFAULT 0xFFFF

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4 0x10822080

/* ddrm_sb_reg_main_SidebandManager_FlagOutSet4.FlagOutSet4 - FlagOut Set register #4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET4__FLAG_OUT_SET4__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4 0x10822084

/* ddrm_sb_reg_main_SidebandManager_FlagOutClr4.FlagOutClr4 - FlagOut Clr register #4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_CLR4__FLAG_OUT_CLR4__HW_DEFAULT 0x0

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4 0x10822088

/* ddrm_sb_reg_main_SidebandManager_FlagOutStatus4.FlagOutStatus4 - FlagOut Status register #4 */
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__SHIFT  0
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__WIDTH  32
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__MASK   0xFFFFFFFF
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__INV_MASK 0x00000000
#define DDRM_SB_REG_MAIN_SIDEBAND_MANAGER_FLAG_OUT_STATUS4__FLAG_OUT_STATUS4__HW_DEFAULT 0xFFFF


#endif  // __NOCDDRM_H__
