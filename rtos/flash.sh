#!/bin/bash
# 
# Copyright (c) 2016, The Linux Foundation. All rights reserved.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 and only version 2 as published by the Free Software
# Foundation. This program is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details. 
# 
# Flashing images and/or filesystem to SDCARD or NAND disk
# Usage flash.sh <devfile> [<targets>]
#
# devfile may be any of: sdb, sdc,..., which is the name of your
#   SDCARD or NAND disk device
# targets may be one or more of:
#  uboot zimage recovery images modules rootfs system data
#  images
#  kernel
#  all
#

readonly usage="\
Flashing images and filesystems to SDCARD or NAND disk for CSR
Linux-based systems. Copyright (C) 2011-2013 CSR plc. Version: 1.30

Usage: $0 <devfile> [ostype=<ostype>] [<targets>]

   or: $0 <devfile> [ostype=<ostype>] kernel
				kernel include zimage and modules

   or: $0 <devfile> [ostype=<ostype>] images
				images include n/uboot, zimage

   or: $0 <devfile> [ostype=<ostype>] all
				include all images and filesystems,
				will create partitions if not available

   or: $0 <devfile> [ostype=<ostype>]
				include all images and kernel modules

Description:
	devfile:
		sdb, sdc, ..., which is the name of your SDCARD or
		NANDDisk device
	ostype:
		currently only support android, linux
		if ./system exists, default ostype is android
		else, default ostype is linux
	targets:
		targets can be one or more of
		uboot, zimage, recovery,
		images, modules, rootfs,
		system and data

Examples:
	$0 sdb linux images
	$0 sdb images
	$0 sdb kernel
	$0 sdb all
	$0 sdb ostype=linux all
"

show_help()
{
	echo -e "$usage"
}

#
# macros used in this file
#
readonly size_1K=1024
readonly size_1M=$((1024*1024))
if [ "$(which mkfs.ext4)" = "" ]; then
readonly rootfs_type="ext3"
else
readonly rootfs_type="ext4"
fi

#
# image file name stored in boot partition
#
readonly uboot=u-boot.csr
readonly zimage=zImage
readonly dtb=dtb
readonly scert=nothing
readonly rimage=recovery.img

if [ $# -lt 1 ]; then
	show_help
	exit 1
fi

for i in $*
do
	case $i in
	ostype*)
		ostype="`echo $i | cut -d'=' -f2`"
		echo "ostype=$ostype"
		;;
	*)
		;;
	esac
done

is_default_ostype=0
if ! test -n "${ostype}"; then
	if test -d system ; then
		ostype=android
		is_default_ostype=1
	else
		ostype=linux
		is_default_ostype=1
	fi
	echo "set ostype to $ostype as default"
fi

#
# device file & path
#
readonly devfile=$1
readonly devpath=/dev/$devfile
readonly rawdev=${devpath}1 #raw partition
readonly raw_part_size=5
readonly bootdev=${devpath}2 # boot partition
readonly gpt=$3
if [ "${ostype}" = "android" ]; then
	swapdev=${devpath}3 # swap partition for hibernation
	rootdev=${devpath}5 # root partition
	systdev=${devpath}6 # system partition (android)
	datadev=${devpath}7 # data partition (android)
	userdev=${devpath}8 # user partition
	boot_part_size=30
	root_part_size=256
	syst_part_size=256
	data_part_size=1024
	swap_part_size=160
fi
if [ "${ostype}" = "linux" ]; then
	#swapdev=${devpath}3 # swap partition for hibernation
	datadev=${devpath}5 # data partition
	rootdev=${devpath}3 # root partition
	userdev=${devpath}6 # user partition
	boot_part_size=30
	root_part_size=256
	#swap_part_size=160
	data_part_size=160
	raw_msize=6
	boot_msize=32
	extended_msize=1
	swap_msize=168
	root_msize=268
fi

#
# config file
#
readonly configfile=.config

if [ $# = 0 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
	show_help
	exit 1
fi

if [ ! "$(whoami)" = "root" ]; then
	echo "Permission denied. Please use 'sudo $0 ...'"
	exit 1
fi

if ! fdisk -s $devpath 2>/dev/null 1>/dev/null; then
	echo -e "invalid block device: '$devpath'\n"
	show_help
	exit 1
fi

readonly removable=$(cat /sys/block/$devfile/removable)

if [ ! "$removable" = "1" ]; then
	echo -e "'$devpath' is not your SDCARD or NAND disk! Please be careful!\a"
	exit 1
fi

readonly sector_size=$(cat /sys/block/$devfile/queue/hw_sector_size)

#
# ---------------
# android storage layout:
# ---------------
#
# sector[0]====================================
#         |  MBR         - (512B~16KB)         |
# sector[1]------------------------------------
#         |  uboot - (2MB-$sector_size)        |
#     [2MB]------------------------------------
#         |  uboot commit flag sector          |
#[2.5MB-4K]------------------------------------
#         |  recovery flag sector              |
#	   ------------------------------------
#         |  			               |
#[3MB-64KB]------------------------------------
#         |  uboot environment variables area  |
#     [3MB]====================================
#         |  scert area                        |
#     [5MB]====================================
#         |  boot partition (ext3/ext4 - ro)   |
#     [...]====================================
#         |  swap part      (swap)             |
#     [...]====================================
#         |  rootfs partition (ext3/ext4 - ro) |
#     [...]====================================
#         |  system partition (ext3/ext4 - ro) |
#         |  android only                      |
#     [...]====================================
#         |  data partition (ext3/ext4 - rw)   |
#     [...]====================================
#         |  user partition (fat32... - rw)    |
#          ====================================
#

#
# ---------------
# linux storage layout:
# ---------------
#
# sector[0]====================================
#         |  MBR         - (512B~16KB)         |
# sector[1]------------------------------------
#         |  uboot - (2MB-$sector_size)        |
#     [2MB]------------------------------------
#         |  uboot commit flag sector          |
#[2.5MB-4K]------------------------------------
#         |  recovery flag sector              |
#	   ------------------------------------
#         |  			               |
#[3MB-64KB]------------------------------------
#         |  uboot environment variables area  |
#     [3MB]====================================
#         |  scert area                        |
#     [5MB]====================================
#         |  boot partition (ext3/ext4 - ro)   |
#     [...]====================================
#         |  rootfs partition (ext3/ext4 - ro  |
#     [...]====================================
#         |  data partition (ext3/ext4 - rw)   |
#     [...]====================================
#         |  user partition (fat32... - rw)    |
#          ====================================
#

#
# storage max reserve length
#
readonly uboot_max_length=$((2*$size_1M-$sector_size))
readonly uboot_env_length=$((64*$size_1K))
readonly scert_max_length=$((2*$size_1M))
readonly rcv_flg_length=$((1*$sector_size))

# uboot begin sector
readonly uboot_beg_sector=1
# uboot commit flag sector (only used by nanddisk)
readonly uboot_flg_sector=$((2*$size_1M/$sector_size))
# uboot environment start sector
readonly uboot_env_sector=$(((3*$size_1M-64*$size_1K)/$sector_size))

# recovery flag sector
readonly rcv_flg_sector=$(((2*$size_1M+$size_1M/2-4*$size_1K)/$sector_size))

#scert begin sector
readonly scert_beg_sector=$(((3*$size_1M)/$sector_size))

# current boot partition size in MBs
min_boot_part_size=30

#
# bytes_to_sectors(bytes)
#    bytes must sector size aligned
bytes_to_sectors()
{
	echo $(($1 / $sector_size))
}

#
# file_length_bytes(file)
#
file_length_bytes()
{
	echo $(($(stat -c %s $1 2>/dev/null)))
}

#
# file_length_sectors(file)
#
file_length_sectors()
{
	echo $((($(file_length_bytes $1) + $sector_size - 1) / $sector_size))
}

#
# file_out_of_range(file, bytes)
#
file_out_of_range()
{
	if ! test -e $1; then
		echo "$1: No such file"
		return
	fi

	local length=$(file_length_bytes $1)
	if [ $length -gt $2 ]; then
		echo "error: $1 out of range ($length bytes > $2 bytes)!"
	fi
}

#
# erase_sector(start, count)
#
erase_sector()
{
	echo erase_sector\($1, $2\)
	dd seek=$1 count=$2 bs=$sector_size if=/dev/zero of=$devpath 1>/dev/null 2>&1
}

#
# erase_rcv_flag()
#
erase_rcv_flag()
{
	echo "erase_rcv_flag"
	erase_sector $rcv_flg_sector 1
}

#
# write_file(start_sector, file)
#
write_file()
{
	echo "write_file($1, $2)"
	if ! test -e $2 ; then
		echo "$2: No such file"
		return
	fi
	local count=$(file_length_sectors $2)
	dd seek=$1 if=$2 of=$devpath bs=$sector_size count=$count 1>/dev/null 2>&1
}

#
# flash_uboot()
#
flash_uboot()
{
	local err=$(file_out_of_range $uboot $uboot_max_length)
	if [ ! "$err" = "" ]; then
		echo $err
		return
	fi
	# erase environment variables
	if [ ! "$gpt" = "gpt" ]; then
		erase_sector $uboot_env_sector $(($uboot_env_length/$sector_size))
	fi
	# write u-boot
	write_file $uboot_beg_sector $uboot
	sync
	# commit u-boot to NAND flash - required by CSR proprietary NANDdisk
	if [ ! "$gpt" = "gpt" ]; then
		erase_sector $uboot_flg_sector 1
	fi
}

#
# flash_nor_uboot()
#
flash_nor_uboot()
{
	# write u-boot into spi nor flash
	write_file 0 $uboot
	sync
}

#
# mount_partition(mountdev, mountdir, vfstype)
#
mount_partition()
{
	local mountdev=$1
	local mountdir=$2
	local vfstype=$3

	if [ "$vfstype" = "" ]; then
		vfstype=$rootfs_type
	fi

	if [ "$mountdir" = "" ]; then
		return
	fi

	mkdir -p $mountdir 2>/dev/null
	# umount old directory
	mount | grep $mountdev | awk {'print $3'} | xargs umount 2>/dev/null
	if mount -t $vfstype $mountdev $mountdir 2>/dev/null; then
		echo done
	else
		rm -r $mountdir
	fi
}

#
# umount_partition(mountdir)
#
umount_partition()
{
	local mountdir=$1
	if umount -fl $mountdir; then
		rm -r $mountdir
		echo done
	fi
}

#
# copy_csrvisor()
#
copy_csrvisor()
{
	if [ ! "$(mount_partition $bootdev disk)" = "" ]; then
		echo -n "copying" csrvisor.bin "..."
	        cp -f csrvisor.bin disk/
		echo "done"

		sync
	        umount_partition disk
	fi
}


#
# copy_zimage()
#
copy_zimage()
{
	if [ ! "$(mount_partition $bootdev disk)" = "" ]; then
		echo -n "copying" $zimage "..."
	        cp -f $zimage disk/"${zimage}"-v1
	        cp -f $zimage disk/"${zimage}"-v2
		echo "done"

		echo -n "copying" $dtb "..."
	        cp -f $dtb disk/"${dtb}"-v1
	        cp -f $dtb disk/"${dtb}"-v2
		echo "done"

		echo -n "update boot config ..."
		if test -e disk/boot.cfg; then
			if [ "`grep kernel_index disk/boot.cfg`" = "" ]; then
				echo "kernel_index=1;" >> disk/boot.cfg
			else
			sed -i '/kernel_index/ s/^.*$/kernel_index=1;/' disk/boot.cfg
			fi
		else
			touch disk/boot.cfg
			echo "kernel_index=1;" >> disk/boot.cfg
		fi

		sync
	        umount_partition disk
	fi
}

#
# copy_modules()
#
copy_modules()
{
	if [ ! "$(mount_partition $rootdev disk)" = "" ]; then
		echo -n "copying kernel modules..."
		mkdir -p disk/lib/modules 2>/dev/null
		cp -fr rootfs/lib/modules/* disk/lib/modules/
		umount_partition disk
	fi
}

#
# copy_user_files()
#
copy_user_files()
{
	if [ ! "$(mount_partition $userdev disk vfat)" = "" ]; then
		echo -n "copying user files..."
		cp -f update.zip disk/ 2>/dev/null
		sync
		umount_partition disk
	fi
}

#
# copy_kernel()
#
copy_kernel()
{
	copy_zimage
	copy_modules
}

#
# flash scert
#
flash_scert()
{
	erase_sector $scert_beg_sector $(($scert_max_length/$sector_size))
}

#
# copy recovery.img
#
copy_recovery()
{
	if [ ! "$(mount_partition $bootdev disk)" = "" ]; then
		echo -n "copying" $rimage "..."
		cp -f $rimage disk/"${rimage}"-v1
		cp -f $rimage disk/"${rimage}"-v2
		echo "done"

		echo -n "copying" $dtb "..."
	        cp -f $dtb disk/rcv_"${dtb}"-v1
	        cp -f $dtb disk/rcv_"${dtb}"-v2
		echo "done"

		echo -n "update boot config ..."
		if test -e disk/boot.cfg; then
			if [ "`grep recovery_index disk/boot.cfg`" = "" ]; then
				echo "recovery_index=1;" >> disk/boot.cfg
			else
			sed -i '/recovery_index/ s/^.*$/recovery_index=1;/' disk/boot.cfg
			fi
		else
			touch disk/boot.cfg
			echo "recovery_index=1;" >> disk/boot.cfg
		fi

		sync
		umount_partition disk
	fi
}

#
# flash all images & kernel modules
#
flash_images()
{
	if [ ! "$gpt" = "gpt" ]; then
		flash_uboot
	fi
	copy_csrvisor
	copy_zimage
	flash_scert
	
	if test -e $rimage; then
		copy_recovery
	fi
}

#
# calc_min_parts
#
calc_min_parts()
{
	#
	# calculate min root partition size
	#
	if ! test -d rootfs; then return; fi
	min_root_part_size=$(expr $(du -bs rootfs | awk {'print $1'}) / $size_1M)
	min_root_part_size=$((($min_root_part_size + 59)/10*10)) # extra 50MB
	if [ $min_root_part_size -gt $root_part_size ]; then
		root_part_size=$min_root_part_size
	fi

	if [ "${ostype}" = "android" ]; then
		#
		# calculate min system partition size
		#
		if ! test -d system; then return; fi
		min_syst_part_size=$(expr $(du -bs system | awk {'print $1'}) / $size_1M)
		min_syst_part_size=$((($min_syst_part_size + 59)/10*10)) # extra 50MB
		if [ $min_syst_part_size -gt $syst_part_size ]; then
			syst_part_size=$min_syst_part_size
		fi

		#
		# calcuate min data partition size
		#
		if ! test -d data; then return; fi
		min_data_part_size=$(expr $(du -bs data | awk {'print $1'}) / $size_1M)
		min_data_part_size=$((($min_data_part_size + 59)/10*10)) # extra 50MB
		if [ $min_syst_part_size -gt $syst_part_size ]; then
			data_part_size=$min_data_part_size
		fi
	fi
}

#
# does the disk have valid partitions?
#
has_valid_partitions()
{
	local block_size=$size_1K
	local part_size=0

	if [ ! "$gpt" = "gpt" ] && [ "`parted -s $devpath print | grep msdos`" = "" ]; then
		echo no
		return
	elif [ "$gpt" = "gpt" ] && [ "`parted -s $devpath print | grep gpt`" = "" ]; then
		echo no
		return
	fi

	calc_min_parts >/dev/null
	if [ "${ostype}" = "android" ]; then
		for i in $rawdev $bootdev $rootdev $systdev $datadev $swapdev $userdev;  do
			if [ ! $(fdisk -s $i 2>/dev/null) ]; then
				echo no
				return
			fi
		done
		part_size=$(expr $(fdisk -s $rootdev) / $block_size)
		if [[ $part_size -lt $root_part_size ]]; then
			echo no
			return
		fi
		part_size=$(expr $(fdisk -s $systdev) / $block_size)
		if [[ $part_size -lt $syst_part_size ]]; then
			echo no
			return
		fi
		part_size=$(expr $(fdisk -s $datadev) / $block_size)
		if [[ $part_size -lt $data_part_size ]]; then
			echo no
			return
		fi
		part_size=$(expr $(fdisk -s $swapdev) / $block_size)
		if [[ $part_size -lt $swap_part_size ]]; then
			echo no
			return
		fi
	fi

	if [ "${ostype}" = "linux" ]; then
		for i in $rawdev $bootdev $rootdev $datadev $userdev;  do
			if [ ! $(fdisk -s $i 2>/dev/null) ]; then
				echo no
				return
			fi
		done
		part_size=$(expr $(fdisk -s $rootdev) / $block_size)
		if [[ $part_size -lt $root_part_size ]]; then
			echo no
			return
		fi
		part_size=$(expr $(fdisk -s $datadev) / $block_size)
		if [ $part_size -lt $data_part_size ] || [ "`mount | grep $datadev`" = "" ]; then
			echo no
			return
		fi
	fi

	part_size=$(expr $(fdisk -s $rawdev) / $block_size)
	if [[ $part_size -lt $raw_part_size ]]; then
		echo no
		return
	fi
	part_size=$(expr $(fdisk -s $bootdev) / $block_size)
	if [[ $part_size -lt $boot_part_size ]]; then
		echo no
		return
	fi
	echo yes
}

#
# umount all partitions
#
umount_all_partitions()
{
	mount | grep "$devpath" | awk {'print $3'} | xargs umount 2>/dev/null || true
}

#
# remove all partitions
#
remove_all_partitions()
{
	if [ ! "$gpt" = "gpt" ]; then
		fdisk -c -u -l $devpath | awk {'print $1'} | grep $devpath | \
		sed 's/[^0-9]//g' | xargs -i parted $devpath rm {} >/dev/null
	else
		parted -s $devpath print | awk {'print $1'} | grep [0-9] | \
		xargs -i parted $devpath rm {} >/dev/null
	fi
}

#
# create_partition(part_type, part_num, part_size)
#
create_partition()
{
	local part_type=$1
	local part_num=$2
	local part_size=$3

	if [ "$part_size" = "" ]; then
		show_size=+all
	else
		show_size=$part_size
	fi

	echo -n "create_partition($part_type, $part_num, $show_size)..."

	case $part_type in
	p|e|l);;
	*)
		echo $part_type not supported!;;
	esac

	echo	"n
	$part_type
	$part_num

	$part_size
	wq " |
	fdisk -c -u $devpath 1>/dev/null || (echo -n "Reloading partition table..."; sleep 1; partprobe $devpath;)
	echo "done"
}

create_partition_gpt()
{
	local part_type=$1
	local part_num=$2
	local start=$3
	local end=$4

	echo -n "create_partition($part_type, $part_num, $start, $end)..."

	case $part_type in
	primary);;
	*)
		echo $part_type not supported!;;
	esac

	parted -s $devpath mkpart $part_type $start $end 1>/dev/null || (echo -n "Reloading partition table..."; sleep 1; partprobe $devpath;)
	echo "done"
}

#
# format_partition(part_num, fs_type, label)
#
format_partition()
{
	local part_num=$1
	local fs_type=$2
	local label=$3

	echo -n "format_partition($part_num, $fs_type)..."

	for i in {1..5}; do
		if test -b ${devpath}${part_num}; then
			break
		fi
		sleep 1
	done

	case $fs_type in
	fat32)
		if [ ! "$gpt" = "gpt" ]; then
			echo "t
			$part_num
			c
			wq " | fdisk -c -u $devpath >/dev/null
			sleep 1
		fi

		# Default cluster size for FAT32 (Volume size: 256 MB - 8 GB)
		default_cluster_size=4096
		hw_sector_size=`blockdev --getss ${devpath}${part_num}`
		sectors_per_cluster=`expr $default_cluster_size / $hw_sector_size`

		if mkfs.vfat -F 32 -s $sectors_per_cluster ${devpath}${part_num} -n $label 1>/dev/null 2>/dev/null; then
			echo done
		else
			echo failed!
		fi
		;;
	swap)
		if mkswap ${devpath}${part_num}  1>/dev/null 2>/dev/null; then
			echo done
		else
			echo failed!
		fi
		;;
	*)
		if mkfs.$fs_type ${devpath}${part_num} -L $label 1>/dev/null 2>/dev/null; then
			echo done
		else
			echo failed!
		fi
		;;
	esac
}

create_all_partition_gpt()
{
	umount_all_partitions
	if ! parted -s $devpath mklabel "gpt"; then
		return
	fi

	calc_min_parts
	local size=` parted -s $devpath print | grep $devpath | awk {'print $3'} \
		| sed 's/MB//g' `
	local start=0
	local end=$raw_msize

	if [ "${ostype}" = "linux" ]; then
		create_partition_gpt primary 1 $start $end
		start=$end
		end=$(expr $end + $boot_msize)
		create_partition_gpt primary 2 $start $end
		start=$end
		end=$(expr $end + $swap_msize)
		create_partition_gpt primary 3 $start $end
		start=$end
		end=$(expr $end + $extended_msize)
		create_partition_gpt primary 4 $start $end
		start=$end
		end=$(expr $end + $root_msize)
		create_partition_gpt primary 5 $start $end
		create_partition_gpt primary 6 $end $size

		format_partition 2 $rootfs_type boot
		format_partition 3 swap
		format_partition 5 $rootfs_type root
		format_partition 6 fat32 user
	fi
}

#
# create_all_partitions required
#
create_all_partitions()
{
	if [ ! "$gpt" = "gpt" ]; then
		if ! parted -s $devpath mklabel "msdos"; then
			return
		fi
	else
		create_all_partition_gpt
		return
	fi

	calc_min_parts


	if [ "${ostype}" = "android" ]; then
		create_partition p 1 +${raw_part_size}M
		create_partition p 2 +${boot_part_size}M
		create_partition p 3 +${swap_part_size}M

		create_partition e 4
		create_partition l 5 +${root_part_size}M
		create_partition l 6 +${syst_part_size}M
		create_partition l 7 +${data_part_size}M
		create_partition l 8

		format_partition 2 $rootfs_type boot
		format_partition 3 swap
		format_partition 5 $rootfs_type root
		format_partition 6 $rootfs_type system
		format_partition 7 $rootfs_type data
		format_partition 8 fat32 user
	fi

	if [ "${ostype}" = "linux" ]; then
		create_partition p 1 +${raw_part_size}M
		create_partition p 2 +${boot_part_size}M
		create_partition p 3 +${root_part_size}M

		create_partition e 4
		create_partition l 5 +${data_part_size}M
		create_partition l 6 +${user_size}M

		format_partition 2 $rootfs_type boot
		format_partition 3 $rootfs_type root
		format_partition 5 $rootfs_type data
		format_partition 6 fat32 user
	fi
}

#
# generate test MAC address for Synergy WiFi
#
generate_mac()
{
	local dat=$(date +%H:%M:%S)

	local mac=$(find disk/etc/firmware/unifi-sdio/ -iname mac.txt 2>/dev/null)
	for i in $mac; do
		echo "00:02:5b:"$dat > $i
	done
}

#
# copy_filesystem(copydir partdev)
#
copy_filesystem()
{
	local copydir=$1
	local partdev=$2

	if ! test -d $copydir; then
		return
	fi

	if [[ ! $(mount_partition $partdev disk) ]]; then
		return
	fi

	echo -n "copying $copydir to $partdev..."
	rm -rf disk/*
	cp -fr $copydir/* disk/
	if [ "$partdev" = "$rootdev" ]; then
		if ! test -h system/etc; then
			if test -d system/etc; then
				cp -fr system/etc/* disk/etc/
			fi
		fi
		generate_mac
		mknod disk/dev/null c 1 3
		mknod disk/dev/console c 5 1
		mknod disk/dev/ttyS1 c 4 65
	elif [ "$partdev" = "$systdev" ]; then
		if ! test -h system/etc; then
			if test -d system/etc; then
				rm -fr disk/etc
				ln -fs /etc disk/etc
			fi
		fi
	fi
	umount_partition disk
}

#
# flash all images and rootfs
#
flash_all()
{
	if [ "`has_valid_partitions`" = "no" ]; then
		echo "-----------------------------------------------------"
		echo "PARTITIONS NOT VALID, WILL REMOVE AND RECREATE ALL!!!"
		echo "-----------------------------------------------------"
		umount_all_partitions
		remove_all_partitions
		create_all_partitions
	fi

	erase_rcv_flag
	flash_images
	if [ "${ostype}" = "linux" ]; then
		copy_filesystem rootfs $rootdev
	fi

	if [ "${ostype}" = "android" ]; then
		copy_filesystem rootfs $rootdev
		copy_filesystem system $systdev
		copy_filesystem data $datadev
	fi

	copy_user_files
	if test -e wifi_bt_files ; then
		./wifi_bt_files_storing.sh $devpath
	fi
}

#
# set os_type to boot config
#
set_os_type()
{
	os_type=$1
	if [ ! "$(mount_partition $bootdev disk)" = "" ]; then
		echo -n "update boot config of os type ..."
		if test -e disk/boot.cfg; then
			if [ "`grep os_type disk/boot.cfg`" = "" ]; then
				echo "os_type=${os_type};" >> disk/boot.cfg
			else
				sed -i '/os_type/ s/^.*$/os_type='${os_type}';/' disk/boot.cfg
			fi
		fi
		sync
	        umount_partition disk
	fi
}

#
# flash the production card for prodution mode
#
flash_production_card()
{
	echo -n "Making production card ... "
	umount_all_partitions
	remove_all_partitions
	flash_uboot
	fdisk $devpath 1>/dev/null 2>&1 << EOF
	n
	p
	1
	10240

	wq
EOF
	mkfs.vfat -F 32 ${devpath}1 -n boot  1>/dev/null 2>/dev/null
	if [ ! "$(mount_partition ${devpath}1 disk vfat)" = "" ]; then
		cp recovery.img disk/zImage
		cp ${dtb} disk/${dtb}
		touch disk/boot.cfg
		echo "kernel_index=1;" > disk/boot.cfg
		echo "recovery_index=1;" >> disk/boot.cfg
		echo "os_type=${ostype};" >> disk/boot.cfg
		touch disk/production.info
		echo "${ostype}" > disk/production.info
		cp update.zip disk/update.zip
		sync
		umount_partition disk
	fi
}

if [ "$is_default_ostype" = "1" ] && [ "$#" = "1" ] ; then
	flash_images
	copy_modules
fi

if [ "$is_default_ostype" = "0" ] && [ "$#" = "2" ]; then
	flash_images
	copy_modules
fi

for i in $*
do
	case $i in
	sd*)
		;;
	gpt)
		;;
	ostype*)
		;;
	uboot)
		flash_uboot
		;;
	nor-uboot)
		flash_nor_uboot
		;;
	csrvisor)
		copy_csrvisor
		;;
	zimage)
		copy_zimage
		;;
	modules)
		copy_modules
		;;
	kernel)
		copy_kernel
		;;
	recovery)
		copy_recovery
		;;
	rootfs)
		copy_filesystem rootfs $rootdev
		;;
	system)
		copy_filesystem system $systdev
		;;
	data)
		copy_filesystem data $datadev
		;;
	images)
		flash_images
		;;
	production)
		flash_production_card
		;;
	all)
		flash_all
		;;
	*)
		echo "wrong target: $i"
		exit 1
		;;
	esac
done
set_os_type $ostype
eject $devfile
