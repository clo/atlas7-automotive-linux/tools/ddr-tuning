/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

	.text
	.arm

.global DCache_CleanInvalidate
        .align 2
        .type DCache_CleanInvalidate, %function
DCache_CleanInvalidate:
        STMFD	sp!, {r0-r12,lr}
        MRC     p15, 1, r0, c0, c0, 1      /* Read CLIDR*/
        ANDS    r3, r0, #0x07000000        /* Extract coherency level*/
        MOV     r3, r3, LSR #23            /* Total cache levels << 1*/
        BEQ     DCIFinished                   /*# If 0, no need to clean*/

        MOV     r10, #0                    /*R10 holds current cache level << 1*/
DCILoop1:   ADD     r2, r10, r10, LSR #1       /* R2 holds cache "Set" position */
        MOV     r1, r0, LSR r2             /* Bottom 3 bits are the Cache-type for this level*/
        AND     r1, r1, #7                 /* Isolate those lower 3 bits*/
        CMP     r1, #2
        BLT     DCISkip                       /* No cache or only instruction cache at this level*/

        MCR     p15, 2, r10, c0, c0, 0     /* Write the Cache Size selection register*/
        ISB                                /*ISB to sync the change to the CacheSizeID reg*/
        MRC     p15, 1, r1, c0, c0, 0      /* Reads current Cache Size ID register*/
        AND     r2, r1, #7                 /*Extract the line length field*/
        ADD     r2, r2, #4                 /* Add 4 for the line length offset (log2 16 bytes)*/
        LDR     r4, =0x3FF
        ANDS    r4, r4, r1, LSR #3         /* R4 is the max number on the way size (right aligned)*/
        CLZ     r5, r4                     /*R5 is the bit position of the way size increment*/
        LDR     r7, =0x7FFF
        ANDS    r7, r7, r1, LSR #13        /* R7 is the max number of the index size (right aligned)*/

DCILoop2:   MOV     r9, r4                     /*R9 working copy of the max way size (right aligned)*/

DCILoop3:   ORR     r11, r10, r9, LSL r5       /* Factor in the Way number and cache number into R11*/
        ORR     r11, r11, r7, LSL r2       /* Factor in the Set number*/
        MCR     p15, 0, r11, c7, c14, 2     /* Clean and Invalidate by Set/Way*/
        SUBS    r9, r9, #1                 /*Decrement the Way number*/
        BGE     DCILoop3
        SUBS    r7, r7, #1                 /* Decrement the Set number*/
        BGE     DCILoop2
DCISkip:    ADD     r10, r10, #2               /* increment the cache number*/
        CMP     r3, r10
        BGT     DCILoop1

DCIFinished:
	DSB
        LDMFD	sp!,{r0-r12,lr}
        BX		lr
        .size  DCache_CleanInvalidate, .-DCache_CleanInvalidate


.end

