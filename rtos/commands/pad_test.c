/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "FreeRTOSConfig.h"
#if SUPPORT_QSPI_TEST

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

#include "chip_utils.h"
#include "std_funcs.h"

#include "soc.h"



#define DEBUG 0
#if DEBUG
#define WRITE_ADR(a,d)  printf("WRITE_REG 0x%08X: 0x%08x - %s\r\n",a,d,#a); WRITE_REG(a,d)
unsigned long read_reg(unsigned long a, char* name)   
{ 
    unsigned long d = READ_REG(a);
    printf("READ_REG  0x%08X: 0x%08x - %s\r\n",a,d, name);
    return d;
}
#define READ_ADR(a) read_reg(a,#a)
#else
#define WRITE_ADR WRITE_REG
#define READ_ADR READ_REG
#endif
#define MAXLOOPS 100000
#define XOTF_BASE_ADDR 0x20000000

/***************************************************************************************
                                   commands          
***************************************************************************************/
unsigned int GetParameterAsUlong(const char *pcCommandString,int paramNum);

static portBASE_TYPE prvMemTest( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
    unsigned long address;
	unsigned long size, pattern;
    unsigned long* ptr = NULL;
    int num_of_errors = 0;

	//Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	// write buffer length is adequate, so does not check for buffer overflows.
	configASSERT( pcWriteBuffer );
	address = GetParameterAsUlong(pcCommandString,1 );
	size = GetParameterAsUlong(pcCommandString,2 );
	pattern = GetParameterAsUlong(pcCommandString,3 );
	
    printf("\r\nWriting pattern to memory...\r\n");    
    for( ptr = (unsigned long*)address; (unsigned long)ptr < address+size; ptr += 4 )
    {
        *ptr = pattern;
    }
    printf("Reading memeory...\r\n");
    for( ptr = (unsigned long*)address; (unsigned long)ptr< address+size; ptr += 4 )
    {
        if( *ptr != pattern )
        {
            num_of_errors++;
        }
    }
    

	printf("Errors: %d\r\n", num_of_errors);
	return pdFALSE;
}


static const CLI_Command_Definition_t xMemTest =
{
	"mem_test", /* The command string to type. */
	"\rmem_test <addr> <size> <pattern> | test memory \r\n",
	prvMemTest, /* The function to run. */
	3 /* n parameters are expected. */
};



void vRegisterPadCommands( void )
{
	FreeRTOS_CLIRegisterCommand( &xMemTest );
}


#endif // SUPPORT_QSPI_TEST

