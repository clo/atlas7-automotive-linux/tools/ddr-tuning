/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "strutils.h"
#include "FreeRTOS.h"
#include "std_funcs.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int localerrnumber =0;

static inline int isSpace(char c)
{
    return (c == ' ' || c == '\t' || c == '\n' || c == '\12');
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

unsigned long _strto_l(const char *str, char **endptr, int base, int uflag)
{
    unsigned long number = 0;
    unsigned long cutoff;
    char *pos = (char *) str;
#if _STRTO_ENDPTR
    char *fail_char = (char *) str;
#endif
    int digit, cutoff_digit;
    int negative;

    while ( (isSpace(*pos ) ) )
	{	/* skip leading whitespace */
	++pos;
    }

    /* handle optional sign */
    negative = 0;
    switch(*pos) {
    case '-': negative = 1;	/* fall through to increment pos */
    case '+': ++pos;
    }

    if ((base == 16) && (*pos == '0')) { /* handle option prefix */
	++pos;
#if _STRTO_ENDPTR
	fail_char = pos;
#endif
	if ((*pos == 'x') || (*pos == 'X')) {
	    ++pos;
		base =16;
	}
    }
    
    if (base == 0) {		/* dynamic base */
	base = 10;		/* default is 10 */
	if (*pos == '0') {
	    ++pos;
	    base -= 2;		/* now base is 8 (or 16) */
#if _STRTO_ENDPTR
	    fail_char = pos;
#endif
	    if ((*pos == 'x') || (*pos == 'X')) {
		base += 8;	/* base is 16 */
		++pos;
	    }
	}
    }

    if ((base < 2) || (base > 36)) { /* illegal base */
	goto DONE;
    }

    cutoff = ULONG_MAX / base;
    cutoff_digit = ULONG_MAX - cutoff * base;

    while (1) {
	digit = 40;
	if ((*pos >= '0') && (*pos <= '9')) {
	    digit = (*pos - '0');
	} else if (*pos >= 'a') {
	    digit = (*pos - 'a' + 10);
	} else if (*pos >= 'A') {
	    digit = (*pos - 'A' + 10);
	} else break;

	if (digit >= base) {
	    break;
	}

	++pos;
#if _STRTO_ENDPTR
	fail_char = pos;
#endif

	/* adjust number, with overflow check */
	if ((number > cutoff)
	    || ((number == cutoff) && (digit > cutoff_digit))) {
	    number = ULONG_MAX;
	    if (uflag) {
		negative = 0; /* since unsigned returns ULONG_MAX */
	    }
#if _STRTO_ERRNO
	    localerrnumber = ERANGE;
#endif
	} else {
	    number = number * base + digit;
	}

    }

 DONE:
#if _STRTO_ENDPTR
    if (endptr) {
	*endptr = fail_char;
    }
#endif

    if (negative) {
	if (!uflag && (number > ((unsigned long)(-(1+LONG_MIN)))+1)) {
#if _STRTO_ERRNO
	    localerrnumber = ERANGE;
#endif
	    return (unsigned long) LONG_MIN;
	}
	return (unsigned long)(-((long)number));
    } else {
	if (!uflag && (number > (unsigned long) LONG_MAX)) {
#if _STRTO_ERRNO
	    localerrnumber = ERANGE;
#endif
	    return LONG_MAX;
	}
	return number;
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

unsigned long strtoul(const char *str, char **endptr, int base)
{
    return _strto_l(str, endptr, base, 1);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
long strtol(const char *str, char **endptr, int base)
{
    return _strto_l(str, endptr, base, 0);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

long strtonum(const char *numstr, int minval, int maxval,const char **errstrp)
{
        int lresult = 0;
        char *ep = (char *)numstr;
        int error = 0;
        struct errval {
                const char *errstr;
                int err;
        } sev[4] = {
                { NULL,         0 },
                { "invalid",    EINVAL },
                { "too small",  ERANGE },
                { "too large",  ERANGE },
        };
		while(*ep != '\0')
		{ 
			++ep;
		}

        sev[0].err = -1;
        localerrnumber = 0;
        if (minval > maxval)
          {
                printf("min %d bigger than max %d \r\n",minval,maxval);
                error = INVALID;
          }
        else {
                lresult = strtol(numstr, &ep, 0);
                if (numstr == ep || *ep != '\0')
                {
                  error = INVALID;
                }
                else if ((lresult == LONG_MIN && localerrnumber == ERANGE) || lresult < minval)
                        error = TOOSMALL;
                else if ((lresult == LONG_MAX && localerrnumber == ERANGE) || lresult > maxval)
                        error = TOOLARGE;
        }
        if (errstrp != NULL)
                *errstrp = sev[error].errstr;
        localerrnumber = sev[error].err;
        if (error)
                lresult = 0;

        return (lresult);
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

unsigned int strtounum(const char *numstr, unsigned int minval, unsigned int maxval,const char **errstrp)
{
        unsigned int lresult = 0;
        char *ep = (char *)numstr;
        int error = 0;
        struct errval {
                const char *errstr;
                int err;
        } sev[4] = {
                { NULL,         0 },
                { "invalid",    EINVAL },
                { "too small",  ERANGE },
                { "too large",  ERANGE },
        };
		while(*ep != '\0')
		{ 
			++ep;
		}

        sev[0].err = -1;
        localerrnumber = 0;
        if (minval > maxval)
          {
                printf("min %d bigger than max %d \r\n",minval,maxval);
                error = INVALID;
          }
        else {
                lresult = strtoul(numstr, &ep, 0);
                if (numstr == ep || *ep != '\0')
                {
                  error = INVALID;
                }
                else if ((lresult == 0 && localerrnumber == ERANGE) || lresult < minval)
                        error = TOOSMALL;
                else if ((lresult == ULONG_MAX && localerrnumber == ERANGE) || lresult > maxval)
                        error = TOOLARGE;
        }
        if (errstrp != NULL)
                *errstrp = sev[error].errstr;
        localerrnumber = sev[error].err;
        if (error)
                lresult = 0;

        return (lresult);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
