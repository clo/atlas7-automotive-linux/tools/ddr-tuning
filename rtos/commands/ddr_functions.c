/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "FreeRTOSConfig.h"

#include "ddrphy.h"
#include "upctl.h"
#include "cpurtc_io.h"
#include "ipc.h"
//#include "pwrc.h"
#include "clkc.h"
//#include "retain_regs.h"
#include "timer.h"
#include "FreeRTOS.h"
#include "chip_utils.h"
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
//#include "noc_ddrm.h"
#include "ringosc.h"
#include "ddr_bist.h"

#define SCTL_INIT       (0x0)
#define SCTL_CFG        (0x1)
#define SCTL_GO         (0x2)
#define SCTL_SLEEP      (0x3)
#define SCTL_WAKEUP     (0x4)
#define STAT_INIT_MEM   (0x0)
#define STAT_CONFIG     (0x1)
#define STAT_ACESS      (0x3)
#define STAT_LOW_POWER  (0x5)

//#define DDR_2X8

#ifdef DDR_2X8
   #define WRITE_MAN_TRAIN 0
#else
   #define WRITE_MAN_TRAIN 1
#endif

#define READ_MAN_TRAIN  0
#define SPARE           0 // take spares in all ddr timing (TBD, not implemented yet)

#define DDR_ZDATA      0x188D001C

#define DDR_TRAINING_START_ADDR 0x5fffe000 // keep it aligned to 0x40, in ddr address range
#define DDR_TRAINING_SIZE           0x2000 // keep it aligned to 0x40, min 0x40

// translate logical address to physical address (assuming default mapping in the NoC):
// 33 2222222222111111 1111 000 000000 0
// 10 9876543210987654 3210 987 654321 0
// -- RRRRRRRRRRRRRRRR CCCC BBB CCCCCC -
#define DTAR_LOGICAL_ADDR       (DDR_TRAINING_START_ADDR & 0x3fffffff)
#define DTAR_BANK               ((DTAR_LOGICAL_ADDR >>  7) & 0x0007)
#define DTAR_ROW                ((DTAR_LOGICAL_ADDR >> 14) & 0xffff)
#define DTAR_COL_LOW            ((DTAR_LOGICAL_ADDR >>  1) & 0x003f)
#define DTAR_COL_HIGH           ((DTAR_LOGICAL_ADDR >> 10) & 0x000f)
#define DTAR_COL                ((DTAR_COL_HIGH<<6) | DTAR_COL_LOW)
#define DTAR_PHYSICAL_ADDR      (((DTAR_BANK<<28) | (DTAR_ROW<<12) | (DTAR_COL<<0)) & 0xffffffe0)

////////////////////////////////////////////////////////////
/// DDR parameters
////////////////////////////////////////////////////////////

#define DDR_2T_EN            0  // 
#define AL                   0  // [cycles] Additive Latency
#define DDR_8BITS            0  // if using DRAM of 8 bits

// This parameter allows the user to increase the delay between issuing Write commands to the SDRAM when preceded by Read commands. 
// This provides an option to increase bus turn-around margin for high frequency systems.
#define tRTWn  (SPARE ? 1 : 0)  // 0 or 1 (0 = standard bus turn around delay; 1 = add 1 clock to standard bus turn around delay). 
#define tFAW             40000  // [ps]
#define tWR              15000  // [ps]
#define tRC              48750  // [ps]
#define tRP              13750  // [ps]
#define tRAS             35000  // [ps]
#define tRCD             13750  // [ps]
#define tRRD              7500  // [ps]
#define tRRDn                4  // [cycles]
#define tWTR              7500  // [ps]
#define tWTRn                4  // [cycles]
#define tRTP              7500  // [ps]
#define tRTPn                4  // [cycles]
#define tMRDn                4  // [cycles]
#define tDLLKn             512  // [cycles]
#define tXP               6000  // [ps]
#define tXPn                 3  // [cycles]
#define tXPDLL           24000  // [ps]
#define tXPDLLn             10  // [cycles]
#define tZQCS            80000  // [ps]
#define tZQCSn              64  // [cycles]
#define tZQINIT         640000  // [ps]
#define tZQINITn           512  // [cycles]
#define tZQOPER         320000  // [ps]
#define tZQOPERn           256  // [cycles]
#define tCKSRE           10000  // [ps]
#define tCKSREn              5  // [cycles]
#define tCKSRX           10000  // [ps]
#define tCKSRXn              5  // [cycles]
#define tCKE              5000  // [ps]
#define tCKEn                3  // [cycles]
#define tMOD             15000  // [ps]
#define tMODn               12  // [cycles]
#define tWLMRDn             40  // [cycles]
#define tWLO              7500  // [ns]
#define tCCDn                4  // [cycles]
#define tDLLKn             512  // [cycles]
#define tXS       (tRFC+10000)  // [ps]
#define tXSn                 5  // [cycles]
#define tXSDLLn         tDLLKn  // [cycles]
#define tZQCL   (max(tZQINIT ,tZQOPER))
#define tZQCLn  (max(tZQINITn,tZQOPERn))

// User adjustable uPCTL settings
static long tREFI =   7800; // [ns]
static long tRFC  = 350000; // [ps]

////////////////////////////////////////////////////////////
/// defines
////////////////////////////////////////////////////////////
#define max(a,b) ((a>b)?a:b)
#define min(a,b) ((a>b)?b:a)

#define SPINLOCK_LOCK(error_code)       POLL_REG(IPC_TESTANDSET_0, 1, 1, 1000, error_code);
#define SPINLOCK_RELEASE                WRITE_REG(IPC_TESTANDSET_0, 1);

// "Note that PGSR0[IDONE] is not cleared immediately after PIR[INIT] is set, and software must wait a minimum of 20 configuration clock cycles from when PIR[INIT] is set to when it starts polling for PGSR0[IDONE]"
#define WAIT_PGSR0_IDONE(error_code) {                                                                                                  \
                                wait_100ns(1);                                                                                          \
                                POLL_REG(MEMC_PHY_PGSR0,          1, 1, 100000, error_code);                                            \
                                POLL_REG(MEMC_PHY_PGSR0, 0x0ff00000, 0,      1, (error_code | ((READ_REG(MEMC_PHY_PGSR0)>>20)&0xff)));  \
                        }

static int DDR_DEBUG_PRINT=0xff; // 0xff for full info
static struct stDdrSetup ddrSetup;

void init_DDRPHY_LUTs(int acPD,int acPU,int clkPD,int clkPU);

////////////////////////////////////////////////////////////
/// cycles
////////////////////////////////////////////////////////////
unsigned int cycles (unsigned int time_in_ps, unsigned int freq_in_16mhz) {
   int o;
   unsigned int c;

   o = 0;
   // to avoid 32 bits overflow: (0xffffffff / (16*800))
   while (time_in_ps > 333000) {
      o++;
      time_in_ps = (unsigned int)((time_in_ps+1)>>1);
   }

   c = (unsigned int) (((unsigned long int)time_in_ps * (unsigned long int)freq_in_16mhz + 15999999) / 16000000);

   while (o) {
      c *= 2;
      o--;
   }
   return (c);
}

////////////////////////////////////////////////////////////
/// mylog2
////////////////////////////////////////////////////////////
int mylog2 (unsigned int num) {
   int v = -1;
   while (num) {
      v++;
      num >>= 1;
   }
   return (v);
}

////////////////////////////////////////////////////////////
/// ddr_freq_set
////////////////////////////////////////////////////////////
int ddr_freq_set (unsigned int target_ddr_freq, unsigned int xtal_freq) {
   // AB-PLL config calculation:
   // Fvco = xtal_freq x (divf+1) x 2 / (divr+1)
   // Fout = Fvco / (2^divq)
   // 1800 < Fvco < 3600
   // 5 < xtal_freq/(divr+1) < 30

   // target Fout is half the DDR freq
   unsigned int Fout = target_ddr_freq >> 1;
   // divq
   int divq = mylog2((unsigned int)(3600/Fout));
   if ((divq<1) || (divq>5)) {
      return (0xee100000);
   }
   // target Fvco
   unsigned int Fvco = Fout << divq;
   // divr
   unsigned int divr = 0; // lower divr gives lower long term jitter // (unsigned int)(xtal_freq * 2 * 512 / Fvco - 1);
   //unsigned int divr = (unsigned int)(xtal_freq * 2 * 512 / Fvco - 1); //(a more precize divr value)

   //$Rmax = div(xtal_freq,  5, -1) - 1;
   //$Rmin = div(xtal_freq, 30,  1) - 1;
   //$divr = $Rmax if ($divr>$Rmax);
   //$divr = $Rmin if ($divr<$Rmin);
   // divf
   int divf = (int)(Fvco * (divr+1) / (xtal_freq) / 2 - 1);
   if (divf>511) divf=511;
   if (divf<  0) divf=  0;

   // reset PLL (for running this function again)
   RD_MOD_WR(CLKC_MEMPLL_AB_CTRL0 , ~0x1, 0); // RSB=0 (assert reset)

   // config the freq
   WRITE_REG(CLKC_MEMPLL_AB_FREQ  , divf | (divr << 16));
   WRITE_REG(CLKC_MEMPLL_AB_CTRL1 , (1<<12) | (divq<<0) | 0x110);

   // bypass, reset
   RD_MOD_WR(CLKC_MEMPLL_AB_CTRL0 , ~0x00000010, 0); // bypass=0
   RD_MOD_WR(CLKC_MEMPLL_AB_CTRL0 , ~0x00000000, 1); // RSB=1 (release reset)

   // final values
   Fvco = xtal_freq * (divf+1) * 2 / (divr+1);
   // Fout = Fvco >> divq; // final Fout: this is the controller clk (half of ddr_freq)
   // ddr_freq = Fout << 1; // DDR freq is double the PLL output
   unsigned int final_freq_x16 = Fvco << (5-divq);
   //$Frefdiv = xtal_freq / ($divr+1);
   //$RefClkRange = ($Frefdiv<13) ? 0 : 1;
    
   // wait for PLL lock
   POLL_REG(CLKC_MEMPLL_AB_STATUS, 1, 1, 100000, 0xeeee0000);

   // return ddr_freq*16
   return (final_freq_x16);
}


#if SUPPORT_DDR   
void setDdrDebugPrint(unsigned long ddrDebugPrint)
{
   DDR_DEBUG_PRINT=ddrDebugPrint;
} // setDdrDebugPrint

////////////////////////////////////////////////////////////
/// ddr_range_check
////////////////////////////////////////////////////////////
int ddr_range_check (unsigned int addr, unsigned int size, unsigned int loop, unsigned int op, unsigned int wait_mask) {
   unsigned int a, d, b, r;
   unsigned int ee=0; // total errors in all loops
   unsigned int seed = 0x89abcdef;
   volatile unsigned int w;

   while (loop) 
   {
      printf("\r\n############ loop #%d\r\n",loop);

      // seed
      seed++;
      if (!seed) seed++;

      // write
      if (op&1) 
      {
         printf("\r\n### writing...\r\n");
         d = seed;
         for (a=addr; a<(addr+size); a+=4) 
         {
            *(volatile unsigned int *)(a) = d;
            b = ((d >> 31) ^ (d >> 28)) & 1;
            d = ((d << 1) | b) & 0xffffffff;
            
            if (wait_mask) {
               w = (d & wait_mask); // +10;
               while (w--){;}
            }
         }
      }

      // read
      if (op&2) 
      {
         printf("\r\n### reading...\r\n");
         d = seed;
         for (a=addr; a<(addr+size); a+=4) 
         {
            r = (*(volatile unsigned int *)(a));
            if (r != d) 
            {
               ee++;
               //printf("addr = %x exp = %x read = %x (%x)\r\n", a, d, r, d^r);
               //for (j=0; j<5; j++){
               //    r = (*(volatile unsigned int *)(a));
               //    printf("                exp = %x read = %x (%x)\r\n", d, r, d^r);
               //}
            }
            b = ((d >> 31) ^ (d >> 28)) & 1;
            d = ((d << 1) | b) & 0xffffffff;
            if (wait_mask) {
                w = d & wait_mask;
                while (w--){;}
            }
         }
      }

      loop--;
   }

   //printf("max mdlr_dx0 value found 0x%x\r\n", max_r_dx0);
   //printf("min mdlr_dx0 value found 0x%x\r\n", min_r_dx0);
   //printf("max mdlr_dx1 value found 0x%x\r\n", max_r_dx1);
   //printf("min mdlr_dx1 value found 0x%x\r\n", min_r_dx1);
    
   if (ee) {printf("\r\nnum of errors %d\r\n",ee);    }

   return(ee);
} // ddr_range_check

////////////////////////////////////////////////////////////
/// ddr_bist_phy_dram
////////////////////////////////////////////////////////////
#define PHY_BIST_SIZE 0x1000 // units of bytes, multilple of 0x20, max 0x7ffe0

int ddr_bist_dram (unsigned int row_bits,     // number of row bits in the dram (e.g. 16 bits for 1GB device)
                   unsigned int initial_addr, // [B] byte address = {ba[2:0],row[row_bits-1:0],col[9:0],1'b0}; must be multiple of 0x10
                   unsigned int initial_size, // [B] units of bytes, must be multiple of 0x20
                   unsigned int loops) {
   unsigned int ee=0; // total errors

   // disable VT by setting PGCR1.INHVT=1 // address 0x1081000C bit[26]
   RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 1<<26);

   while (loops)
   {
      unsigned int bist_status = 0;
      unsigned int status;
      unsigned int seed;
      unsigned int curr_size; // [B]
      unsigned int ba, row, col, e=0;
      unsigned int row_mask;
      unsigned int trefi;
      int byte, loop=0, num_of_bytes;
      unsigned int size = initial_size;
      unsigned int addr = initial_addr;

      #if DDR_8BITS
         num_of_bytes=1;
      #else
         num_of_bytes=2;
      #endif

      // printf("\r\nWorking... # of loops left %d\r\n",loops);      

      // move to config state, and disable refreshes from controller (phy is doing refreshes while in bist, and phy may violate tRFC if it gets external refresh commands (from dfi) while it is doing bist)
      WRITE_REG(MEMC_SCTL , SCTL_CFG);
      POLL_REG(MEMC_STAT, 0xf, STAT_CONFIG, 100, 0xee650000);
      trefi = READ_REG(MEMC_TREFI); // keep to restore later
      WRITE_REG(MEMC_TREFI, (1<<31)); // stop refreshes

      // address and size
      row_mask = 0xffffffff >> (32-row_bits);
      size &= 0xffffffe0; // size must be multiple of 0x20 bytes
      addr &= 0xfffffff0; // size must be multiple of 0x10 bytes

      // global bist configurations
      WRITE_REG(MEMC_PHY_BISTAR2 , (0x7<<28)|(row_mask<<12)|(0x3f8<<0)); // max bank/row/col
      WRITE_REG(MEMC_PHY_BISTAR1 , (0x008<<4)); // col-increment = 8

      while (size) {
         curr_size = min(size, PHY_BIST_SIZE); // current size

         col = (addr >> (1         )) & 0x3ff;
         row = (addr >> (11         )) & row_mask;
         ba  = (addr >> (11+row_bits)) & 0x7;

         WRITE_REG(MEMC_PHY_BISTWCR , curr_size>>3); // in units of 4-beats, i.e. units of 8B, must be multiple of 4 (BL/2)
         WRITE_REG(MEMC_PHY_BISTAR0 , (ba<<28)|(row<<12)|(col<<0)); // start-address

         // run bist for each byte
         for (byte=0; byte<num_of_bytes; byte++)
         {
            // update seed: start with different lfsr seed each time (any seed but zero)
            seed = READ_REG(MEMC_PHY_BISTLSR)+1;
            if (!seed) seed++;
            WRITE_REG(MEMC_PHY_BISTLSR , seed);

            // reset all BIST counters and indications
            RD_MOD_WR(MEMC_PHY_BISTRR, ~7, 3);

            // user defined pattern
            WRITE_REG(MEMC_PHY_BISTUDPR, (0xa5a5<<16)|(0xa5a5<<0)); //00ff00ff
            //WRITE_REG(MEMC_PHY_BISTUDPR, (0xaa00<<16)|(0x00aa<<0)); //aa55aa55
            //WRITE_REG(MEMC_PHY_BISTUDPR, (0xa5a5<<16)|(0x5a5a<<0)); //aa55aa55
            //WRITE_REG(MEMC_PHY_BISTUDPR, (0x0<<16)|(0x0<<0)); //0x0
            //WRITE_REG(MEMC_PHY_BISTUDPR, (0xffff<<16)|(0xffff<<0)); //0xffffffff

            // bist go
            WRITE_REG(MEMC_PHY_BISTRR,  (byte<< 19) |  // byte select
                                        (2 << 17) |  // pattern (0-walking-0, 1-walking-1, 2-pseudo-random, 3-user-programmable)
                                        (0 << 15) |  // BIST AC enable (0 in dram mode)
                                        (1 << 14) |  // BIST DX enable
                                        (0 << 13) |  // stop on Nth fail
                                        (0 <<  5) |  // number of failures to stop on (0..255 for 1..256)
                                        (0 <<  4) |  // infinite run
                                        (1 <<  3) |  // mode (0-loopback, 1-dram)
                                        (1 <<  0)); // cmd (0-nop, 1-run, 2-stop, 3-reset)

            // wait for bist done
            POLL_REG(MEMC_PHY_BISTGSR, 1, 1, 100000, (loop<<16) | 0xee00 | (1<<byte));

            // read bist status
            status = ((READ_REG(MEMC_PHY_BISTGSR) >> 2) & 0x1);
            bist_status |= status << byte;
            if (status) {
               //unsigned int bsi;
               //unsigned int cnt = (READ_REG(MEMC_PHY_BISTWER) >> 16) & 0xffff;
               //unsigned int ber2 = READ_REG(MEMC_PHY_BISTBER2);
               //unsigned int ber3 = READ_REG(MEMC_PHY_BISTBER3);
               //unsigned int berr0=0; for(bsi=0;bsi<8;bsi++){ berr0 |= ((((ber2>>(2*bsi))&3) ? 1 : 0)<<bsi); }
               //unsigned int berr1=0; for(bsi=0;bsi<8;bsi++){ berr1 |= ((((ber2>>(16+2*bsi))&3) ? 1 : 0)<<bsi); }
               //unsigned int berr2=0; for(bsi=0;bsi<8;bsi++){ berr2 |= ((((ber3>>(2*bsi))&3) ? 1 : 0)<<bsi); }
               //unsigned int berr3=0; for(bsi=0;bsi<8;bsi++){ berr3 |= ((((ber3>>(16+2*bsi))&3) ? 1 : 0)<<bsi); }
               //unsigned int berr = berr0 | berr1 | berr2 | berr3;
               //printf("bist failed loop=%d byte=%d cnt=%x err=%x (%x %x %x %x)\r\n", loop, byte, cnt, berr, berr0, berr1, berr2, berr3);
               e++;
            }
         } // each byte

         // update parameters for next loop
         loop++;
         size -= curr_size;
         addr += curr_size;
      } // size loop

      // restore trefi, go back to access state
      WRITE_REG(MEMC_TREFI, trefi | (1<<31));
      WRITE_REG(MEMC_SCTL , SCTL_GO);
      POLL_REG(MEMC_STAT, 0xf, STAT_ACESS , 100, 0xee660000);

      // epilogue
      if (e) { printf("\r\nloop: %3d num of errors %6d\r\n",loops,e); }
      ee += e;
      loops--;
   } // loop counter

   // enable VT by setting PGCR1.INHVT=0 // address 0x1081000C bit[26]
   RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 0<<26);

   if (ee) { printf("\r\ntotal num of errors %6d\r\n",ee); }

   return(ee);
}

////////////////////////////////////////////////////////////
/// ddr_bist_loopback
////////////////////////////////////////////////////////////
int ddr_bist_loopback (void) {
   unsigned int bist_status = 0;

   // [27]    - IOLB  : Loopback is before output buffer (core-side). After output buffer called pad-side.
   // [28]    - LBDQSS: PUB sets the read DQS LCDL to 0; DQS is already shifted 90 degrees by write path.
   // [30:29] - LBGDQS: DQS gate is always on.
   RD_MOD_WR(MEMC_PHY_PGCR1, 0x87FFFFFF, 0x08000000);

   // [15:0] - BWCNT: BIST Word Count: Indicates the number of words to generate during BIST
   WRITE_REG(MEMC_PHY_BISTWCR , 0x64);

   // While pad-side loopback is selected:
   // [1]  - ACOE: Address/Command Output Enable: Enables, when set, the output driver on the I/O for all address and command pins.
   // [29] - RSTIOM: SDRAM Reset I/O Mode: Selects SSTL mode (when set to 0) or CMOS mode (when set to 1) of the I/O for SDRAM Reset.
   WRITE_REG(MEMC_PHY_ACIOCR , 0x20000002);

   // [2:0] - BINST: BIST Instruction: Run
   // [3]   - BMODE: BIST Mode. 0 = Loopback mode, 1 = DRAM mode
   // [14]  - BDXEN: BIST DATX8 Enable
   // [15]  - BACEN: BIST AC Enable
   // [16]  - BDMEN: BIST DM Enable
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFFFE3FF0, 0x00008001);

   // [0] - BDONE: BIST Done: Indicates if set that the BIST has finished executing. This bit is reset to zero when BIST is triggered.
   POLL_REG(MEMC_PHY_BISTGSR, 1, 1, 100000, 0xee100000);

   // [2:0] - BINST: BIST Instruction: Stop
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFFFFFFF8, 0x00000002);

   bist_status |= ((READ_REG(MEMC_PHY_BISTGSR) >> 1) & 0x1) << 4;

   // [15:0] - BWCNT: BIST Word Count: Indicates the number of words to generate during BIST
   WRITE_REG(MEMC_PHY_BISTWCR , 0x100);

   // [2:0] - BINST: BIST Instruction: Reset --> Run
   // [14]  - BDXEN: BIST DATX8 Enable.
   // [15]  - BACEN: BIST AC Enable.
   // [16]  - BDMEN: BIST DM Enable
   // [18:17] - BDPAT : BIST data pattern: Walking 0
   // [22:19] - BDXSEL: BIST DATX8 Select: Lane 0
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFFFFFFF8, 0x00000003);
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFFFE3FF8, 0x00014001);

   POLL_REG(MEMC_PHY_BISTGSR, 1, 1, 100000, 0xee110000);

   bist_status |= ((READ_REG(MEMC_PHY_BISTGSR) >> 2) & 0x1) << 0;

   // [2:0]   - BINST : BIST Instruction: Reset --> Run
   // [14]    - BDXEN : BIST DATX8 Enable.
   // [15]    - BACEN : BIST AC Enable.
   // [16]    - BDMEN : BIST DM Enable
   // [18:17] - BDPAT : BIST data pattern: Walking 1
   // [22:19] - BDXSEL: BIST DATX8 Select: Lane 1
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFFFFFFF8, 0x00000003);
   RD_MOD_WR(MEMC_PHY_BISTRR, 0xFF863FF8, 0x000B4001);

   POLL_REG(MEMC_PHY_BISTGSR, 1, 1, 100000, 0xee120000);

   bist_status |= ((READ_REG(MEMC_PHY_BISTGSR) >> 2) & 0x1) << 1;

   return(bist_status);
}

///////////////////////////////////////////////////////////
// Get the TPRD value in ps for one of the bytes
///////////////////////////////////////////////////////////
int getTPRD(int byte)
{
   int TPRD, MDLR, tap;
    
   // MDLR is Master Delay Line Registers    
   if (byte == 0) { MDLR = (int)(READ_REG(MEMC_PHY_DX0MDLR)); }    
   else           { MDLR = (int)(READ_REG(MEMC_PHY_DX1MDLR)); }
    
   TPRD = (int)((MDLR >> 8) & 0x000000ff); // TPRD is the Target period    
   tap  = (int)(1000000/(2*TPRD*(ddrSetup.freq_x16/16))); // calculation of tap size in psec

   return(tap);
}

///////////////////////////////////////////////////////////
// win
///////////////////////////////////////////////////////////
#define PASS 0
#define FAIL 1
unsigned int win(unsigned int addr, unsigned int mask, int range, int startbit, int byte)
{
   int j, error, center, new_center;
   int status = FAIL;
   int left = 0;
   int right = 0;
   int temp_left = 0;
   int temp_right = 0;
   int TPRD, tDS;

   // This assumes we start at a failing positiong, find a passing region, and another failing region
   // *********...................*************
   for (j=0x0; j<range; j++)
   {
      RD_MOD_WR(addr, mask, j<<startbit);
      error = ddr_memvex(DDR_TRAINING_START_ADDR,DDR_TRAINING_SIZE,0x40020100,0x0000ffff);

      if (error!=0) // fail
      { 
         if (status == FAIL) { }
         else 
         { // if first fail
            temp_right = j-1;
            if ((temp_right-temp_left) > (right-left))
            {
               left = temp_left;
               right = temp_right;
            }
         } // first fail
         status = FAIL;
      } // failed
      else
      {  
         if (status == FAIL) { temp_left = j; } // if first pass
         status = PASS;
      } // passed               
   } // each register step

   if ((right==0x0) & (left==0x0)) 
   { center=range>>1; } // for the case of no failures, the center in middle of the checked range
   else
   { center = (right+left)>>1; } // center is initially in the middle of the window

   // new center is changed to be center+6, in order to help the data hold time     
   new_center = center + 6;
     
   TPRD = getTPRD(byte);
   tDS  = TPRD * (right - new_center); // calculation of the margin for tDS in psec
      
   // while to ensure tDS greater than 100[psec]   
   while ((tDS < 120) & (new_center!=center)) 
   {
      new_center = new_center -1;
      tDS = TPRD * (right - new_center);
   }

   //if (DDR_DEBUG_PRINT&0x10)
   //{
   //    printf("\r\nwindow on addr 0x%x :: 0x%x [0x%x .. 0x%x] TPRD: %d tDS: %d\r\n",addr,new_center,left,right,TPRD,tDS);
   //}
   return (new_center);
}

///////////////////////////////////////////////////////////
// winCheck
///////////////////////////////////////////////////////////
void winCheck(char *name,unsigned int addr,unsigned int mask,int startbit,int byte)
{
   int j, error;
   int status = FAIL;
   int left = 0;
   int right = 0;
   int temp_left = 0;
   int temp_right = 0;
   int TPRD;
   int range;
   unsigned long ddrDebugPrint;
   unsigned long orgRegValue;
   unsigned long value;

   // save the registers original value
   orgRegValue=READ_REG(addr);
   TPRD = getTPRD(byte);
   value = (orgRegValue & (~mask)) >> startbit;

   ddrDebugPrint=DDR_DEBUG_PRINT;
   if (name[0]=='A') { ddrDebugPrint=DDR_DEBUG_PRINT; DDR_DEBUG_PRINT=0x00; }

   printf("   %s: ",name);

   // Math needs to be simple integers - 11363 = 90909/8 which is 11ps in Mhz
   // This works since freq is desiredfreq * 16
   range=(8*(11363/(ddrSetup.freq_x16>>3)))-2;
   //printf("range: %d\r\n",range);

   for (j=0x0; j<range; j++)
   {
      RD_MOD_WR(addr, mask, j<<startbit);
      error = ddr_memvex(0x40000000,0x2000,0x40020100,0x0000ffff);

      if (error!=0) // fail
      {
         if (j == value) 
         { printf("x"); } 
         else 
         { printf("*"); }

         if (status == FAIL) { }
         else 
         { // if first fail
            temp_right = j-1;
            if ((temp_right-temp_left) > (right-left))
            {
               left = temp_left;
               right = temp_right;
               if (name[0]=='A') { break; }
            }
         } // first fail
         status = FAIL;
      } // failed
      else
      {  
         if (j == value) 
         { printf("+"); } 
         else 
         { printf("."); }

         if (status == FAIL) { temp_left = j; } // if first pass
         status = PASS;
      } // passed               
      
      if (name[0]=='A') 
      { 
         WRITE_REG(addr,orgRegValue);
         runDdrInit();
         RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 1<<26);  
         RD_MOD_WR(MEMC_PHY_DX0GCR, 0xbfffffff, 0); 
         RD_MOD_WR(MEMC_PHY_DX1GCR, 0xbfffffff, 0);
      } // Addr/Cmd windows
   } // each register step
      
   if (name[0]=='A') { j++; while (j<range) { printf("*"); j++; } }

   printf(" Win: %3d - %3d %3d Setting: %3d(ps) Margin: %3d / %3d\r\n",left*TPRD,right*TPRD,(right-left)*TPRD,value*TPRD,(value-left)*TPRD,(right-value)*TPRD);

   // Restore the register to its original value
   WRITE_REG(addr,orgRegValue);
   DDR_DEBUG_PRINT=ddrDebugPrint;
} // winCheck

///////////////////////////////////////////////////////////
// ddr_test
///////////////////////////////////////////////////////////
int ddr_test (unsigned int i0, unsigned int i1, unsigned int i2, unsigned int i3, unsigned int i4) 
{
//   if (i0==0x1)
//   {
//      unsigned int zdata;
//      printf ("\r\nrunning test #1...\r\n");
//      zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, 0xf0000000, zdata);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//      printf ("zcal...\r\n");
//      WRITE_REG(MEMC_PHY_PIR , 0x3);
//      WAIT_PGSR0_IDONE(0xeed00000);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//   }
//
//   if (i0==0x2)
//   {
//      unsigned int zdata;
//      printf ("\r\nrunning test #2...\r\n");
//      zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, 0xf0000000, zdata);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//      printf ("set/unset ZDEN\r\n");
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, ~0xf0000000, 0x50000000);
//      wait_us(10);
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, ~0xf0000000, 0x40000000);
//      wait_us(10);
//      printf ("zcal...\r\n");
//      WRITE_REG(MEMC_PHY_PIR , 0x3);
//      WAIT_PGSR0_IDONE(0xeed10000);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//   }
//
//   if (i0==0x3)
//   {
//      unsigned int zdata;
//      printf ("\r\nrunning test #3...\r\n");
//      zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, 0xf0000000, zdata);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//      printf ("set/unset ZDEN/ZQPD/ZCALEN\r\n");
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, ~0xf0000000, 0xb0000000);
//      wait_us(10);
//      RD_MOD_WR(MEMC_PHY_ZQ0CR0, ~0xf0000000, 0x40000000);
//      wait_us(10);
//      printf ("zcal...\r\n");
//      WRITE_REG(MEMC_PHY_PIR , 0x3);
//      WAIT_PGSR0_IDONE(0xeed20000);
//      printf ("status:\r\n");    dump_ddrphy_regs();
//   }
//
//   if (i0==0x4) 
//   {
//      unsigned int i;
//      printf ("\r\nrunning test #4...\r\n");
//
//      //zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
//      printf ("\r\nstatus:");    dump_ddrphy_regs();
//
//      for (i=0; i<0x20; i++)
//      {
//         WRITE_REG(MEMC_PHY_ZQ0CR0, 0x50000000 | (i<<15) | (i<<10));
//         WRITE_REG(MEMC_PHY_ZQ0CR0, 0x40000000 | (i<<15) | (i<<10));
//         WRITE_REG(MEMC_PHY_PIR , 0x3);
//         wait_100ns(1);
//         POLL_REG(MEMC_PHY_PGSR0, 1, 1, 100000, (0xee77|i));
//         printf ("i=%x ZQ0SR1=%x PGSR0=%x\r\n", i, READ_REG(MEMC_PHY_ZQ0SR1), READ_REG(MEMC_PHY_PGSR0));
//      }
//
//      for (i=0; i<0x20; i++)
//      {
//         WRITE_REG(MEMC_PHY_ZQ0CR0, 0x50000000 | (i<< 5) | (i<< 0));
//         WRITE_REG(MEMC_PHY_ZQ0CR0, 0x40000000 | (i<< 5) | (i<< 0));
//         WRITE_REG(MEMC_PHY_PIR , 0x3);
//         wait_100ns(1);
//         POLL_REG(MEMC_PHY_PGSR0, 1, 1, 100000, (0xee77|i));
//         printf ("i=%x ZQ0SR1=%x PGSR0=%x\r\n", i, READ_REG(MEMC_PHY_ZQ0SR1), READ_REG(MEMC_PHY_PGSR0));
//      }
//   }
//
//   if ((i0&0xfffffff0)==0x10)
//   {
//      unsigned int zdata;
//      printf ("\r\nrunning test #f...\r\n");
//
//      // just update override data
//      zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
//      WRITE_REG(MEMC_PHY_ZQ0CR0, 0x40000000 | zdata);
//
//      printf ("dump #1:\r\n");    dump_ddrphy_regs();
//
//      // zcal
//      WRITE_REG(MEMC_PHY_PIR , 0x3);
//      wait_100ns(1);
//      while ((READ_REG(MEMC_PHY_PGSR0) & 1) == 0) {; }
//
//      printf ("dump #2:\r\n");    dump_ddrphy_regs();
//
//      // override on then off
//      WRITE_REG(MEMC_PHY_ZQ0CR0, 0xb0000000 | zdata); wait_us(10);
//      WRITE_REG(MEMC_PHY_ZQ0CR0, 0x40000000 | zdata); wait_us(10);
//
//      printf ("dump #3:\r\n");    dump_ddrphy_regs();
//
//      // reset
//      if ((i0&0xf)==1) 
//      {
//         // reset ddrphy and upctl (bits[0,1,3]) 'b1011
//         // disable clocks: mem, memdiv2, nocr(ddrm)
//         WRITE_REG(CLKC_RSTC_MEM_SW_RST_CLR , 0xb); // assert reset (active low)
//         wait_us(1);
//         WRITE_REG(CLKC_MEM_LEAF_CLK_EN_CLR , 0xF); // disable clock
//         wait_us(1);
//         WRITE_REG(CLKC_RSTC_MEM_SW_RST_SET , 0xb); // deassert reset
//         wait_us(1);
//         WRITE_REG(CLKC_MEM_LEAF_CLK_EN_SET, 0xf);   // enable clocks
//      }
//      if ((i0&0xf)==2) {
//         WRITE_REG(MEMC_PHY_PIR , 0x41);
//         wait_us(10);
//      }
//
//      // zcal
//      WRITE_REG(MEMC_PHY_PIR , 0x3);
//      wait_100ns(1);
//      while((READ_REG(MEMC_PHY_PGSR0)&1) == 0) {; }
//
//      printf ("dump #4:\r\n");    dump_ddrphy_regs();
//   }
//
//   printf ("\r\n");
   return(0);
}

////////////////////////////////////////////////////////////
/// dump_ddrphy_regs
////////////////////////////////////////////////////////////
void dump_ddrphy_regs ()
{
   printf("\r\n");

   // global
   printf("PIR       = 0x%08x\r\n", READ_REG(MEMC_PHY_PIR));
   printf("PGCR0     = 0x%08x\r\n", READ_REG(MEMC_PHY_PGCR0));
   printf("PGCR1     = 0x%08x\r\n", READ_REG(MEMC_PHY_PGCR1));
   printf("PGCR2     = 0x%08x\r\n", READ_REG(MEMC_PHY_PGCR2));
   printf("PGSR0     = 0x%08x\r\n", READ_REG(MEMC_PHY_PGSR0));
   printf("PGSR1     = 0x%08x\r\n", READ_REG(MEMC_PHY_PGSR1));

   // ZQ control/status
   printf("ZQ0CR0    = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0CR0));
   printf("ZQ0CR1    = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0CR1));
   printf("ZQ0SR0    = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0SR0));
   printf("ZQ0SR0_AC = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0SR0_AC));
   printf("ZQ0SR0_CK = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0SR0_CLK));
   printf("ZQ0SR1    = 0x%08x\r\n", READ_REG(MEMC_PHY_ZQ0SR1));

   // training
   printf("Byte 0 Debug Regs\r\n"); 
   printf("DTEDR0    = 0x%08x\r\n", READ_REG(MEMC_PHY_DTEDR0));
   printf("DTEDR1    = 0x%08x\r\n", READ_REG(MEMC_PHY_DTEDR1));
   RD_MOD_WR(MEMC_PHY_DTCR, ~0x10000, 0x1<<16); 
   printf("Byte 1 Debug Regs\r\n"); 
   printf("DTEDR0    = 0x%08x\r\n", READ_REG(MEMC_PHY_DTEDR0));
   printf("DTEDR1    = 0x%08x\r\n", READ_REG(MEMC_PHY_DTEDR1));
   RD_MOD_WR(MEMC_PHY_DTCR, ~0x10000, 0x0<<16); 

   // DX0
   printf("DX0GCR    = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0GCR));
   printf("DX0GSR0   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0GSR0));
   printf("DX0GSR1   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0GSR1));
   printf("DX0GSR2   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0GSR2));
   printf("DX0BDLR0  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0BDLR0));
   printf("DX0BDLR1  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0BDLR1));
   printf("DX0BDLR2  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0BDLR2));
   printf("DX0BDLR3  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0BDLR3));
   printf("DX0BDLR4  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0BDLR4));
   printf("DX0LCDLR0 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0LCDLR0));
   printf("DX0LCDLR1 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0LCDLR1));
   printf("DX0LCDLR2 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0LCDLR2));
   printf("DX0MDLR   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0MDLR));
   printf("DX0GTR    = 0x%08x\r\n", READ_REG(MEMC_PHY_DX0GTR));

   // DX1
   printf("DX1GCR    = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1GCR));
   printf("DX1GSR0   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1GSR0));
   printf("DX1GSR1   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1GSR1));
   printf("DX1GSR2   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1GSR2));
   printf("DX1BDLR0  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1BDLR0));
   printf("DX1BDLR1  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1BDLR1));
   printf("DX1BDLR2  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1BDLR2));
   printf("DX1BDLR3  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1BDLR3));
   printf("DX1BDLR4  = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1BDLR4));
   printf("DX1LCDLR0 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1LCDLR0));
   printf("DX1LCDLR1 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1LCDLR1));
   printf("DX1LCDLR2 = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1LCDLR2));
   printf("DX1MDLR   = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1MDLR));
   printf("DX1GTR    = 0x%08x\r\n", READ_REG(MEMC_PHY_DX1GTR));

/*   
    // timing registers
    if(p&0x00001000) printf("PTR0             = 0x%x\r\n", READ_REG(MEMC_PHY_PTR0));
    if(p&0x00001000) printf("PTR1             = 0x%x\r\n", READ_REG(MEMC_PHY_PTR1));
    if(p&0x00001000) printf("PTR2             = 0x%x\r\n", READ_REG(MEMC_PHY_PTR2));
    if(p&0x00001000) printf("PTR3             = 0x%x\r\n", READ_REG(MEMC_PHY_PTR3));
    if(p&0x00001000) printf("PTR4             = 0x%x\r\n", READ_REG(MEMC_PHY_PTR4));
    if(p&0x00001000) printf("DTPR0            = 0x%x\r\n", READ_REG(MEMC_PHY_DTPR0));
    if(p&0x00001000) printf("DTPR1            = 0x%x\r\n", READ_REG(MEMC_PHY_DTPR1));
    if(p&0x00001000) printf("DTPR2            = 0x%x\r\n", READ_REG(MEMC_PHY_DTPR2));

    // BIST
    if(p&0x00010000) printf("BISTRR           = 0x%x\r\n", READ_REG(MEMC_PHY_BISTRR));
    if(p&0x00010000) printf("BISTWCR          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTWCR));
    if(p&0x00010000) printf("BISTMSKR0        = 0x%x\r\n", READ_REG(MEMC_PHY_BISTMSKR0));
    if(p&0x00010000) printf("BISTMSKR1        = 0x%x\r\n", READ_REG(MEMC_PHY_BISTMSKR1));
    if(p&0x00010000) printf("BISTMSKR2        = 0x%x\r\n", READ_REG(MEMC_PHY_BISTMSKR2));
    if(p&0x00010000) printf("BISTLSR          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTLSR));
    if(p&0x00010000) printf("BISTAR0          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTAR0));
    if(p&0x00010000) printf("BISTAR1          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTAR1));
    if(p&0x00010000) printf("BISTAR2          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTAR2));
    if(p&0x00010000) printf("BISTUDPR         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTUDPR));
    if(p&0x00010000) printf("BISTGSR          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTGSR));
    if(p&0x00010000) printf("BISTWER          = 0x%x\r\n", READ_REG(MEMC_PHY_BISTWER));
    if(p&0x00010000) printf("BISTBER0         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTBER0));
    if(p&0x00010000) printf("BISTBER1         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTBER1));
    if(p&0x00010000) printf("BISTBER2         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTBER2));
    if(p&0x00010000) printf("BISTBER3         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTBER3));
    if(p&0x00010000) printf("BISTWCSR         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTWCSR));
    if(p&0x00010000) printf("BISTFWR0         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTFWR0));
    if(p&0x00010000) printf("BISTFWR1         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTFWR1));
    if(p&0x00010000) printf("BISTFWR2         = 0x%x\r\n", READ_REG(MEMC_PHY_BISTFWR2));

    // DCU
    if(p&0x00100000) printf("DCUAR            = 0x%x\r\n", READ_REG(MEMC_PHY_DCUAR));
    if(p&0x00100000) printf("DCUDR            = 0x%x\r\n", READ_REG(MEMC_PHY_DCUDR));
    if(p&0x00100000) printf("DCURR            = 0x%x\r\n", READ_REG(MEMC_PHY_DCURR));
    if(p&0x00100000) printf("DCULR            = 0x%x\r\n", READ_REG(MEMC_PHY_DCULR));
    if(p&0x00100000) printf("DCUGCR           = 0x%x\r\n", READ_REG(MEMC_PHY_DCUGCR));
    if(p&0x00100000) printf("DCUTPR           = 0x%x\r\n", READ_REG(MEMC_PHY_DCUTPR));
    if(p&0x00100000) printf("DCUSR0           = 0x%x\r\n", READ_REG(MEMC_PHY_DCUSR0));
    if(p&0x00100000) printf("DCUSR1           = 0x%x\r\n", READ_REG(MEMC_PHY_DCUSR1));

    // other
    if(p&0x80000000) printf("RIDR             = 0x%x\r\n", READ_REG(MEMC_PHY_RIDR));
    if(p&0x80000000) printf("PLLCR            = 0x%x\r\n", READ_REG(MEMC_PHY_PLLCR));

    if(p&0x80000000) printf("ACMDLR           = 0x%x\r\n", READ_REG(MEMC_PHY_ACMDLR));
    if(p&0x80000000) printf("ACBDLR           = 0x%x\r\n", READ_REG(MEMC_PHY_ACBDLR));
    if(p&0x80000000) printf("ACIOCR           = 0x%x\r\n", READ_REG(MEMC_PHY_ACIOCR));
    if(p&0x80000000) printf("DXCCR            = 0x%x\r\n", READ_REG(MEMC_PHY_DXCCR));
    if(p&0x80000000) printf("DSGCR            = 0x%x\r\n", READ_REG(MEMC_PHY_DSGCR));
    if(p&0x80000000) printf("DCR              = 0x%x\r\n", READ_REG(MEMC_PHY_DCR));

    if(p&0x80000000) printf("MR0              = 0x%x\r\n", READ_REG(MEMC_PHY_MR0));
    if(p&0x80000000) printf("MR1              = 0x%x\r\n", READ_REG(MEMC_PHY_MR1));
    if(p&0x80000000) printf("MR2              = 0x%x\r\n", READ_REG(MEMC_PHY_MR2));
    if(p&0x80000000) printf("MR3              = 0x%x\r\n", READ_REG(MEMC_PHY_MR3));

    if(p&0x80000000) printf("ODTCR            = 0x%x\r\n", READ_REG(MEMC_PHY_ODTCR));
    if(p&0x80000000) printf("DTCR             = 0x%x\r\n", READ_REG(MEMC_PHY_DTCR));
    if(p&0x80000000) printf("DTAR0            = 0x%x\r\n", READ_REG(MEMC_PHY_DTAR0));
    if(p&0x80000000) printf("DTAR1            = 0x%x\r\n", READ_REG(MEMC_PHY_DTAR1));
    if(p&0x80000000) printf("DTAR2            = 0x%x\r\n", READ_REG(MEMC_PHY_DTAR2));
    if(p&0x80000000) printf("DTAR3            = 0x%x\r\n", READ_REG(MEMC_PHY_DTAR3));
    if(p&0x80000000) printf("DTDR0            = 0x%x\r\n", READ_REG(MEMC_PHY_DTDR0));
    if(p&0x80000000) printf("DTDR1            = 0x%x\r\n", READ_REG(MEMC_PHY_DTDR1));

    if(p&0x80000000) printf("AACR             = 0x%x\r\n", READ_REG(MEMC_PHY_AACR));

    if(p&0x80000000) printf("RDIMMGCR0        = 0x%x\r\n", READ_REG(MEMC_PHY_RDIMMGCR0));
    if(p&0x80000000) printf("RDIMMGCR1        = 0x%x\r\n", READ_REG(MEMC_PHY_RDIMMGCR1));
    if(p&0x80000000) printf("RDIMMCR0         = 0x%x\r\n", READ_REG(MEMC_PHY_RDIMMCR0));
    if(p&0x80000000) printf("RDIMMCR1         = 0x%x\r\n", READ_REG(MEMC_PHY_RDIMMCR1));
*/
}

////////////////////////////////////////////////////////////
/// dump_upctl_regs
////////////////////////////////////////////////////////////
void dump_upctl_regs (void) 
{
   //printf("SCFG                 = 0x%x\r\n", READ_REG(MEMC_SCFG));
   //printf("SCTL                 = 0x%x\r\n", READ_REG(MEMC_SCTL));
   //printf("STAT                 = 0x%x\r\n", READ_REG(MEMC_STAT));
   //printf("INTRSTAT             = 0x%x\r\n", READ_REG(MEMC_INTRSTAT));
   //printf("MCMD                 = 0x%x\r\n", READ_REG(MEMC_MCMD));
   //printf("POWCTL               = 0x%x\r\n", READ_REG(MEMC_POWCTL));
   //printf("POWSTAT              = 0x%x\r\n", READ_REG(MEMC_POWSTAT));
   //printf("CMDTSTAT             = 0x%x\r\n", READ_REG(MEMC_CMDTSTAT));
   //printf("CMDTSTATEN           = 0x%x\r\n", READ_REG(MEMC_CMDTSTATEN));
   //printf("MRRCFG0              = 0x%x\r\n", READ_REG(MEMC_MRRCFG0));
   //printf("MRRSTAT0             = 0x%x\r\n", READ_REG(MEMC_MRRSTAT0));
   //printf("MRRSTAT1             = 0x%x\r\n", READ_REG(MEMC_MRRSTAT1));
   //printf("MCFG1                = 0x%x\r\n", READ_REG(MEMC_MCFG1));
   //printf("MCFG                 = 0x%x\r\n", READ_REG(MEMC_MCFG));
   //printf("PPCFG                = 0x%x\r\n", READ_REG(MEMC_PPCFG));
   //printf("MSTAT                = 0x%x\r\n", READ_REG(MEMC_MSTAT));
   //printf("LPDDR23ZQCFG         = 0x%x\r\n", READ_REG(MEMC_LPDDR23ZQCFG));
   //printf("DTUPDES              = 0x%x\r\n", READ_REG(MEMC_DTUPDES));
   //printf("DTUNA                = 0x%x\r\n", READ_REG(MEMC_DTUNA));
   //printf("DTUNE                = 0x%x\r\n", READ_REG(MEMC_DTUNE));
   //printf("DTUPRD0              = 0x%x\r\n", READ_REG(MEMC_DTUPRD0));
   //printf("DTUPRD1              = 0x%x\r\n", READ_REG(MEMC_DTUPRD1));
   //printf("DTUPRD2              = 0x%x\r\n", READ_REG(MEMC_DTUPRD2));
   //printf("DTUPRD3              = 0x%x\r\n", READ_REG(MEMC_DTUPRD3));
   //printf("DTUAWDT              = 0x%x\r\n", READ_REG(MEMC_DTUAWDT));
   //printf("TOGCNT1U             = 0x%x\r\n", READ_REG(MEMC_TOGCNT1U));
   //printf("TOGCNT100N           = 0x%x\r\n", READ_REG(MEMC_TOGCNT100N));
   //printf("TINIT                = 0x%x\r\n", READ_REG(MEMC_TINIT));
   //printf("TRSTH                = 0x%x\r\n", READ_REG(MEMC_TRSTH));
   //printf("TREFI                = 0x%x\r\n", READ_REG(MEMC_TREFI));
   //printf("TMRD                 = 0x%x\r\n", READ_REG(MEMC_TMRD));
   //printf("TRFC                 = 0x%x\r\n", READ_REG(MEMC_TRFC));
   //printf("TRP                  = 0x%x\r\n", READ_REG(MEMC_TRP));
   //printf("TRTW                 = 0x%x\r\n", READ_REG(MEMC_TRTW));
   //printf("TAL                  = 0x%x\r\n", READ_REG(MEMC_TAL));
   //printf("TCL                  = 0x%x\r\n", READ_REG(MEMC_TCL));
   //printf("TCWL                 = 0x%x\r\n", READ_REG(MEMC_TCWL));
   //printf("TRAS                 = 0x%x\r\n", READ_REG(MEMC_TRAS));
   //printf("TRC                  = 0x%x\r\n", READ_REG(MEMC_TRC));
   //printf("TRCD                 = 0x%x\r\n", READ_REG(MEMC_TRCD));
   //printf("TRRD                 = 0x%x\r\n", READ_REG(MEMC_TRRD));
   //printf("TRTP                 = 0x%x\r\n", READ_REG(MEMC_TRTP));
   //printf("TWR                  = 0x%x\r\n", READ_REG(MEMC_TWR));
   //printf("TWTR                 = 0x%x\r\n", READ_REG(MEMC_TWTR));
   //printf("TEXSR                = 0x%x\r\n", READ_REG(MEMC_TEXSR));
   //printf("TXP                  = 0x%x\r\n", READ_REG(MEMC_TXP));
   //printf("TXPDLL               = 0x%x\r\n", READ_REG(MEMC_TXPDLL));
   //printf("TZQCS                = 0x%x\r\n", READ_REG(MEMC_TZQCS));
   //printf("TZQCSI               = 0x%x\r\n", READ_REG(MEMC_TZQCSI));
   //printf("TDQS                 = 0x%x\r\n", READ_REG(MEMC_TDQS));
   //printf("TCKSRE               = 0x%x\r\n", READ_REG(MEMC_TCKSRE));
   //printf("TCKSRX               = 0x%x\r\n", READ_REG(MEMC_TCKSRX));
   //printf("TCKE                 = 0x%x\r\n", READ_REG(MEMC_TCKE));
   //printf("TMOD                 = 0x%x\r\n", READ_REG(MEMC_TMOD));
   //printf("TRSTL                = 0x%x\r\n", READ_REG(MEMC_TRSTL));
   //printf("TZQCL                = 0x%x\r\n", READ_REG(MEMC_TZQCL));
   //printf("TMRR                 = 0x%x\r\n", READ_REG(MEMC_TMRR));
   //printf("TCKESR               = 0x%x\r\n", READ_REG(MEMC_TCKESR));
   //printf("TDPD                 = 0x%x\r\n", READ_REG(MEMC_TDPD));
   //printf("TREFI_MEM_DDR3       = 0x%x\r\n", READ_REG(MEMC_TREFI_MEM_DDR3));
   //printf("DTUWACTL             = 0x%x\r\n", READ_REG(MEMC_DTUWACTL));
   //printf("DTURACTL             = 0x%x\r\n", READ_REG(MEMC_DTURACTL));
   //printf("DTUCFG               = 0x%x\r\n", READ_REG(MEMC_DTUCFG));
   //printf("DTUECTL              = 0x%x\r\n", READ_REG(MEMC_DTUECTL));
   //printf("DTUWD0               = 0x%x\r\n", READ_REG(MEMC_DTUWD0));
   //printf("DTUWD1               = 0x%x\r\n", READ_REG(MEMC_DTUWD1));
   //printf("DTUWD2               = 0x%x\r\n", READ_REG(MEMC_DTUWD2));
   //printf("DTUWD3               = 0x%x\r\n", READ_REG(MEMC_DTUWD3));
   //printf("DTUWDM               = 0x%x\r\n", READ_REG(MEMC_DTUWDM));
   //printf("DTURD0               = 0x%x\r\n", READ_REG(MEMC_DTURD0));
   //printf("DTURD1               = 0x%x\r\n", READ_REG(MEMC_DTURD1));
   //printf("DTURD2               = 0x%x\r\n", READ_REG(MEMC_DTURD2));
   //printf("DTURD3               = 0x%x\r\n", READ_REG(MEMC_DTURD3));
   //printf("DTULFSRWD            = 0x%x\r\n", READ_REG(MEMC_DTULFSRWD));
   //printf("DTULFSRRD            = 0x%x\r\n", READ_REG(MEMC_DTULFSRRD));
   //printf("DTUEAF               = 0x%x\r\n", READ_REG(MEMC_DTUEAF));
   //printf("DFITCTRLDELAY        = 0x%x\r\n", READ_REG(MEMC_DFITCTRLDELAY));
   //printf("DFIODTCFG            = 0x%x\r\n", READ_REG(MEMC_DFIODTCFG));
   //printf("DFIODTCFG1           = 0x%x\r\n", READ_REG(MEMC_DFIODTCFG1));
   //printf("DFIODTRANKMAP        = 0x%x\r\n", READ_REG(MEMC_DFIODTRANKMAP));
   //printf("DFITPHYWRDATA        = 0x%x\r\n", READ_REG(MEMC_DFITPHYWRDATA));
   //printf("DFITPHYWRLAT         = 0x%x\r\n", READ_REG(MEMC_DFITPHYWRLAT));
   //printf("DFITPHYWRDATALAT     = 0x%x\r\n", READ_REG(MEMC_DFITPHYWRDATALAT));
   //printf("DFITRDDATAEN         = 0x%x\r\n", READ_REG(MEMC_DFITRDDATAEN));
   //printf("DFITPHYRDLAT         = 0x%x\r\n", READ_REG(MEMC_DFITPHYRDLAT));
   //printf("DFITPHYUPDTYPE0      = 0x%x\r\n", READ_REG(MEMC_DFITPHYUPDTYPE0));
   //printf("DFITPHYUPDTYPE1      = 0x%x\r\n", READ_REG(MEMC_DFITPHYUPDTYPE1));
   //printf("DFITPHYUPDTYPE2      = 0x%x\r\n", READ_REG(MEMC_DFITPHYUPDTYPE2));
   //printf("DFITPHYUPDTYPE3      = 0x%x\r\n", READ_REG(MEMC_DFITPHYUPDTYPE3));
   //printf("DFITCTRLUPDMIN       = 0x%x\r\n", READ_REG(MEMC_DFITCTRLUPDMIN));
   //printf("DFITCTRLUPDMAX       = 0x%x\r\n", READ_REG(MEMC_DFITCTRLUPDMAX));
   //printf("DFITCTRLUPDDLY       = 0x%x\r\n", READ_REG(MEMC_DFITCTRLUPDDLY));
   //printf("DFIUPDCFG            = 0x%x\r\n", READ_REG(MEMC_DFIUPDCFG));
   //printf("DFITREFMSKI          = 0x%x\r\n", READ_REG(MEMC_DFITREFMSKI));
   //printf("DFITCTRLUPDI         = 0x%x\r\n", READ_REG(MEMC_DFITCTRLUPDI));
   //printf("DFITRCFG0            = 0x%x\r\n", READ_REG(MEMC_DFITRCFG0));
   //printf("DFITRSTAT0           = 0x%x\r\n", READ_REG(MEMC_DFITRSTAT0));
   //printf("DFITRWRLVLEN         = 0x%x\r\n", READ_REG(MEMC_DFITRWRLVLEN));
   //printf("DFITRRDLVLEN         = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLEN));
   //printf("DFITRRDLVLGATEEN     = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLGATEEN));
   //printf("DFISTSTAT0           = 0x%x\r\n", READ_REG(MEMC_DFISTSTAT0));
   //printf("DFISTCFG0            = 0x%x\r\n", READ_REG(MEMC_DFISTCFG0));
   //printf("DFISTCFG1            = 0x%x\r\n", READ_REG(MEMC_DFISTCFG1));
   //printf("DFITDRAMCLKEN        = 0x%x\r\n", READ_REG(MEMC_DFITDRAMCLKEN));
   //printf("DFITDRAMCLKDIS       = 0x%x\r\n", READ_REG(MEMC_DFITDRAMCLKDIS));
   //printf("DFISTCFG2            = 0x%x\r\n", READ_REG(MEMC_DFISTCFG2));
   //printf("DFISTPARCLR          = 0x%x\r\n", READ_REG(MEMC_DFISTPARCLR));
   //printf("DFISTPARLOG          = 0x%x\r\n", READ_REG(MEMC_DFISTPARLOG));
   //printf("DFILPCFG0            = 0x%x\r\n", READ_REG(MEMC_DFILPCFG0));
   //printf("DFITRWRLVLRESP0      = 0x%x\r\n", READ_REG(MEMC_DFITRWRLVLRESP0));
   //printf("DFITRWRLVLRESP1      = 0x%x\r\n", READ_REG(MEMC_DFITRWRLVLRESP1));
   //printf("DFITRWRLVLRESP2      = 0x%x\r\n", READ_REG(MEMC_DFITRWRLVLRESP2));
   //printf("DFITRRDLVLRESP0      = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLRESP0));
   //printf("DFITRRDLVLRESP1      = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLRESP1));
   //printf("DFITRRDLVLRESP2      = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLRESP2));
   //printf("DFITRWRLVLDELAY0     = 0x%x\r\n", READ_REG(MEMC_DFITRWRLVLDELAY0));
   //printf("DFITRRDLVLDELAY0     = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLDELAY0));
   //printf("DFITRRDLVLGATEDELAY0 = 0x%x\r\n", READ_REG(MEMC_DFITRRDLVLGATEDELAY0));
   //printf("DFITRCMD             = 0x%x\r\n", READ_REG(MEMC_DFITRCMD));
   //printf("IPVR                 = 0x%x\r\n", READ_REG(MEMC_IPVR));
   //printf("IPTR                 = 0x%x\r\n", READ_REG(MEMC_IPTR));
}
#endif // SUPPORT_DDR    

static int PU_PD_LUT[32] = { 0x00, 0x01, 0x02, 0x03, 0x06, 0x07, 0x04, 0x05, 
                             0x0C, 0x0D, 0x0E, 0x0F, 0x0A, 0x0B, 0x08, 0x09,
                             0x18, 0x19, 0x1A, 0x1B, 0x1E, 0x1F, 0x1C, 0x1D,
                             0x14, 0x15, 0x16, 0x17, 0x12, 0x13, 0x10, 0x11 };

unsigned int getLUT_Value(int idx,int offset)
{
   if ((idx + offset) <  0) 
   { return(PU_PD_LUT[0]); }
   else if ((idx + offset) > 31) 
   { return(PU_PD_LUT[31]); }
   else 
   { return(PU_PD_LUT[idx+offset]); }
} // getLUT_Value

void init_DDRPHY_LUTs(int acPD,int acPU,int clkPD,int clkPU)
{
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_0,  getLUT_Value( 0, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_1,  getLUT_Value( 1, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_2,  getLUT_Value( 2, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_3,  getLUT_Value( 3, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_6,  getLUT_Value( 4, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_7,  getLUT_Value( 5, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_4,  getLUT_Value( 6, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_5,  getLUT_Value( 7, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_12, getLUT_Value( 8, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_13, getLUT_Value( 9, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_14, getLUT_Value(10, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_15, getLUT_Value(11, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_10, getLUT_Value(12, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_11, getLUT_Value(13, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_8,  getLUT_Value(14, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_9,  getLUT_Value(15, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_24, getLUT_Value(16, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_25, getLUT_Value(17, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_26, getLUT_Value(18, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_27, getLUT_Value(19, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_30, getLUT_Value(20, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_31, getLUT_Value(21, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_28, getLUT_Value(22, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_29, getLUT_Value(23, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_20, getLUT_Value(24, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_21, getLUT_Value(25, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_22, getLUT_Value(26, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_23, getLUT_Value(27, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_18, getLUT_Value(28, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_19, getLUT_Value(29, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_16, getLUT_Value(30, acPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PD_17, getLUT_Value(31, acPD));
   
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_0,  getLUT_Value( 0, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_1,  getLUT_Value( 1, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_2,  getLUT_Value( 2, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_3,  getLUT_Value( 3, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_6,  getLUT_Value( 4, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_7,  getLUT_Value( 5, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_4,  getLUT_Value( 6, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_5,  getLUT_Value( 7, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_12, getLUT_Value( 8, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_13, getLUT_Value( 9, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_14, getLUT_Value(10, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_15, getLUT_Value(11, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_10, getLUT_Value(12, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_11, getLUT_Value(13, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_8,  getLUT_Value(14, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_9,  getLUT_Value(15, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_24, getLUT_Value(16, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_25, getLUT_Value(17, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_26, getLUT_Value(18, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_27, getLUT_Value(19, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_30, getLUT_Value(20, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_31, getLUT_Value(21, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_28, getLUT_Value(22, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_29, getLUT_Value(23, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_20, getLUT_Value(24, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_21, getLUT_Value(25, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_22, getLUT_Value(26, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_23, getLUT_Value(27, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_18, getLUT_Value(28, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_19, getLUT_Value(29, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_16, getLUT_Value(30, acPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_AC_PU_17, getLUT_Value(31, acPU));
   
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_0,  getLUT_Value( 0, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_1,  getLUT_Value( 1, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_2,  getLUT_Value( 2, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_3,  getLUT_Value( 3, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_6,  getLUT_Value( 4, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_7,  getLUT_Value( 5, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_4,  getLUT_Value( 6, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_5,  getLUT_Value( 7, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_12, getLUT_Value( 8, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_13, getLUT_Value( 9, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_14, getLUT_Value(10, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_15, getLUT_Value(11, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_10, getLUT_Value(12, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_11, getLUT_Value(13, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_8,  getLUT_Value(14, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_9,  getLUT_Value(15, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_24, getLUT_Value(16, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_25, getLUT_Value(17, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_26, getLUT_Value(18, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_27, getLUT_Value(19, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_30, getLUT_Value(20, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_31, getLUT_Value(21, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_28, getLUT_Value(22, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_29, getLUT_Value(23, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_20, getLUT_Value(24, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_21, getLUT_Value(25, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_22, getLUT_Value(26, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_23, getLUT_Value(27, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_18, getLUT_Value(28, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_19, getLUT_Value(29, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_16, getLUT_Value(30, clkPD));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PD_17, getLUT_Value(31, clkPD));
   
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_0,  getLUT_Value( 0, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_1,  getLUT_Value( 1, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_2,  getLUT_Value( 2, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_3,  getLUT_Value( 3, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_6,  getLUT_Value( 4, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_7,  getLUT_Value( 5, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_4,  getLUT_Value( 6, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_5,  getLUT_Value( 7, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_12, getLUT_Value( 8, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_13, getLUT_Value( 9, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_14, getLUT_Value(10, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_15, getLUT_Value(11, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_10, getLUT_Value(12, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_11, getLUT_Value(13, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_8,  getLUT_Value(14, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_9,  getLUT_Value(15, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_24, getLUT_Value(16, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_25, getLUT_Value(17, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_26, getLUT_Value(18, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_27, getLUT_Value(19, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_30, getLUT_Value(20, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_31, getLUT_Value(21, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_28, getLUT_Value(22, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_29, getLUT_Value(23, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_20, getLUT_Value(24, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_21, getLUT_Value(25, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_22, getLUT_Value(26, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_23, getLUT_Value(27, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_18, getLUT_Value(28, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_19, getLUT_Value(29, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_16, getLUT_Value(30, clkPU));
   WRITE_REG(MEMC_PHY_DDRPHY_LUT_CLK_PU_17, getLUT_Value(31, clkPU));
} // init_DDRPHY_LUTs

void initDdrSetupParameters()
{
   #ifdef DDR_ENABLE_LDO_WA
      // The DDR Phy LDO must be turned on prior to any DDR operations
      // From cold boot it is on by default so this has no effect, however,
      // prior to entering certain sleep modes, the DDR Phy. LDO is disabled and thus
      // needs re-enabled here.
      rtcwrite(0x3030, 0xf075); 
      wait_us(500); 
   #endif

   ddrSetup.targetFreq=800;
   ddrSetup.xtalFreq=26;
   ddrSetup.apbFreq=200;
   ddrSetup.clkDelay=15;      /* Affects address/command setup and hold timing */
   ddrSetup.clkPuOffset=0;
   ddrSetup.clkPdOffset=0;
   ddrSetup.acPuOffset=-5;     /* -31 .. 31, not all legal steps are pratical -5 =~ 55ohms */
   ddrSetup.acPdOffset=-5;     /* -31 .. 31, not all legal steps are pratical -5 =~ 55ohms */
   ddrSetup.Atlas7DataRout=13; /* See table in RTOS user guide */  
   ddrSetup.Atlas7DataOdt=5;   /* See table in RTOS user guide */  
   ddrSetup.DramDataRout=34;   /* 34 or 40 */
   ddrSetup.DramDataOdt=60;    /* 40, 60, or 120 */
  
   #ifdef DDR_2X8 // Defauls for the 2x8 config
      ddrSetup.acPuOffset=-4; /* -31 .. 31, not all legal steps are pratical -4 =~ 49ohms */
      ddrSetup.acPdOffset=-4; /* -31 .. 31, not all legal steps are pratical -4 =~ 49ohms */
   #endif
  
   // Configurable uPCTL settings
   tREFI =   7800; // [ns] 
   tRFC  = 350000; // [ps]
} // initDdrSetupParameters

void setDdrParameter(enum eDdrParam pIdx,long value)
{
   // Need to keep the compile size small so use an indexed switch statement instead of 
   // string processing. Strings take up too much memory!
   switch(pIdx)
   {
      case e_targetFreq     : ddrSetup.targetFreq=value;     break;
      case e_xtalFreq       : ddrSetup.xtalFreq=value;       break;
      case e_apbFreq        : ddrSetup.apbFreq=value;        break;
      case e_clkDelay       : ddrSetup.clkDelay=value;       break;
      case e_clkPuOffset    : ddrSetup.clkPuOffset=value;    break;
      case e_clkPdOffset    : ddrSetup.clkPdOffset=value;    break;
      case e_acPuOffset     : ddrSetup.acPuOffset=value;     break;
      case e_acPdOffset     : ddrSetup.acPdOffset=value;     break;
      case e_Atlas7DataRout : ddrSetup.Atlas7DataRout=value; break;
      case e_Atlas7DataOdt  : ddrSetup.Atlas7DataOdt=value;  break;
      case e_DramDataRout   : ddrSetup.DramDataRout=value;   break;
      case e_DramDataOdt    : ddrSetup.DramDataOdt=value;    break;
      case e_tREFI          : tREFI=value;                   break;
      case e_tRFC           : tRFC=value;                    break;
      
      default : printf("\r\nsetDdrParameter: %d is unknown\r\n",pIdx); break; 
   } // switch
} // setDdrParameter

void printDdrParameters()
{
   printf("DdrParameters:\r\n");
   printf("   Frequency(Mhz):         %3d\r\n",ddrSetup.targetFreq);
   printf("   xtal freq.(mhz):        %3d\r\n",ddrSetup.xtalFreq);
   printf("   apb freq.(mhz):         %3d\r\n",ddrSetup.apbFreq);
   printf("   Clock Delay:            %3d\r\n",ddrSetup.clkDelay);
   printf("   Clock PU Offset:        %3d\r\n",ddrSetup.clkPuOffset);
   printf("   Clock PD Offset:        %3d\r\n",ddrSetup.clkPdOffset);
   printf("   Addr/Cmd PU Offset:     %3d\r\n",ddrSetup.acPuOffset);
   printf("   Addr/Cmd PD Offset:     %3d\r\n",ddrSetup.acPdOffset);
   printf("   Atlas 7 Data Rout(div): %3d\r\n",ddrSetup.Atlas7DataRout);
   printf("   Atlas 7 Data Odt(div):  %3d\r\n",ddrSetup.Atlas7DataOdt);
   printf("   DRAM Data Rout(ohms):   %3d\r\n",ddrSetup.DramDataRout);
   printf("   DRAM Data Odt(ohms):    %3d\r\n",ddrSetup.DramDataOdt);
   printf("   tREFI(ns):              %6d\r\n",tREFI);
   printf("   tRFC(ps):               %6d\r\n",tRFC);
   printf("\r\n");
} // printDdrParameters

////////////////////////////////////////////////////////////
/// runDdrInit
////////////////////////////////////////////////////////////
int runDdrInit () {
   int ddr_retention=0;
   unsigned int zdata;

   // PLL Setup
   ddrSetup.freq_x16=ddr_freq_set(ddrSetup.targetFreq,ddrSetup.xtalFreq);
   if (DDR_DEBUG_PRINT & 0x01) { printf("\r\nDDR Frequency = %d(Mhz)\r\n",ddrSetup.freq_x16>>4); }
   
   #ifndef DDR_ENABLE_LDO_WA
      printf("\r\nDDR Phy. LDO Workaround is disabled!\r\n");
   #endif

   int cas_write_latency = (ddrSetup.freq_x16 <= (400<<4)) ?  5 :
                           (ddrSetup.freq_x16 <= (533<<4)) ?  6 :
                           (ddrSetup.freq_x16 <= (666<<4)) ?  7 :
                           (ddrSetup.freq_x16 <= (800<<4)) ?  8 :
                           (ddrSetup.freq_x16 <= (934<<4)) ?  9 :
                           (ddrSetup.freq_x16 <= (1069<<4)) ? 10 :
                           (ddrSetup.freq_x16 <= (1200<<4)) ? 11 :
                           (ddrSetup.freq_x16 <= (1333<<4)) ? 12 : 0;
   int cas_latency = (cas_write_latency ==  5) ?  6 :
                     (cas_write_latency ==  6) ?  8 :
                     (cas_write_latency ==  7) ? 10 :
                     (cas_write_latency ==  8) ? 11 :
                     (cas_write_latency ==  9) ? 13 :
                     (cas_write_latency == 10) ? 14 : 0;
   int tFAW_cycles     =      cycles(tFAW     , ddrSetup.freq_x16);
   int tWR_cycles      =      cycles(tWR      , ddrSetup.freq_x16);
   int tRP_cycles      =      cycles(tRP      , ddrSetup.freq_x16);
   int tRAS_cycles     =      cycles(tRAS     , ddrSetup.freq_x16);
   int tRCD_cycles     =      cycles(tRCD     , ddrSetup.freq_x16);
   int tRFC_cycles     =      cycles(tRFC     , ddrSetup.freq_x16);
   int tRRD_cycles     = max (cycles(tRRD     , ddrSetup.freq_x16), tRRDn);
   int tWTR_cycles     = max (cycles(tWTR     , ddrSetup.freq_x16), tWTRn);
   int tRTP_cycles     = max (cycles(tRTP     , ddrSetup.freq_x16), tRTPn);
   int tXP_cycles      = max (cycles(tXP      , ddrSetup.freq_x16), tXPn);
   int tXS_cycles      = max (cycles(tXS      , ddrSetup.freq_x16), tXSn);
   int tXPDLL_cycles   = max (cycles(tXPDLL   , ddrSetup.freq_x16), tXPDLLn);
   int tZQCS_cycles    = max (cycles(tZQCS    , ddrSetup.freq_x16), tZQCSn);
   int tZQCL_cycles    = max (cycles(tZQCL    , ddrSetup.freq_x16), tZQCLn);
   int tCKSRE_cycles   = max (cycles(tCKSRE   , ddrSetup.freq_x16), tCKSREn);
   int tCKSRX_cycles   = max (cycles(tCKSRX   , ddrSetup.freq_x16), tCKSRXn);
   int tCKE_cycles     = max (cycles(tCKE     , ddrSetup.freq_x16), tCKEn);
   int tMOD_cycles     = max (cycles(tMOD     , ddrSetup.freq_x16), tMODn);
   int tWLO_cycles     =      cycles(tWLO     , ddrSetup.freq_x16);
   int tRC_cycles      = tRP_cycles + tRAS_cycles;

   // tFAW= (4 + MCFG.tfaw_cfg)*tRRD - tfaw_cfg_offset
   int tfaw_cfg = 0;
   int tfaw_cfg_offset = 4*tRRD_cycles - tFAW_cycles;
   while (tfaw_cfg_offset < 0) { tfaw_cfg++; tfaw_cfg_offset += tRRD_cycles; }
   if (tFAW_cycles != ((4+tfaw_cfg)*tRRD_cycles-tfaw_cfg_offset)) { return (0xee700000 | (tfaw_cfg<<8) | tfaw_cfg_offset); }

   // tWR_cycles allowed values
   if  (tWR_cycles < 5)                        tWR_cycles = 5;
   if ((tWR_cycles > 8) && (tWR_cycles & 1))   tWR_cycles++;
   if  (tWR_cycles > 16)                       return(0xee800000);

   // RCD should be == cas_latency
   if (tRCD_cycles > cas_latency) return (0xee810000);
   tRCD_cycles = cas_latency;

   int cas_latency_code = cas_latency - 4;
   int write_recovery_code = (tWR_cycles < 8) ? (tWR_cycles - 4) : ((tWR_cycles >> 1) & 7);
   int mode_register_0 = 0x100 | (write_recovery_code << 9) | ((cas_latency_code&8) >> 1) | ((cas_latency_code&7) << 4);
   int mode_register_1 = ((ddrSetup.DramDataRout < 40) ? 0x02 : 0x00) | ((ddrSetup.DramDataOdt < 120) ? ((ddrSetup.DramDataOdt < 60) ? 0x44 : 0x04) : 0x40);
   int write_latency = cas_write_latency + AL;
   int read_latency = cas_latency + AL;
   int cas_write_latency_code = cas_write_latency - 5;
   int mode_register_2 = cas_write_latency_code << 3;
   int mode_register_3 = 0x0;

   ////////////////////////////////////////////////////
   // enable timer3 in loop mode
   WRITE_REG(TIMER_32COUNTER_3_CTRL, 5);
   ////////////////////////////////////////////////////
   SPINLOCK_LOCK(0xee200000);
   WRITE_REG(CPURTCIOBG_ADDR , 0x18843004);
   WRITE_REG(CPURTCIOBG_WRBE , 0);
   WRITE_REG(CPURTCIOBG_CTRL , 1);
   POLL_REG(CPURTCIOBG_CTRL, 1, 0, 1000, 0xee300000);
   // read dram_hold (PWRC_PDN_CTRL_CLR[3]) and keep it (ddr_retention) for future usage
   ddr_retention = (READ_REG(CPURTCIOBG_DATA) >> 3) & 1;
   SPINLOCK_RELEASE;
   ////////////////////////////////////////////////////

   if (DDR_DEBUG_PRINT & 0x10) { printf("ddr_retention = %d\r\n", ddr_retention); }

   WRITE_REG(CLKC_RSTC_MEM_SW_RST_CLR , 0xb); // assert reset (active low)
   wait_us(1);
   WRITE_REG(CLKC_MEM_LEAF_CLK_EN_CLR , 0xF); // disable clock
   wait_us(1);
   WRITE_REG(CLKC_RSTC_MEM_SW_RST_SET , 0xb); // deassert reset
   wait_us(1);
   WRITE_REG(CLKC_MEM_LEAF_CLK_EN_SET, 0xf);   // enable clocks

   #if DDR_8BITS
      WRITE_REG(MEMC_PHY_DX1GCR , 0x0);
      WRITE_REG(DDRM_SB_MAIN_SIDEBAND_MANAGER_FLAG_OUT_SET0 , (1<<2)); // NoC: bit[2] is partial_pop enable
      WRITE_REG(UPCTL_DATA_T_MAIN_SCHEDULER_DDR_CONF , 2); // NoC: choose ddr-conf in which one column bit goes to msb of conf
      WRITE_REG(MEMC_PPCFG , 1); // upctl: set ppmem=1 (partially populated memory)
      WRITE_REG(MEMC_DFISTCFG0, 1<<2);
   #endif

   RD_MOD_WR(MEMC_PHY_DSGCR, 0xffffffff, (1<<18)); // set RRMODE bit (Rise to Rise mode)

   WRITE_REG(MEMC_PHY_PTR0 , ((ddrSetup.apbFreq+1)<<21) | ((4*ddrSetup.apbFreq+1)<<6) | (16<<0));
   WRITE_REG(MEMC_PHY_PTR1 , ((100*ddrSetup.apbFreq+1)<<16) | ((3*ddrSetup.apbFreq+1)<<0));
   //RD_MOD_WR(MEMC_PHY_DCR , 0xffffffff, (DDR_2T_EN<<28)); // STAR 9000675641 B2-Medium Training does not follow 2T rules when 2T enabled. Workaround: Disable the 2T timing feature
   WRITE_REG(MEMC_PHY_MR0 , mode_register_0);
   WRITE_REG(MEMC_PHY_MR1 , mode_register_1);
   WRITE_REG(MEMC_PHY_MR2 , mode_register_2);
   WRITE_REG(MEMC_PHY_MR3 , mode_register_3);
   WRITE_REG(MEMC_PHY_DTPR0 ,  (tRC_cycles <<26) |
                               (tRRD_cycles<<22) |
                               (tRAS_cycles<<16) |
                               (tRCD_cycles<<12) |
                               (tRP_cycles << 8) |
                               (tWTR_cycles<< 4) |
                               (tRTP_cycles<< 0));
   WRITE_REG(MEMC_PHY_DTAR0 , DTAR_PHYSICAL_ADDR | 0x00);
   WRITE_REG(MEMC_PHY_DTAR1 , DTAR_PHYSICAL_ADDR | 0x08);
   WRITE_REG(MEMC_PHY_DTAR2 , DTAR_PHYSICAL_ADDR | 0x10);
   WRITE_REG(MEMC_PHY_DTAR3 , DTAR_PHYSICAL_ADDR | 0x18);

   RD_MOD_WR(MEMC_PHY_DTCR  , 0xf0ffffff , 0x0100004f); // enable only rank0 | enable MPR mode |Training repeat is max[3:0]
   //RD_MOD_WR(MEMC_PHY_DTCR  , 0xf07fffff , 0x0180004f); // enable only rank0 | enable MPR mode |Training repeat is max[3:0]|Allow extended gate training

   //extend DQS training
   //RD_MOD_WR(MEMC_PHY_DSGCR, ~(1<<6),  1<<6);
   //RD_MOD_WR(MEMC_PHY_DTCR,  ~(1<<7),  0<<7); //DTCMPD set to 0x0 - don't compare data during gate training

   if (ddr_retention) {
      POLL_REG(MEMC_PHY_PGSR0,          1, 1, 100000, 0xee100000);
      RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x40001); // CTLDINIT=1 (initialization is done by sw throught the upctl)
      POLL_REG(MEMC_PHY_PGSR0,          1, 1, 100000, 0xee110000);
   }
   else {
      WAIT_PGSR0_IDONE(0xee100000);
      RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x40001); // CTLDINIT=1 (initialization is done by sw throught the upctl)
      WAIT_PGSR0_IDONE(0xee110000);
   }

   WRITE_REG(MEMC_TOGCNT1U       , cycles (1000000/2, ddrSetup.freq_x16));
   WRITE_REG(MEMC_TOGCNT100N     , cycles (100000/2, ddrSetup.freq_x16));
   WRITE_REG(MEMC_TINIT          , 200); // 200[us] as JEDEC defines
   WRITE_REG(MEMC_TRSTH          , 500); // 500[us] as JEDEC defines

   WRITE_REG(MEMC_MCFG           , 0x00000021 | (DDR_2T_EN<<3) | (tfaw_cfg << 18));
   RD_MOD_WR(MEMC_MCFG1, 0xffffffff, (tfaw_cfg_offset << 8));
   // DFI timing (4.3.12 in PUB spec) for HDR mode RR mode
   WRITE_REG(MEMC_DFITCTRLDELAY  , 3);
   WRITE_REG(MEMC_DFITPHYWRDATA  , 1);
   WRITE_REG(MEMC_DFITPHYWRLAT   , (write_latency & 1) ? ((write_latency-3)>>1) : ((write_latency-4)>>1)); // from the PUB spec: "The only variable in the DFI timing is the tphy_wrlat, which is the spacing between DFI write command and DFI write data enable (dfi_wrdata_en) signal. As shown in Table 4-8 on page 179, this spacing is equal to (WL-3)/2 for odd WL and (WL-4)/2 for even WL."
   WRITE_REG(MEMC_DFITRDDATAEN   , (read_latency  & 1) ? ((read_latency -3)>>1) : ((read_latency -4)>>1)); // from the PUB spec: "The only variable in the DFI timing is the trddata_en, which is the spacing between DFI read command and DFI read data enable (dfi_rddata_en) signal. As shown in Table 4-8 on page 179, this spacing is equal to (RL-3)/2 for odd commands and (RL-4)/2 for even commands."
   WRITE_REG(MEMC_DFITPHYRDLAT   , 15); // TBD 13.75 according to PUB spec: tphy_rdlat = tCLAT + tQSLAT + tDFI2PHY + tPHY2DFI + (tRSLMAX / 2) + tXLAT + tOPLAT + tIPLAT + tSDRLAT = 1.25 + 5 + 3 + 1 + (7/2) + 0 + 0 + 0 + 0 = 13.75
   WRITE_REG(MEMC_DFITDRAMCLKDIS , 2); // TBD should be 1 according to PUB spec
   WRITE_REG(MEMC_DFITDRAMCLKEN  , 2); // TBD should be 1 according to PUB spec
   WRITE_REG(MEMC_DFITPHYUPDTYPE0, (unsigned int)((ddrSetup.freq_x16 + 4*ddrSetup.apbFreq - 1)/(4*ddrSetup.apbFreq))); //   8*tCFGCLK/tDFICLK, rounded up
   WRITE_REG(MEMC_DFITPHYUPDTYPE1, (unsigned int)((25*ddrSetup.freq_x16 + ddrSetup.apbFreq - 1)/(ddrSetup.apbFreq))); // 800*tCFGCLK/tDFICLK, rounded up

   WRITE_REG(MEMC_DFISTCFG1      , 1); // Enables support of the dfi_dram_clk_disable signal with Self Refresh (SR).
   WRITE_REG(MEMC_DFILPCFG0      , (3<<28)|(1<<24)|(7<<16)|(3<<12)|(1<<8)|(3<<4)|(1<<0));

   WRITE_REG(MEMC_POWCTL         , 0x1); // When this bit is set to 1'b1, uPCTL starts the CKE and RESET# power up sequence to the memories.
   POLL_REG(MEMC_POWSTAT, 1, 1, 100000, 0xee400000);

   WRITE_REG(MEMC_DFIODTCFG      , 0x8); // rank0_odt_write_sel=1 (activate ODT pin during writes)
   //RD_MOD_WR(MEMC_DFIODTCFG1     , ~0x1f, (write_latency - 1)); // ODT signal should be asserted/deasserted ODTLon/off (=WL-2) cycles prior to when DDR is activating RTT, therefore no need to delay the ODT signal
   WRITE_REG(MEMC_TREFI          , (int)(tREFI / 100) | (1<<31)); // in units of 100[ns], round down
   WRITE_REG(MEMC_TMRD           , tMRDn);
   WRITE_REG(MEMC_TRFC           , tRFC_cycles);
   WRITE_REG(MEMC_TRP            , tRP_cycles);
   WRITE_REG(MEMC_TRTW           , 2);
   WRITE_REG(MEMC_TAL            , AL);
   WRITE_REG(MEMC_TCL            , cas_latency);
   WRITE_REG(MEMC_TCWL           , cas_write_latency);
   WRITE_REG(MEMC_TRAS           , tRAS_cycles);
   WRITE_REG(MEMC_TRC            , tRC_cycles);
   WRITE_REG(MEMC_TRCD           , tRCD_cycles);
   WRITE_REG(MEMC_TRRD           , tRRD_cycles);
   WRITE_REG(MEMC_TRTP           , tRTP_cycles);
   WRITE_REG(MEMC_TWR            , tWR_cycles);
   WRITE_REG(MEMC_TWTR           , tWTR_cycles);
   WRITE_REG(MEMC_TEXSR          , tDLLKn);
   WRITE_REG(MEMC_TXP            , tXP_cycles);
   WRITE_REG(MEMC_TXPDLL         , tXPDLL_cycles);
   WRITE_REG(MEMC_TZQCL          , tZQCL_cycles);
   WRITE_REG(MEMC_TZQCS          , tZQCS_cycles);
   WRITE_REG(MEMC_TZQCSI         , 10); // perform ZQCS every TZQCSI refreshes
   WRITE_REG(MEMC_TDQS           , 1); // not relevant (only 1 rank)
   WRITE_REG(MEMC_TCKSRE         , tCKSRE_cycles);
   WRITE_REG(MEMC_TCKSRX         , tCKSRX_cycles);
   WRITE_REG(MEMC_TCKE           , tCKE_cycles);
   WRITE_REG(MEMC_TMOD           , tMOD_cycles);
   WRITE_REG(MEMC_TRSTL          , cycles(100000, ddrSetup.freq_x16)); // Memory Reset Low time, in memory clock cycles. Defines the time period to hold dfi_reset_n signal low during a software driven DDR3 Reset Operation. The value programmed must correspond to at least 100ns of delay.
   WRITE_REG(MEMC_TMRR           , 0);
   WRITE_REG(MEMC_TCKESR         , tCKE_cycles + 1);
   WRITE_REG(MEMC_TDPD           , 0); // not relevant for DDR3 and should be configured with 0
   WRITE_REG(MEMC_TREFI_MEM_DDR3 , cycles(tREFI * 1000, ddrSetup.freq_x16) - 32); // DDR3 tREFI (7.8us/3.9us) time, in memory clock cycles.
   RD_MOD_WR(MEMC_PHY_PGCR2, 0xfffc0000, (cycles(8 * tREFI * 1000, ddrSetup.freq_x16) - 800)); // need 9, but use 8 to be on the safe side...

   RD_MOD_WR(MEMC_SCFG, 0xffffffff, 0x1); // set hw_low_power_en to 1 to enable hardware srefresh_enter

   // Change PHY [1]/Controller[0] initiated updates status
   //RD_MOD_WR(MEMC_DFIUPDCFG, ~0x2, 0x0);
   //RD_MOD_WR(MEMC_DFIUPDCFG, ~0x1, 0x0);

   WRITE_REG(MEMC_PHY_DTPR1 ,  ((tWLO_cycles+5)                        << 26) |
                               ((tWLMRDn)                              << 20) |
                               ((tRFC_cycles)                          << 11) |
                               ((tFAW_cycles)                          <<  5) |
                               ((tMOD_cycles-12)                       <<  2) |
                               ((tMRDn-4)                              <<  0));

   WRITE_REG(MEMC_PHY_DTPR2 ,  ((tCCDn-4)                              << 31) |
                               ((tRTWn)                                << 30) |
                               ((0)                                    << 29) | // tRTODT
                               ((tDLLKn)                               << 19) |
                               ((tCKE_cycles+1)                        << 15) |
                               ((max(tXPDLL_cycles, tXP_cycles))       << 10) |
                               ((max(tXS_cycles, tXSDLLn))             <<  0));

   // Updated FRQSEL range of the PLL
   RD_MOD_WR(MEMC_PHY_PLLCR, ~(3<<18), ((ddrSetup.freq_x16 <= (250<<5)) ? (1<<18) : (0<<18)));

   // Change DQS resistors of the PDQSR cell
   RD_MOD_WR(MEMC_PHY_DXCCR, ~0x1FE0, (0x1<<9) | (0x1<<5));

   if (ddr_retention) {
      // prior to releasing the retention need to use manual impedance instead values from auto-calibration
      // ZDEN=1 (MEMC_PHY_ZQ0CR0[28], Impedance Over-ride Enable).
      // the ZDATA (MEMC_PHY_ZQ0CR0[27:0]) value itself
      // should be read from MEMC_PHY_ZQ0SR0[27:0] prior to activating the DDR IO retention
      zdata = 0x0014a; // READ_REG(DDR_ZDATA); // due to unknown reason, when starting ZQ calibration with some values, the calibration fails (see Synopsys SolvNet Case #8000753977). 
                                               // therefore need to start with this reset value, in which case the calibration succeeds. this value is good enough to keep the CKE 
                                               // stable until ZQ calibration will be executed.
      WRITE_REG(MEMC_PHY_ZQ0CR0 , (0x50000000 | zdata));
   
      // upctl into self-refresh mode (dwc_ddr_upctl_umctl_databook.pdf: P2.6.1.1 Software Control of the uPCTL State M/C)
      WRITE_REG(MEMC_SCTL, SCTL_CFG); POLL_REG(MEMC_STAT, 0x07, STAT_CONFIG   , 10000, 0xee500000);
      WRITE_REG(MEMC_SCTL, SCTL_GO); POLL_REG(MEMC_STAT, 0x07, STAT_ACESS    , 10000, 0xee510000);
      WRITE_REG(MEMC_SCTL, SCTL_SLEEP); POLL_REG(MEMC_STAT, 0x07, STAT_LOW_POWER, 10000, 0xee520000);
      POLL_REG(MEMC_STAT, 0x70, 0x40, 1, 0xee530000); // Check that STAT.lp_trig[2]=1, to confirm that this transition was caused by Software
   
      // release DDR IO retention (clear dram_hold (PWRC_PDN_CTRL_CLR[3]))
      SPINLOCK_LOCK(0xee210000);
      WRITE_REG(CPURTCIOBG_ADDR , 0x18843004);
      WRITE_REG(CPURTCIOBG_DATA , 0x8);
      WRITE_REG(CPURTCIOBG_WRBE , (0xF << 4) | 1);
      WRITE_REG(CPURTCIOBG_CTRL , 1);
      POLL_REG(CPURTCIOBG_CTRL, 1, 0, 1000, 0xee310000);
      SPINLOCK_RELEASE;
   
      // Remove ZQ calibration override by writing 0 to PUB register ZQnCR.ZDEN
      WRITE_REG(MEMC_PHY_ZQ0CR0 , (0x40000000 | zdata));
   
      // clear flags in PGSR0 (otherwise next WAIT_PGSR0_IDONE fails on ZCERR)
      RD_MOD_WR(MEMC_PHY_PIR, ~(1<<27), (1<<27));
   
      // Trigger ZQ calibration by writing 1 to PUB PIR.INIT and 1 to PIR.ZCAL
      WRITE_REG(MEMC_PHY_PIR , 0x3);
      // Wait for ZQ calibration to finish by polling a 1 status on PUB register PGSR0.IDONE
      WAIT_PGSR0_IDONE(0xee120000);
   
      // Indicate to the PUB that SDRAM initialization will not be triggered by writing 1 to PUB register PIR.INIT and 1 to PIR.CTLDINIT
      WRITE_REG(MEMC_PHY_PIR , 0x40001);
      WAIT_PGSR0_IDONE(0xee130000);
   
      // Issue self-refresh exit command to the memory (uPCTL databook P2.6.1.1)
      WRITE_REG(MEMC_SCTL , SCTL_WAKEUP);
      POLL_REG(MEMC_STAT, 0x07, STAT_ACESS    , 10000, 0xee540000);
   } // ddr_retention

   WRITE_REG(MEMC_SCTL , SCTL_CFG); // from 'Init_mem' if !ddr_retention; from 'Access' if ddr_retention
   POLL_REG(MEMC_STAT, 0xf, STAT_CONFIG, 100, 0xee650000);

   WRITE_REG(MEMC_MCMD , (1<<31) | (1<<20) | (2<<17) | (mode_register_2<<4) | 3); // MRS
   POLL_REG(MEMC_MCMD, 0x80000000, 0, 100, 0xee600000);

   WRITE_REG(MEMC_MCMD , (1<<31) | (1<<20) | (3<<17) | (mode_register_3<<4) | 3); // MRS
   POLL_REG(MEMC_MCMD, 0x80000000, 0, 100, 0xee610000);

   WRITE_REG(MEMC_MCMD , (1<<31) | (1<<20) | (1<<17) | (mode_register_1<<4) | 3); // MRS
   POLL_REG(MEMC_MCMD, 0x80000000, 0, 100, 0xee620000);

   WRITE_REG(MEMC_MCMD , (1<<31) | (1<<20) | (0<<17) | (mode_register_0<<4) | 3); // MRS
   POLL_REG(MEMC_MCMD, 0x80000000, 0, 100, 0xee630000);

   WRITE_REG(MEMC_MCMD , (1<<31) | (1<<20) | 5); // ZQCL
   POLL_REG(MEMC_MCMD, 0x80000000, 0, 100, 0xee640000);

   WRITE_REG(MEMC_SCTL, SCTL_GO);
   POLL_REG(MEMC_STAT, 0xf, STAT_ACESS , 100, 0xee660000);

   RD_MOD_WR(MEMC_PHY_ACBDLR, 0xffffffC0, ddrSetup.clkDelay);

   // Enable AC ODT
   // WRITE_REG(MEMC_PHY_ACIOCR , 0x30400816);

   // dxOdt is either 120, 60, or 40 no in-between values allowed
   // dxRout is either 34 or 40ohms, no other setting available
   //RD_MOD_WR(MEMC_PHY_ZQ0CR1, 0xffffff00, (ddrSetup.dataOdt<120 ? (ddrSetup.dataOdt < 60 ? 0x80 : 0x50) : 0x10) | (ddrSetup.dataRout<40 ? 0x0d : 0x0b));
   RD_MOD_WR(MEMC_PHY_ZQ0CR1, 0xffffff00, ((ddrSetup.Atlas7DataOdt & 0xf) << 4) | (ddrSetup.Atlas7DataRout & 0xf));

   // init the LUTs for Address/Command and Clock
   init_DDRPHY_LUTs(ddrSetup.acPdOffset,ddrSetup.acPuOffset,ddrSetup.clkPdOffset,ddrSetup.clkPuOffset);

   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x0003); WAIT_PGSR0_IDONE(0xee1e0000); // Impedance calibration

   // Disable impedance calibration enable
   // RD_MOD_WR(MEMC_PHY_ZQ0CR0, ~(1<<30), 0<<30);

   /*
   WRITE_REG(MEMC_PHY_ZQ0CR0,      (1<<28) |  // impedance override enable
                                   (0x5<<15) |  // ODT pullup
                                   (0x5<<10) |  // ODT pulldown
                                   (0xc<< 5) |  // output pullup
                                   (0xc<< 0)); // output pulldown
   */

   // SEED to the BDL's
   #if DDR_8BITS
      WRITE_REG(MEMC_PHY_DX0BDLR0, (0x1C)|(0x1C<<6)|(0x1C<<12)|(0x1C<<18)|(0x1C<<24));
      WRITE_REG(MEMC_PHY_DX0BDLR1, (0x1C)|(0x1C<<6)|(0x1C<<12)|(0x1C<<18)|(0x1C<<24));
      WRITE_REG(MEMC_PHY_DX0BDLR2, (0x1C)|(0x1C<<6)|(0x00<<12));
   #else
      // seed of 0x18
      WRITE_REG(MEMC_PHY_DX0BDLR0, (0x18)|(0x18<<6)|(0x18<<12)|(0x18<<18)|(0x18<<24));
      WRITE_REG(MEMC_PHY_DX0BDLR1, (0x18)|(0x18<<6)|(0x18<<12)|(0x18<<18)|(0x18<<24));
      WRITE_REG(MEMC_PHY_DX0BDLR2, (0x18)|(0x18<<6)|(0x00<<12));

      WRITE_REG(MEMC_PHY_DX1BDLR0, (0x18)|(0x18<<6)|(0x18<<12)|(0x18<<18)|(0x18<<24));
      WRITE_REG(MEMC_PHY_DX1BDLR1, (0x18)|(0x18<<6)|(0x18<<12)|(0x18<<18)|(0x18<<24));
      WRITE_REG(MEMC_PHY_DX1BDLR2, (0x18)|(0x18<<6)|(0x00<<12));
   #endif

   //if (DDR_DEBUG_PRINT&0x10)
   //{
   //   printf("The value of DX0BDLR1 is 0x%x\r\n", READ_REG(MEMC_PHY_DX0BDLR1));
   //   printf("The value of DX1BDLR1 is 0x%x\r\n", READ_REG(MEMC_PHY_DX1BDLR1));
   //
   //   printf("The value of DX0GTR before training is 0x%x\r\n", READ_REG(MEMC_PHY_DX0GTR));
   //   printf("The value of DX1GTR before training is 0x%x\r\n", READ_REG(MEMC_PHY_DX1GTR));    
   //}

   // WLRKEN=0x1 (DXnGCR[29:26], Write level rank enable) Enable only rank 0 for WL
   RD_MOD_WR(MEMC_PHY_DX0GCR, 0xC7ffffff, 0);
   RD_MOD_WR(MEMC_PHY_DX1GCR, 0xC7ffffff, 0);

   //printf("The value of DTCR is 0x%x\r\n", READ_REG(MEMC_PHY_DTCR));
   //RD_MOD_WR(MEMC_PHY_DTCR, ~0x1000 , 0x0); // remove DM training  
   //RD_MOD_WR(MEMC_PHY_DTCR, ~0xf00 , 0xf00); // change WDQ training margin
   //printf("The value of DTCR is 0x%x\r\n", READ_REG(MEMC_PHY_DTCR));

   //// MDLEN=0 (DXnGCR[30], master delay line enable)
   //RD_MOD_WR(MEMC_PHY_DX0GCR, 0xbfffffff, 0);
   //RD_MOD_WR(MEMC_PHY_DX1GCR, 0xbfffffff, 0);

   #if (READ_MAN_TRAIN) || (WRITE_MAN_TRAIN)
      //disable VT by setting PGCR1.INHVT=1 // address 0x1081000C bit[26]
      RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 1<<26);  
   #endif    

   //disable ZQ by setting DSGCR.ZUEN=0  // address 0x10810040 bit[2]  ==> 
   //RD_MOD_WR(MEMC_PHY_DSGCR, ~(1<<2),  0<<2);

   //Change write latency select type (encoding type)
   //RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<6), 0<<6);

   //Enable VT compensation for AC macro
   //RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<9), 1<<9);

   // Add delay to clk [0:5]
   //WRITE_REG(MEMC_PHY_ACBDLR,0x10);

   //Enable DTO
   //RD_MOD_WR(MEMC_PHY_DSGCR, ~(1<<17), 1<<17);
   //Enable ATO output and place some default config in PLLCR
   //RD_MOD_WR(MEMC_PHY_DSGCR, ~(1<<16), 1<<16);
   //RD_MOD_WR(MEMC_PHY_PLLCR, 0xffffff00, 0x85);

   // Change primary DQ bit
   //printf("DCR              = 0x%x\r\n", READ_REG(MEMC_PHY_DCR));
   //RD_MOD_WR(MEMC_PHY_DCR , 0xfffc038f, (0x80<<10)|(0x7<<4));
   //printf("DCR              = 0x%x\r\n", READ_REG(MEMC_PHY_DCR));

   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x0201); WAIT_PGSR0_IDONE(0xee140000); // write_leveling

   //if (DDR_DEBUG_PRINT&0x80) 
   //{ timer_start = READ_REG(TIMER_32COUNTER_3_MATCH_NUM); }

   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x0401); WAIT_PGSR0_IDONE(0xee150000); // read_dqs_gate_training

   //if (DDR_DEBUG_PRINT&0x80) 
   //{
   //   timer_end = READ_REG(TIMER_32COUNTER_3_MATCH_NUM);
   //   printf("dqs gate training time = %d [ns] ((0x%x 0x%x))\r\n", (timer_end-timer_start)*1000/26, timer_start, timer_end);
   //}

   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x0801); WAIT_PGSR0_IDONE(0xee160000); // write_leveling2
   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x1001); WAIT_PGSR0_IDONE(0xee170000); // read_data_bit_deskew
   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x2001); WAIT_PGSR0_IDONE(0xee180000); // write_data_bit_deskew
   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x4001); WAIT_PGSR0_IDONE(0xee190000); // read_data_eye_training
   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x8001); WAIT_PGSR0_IDONE(0xee1a0000); // write_data_eye_training
   //RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0xfe01); WAIT_PGSR0_IDONE(0xee1f0000); // training_all
   RD_MOD_WR(MEMC_PHY_DTCR, ~0x40 , 0x0); // DTMPR=0 (Read Data Training not to use MPR)
   RD_MOD_WR(MEMC_PHY_PIR, 0xffffffff, 0x0401); WAIT_PGSR0_IDONE(0xee1b0000); // read_dqs_gate_training

   if (DDR_DEBUG_PRINT&0x10) { dump_ddrphy_regs(); }
  
   #if (READ_MAN_TRAIN) || (WRITE_MAN_TRAIN)
      // MDLEN=0 (DXnGCR[30], master delay line disable)
      RD_MOD_WR(MEMC_PHY_DX0GCR, 0xbfffffff, 0);
      RD_MOD_WR(MEMC_PHY_DX1GCR, 0xbfffffff, 0);
   #endif
      
   #if (WRITE_MAN_TRAIN)
      int bw_w;
     
      // win (addr, mask, range, startbit, byte, freq_x16)
      bw_w = win(MEMC_PHY_DX0LCDLR1, 0xffffff00, 0x3f, 0, 0);
      RD_MOD_WR(MEMC_PHY_DX0LCDLR1, 0xffffff00, bw_w);
      if (DDR_DEBUG_PRINT&0x10) { printf("Write Training: DX0LCDLR1 WDQ = 0x%02x\r\n", bw_w); }

      bw_w = win(MEMC_PHY_DX1LCDLR1, 0xffffff00, 0x3f, 0, 1);
      RD_MOD_WR(MEMC_PHY_DX1LCDLR1, 0xffffff00, bw_w);
      if (DDR_DEBUG_PRINT&0x10) { printf("Write Training: DX1LCDLR1 WDQ = 0x%02x\r\n", bw_w);}
   #endif

   #if (READ_MAN_TRAIN)
      int bw_r;
     
      // win (addr, mask, range, startbit, byte, freq_x16)
      bw_r = win(MEMC_PHY_DX0LCDLR1, 0xffff00ff, 0x3f, 8, 0);
      RD_MOD_WR(MEMC_PHY_DX0LCDLR1, 0xffff00ff, bw_r << 8);
      if (DDR_DEBUG_PRINT&0x10) { printf("Read Training: DX0LCDLR1 RDQ = 0x%02x\r\n", bw_r); }

      bw_r = win(MEMC_PHY_DX1LCDLR1, 0xffff00ff, 0x3f, 8, 1);
      RD_MOD_WR(MEMC_PHY_DX1LCDLR1, 0xffff00ff, bw_r << 8);
      if (DDR_DEBUG_PRINT&0x10) { printf("Read Training: DX1LCDLR1 RDQ = 0x%02x\r\n", bw_r); }
   #endif // READ_MAN_TRAIN
   
   #if (READ_MAN_TRAIN) || (WRITE_MAN_TRAIN)
      // MDLEN=1 (DXnGCR[30], master delay line enable)
      RD_MOD_WR(MEMC_PHY_DX0GCR, 0xbfffffff, 0x40000000);
      RD_MOD_WR(MEMC_PHY_DX1GCR, 0xbfffffff, 0x40000000);
     
      // enable VT by setting PGCR1.INHVT=0 // address 0x1081000C bit[26]
      RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 0<<26);  
   #endif

   // keep zdata in always-on domain memory, to be used after deep-sleep
   zdata = READ_REG(MEMC_PHY_ZQ0SR0) & 0x0fffffff;
   WRITE_REG(DDR_ZDATA, zdata);

   return(0);
} // runDdrInit

////////////////////////////////////////////////////////////
/// ddr_self_refresh
////////////////////////////////////////////////////////////
int ddr_self_refresh_enter (void) {
  // disconnect all DDR ports in the NoC (not must in case low-power by sw)
  WRITE_REG(CLKC_PLUG_CLK_IDLEREQ_SET,    0x7c);
  POLL_REG (CLKC_PLUG_CLK_IDLEACK_STATUS, 0x7c, 0x7c, 100000, 0xee100000);
  POLL_REG (CLKC_PLUG_CLK_IDLE_STATUS,    0x7c, 0x7c, 100000, 0xee110000);

  // move upctl to low-power state
  WRITE_REG(MEMC_SCTL, SCTL_SLEEP);
  POLL_REG(MEMC_STAT, 0x07, STAT_LOW_POWER, 100000, 0xee120000);

  // set dram_hold=1 (PWRC_OFFSET_PDN_CTRL_SET[3]=1)
  WRITE_REG(CPURTCIOBG_WRBE, 0xf1);
  WRITE_REG(CPURTCIOBG_ADDR, 0x18843000);
  WRITE_REG(CPURTCIOBG_DATA, 0x8);
  WRITE_REG(CPURTCIOBG_CTRL, 1);
  POLL_REG(CPURTCIOBG_CTRL, 1, 0, 10000, 0xee130000);

  // reconnect all DDR ports in the NoC
  WRITE_REG(CLKC_PLUG_CLK_IDLEREQ_CLEAR,  0x7c);
  POLL_REG (CLKC_PLUG_CLK_IDLEACK_STATUS, 0x7c, 0x00, 100000, 0xee100000);
  POLL_REG (CLKC_PLUG_CLK_IDLE_STATUS,    0x7c, 0x00, 100000, 0xee110000);

  return (0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ddr_memvex
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Op code: Bits   - Purpose
//          [2..0] - Operation
//                   0x00000 - Write/Read and return the error count
//                   0x00001 - Infinate write loop
//                   0x00002 - Infinate read loop
//
//          [7..4] - Data background
//                   0x0000 - PI
//                   0x0010 - XTALK
//                   0x0020 - QP1
//                   0x0040 - QP2
//                   0x0080 - QP3
//                   0x0100 - 0xffff/0x0000
//
//          [16]   - Mix reads and writes (user data)
//          [17]   - Mix reads and writes (PRBS data)
//          [18]   - Read only functional test 
//
//          [30]   - Suppress printing of the status register and error count
//          [31]   - Dump debug registers
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define MV_WRITE      0x000001 // DDR_BIST_CONFIG - Bit 0 (0x0001)
                               // should the bist operation include write to dram (1) or not (0). CONFIG.WRITE and CONFIG.READ may not be both 0. (default: 0x0) 

#define MV_READ       0x000002 // DDR_BIST_CONFIG - Bit 1 (0x0002)
                               // should the bist operation include read from dram (1) or not (0). CONFIG.WRITE and CONFIG.READ may not be both 0. (default: 0x0)

#define MV_RW         0x000004 // DDR_BIST_CONFIG - Bit 2 (0x0004)
                               // should the read be done simultaneously with the write (1) or after the write is done (0). may be configured '1' only when both 
                               // write and read are performed (CONFIG.WRITE and CONFIG.READ are both 1). (default: 0x0) 

#define MV_WR_MASK    0x000010 // DDR_BIST_CONFIG - Bit 4 (0x0010)
                               // if set, the written data is masked with MASK* parameters. i.e. in each chunk of 128 bits, bits marked with '1' in MASK* parameters 
                               // are taken from the data to be written, and bits marked with '0' in MASK* parameters are taken from CONFIG.WR_MASK_DATA. (default: 0x0) 

#define MV_RD_MASK    0x000020 // DDR_BIST_CONFIG - Bit 5 (0x0020)
                               // if set, check only bits marked 1 in MASK* parameters. (default: 0x0)  

#define MV_WR_DM      0x000040 // DDR_BIST_CONFIG - Bit 6 (0x0040)
                               // if CONFIG.WR_MASK=1, all bits marked '0' in MASK* parameters are written with CONFIG.WR_MASK_DATA value. (default: 0x0) 

#define MV_BURST_LEN  0x000400 // DDR_BIST_CONFIG - Bits 10:8 (0x0700)
                               // axi burst length (awlen/arlen) will be 2^CONFIG.BURST_LEN data transfers (each of 64 bits). (default: 0x0) 0:4 : for 1,2,4,8,16 transfers of 64 bits each.

#define MV_DATA_USER  0x000000 // DDR_BIST_CONFIG - Bits 13:12 (0x3000)
#define MV_DATA_LFSR  0x001000 // 0 : User defineable, 1: LFSR (PRBS), 2:3 reserved 

#define MV_RW_MAX_GAP 0x000000 // DDR_BIST_CONFIG - Bits 23:16 (0xff0000)
                               // for interleaved wr-rd mode (CONFIG.RW=1), determines the maximum gap (minus 1) allowed between write and read in terms of axi requests. (default: 0x0)
                               // 0:0xfe : for max gap of 1..0xff axi requests.

#define MV_WR_RD_MIXED    (MV_WRITE | MV_READ | MV_RW | MV_BURST_LEN | MV_DATA_USER | MV_RW_MAX_GAP)
#define MV_WR_RD_PRBS     (MV_WRITE | MV_READ | MV_RW | MV_BURST_LEN | MV_DATA_LFSR | MV_RW_MAX_GAP)
#define MV_WRITE_AND_READ (MV_WRITE | MV_READ |         MV_BURST_LEN | MV_DATA_USER | MV_RW_MAX_GAP)
#define MV_WRITE_ONLY     (MV_WRITE |                   MV_BURST_LEN | MV_DATA_USER | MV_RW_MAX_GAP)
#define MV_READ_ONLY      (MV_READ  |                   MV_BURST_LEN | MV_DATA_USER | MV_RW_MAX_GAP)

int ddr_memvex (unsigned int addr, unsigned int size, unsigned int op, unsigned int timeout) {
   unsigned int r,ee;

   if (!(op & 0x40000000))
   {
      printf("\r\n\rddr_memvex addr: 0x%08x size: 0x%08x opcode: 0x%04x\n\r",addr,size,op);
   } // print
    
   WRITE_REG(MEMC_TREFI, (1<<31)); // stop refreshes, we want pure writes and reads (keep the loops under 64ms or we could have trouble!)

   if (op & 0x00010000) 
   { WRITE_REG(DDR_BIST_CONFIG, MV_WR_RD_MIXED); }
   else if (op & 0x00020000) 
   { WRITE_REG(DDR_BIST_CONFIG, MV_WR_RD_PRBS); }
   else if (op & 0x00040000)
   { WRITE_REG(DDR_BIST_CONFIG, MV_READ_ONLY); }
   else
   { WRITE_REG(DDR_BIST_CONFIG, MV_WRITE_AND_READ); }
   
   WRITE_REG(DDR_BIST_ADDRESS, addr);
   WRITE_REG(DDR_BIST_SIZE,    size);

   // User Config Data: write cyclicly the 128 bits in DATA* parameters. in terms of linear address this is {DATA1H,DATA1L,DATA0H,DATA0L}. in terms of DDR physical address 
   // each such 128 bits are done in burst of 8 accesses of 16 bits in this order:
   // Verified emperically with a scope for DQ0 on a 1x16 0.8mm board (0836)
   //   DQ    [15: 8], DQ    [ 7: 0]   Time goes down
   // { DATA0L[ 7: 0], DATA0H[ 7: 0] } Burst 0
   // { DATA0L[15: 8], DATA0H[15: 8] } Burst 1
   // { DATA0L[23:16], DATA0H[23:16] } Burst 2
   // { DATA0L[31:24], DATA0H[31:24] } Burst 3
   // { DATA1L[ 7: 0], DATA1H[ 7: 0] } Burst 4
   // { DATA1L[15: 8], DATA1H[15: 8] } Burst 5
   // { DATA1L[23:16], DATA1H[23:16] } Burst 6
   // { DATA1L[31:24], DATA1H[31:24] } Burst 7
   if      (op & 0x0010) { WRITE_REG(DDR_BIST_DATA0L,0x5aa55aa5); WRITE_REG(DDR_BIST_DATA0H,0x5aa55aa5); WRITE_REG(DDR_BIST_DATA1L,0xff00f00f); WRITE_REG(DDR_BIST_DATA1H,0xff00f00f); } // SI
   else if (op & 0x0020) { WRITE_REG(DDR_BIST_DATA0L,0x00ff00ff); WRITE_REG(DDR_BIST_DATA0H,0x01fe01fe); WRITE_REG(DDR_BIST_DATA1L,0x00ff00ff); WRITE_REG(DDR_BIST_DATA1H,0x01fe01fe); } // QP1
   else if (op & 0x0040) { WRITE_REG(DDR_BIST_DATA0L,0x00000000); WRITE_REG(DDR_BIST_DATA0H,0x01fe01fe); WRITE_REG(DDR_BIST_DATA1L,0x00000000); WRITE_REG(DDR_BIST_DATA1H,0x01fe01fe); } // QP2
   else if (op & 0x0080) { WRITE_REG(DDR_BIST_DATA0L,0x00000000); WRITE_REG(DDR_BIST_DATA0H,0x01010100); WRITE_REG(DDR_BIST_DATA1L,0x00000000); WRITE_REG(DDR_BIST_DATA1H,0x00000000); } // QP3
   else if (op & 0x0100) { WRITE_REG(DDR_BIST_DATA0L,0xff00ff00); WRITE_REG(DDR_BIST_DATA0H,0xff00ff00); WRITE_REG(DDR_BIST_DATA1L,0xff00ff00); WRITE_REG(DDR_BIST_DATA1H,0xff00ff00); } // QP4
   else                  { WRITE_REG(DDR_BIST_DATA0L,0xffff00ff); WRITE_REG(DDR_BIST_DATA0H,0xffff00ff); WRITE_REG(DDR_BIST_DATA1L,0x0000ff00); WRITE_REG(DDR_BIST_DATA1H,0x0000ff00); } // PI - 1011 0100

   // Run BIST
   WRITE_REG(DDR_BIST_CONTROL,1); 
   POLL_REG(DDR_BIST_CONTROL,0x00000001,0x00000000,timeout,0xffff);
   r=READ_REG(DDR_BIST_STATUS);     
   ee=READ_REG(DDR_BIST_ERROR_CNT); 

   // Print the status and error count
   if (!(op & 0x40000000))
   {
      printf("DDR_BIST_STATUS:      0x%08x\n\r",r);
      printf("DDR_BIST_ERROR_COUNT: %d\n\r",ee);
   } // limited output

   if (op & 0x80000000)
   {
      r=READ_REG(DDR_BIST_CONTROL);               printf("DDR_BIST_CONTROL:               0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_ERROR0L);               printf("DDR_BIST_ERROR0L:               0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_ERROR0H);               printf("DDR_BIST_ERROR0H:               0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_ERROR1L);               printf("DDR_BIST_ERROR1L:               0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_ERROR1H);               printf("DDR_BIST_ERROR1H:               0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_FIRST_ERROR_ADDR);      printf("DDR_BIST_FIRST_ERROR_ADDR:      0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_FIRST_ERROR_EXP_DATAL); printf("DDR_BIST_FIRST_ERROR_EXP_DATAL: 0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_FIRST_ERROR_EXP_DATAH); printf("DDR_BIST_FIRST_ERROR_EXP_DATAH: 0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_FIRST_ERROR_RD_DATAL);  printf("DDR_BIST_FIRST_ERROR_RD_DATAL:  0x%08x\n\r",r);
      r=READ_REG(DDR_BIST_FIRST_ERROR_RD_DATAH);  printf("DDR_BIST_FIRST_ERROR_RD_DATAH:  0x%08x\n\r",r);
   } // all status registers

   if (op & 0x01)
   {
      printf("Starting write only loop...\n\r");
      WRITE_REG(DDR_BIST_CONFIG, MV_WRITE_ONLY);
      WRITE_REG(DDR_BIST_CONTROL,0x3); 
      POLL_REG(DDR_BIST_CONTROL,0x00000001,0x00000000,timeout,0xffff);
   } // continuous write loop
   
   if (op & 0x02)
   {
      printf("Starting read only loop...\n\r");
      WRITE_REG(DDR_BIST_CONFIG, MV_READ_ONLY);
      WRITE_REG(DDR_BIST_CONTROL,0x3); 
      POLL_REG(DDR_BIST_CONTROL,0x00000001,0x00000000,timeout,0xffff);
   } // continuous read loop

   if (!(op & 0x40000000))
   {
      if (ee) { printf("FAIL\n\r"); }
      else    { printf("PASS\n\r"); }
   } // print

   return(ee);
} // ddr_memvex

