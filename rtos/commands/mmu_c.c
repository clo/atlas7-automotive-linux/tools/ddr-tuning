/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "FreeRTOSConfig.h"
//#if SUPPORT_POWER_FEATURES   
#if SUPPORT_DDR 
extern void DCache_CleanInvalidate(void);

void DisableMMU(void)
{

    unsigned int temp;

    __asm__ __volatile__ (
        "mrc   p15, 0, %[value], c1, c0, 0 "        //read CP15 register 1
        :[value] "=r" (temp)
    );

    //if mmu is enable, so we clean and invalid the data cache
    if(temp &0x01) {
        //after celan&invalidate, only read stack to registers, so the cache line is clean, we can disable MMU&DCache
        DCache_CleanInvalidate();
        __asm__ __volatile__(
            "DSB"
        );
    }

    temp &= ~0x05;         // disable the MMU & dCaches

    __asm__ __volatile__(
        "DSB\n\t"
        "ISB"
    );

    __asm__ __volatile__ (
        "mcr p15,0,%[value],c1,c0,0"
        ::[value] "r" (temp)
    );

    __asm__ __volatile__(
        "ISB"
    );

    temp= 0;
    __asm__ __volatile__ (
        "MCR p15, 0, %[value], c8, c7, 0\n\t"       //invalidate the TLB, so no valid entry in TLB before enable the MMU.(Note: temp value is ignored)
        "mcr p15, 0, %[value], c7, c5, 4\n\t"       // flush prefetch buffer (ISB)
        "mcr p15, 0, %[value], c7, c5, 6\n\t"       // flush BTAC, Invalidate entire branch predictor array.
        "mcr p15, 0, %[value], c7, c10, 4\n\t"      // DSB (guarantee completion of any cache and memory operation)
        "mcr p15, 0, %[value], c7, c5, 4\n\t"       // flush prefetch buffer (ISB), guarantee BTAC flush visible to next instruction
        "nop"
        ::[value] "r" (temp)
    );

}


#endif


