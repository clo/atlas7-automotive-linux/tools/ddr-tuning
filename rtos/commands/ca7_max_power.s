/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

   .macro MESSAGE ch
                PUSH	{r0-r3, lr}
                LDR     r0, =\ch
              	LDR		r2, fdumpCharConst
                BLX		r2
                POP		{r0-r3, lr}
   .endm
        
   
   .macro TT_SECTION_WRITE pa, va, mem_attr

                LDR     r0, =\pa
                LDR     r1, =\va
                LDR     r2, =\mem_attr
                PUSH    {r3}
                LDR     r3, =0xFFF00000
                AND     r0, r0, r3                  // Ensure word alignment when storing TT entry.
                AND     r1, r1, r3                  // Ensure word alignment when storing TT entry.
                POP     {r3}
                MRC     p15, 0, r3, c2, c0 ,0       // read ttb
                ORR     r0, r0, r2                  // add control bits to physical address
                ORR     r0, r0, #2
                STR     r0,[r3, r1, LSR #18]        // obtain MB offset from page (LSR), add to ttb and store page to this location
                DMB
   .endm

.set SECTION         ,   (0x00000002)
.set FAULT           ,   (0x00000000)

.set GLOBAL          ,   (0x00000000)
.set NONGLOBAL       ,   (0x00020000)

.set EXE             ,   (0x00000000)
.set NON_EXE         ,   (0x00000010)

.set RW              ,   (0x00000C00)   // Both user and priv, read/write
.set RO              ,   (0x00008800)   // Both user and priv, read only
.set PRIV_ONLY       ,   (0x00000400)   // Priv read/write, user no access
.set PRIV_USER_RO    ,   (0x00000800)   // Priv read/write, user read-only
.set NOACCESS        ,   (0x00000000)

.set WB              ,   (0x0000000C)   // Write back
.set WT              ,   (0x00000008)   // Write through
.set NO_CACHE        ,   (0x00001000)   // no cache or buffer
.set SDEVICE         ,   (0x00000004)   // shared device
.set NSDEVICE        ,   (0x00002000)   // non shared device
.set SORDERED        ,   (0x00000000)   // strongly ordered

.set SHARED          ,   (0x00010000)    // Used for NORMAL types only
.set NON_SHARED      ,   (0x00000000)    // Used for NORMAL types only



/*
       // Include macros
                GET     macros.hs
                GET     configuration
                GET     intc.inc        // For EVENTI register
                GET     noc_cpum.inc    // For "CPUM_SB_MAIN_SIDEBANDMANAGER_FLAGOUTSET0".

                AREA  Init, CODE, READONLY
*/                

    .extern data_cpuid


	.global max_power
	.global max_power_setup
    .global setup_cpu1
    
	.text
	.arm

.align 8

// - Standard definitions of mode bits and interrupt (I&F) flags in PSRs
.set Mode_USR,   0x10
.set Mode_FIQ,   0x11
.set Mode_IRQ,   0x12
.set Mode_SVC,   0x13
.set Mode_MON,   0x16
.set Mode_ABT,   0x17
.set Mode_UNDEF, 0x1B
.set Mode_SYS,   0x1F
.set I_Bit,      0x80 // when I bit is set, IRQ is disabled
.set F_Bit,      0x40 // when F bit is set, FIQ is disabled

.set num_iteration,   0x000FFFFF  // power loop iteration number

/*pointers for dram */
// Location of master copy of TLB
.set table_base0,   0x42000000
.set table_base1,   0x42004000
.set table_base2,   0x42008000
.set table_base3,   0x4200C000
.set SPD_ADDR,      0x04000000

.set TBVAL_CFGL1,   0x00000000
                ENTRY:
/*
Vectors
                B       Reset_Handler
                B       Undefined_Handler
                B       SWI_Handler
                B       Prefetch_Handler
                B       Abort_Handler
                NOP     //Reserved vector
                B       IRQ_Handler
                B       FIQ_Handler

//-------------------------------------------------------------------------------
// Handlers for unused exceptions
//-------------------------------------------------------------------------------

Undefined_Handler
                MESSAGE "Unexpected undefined instruction \n"
                TEST_FAIL
SWI_Handler
                MESSAGE "Unexpected SWI call \n"
                TEST_FAIL
Prefetch_Handler
                MESSAGE "Unexpected prefetch abort \n"
                TEST_FAIL
Abort_Handler
                MESSAGE "Unexpected data abort \n"
                TEST_FAIL
IRQ_Handler
                MESSAGE "Unexpected IRQ \n"
                TEST_FAIL
FIQ_Handler
                MESSAGE "Unexpected FIQ \n"
                TEST_FAIL

//-------------------------------------------------------------------------------
// Reset Handler
//-------------------------------------------------------------------------------

zero_mem        % 56
*/
Reset_Handler:
// ------------------------------------------------------------
// Clear registers
// ------------------------------------------------------------
                PUSH	{r0-r12, lr} 
                MESSAGE 'A'
/*                
                LDR     r0, =zero_mem
                LDM     r0, {r1-r14}

                CPS     #Mode_FIQ
                LDM     r0, {r8-r14}

                CPS     #Mode_IRQ
                LDM     r0, {r13-r14}

                CPS     #Mode_ABT
                LDM     r0, {r13-r14}

                CPS     #Mode_UNDEF
                LDM     r0, {r13-r14}

                CPS     #Mode_MON
                LDM     r0, {r13-r14}

                CPS     #Mode_SYS
                LDM     r0, {r13-r14}
*/
        // Initialise memory locations
                MOV     r1,#0
                MOV     r2,#0
                MOV     r3,#0
                MOV     r4,#0
                MOV     r5,#0
                MOV     r6,#0
                MOV     r7,#0
                MOV     r8,#0

       // Read Multiprocessor Affinity Register MPIDR to determine which CPU
       // is running the code.
                MESSAGE 'B'
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
       // Set up stack pointer(s)
                CMP     r0, #0
                BEQ     init_cpu0
                CMP     r0, #1
                BEQ     init_cpu1
                CMP     r0, #2
                BEQ     init_cpu2
                CMP     r0, #3
                BEQ     init_cpu3
       // This code should not be reached
                B       err_run

       // CPU Physical memory init
init_cpu0:
                MESSAGE '1'
                LDR     r0,=AuxArea
                STMIA   r0,{r1-r8}
                ADD     r0, r0, #0x20
                STMIA   r0,{r1-r8}
       // Initialise system configuration flags
                MOV     r0, #0
                LDR     r1, =data_cpuid
                LDR     r3, =data_cpunb
                MOV     r3, #2
                LDR     r4, =setup_flag
                LDR     r5, =sync_flag
                STR     r0, [r1]
                //STR     r0, [r2]
                STR     r0, [r3]
                STR     r0, [r4]
                STR     r0, [r5]
                //LDR     sp, =stack0
                PUSH    {r2, lr}
               	LDR		r2, CpuInitCore0
                BLX		r2
                //IMPORT  CpuInitCore0 // [rp]: not clear what this means
                //BL      CpuInitCore0
                POP     {r2, lr}

       // This is core 0, so we initialize memory controller:
        // [rp]: we do that before test starts
        /*
                PUSH    {lr}
                IMPORT  InitDram
                BL      InitDram
                POP     {lr}
*/
                B       start
init_cpu1:
                MESSAGE '2'
                LDR     r0,=AuxArea
                STMIA   r0,{r1-r8}
                ADD     r0, r0, #0x20
                STMIA   r0,{r1-r8}
//                LDR     sp, =stack1
                B       cpu_wait
init_cpu2:
                LDR     r0,=AuxArea
                STMIA   r0,{r1-r8}
                ADD     r0, r0, #0x20
                STMIA   r0,{r1-r8}
 //               LDR     sp, =stack2
                B       cpu_wait
init_cpu3:
                LDR     r0,=AuxArea
                STMIA   r0,{r1-r8}
                ADD     r0, r0, #0x20
                STMIA   r0,{r1-r8}
 //               LDR     sp, =stack3
                B       cpu_wait
start:
                //MESSAGE "ca7_max_power.s\n"
                MESSAGE 'C'            
       // Store CPU ID
       // Read Multiprocessor Affinity Register MPIDR to determine which CPU
       // is running the code.
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
                LDR     r1, =data_cpuid
                STR     r0, [r1]
                DSB

cfg_mp:
       // Cortex-A7 MPCore Configuration
                //CPU_NBCORE data_cpunb, err_x, err_cfg
                // [rp]: in Atlas7 number of core is 2. replacing data_cpunb with '2'
                

run_setup:
                MESSAGE 'D'
                //MESSAGE "Running Setup:\n"
                BL      setup

       // If not running CPU go to wait
                LDR     r4, =setup_flag
                LDR     r5, [r4]
                CMP     r5, r0
                BNE     cpu_wait

       // Update the setup_flag to enable the next Core to run the code.
       // If last CPU running the setup then branch to cpu_wait_end
                LDR     r7, =data_cpunb
                LDR     r8, [r7]
                SUB     r9, r8, #1
                CMP     r9, r0
                BEQ     cpu_sync

                ADD     r5, #1
                STR     r5, [r4]
                DSB
                MESSAGE 'E'
cpu_wait:
                MESSAGE 'w'
       // Read Multiprocessor Affinity Register MPIDR to determine which CPU
       // is running the code.
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
       // Idle loop
                LDR     r4, =setup_flag
                LDR     r5, [r4]
                CMP     r0, r5
                BLT     cpu_sync
                BEQ     cpu_wait_end
                BGT     cpu_wait

cpu_wait_end:
                MESSAGE 'F'
       // Store CPU ID
                LDR     r1, =data_cpuid
                STR     r0, [r1]
                DSB
                B       run_setup

cpu_sync:
                MESSAGE 's'
       // working out the sync_flag update based on CPU ID
                MOV     r6, #1
                LSL     r6, r0
       // working out the synchronising value to expect in the sync_flag
                LDR     r7, =data_cpunb
                LDR     r8, [r7]
                MOV     r9, #1
                LSL     r9, r8
                SUB     r9, #1
       // updating sync_flag
                LDR     r10, =sync_flag
                LDR     r11, [r10]
                ORR     r11, r6
                STR     r11, [r10]
       // check sync_flag value: branch to main if matching the expected value
                MESSAGE 'G'
cpu_sync_loop:
//                LDR     r11, [r10]
//                CMP     r11, r9
//                BEQ     main
//                B       cpu_sync_loop

//-------------------------------------------------------------------------------
// Test Setup
//-------------------------------------------------------------------------------
setup:
//                PUSH    {r0-r12,lr}
                MESSAGE 'a'
                
                // SMP Enable
                MRC     p15, 0, r0, c1, c0, 1
                ORR     r0, r0, #1<<6
                MCR     p15, 0, r0, c1, c0, 1
  
// ------------------------------------------------------------
// if FPU is present, enable it
// ------------------------------------------------------------
                //FPU_PRESENT r0
                MOV     r0,#0xf00000               // Write CPACR (Coprocessor Access Control Register)
                MCR     p15,0,r0,c1,c0,2           // to enable coprocessors cp10 & cp11
                MRC     p15,0,r0,c1,c0,2           // Read CPACR
                UBFX    r0, r0, #20, #1            // if bit 20 is zero then so the bits 23:21 so FPU present
                
                CMP     r0,#1
                BNE     skip_fpu_neon
                //ENABLE_FPU
                PUSH    {r0}
                MOV     r0,#1<<30
                MCR     p10, #7, r0, c8, c0, 0      // enable FPU by writing FPEXC
                MOV     r0,#0
                MCR     p10, #7, r0, c1, c0, 0      // init FPSR
                LDC     p11, c0, [r0], {32}         // init registers d0-d15
                POP     {r0}
                MESSAGE 'b'
// ------------------------------------------------------------
// if NEON is present, enable it
// ------------------------------------------------------------
                //NEON_PRESENT r0
                MOV     r0,#0xf00000               // Write CPACR (Coprocessor Access Control Register)
                MCR     p15,0,r0,c1,c0,2           // to enable coprocessors cp10 & cp11
                MRC     p15,0,r0,c1,c0,2           // Read CPACR;
                CMP     r0, #0x00f00000            // if cp10 & cp11 are enabled and bit 31 is zero then NEON present
                MOVEQ   r0, #1
                MOVNE   r0, #0

                MESSAGE 'c'
                CMP     r0, #1
                BNE     skip_fpu_neon
                MOV     r0, #1<<30
                MCR     p10, #7, r0, c8, c0, 0       // enable NEON by writing FPEXC
                MOV     r0, #0
                LDC     p11, c0, [r0], {32}          // init registers d0-d15
                LDCL    p11, c0, [r0], {32}          // init registers d16-d31
                MESSAGE 'd'

skip_fpu_neon:

// ------------------------------------------------------------
// Initialize cp15 registers
// ------------------------------------------------------------
                MOV     r0, #0x0
                MCR     p15,0,r0,c5,c0,0                       //DFSR
                MCR     p15,0,r0,c5,c0,1                       //IFSR
                MCR     p15,0,r0,c6,c0,0                       //DFAR
                MCR     p15,0,r0,c6,c0,2                       //IFAR
                MCR     p15,0,r0,c9,c13,0                      //PMCCNTR - PMU cycle Count Register
                MCR     p15,0,r0,c9,c13,2                      //PMXEVCTR - PMU Event Count Registers

// ------------------------------------------------------------
// Invalidate caches and TLB if required
// ------------------------------------------------------------

      // Check the status of L1RSTDISABLE and only
      // run the invalidate sequence if it has not been
      // carried at at reset in hardware.
                MESSAGE 'e'

      // get CPU ID
               LDR     r12, =data_cpuid
               LDR     r12, [r12]
               LDR     r6,=TBVAL_CFGL1
               LDR     r0, [r6]
               LSR     r0, r12
               TST     r0, #1<<16
               BEQ     skip_inval

               MESSAGE 'a' //"- Invalidate caches\n"
      // Invalidate the I Cache
               MOV     r0, #0
               MCR     p15, 0, r0, c7, c5, 0   // ICIALLU - Invalidate entire I Cache, and flushes branch target cache
      // Invalidate the D Cache
               //INVALIDATE_DCACHE
                MRC     p15, 1, r0, c0, c0, 0       // Read the Cache Size Identification register (CCSIDR)
                MOV     r0, r0, LSL #10
                MOV     r0, r0, LSR #23             // Mask off to leave the NumSets

                MOV     r2, #0x0                    // Set r2 to initial MVA (Way=0, Set=0)
                MOV     r1, #0                      // Use r1 as loop counter for WAYs
                MOV     r3, #0x0                    // Use r3 as a loop counter for SETs
                MESSAGE 'f'

invalidate_cache_loop:
                MCR     p15, 0, r2, c7, c6, 2       // DCISW - Invalidate data cache by set/way
                ADD     r2, r2, #0x0020             // Increment the SET field

                ADD     r3, r3, #1                  // Increment loop counter
                CMP     r3, r0                      // Compare loop counter with num_sets
                BLE     invalidate_cache_loop       // If (loop_counter =< num_sets) branch
                                                    // Prepare register for next pass
                ADD     r2, r2, #0x40000000         // Increment WAY field
                AND     r2, r2, #0xC0000000         // Clear the rest of the register (clear the SET field)
                MOV     r3, #0                      // Reset loop counter

                ADD     r1, r1, #1
                CMP     r1, #4
             
                BNE     invalidate_cache_loop

                MESSAGE 'g'   

      // Invalidate TLBs
               MCR     p15, 0, r0, c8, c7, 0                  // TLBIALL - Invalidate entire Unified TLB

skip_inval:
                MESSAGE 'h'  
// ------------------------------------------------------------
// Set up Domain Access Control Reg
// ------------------------------------------------------------
       // b00 - No Access (abort)
       // b01 - Client (respect table entry)
       // b10 - RESERVED
       // b11 - Manager (ignore access permissions)
       // Setting D0 to client, all others to No Access

                MOV     r0, #0x01
                MCR     p15, 0, r0, c3, c0, 0                  // DACR - Domain Access Control Register

// ------------------------------------------------------------
// Set Table Base Control Register
// ------------------------------------------------------------
                MOV     r0,#0x0
                MCR     p15, 0, r0, c2, c0, 2
                MESSAGE 'i'  
          
// ------------------------------------------------------------
// Page table modifications
// ------------------------------------------------------------
       // Stored in memory is a set of page tables that flat-maps memory
       // This image works by using the same VAs across the MP, but setting different PAs
       // If not core 0, copy to a new location....

                MESSAGE 'j'  
                MRC     p15, 0, r1, c0, c0, 5  // Read Multiprocessor Affinity Register
                AND     r1, r1, #0x03          // Mask off, leaving the CPU ID field
                CMP     r1, #0
                BEQ     cpu0_table
                CMP     r1, #1
                BEQ     cpu1_table
/*                CMP     r1, #2
                BEQ     cpu2_table
                CMP     r1, #3
                BEQ     cpu3_table
                */
       // This code should not be reached
                B       err_run


.set TBVAL_SYSADDR,         0x18020000
.set INT_EVENT_CONFIG,      0x102200C4
.set EVENTI_REGISTER         , INT_EVENT_CONFIG
.set TBVAL_SYSTLBADDR        , TBVAL_SYSADDR                       >> (18 + 2) << (18 + 2)
.set EVENTI_REG_TLBADDR      , 0x18620000                          >> (18 + 2) << (18 + 2)
.set TESTADDR1_TLBADDR_V     , 0xBFFFFFFA                          >> (18 + 2) << (18 + 2)
.set TESTADDR1_TLBADDR_P     , 0x4FFFFFFA                          >> (18 + 2) << (18 + 2)
.set TESTADDR2_TLBADDR_V     , 0x80000002                          >> (18 + 2) << (18 + 2)
.set TESTADDR2_TLBADDR_P     , 0x40000002                          >> (18 + 2) << (18 + 2)

cpu0_table:
// ------------------------------------------------------------
// Set location of level 1 page table
// ------------------------------------------------------------
                MESSAGE '0'  
                LDR     r0, =table_base0       // Location of master copy of TLB
                MCR     p15, 0, r0, c2, c0 ,0
// ------------------------------------------------------------
//  Create sections for translation table
// ------------------------------------------------------------
                TT_SECTION_WRITE  0x0, 0x0, WB | RO | SHARED | EXE
                TT_SECTION_WRITE  SPD_ADDR, SPD_ADDR, SHARED | RW | EXE
                TT_SECTION_WRITE  EVENTI_REG_TLBADDR, EVENTI_REG_TLBADDR, SDEVICE | RW | NON_EXE          // Register bus (only for EVENTI register).
                TT_SECTION_WRITE  TESTADDR1_TLBADDR_P, TESTADDR1_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (low addresses).
                TT_SECTION_WRITE  TESTADDR2_TLBADDR_P, TESTADDR2_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (high addresses).
                TT_SECTION_WRITE  TBVAL_SYSTLBADDR, TBVAL_SYSTLBADDR, SDEVICE | RW | NON_EXE
                B       enable_mmu

cpu1_table:
// ------------------------------------------------------------
// Set location of level 1 page table
// ------------------------------------------------------------
                MESSAGE '1'  
                LDR     r0, =table_base1       // Location of master copy of TLB
                MCR     p15, 0, r0, c2, c0 ,0
// ------------------------------------------------------------
//  Create sections for translation table
// ------------------------------------------------------------
                TT_SECTION_WRITE  0x0, 0x0, WB | RO | SHARED | EXE
                TT_SECTION_WRITE  SPD_ADDR, SPD_ADDR, WB | SHARED | RW | EXE
                TT_SECTION_WRITE  EVENTI_REG_TLBADDR, EVENTI_REG_TLBADDR, SDEVICE | RW | NON_EXE          // Register bus (only for EVENTI register).
                TT_SECTION_WRITE  TESTADDR1_TLBADDR_P, TESTADDR1_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (low addresses).
                TT_SECTION_WRITE  TESTADDR2_TLBADDR_P, TESTADDR2_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (high addresses).
                TT_SECTION_WRITE  TBVAL_SYSTLBADDR, TBVAL_SYSTLBADDR, SDEVICE | RW | NON_EXE
                B       enable_mmu
/*
cpu2_table:
// ------------------------------------------------------------
// Set location of level 1 page table
// ------------------------------------------------------------
                MESSAGE '2'  
                LDR     r0, =table_base2       // Location of master copy of TLB
                MCR     p15, 0, r0, c2, c0 ,0
// ------------------------------------------------------------
//  Create sections for translation table
// ------------------------------------------------------------
                TT_SECTION_WRITE  0x0, 0x0, WB | RO | SHARED
                TT_SECTION_WRITE  SPD_ADDR, SPD_ADDR, WB | SHARED | RW | EXE
                TT_SECTION_WRITE  EVENTI_REG_TLBADDR, EVENTI_REG_TLBADDR, SDEVICE | RW | NON_EXE          // Register bus (only for EVENTI register).
                TT_SECTION_WRITE  TESTADDR1_TLBADDR_P, TESTADDR1_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (low addresses).
                TT_SECTION_WRITE  TESTADDR2_TLBADDR_P, TESTADDR2_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (high addresses).
                TT_SECTION_WRITE  TBVAL_SYSTLBADDR, TBVAL_SYSTLBADDR, SDEVICE | RW | NON_EXE
                B       enable_mmu

cpu3_table:
// ------------------------------------------------------------
// Set location of level 1 page table
// ------------------------------------------------------------
                MESSAGE '3'  
                LDR     r0, =table_base3       // Location of master copy of TLB
                MCR     p15, 0, r0, c2, c0 ,0
// ------------------------------------------------------------
//  Create sections for translation table
// ------------------------------------------------------------
                TT_SECTION_WRITE  0x0, 0x0, WB | RO | SHARED
                TT_SECTION_WRITE  0x100000, 0x100000, SHARED | WB | RW | EXE
                TT_SECTION_WRITE  SPD_ADDR, SPD_ADDR, SHARED | RW | EXE
                TT_SECTION_WRITE  EVENTI_REG_TLBADDR, EVENTI_REG_TLBADDR, SDEVICE | RW | NON_EXE          // Register bus (only for EVENTI register).
                TT_SECTION_WRITE  TESTADDR1_TLBADDR_P, TESTADDR1_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (low addresses).
                TT_SECTION_WRITE  TESTADDR2_TLBADDR_P, TESTADDR2_TLBADDR_V, SHARED | WB | RW | EXE     // DRAM (high addresses).
                TT_SECTION_WRITE  TBVAL_SYSTLBADDR, TBVAL_SYSTLBADDR, SDEVICE | RW | NON_EXE
                TT_SECTION_WRITE  0xfff00000, 0xfff00000, WB | RW | SHARED
*/
enable_mmu:
// ------------------------------------------------------------
// Enable the MMU
// ------------------------------------------------------------
                MESSAGE 'm'  
       // Set SCTLR bit 0 to enable MMU and to activate virtual address
       // system
                //MESSAGE "- Enable MMU\n"
                //ENABLE_MMU
                DSB
                MRC     p15, 0, r0, c1, c0, 0      // Read SCTLR - System Control Register
                ORR     r0, r0, #0x01              // Set M bit (bit 0)
                MCR     p15, 0, r0, c1, c0, 0      // Write SCTLR
                ISB

// ------------------------------------------------------------
// Branch Prediction Init
// ------------------------------------------------------------
                //MESSAGE "- Enable Caches\n"
                //ENABLE_CACHES
                MRC     p15, 0, r0, c1, c0, 0       // Read System Control Register configuration data
                AND     r0, r0,#~0x0004
                AND     r0, r0,#~0x1000
                ORR     r0, r0, #0x0004             // Set C bit
                ORR     r0, r0, #0x1000             // Set I bit
                MCR     p15, 0, r0, c1, c0, 0       // Write System Control Register configuration data

                MESSAGE '-' 
                POP     {r0-r12,pc}

//-------------------------------------------------------------------------------
// Main body of code
//-------------------------------------------------------------------------------
.align 8
main:
                PUSH	{r0-r12, lr}
                
                //-----------------------
                // ENABLE INVASIVE DEBUG
                //-----------------------
                MRC     p14, 0, r0, c0, c2, 2   // Read DBGDSCR into r0
                ORR     r0, r0, #0x4000         // Set HDBGen bit (bit 14)
                MCR     p14, 0, r0, c0, c2, 2   // Write DBGDSCR

                //-----------------------
                // ENABLE WATCHPOINTS &
                // BREAKPOINTS
                //-----------------------
                MOVW    r0, #0x1110
                MCR     p14, 0, r0, c0, c0, 4   // Write DBGDBVR0

                MOVW    r0, #0x2220
                MCR     p14, 0, r0, c0, c1, 4   // Write DBGDBVR1

                MOVW    r0, #0x3330
                MCR     p14, 0, r0, c0, c2, 4   // Write DBGDBVR2

                MOVW    r0, #0x444
                MCR     p14, 0, r0, c0, c0, 6   // Write DBGDWVR0

                MOVW    r0, #0x5550
                MCR     p14, 0, r0, c0, c1, 6   // Write DBGDWVR1

                MOV     r0, #0x00000007
                MCR     p14, 0, r0, c0, c0, 5   // Write DBGDBCR0
                MCR     p14, 0, r0, c0, c1, 5   // Write DBGDBCR1
                MCR     p14, 0, r0, c0, c2, 5   // Write DBGDBCR2
                MCR     p14, 0, r0, c0, c0, 7   // Write DBGDWCR0
                MCR     p14, 0, r0, c0, c1, 7   // Write DBGDWCR1
                
                // SMP Enable
                MRC     p15, 0, r0, c1, c0, 1
                ORR     r0, r0, #1<<6
                MCR     p15, 0, r0, c1, c0, 1
                

                //------------------------------------------------------------------
                // SET UP MEMORY MAPPED COMPONENT FOR MP SYNCHRONISING TRIGGER
                //------------------------------------------------------------------
                // In MPCore config, only CPU0 needs to access the memory mapped
                // validation component.
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
                CMP     r0, #0
                BNE     skip_event

                // Disable all previously programmed interrupt/event generation
                // (inc. EVENTI, register VAL_INTSEL)
                LDR     r7, =TBVAL_INTSEL
                MOV     r0, #0
                STR     r0, [r7]
                DSB

                // Trigger condition: "All CPU's WFE must be set in order to set EVENTI"
                // NB: depends on the number of CPUs implemented
                LDR     r7, =TBVAL_INTTRIG
                LDR     r1, =data_cpunb
                LDR     r0, [r1]
                MOV     r1, #1
                LSL     r1, r0
                SUB     r0, r1, #1
                LSL     r0, #4
                STR     r0, [r7]
                DSB

                // EVENTI generation enabled (held until programmed trigger condition is true)
                LDR     r7, =TBVAL_INTSEL
                MOV     r0, #1<<8
                STR     r0, [r7]
                DSB

                // Schedule trigger now (delay == 0 cycle)
                LDR     r7, =TBVAL_INTSCHED
                MOV     r0, #0
                STR     r0, [r7]
                DSB

                //MESSAGE "\nRunning Power Loops\n\n"

                B       skip_event// branch over alignement padding

//-------------------------------------------------------------------------------
// Main code loop to measure max power
//-------------------------------------------------------------------------------
.align 8  //32      // align to $ line
skip_event:
                // Last core will program EVENTI register to send event, and re-program that register later to stop sending event:

                // Check if it's the last core:
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
                LDR     r1, =data_cpunb
                LDR     r1, [r1]
                SUB     r1, r1, #1
                CMP     r1, r0
                BNE     AftSendEvent

                // Program EVENTI register to send event soon (pulse period, pulse width, pulse enable):
                LDR     r12, =((2000 << 8) | (1 << 4) | (1 << 0))
                LDR     r11, =EVENTI_REGISTER
                STR     r12, [r11]
                DSB
                MOV     r12, #0 // Prepare to re-program EVENTI register to stop sending events.

AftSendEvent:

                // set the value of the data
                LDR     r1, =0x80000002
                MOV     r0, #0xaa
                STR     r0, [r1]
                
                
                // File "cpu_ca7_max_power_power_loop.xlsx" can help to choose these values in case DRAM will change its address interval.
                // But if we change these values, we should also change definitions for "TESTADDR{1,2}_TLBADDR_{V,P}" earlier in this file.
                LDR     r0,=0x40000000
                LDR     r1,=0x3ffffffd
                LDR     r2,=0x3ffffffc
                LDR     r3,=0x40000006
                LDR     r4,=0x40000000
                LDR     r5,=0x3ffffffd
                LDR     r6,=0x3ffffffc
                LDR     r7,=0x40000006

                LDR     r10,=num_iteration// num of iterations round power_loop
/*
                // Wait For Event
                // Use two WFE instructions to make sure the event register is
                // clear so that STANDBYWFE will be raise and then used as a
                // trigger to generate EVENTI
                WFE
                STREQ   r12, [r11] // Reset EVENTI register to prevent further EVENTI pulses (only for the last core).
                DSB
                WFE
*/

power_loop:
                SUBS     r10, r10, #1

                // Group-1
                LDR     r8, [r0,+r1,LSL #1]! // Dual Issue Pair 0, Instruction 0
                MOV     r6, r2               // Dual Issue Pair 0, Instruction 1

                LDR     r8, [r2,+r3]!        // Dual Issue Pair 1, Instruction 0
                ADD     r0, r1, #0x003       // Dual Issue Pair 1, Instruction 1

                LDR     r8, [r4,+r5,LSL #1]! // Dual Issue Pair 2, Instruction 0
                SUB     r2, r3, #0x00a       // Dual Issue Pair 2, Instruction 1

                LDR     r8, [r6,+r7]!        // Dual Issue Pair 3, Instruction 0
                ADD     r4, r5, #0x003       // Dual Issue Pair 3, Instruction 1

                // Group-2
                LDR     r8, [r0,+r1,LSL #1]! // Dual Issue Pair 0, Instruction 0
                MOV     r6, r2               // Dual Issue Pair 0, Instruction 1

                LDR     r8, [r2,+r3]!        // Dual Issue Pair 1, Instruction 0
                ADD     r0, r1, #0x003       // Dual Issue Pair 1, Instruction 1

                LDR     r8, [r4,+r5,LSL #1]! // Dual Issue Pair 2, Instruction 0
                SUB     r2, r3, #0x00a       // Dual Issue Pair 2, Instruction 1

                LDR     r8, [r6,+r7]!        // Dual Issue Pair 3, Instruction 0
                ADD     r4, r5, #0x003       // Dual Issue Pair 3, Instruction 1

                // Group-3
                LDR     r8, [r0,+r1,LSL #1]! // Dual Issue Pair 0, Instruction 0
                MOV     r6, r2               // Dual Issue Pair 0, Instruction 1

                LDR     r8, [r2,+r3]!        // Dual Issue Pair 1, Instruction 0
                ADD     r0, r1, #0x003       // Dual Issue Pair 1, Instruction 1

                LDR     r8, [r4,+r5,LSL #1]! // Dual Issue Pair 2, Instruction 0
                SUB     r2, r3, #0x00a       // Dual Issue Pair 2, Instruction 1

                LDR     r9, [r6,+r7]!        // Dual Issue Pair 3, Instruction 0
                ADD     r4, r5, #0x003       // Dual Issue Pair 3, Instruction 1

                // Group-4
                LDR     r8, [r0,+r1,LSL #1]! // Dual Issue Pair 0, Instruction 0
                MOV     r6, r2               // Dual Issue Pair 0, Instruction 1

                LDR     r9, [r2,+r3]!        // Dual Issue Pair 1, Instruction 0
                ADD     r0, r1, #0x003       // Dual Issue Pair 1, Instruction 1

                LDR     r8, [r4,+r5,LSL #1]! // Dual Issue Pair 2, Instruction 0
                SUB     r2, r3, #0x00a       // Dual Issue Pair 2, Instruction 1

                LDR     r8, [r6,+r7]!        // Dual Issue Pair 3, Instruction 0
                ADD     r4, r5, #0x003       // Dual Issue Pair 3, Instruction 1

                BNE     power_loop
                
                CMP     r9, #0xaa
                BNE     loop_err              
                B       loop_end
loop_err:
                MESSAGE 'x'
loop_end:                        
                // end of main loop return to caller.
                POP		{r0-r12, pc} 
                
/*
exit:
       // Read Multiprocessor Affinity Register MPIDR to determine which CPU
       // is running the code.
                MRC     p15, 0, r0, c0, c0, 5
                UBFX    r0, r0, #0, #2
                CMP     r0, #0
                BEQ     pass

                WFI

//-------------------------------------------------------------------------------
// End of test
//-------------------------------------------------------------------------------

pass:
                MESSAGE 'p'
                MESSAGE 'a'
                MESSAGE 's'
                MESSAGE 's'
                b f_return
fail:
                MESSAGE 'f'
                MESSAGE 'a'
                MESSAGE 'i'
                MESSAGE 'l'

f_return:
                MESSAGE '\r'
                MESSAGE '\n'

                POP		{r0-r12, pc}                

//-------------------------------------------------------------------------------
// Error Messages
//-------------------------------------------------------------------------------

err_x:
                //MESSAGE "UNKNOWN\n"
                MESSAGE 'U'               
                
err_run:
                //MESSAGE "\nThis code should not be reached\n"
                MESSAGE 'R'               
                B       fail
err_cfg:
                //MESSAGE "\nConfiguration mismatch between test and RTL\n"
                MESSAGE 'C'               
                B       fail

*/


//-------------------------------------------------------------------------------
// Pagetable Data
//-------------------------------------------------------------------------------
/*
                AREA    TT0,DATA,NOINIT, ALIGN=14
table_base0
                % 16384

                AREA    TT1,DATA,NOINIT, ALIGN=14
table_base1
                % 16384

                AREA    TT2,DATA,NOINIT, ALIGN=14
table_base2
                % 16384

                AREA    TT3,DATA,NOINIT, ALIGN=14
table_base3
                % 16384

//-------------------------------------------------------------------------------
// Stack Data
//-------------------------------------------------------------------------------

                AREA    Stack, DATA, READWRITE, ALIGN=12, NOINIT

                % 256
stack0
                % 256
stack1
                % 256
stack2
                % 256
stack3
                % 256

//-------------------------------------------------------------------------------
// Test Data
//-------------------------------------------------------------------------------

                AREA    Misc, DATA, READWRITE, ALIGN=12

data_cpuid      DCD     0
data_cpunb      DCD     0
setup_flag      DCD     0
sync_flag       DCD     0
AuxArea         % 8 * 4 * 2 // 8 registers, 4 bytes each, 2 areas.

//-------------------------------------------------------------------------------
// BST instructions
//-------------------------------------------------------------------------------

                AREA    |~BST CODE|, CODE, ALIGN=12

                BST_START
                BST "EXIT"
                BST "PRINTF ERROR: Should not be reached"
                BST_END

                END
*/


.type max_power_setup, %function
max_power_setup:
    /* disable interrupts!!!!! (no more OS activity) */
    CPSID if
    
	B Reset_Handler
	B err_run



.type max_power, %function
max_power:
    MESSAGE '0'
	B main

 //   .set Mode_SYS,   0x1F


.type setup_cpu1, %function
setup_cpu1:
    CPS     #Mode_SYS      
    LDR     sp, =0x0401A000    
    MESSAGE '~'
    BL Reset_Handler
    MESSAGE '!'
cpu1_loop:
    MESSAGE '1'    
    BL main   
    b cpu1_loop


err_run:
                //MESSAGE "\nThis code should not be reached\n"
                MESSAGE 'R'   
f_return:
                MESSAGE '\n'

                POP		{r0-r12, pc}                



fdumpCharConst: .word dumpChar
CpuInitCore0: .word __CpuInitCore0

data_cpunb: .word uldata_cpunb
data_cpuid: .word uldata_cpuid      
setup_flag: .word ulsetup_flag      
sync_flag:  .word ulsync_flag       
AuxArea:	.word ulAuxArea8x4x8
zero_mem:   .word ulzero_mem56

TBVAL_INTSEL:    .word ulTBVAL_INTSEL32
TBVAL_INTTRIG:   .word ulTBVAL_INTTRIG 
TBVAL_INTSCHED:  .word ulTBVAL_INTSCHED

.align 8

.end
                
