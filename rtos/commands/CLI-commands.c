/*
    FreeRTOS V8.0.1 - Copyright (C) 2014 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that has become a de facto standard.             *
     *                                                                       *
     *    Help yourself get started quickly and support the FreeRTOS         *
     *    project by purchasing a FreeRTOS tutorial book, reference          *
     *    manual, or both from: http://www.FreeRTOS.org/Documentation        *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available from the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?"                                     *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
	QTIL elects to receive the code under the GPLv2 only.
*/

/* FreeRTOS includes. */
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "otp.h"
#include "ringosc.h"

// chip includes
#include "chip_utils.h"
#include "ddrphy.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
#include "strutils.h"
#include "cache.h"

// Remove compile time warnings about unused parameters, //
#define USE_ARGS() (void) pcCommandString; (void) xWriteBufferLen;
#define DDR_INTERNAL_USE 0 

static portBASE_TYPE prvReadRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static portBASE_TYPE prvWriteRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static portBASE_TYPE prvSleepCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static portBASE_TYPE prvDdrSetParam(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static portBASE_TYPE prvDdrSetInit(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static portBASE_TYPE prvDdrWinReport(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

#if DDR_INTERNAL_USE
   static portBASE_TYPE prvBlinkLedCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvReadModifyWriteRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvPollingCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
#endif

#if SUPPORT_HISTORY
   //static portBASE_TYPE prvTaskheapCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvPrintHistory(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
#endif

#if SUPPORT_DDR
   /* G2D / DMA Includes */
   #include "soc_fc_g2d.h"
   #include "soc_g2d.h"
   #include "clkc.h"

   static portBASE_TYPE prvRtcIoBgRead(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvRtcIoBgWrite(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   //static portBASE_TYPE prvDdrTest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrDump(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrDMATest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrAccess(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrRangeTest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrMemvex(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   static portBASE_TYPE prvDdrSelfRefreshEnter (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   //static portBASE_TYPE prvKasSpiInit(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

   #if DDR_INTERNAL_USE
      //static portBASE_TYPE RingOscTestCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
      static portBASE_TYPE prvDdrBistLoopback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
      static portBASE_TYPE prvDdrBist(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   #endif

   #if (COMPACT_IMAGE == 0)
      static portBASE_TYPE prvCountData(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
      static portBASE_TYPE prvNoCSocketConnect(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
      static portBASE_TYPE prvGetData(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
      static portBASE_TYPE prvDisableMMU(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
   #endif
#endif

unsigned int GetParameterAsUlong(const char *pcCommandString,int paramNum)
{
   const char *pcParameter;
   portBASE_TYPE lParameterStringLength;
   unsigned int ldata;
   /* Obtain the parameter string. */
   pcParameter = FreeRTOS_CLIGetParameter (pcCommandString, paramNum, &lParameterStringLength);   /* Store the parameter string length. */
   /* Sanity check something was returned. */
   configASSERT(pcParameter);
   //printf("got parameter %s\r\n",pcParameter);
   ldata = strtounum(pcParameter, 0, 0xFFFFFFFF,NULL);
   return ldata;
}

long GetParameterAsLong(const char *pcCommandString,int paramNum)
{
   const char *pcParameter;
   portBASE_TYPE lParameterStringLength;
   long ldata;

   /* Obtain the parameter string. */
   pcParameter = FreeRTOS_CLIGetParameter (pcCommandString, paramNum, &lParameterStringLength);   /* Store the parameter string length. */
   /* Sanity check something was returned. */
   configASSERT(pcParameter);
   //printf("got parameter %s\r\n",pcParameter);
   ldata = strtonum(pcParameter, -0xFFFF, 0xFFFF, NULL);

   return ldata;
}

static const CLI_Command_Definition_t xr =
{
   "r",
   "r <...>\t\t\t\t\t | Read Register\r\n",
   prvReadRegisterCommand, /* The function to run. */
   1 /* one argument, the address  */
};

static const CLI_Command_Definition_t xw =
{
   "w",
   "w <...>\t\t\t\t\t | Write Register\r\n",
   prvWriteRegisterCommand, /* The function to run. */
   2 /* 2 arguments, address value. */
};

#if DDR_INTERNAL_USE
   static const CLI_Command_Definition_t xrmw =
   {
      "rmw",
      "rmw <addr> <and mask> <or mask>\t\t | Read modify Write Register\r\n",
      prvReadModifyWriteRegisterCommand, /* The function to run. */
      3 /* 3 arguments, address value. */
   };
#endif

#if DDR_INTERNAL_USE
   static const CLI_Command_Definition_t xp =
   {
      "p",
      "p <addr> <mask> <value> <timeout>\t | Register polling Command.\r\n",
      prvPollingCommand, /* The function to run. */
      4 /* 4 arguments, address mask value, timeout */
   };
#endif

#if DDR_INTERNAL_USE
   static const CLI_Command_Definition_t xBlinkLed =
   {
      "BlinkLed",
      "\rBlinkLed <interval (ms)>\t\t | Test the led on the EVB. 0 to stop blinking.\r\n",
      prvBlinkLedCommand, /* The function to run. */
      1 /* 1 argument, time in ms. */
   };
#endif

static const CLI_Command_Definition_t xsleep =
{
   "sleep",
   "sleep <mode=0><dram_retention =0>\t | mode: 0 - psaving_mode_m3\n\r\t\t\t\t\t |\t 1 - psaving_mode .\r\n",
   prvSleepCommand, /* The function to run. */
   -1 /* one or two arguments. */
};

static portBASE_TYPE prvReadRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
   unsigned int laddress;
   unsigned int ldata;

   USE_ARGS();
   //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
   // write buffer length is adequate, so does not check for buffer overflows.
   configASSERT(pcWriteBuffer);
   laddress = GetParameterAsUlong(pcCommandString,1);
   ldata = READ_REG(laddress);
   printf("\r\n0x%x\r\n",ldata);
   return pdFALSE;
}

static portBASE_TYPE prvWriteRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
   unsigned int laddress;
   unsigned int ldata;

   USE_ARGS();
   //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
   // write buffer length is adequate, so does not check for buffer overflows.
   configASSERT(pcWriteBuffer);
   laddress = GetParameterAsUlong(pcCommandString,1);
   ldata = GetParameterAsUlong(pcCommandString,2);
   WRITE_REG(laddress,ldata);
   return pdFALSE;
}

#if DDR_INTERNAL_USE
   static portBASE_TYPE prvReadModifyWriteRegisterCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int laddress;
      unsigned int ldata, andValue, orValue;
   
      USE_ARGS();
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      configASSERT(pcWriteBuffer);
      laddress = GetParameterAsUlong(pcCommandString,1);
      andValue = GetParameterAsUlong(pcCommandString,2);
      orValue = GetParameterAsUlong(pcCommandString,3);
      ldata = READ_REG(laddress);
      ldata = ldata & andValue;
      ldata = ldata | orValue;
      WRITE_REG(laddress,ldata);
      return pdFALSE;
   }

   static portBASE_TYPE prvPollingCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int laddress, lval, lmask, llimit;
   
      USE_ARGS();
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      configASSERT(pcWriteBuffer);
      laddress = GetParameterAsUlong(pcCommandString,1);
      lmask = GetParameterAsUlong(pcCommandString,2);
      lval = GetParameterAsUlong(pcCommandString,3);
      llimit = GetParameterAsUlong(pcCommandString,4);
      Reg_Polling(laddress,lmask,lval,llimit); 
      return pdFALSE;
   }
#endif

static portBASE_TYPE prvSleepCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
   unsigned int lmode=0, lhibernation=0; //, wa=2;
   unsigned long ltemp;
   portBASE_TYPE lParameterStringLength;

   USE_ARGS();
   configASSERT(pcWriteBuffer); 
   
   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) { lmode        = GetParameterAsUlong(pcCommandString,1); }
   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)) { lhibernation = GetParameterAsUlong(pcCommandString,2); }
   //if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,3 ,&lParameterStringLength)) { wa           = GetParameterAsUlong(pcCommandString,3); }

   if (lhibernation == 1)
   {
      // sw_m3_enter_dram_refrash
      WRITE_REG(0x186202dc,0x7c);
      ltemp = READ_REG(0x186202f8);
      // move upctl to low-power state
      printf("\r\n 0x%x\r\n",ltemp);
      wait_us(1000);
      printf("\r\n 0x%x\r\n",ltemp);

      WRITE_REG(0x10800004,3);
      wait_us(1000);
      ltemp = READ_REG(0x10800008);
      wait_us(1000);
      printf("\r\n 0x%x\r\n",ltemp);
      wait_us(1000);
   } // DRAM hibernation

   if (lmode == 1)
   {
      printf("\r\n Sleep mode = 1\r\n");
      #ifdef DDR_ENABLE_LDO_WA
         // Disable the DDR Phy. LDO
         printf("\r\n DDR Phy. powerdown now...\r\n");
         printf("\r\n 0x%x \r\n", rtcread(0x3030)); 
         rtcwrite(0x3030,0x0000f076); 
         wait_us(500);
         printf("\r\n 0x%x \r\n", rtcread(0x3030)); 
      #else
         printf("\r\n DDR Phy. LDO Workaround is disabled!\r\n");
      #endif         
      wait_us(200000); // Needed if you want to see the printf output

      // Put the M3 to light sleep mode
      rtcwrite(0x3050,0x40000000); 
      wait_us(1000);
   } // keep Vddr alive and DRAM in self refresh

   if (lhibernation == 1) { rtcwrite(0x3000,0x3); } // Keep Vddr alive, turn off other supplies
   else                   { rtcwrite(0x3000,0x1); } // Turn off supplies including Vddr

   // it doesnt matter what we do now - since it is dead!

   return pdFALSE;
}

#if DDR_INTERNAL_USE
   static portBASE_TYPE prvBlinkLedCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int ltimeInMs;
      USE_ARGS();
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      configASSERT(pcWriteBuffer);
      ltimeInMs = GetParameterAsUlong(pcCommandString,1);
      set_funcsel__gp__gpio_3(0);
       
      BlinkLed(ltimeInMs);
      return pdFALSE;
   }
#endif

static const CLI_Command_Definition_t xDdrSetParam =
{
   "DdrSetParam",
   "DdrSetParam <index> <value>\t\t | Set DDR sub-system configuration parameters\r\n"
      "\t\t\t\t\t |\t 0 - Frequency (600 - 800+)\r\n"
      "\t\t\t\t\t |\t 1 - xtal Frequency (26)\r\n"
      "\t\t\t\t\t |\t 2 - APB Frequency (200)\r\n"
      "\t\t\t\t\t |\t 3 - Clock Delay (0 to 63)\r\n"
      "\t\t\t\t\t |\t 4 - Clock PU Offset (-12 to 2)\r\n"
      "\t\t\t\t\t |\t 5 - Clock PD Offset (-12 to 2)\r\n"
      "\t\t\t\t\t |\t 6 - Addr/Cmd PU Offset (-12 to 2)\r\n"
      "\t\t\t\t\t |\t 7 - Addr/Cmd PD Offset (-12 to 2)\r\n"
      "\t\t\t\t\t |\t 8 - Atlas 7 Addr/Cmd and Data Rout setting (1 to 15)\r\n"
      "\t\t\t\t\t |\t 9 - Atlas 7 Data ODT setting (1 to 15)\r\n"
      "\t\t\t\t\t |\t10 - DRAM Data Rout (34 or 40) ohms\r\n"
      "\t\t\t\t\t |\t11 - DRAM Data ODT (40, 60, or 120) ohms\r\n"
      "\t\t\t\t\t |\t12 - tREFI(ns) Default is 7800\r\n"
      "\t\t\t\t\t |\t13 - tRFC(ps) Default is 350000\r\n",
   prvDdrSetParam, /* The function to run. */
   -1 /* 2 arguments, . */
};

static portBASE_TYPE prvDdrSetParam(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
   int valueSet = 0;
   long param   = -1;
   long value   = 0;

   portBASE_TYPE lParameterStringLength;
   
   USE_ARGS();
   configASSERT(pcWriteBuffer);

   if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,1,&lParameterStringLength))  { param = GetParameterAsLong(pcCommandString,1); }
   if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,2,&lParameterStringLength))  { value = GetParameterAsLong(pcCommandString,2); valueSet=1; }

   if ((param >= 0) && (valueSet == 1))
   { setDdrParameter(param,value); }
   else
   { printf("\r\nDdrSetParam requires 2 arguments: Parameter Index and a value\r\n"); }

   return pdFALSE;
} // prvDdrSetParam

static const CLI_Command_Definition_t xDdrSetInit =
{
   "DdrSetInit",
   "DdrSetInit <freq>\t\t\t | Set DDR frequency and init the DDR (dependant on DdrSetParam values)\r\n",
   prvDdrSetInit, /* The function to run. */
   -1 /* 1 argument */
};

static portBASE_TYPE prvDdrSetInit(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
   unsigned int lreturnVal;
   unsigned long freq = 800;   

   portBASE_TYPE lParameterStringLength;
   
   USE_ARGS();
   configASSERT(pcWriteBuffer);

   if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,1,&lParameterStringLength))  { freq        = GetParameterAsUlong(pcCommandString,1); }

   setDdrParameter(e_targetFreq,  freq);
   lreturnVal = runDdrInit();
   printf("ddr_init returned  0x%x!\r\n",lreturnVal);

   return pdFALSE;
} // prvDdrSetInit

#if SUPPORT_HISTORY
   //static const CLI_Command_Definition_t xheapStats =
   //{
   //   "heap", /* The command string to type. */
   //   "\rheap\t\t\t\t | Displays the OS heap statistics\r\n",
   //   prvTaskheapCommand, /* The function to run. */
   //   0 /* No parameters are expected. */
   //};
   
   //static portBASE_TYPE prvTaskheapCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   //{
   //    USE_ARGS();
   //   //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
   //   // write buffer length is adequate, so does not check for buffer overflows.
   //   configASSERT(pcWriteBuffer);
   // 
   //   sprintf(pcWriteBuffer, "\r\nHeap Size: %d\r\ncurent free heap size: %d\r\nMinimum free heap size: %d\r\n",
   //                  configTOTAL_HEAP_SIZE,  xPortGetFreeHeapSize(), xPortGetMinimumEverFreeHeapSize());
   //   /* There is no more data to return after this single string, so return
   //   pdFALSE. */
   //   return pdFALSE;
   //}
   
   static const CLI_Command_Definition_t xHistory =
   {
      "h",
      "h\t\t\t\t\t | Print command line history\r\n",
      prvPrintHistory, /* The function to run. */
      0 /* no arguments  */
   };
   
   void printHistory(); // implemented in src/cli.c
   
   static portBASE_TYPE prvPrintHistory(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      USE_ARGS();
    
      configASSERT(pcWriteBuffer);
      printHistory();
      return pdFALSE;
   }
#endif // SUPPORT_HISTORY

#if SUPPORT_DDR
   static const CLI_Command_Definition_t xRtcIoBgRead =
   {
      "rtcread",
      "rtcread <addr>\t\t\t\t | read via the rtc IO bridge\r\n",
      prvRtcIoBgRead, 
      1 
   };
   
   static const CLI_Command_Definition_t xRtcIoBgWrite =
   {
      "rtcwrite",
      "rtcwrite <addr> <data>\t\t\t | write via the rtc IO bridge\r\n",
      prvRtcIoBgWrite, 
      2 
   };

   /*
   static const CLI_Command_Definition_t xRingOscTest =
   {
      "ro",
      "\rro\t\t\t\t\t | Ring Oscillator test\r\n",
      RingOscTestCommand, 
      0 
   };
   */

   static const CLI_Command_Definition_t xDdrAccess =
   {
      "ddraccess",
      "ddraccess\t\t\t\t | Intensive and infinate access to the ddr using the g2d DMA hw\r\n",
      prvDdrAccess, 
      -1 
   };

   #if DDR_INTERNAL_USE
      static const CLI_Command_Definition_t xDdrBistLoopback =
      {
         "bist_loopback",
         "bist_loopback\t\t\t\t | ddr bist loop back test.\r\n",
         prvDdrBistLoopback, /* The function to run. */
         0 /* no arguments. */
      };
         
      static const CLI_Command_Definition_t xDdrBist =
      {
         "ddr_bist",
         "ddr_bist <addr> <size> <loops>\t\t | Use the BIST in the DDR Phy to test the DDR\r\n",
         prvDdrBist, /* The function to run. */
         -1 /* row_mask, size */
      };
   #endif

   //static const CLI_Command_Definition_t xDdrTest =
   //{
   //   "ttt",
   //   "ttt <...>\t\t\t | global test...\r\n",
   //   prvDdrTest, /* The function to run. */
   //   -1
   //};
   
   static const CLI_Command_Definition_t xDdrDump =
   {
      "ddreg",
      "ddreg\t\t\t\t\t | Print DDR registers, same output as DdrSetInit\r\n",
      prvDdrDump, /* The function to run. */
      -1
   };
   
   static const CLI_Command_Definition_t xDdrRangeTest =
   {
      "DdrRangeTest",
      "DdrRangeTest <addr> <size> <loops> <op>\t | defaults: 0x40000000 0x20000000 1 3\r\n",
      prvDdrRangeTest, /* The function to run. */
      -1 /* 3 arguments, . */
   };
   
   static const CLI_Command_Definition_t xDdrMemvex =
   {
      "DdrMemvex",
      "DdrMemvex <addr> <size> <op>\t\t | defaults: 0x40000000 0x20000000 0x0000\r\n",
      prvDdrMemvex, /* The function to run. */
      -1 /* 3 arguments, . */
   };

   static const CLI_Command_Definition_t xDdrWinReport =
   {
      "DdrWinReport",
      "DdrWinReport\t\t\t\t | Uses values configured with DdrSetParam\r\n",
      prvDdrWinReport, /* The function to run. */
      -1 /* 3 arguments, . */
   };
   
   static const CLI_Command_Definition_t xDdrSelfRefreshEnter =
   {
      "DdrSelfRefreshEnter",
      "DdrSelfRefreshEnter\t\t\t | ddr self refresh enter\r\n",
      prvDdrSelfRefreshEnter, /* The function to run. */
      0 /* no arguments. */
   };
   
   static const CLI_Command_Definition_t xDmaTest =
   {
      "dma",
      "dma <ptrn> <dst> <size>\t\t\t | execute dma test.\r\n",
      prvDdrDMATest, /* The function to run. */
      -1 /* arguments, . */
   };

   //static const CLI_Command_Definition_t xKasSpiInit =
   //{
   //   "kas_spi_init",
   //   "\rkas_spi_init\t\t\t | Config KAS SPI pins and enable KAS clock.\r\n",
   //   prvKasSpiInit, /* The function to run. */
   //   0 /* 0 arguments. */
   //};

   static portBASE_TYPE prvRtcIoBgWrite(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int laddress;
      unsigned int ldata;
   
      USE_ARGS();
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      configASSERT(pcWriteBuffer);
      laddress = GetParameterAsUlong(pcCommandString,1);
      ldata = GetParameterAsUlong(pcCommandString,2);
      rtcwrite(laddress,ldata);

      //printf("\r\n0x%x\r\n",ldata);
      return pdFALSE;
   }

   static portBASE_TYPE prvRtcIoBgRead(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int laddress;
      unsigned int ldata;
   
      USE_ARGS();
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      configASSERT(pcWriteBuffer);
      laddress = GetParameterAsUlong(pcCommandString,1);
      ldata = rtcread(laddress);
      printf("\r\n0x%x\r\n",ldata);
      return pdFALSE;
   }

   #if DDR_INTERNAL_USE
      static portBASE_TYPE prvDdrBistLoopback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         unsigned int lreturnVal;
         USE_ARGS();
         //Check the write buffer is not NUprinttttttttttLL.  NOTE - for simplicity, this example assumes the
         // write buffer length is adequate, so does not check for buffer overflows.
         configASSERT(pcWriteBuffer);
         lreturnVal = (unsigned int)ddr_bist_loopback();
         printf("\r\n0x%x\r\n",lreturnVal);
         
         return pdFALSE;
      }
   
      static portBASE_TYPE prvDdrBist(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         unsigned int lreturnVal;
         unsigned int laddr=0x0,lsize=0x20000000,lloop=0x1;
         portBASE_TYPE lParameterStringLength;
         
         USE_ARGS();
         configASSERT(pcWriteBuffer);
      
         if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) { laddr = GetParameterAsUlong(pcCommandString,1); }
         if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)) { lsize = GetParameterAsUlong(pcCommandString,2); }
         if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,3 ,&lParameterStringLength)) { lloop = GetParameterAsUlong(pcCommandString,3); }    
      
         lreturnVal = (unsigned int)ddr_bist_dram(15, laddr,lsize,lloop);
         printf("\r\n0x%x\r\n",lreturnVal);
      
         return pdFALSE;
      } // prvDdrBist
   #endif

   //static portBASE_TYPE prvDdrTest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   //{
   //   unsigned int lreturnVal;
   //   unsigned int li0=0,li1=0,li2=0,li3=0,li4=0;
   //   portBASE_TYPE lParameterStringLength;
   // 
   //   USE_ARGS();
   //   configASSERT(pcWriteBuffer);
   // 
   //   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) { li0 = GetParameterAsUlong(pcCommandString,1); }
   //   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)) { li1 = GetParameterAsUlong(pcCommandString,2); }
   //   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,3 ,&lParameterStringLength)) { li2 = GetParameterAsUlong(pcCommandString,3); }
   //   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,4 ,&lParameterStringLength)) { li3 = GetParameterAsUlong(pcCommandString,4); }
   //   if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,5 ,&lParameterStringLength)) { li4 = GetParameterAsUlong(pcCommandString,5); }
   //
   //   lreturnVal = (unsigned int)ddr_test(li0,li1,li2,li3,li4);
   //   printf("0x%x\r\n",lreturnVal);
   // 
   //   return pdFALSE;
   //} // prvDdrTest

   static portBASE_TYPE prvDdrDump(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      dump_ddrphy_regs(0xffffffff);
      dump_upctl_regs();

      return pdFALSE;
   }

   static portBASE_TYPE prvDdrRangeTest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal;
      unsigned int laddr=0x40000000,lsize=0x20000000,lloop=0x1,lop=0x3,lwm=0;
      portBASE_TYPE lParameterStringLength;
      
      USE_ARGS();
      configASSERT(pcWriteBuffer);
   
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) { laddr = GetParameterAsUlong(pcCommandString,1); }
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)) { lsize = GetParameterAsUlong(pcCommandString,2); }
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,3 ,&lParameterStringLength)) { lloop = GetParameterAsUlong(pcCommandString,3); }    
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,4 ,&lParameterStringLength)) { lop   = GetParameterAsUlong(pcCommandString,4); }    
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,5 ,&lParameterStringLength)) { lwm   = GetParameterAsUlong(pcCommandString,5); }    
   
      //Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      lreturnVal = (unsigned int)ddr_range_check(laddr,lsize,lloop,lop,lwm);
      printf("0x%x\r\n",lreturnVal);
      return pdFALSE;
   } // prvDdrRangeTest
      
   static portBASE_TYPE prvDdrMemvex(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal;
      unsigned int laddr=0x40000000,lsize=0x20000000,lop=0x3;
      portBASE_TYPE lParameterStringLength;
   
      USE_ARGS();
      configASSERT(pcWriteBuffer);
   
      if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,1,&lParameterStringLength)) { laddr = GetParameterAsUlong(pcCommandString,1); }
      if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,2,&lParameterStringLength)) { lsize = GetParameterAsUlong(pcCommandString,2); }
      if (NULL != FreeRTOS_CLIGetParameter(pcCommandString,3,&lParameterStringLength)) { lop   = GetParameterAsUlong(pcCommandString,3); }
      
      // Check the write buffer is not NULL.  NOTE - for simplicity, this example assumes the
      // write buffer length is adequate, so does not check for buffer overflows.
      lreturnVal = (unsigned int)ddr_memvex(laddr,lsize,lop,0xffffffff);
      printf("0x%x\r\n",lreturnVal);
      return pdFALSE;
   } // prvDdrMemvex
      
   static portBASE_TYPE prvDdrWinReport(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal;
 
      printf("\r\n");

      // suppress most of the debug output for customers
      setDdrDebugPrint(0x01);
      lreturnVal = runDdrInit();
      printf("ddr_init returned  0x%x!\r\n\r\n",lreturnVal);
      printDdrParameters();
      
      // MDLEN=0 (DXnGCR[30], master delay line disable)
      RD_MOD_WR(MEMC_PHY_PGCR1, ~(1<<26), 1<<26);  
      RD_MOD_WR(MEMC_PHY_DX0GCR, 0xbfffffff, 0); 
      RD_MOD_WR(MEMC_PHY_DX1GCR, 0xbfffffff, 0);

      winCheck("Byte 0 Write",MEMC_PHY_DX0LCDLR1, 0xffffff00,  0, 0);
      winCheck("Byte 1 Write",MEMC_PHY_DX1LCDLR1, 0xffffff00,  0, 1);
      winCheck("Byte 0 Read ",MEMC_PHY_DX0LCDLR1, 0xffff00ff,  8, 0);
      winCheck("Byte 1 Read ",MEMC_PHY_DX1LCDLR1, 0xffff00ff,  8, 1);
      winCheck("Addr/CMD    ",MEMC_PHY_ACBDLR,    0xff03ffff, 18, 0);
      printf("Reset the board...\r\n",lreturnVal);

      return pdFALSE;
   } // prvDdrWinReport

   static portBASE_TYPE prvDdrSelfRefreshEnter(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal;
      USE_ARGS();
      configASSERT(pcWriteBuffer);
      lreturnVal = (unsigned int)ddr_self_refresh_enter();
      printf("0x%x\r\n",lreturnVal);
      return pdFALSE;
   }
   
   // dram starts at 0x40000000
   // 0x40000000 - 0x40001000 - RB
   // 0x40001000 - 0x40002000 - FB
   #define G2D_MEM_BASE 0x40000000 // memory base: in DRAM
   
   int g2d_render(unsigned long* pSrc, unsigned long* pTgt, unsigned long lWidth, unsigned long lHeight)
   {
      unsigned long uSrcOffset = ((unsigned long)pSrc - G2D_MEM_BASE);
      unsigned long uTargetOffset = ((unsigned long)pTgt - G2D_MEM_BASE);
      volatile unsigned long temp;
   
      WRITE_REG(CLKC_GRAPH_LEAF_CLK_EN_SET,0x10); // g2d_g2d_clken set
      // configuration done according to sarah:
      temp = READ_REG(CLKC_SHARED_DIVIDERS_ENA); // clkc_shared_dividers_ena
      temp |= (1<<20);
      WRITE_REG(CLKC_SHARED_DIVIDERS_ENA,temp); // clkc_shared_dividers_ena
      WRITE_REG(CLKC_G2D_CLK_SEL,0x7); //g2d_clk_sel
     
      configASSERT((unsigned long)pSrc >= G2D_MEM_BASE);
      configASSERT((unsigned long)pTgt >= G2D_MEM_BASE);
      configASSERT((lWidth & 0x3) == 0); // DW Allign
      configASSERT(lWidth < (1 << 13));   // fit in register (12 bits)
     
      if (0 == Reg_Polling(G2D_ENG_STATUS, 0x00000002, 0x00000002,40000))
      { return 0; }
   
      WRITE_REG(G2D_FB_BASE,G2D_MEM_BASE);
      WRITE_REG(G2D_ENG_CTRL,0x0); // no ring buffer, work with registers
      WRITE_REG(G2D_DST_OFFSET, uTargetOffset);
      WRITE_REG(G2D_DST_FORMAT, (0x3FFF & (lWidth >> 2)));
      WRITE_REG(G2D_DST_LT,0x0);
      WRITE_REG(G2D_DST_RB, (lHeight << 16 | lWidth));
      WRITE_REG(G2D_CLIP_LT,0x0);
   
      WRITE_REG(G2D_SRC_OFFSET, uSrcOffset);
      WRITE_REG(G2D_SRC_FORMAT, (0x3FFF & (lWidth >> 2)));
      WRITE_REG(G2D_SRC_LT,0x0);
      WRITE_REG(G2D_SRC_RB, (lHeight << 16 | lWidth));
      WRITE_REG(G2D_GBL_ALPHA,0x000000FF);
      WRITE_REG(G2D_DRAW_CTL, 0x000000CC); // last register, start the operation
   
      return 1;
   } // g2d_render
   
   static portBASE_TYPE prvDdrAccess (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal =1;
      unsigned long pattern1 = (unsigned long)0x5aa55aa5;   
      unsigned long pattern2 = (unsigned long)0xa55aa55a;   
      unsigned long* pDst =(unsigned long*)0x5fff0000;   
      unsigned long* pSrc1 =(unsigned long*)0x40000000;   
      unsigned long* pSrc2 =(unsigned long*)0x41000000;   
      const unsigned int blocksize = 0x800; //0xFFC
      unsigned int i,blockcount =0;
       
      for(i=0; i<  10 *blocksize; i++)
      {
         pSrc1[i] = pattern1;
         i++;
         pSrc1[i] = pattern2;
      }
   
      for(i=0; i<  10 *blocksize; i++)
      {
         pSrc2[i] = pattern2;  
         i++;
         pSrc2[i] = pattern1;  
      }
   
      printf("\r\nPattern filled by 0x%x\r\n",&pSrc1[i]);
   
      while ((lreturnVal == 1) && (blockcount < 30000000))
      {
         blockcount++;
         lreturnVal = g2d_render(pSrc1, pDst ,blocksize,1);
         if (lreturnVal == 0)
         {
            sprintf(pcWriteBuffer, "\r\nDMA operation fail!\r\n");
            return pdFALSE;
         }
         lreturnVal = g2d_render(pSrc2, pDst ,blocksize,1);
         if (lreturnVal == 0)
         {
            sprintf(pcWriteBuffer, "\r\nDMA operation fail!\r\n");
            return pdFALSE;
         }
      } // really long loop for scope capture to be done during read/write access
       
      sprintf(pcWriteBuffer, "\r\nDMA test %s\r\n", (0 == lreturnVal) ? "fail" : "succeed");
   
      return pdFALSE;
   } // prvDdrAccess

   static portBASE_TYPE prvDdrDMATest(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   {
      unsigned int lreturnVal;
      unsigned long data1,data2,data3,data4,pattern = (unsigned long)0x5a5a5a5a;   
      unsigned long* pSrc =(unsigned long*)0x40000000;   
      unsigned long* pDst =(unsigned long*)0x42000000;   
      const unsigned int blocksize = 0x800; //0xFFC
      unsigned int size = 2 *blocksize;
      unsigned int endAddress,i,blockcount =0;//,currEndAddress = 0x60000000;
      unsigned int ee=0; // total errors in all loops
      portBASE_TYPE lParameterStringLength;
      USE_ARGS();
      configASSERT(pcWriteBuffer);
   
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) { pattern = (unsigned long)GetParameterAsUlong(pcCommandString,1); }
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)) { pDst = (unsigned long*)GetParameterAsUlong(pcCommandString,2); }
      if(NULL != FreeRTOS_CLIGetParameter(pcCommandString,3 ,&lParameterStringLength)) { size = GetParameterAsUlong(pcCommandString,3); }
       
      //size = (size + blocksize - 1) - ((size + blocksize - 1) % blocksize);
      //printf("\r\nSize aligned to 0x%x\r\n",size);
      //configASSERT(((unsigned long)pDst  > ((unsigned long)pSrc + blocksize)); // DW Allign
      /* fill the source section of the memory with data */
       
      for(i=0; i<10*blocksize; i++)
      {
         pSrc[i] = pattern;  
         //*(pSrc+i) = pattern;
      }
      printf("\r\npattern filled by 0x%x\r\n",&pSrc[i]);
   
      endAddress = (unsigned int)&pDst[size/4];
      printf("\r\nDMA will check from 0x%x to 0x%x in blocks of %d Bytes\r\n",(unsigned long)pDst, endAddress,4*blocksize);
   
      lreturnVal = g2d_render(pSrc, pDst ,blocksize,1);
      if(lreturnVal == 0)
      {
         sprintf(pcWriteBuffer, "\r\nDMA operation fail!\r\n");
         return pdFALSE;
      }
      //foreachgiga:
      //currEndAddress = MIN(currEndAddress,endAddress);
      /* perform first DMA operation */
      while ((unsigned int) pDst < endAddress)
      {
         //printf("\r\nCheck block %d, dest address is 0x%x block size is 0x%x\r\n",blockcount,(unsigned long)pDst,blocksize);
         if ((unsigned int) pDst < endAddress)
         {
            lreturnVal = g2d_render(pSrc, &pDst[blocksize] ,blocksize,1);
            if(lreturnVal == 0)
            {
               sprintf(pcWriteBuffer, "\r\nDMA operation fail!\r\n");
               return pdFALSE;
            }
         }
         
         lreturnVal = 1;
   
         /* test results */
         //printf("\r\n before comparing addrress is 0x%x\r\n",(unsigned long)&pDst[0]);
         for(i=0; i<blocksize; i+=4)
         {
            data1 = pDst[i];
            data2 = pDst[i+1];
            data3 = pDst[i+2];
            data4 = pDst[i+3];
            //if((pSrc[i] != data1) || (pSrc[i+1] != data2) || (pSrc[i+2] != data3) || (pSrc[i+3] != data4))
            if((pattern != data1) || (pattern != data2) || (pattern != data3) || (pattern != data4))
            {
               printf("\r\ncomp buffer failed expected 0x%x\n\r Address 0x%x: got 0x%x\n\r Address 0x%x: got 0x%x\n\r Address 0x%x: got 0x%x\n\r Address 0x%x: got 0x%x\n\r\n\r",pattern, (unsigned long) &pDst[i],data1, (unsigned long) &pDst[i+1],data2, (unsigned long) &pDst[i+2],data3, (unsigned long) &pDst[i+3],data4); 
               lreturnVal = 0;
               ee++;
               //return pdFALSE;
            }
            //printf("Checked address 0x%x\r\n",&pDst[i+3]);
         }
        
         //printf("last address tested is 0x%x\n",&pDst[i]);
         //temp = (unsigned long) pDst;
         pDst = &pDst[i];
         //printf("\r\ndebug: block done! next address is 0x%x",(unsigned long)pDst);
         
         blockcount ++;
         if ((blockcount % 8192) == 0)
         {
            printf("\r\nDMA: pass Address 0x%x , so far so good\r\n",(unsigned long)pDst);
         }
      }
   
      if (ee) {printf("\r\nnum of errors %d\r\n",ee);    }
      else    {sprintf(pcWriteBuffer, "\r\nDMA test %s\r\n", (0 == lreturnVal) ? "fail" : "succeed");   }
   
      return pdFALSE;
   } // prvDdrDMATest

   //static portBASE_TYPE prvKasSpiInit(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
   //{
   //   USE_ARGS();
   //   configASSERT(pcWriteBuffer);
   //   printf("setting kas on usp0 pins: clk, cs, din, dout \r\n");
   //   WRITE_REG(0x10e4012c, 0x00000007);  //SW_TOP_FUNC_SEL_21_REG_CLR - clk 
   //   WRITE_REG(0x10e40128, 0x00000002);  //SW_TOP_FUNC_SEL_21_REG_SET - clk
   //   WRITE_REG(0x10e4012c, 0x00007000);  //SW_TOP_FUNC_SEL_21_REG_CLR - cs
   //   WRITE_REG(0x10e40128, 0x00002000);  //SW_TOP_FUNC_SEL_21_REG_SET - cs
   //   WRITE_REG(0x10e4012c, 0x00000700);  //SW_TOP_FUNC_SEL_21_REG_CLR - din
   //   WRITE_REG(0x10e40128, 0x00000200);  //SW_TOP_FUNC_SEL_21_REG_SET - din
   //   WRITE_REG(0x10e4012c, 0x00000070);  //SW_TOP_FUNC_SEL_21_REG_CLR - dout
   //   WRITE_REG(0x10e40128, 0x00000020);  //SW_TOP_FUNC_SEL_21_REG_SET - dout
   //   WRITE_REG(0x18880a0c, 0x00000100);  //SW_RTC_IN_DISABLE_1_REG_CLR - enable cs input
   //   WRITE_REG(0x18880a08, 0x00000000);  //SW_RTC_IN_DISABLE_1_REG_SET
   //   printf("CLKC config \r\n");
   //   WRITE_REG(0x186201F4, 0x4);         //CLKC_KAS_CLK_SEL - SYS1_USBPLL DIV19 
   //   WRITE_REG(0x186204A0, 0xffff);      //CLKC_LEAF_CLK_EN1_SET - all KAS and audio clocks are enabled 
   //   sprintf(pcWriteBuffer, "\r\nDone.\r\n");
   //   return pdFALSE;
   //}

   /*
   #if DDR_INTERNAL_USE
      static portBASE_TYPE RingOscTestCommand(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         printf("\r\nRing Oscillator TEST:\r\n");
         int err = ringosc_one();
         printf("Ring Oscillator TEST Result: %d\r\n", err);
       
         return pdFALSE;
      }
   #endif
   */
#endif // SUPPORT_DDR

#if SUPPORT_DDR 
   #if (COMPACT_IMAGE == 0)
      static const CLI_Command_Definition_t xNocConnect =
      {
         "NocConnect",
         "NocConnect <oper> <socket id>\t | Connect/Disconnect Noc socket DRAM.\n",
         prvNoCSocketConnect,
        -1, // to allow help
      };
      
      static const CLI_Command_Definition_t xCountData =
      {
         "Count",
         "Count <niu id>\t\t\t | Start meausre BW from DRAM.\n",
         prvCountData,
        -1,
      };
      
      static const CLI_Command_Definition_t xGetData =
      {
         "GetData",
         "GetData\t\t\t\t | Get Data\n",
         prvGetData, 
         0 
      };

      static const CLI_Command_Definition_t xDisableMMU =
      {
         "DisableMMU",
         "DisableMMU\t\t\t | Disable MMU and Cache Access.\r\n",
         prvDisableMMU, /* The function to run. */
         0 /* 0 arguments. */
      };

      static portBASE_TYPE prvNoCSocketConnect(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         portBASE_TYPE lParameterStringLength;
         unsigned long socket_id,operation,ldata;
         USE_ARGS();
         configASSERT(pcWriteBuffer);
      
         if((NULL == FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength)) 
            || (NULL == FreeRTOS_CLIGetParameter(pcCommandString,2 ,&lParameterStringLength)))
         {
            // print usage!
            printf("\r\n Usage: NocConnect <Connect/Disconnect><socket_id>\n");
            printf("\r Connect = 1, Disconnect = 0\n");
            printf("\n\r |=======================================|\n");
            printf("\r | socket id \t |  socket Name \t\t |\n");
            
            printf("\r | 0x100 \t |  ALL Sockets\t |\n");
            printf("\r | 0 \t\t |  Audio IF \t\t\t |\n");
            printf("\r | 1 \t\t |  DMAC2 \t\t |\n");
            printf("\r | 2 \t\t |  DMAC3 \t\t |\n");
            printf("\r | 3 \t\t |  SPDIF \t\t |\n");
            printf("\r | 4 \t\t |  USP0 \t\t |\n");
            printf("\r | 5 \t\t |  USP1 \t\t |\n");
            printf("\r | 6 \t\t |  USP2 \t\t |\n");
            printf("\r | 7 \t\t |  DVM \t\t |\n");
            printf("\r | 8 \t\t |  LVDS \t\t |\n");
            printf("\r | 9 \t\t |  A7 SPRAM1\t |\n");
            printf("\r | 10 \t\t |  A7 SPRAM2\t |\n");
            printf("\r | 11 \t\t |  DMAC0 \t\t |\n");
            printf("\r | 12 \t\t |  SPI1 \t\t |\n");
            printf("\r | 13 \t\t |  UART0 \t\t |\n");
            printf("\r | 14 \t\t |  UART1 \t\t |\n");
            printf("\r | 15 \t\t |  UART2 \t\t |\n");
            printf("\r | 16 \t\t |  UART3 \t\t |\n");
            printf("\r | 17 \t\t |  UART4 \t\t |\n");
            printf("\r | 18 \t\t |  UART5 \t\t |\n");
            printf("\r | 19 \t\t |  GPIO0 \t\t |\n");
            printf("\r | 20 \t\t |  I2C1 \t\t |\n");
            printf("\r | 21 \t\t |  I2C \t\t |\n");
            printf("\r |=======================================|\n\r");
            return pdFALSE;
         }
      
         operation =  (unsigned long)GetParameterAsUlong(pcCommandString,1);
         socket_id =  (unsigned long)GetParameterAsUlong(pcCommandString,2);
         if (socket_id == 0x100)
         {
            ldata = 0x3fffff;
         }
         else
         {
            ldata = 1<< socket_id;
            printf ("ldata = 0x%x\n",ldata);
         }
         if (operation == 0)
         {
            // disconnect
            WRITE_REG(0x186202ec,ldata); //CLKC_NOC_CLK_SLVRDY_CLEAR
            // wait on socket conn
            Reg_Polling(0x18620304,ldata,0,20000);
            // turn off clk
         }
         else // connecting
         {
            // turn on clk
            WRITE_REG(0x186202e8,ldata); // CLKC_NOC_CLK_SLVRDY_SET
            // wait on socket conn
            Reg_Polling(0x18620304,ldata,ldata,20000);
         }
      
         return pdFALSE;
      }
      
      #define A7_probe_base_address  0x10200400
      #define DDR_probe_base_address  0x10820400
      #define audio_probe_base_address  0x10ed0000
      #define kas_probe_base_address   0x10ed3000
      #define btm_probe_base_address   0x11010400
      #define gnss_probe_base_address   0x18100800
      #define gnss_io_probe_base_address  0x18102000
      #define gpu_probe_base_address   0x13000400
      #define sdr_probe_base_address   0x13002000
      #define mediam_probe_base_address   0x170a0c00
      #define vxd_probe_base_address   0x170a2000
      #define trans_probe_base_address   0x18810400
      #define vdifm_deint_probe_base_address  0x13290000
      #define disp0_probe_base_address   0x13293000
      #define disp1_probe_base_address   0x13294000
      #define vdifm_io_probe_base_address   0x13295000
      #define vip1_probe_base_address   0x13296000
      
      #define CoreId   0x000//   r/o
      #define RevisionId   0x004//   r/o
      #define MainCtl   0x008 //   r/w
      #define TracePortSel 0x10
      #define CfgCtl   0x00C //   r/w
      #define FilterLut   0x014 //   r/w
      #define TraceAlarmEn   0x018 //   r/w
      #define TraceAlarmStatus   0x01C //   r/o
      #define TraceAlarmClr   0x020 //   r/w
      #define StatPeriod   0x024 //   r/w
      #define StatGo   0x028 //   r/w
      #define StatAlarmMin   0x02C //   r/w
      #define StatAlarmMax   0x030 //   r/w
      #define StatAlarmStatus   0x034 //   r/o
      #define StatAlarmClr   0x038 //   r/w
      #define StatAlarmEn   0x03C //   r/w
      #define Filters_0_RouteIdBase   0x044 //   r/w
      #define Filters_0_RouteIdMask   0x048 //   r/w
      #define Filters_0_AddrBase_Low   0x04C //   r/w
      #define Filters_0_WindowSize   0x054 //   r/w
      #define Filters_0_Opcode   0x060 //   r/w
      #define Filters_0_Status   0x064 //   r/w
      #define Filters_0_Length   0x068 //   r/w
      #define Filters_0_Urgency   0x06C //   r/w
      #define Filters_0_UserBase   0x070 //   r/w
      #define Filters_0_UserMask   0x074 //   r/w
      #define Filters_1_RouteIdBase   0x080 //   r/w
      #define Filters_1_RouteIdMask   0x084 //   r/w
      #define Filters_1_AddrBase_Low   0x088 //   r/w
      #define Filters_1_WindowSize   0x090 //   r/w
      #define Filters_1_Opcode   0x09C //   r/w
      #define Filters_1_Status   0x0A0 //   r/w
      #define Filters_1_Length   0x0A4 //   r/w
      #define Filters_1_Urgency   0x0A8 //   r/w
      #define Filters_1_UserBase   0x0AC //   r/w
      #define Filters_1_UserMask   0x0B0 //   r/w
      #define Filters_2_RouteIdBase   0x0BC //   r/w
      #define Filters_2_RouteIdMask   0x0C0 //   r/w
      #define Filters_2_AddrBase_Low   0x0C4 //   r/w
      #define Filters_2_WindowSize   0x0CC //   r/w
      #define Filters_2_Opcode   0x0D8 //   r/w
      #define Filters_2_Status   0x0DC //   r/w
      #define Filters_2_Length   0x0E0 //   r/w
      #define Filters_2_Urgency   0x0E4 //   r/w
      #define Filters_2_UserBase   0x0E8 //   r/w
      #define Filters_2_UserMask   0x0EC //   r/w
      #define Filters_3_RouteIdBase   0x0F8 //   r/w
      #define Filters_3_RouteIdMask   0x0FC //   r/w
      #define Filters_3_AddrBase_Low   0x100 //   r/w
      #define Filters_3_WindowSize   0x108 //   r/w
      #define Filters_3_Opcode   0x114 //   r/w
      #define Filters_3_Status   0x118 //   r/w
      #define Filters_3_Length   0x11C //   r/w
      #define Filters_3_Urgency   0x120 //   r/w
      #define Filters_3_UserBase   0x124 //   r/w
      #define Filters_3_UserMask   0x128 //   r/w
      #define Counters_0_Src   0x138 //   r/w
      #define Counters_0_AlarmMode   0x13C //   r/w
      #define Counters_0_Val   0x140 //   r/w
      #define Counters_1_AlarmMode   0x150 //   r/w 
      #define Counters_1_Src   0x14c //   r/w
      #define Counters_1_Val   0x154 //   r/w
      
      #define SINGLE_PORT     0x55
      #define BASE_ADDRESS    0
      #define PORT            1
      #define NUM_NIUS        22
      #define ALL_NIUS        0x100
      #define ALL_NIUS_PORT0  0x100
      #define ALL_NIUS_PORT1  0x101
      #define ALL_NIUS_PORT2  0x102
      #define ALL_NIUS_PORT3  0x103
      
      #define FILTER_CODE_OFF 0x0
      #define FILTER_CODE_COUNT_CYCLE 0x1
      #define FILTER_CODE_COUNT_IDLE_CYCLES 0x2
      #define FILTER_CODE_COUNT_XFER_CYCLES 0x3
      #define FILTER_CODE_COUNT_BUSY_CYCLES 0x4
      #define FILTER_CODE_COUNT_WAIT_CYCLES 0x5
      #define FILTER_CODE_COUNT_PACKETS     0x6
      #define FILTER_CODE_COUNT_PACKET_SELECTED_BY_LUT 0x7
      #define FILTER_CODE_COUNT_BYTES         0x8
      #define FILTER_CODE_COUNT_PRESS0_CYCLES 0x9
      #define FILTER_CODE_COUNT_PRESS1_CYCLES 0xA
      #define FILTER_CODE_COUNT_PRESS2_CYCLES 0xB
      #define FILTER_CODE_COUNT_FILTER0_PACKETS 0xC
      #define FILTER_CODE_COUNT_FILTER1_PACKETS 0xD
      #define FILTER_CODE_COUNT_FILTER2_PACKETS 0xE
      #define FILTER_CODE_COUNT_FILTER3_PACKETS 0xF
      #define FILTER_CODE_CHAIN_TO_PREV_COUNT 0x10
      
      #define OPCODE_FLAG_READ_PACKETS 1
      #define OPCODE_FLAG_WRITE_PACKETS 2
      #define OPCODE_FLAG_LCOK_ENABLE_PACKETS 4
      #define OPCODE_FLAG_URGENCY_PACKETS 8
      
      #define STATUS_FLAG_REQUESTS 1
      #define STATUS_FLAG_RESPONCES 2
      
      #define ALARM_TRIGER_BY_MIN_AND_MAX 0x3
      
      static const unsigned long ProbLut[NUM_NIUS][2] =
      {
        { A7_probe_base_address, SINGLE_PORT }, // A7
        { DDR_probe_base_address, SINGLE_PORT }, // DDR
        { audio_probe_base_address, SINGLE_PORT }, // vip0 
        { kas_probe_base_address,  1}, //dmac2  
        { kas_probe_base_address,  0}, //dmac3
        { kas_probe_base_address,  3}, //kas 
        { kas_probe_base_address,  2}, //usp0
        { btm_probe_base_address,  SINGLE_PORT }, //dmac4
        { gnss_probe_base_address, SINGLE_PORT }, // dmac0 
        { gnss_io_probe_base_address,SINGLE_PORT }, //eth  
        { gpu_probe_base_address,  SINGLE_PORT }, //sgx
        { sdr_probe_base_address,  SINGLE_PORT }, //sdr
        { mediam_probe_base_address, SINGLE_PORT }, // g2d 
        { vxd_probe_base_address,  SINGLE_PORT }, //vxd
        { trans_probe_base_address,  SINGLE_PORT }, // ??
        { vdifm_deint_probe_base_address,  SINGLE_PORT }, // dcu
        { disp0_probe_base_address,  0 }, //lcd0
        { disp0_probe_base_address,  1}, //vpp0
        { disp1_probe_base_address,  0}, //lcd1
        { disp1_probe_base_address,  1}, //vpp1
        { vdifm_io_probe_base_address, SINGLE_PORT }, //sys2pci 
        { vip1_probe_base_address,  SINGLE_PORT }  //vip
      
      };
      
      static unsigned long lBaseAddress;
      static unsigned long lPort;
      
      static portBASE_TYPE prvCountData(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         portBASE_TYPE lParameterStringLength;
         unsigned long tmp,niu_id, lcurrport,i;
         USE_ARGS();
         configASSERT(pcWriteBuffer);
      
         if(NULL == FreeRTOS_CLIGetParameter(pcCommandString,1 ,&lParameterStringLength))
         {
            // print usage!
            printf("\r\n Usage: Count <niu_id>\n");
            printf("\n\r |=======================================|\n");
            printf("\r | niu id \t |  niu Name \t\t |\n");
            
            printf("\r | 0x100 \t |  ALL NIUS Port 0\t |\n");
            printf("\r | 0x101 \t |  ALL NIUS Port 1\t |\n");
            printf("\r | 0x102 \t |  ALL NIUS Port 2\t |\n");
            printf("\r | 0x103 \t |  ALL NIUS Port 3\t |\n");
            printf("\r | 0 \t\t |  A7 \t\t\t |\n");
            printf("\r | 1 \t\t |  DDR \t\t |\n");
            printf("\r | 2 \t\t |  vip0 \t\t |\n");
            printf("\r | 3 \t\t |  dmac2 \t\t |\n");
            printf("\r | 4 \t\t |  dmac3 \t\t |\n");
            printf("\r | 5 \t\t |  kas \t\t |\n");
            printf("\r | 6 \t\t |  usp0 \t\t |\n");
            printf("\r | 7 \t\t |  dmac4 \t\t |\n");
            printf("\r | 8 \t\t |  dmac0 \t\t |\n");
            printf("\r | 9 \t\t |  eth \t\t |\n");
            printf("\r | 10 \t\t |  sgx \t\t |\n");
            printf("\r | 11 \t\t |  sdr \t\t |\n");
            printf("\r | 12 \t\t |  g2d \t\t |\n");
            printf("\r | 13 \t\t |  vxd \t\t |\n");
            printf("\r | 14 \t\t |  ??? \t\t |\n");
            printf("\r | 15 \t\t |  dcu \t\t |\n");
            printf("\r | 16 \t\t |  lcd0 \t\t |\n");
            printf("\r | 17 \t\t |  vpp0 \t\t |\n");
            printf("\r | 18 \t\t |  lcd1 \t\t |\n");
            printf("\r | 19 \t\t |  vpp1 \t\t |\n");
            printf("\r | 20 \t\t |  sys2pci \t\t |\n");
            printf("\r | 21 \t\t |  vip \t\t |\n");
            printf("\r |=======================================|\n\r");
            return pdFALSE;
         }
      
         niu_id =  (unsigned long)GetParameterAsUlong(pcCommandString,1);
         if (niu_id >= ALL_NIUS)
         {
            lPort = niu_id - ALL_NIUS;
            printf("\rEnableing all clks to avoid data aborts!\n\r");
            WRITE_REG(0x186204b8,0xfffff);
            WRITE_REG(0x18620548,0xc3);
            WRITE_REG(0x186204d0,0x1e406);
            WRITE_REG(0x18620530,7);
            WRITE_REG(0x186204e8,5);
      
            for (i= 0; i < NUM_NIUS; i++)
            {
               if ((ProbLut[i][PORT] == SINGLE_PORT) || (ProbLut[i][PORT] == lPort))
               {
                  lBaseAddress = ProbLut[i][BASE_ADDRESS];
                  lcurrport = ProbLut[i][PORT];
                  tmp = READ_REG(lBaseAddress + MainCtl);// probe_main_Probe_MainCtl
                  WRITE_REG((lBaseAddress + MainCtl), tmp | 0x8); // probe_main_Probe_MainCtl
                  //II.   Only if The table above contain port number:
                  //Set register Counters_0_PortSel to the value corresponding to the probe point of interest. 
                  // - no need , A& probe doesnt have more than one port
                  //III.    Set register Counters_0_Src to 0x8 (BYTES) to count bytes.
                  if (lcurrport != SINGLE_PORT)
                  {
                     WRITE_REG((lBaseAddress + TracePortSel),lcurrport); //probe_main_Probe_Counters_0_Src
                  }
                  WRITE_REG((lBaseAddress + Counters_0_Src),FILTER_CODE_COUNT_BYTES); //probe_main_Probe_Counters_0_Src
                  //IV.   Set register Counters_1_Src to 0x10 (CHAIN) to increment when counter 0 wraps.
                  WRITE_REG((lBaseAddress + Counters_1_Src),FILTER_CODE_CHAIN_TO_PREV_COUNT); //probe_main_Probe_Counters_1_Src
                  //V.   Setting register StatPeriod to 0x00 (manual mode)
                  WRITE_REG((lBaseAddress + StatPeriod),0x0); //probe_main_Probe_StatPeriod
                  //VI.   Set field GlobalEn of register CfgCtl to 1 to enable the counting of bytes.
                  WRITE_REG((lBaseAddress + CfgCtl), 0x1); //probe_main_Probe_CfgCtl
                  //VII.   Set register StatGo  to 1
                  WRITE_REG((lBaseAddress + StatGo), 0x1); //probe_main_Probe_StatGo
               }
            }
            // signal to get data to get all
            lBaseAddress= 0;
            return pdFALSE;
         }
      
         // single NIU!!!
      
         // Read data transferred between A7 & DRAM - witnin known interval (sw start and stop the measurements 
         //I.   Set field StatEn to 1 in register MainCtl
         lBaseAddress = ProbLut[niu_id][BASE_ADDRESS];
         printf("\r\n lbase address = 0x%x\r\n",lBaseAddress);
         lcurrport = ProbLut[niu_id][PORT];
      
         tmp = READ_REG(lBaseAddress + MainCtl);// probe_main_Probe_MainCtl
         WRITE_REG((lBaseAddress + MainCtl), tmp | 0x8); // probe_main_Probe_MainCtl
         //II.   Only if The table above contain port number:
         //Set register Counters_0_PortSel to the value corresponding to the probe point of interest. 
         // - no need , A& probe doesnt have more than one port
         //III.    Set register Counters_0_Src to 0x8 (BYTES) to count bytes.
         if (lcurrport != SINGLE_PORT)
         {
            WRITE_REG((lBaseAddress + TracePortSel),lcurrport); //probe_main_Probe_Counters_0_Src
         }
      
         WRITE_REG((lBaseAddress + Counters_0_Src),FILTER_CODE_COUNT_BYTES); //probe_main_Probe_Counters_0_Src
         //IV.   Set register Counters_1_Src to 0x10 (CHAIN) to increment when counter 0 wraps.
         WRITE_REG((lBaseAddress + Counters_1_Src),FILTER_CODE_CHAIN_TO_PREV_COUNT); //probe_main_Probe_Counters_1_Src
         //V.   Setting register StatPeriod to 0x00 (manual mode)
         WRITE_REG((lBaseAddress + StatPeriod),0x0); //probe_main_Probe_StatPeriod
      
         // filter
         if (lBaseAddress == DDR_probe_base_address) // only DDR has filter, this is an example to use the filter
         {
            WRITE_REG((lBaseAddress + Filters_0_Opcode),OPCODE_FLAG_READ_PACKETS | OPCODE_FLAG_WRITE_PACKETS); // read & write
            WRITE_REG((lBaseAddress + Filters_0_Status),STATUS_FLAG_REQUESTS); // read & write
            WRITE_REG((lBaseAddress + Filters_0_Length),0xf); // read & write
            WRITE_REG((lBaseAddress + Filters_0_AddrBase_Low),0x0); // read & write
            WRITE_REG((lBaseAddress + Filters_0_WindowSize),0x8); // 256!
            WRITE_REG((lBaseAddress + StatAlarmMin),0xFFFFFFFF); // less than 0xffffffff bytes
            WRITE_REG((lBaseAddress + StatAlarmMax),0); // more than zero bytes
            WRITE_REG((lBaseAddress + Counters_0_AlarmMode),ALARM_TRIGER_BY_MIN_AND_MAX); //both min & max
         }
      
         //VI.   Set field GlobalEn of register CfgCtl to 1 to enable the counting of bytes.
         WRITE_REG((lBaseAddress + CfgCtl), 0x1); //probe_main_Probe_CfgCtl
         //VII.   Set register StatGo  to 1
         //WRITE_REG((lBaseAddress + StatGo), 0x1); // if we configure the alram trigger, than stat Go will freeze the Counters.
      
         return pdFALSE;
      }
      
      static portBASE_TYPE prvGetData(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         USE_ARGS();
         unsigned long cnt0, cnt1,total_bytes, i;
      
         if (lBaseAddress == 0)
         {
            printf("\n\r |=======================================|\n");
            printf("\r | niu id  |  total bytes \t\t |\n");
      
            for (i=0; i < NUM_NIUS; i++)
            {
               lBaseAddress = ProbLut[i][BASE_ADDRESS];
               if ((ProbLut[i][PORT] == SINGLE_PORT) || (ProbLut[i][PORT] == lPort))
               {
                  //printf("reading from 0x%x & ox%x\n\n",(lBaseAddress + Counters_0_Val),(lBaseAddress + Counters_1_Val));
                  cnt0 = READ_REG(lBaseAddress + Counters_0_Val); //probe_main_Probe_Counters_0_Val
                  cnt1 = READ_REG(lBaseAddress + Counters_1_Val); //probe_main_Probe_Counters_1_Val
                  total_bytes = (cnt1 << 16) + cnt0;
                  printf("\r | %2d\t |  0x%08x (%08d) Bytes |\n",i,total_bytes, total_bytes);
               }
            }
            lBaseAddress = 0;
            printf("\n\r |=======================================|\n\r");
            return pdFALSE;
         }
      
         //So user can read the number of packets contained in registers:
         //Counters_0_Val 
         //Counters_1_Val.
         //NOTE: The statistics counters are not frozen.
         // Read data transferred between A7 & DRAM 
         //printf("reading from 0x%x & ox%x\n\n",(lBaseAddress + Counters_0_Val),(lBaseAddress + Counters_1_Val));
         cnt0 = READ_REG(lBaseAddress + Counters_0_Val); //probe_main_Probe_Counters_0_Val
         cnt1 = READ_REG(lBaseAddress + Counters_1_Val); //probe_main_Probe_Counters_1_Val
         total_bytes = (cnt1 << 16) + cnt0;
         printf("total Bytes %u\n\r" ,total_bytes);
      
         return pdFALSE;
      }
   
      void DisableMMU(); // implemented in mmu_c.c

      static portBASE_TYPE prvDisableMMU(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
      {
         USE_ARGS();
    
         configASSERT(pcWriteBuffer);
         DisableMMU();
         return pdFALSE;
      }
   #endif // COMPACT_IMAGE == 0
#endif // SUPPORT_DDR

void vRegisterCLICommands(void)
{
   FreeRTOS_CLIRegisterCommand(&xr);
   FreeRTOS_CLIRegisterCommand(&xw);
   FreeRTOS_CLIRegisterCommand(&xRtcIoBgRead);
   FreeRTOS_CLIRegisterCommand(&xRtcIoBgWrite);
   FreeRTOS_CLIRegisterCommand(&xHistory);

   #if DDR_INTERNAL_USE
      FreeRTOS_CLIRegisterCommand(&xBlinkLed);
      FreeRTOS_CLIRegisterCommand(&xrmw);
      FreeRTOS_CLIRegisterCommand(&xp);
      //FreeRTOS_CLIRegisterCommand(&xRingOscTest);
   #endif

   FreeRTOS_CLIRegisterCommand(&xDdrSetParam);
   FreeRTOS_CLIRegisterCommand(&xDdrSetInit);
   FreeRTOS_CLIRegisterCommand(&xDdrWinReport);
   FreeRTOS_CLIRegisterCommand(&xDdrAccess);
   FreeRTOS_CLIRegisterCommand(&xDdrRangeTest);   
   FreeRTOS_CLIRegisterCommand(&xDdrSelfRefreshEnter);   
   FreeRTOS_CLIRegisterCommand(&xDdrMemvex);
   FreeRTOS_CLIRegisterCommand(&xDmaTest);
   FreeRTOS_CLIRegisterCommand(&xsleep);
   FreeRTOS_CLIRegisterCommand(&xDdrDump);
   
   #if DDR_INTERNAL_USE
      FreeRTOS_CLIRegisterCommand(&xDdrBist);
      FreeRTOS_CLIRegisterCommand(&xDdrBistLoopback);
   #endif

#if SUPPORT_HISTORY
   //FreeRTOS_CLIRegisterCommand(&xheapStats);
#endif

#if SUPPORT_DDR    

   //FreeRTOS_CLIRegisterCommand(&xDdrTest);
   //FreeRTOS_CLIRegisterCommand(&xRingOscTest);
   //FreeRTOS_CLIRegisterCommand(&xKasSpiInit);

   //#if (COMPACT_IMAGE == 0)
   //   FreeRTOS_CLIRegisterCommand(&xNocConnect);
   //   FreeRTOS_CLIRegisterCommand(&xCountData);
   //   FreeRTOS_CLIRegisterCommand(&xGetData);
   //   FreeRTOS_CLIRegisterCommand(&xDisableMMU);
   //#endif   
#endif
    
   /* Setup the default DDR parameters before any commands get run */
   initDdrSetupParameters();
} // vRegisterCLICommands

