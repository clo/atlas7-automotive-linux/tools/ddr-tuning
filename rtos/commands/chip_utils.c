/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

// Module Name: 
//    chip_utils.c
//
// Abstract: 
//   
// 
// Notes:
//    
//
/*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
#include "FreeRTOSConfig.h"

#include "chip_utils.h"
#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS.h"
#include "task.h"

// chip includes
#include "chip_utils.h"
/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
#include "strutils.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void idle( int Iter )
{
  volatile int i;

  for( i = 0; i < Iter; i++ )
    ;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void wait_us ( unsigned int n ) {
    volatile unsigned int u, c;
    u = n;
    while (u--){
        c = 120;
        while (c--){
            ;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void wait_100ns ( unsigned int n ) {
    volatile unsigned int u, c;
    u = n;
    while (u--){
        c = 12;
        while (c--){
            ;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void rtcwrite(unsigned long laddress, unsigned long ldata)
{
  WRITE_REG(0x18840008,laddress); // CPURTCIOBG_ADDR
  wait_100ns (1);
  WRITE_REG(0x1884000C,ldata); // CPURTCIOBG_DATA
  wait_100ns (1);
  WRITE_REG(0x18840004,0xF1); // CPURTCIOBG_WRBE - command Write
  wait_100ns (1);
  WRITE_REG(0x18840000,1); // CPURTCIOBG_CTRL - GO!
  wait_100ns (1);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
unsigned long rtcread ( unsigned long laddress)
{
  unsigned long ldata;

  WRITE_REG(0x18840008,laddress); // CPURTCIOBG_ADDR
  wait_100ns (1);
  WRITE_REG(0x18840004,0x0); // CPURTCIOBG_WRBE - command read
  wait_100ns (1);
  WRITE_REG(0x18840000,1); // CPURTCIOBG_CTRL - GO! 
  Reg_Polling(0x18840000,1,0,5000); 
  wait_100ns (1);
  ldata = READ_REG(0x1884000C); // CPURTCIOBG_DATA
  wait_100ns (1);
  return ldata;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
int Reg_Polling(unsigned int laddress, unsigned int lmask, unsigned int lval, int ilimit)
{
  int i;
  unsigned int regVal =0;
  lval &= lmask;

  for ( i =0; i < ilimit; i++)
  {
    regVal = READ_REG(laddress);
    if ( lval == (regVal & lmask ))
    {
      return 1;
    }
	//wait_us(1);
	//printf("polling\n");
  }
  printf ("Polling on register 0x%x has failed! value is 0x%x\r\n",laddress,regVal);
  return 0;
}

//pSrc[i++] = pattern1;  


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
static unsigned int msDelay;

static void BlinkLedTask( void *pvParameters )
{
    portTickType xNextWakeTime;

    /* Remove warning about unused parameters. */
    (void)pvParameters;

    /* Initialise xNextWakeTime - this only needs to be done once. */
    xNextWakeTime = xTaskGetTickCount();

    for( ;; )
    {
        rGPIO_VDIFM_CFG_GNSS_3  = rGPIO_VDIFM_CFG_GNSS_3  | 0xE0; //out, data set
        vTaskDelayUntil( &xNextWakeTime, msDelay );

        rGPIO_VDIFM_CFG_GNSS_3  = rGPIO_VDIFM_CFG_GNSS_3  & 0xbf; //data clear
        vTaskDelayUntil( &xNextWakeTime, msDelay );
    }
}

void BlinkLed(unsigned int lcycles)
{
    static TaskHandle_t vBlinkTask = NULL;
    
    if( lcycles > 0 )
    {
        msDelay = lcycles;
        if( vBlinkTask == NULL )
        {
            xTaskCreate( BlinkLedTask, "BlinkLed", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, &vBlinkTask );
        }
    }
    else
    {
        if( vBlinkTask != NULL )
        {
            vTaskDelete( vBlinkTask );    
            vBlinkTask = NULL ;
        }
    }
}

