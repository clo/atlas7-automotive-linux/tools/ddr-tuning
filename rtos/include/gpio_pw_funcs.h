/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef __GPIO_PW_FUNS_H__
#define __GPIO_PW_FUNS_H__

void pw_gpio_setup( void );
void pw_set_wr (int val);
void pw_set_val (int val);
int pw_get_ack(void);
void pw_set_data_cmd(unsigned long dat, int cmd);
unsigned long pw_get_data(void);
int pw_get_cmd(void);
void pw_bus_release_b4_read(void);


#endif // __GPIO_PW_FUNS_H__

