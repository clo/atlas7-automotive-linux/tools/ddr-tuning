/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_CLKC_H
#define _SOC_CLKC_H


//#include "clkc_regs.h"
//#include "clkc_define.h"


//=====================================================================================
//  			ABPLL CONFIG                                
//=====================================================================================
//PLLOUT1 = VCO/DIVQ1
//PLLOUT2 = VCO/DIVQ2
//DIV_IN = VCO/2
//PLL      PLLDIV1   VCO/DIVQ1   DIV_IN
//cpupll  = 793      = 3172/4
//mempll  = 396.5    = 3172/8
//sys0pll = 1040     = 2080/2    DIV_IN=1040
//sys1pll = 1200     = 2400/2    DIV_IN=1200
//sys2pll = 1625     = 3250/4    DIV_IN=1625

#define PLLOUT_EN12_QDIV2    ( 0x00003511 )   //enable pllout1 and pllout2, set divQ1,2 to 2^1
#define PLLOUT_EN12_QDIV4    ( 0x00003522 )   //enable pllout1 and pllout2, set divQ1,2 to 2^2
#define PLLOUT_EN12_QDIV8    ( 0x00003533 )   //enable pllout1 and pllout2, set divQ1,2 to 2^3
#define PLLOUT_EN12_QDIV16   ( 0x00003544 )   //enable pllout1 and pllout2, set divQ1,2 to 2^4
#define PLLOUT_EN12_QDIV32   ( 0x00003555 )   //enable pllout1 and pllout2, set divQ1,2 to 2^5
#define PLLOUT_EN1_QDIV2     ( 0x00001511 )   //enable pllout1, set divQ1,2 to 2^1
#define PLLOUT_EN1_QDIV4     ( 0x00001522 )   //enable pllout1, set divQ1,2 to 2^2
#define PLLOUT_EN1_QDIV8     ( 0x00001533 )   //enable pllout1, set divQ1,2 to 2^3
#define PLLOUT_EN1_QDIV16    ( 0x00001544 )   //enable pllout1, set divQ1,2 to 2^4
#define PLLOUT_EN1_QDIV32    ( 0x00001555 )   //enable pllout1, set divQ1,2 to 2^5
#define SET_VCO2080		( 0x00000027 )   //set VCO to 2080mhz (26*(39+1)*2)
#define SET_VCO2392		( 0x0000002d )   //set VCO to 2392mhz (26*(45+1)*2)
#define SET_VCO3250		( 0x0001007c )   //set VCO to 3250mhz (26*(124+1)*2)/(1+1)
#define SET_VCO3172		( 0x0000003c )   //set VCO to 3172mhz (26*(60+1)*2)
#define SET_VCO2400		( 0x0000005f )   //set VCO to 2400mhz using CSS (95)
#define SET_VCO2396		( 0x000000ff )   //set VCO to 2396mhz using CSS (255)
#define CSS_2400        (0x2c5f9 )  //SSDIV = 709,SSMOD = 249
#define CSS_2396        (0x2c680 )  //SSDIV = 710,SSMOD = 80
#define ABPLL_BYP			( 0x00000010 )   //enable the pll bypass
#define ABPLL_RESET_EN	( 0x00000001)    //clear pll reset
#define ABPLL_SSE_EN	( 0x00001000)    //enable SSE
#define ALLCLK_EN		( 0xffffffff )   //enable all clocks
#define PRE_SEL2	( 0x00000002 )   //select sys2_QA20 (100MHz)
#define PRE_SEL3	( 0x00000003 )   //select sys1_QA20 (150MHz)
#define PRE_SEL4	( 0x00000004 )   //select sys1_QA19 (200MHz)
#define PRE_SEL5	( 0x00000005 )   //select sys1_QA18 (240MHz)
#define PRE_SEL6	( 0x00000006 )   //select sys0_QA20 (260MHz)
#define PRE_SEL7	( 0x00000007 )   //select sys1_QA17 (3000MHz)

// Xiaoning: The following defines are currently inversed to what the spec defines
// VLSI engineer will fix it later on 
#define SYS1_SEL  ( 0x3 )  
#define SYS0_SEL  ( 0x2 )  
#define XINW_SEL  ( 0x1 )   
#define XINM_SEL  ( 0x0 )   

// Xiaoning end

#define SYS0_EN  ( 0x1 )    //en sys0 DIV_QA
#define SYS1_EN  ( 0x10 )   //en sys1 DIV_QA
#define SYS2_EN  ( 0x100 )  //en sys2 DIV_QA
#define SYS3_EN  ( 0x1000 ) //en sys3 DIV_QA

#define SYS0_QA_SHIFT (0)
#define SYS1_QA_SHIFT (8)
#define SYS2_QA_SHIFT (16)
#define SYS3_QA_SHIFT (24)



#define PLLOUT_EN1  ( 0x00001000 )   //enable pllout1
#define PLLOUT_EN12 ( 0x00003000 )   //enable pllout1 and pllout2

#define SYS0_SEL  ( 0x2 )   //select sys0
#define SYS1_SEL  ( 0x3 )   //select sys1
#define SYS2_SEL  ( 0x4 )   //select sys2
#define SYS3_SEL  ( 0x5 )   //select sys3

#endif //_SOC_CLKC_H




