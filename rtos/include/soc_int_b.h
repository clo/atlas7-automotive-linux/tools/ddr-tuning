/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_INT_H
#define _SOC_INT_H
//Add macro by Weiliu

//********************************************************************************
//interrupt id macro define                                                      *
//********************************************************************************
#define INT_ID_TIMER0                   0
#define INT_ID_TIMER1                   1
#define INT_ID_TIMER2                   2
#define INT_ID_I2S1                     3
#define INT_ID_SPDIF                    4
#define INT_ID_MEDIA                    5
#define INT_ID_GRAPHIC                  6
#define INT_ID_VITERBI                  7
#define INT_ID_VSS_RD_DMAC              8
#define INT_ID_VSS_WR_DMAC              9
#define INT_ID_USB0                     10
#define INT_ID_USB1                     11
#define INT_ID_DMAC0                    12
#define INT_ID_GPIO_MEDIAM_0            13
#define INT_ID_GPIO_MEDIAM_1            14
#define INT_ID_SPI0                     15
#define INT_ID_SPI1                     16
#define INT_ID_UART0                    17
#define INT_ID_UART1                    18
#define INT_ID_UART2                    19
#define INT_ID_USP0                     20
#define INT_ID_USP1                     21
#define INT_ID_USP2                     22
#define INT_ID_SDIO23                   23
#define INT_ID_I2C0                     24
#define INT_ID_I2C1                     25
#define INT_ID_DS_SEC                   26
#define INT_ID_DS_PUB                   27
#define INT_ID_SYS2PCI                  28
#define INT_ID_CPUPMU                   29
#define INT_ID_LCD0                     30
#define INT_ID_VPP0                     31
#define INT_ID_PWRC                     32
#define INT_ID_TSC                      33
#define INT_ID_TSCADC                   34
#define INT_ID_AC97                     35
#define INT_ID_ROM                      36
#define INT_ID_TZC0                     37
#define INT_ID_SDIO01                   38
#define INT_ID_SDIO45                   39
#define INT_ID_PCICOPY                  40
#define INT_ID_NAND                     41
#define INT_ID_SECURITY                 42
#define INT_ID_GPIO_VDIFM_0             43
#define INT_ID_GPIO_VDIFM_1             44
#define INT_ID_GPIO_VDIFM_2             45
#define INT_ID_GPIO_VDIFM_3             46
#define INT_ID_GPIO_RTCM                47
#define INT_ID_PULSEC                   48
#define INT_ID_TIMER3                   49
#define INT_ID_TIMER4                   50
#define INT_ID_TIMER5                   51
#define INT_ID_SYSRTCALARM0             52
#define INT_ID_SYSRTCTIC                53
#define INT_ID_SYSRTCALARM1             54
#define INT_ID_DMAC2                    55
#define INT_ID_DMAC3                    56
#define INT_ID_A7CA                     57
#define INT_ID_TIMER_M3_WD_1ST          58
#define INT_ID_GMAC                     59
#define INT_ID_IACC                     60
#define INT_ID_G2D                      61
#define INT_ID_LCD1                     62
#define INT_ID_VIP1                     63
#define INT_ID_LVDS                     64
#define INT_ID_VPP1                     65
#define INT_ID_UART3                    66
#define INT_ID_CBUS0                    67
#define INT_ID_CBUS1                    68
#define INT_ID_UART4                    69
#define INT_ID_GMAC_PMT                 70
#define INT_ID_UART5                    71
#define INT_ID_MEDIAE0                  72
#define INT_ID_DMAC1                    73
#define INT_ID_TIMER6                   74
#define INT_ID_TIMER7                   75
#define INT_ID_TIMER8                   76
#define INT_ID_TIMER9                   77
#define INT_ID_TIMER10                  78
#define INT_ID_TIMER11                  79
#define INT_ID_DMACN_USP0               80
#define INT_ID_DMAC_NAND                81
#define INT_ID_CPU_PMU1                 82
#define INT_ID_CPU_AXIERR               83
#define INT_ID_DMACN_VIP1               84
#define INT_ID_AFECVDVIP0               85
#define INT_ID_DMAC0_SEC                86
#define INT_ID_DMAC2_SEC                87
#define INT_ID_DMAC3_SEC                88
#define INT_ID_DMAC4_SEC                89
#define INT_ID_LCD0_FRONT               90
#define INT_ID_LCD1_FRONT               91
#define INT_ID_M3_CTINTISR0             92
#define INT_ID_M3_CTINTISR1             93
#define INT_ID_CPU_CTIIRQ0              94
#define INT_ID_CPU_CTIIRQ1              95
#define INT_ID_DCU                      96
#define INT_ID_NOCFIFO                  97
#define INT_ID_SDIO67                   98
#define INT_ID_DMAC4                    99
#define INT_ID_UART6                    100
#define INT_ID_USP3                     101
#define INT_ID_NOC_AUDMSCM_INTR         102
#define INT_ID_NOC_BTM_INTR             103
#define INT_ID_NOC_CPUM_INTR            104
#define INT_ID_NOC_DDRM_INTR            105
#define INT_ID_NOC_GNSSM_INTR           106
#define INT_ID_NOC_GPUM_INTR            107
#define INT_ID_NOC_MEDIAM_INTR          108
#define INT_ID_NOC_RTCM_INTR            109
#define INT_ID_NOC_VDIFM_INTR           110
#define INT_ID_TIMER_M3_0               111
#define INT_ID_TIMER_M3_1               112
#define INT_ID_TIMER_M3_2               113
#define INT_ID_I2S_HS_0                 114
#define INT_ID_I2S_HS_1                 115

#define INT_ID_IPC_TRGT0_INIT1_INTR1    116
#define INT_ID_IPC_TRGT0_INIT2_INTR1    117
#define INT_ID_IPC_TRGT0_INIT3_INTR1    118
#define INT_ID_IPC_TRGT0_INIT1_INTR2    119
#define INT_ID_IPC_TRGT0_INIT2_INTR2    120
#define INT_ID_IPC_TRGT0_INIT3_INTR2    121
#define INT_ID_IPC_TRGT1_INIT0_INTR1    122
#define INT_ID_IPC_TRGT1_INIT2_INTR1    123
#define INT_ID_IPC_TRGT1_INIT3_INTR1    124
#define INT_ID_IPC_TRGT1_INIT0_INTR2    125
#define INT_ID_IPC_TRGT1_INIT2_INTR2    126
#define INT_ID_IPC_TRGT1_INIT3_INTR2    127
#define INT_ID_IPC_TRGT2_INIT0_INTR1    128
#define INT_ID_IPC_TRGT2_INIT1_INTR1    129
#define INT_ID_IPC_TRGT2_INIT3_INTR1    130
#define INT_ID_IPC_TRGT2_INIT0_INTR2    131
#define INT_ID_IPC_TRGT2_INIT1_INTR2    132
#define INT_ID_IPC_TRGT2_INIT3_INTR2    133
#define INT_ID_IPC_TRGT3_INIT0_INTR1    134
#define INT_ID_IPC_TRGT3_INIT1_INTR1    135
#define INT_ID_IPC_TRGT3_INIT2_INTR1    136
#define INT_ID_IPC_TRGT3_INIT0_INTR2    137
#define INT_ID_IPC_TRGT3_INIT1_INTR2    138
#define INT_ID_IPC_TRGT3_INIT2_INTR2    139

/* Some of interrupts from the point of view of SCPU (Cortex-M3): */
#define INT_ID_IPC_TRGT2_INIT0_INTR1_SCPU       122
#define INT_ID_IPC_TRGT2_INIT1_INTR1_SCPU       123
#define INT_ID_IPC_TRGT2_INIT3_INTR1_SCPU       124
#define INT_ID_IPC_TRGT2_INIT0_INTR2_SCPU       125
#define INT_ID_IPC_TRGT2_INIT1_INTR2_SCPU       126
#define INT_ID_IPC_TRGT2_INIT3_INTR2_SCPU       127

/* Some of interrupts from the point of view of KCPU (Kalimba): */
#define INT_ID_KAS_KEYHOLE_ERR_KCPU               1
#define INT_ID_KAS_DMAC_INTR_KCPU                 2
#define INT_ID_IPC_TRGT3_INIT0_INTR1_KCPU        16
#define INT_ID_IPC_TRGT3_INIT1_INTR1_KCPU        17
#define INT_ID_IPC_TRGT3_INIT2_INTR1_KCPU        18
#define INT_ID_IPC_TRGT3_INIT0_INTR2_KCPU        19
#define INT_ID_IPC_TRGT3_INIT1_INTR2_KCPU        20
#define INT_ID_IPC_TRGT3_INIT2_INTR2_KCPU        21

//********************************************************************************
//int panding macro define                                                       *
//********************************************************************************

// general macro
#define INT_PENDING(int_id)         (1<<((int_id)%32))

// specific macro
#define INT_PENDING_TIMER0          (INT_PENDING(INT_ID_TIMER0))
#define INT_PENDING_TIMER1          (INT_PENDING(INT_ID_TIMER1))
#define INT_PENDING_TIMER2          (INT_PENDING(INT_ID_TIMER2))
#define INT_PENDING_CLKC            (INT_PENDING(INT_ID_CLKC))
#define INT_PENDING_SPDIF           (INT_PENDING(INT_ID_SPDIF))
#define INT_PENDING_MEDIA           (INT_PENDING(INT_ID_MEDIA))
#define INT_PENDING_GRAPHIC         (INT_PENDING(INT_ID_GRAPHIC))
#define INT_PENDING_GPS             (INT_PENDING(INT_ID_GPS))
#define INT_PENDING_DSP             (INT_PENDING(INT_ID_DSP))
#define INT_PENDING_DSPIF           (INT_PENDING(INT_ID_DSPIF))
#define INT_PENDING_USB0            (INT_PENDING(INT_ID_USB0))
#define INT_PENDING_USB1            (INT_PENDING(INT_ID_USB1))
#define INT_PENDING_DMAC0           (INT_PENDING(INT_ID_DMAC0))
#define INT_PENDING_DMAC1           (INT_PENDING(INT_ID_DMAC1))
#define INT_PENDING_DMAC4           (INT_PENDING(INT_ID_DMAC4))
#define INT_PENDING_VIP0            (INT_PENDING(INT_ID_VIP0))
#define INT_PENDING_SPI0            (INT_PENDING(INT_ID_SPI0))
#define INT_PENDING_SPI1            (INT_PENDING(INT_ID_SPI1))
#define INT_PENDING_UART0           (INT_PENDING(INT_ID_UART0))
#define INT_PENDING_UART1           (INT_PENDING(INT_ID_UART1))
#define INT_PENDING_UART2           (INT_PENDING(INT_ID_UART2))
#define INT_PENDING_UART3           (INT_PENDING(INT_ID_UART3))
#define INT_PENDING_USP0            (INT_PENDING(INT_ID_USP0))
#define INT_PENDING_USP1            (INT_PENDING(INT_ID_USP1))
#define INT_PENDING_USP2            (INT_PENDING(INT_ID_USP2))
#define INT_PENDING_USP3            (INT_PENDING(INT_ID_USP3))
#define INT_PENDING_NOC_AUDMSCM_INTR (INT_PENDING(INT_ID_NOC_AUDMSCM_INTR))
#define INT_PENDING_NOC_BTM_INTR     (INT_PENDING(INT_ID_NOC_BTM_INTR))
#define INT_PENDING_NOC_CPUM_INTR    (INT_PENDING(INT_ID_NOC_CPUM_INTR))
#define INT_PENDING_NOC_DDRM_INTR    (INT_PENDING(INT_ID_NOC_DDRM_INTR))
#define INT_PENDING_NOC_GNSSM_INTR   (INT_PENDING(INT_ID_NOC_GNSSM_INTR))
#define INT_PENDING_NOC_GPUM_INTR    (INT_PENDING(INT_ID_NOC_GPUM_INTR))
#define INT_PENDING_NOC_MEDIAM_INTR  (INT_PENDING(INT_ID_NOC_MEDIAM_INTR))
#define INT_PENDING_NOC_RTCM_INTR    (INT_PENDING(INT_ID_NOC_RTCM_INTR))
#define INT_PENDING_NOC_VDIFM_INTR   (INT_PENDING(INT_ID_NOC_VDIFM_INTR))
#define INT_PENDING_TIMER_M3_0       (INT_PENDING(INT_ID_TIMER_M3_0))
#define INT_PENDING_TIMER_M3_1       (INT_PENDING(INT_ID_TIMER_M3_1))
#define INT_PENDING_TIMER_M3_2       (INT_PENDING(INT_ID_TIMER_M3_2))
#define INT_PENDING_I2S_HS_0         (INT_PENDING(INT_ID_I2S_HS_0))
#define INT_PENDING_I2S_HS_1         (INT_PENDING(INT_ID_I2S_HS_1))

#define INT_PENDING_IPC_TRGT0_INIT1_INTR1   (INT_PENDING(INT_ID_IPC_TRGT0_INIT1_INTR1))
#define INT_PENDING_IPC_TRGT0_INIT2_INTR1   (INT_PENDING(INT_ID_IPC_TRGT0_INIT2_INTR1))
#define INT_PENDING_IPC_TRGT0_INIT3_INTR1   (INT_PENDING(INT_ID_IPC_TRGT0_INIT3_INTR1))
#define INT_PENDING_IPC_TRGT0_INIT1_INTR2   (INT_PENDING(INT_ID_IPC_TRGT0_INIT1_INTR2))
#define INT_PENDING_IPC_TRGT0_INIT2_INTR2   (INT_PENDING(INT_ID_IPC_TRGT0_INIT2_INTR2))
#define INT_PENDING_IPC_TRGT0_INIT3_INTR2   (INT_PENDING(INT_ID_IPC_TRGT0_INIT3_INTR2))
#define INT_PENDING_IPC_TRGT1_INIT0_INTR1   (INT_PENDING(INT_ID_IPC_TRGT1_INIT0_INTR1))
#define INT_PENDING_IPC_TRGT1_INIT2_INTR1   (INT_PENDING(INT_ID_IPC_TRGT1_INIT2_INTR1))
#define INT_PENDING_IPC_TRGT1_INIT3_INTR1   (INT_PENDING(INT_ID_IPC_TRGT1_INIT3_INTR1))
#define INT_PENDING_IPC_TRGT1_INIT0_INTR2   (INT_PENDING(INT_ID_IPC_TRGT1_INIT0_INTR2))
#define INT_PENDING_IPC_TRGT1_INIT2_INTR2   (INT_PENDING(INT_ID_IPC_TRGT1_INIT2_INTR2))
#define INT_PENDING_IPC_TRGT1_INIT3_INTR2   (INT_PENDING(INT_ID_IPC_TRGT1_INIT3_INTR2))

#define INT_PENDING_SDIO23          (INT_PENDING(INT_ID_SDIO23))
#define INT_PENDING_SDIO67          (INT_PENDING(INT_ID_SDIO67))
#define INT_PENDING_I2C0            (INT_PENDING(INT_ID_I2C0))
#define INT_PENDING_I2C1            (INT_PENDING(INT_ID_I2C1))
#define INT_PENDING_CPUIF           (INT_PENDING(INT_ID_CPUIF))
#define INT_PENDING_MEMC            (INT_PENDING(INT_ID_MEMC))
#define INT_PENDING_SYS2PCI         (INT_PENDING(INT_ID_SYS2PCI))
#define INT_PENDING_CPUPMU          (INT_PENDING(INT_ID_CPUPMU))
#define INT_PENDING_LCD0            (INT_PENDING(INT_ID_LCD))
#define INT_PENDING_VPP0            (INT_PENDING(INT_ID_VPP))
#define INT_PENDING_PWRC            (INT_PENDING(INT_ID_PWRC))
#define INT_PENDING_TSC             (INT_PENDING(INT_ID_TSC))
#define INT_PENDING_TSCADC          (INT_PENDING(INT_ID_TSCADC))
#define INT_PENDING_AUDIO           (INT_PENDING(INT_ID_AUDIO))
#define INT_PENDING_ROM             (INT_PENDING(INT_ID_ROM))
#define INT_PENDING_TZC0            (INT_PENDING(INT_ID_TZC0))
#define INT_PENDING_SDIO01          (INT_PENDING(INT_ID_SDIO01))
#define INT_PENDING_SDIO45          (INT_PENDING(INT_ID_SDIO45))
#define INT_PENDING_PCICOPY         (INT_PENDING(INT_ID_PCICOPY))
#define INT_PENDING_NAND            (INT_PENDING(INT_ID_NAND))
#define INT_PENDING_SECURITY        (INT_PENDING(INT_ID_SECURITY))
#define INT_PENDING_GPIO0           (INT_PENDING(INT_ID_GPIO0))
#define INT_PENDING_GPIO1           (INT_PENDING(INT_ID_GPIO1))
#define INT_PENDING_GPIO2           (INT_PENDING(INT_ID_GPIO2))
#define INT_PENDING_GPIO3           (INT_PENDING(INT_ID_GPIO3))
#define INT_PENDING_GPIO4           (INT_PENDING(INT_ID_GPIO4))
#define INT_PENDING_PULSEC          (INT_PENDING(INT_ID_PULSEC))
#define INT_PENDING_TIMER3          (INT_PENDING(INT_ID_TIMER3))
#define INT_PENDING_TIMER4          (INT_PENDING(INT_ID_TIMER4))
#define INT_PENDING_TIMER5          (INT_PENDING(INT_ID_TIMER5))
#define INT_PENDING_SYSRTCALARM0    (INT_PENDING(INT_ID_SYSRTCALARM0))
#define INT_PENDING_SYSRTCTIC       (INT_PENDING(INT_ID_SYSRTCTIC))
#define INT_PENDING_SYSRTCALARM1    (INT_PENDING(INT_ID_SYSRTCALARM1))
#define INT_PENDING_GPSRTCALARM0    (INT_PENDING(INT_ID_GPSRTCALARM0))
#define INT_PENDING_GPSRTCALARM1    (INT_PENDING(INT_ID_GPSRTCALARM1))
#define INT_PENDING_GPSRTCTIC       (INT_PENDING(INT_ID_GPSRTCTIC))
#define INT_PENDING_TIMER_M3_WD_1ST (INT_PENDING(INT_ID_TIMER_M3_WD_1ST))
#define INT_PENDING_GMAC            (INT_PENDING(INT_ID_GMAC))
#define INT_PENDING_IACC            (INT_PENDING(INT_ID_IACC))
#define INT_PENDING_GPIODSP         (INT_PENDING(INT_ID_GPIODSP))
#define INT_PENDING_G2D             (INT_PENDING(INT_ID_G2D))
#define INT_PENDING_LCD1            (INT_PENDING(INT_ID_LCD1))
#define INT_PENDING_VIP1            (INT_PENDING(INT_ID_VIP1))
#define INT_PENDING_LVDS            (INT_PENDING(INT_ID_LVDS))
#define INT_PENDING_VPP1            (INT_PENDING(INT_ID_VPP1))
#define INT_PENDING_UART3           (INT_PENDING(INT_ID_UART3))
#define INT_PENDING_CBUS0           (INT_PENDING(INT_ID_CBUS0))
#define INT_PENDING_CBUS1           (INT_PENDING(INT_ID_CBUS1))
#define INT_PENDING_UART4           (INT_PENDING(INT_ID_UART4))
#define INT_PENDING_CVE             (INT_PENDING(INT_ID_CVE))
#define INT_PENDING_CVD             (INT_PENDING(INT_ID_CVD))
#define INT_PENDING_MEDIAE0         (INT_PENDING(INT_ID_MEDIAE0))
#define INT_PENDING_DMAC1           (INT_PENDING(INT_ID_DMAC1))
#define INT_PENDING_TIMER6          (INT_PENDING(INT_ID_TIMER6))
#define INT_PENDING_TIMER7          (INT_PENDING(INT_ID_TIMER7))
#define INT_PENDING_TIMER8          (INT_PENDING(INT_ID_TIMER8))
#define INT_PENDING_TIMER9          (INT_PENDING(INT_ID_TIMER9))
#define INT_PENDING_TIMER10         (INT_PENDING(INT_ID_TIMER10))
#define INT_PENDING_TIMER11         (INT_PENDING(INT_ID_TIMER11))
#define INT_PENDING_DMACN_USP0      (INT_PENDING(INT_ID_DMACN_USP0))
#define INT_PENDING_DMAC_NAND       (INT_PENDING(INT_ID_DMAC_NAND))
#define INT_PENDING_CPU_PMU1        (INT_PENDING(INT_ID_CPU_PMU1))
#define INT_PENDING_DMACN_VIP0      (INT_PENDING(INT_ID_DMACN_VIP0))
#define INT_PENDING_DMACN_VIP1      (INT_PENDING(INT_ID_DMACN_VIP1))
#define INT_PENDING_DCU             (INT_PENDING(INT_ID_DCU))
#define INT_PENDING_NOCFIFP         (INT_PENDING(INT_ID_NOCFIFO))

#define INT_PENDING_INT_ID_AFECVDVIP0       (INT_PENDING(INT_ID_AFECVDVIP0))
#define INT_PENDING_INT_ID_AFECVDVIP1       (INT_PENDING(INT_ID_AFECVDVIP1))
#define INT_PENDING_INT_ID_AFECVDVIP2       (INT_PENDING(INT_ID_AFECVDVIP2))
#define INT_PENDING_INT_ID_AFECVDVIP3       (INT_PENDING(INT_ID_AFECVDVIP3))
#define INT_PENDING_INT_ID_DMACN_SPI0_W     (INT_PENDING(INT_ID_DMACN_SPI0_W))
#define INT_PENDING_INT_ID_DMACN_SPI0_R     (INT_PENDING(INT_ID_DMACN_SPI0_R))
#define INT_PENDING_INT_ID_TZC1             (INT_PENDING(INT_ID_TZC1))
#define INT_PENDING_INT_ID_M3_CTINTISR0     (INT_PENDING(INT_ID_M3_CTINTISR0))
#define INT_PENDING_INT_ID_M3_CTINTISR1     (INT_PENDING(INT_ID_M3_CTINTISR1))
#define INT_PENDING_INT_ID_CPU_CTIIRQ0      (INT_PENDING(INT_ID_CPU_CTIIRQ0))
#define INT_PENDING_INT_ID_CPU_CTIIRQ1      (INT_PENDING(INT_ID_CPU_CTIIRQ1))

/***************************************************************************************\
| old test vector style
\***************************************************************************************/
#define INT_MASK_ALL                             0xffffffff

#define DMA_CH0_INT                              0x00000001
#define DMA_CH1_INT                              0x00000002
#define DMA_CH2_INT                              0x00000004
#define DMA_CH3_INT                              0x00000008
#define DMA_CH4_INT                              0x00000010
#define DMA_CH5_INT                              0x00000020
#define DMA_CH6_INT                              0x00000040
#define DMA_CH7_INT                              0x00000080
#define DMA_CH8_INT                              0x00000100
#define DMA_CH9_INT                              0x00000200
#define DMA_CH10_INT                             0x00000400
#define DMA_CH11_INT                             0x00000800
#define DMA_CH12_INT                             0x00001000
#define DMA_CH13_INT                             0x00002000
#define DMA_CH14_INT                             0x00004000
#define DMA_CH15_INT                             0x00008000
#define DMA_TRGT0_SEC_TR_RESP_ERR_INT            0x00010000
#define DMA_TRGT0_NON_SEC_TR_RESP_ERR_INT        0x00020000
#define DMA_TRGT1_SEC_TR_RESP_ERR_INT            0x00040000
#define DMA_TRGT1_NON_SEC_TR_RESP_ERR_INT        0x00080000
#define DMA_TRGT2_SEC_TR_RESP_ERR_INT            0x00100000
#define DMA_TRGT2_NON_SEC_TR_RESP_ERR_INT        0x00200000
#define DMA_TRGT3_SEC_TR_RESP_ERR_INT            0x00400000
#define DMA_TRGT3_NON_SEC_TR_RESP_ERR_INT        0x00800000

#define INT_ENABLE0(x)              rINT_RISC_MASK0_SET = (x)
#define INT_ENABLE1(x)              rINT_RISC_MASK1_SET = (x)
#define INT_ENABLE2(x)              rINT_RISC_MASK2_SET = (x)

#define INT_DISABLE0(x)             rINT_RISC_MASK0_CLR = (x)
#define INT_DISABLE1(x)             rINT_RISC_MASK1_CLR = (x)
#define INT_DISABLE2(x)             rINT_RISC_MASK2_CLR = (x)

#define INT_SET_FIQ0(x)             rINT_RISC_LEVEL0_SET = (x)
#define INT_SET_FIQ1(x)             rINT_RISC_LEVEL1_SET = (x)
#define INT_SET_FIQ2(x)             rINT_RISC_LEVEL2_SET = (x)

#define INT_SET_IRQ0(x)             rINT_RISC_LEVEL0_CLR = (x)
#define INT_SET_IRQ1(x)             rINT_RISC_LEVEL1_CLR = (x)
#define INT_SET_IRQ2(x)             rINT_RISC_LEVEL2_CLR = (x)

#define GetSysRTC()                 rINT_SYSRTC_CNT

#endif //_SOC_INT_H
