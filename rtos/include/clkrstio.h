/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef __CLKRSTIO_H__
#define __CLKRSTIO_H__

#include "types.h"

extern DWORD g_IOClk;

typedef enum 
{
    MODULE_NAND = 1,
    MODULE_SD0,
    MODULE_SD2,
    MODULE_QSPI,
    MODULE_UART1,
    MODULE_USB1,
    MODULE_TIMER,
    MODULE_DISCRETIX,
    MODULE_OTP
} CLKMODULES;



void StartPLL(void);
void EnableModuleClk(CLKMODULES module, BOOL enable);
void ResetModule(CLKMODULES module);
void SetModuleIO(CLKMODULES module);
BOOL NANDSwitchClkSrc(void);
VOID SPISwitchClk(DWORD dwParam);

#define SPI_DEFAULT_CLK         (12000000)           //12MHz default, it is 192MHz/16


#endif

