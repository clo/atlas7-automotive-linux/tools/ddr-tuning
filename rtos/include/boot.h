/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/*****************************************************************************/
// Module Name: 
//    boot.h
//
// Abstract: 
//    defines for NAND boot, SD boot and UART boot.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _BOOT_H_
#define _BOOT_H_

#include "soc.h"
#include "debug.h"

#define UART_BAUD_RATE          (115200)



/*****************************/
// SRAM address definition
/*****************************/
#define SRAM_CODE_SIZE_LIMIT        (54*1024)       //SRAM code buffer including: parameter, VRL, debug cert, image1
#define SECTOR_BUFFER_BASE          (BUFFER_SRAM_BASE+SRAM_CODE_SIZE_LIMIT) //SRAM buffer for sector read
#define SRAM_USBBUFFER_BASE             (BUFFER_SRAM_BASE+87*1024)      //87KB offset
#define SRAM_USBBUFFER_SIZE             (2*1024)
#define DX_WORKING_BUFFER_ADDR          (SRAM_USBBUFFER_BASE+SRAM_USBBUFFER_SIZE)//89KB offset
#define DX_WORKING_BUFFER_SIZE          (8*1024)
#define SRAM_NANDINFO_BASE    (DX_WORKING_BUFFER_ADDR+DX_WORKING_BUFFER_SIZE)   //reserve 1KB for NAND info //97KB offset
#define SRAM_NANDINFO_SIZE    (1*1024)
#define BOOTLOADERIF_ADDRESS            (SRAM_NANDINFO_BASE+SRAM_NANDINFO_SIZE)            //bootloader interface is a structrue shared between ROMCODE and bootloader //98KB offset
#define BOOTLOADERIF_SIZE            (1*1024) 
#define SRAM_DIAG_BASE          (BOOTLOADERIF_ADDRESS+BOOTLOADERIF_SIZE)    //99KB offset
#define SRAM_DIAG_SIZE          (13*1024)
#define MMU_DEFAULT_ADDRESS             (SRAM_DIAG_BASE+SRAM_DIAG_SIZE)         //Default MMU table address, 16KB align    //112KB offset

#define BOOT_VRL_BASE (BUFFER_SRAM_BASE+sizeof(BOOTPARAMETER))


#include "diagnose.h"
/*******************************/

#ifdef PLATFORM_FPGA 
#define SOC_IO_CLK				(20*1000*1000) 
#else
#define SOC_IO_CLK				(26*1000*1000) 
#endif

#define SOC_IO_24M_CLK				(24*1000*1000) 
#define SOC_IO_CLKHS				(150*1000*1000) 

#define MMU_PTE_PAGE_SIZE               0x100000         //1*1024*1024
#define MMU_PTE_TABLE_SIZE              (16*1024)

//#define MMU_DEFAULT_ADDRESS             (SRAM_BASE + 128*1024 - 16*1024)         //Default MMU table address, 16KB align


#define MMU_DEFAULT_ATTR_NCNS           (0x0)           //non-cacheable, non-shareable
#define MMU_DEFAULT_ATTR_CNS           (0x40)           //cacheable, write allocate, non-shareable
#define MMU_DEFAULT_ADDR_ATTR         (MMU_DEFAULT_ADDRESS | MMU_DEFAULT_ATTR_NCNS)           //The default address to store translation table and attributes, non-cacheable, non-shareable

                                                                                                                //placed at offset 98KB

#define BOOT_IMAGE_UPDATE_FLAG   0x424F4F54   //"BOOT"

#define EPSIG         0x55AAAA55

#define BOOT_PARAM_MAGIC    (0x6E3C5A7D)

typedef struct BOOTLOADER_INTERFACE
{
    DWORD returnAdrs;          //romcode->bootloader
    DWORD loadIdx;            //bootloader->romcode, the index of image for next loading
    DWORD launchAdrs;          //bootloader->romcode
    DWORD updateFlag;          //romcode->bootloader
    DWORD dwSig;
    DWORD dwBootMode;
}BOOTLOADER_INTERFACE;


///////////For secure boot //////////////////////////
/*
 * The size of the signature area is set to 1024,
 * current the signature is about 46~48 bytes,
 * so two signatures will not exceed 100B.
 */
#define HASH_SIZE       32       //hash256 & ECC256, byte number is 32 
 

#define MAGIC_SIG   0x53435352    //'SCSR'
#define IMG1_DIRECT_LAUNCH          0x4C443149  //"I1DL"



typedef struct PARAM_HEADER
{
    unsigned short head_len;    /* bootloader header length, in bytes*/
    unsigned short img1_len;    /* first part image length, in bytes */
    unsigned long  fullimg_len;    /* total bootloader length, in bytes. when use image1 direct launch mode, this is the image1 length */
    unsigned short sig1_ofs;    /* signature 1 byte offset */
    unsigned short sig2_ofs;    /* signature 2 byte offset */
    unsigned short para_ofs;    /* parameters area byte offset */
    unsigned short pubk_ofs;    /* public key offset, 0 if no public key stored */
    unsigned long   magicsig;  //magic number to verify valid image header
    unsigned long   bootflag;   //0x4C443149 for image1 direct launch mode, other value for normal mode
    unsigned long   img1loadaddr;   //image1 load address, valid when use image1 direct launch mode
    unsigned long   img1launchaddr; //image1 launch address, valid when use image1 direct launch mode
}PARAM_HEADER;


typedef struct BOOTMEDIA_INFO
{
    DWORD dwSectorSize;
}BOOTMEDIA_INFO;

typedef BOOL (*BOOTMEDIAINIT) (void* arg);
typedef BOOL (*BOOTMEDIAGETINFO) (BOOTMEDIA_INFO *Info);
typedef BOOL (*BOOTMEDIAPARAMHANDLER) (void* arg);
typedef BOOL (*BOOTMEDIAREAD) (DWORD dwStartSec, DWORD dwSecs, PBYTE pbBuffer);
typedef BOOL (*BOOTMEDIADEINIT) (void* arg);

typedef struct BOOTMEDIA_DRIVER
{
    BOOTMEDIAINIT Init;
    BOOTMEDIAGETINFO GetInfo;
    BOOTMEDIAPARAMHANDLER ParamHandler;
    BOOTMEDIAREAD Read;
    BOOTMEDIADEINIT Deinit;
}BOOTMEDIA_DRIVER;

//Boot mode has 3 boot stages
//Main stage is used as norml boot
//when main stage boot fail, use second level boot mode (SD2)
//Then 3rd level boot mode if 2nd failed (USB1)
typedef enum  BOOTMODESTAGE{
    BMS_MAIN = 1,
    BMS_2ND = 2,
    BMS_3RD = 3,
    BMS_UNDEF 
} BOOTMODESTAGE;

typedef struct BOOTPARAMETER
{
    DWORD dwMagicNum;
    DWORD dwCustom;
    DWORD dwCertOffset;
    DWORD dwCertSize;
    DWORD dwReserved;
}BOOTPARAMETER;

#define INIT_SYS_REG_DELAY          0xFF44CC00
#define INIT_SYS_REG_BIT_CLEAR      0xFF44CC0F
#define INIT_SYS_REG_BIT_SET        0xFF44CCF0
#define INIT_SYS_REG_DATA_COPY      0xFF44CC33
#define INIT_SYS_REG_IF             0xFF44CC3F
#define INIT_SYS_REG_WHILE          0xFF44CCF3
#define INIT_SYS_REG_END            0xFF44CCFF
#define INIT_SYS_REG_INIT_MMU       0xFF44C555
#define INIT_SYS_REG_MMU_UPDATE       0xFF44CA5A
#define INIT_SYS_REG_WHILE1       0xFF44C666
#define INIT_SYS_REG_PRINT_ADDR       0xFF44C888



#define MAX_COMMAND_FILE_LEN        (3*1024/sizeof(DWORD))

#define SECURE_BOOT_IMG_PART     0       //first part of boot image
#define SECURE_BOOT_IMG_FULL     1       //whole boot image
#define SECURE_BOOT_IMG_DIRECT     2       //Image1 for direct launch





#define TIMEOUT_INFINITE  ((DWORD)-1)


#define BM_SDR_NAND     0x00
#define BM_USB			0x01
#define BM_SD2			0x02
#define BM_SD0			0x03
#define BM_NAND_DIRECT		0x04    //reserved
#define BM_SPI			0x05
#define BM_DDR_NAND     0x06
#define BM_SD0_BP      	0x07
#define BM_SERIAL      	0x08   //not defined by boot pins



#define GET_BOOTMODE() (rRESET_LATCH_REGISTER & 0x07)

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif




#ifdef __cplusplus
extern "C" {
#endif


BOOL uartConnectSetup(DWORD dwBaudRate);
DWORD getUartSetupDelay(DWORD u_baudrate);
void Launch(unsigned long jump_addr);
void DirectLaunch(unsigned long jump_addr);
void sysRegInit(DWORD *u_pCmd);
BOOL verify_launch(DWORD u_imgIdx, DWORD u_launchAdrs);
BOOL paramHeaderVerify(PARAM_HEADER *u_pHead, DWORD dwSectorSize);
void cacheLineFlush(DWORD u_virtualAdrs, DWORD u_length, BOOL u_isDataCache);
void cacheLineInvalidate(DWORD u_virtualAdrs, DWORD u_length);
void ICacheFlush(void);
void DisableMMU(void);
void dCacheRefresh(DWORD u_Flag);

void BytesCpy(BYTE *pDst,BYTE *pSrc,DWORD size);
void OSTimerInit(DWORD sys_clk, DWORD dwLow);
DWORD OSTGetCurrentTick (void);
void usWait(DWORD usNum);

void WatchDogResetSystem(void);
void SetTimeOut(DWORD us);   //us
DWORD GetSpendTime(void);
BOOL IsTimeOut(void);


//boot media driver interface
BOOL SDMMC_IF_Init(void* RealBM);
BOOL SDMMC_IF_Read(DWORD dwStartSec, DWORD dwSecs, PBYTE pbBuffer);
BOOL SDMMC_IF_GetInfo(BOOTMEDIA_INFO *Info);
BOOL SDMMC_IF_Deinit(void* arg);
BOOL SDMMC_IF_ParamHandler(void* arg);

BOOL NAND_IF_Init(void* RealBM);
BOOL NAND_IF_Deinit(void* arg);
BOOL NAND_IF_GetInfo(BOOTMEDIA_INFO *Info);
BOOL NAND_IF_ParamHandler(void* arg);
BOOL NAND_IF_Read(DWORD dwStartSec, DWORD dwSecs, PBYTE pbBuffer);

BOOL USB_IF_Init(void* RealBM);
BOOL USB_IF_Deinit(void* arg);
BOOL USB_IF_GetInfo(BOOTMEDIA_INFO *Info);
BOOL USB_IF_ParamHandler(void* arg);
BOOL USB_IF_Read(DWORD dwStartSec, DWORD dwSecs, PBYTE pbBuffer);

BOOL SPI_IF_Init(void* RealBM);
BOOL SPI_IF_Deinit(void* arg);
BOOL SPI_IF_GetInfo(BOOTMEDIA_INFO *Info);
BOOL SPI_IF_ParamHandler(void* arg);
BOOL SPI_IF_Read(DWORD dwStartSec, DWORD dwSecs, PBYTE pbBuffer);







BOOL SD0_BOOT(void);
BOOL SD2_BOOT(void);
BOOL SD0_BOOT_BP(void);


BOOL NAND_BOOT_SDR(void);
BOOL NAND_BOOT_DDR(void);
BOOL NAND_BOOT_EFUSE(void);
BOOL SPI_BOOT(void);
BOOL USB_BOOT(void);
BOOL UART_Boot(void);
extern BOOL isSecureBootEnable(void);




DWORD eFuseBaudrate(void);
BOOL OTPIsUsbFullSpeed(void);
BYTE OTPSpiMode(void);

BOOL eFuseIsTLC(void);
BOOL OTPNandInfoIsUseRandomizer(void);
BOOL GetOTP24MHz(void);
BOOL GetOTPDualKey(void);
DWORD iobrgRead(DWORD u_adrs);
BOOL iobrgWrite(DWORD u_adrs, DWORD dwValue);



void ARM_DSB(void);
void ARM_ISB(void);
void SystemError(void);

void StartupInitMMU (void);
BOOL BootSequence(void);
BOOL BootCommon(const BOOTMEDIA_DRIVER *DeviceDriver);





#ifdef __cplusplus
}
#endif

#endif //_BOOT_H_


