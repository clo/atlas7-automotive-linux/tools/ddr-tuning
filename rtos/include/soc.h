/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    soc.h
//
// Abstract: 
//    define system address space.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _SOC_H_
#define _SOC_H_


#define SDRAM_DOMAIN_BASE					0x40000000
#define QSPI_XIP_DOMAIN_BASE              	0x20000000
#define RTCM_DOMAIN_BASE                  	0x18800000
#define CGUM_DOMAIN_BASE                  	0x18600000
#define GNSSM_DOMAIN_BASE                 	0x18000000
#define MEDIAM_DOMAIN_BASE                	0x17000000
#define VDIFM_DOMAIN_BASE                 	0x13100000
#define GPUM_DOMAIN_BASE                  	0x13000000
#define BTM_DOMAIN_BASE                   	0x11000000
#define AUDMSCM_DOMAIN_BASE              	0x10C00000
#define DDRM_DOMAIN_BASE                  	0x10800000
#define CPUM_DOMAIN_BASE                  	0x10200000


//CPUM
#define _CA7ROM_MODULE_BASE			        0x00000000
#define SRAM_BASE          					0x04000000  //Size: 192KB
#define BUFFER_SRAM_BASE					SRAM_BASE
#define M3_SRAM_BASE                            0x188A0000

#define _CORESIGHT_MODULE_BASE    			0x10000000
#define _CPU_SCU_MODULE_BASE      			0x10300000
#define _INTC_MODULE_BASE         			(CPUM_DOMAIN_BASE+0x20000)
#define _RISC_MODULE_BASE         			(CPUM_DOMAIN_BASE+0x30000)

//DDRM
#define _MEMORY_MODULE_BASE			        (DDRM_DOMAIN_BASE)
#define _DDRPHY_MODULE_BASE       			(DDRM_DOMAIN_BASE+0x10000)
//AUDMSCM
#define _KAS_MODULE_BASE          	(AUDMSCM_DOMAIN_BASE)
#define _CODEC_MODULE_BASE        	(AUDMSCM_DOMAIN_BASE+0x100000)
#define _USP0_MODULE_BASE         	(AUDMSCM_DOMAIN_BASE+0x120000)
#define _USP1_MODULE_BASE         	(AUDMSCM_DOMAIN_BASE+0x130000)
#define _USP2_MODULE_BASE         	(AUDMSCM_DOMAIN_BASE+0x140000)
#define _DMAC2_MODULE_BASE        	(AUDMSCM_DOMAIN_BASE+0x150000)
#define _DMAC3_MODULE_BASE        	(AUDMSCM_DOMAIN_BASE+0x160000)
#define _TSCIF_MODULE_BASE        	(AUDMSCM_DOMAIN_BASE+0x180000)
#define _PULSEC_MODULE_BASE       	(AUDMSCM_DOMAIN_BASE+0x190000)
#define _CVD_MODULE_BASE          	(AUDMSCM_DOMAIN_BASE+0x1B0000)
#define _OST_MODULE_BASE			(AUDMSCM_DOMAIN_BASE+0x1C0000)
#define _LVDS_MODULE_BASE     		(AUDMSCM_DOMAIN_BASE+0x210000)
#define _DVM_MODULE_BASE    		(AUDMSCM_DOMAIN_BASE+0x220000)
#define _IOC_MODULE_BASE    		(AUDMSCM_DOMAIN_BASE+0x240000)
#define _RSC_MODULE_BASE			(AUDMSCM_DOMAIN_BASE+0x250000)
#define _PWRC_MODULE_BASE               (0x188D0000)
#define _IOC_RTC_MODULE_BASE            (0x18880000)

//BTM
#define _A7CA_MODULE_BASE     		(BTM_DOMAIN_BASE)
#define _DMAC4_MODULE_BASE_PHYSICAL			(BTM_DOMAIN_BASE+0x2000)

//GPUM
#define _SGX_MODULE_BASE    		0x12000000

#define _SDR_MODULE_BASE    		(GPUM_DOMAIN_BASE+0x10000)

//VDIFM
#define _LCD0_MODULE_BASE     		(VDIFM_DOMAIN_BASE)
#define _VPP0_MODULE_BASE     		(VDIFM_DOMAIN_BASE+0x10000)
#define _LCD1_MODULE_BASE     		(VDIFM_DOMAIN_BASE+0x20000)
#define _VPP1_MODULE_BASE     		(VDIFM_DOMAIN_BASE+0x30000)
#define _CAM1_MODULE_BASE         	(VDIFM_DOMAIN_BASE+0x60000)

#define _SYS2PCI_MODULE_BASE  		(VDIFM_DOMAIN_BASE+0xE0000)
#define _ROM_MODULE_BASE    		(VDIFM_DOMAIN_BASE+0xF0000)

#define _DCU_MODULE_BASE          	(VDIFM_DOMAIN_BASE+0x120000)
#define _IPC_MODULE_BASE          	(VDIFM_DOMAIN_BASE+0x140000)

#define _PCI_MODULE_BASE    		0x14000000            //(SD2/3/4/5/6/7)

#define _SD2_MODULE_BASE    		(_PCI_MODULE_BASE+0x200000)
#define _SD3_MODULE_BASE    		(_PCI_MODULE_BASE+0x300000)
#define _SD4_MODULE_BASE    		(_PCI_MODULE_BASE+0x400000)
#define _SD5_MODULE_BASE    		(_PCI_MODULE_BASE+0x500000)
#define _SD6_MODULE_BASE    		(_PCI_MODULE_BASE+0x600000)
#define _SD7_MODULE_BASE    		(_PCI_MODULE_BASE+0x700000)

#define _PCI_COPY_BASE      		0x14900000
#define _PCI_ROM_BASE       		0x14A00000  //Ask Shahar what is this?

//MEDIAM
#define _PCI2_MODULE_BASE     		0x16000000            //(SD0/1)

#define _SD0_MODULE_BASE     		(_PCI2_MODULE_BASE)
#define _SD1_MODULE_BASE    		(_PCI2_MODULE_BASE+0x100000)
#define _SD0_PCI_CONFIG_BASE  		(_PCI2_MODULE_BASE+0x800000)

#define _I2C0_MODULE_BASE     		(MEDIAM_DOMAIN_BASE+0x20000)
#define _I2C1_MODULE_BASE     		(MEDIAM_DOMAIN_BASE+0x30000)

#define _NAND_MODULE_BASE     		(MEDIAM_DOMAIN_BASE+0x50000)

#define _USBOTG0_MODULE_BASE      	(MEDIAM_DOMAIN_BASE+0x60000)
#define _USBOTG1_MODULE_BASE      	(MEDIAM_DOMAIN_BASE+0x70000)

//GNSS
#define _DMAC0_MODULE_BASE        	(GNSSM_DOMAIN_BASE)
#define _UART0_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x10000)
#define _UART1_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x20000)
#define _UART2_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x30000)
#define _UART3_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x40000)
#define _UART4_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x50000)
#define _UART5_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x60000)

#define _SPI1_MODULE_BASE        	(GNSSM_DOMAIN_BASE+0x200000)

#define _SEC_SECURE_MODULE_BASE			   (GNSSM_DOMAIN_BASE+0x240000)

//CGUM
#define _CLOCK_MODULE_BASE			     	(CGUM_DOMAIN_BASE+0x20000)

//RTCM
#define _CPU2RTC_IO_BRIDGE				   	(RTCM_DOMAIN_BASE+0x40000)
#define _QSPI_MODULE_BASE			     	(RTCM_DOMAIN_BASE+0xB0000)
#define _RETAIN_MODULE_BASE					(RTCM_DOMAIN_BASE+0xD0000)

#define _QSPI_XIP_BASE	      				0x20000000        //128M

//Discretix
#define _DISCRETIX_MODULE_BASE          (0x18240000)
#define rDISCRETIX_DCU         *((volatile unsigned *)(0x18240A64))



//OTP
#define _OTP_BOOTSTRAP_BASE                 (0x1826001C)
//#define rOTP_BR_STATUS                          *((volatile unsigned *)0x18260000)

//RTC Bootstrap
#define _PWRC_FASTCLK_BASE          (0x188d0000)
#define rRTC_BOOTSTRAP0         *((volatile unsigned *)(_PWRC_FASTCLK_BASE + 0x30))
#define rRTC_SP10         *((volatile unsigned *)(_PWRC_FASTCLK_BASE + 0x24))


/***************************************************************************************\
| PWRC fast clock
\***************************************************************************************/
#define rPWRC_SCRATCH_PAD1                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x0)        //used for custom boot mode
#define rPWRC_SCRATCH_PAD2                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x4)
#define rPWRC_SCRATCH_PAD3                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x8)
#define rPWRC_SCRATCH_PAD4                 *((volatile unsigned *)_PWRC_MODULE_BASE+0xC)
#define rPWRC_SCRATCH_PAD5                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x10)
#define rPWRC_SCRATCH_PAD6                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x14)
#define rPWRC_SCRATCH_PAD7                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x18)
#define rPWRC_SCRATCH_PAD8                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x1C)
#define rPWRC_SCRATCH_PAD9                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x20)
#define rPWRC_SCRATCH_PAD10                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x24)
#define rPWRC_SCRATCH_PAD11                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x28)
#define rPWRC_SCRATCH_PAD12                 *((volatile unsigned *)_PWRC_MODULE_BASE+0x2C)


#define rRESET_LATCH_REGISTER               *((volatile unsigned *)0x1023001C)

/***************************************************************************************\
| General-purposed I/O registers offset 0xB0120000
\***************************************************************************************/
//#define rGPIO0_PAD_EN				*((volatile unsigned *)(_GPIO_MODULE_BASE + 0x0084 +0x000))
//#define rGPIO1_PAD_EN				*((volatile unsigned *)(_GPIO_MODULE_BASE + 0x0084 +0x100))
//#define rGPIO2_PAD_EN				*((volatile unsigned *)(_GPIO_MODULE_BASE + 0x0084 +0x200))
//#define rGPIO3_PAD_EN               *((volatile unsigned *)(_GPIO_MODULE_BASE + 0x0084 +0x300))
//#define rGPIO4_PAD_EN               *((volatile unsigned *)(_GPIO_MODULE_BASE + 0x0084 +0x400))

/***************************************************************************************\
| OS timer registers offset 0x90050000 
\***************************************************************************************/
#define rTIMER_32COUNTER_0_CTRL                 *((volatile unsigned *)_OST_MODULE_BASE)
#define rTIMER_32COUNTER_1_CTRL                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x0004))
#define rTIMER_32COUNTER_2_CTRL                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x0008))
#define rTIMER_32COUNTER_3_CTRL                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x000c))
#define rTIMER_32COUNTER_4_CTRL                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x0010))
#define rTIMER_32COUNTER_5_CTRL                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x0014))

#define rTIMER_MATCH_0                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x0018))
#define rTIMER_MATCH_1                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x001c))
#define rTIMER_MATCH_2                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x0020))
#define rTIMER_MATCH_3                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x0024))
#define rTIMER_MATCH_4                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x0028))
#define rTIMER_MATCH_5                          *((volatile unsigned *)(_OST_MODULE_BASE + 0x002c))

#define rTIMER_32COUNTER_0_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x0030))
#define rTIMER_32COUNTER_1_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x0034))
#define rTIMER_32COUNTER_2_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x0038))
#define rTIMER_32COUNTER_4_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x003c))
#define rTIMER_32COUNTER_3_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x0040))
#define rTIMER_32COUNTER_5_MATCH_NUM            *((volatile unsigned *)(_OST_MODULE_BASE + 0x0044))


#define rTIMER_COUNTER_0                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x0048))
#define rTIMER_COUNTER_1                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x004c))
#define rTIMER_COUNTER_2                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x0050))
#define rTIMER_COUNTER_3                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x0054))
#define rTIMER_COUNTER_4                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x0058))
#define rTIMER_COUNTER_5                        *((volatile unsigned *)(_OST_MODULE_BASE + 0x005c))

#define rTIMER_INTR_STATUS                      *((volatile unsigned *)(_OST_MODULE_BASE + 0x0060))
#define rTIMER_WATCHDOG_EN                      *((volatile unsigned *)(_OST_MODULE_BASE + 0x0064))

#define rTIMER_64COUNTER_CTRL              *((volatile unsigned *)(0x10dc0000 + 0x8068))
#define rTIMER_64COUNTER_LO                *((volatile unsigned *)(0x10dc0000 + 0x806c))
#define rTIMER_64COUNTER_HI                *((volatile unsigned *)(0x10dc0000 + 0x8070))
#define rTIMER_64COUNTER_LOAD_LO           *((volatile unsigned *)(0x10dc0000 + 0x8074))
#define rTIMER_64COUNTER_LOAD_HI           *((volatile unsigned *)(0x10dc0000 + 0x8078))
#define rTIMER_64COUNTER_RLATCHED_LO       *((volatile unsigned *)(0x10dc0000 + 0x807c))
#define rTIMER_64COUNTER_RLATCHED_HI       *((volatile unsigned *)(0x10dc0000 + 0x8080))


#define rTIMER_32COUNTER_6_CTRL              *((volatile unsigned *)(_OST_MODULE_BASE + 0x8000))
#define rTIMER_32COUNTER_7_CTRL              *((volatile unsigned *)(_OST_MODULE_BASE + 0x8004))
#define rTIMER_32COUNTER_8_CTRL              *((volatile unsigned *)(_OST_MODULE_BASE + 0x8008))
#define rTIMER_32COUNTER_9_CTRL              *((volatile unsigned *)(_OST_MODULE_BASE + 0x800c))
#define rTIMER_32COUNTER_10_CTRL             *((volatile unsigned *)(_OST_MODULE_BASE + 0x8010))
#define rTIMER_32COUNTER_11_CTRL             *((volatile unsigned *)(_OST_MODULE_BASE + 0x8014))

#define rTIMER_MATCH_6                       *((volatile unsigned *)(_OST_MODULE_BASE + 0x8018))
#define rTIMER_MATCH_7                       *((volatile unsigned *)(_OST_MODULE_BASE + 0x801c))
#define rTIMER_MATCH_8                       *((volatile unsigned *)(_OST_MODULE_BASE + 0x8020))
#define rTIMER_MATCH_9                       *((volatile unsigned *)(_OST_MODULE_BASE + 0x8024))
#define rTIMER_MATCH_10                      *((volatile unsigned *)(_OST_MODULE_BASE + 0x8028))
#define rTIMER_MATCH_11                      *((volatile unsigned *)(_OST_MODULE_BASE + 0x802c))

#define rTIMER_32COUNTER_6_MATCH_NUM         *((volatile unsigned *)(_OST_MODULE_BASE + 0x8030))
#define rTIMER_32COUNTER_7_MATCH_NUM         *((volatile unsigned *)(_OST_MODULE_BASE + 0x8034))
#define rTIMER_32COUNTER_8_MATCH_NUM         *((volatile unsigned *)(_OST_MODULE_BASE + 0x8038))
#define rTIMER_32COUNTER_9_MATCH_NUM         *((volatile unsigned *)(_OST_MODULE_BASE + 0x803c))
#define rTIMER_32COUNTER_10_MATCH_NUM        *((volatile unsigned *)(_OST_MODULE_BASE + 0x8040))
#define rTIMER_32COUNTER_11_MATCH_NUM        *((volatile unsigned *)(_OST_MODULE_BASE + 0x8044))

#define rTIMER_COUNTER_6                     *((volatile unsigned *)(_OST_MODULE_BASE + 0x8048))
#define rTIMER_COUNTER_7                     *((volatile unsigned *)(_OST_MODULE_BASE + 0x804c))
#define rTIMER_COUNTER_8                     *((volatile unsigned *)(_OST_MODULE_BASE + 0x8050))
#define rTIMER_COUNTER_9                     *((volatile unsigned *)(_OST_MODULE_BASE + 0x8054))
#define rTIMER_COUNTER_10                    *((volatile unsigned *)(_OST_MODULE_BASE + 0x8058))
#define rTIMER_COUNTER_11                    *((volatile unsigned *)(_OST_MODULE_BASE + 0x805c))

#define rTIMER_INTR_STATUS_1                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x8060))
#define rTIMER_WATCHDOG_EN_1                 *((volatile unsigned *)(_OST_MODULE_BASE + 0x8064))

#define rTIMER_64COUNTER_CTRL_1              *((volatile unsigned *)(_OST_MODULE_BASE + 0x8068))
#define rTIMER_64COUNTER_LO_1                *((volatile unsigned *)(_OST_MODULE_BASE + 0x806c))
#define rTIMER_64COUNTER_HI_1                *((volatile unsigned *)(_OST_MODULE_BASE + 0x8070))
#define rTIMER_64COUNTER_LOAD_LO_1           *((volatile unsigned *)(_OST_MODULE_BASE + 0x8074))
#define rTIMER_64COUNTER_LOAD_HI_1           *((volatile unsigned *)(_OST_MODULE_BASE + 0x8078))
#define rTIMER_64COUNTER_RLATCHED_LO_1       *((volatile unsigned *)(_OST_MODULE_BASE + 0x807c))
#define rTIMER_64COUNTER_RLATCHED_HI_1       *((volatile unsigned *)(_OST_MODULE_BASE + 0x8080))


/****************************************************************************************
    _CPU2RTC_IO_BRIDGE,  offset 0x80030000
    including GPSRTC, SYSRTC, POWER, EFUSEPMU modules
****************************************************************************************/
#define rCPURTCIOBG_CTRL             *((volatile unsigned *)(_CPU2RTC_IO_BRIDGE + 0x0000))  
#define rCPURTCIOBG_WRBE             *((volatile unsigned *)(_CPU2RTC_IO_BRIDGE + 0x0004))  
#define rCPURTCIOBG_ADDR             *((volatile unsigned *)(_CPU2RTC_IO_BRIDGE + 0x0008))  
#define rCPURTCIOBG_DATA             *((volatile unsigned *)(_CPU2RTC_IO_BRIDGE + 0x000C))
#define PWRC_OFFSET_PON_STATUS      (0x3008)
#define PWRC_OFFSET_FSM_M3_CTRL      (0x3050)
#define PWRC_OFFSET_M3_CLK_EN           (0x308c)
#define PWRC_OFFSET_SPI0_CLK_EN         (0x3094)
#define PWRC_OFFSET_RTC_SEC_CLK_EN  (0x3098)
#define PWRC_OFFSET_RTC_NOC_CLK_EN  (0x309c)
#define PWRC_OFFSET_RTC_BOOTSTRAP0  (0x0030)


#define rIPC_TESTANDSET_0         *((volatile unsigned *)(0x13240404))


/****************************************************************************************
    Clocks and PLL registers
****************************************************************************************/
/* use definitions in clkc.h */


#include "soc_clkc.h"


/***************************************************************************************\
| SPI 
\***************************************************************************************/
#define rSPI_CTRL                                *((volatile unsigned *)_SPI1_MODULE_BASE)
#define rSPI_CMD                                 *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0004))
#define rSPI_TX_RX_EN                            *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0008))
#define rSPI_INT_EN                              *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x000c))
#define rSPI_INT_STATUS                          *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0010))
        
#define rSPI_TX_DMA_IO_CTRL                      *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0100))
#define rSPI_TX_DMA_IO_LEN                       *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0104))
#define rSPI_TXFIFO_CTRL                         *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0108))
#define rSPI_TXFIFO_LEVEL_CHK                    *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x010c))
#define rSPI_TXFIFO_OP                           *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0110))
#define rSPI_TXFIFO_STATUS                       *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0114))
#define rSPI_TXFIFO_DATA                         *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0118))
        
#define rSPI_RX_DMA_IO_CTRL                      *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0120))
#define rSPI_RX_DMA_IO_LEN                       *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0124))
#define rSPI_RXFIFO_CTRL                         *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0128))
#define rSPI_RXFIFO_LEVEL_CHK                    *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x012c))
#define rSPI_RXFIFO_OP                           *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0130))
#define rSPI_RXFIFO_STATUS                       *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0134))
#define rSPI_RXFIFO_DATA                         *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0138))
#define rSPI_SLV_RX_SAMPLE_MODE				     *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0140))
#define	rSPI_DUMMY_DELAY_CTRL					 *((volatile unsigned *)(_SPI1_MODULE_BASE + 0x0144))






//bit define for rCLKC_CLK_EN0
//#define CLK_DSP_EN                       		0x00000001
//#define CLK_USB0_EN	                            0x00010000         
//#define CLK_USB1_EN                             0x00020000
//#define CLK_SECURITY_EN	                        0x00080000   

//bit define for rCLKC_CLK_EN1
//#define CLK_NAND_EN                      		0x00000004  //default is enabled
//#define CLK_UART1_EN                            0x00000020   
//#define CLK_EFUSESYS_EN                  		0x00020000  //default is enabled
//#define CLK_ROM_EN                       		0x02000000  //default is enabled
//#define CLK_SDIO01_EN                           0x08000000
//#define CLK_SDIO23_EN                           0x10000000


/***************************************************************************************\
| Resource Sharing registers offset 0x90030000
\***************************************************************************************/
#define rRSC_USB_UART_SHARE_SET                 *((volatile unsigned *)(_RSC_MODULE_BASE + 0x0000))
#define rRSC_USB_UART_SHARE_CLR                 *((volatile unsigned *)(_RSC_MODULE_BASE + 0x0004))
#define rRSC_PIN_MUX_SET                        *((volatile unsigned *)(_RSC_MODULE_BASE + 0x0008))
#define rRSC_PIN_MUX_CLR                        *((volatile unsigned *)(_RSC_MODULE_BASE + 0x000c))
#define rRSC_USB_CTRL_SET                       *((volatile unsigned *)(_RSC_MODULE_BASE + 0x0010))
#define rRSC_USB_CTRL_CLR                       *((volatile unsigned *)(_RSC_MODULE_BASE + 0x0014))




#define USBPHY_PLL_POWERDOWN		0x2
#define USBPHY_PLL_BYPASS			0x4
#define USBPHY_PLL_LOCK			    0x8

/***************************************************************************************\
|  system registers, offset 0x80000000
\***************************************************************************************/
#define rSYS_TIMEOUT				*((volatile unsigned *)_SYSTEM_BASE)


/***************************************************************************************\
|  Reset controller registers, offset 0xb8010000
\***************************************************************************************/
#define USBOTG_OTGSC             	*((volatile unsigned *)(_USBOTG1_MODULE_BASE + 0x1A4))
#define USBOTG_PHY_PROGRAM          *((volatile unsigned *)(_USBOTG1_MODULE_BASE + 0x200))




#define USB_BASE_ADDRESS       _USBOTG1_MODULE_BASE



// Identification Core Registers
#define rUSBOTG_ID                                         *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0000))
#define rUSBOTG_HWGENERAL                              *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0004))
#define rUSBOTG_HWHOST                                     *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0008))
#define rUSBOTG_HWDEVICE                               *((volatile unsigned *)(USB_BASE_ADDRESS + 0x000c))
#define rUSBOTG_HWTXBUF                                *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0010))
#define rUSBOTG_HWRXBUF                                    *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0014))
                                                
#define rUSBOTG_GPTIMER0LD                             *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0080))
#define rUSBOTG_GPTIMER0CTRL                           *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0084))
#define rUSBOTG_GPTIMER1LD                                     *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0088))
#define rUSBOTG_GPTIMER1CTRL                               *((volatile unsigned *)(USB_BASE_ADDRESS + 0x008C))
                                                
  // Capability Registers                       
#define rUSBOTG_CAPLEN_HCIVER                          *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0100))
#define rUSBOTG_HCSPARAMS                              *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0104))
#define rUSBOTG_HCCPARAMS                                      *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0108))
                                                
#define rUSBOTG_DCIVERSION                             *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0120))
#define rUSBOTG_DCCPARAMS                                  *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0124))
                                                
// Operational Registers                        
#define rUSBOTG_USBCMD                                     *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0140))
#define rUSBOTG_USBSTS                                         *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0144))
#define rUSBOTG_USBINTR                                        *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0148))
#define rUSBOTG_FRINDEX                                    *((volatile unsigned *)(USB_BASE_ADDRESS + 0x014C))
                                                
#define rUSBOTG_PERIODICLISTBASE_DEVICEADDR    *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0154))
#define rUSBOTG_ASYNCLISTADDR_EPLISTADDR       *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0158))
#define rUSBOTG_TTCTRL                                 *((volatile unsigned *)(USB_BASE_ADDRESS + 0x015C))
#define rUSBOTG_BURSTSIZE                              *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0160))
#define rUSBOTG_TXFILLTUNING                           *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0164))
                                                
#define rUSBOTG_ULPIVIEWPORT                           *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0170))
                                                
#define rUSBOTG_ENDPTNAK                                   *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0178))
#define rUSBOTG_ENDPTNAKEN                             *((volatile unsigned *)(USB_BASE_ADDRESS + 0x017C))
#define rUSBOTG_CONFIGFLAG                             *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0180))
#define rUSBOTG_PORTSC1                                    *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0184))
                                                
#define rUSBOTG_OTGSC                                          *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01A4))
#define rUSBOTG_USBMODE                                    *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01A8))
#define rUSBOTG_ENDPTSETUPSTAT                         *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01AC))
#define rUSBOTG_ENDPTPRIME                             *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01B0))
#define rUSBOTG_ENDPTFLUSH                                 *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01B4))
#define rUSBOTG_ENDPTSTATUS                            *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01B8))
#define rUSBOTG_ENDPTCOMPLETE                          *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01BC))

#define rUSBOTG_ENDPTCTRL(ep)                                  *((volatile unsigned *)(USB_BASE_ADDRESS + 0x01C0 + ep *4))
#define rUSBOTG_PHY_PROGRAM                                        *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0200))
#define rUSBOTG_PHY_TESTIF                                        *((volatile unsigned *)(USB_BASE_ADDRESS + 0x0204))
#define  rUSBOTG1_AHB_SEL                               *((volatile unsigned *)( USB_BASE_ADDRESS + 0x208))



#define STALL_EPX_RX(ep)			(rUSBOTG_ENDPTCTRL(ep) |= (0x1 << 0))
#define STALL_EPX_TX(ep)			(rUSBOTG_ENDPTCTRL(ep) |= (0x1 << 16))


/*************************************************************\
|  IOC register
\*************************************************************/
#define rIOC_TOP_FUNC_SEL_1_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0088)) 
#define rIOC_TOP_FUNC_SEL_1_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x008C)) 
#define rIOC_TOP_FUNC_SEL_2_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0090)) 
#define rIOC_TOP_FUNC_SEL_2_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0094)) 
#define rIOC_TOP_FUNC_SEL_3_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0098)) 
#define rIOC_TOP_FUNC_SEL_3_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x009C)) 
#define rIOC_TOP_FUNC_SEL_4_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x00A0)) 
#define rIOC_TOP_FUNC_SEL_4_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x00A4)) 
#define rIOC_TOP_FUNC_SEL_5_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x00A8)) 
#define rIOC_TOP_FUNC_SEL_5_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x00AC)) 
#define rIOC_TOP_FUNC_SEL_16_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0100)) 
#define rIOC_TOP_FUNC_SEL_16_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0104)) 
#define rIOC_TOP_FUNC_SEL_19_REG_SET     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x0118)) 
#define rIOC_TOP_FUNC_SEL_19_REG_CLR     *((volatile unsigned *)(_IOC_MODULE_BASE + 0x011C)) 
#define rIOC_TOP_PULL_EN_2_REG_CLR     *((volatile unsigned *)(0x10E40190))
#define rIOC_TOP_PULL_EN_3_REG_SET     *((volatile unsigned *)(0x10E40198)) 
#define rIOC_TOP_PULL_EN_3_REG_CLR     *((volatile unsigned *)(0x10E4019C)) 
#define rIOC_TOP_PULL_EN_11_REG_CLR     *((volatile unsigned *)(0x10E40254))


#define rIOC_RTC_FUNC_SEL_0_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0000)) 
#define rIOC_RTC_FUNC_SEL_0_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0004))
#define rIOC_RTC_FUNC_SEL_1_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0008)) 
#define rIOC_RTC_FUNC_SEL_1_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x000C))
#define rIOC_RTC_FUNC_SEL_2_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0010)) 
#define rIOC_RTC_FUNC_SEL_2_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0014))
#define rIOC_RTC_PULL_EN_0_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0100)) 
#define rIOC_RTC_PULL_EN_0_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0104))
#define rIOC_RTC_PULL_EN_1_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0108)) 
#define rIOC_RTC_PULL_EN_1_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x010C))
#define rIOC_RTC_STRENGTH_CNTL_0_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0200)) 
#define rIOC_RTC_STRENGTH_CNTL_0_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0204))
#define rIOC_RTC_STRENGTH_CNTL_1_REG_SET    *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0208)) 
#define rIOC_RTC_STRENGTH_CNTL_1_REG_CLR     *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x020C))
#define rIOC_RTC_IN_DISABLE_1_REG_CLR   *((volatile unsigned *)(_IOC_RTC_MODULE_BASE + 0x0A0C))





/***************************************************************************************\
| I2C interface registers offset 0x800b0000 + 0x0000
\***************************************************************************************/
//#define _I2C_MODULE_BASE                        (_PERIPHERAL_BASE + 0xe0000)
//#define _I2C1_MODULE_BASE                       (_PERIPHERAL_BASE + 0xf0000)

#define rI2C_CLK                                *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0000))
#define rI2C_SAR                                *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0004))
#define rI2C_DBR                                *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0008))
#define rI2C_SR                                 *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x000c))
#define rI2C_CR                                 *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0010))
#define rI2C_IO_CTRL                            *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0014))
#define rI2C_SDA_DELAY                          *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0018))
#define rI2C_CMD_START                          *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x001c))

#define rI2C_CMD_0                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0030))
#define rI2C_CMD_1                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0034))
#define rI2C_CMD_2                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0038))
#define rI2C_CMD_3                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x003c))
#define rI2C_CMD_4                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0040))
#define rI2C_CMD_5                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0044))
#define rI2C_CMD_6                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0048))
#define rI2C_CMD_7                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x004c))
#define rI2C_CMD_8                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0050))
#define rI2C_CMD_9                              *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0054))
#define rI2C_CMD_10                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0058))
#define rI2C_CMD_11                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x005c))
#define rI2C_CMD_12                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0060))
#define rI2C_CMD_13                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0064))
#define rI2C_CMD_14                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0068))
#define rI2C_CMD_15                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x006c))

#define rI2C_DATA_0                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0080))
#define rI2C_DATA_1                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0084))
#define rI2C_DATA_2                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x0088))
#define rI2C_DATA_3                             *((volatile unsigned *)(_I2C0_MODULE_BASE + 0x008c))
/***************************************************************************************\
| I2C interface registers offset 0x800b0000 + 0x0100
\***************************************************************************************/
#define rI2C_1_CLK                              *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0000))
#define rI2C_1_SAR                              *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0004))
#define rI2C_1_DBR                              *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0008))
#define rI2C_1_SR                               *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x000c))
#define rI2C_1_CR                               *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0010))
#define rI2C_1_IO_CTRL                          *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0014))
#define rI2C_1_SDA_DELAY                        *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0018))
#define rI2C_1_CMD_START                        *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x001c))

#define rI2C_1_CMD_0                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0030))
#define rI2C_1_CMD_1                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0034))
#define rI2C_1_CMD_2                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0038))
#define rI2C_1_CMD_3                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x003c))
#define rI2C_1_CMD_4                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0040))
#define rI2C_1_CMD_5                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0044))
#define rI2C_1_CMD_6                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0048))
#define rI2C_1_CMD_7                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x004c))
#define rI2C_1_CMD_8                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0050))
#define rI2C_1_CMD_9                            *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0054))
#define rI2C_1_CMD_10                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0058))
#define rI2C_1_CMD_11                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x005c))
#define rI2C_1_CMD_12                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0060))
#define rI2C_1_CMD_13                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0064))
#define rI2C_1_CMD_14                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0068))
#define rI2C_1_CMD_15                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x006c))

#define rI2C_1_DATA_0                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0080))
#define rI2C_1_DATA_1                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0084))
#define rI2C_1_DATA_2                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x0088))
#define rI2C_1_DATA_3                           *((volatile unsigned *)(_I2C1_MODULE_BASE + 0x008c))


/***************************************************************************************\
| QSPI interface registers
\***************************************************************************************/
#define rQSPI_FCSPI_CTRL           			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0000))
#define rQSPI_FCSPI_STAT           			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0004))
#define rQSPI_FCSPI_ACCRR0           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0008))
#define rQSPI_FCSPI_ACCRR1           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x000C))
#define rQSPI_FCSPI_ACCRR2           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0010))
#define rQSPI_FCSPI_DDPM           			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0014))
#define rQSPI_FCSPI_RWDATA           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0018))
#define rQSPI_FCSPI_FFSTAT           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x001C))
#define rQSPI_FCSPI_DEFMEM           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0020))
#define rQSPI_FCSPI_EXADDR           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0024))
#define rQSPI_FCSPI_MEMSPEC          		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0028))
#define rQSPI_FCSPI_INTMSK           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x002C))
#define rQSPI_FCSPI_INTREQ           		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0030))
#define rQSPI_FCSPI_CICFG          			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0034))
#define rQSPI_FCSPI_CIDR0          			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0038))
#define rQSPI_FCSPI_CIDR1          			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x003C))
#define rQSPI_FCSPI_RDC            			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0040))

#define rQSPI_FCSPI_DMA_SADDR        		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0800))
#define rQSPI_FCSPI_DMA_FADDR        		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0804))
#define rQSPI_FCSPI_DMA_LEN         	 	*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0808))
#define rQSPI_FCSPI_DMA_CST          		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x080C))
#define rQSPI_FCSPI_DEBUG          			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0810))
#define rQSPI_FCSPI_XOTF_EN          		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0814))
#define rQSPI_FCSPI_XOTF_BASE        		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0818))
#define rQSPI_FCSPI_DELAY_LINE       		*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x081C))
#define rQSPI_FCSPI_ADDR_EXTMODE          			*((volatile unsigned *)(_QSPI_MODULE_BASE + 0x0820))

#define rINT_RISC_MASK0_SET                       (*(volatile unsigned *) 0x10220000U)
#define rINT_RISC_MASK0_CLR                       (*(volatile unsigned *) 0x10220004U)
#define rINT_RISC_MASK1_SET                       (*(volatile unsigned *) 0x10220008U)
#define rINT_RISC_MASK1_CLR                       (*(volatile unsigned *) 0x1022000CU)
#define rINT_RISC_MASK2_SET                       (*(volatile unsigned *) 0x10220010U)
#define rINT_RISC_MASK2_CLR                       (*(volatile unsigned *) 0x10220014U)
#define rINT_RISC_MASK3_SET                       (*(volatile unsigned *) 0x10220018U)
#define rINT_RISC_MASK3_CLR                       (*(volatile unsigned *) 0x1022001CU)
#define rINT_RISC_EVENTI_MASK0_SET                (*(volatile unsigned *) 0x1022009CU)
#define rINT_RISC_EVENTI_MASK0_CLR                (*(volatile unsigned *) 0x102200A0U)
#define rINT_RISC_EVENTI_MASK1_SET                (*(volatile unsigned *) 0x102200A4U)
#define rINT_RISC_EVENTI_MASK1_CLR                (*(volatile unsigned *) 0x102200A8U)
#define rINT_RISC_EVENTI_MASK2_SET                (*(volatile unsigned *) 0x102200ACU)
#define rINT_RISC_EVENTI_MASK2_CLR                (*(volatile unsigned *) 0x102200B0U)
#define rINT_RISC_EVENTI_MASK3_SET                (*(volatile unsigned *) 0x102200B4U)
#define rINT_RISC_EVENTI_MASK3_CLR                (*(volatile unsigned *) 0x102200B8U)
#define rINT_RISC_EDBGREQ_MASK                    (*(volatile unsigned *) 0x102200BCU)

#include "types.h"

#endif //_SOC_H_

