/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef __DIAGNOSE_H__
#define __DIAGNOSE_H__

//Define this to enable diagnose functions for "functon name" and "progress tree"
//#define DIAG_FUNC

//Define this to enable "time log" and "message"
//#define DIAG_TIME_MSG


//This function should be called before use any macro of diagnose function
void DiagInit(void);

//Print diagnose infomation
void DiagPTPrint(void);
void DiagTimeLogPrint(void);
void DiagMsgPrint(void);



//Internal used function
void DiagPTStart(const char *FuncName);
void DiagPTReturn(const char *FuncName, BOOL fHaveRet, DWORD dwRetValue);
void DiagTimeLog (char* pbMsg);
void DiagMsg(char* pbMsg);
void DiagMsgNumber(DWORD dwData);




//Address and size
//Total reserved 13KB, from address 0xCA004400
#define DIAG_PROGRESSTREE_BASE_ADDR         (SRAM_DIAG_BASE)
#define DIAG_PROGRESSTREE_MAXSIZE               (8*1024)  

#define DIAG_TIMELOG_BASE_ADDR         (DIAG_PROGRESSTREE_BASE_ADDR+DIAG_PROGRESSTREE_MAXSIZE)
#define DIAG_TIMELOG_MAXSIZE               (2*1024)     //2KB

#define DIAG_MSG_BASE_ADDR         (DIAG_TIMELOG_BASE_ADDR+DIAG_TIMELOG_MAXSIZE)
#define DIAG_MSG_MAXSIZE               (3*1024)     //3KB for message





//Progress tree record
#define DIAG_PT_LEAVE               (1u << 31)
#define DIAG_PT_RET_VALUE       (1u << 30)

#ifdef  DIAG_FUNC
//Mark enter a function
#define DIAG_START()        DiagPTStart(__FUNCTION__)

//Mark return from a function with a value (hex)
#define DIAG_RET_VALUE(x)             DiagPTReturn(__FUNCTION__, TRUE, (x))

//Mark return from a function without value
#define DIAG_RET()             DiagPTReturn(__FUNCTION__, FALSE, 0)

#else
#define DIAG_START()
#define DIAG_RET_VALUE(x)         
#define DIAG_RET()                     

#endif


//Time log record

#ifdef  DIAG_TIME_MSG
//Log the time stamp with message
#define DIAG_TIMELOG(x)         DiagTimeLog(x)
#else
#define DIAG_TIMELOG(x)
#endif


//Message record

#ifdef  DIAG_TIME_MSG
//Log message
#define DIAG_MSG(x)         DiagMsg(x)
#else
#define DIAG_MSG(x)         DebugMsg(x)
#endif

#ifdef  DIAG_TIME_MSG
//Log value (hex)
#define DIAG_MSG_NUM(x)         DiagMsgNumber(x)
#else
#define DIAG_MSG_NUM(x)
#endif

#endif  //#ifndef __DIAGNOSE_H__

