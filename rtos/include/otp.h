/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    otp.h
//
// Abstract: 
//    MACROs for otp.c.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _OTP_H_
#define _OTP_H_

#include "soc.h"
#include "ds_sec.h"
#include "ds_otp.h"

#define ReadExpected(Address, Expected) ((*(volatile uint32_t*)(Address)) != (Expected))
#define Read_Byte(Address) (*(volatile uint32_t*)(Address))
#define Read_Masked(Address, Mask, Expected) (((*(volatile uint32_t*)(Address)) & (Mask)) != ((Expected)& (Mask)))
#define Dummy_Read(Address) (*(volatile uint32_t*)(Address))
#define Wait(Address, Mask, Expected) do{;}while(((*(volatile uint32_t*)(Address)) & (Mask)) != (Expected))
#define Wait_gt(Address, Threshold) do{;}while(((*(volatile uint32_t*)(Address)) ) <= (Threshold))



//#define Write(Address, Value) ((*(volatile uint32_t*)(Address)) = (Value))
#define Write_Byte(Address, Value) ((*(volatile uint8_t*)(Address)) = (Value))
#define Write_HalfWord(Address, Value) ((*(volatile uint16_t*)(Address)) = (Value))

#define WriteReg(addr, val) \
	((*((unsigned int *)(addr))) = (unsigned int)(val))

#define ReadReg(addr) \
	(*((unsigned int *)(addr)))

/**************************************************************************************\
| OTP registers
\***************************************************************************************/
#define BLANK_TESTMODE			1
#define TESTDEC_TESTMODE		2
#define WRTEST_TESTMODE			4

#define BLANK_TESTMODE_DONE_MASK	0x200
#define TESTDEC_TESTMODE_DONE_MASK	0x400
#define WRTEST_TESTMODE_DONE_MASK	0x600
#define CMD_DONE_MASK			0x040

#define TEST_ERR_MASK			0x1

#define TEST_CMD			0x00070000
#define DONECLR_CMD			0x80000000

#define BR_STAT_ERR_MASK		0x3C00

//#define DS_SEC_PHY_ADDR		0x18260000
#define MAP_SIZE		0x10000UL
#define MAP_MASK		(MAP_SIZE - 1)

#define OTP_PE_SHIFT 			0x700

/* macros */
#define DX_HOST_AIB_ADDR_REG_REG_OFFSET		0xAA4UL
#define DX_HOST_AIB_WDATA_REG_REG_OFFSET	0xAA8UL
#define DX_HOST_AIB_RDATA_REG_REG_OFFSET	0xAACUL
#define DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET	0xAB0UL
#define DX_AIB_FUSE_ACK_REG_OFFSET		0xAB4UL

#define OTP_MAX_OFFSET 0xff

#define SYS_WriteRegister(addr, val) \
	((*((unsigned int *)(addr))) = (unsigned int)(val))

#define SYS_ReadRegister(addr, val) \
	((val) = (*((unsigned int *)(addr))))

#define DX_MNG_READ_WORD_VIA_AIB(baseAddr, nvmAddr, nvmData)	\
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			baseAddr + DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvmAddr); \
		do { \
			SYS_ReadRegister( \
				baseAddr + DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		SYS_ReadRegister( \
			baseAddr + DX_HOST_AIB_RDATA_REG_REG_OFFSET, nvmData);\
	} while (0)

/* Write a word via NVM */
#define DX_MNG_WRITE_WORD_VIA_AIB(baseAddr, nvmAddr, nvmData) \
	do { \
		unsigned int tval; \
		SYS_WriteRegister( \
			baseAddr + DX_HOST_AIB_WDATA_REG_REG_OFFSET, nvmData); \
		SYS_WriteRegister( \
			baseAddr + DX_HOST_AIB_ADDR_REG_REG_OFFSET, nvmAddr); \
		do { \
			SYS_ReadRegister( \
				baseAddr + DX_AIB_FUSE_ACK_REG_OFFSET, tval); \
		} while (!(tval & 0x1)); \
		do { \
			SYS_ReadRegister( \
				baseAddr + \
				DX_AIB_FUSE_PROG_COMPLETED_REG_OFFSET, \
				tval); \
		} while (!(tval & 0x1)); \
	} while (0)

/***************************************************************************************\
| OTP functions
\***************************************************************************************/
enum CMD_RET {
	CMD_INV = -2,
	CMD_ERR = -1,
	CMD_OK = 0,
};


struct CMD_ENT {
	unsigned int	cmd_id;
	char	*cmd_name;
	//enum CMD_RET (*handler)(long *argc, int n);
	enum CMD_RET (*handler)(long *Inargc, int in_n, long *Outargc, int *out_n);
};
int otp_test (int non_blank_read) ;
int otp_read();
int full_otp_test (void);

unsigned int Read_Aib  (   unsigned int start_address,  
                              unsigned int num_words);

void         Write_Aib ( unsigned int start_address,  
                           unsigned int num_words);

void  Write_Aib_PE
 (
  unsigned int WS_byte1,  
  unsigned int WS_byte2,
  unsigned int FT_byte1,  
  unsigned int FT_byte2);
 
 unsigned int Write_OTP
 (
  unsigned int address,  
  unsigned long WriteByte);

unsigned int Read_Aib_PE	
(
  unsigned int WS_byte1,  
  unsigned int WS_byte2,
  unsigned int FT_byte1,  
  unsigned int FT_byte2 );
 unsigned int Read_Aib_char	
(
  unsigned int StartAddr,  
  unsigned int EndAddr
);
unsigned int Read_OTP_Verify	
(
  unsigned int OtpReadAddr,  
  unsigned long ExpectedOtpValue);
unsigned int Read_OTP_Value	
(
  unsigned int OtpReadAddr,  
  unsigned long *OtpReadValue);
 
int kilopass_test(int run_option);
enum CMD_RET parse_command(unsigned int cmd, long *in_buf, int in_n, long *out_buf, int *out_n );


//////////////////end function
#endif //_OTP_H_

