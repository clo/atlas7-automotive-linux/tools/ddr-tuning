/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    uart1.h
//
// Abstract: 
//    MACROs for uart1.c.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _UART1_H_
#define _UART1_H_

#include "soc.h"

/***************************************************************************************\
| UART1 registers
\***************************************************************************************/
#define UART1_MODE1					 		*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0000))                


#define UART1_LINE_CTRL					 		*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0040))                
#define UART1_TXRX_ENA_REG                      *((volatile unsigned *)(_UART1_MODULE_BASE + 0x004c))   
#define UART1_DIVISOR                           *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0050))   
#define UART1_INT_ENABLE_SET                        *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0054))   
#define UART1_INT_STATUS                        *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0058))   
#define UART1_RISC_DSP_MODE                     *((volatile unsigned *)(_UART1_MODULE_BASE + 0x005C))   
#define UART1_INT_ENABLE_CLR                        *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0060))   


#define UART1_TX_DMA_IO_CTRL                    *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0100))                                                             
#define UART1_TX_DMA_IO_LEN                     *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0104))      
#define UART1_TXFIFO_CTRL                       *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0108))      
#define UART1_TXFIFO_LEVEL_CHK                  *((volatile unsigned *)(_UART1_MODULE_BASE + 0x010c))      
#define UART1_TXFIFO_OP                         *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0110))      
#define UART1_TXFIFO_STATUS                     *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0114))      
#define UART1_TXFIFO_DATA                   	*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0118))  

#define UART1_RX_DMA_IO_CTRL                    *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0120))
#define UART1_RX_DMA_IO_LEN						*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0124))
#define UART1_RXFIFO_CTRL						*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0128))
#define UART1_RXFIFO_LEVEL_CHK                  *((volatile unsigned *)(_UART1_MODULE_BASE + 0x012c))
#define UART1_RXFIFO_OP                         *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0130))
#define UART1_RXFIFO_STATUS                     *((volatile unsigned *)(_UART1_MODULE_BASE + 0x0134))
#define UART1_RXFIFO_DATA                   	*((volatile unsigned *)(_UART1_MODULE_BASE + 0x0138))

#define UART1_TX_IO_MODE                         0x1
#define UART1_RX_IO_MODE                         0x1
#define UART1_RX_EN                              0x01
#define UART1_TX_EN                              0x02
#define UART1_TXFIFO_RESET                       0x1             
#define UART1_TXFIFO_START                       0x2
#define UART1_RXFIFO_RESET                       0x1
#define UART1_RXFIFO_START                       0x2
#define UART1_RXFIFO_EMPTY                       0x40
#define UART1_TXFIFO_EMPTY						 0x40
#define UART1_TXFIFO_FULL                        0x20
#define UART1_RXFIFO_FULL                        0x20

#define UART_INT_MASK_ALL  						 0xffff
#define UART_FIFO_FULL							 0x20

#define UART_INT_ENABLE_MASK_ALL                0x1ffff
#define UART_INT_RX_DONE_MASK                   0x0001
#define UART_INT_TX_DONE_MASK                   0x0002
#define UART_INT_RX_OF_MASK                     0x0004
#define UART_INT_TX_ALLEMPTY_MASK               0x0008
#define UART_INT_DMA_RX_MASK                    0x0010
#define UART_INT_DMA_TX_MASK                    0x0020
#define UART_INT_RX_FF_MASK                     0x0040
#define UART_INT_TX_FE_MASK                     0x0080
#define UART_INT_RX_THLD_MASK                   0x0100
#define UART_INT_TX_THLD_MASK                   0x0200
#define UART_INT_FRM_ERR_MASK                   0x0400
#define UART_BREAK_MASK                         0x0800
#define UART_INT_TIMEOUT_MASK                   0x1000
#define UART_INT_PARITY_MASK                    0x2000

////////////////////////////////function
void uartInit(DWORD u_div, DWORD ioclk);
void uartWriteByte(BYTE ch);
BYTE uartReadByte (void);
BOOL uartIsDataInReadBuf(DWORD *v_byteNum);
void uartGicInit(void);

/* strate to rhe uart, bypass the OS wrappr */
void dumpString( char* str );
void dumpChar( char ch );
void SendClosingSequence( void );
void InjectChar( char ch );
//////////////////end function
#endif //_UART1_H_

