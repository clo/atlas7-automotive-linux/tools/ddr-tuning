/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
#ifndef _STD_FUNCS_H_
#define _STD_FUNCS_H_

#include <stddef.h>

/* implemented in std_funcs.c */
int strncmp (const char * const s1, const char * const s2, const size_t num);
int strcmp (const char * const s1, const char * const s2);
char* strcpy(char *dest, const char *src);
char* strncpy(char *dest, const char *src, size_t n);
char* strncat(char *dest, const char *src, size_t n);
size_t strlen(const char* str );
void *memset(void *dst, int fill, unsigned long length);


/* implemented in printf-stdarg.c */
int sprintf(char *out, const char *format, ...);
int printf(const char *format, ...);

#endif
