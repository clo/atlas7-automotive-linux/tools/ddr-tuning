/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_GIC_H_
#define _SOC_GIC_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <soc_int.h>

#ifdef __cplusplus
    extern "C" {
#endif

/**
 * Implemented interrupt numbers
 */
#define GIC_NUM_SGI   16 ///< Number of SGI
#define GIC_NUM_PPI   16 ///< Number of PPI
#define GIC_NUM_SPI  128 ///< Number of SPI

/// start ID of SPI
#define SPI_ID_BASE (GIC_NUM_SGI + GIC_NUM_PPI)


/**
 * SGIs IDs
 */
typedef enum {
    SGI_ID_0 = 0,   ///< Software Generated Interrupt ID 0
    SGI_ID_1,       ///< Software Generated Interrupt ID 1
    SGI_ID_2,       ///< Software Generated Interrupt ID 2
    SGI_ID_3,       ///< Software Generated Interrupt ID 3
    SGI_ID_4,       ///< Software Generated Interrupt ID 4
    SGI_ID_5,       ///< Software Generated Interrupt ID 5
    SGI_ID_6,       ///< Software Generated Interrupt ID 6
    SGI_ID_7,       ///< Software Generated Interrupt ID 7
    SGI_ID_8,       ///< Software Generated Interrupt ID 8
    SGI_ID_9,       ///< Software Generated Interrupt ID 9
    SGI_ID_10,      ///< Software Generated Interrupt ID 10
    SGI_ID_11,      ///< Software Generated Interrupt ID 11
    SGI_ID_12,      ///< Software Generated Interrupt ID 12
    SGI_ID_13,      ///< Software Generated Interrupt ID 13
    SGI_ID_14,      ///< Software Generated Interrupt ID 14
    SGI_ID_15       ///< Software Generated Interrupt ID 15
} sgi_id_t;

/**
 * PPIs IDs
 */
enum {
    PPI_ID_GLOBAL_TIMER     = 27, ///< Global Timer Interrupt ID
    PPI_ID_LEGACY_FIQ       = 28, ///< Legacy FIQ ID
    PPI_ID_PRIVATE_TIMER    = 29, ///< Private Timer Interrupt ID
    PPI_ID_WATCHDOG         = 30, ///< Watchdog Interrupt ID
    PPI_ID_LEGACY_IRQ       = 31  ///< Legacy IRQ ID
};

/**
 * SPI ID convert macro
 *
 * Use this macro to convert legacy interrupt ID(defined in soc_int.h) to GIC's SPI ID
 *
 * It's only used to keep backward compatible, Don't use it in your code.
 */
#define SPI_ID(legacy_id)  ( SPI_ID_BASE + (legacy_id) )

/**
 * Legacy ID convert macro
 *
 * Use this macro to convert GIC's SPI ID to legacy interrupt ID(defined in soc_int.h)
 *
 * It's only used to keep backward compatible, Don't use it in your code.
 */
#define LEGACY_ID(gic_id)  ( (gic_id) - SPI_ID_BASE )

/**
 * SPIs IDs
 */
enum {
    SPI_ID_TIMER0           = SPI_ID(INT_ID_TIMER0),
    SPI_ID_TIMER1           = SPI_ID(INT_ID_TIMER1),
    SPI_ID_TIMER2           = SPI_ID(INT_ID_TIMER2),
    SPI_ID_I2S1             = SPI_ID(INT_ID_I2S1),
    SPI_ID_MEMCPM           = SPI_ID(INT_ID_MEMCPM),
    SPI_ID_MEDIA            = SPI_ID(INT_ID_MEDIA),
    SPI_ID_GRAPHIC          = SPI_ID(INT_ID_GRAPHIC),
    SPI_ID_VITERBI          = SPI_ID(INT_ID_VITERBI),
    SPI_ID_VSS_RD_DMAC      = SPI_ID(INT_ID_VSS_RD_DMAC),
    SPI_ID_VSS_WD_DMAC      = SPI_ID(INT_ID_VSS_WR_DMAC),
    SPI_ID_USB0             = SPI_ID(INT_ID_USB0),
    SPI_ID_USB1             = SPI_ID(INT_ID_USB1),
    SPI_ID_DMAC0            = SPI_ID(INT_ID_DMAC0),
    SPI_ID_SPI0             = SPI_ID(INT_ID_SPI0),
    SPI_ID_SPI1             = SPI_ID(INT_ID_SPI1),
    SPI_ID_UART0            = SPI_ID(INT_ID_UART0),
    SPI_ID_UART1            = SPI_ID(INT_ID_UART1),
    SPI_ID_UART2            = SPI_ID(INT_ID_UART2),
    SPI_ID_USP0             = SPI_ID(INT_ID_USP0),
    SPI_ID_USP1             = SPI_ID(INT_ID_USP1),
    SPI_ID_USP2             = SPI_ID(INT_ID_USP2),
    SPI_ID_SDIO23           = SPI_ID(INT_ID_SDIO23),
    SPI_ID_I2C0             = SPI_ID(INT_ID_I2C0),
    SPI_ID_I2C1             = SPI_ID(INT_ID_I2C1),
    SPI_ID_DS_SEC           = SPI_ID(INT_ID_DS_SEC),
    SPI_ID_DS_PUB           = SPI_ID(INT_ID_DS_PUB),
    SPI_ID_SYS2PCI          = SPI_ID(INT_ID_SYS2PCI),
    SPI_ID_CPUPMU           = SPI_ID(INT_ID_CPUPMU),
    SPI_ID_LCD0             = SPI_ID(INT_ID_LCD0),
    SPI_ID_VPP0             = SPI_ID(INT_ID_VPP0),
    SPI_ID_PWRC             = SPI_ID(INT_ID_PWRC),
    SPI_ID_TSC              = SPI_ID(INT_ID_TSC),
    SPI_ID_TSCADC           = SPI_ID(INT_ID_TSCADC),
    SPI_ID_AC97             = SPI_ID(INT_ID_AC97),
    SPI_ID_ROM              = SPI_ID(INT_ID_ROM),
    SPI_ID_TZC0             = SPI_ID(INT_ID_TZC0),
    SPI_ID_SDIO01           = SPI_ID(INT_ID_SDIO01),
    SPI_ID_SDIO45           = SPI_ID(INT_ID_SDIO45),
    SPI_ID_PCICOPY          = SPI_ID(INT_ID_PCICOPY),
    SPI_ID_NAND             = SPI_ID(INT_ID_NAND),
    SPI_ID_SECURITY         = SPI_ID(INT_ID_SECURITY),
    SPI_ID_GPIO_MEDIAM_0    = SPI_ID(INT_ID_GPIO_MEDIAM_0),
    SPI_ID_GPIO_MEDIAM_1    = SPI_ID(INT_ID_GPIO_MEDIAM_1),
    SPI_ID_GPIO_VDIFM_0     = SPI_ID(INT_ID_GPIO_VDIFM_0),
    SPI_ID_GPIO_VDIFM_1     = SPI_ID(INT_ID_GPIO_VDIFM_1),
    SPI_ID_GPIO_VDIFM_2     = SPI_ID(INT_ID_GPIO_VDIFM_2),
    SPI_ID_GPIO_VDIFM_3     = SPI_ID(INT_ID_GPIO_VDIFM_3),
    SPI_ID_GPIO_RTCM        = SPI_ID(INT_ID_GPIO_RTCM),
    SPI_ID_PULSEC           = SPI_ID(INT_ID_PULSEC),
    SPI_ID_TIMER3           = SPI_ID(INT_ID_TIMER3),
    SPI_ID_TIMER4           = SPI_ID(INT_ID_TIMER4),
    SPI_ID_TIMER5           = SPI_ID(INT_ID_TIMER5),
    SPI_ID_SYSRTCALARM0     = SPI_ID(INT_ID_SYSRTCALARM0),
    SPI_ID_SYSRTCTIC        = SPI_ID(INT_ID_SYSRTCTIC),
    SPI_ID_SYSRTCALARM1     = SPI_ID(INT_ID_SYSRTCALARM1),
    SPI_ID_DMAC2            = SPI_ID(INT_ID_DMAC2),
    SPI_ID_DMAC3            = SPI_ID(INT_ID_DMAC3),
    SPI_ID_KASGPIO          = SPI_ID(INT_ID_A7CA),
    SPI_ID_PMU              = SPI_ID(INT_ID_PMU),
    SPI_ID_GMAC             = SPI_ID(INT_ID_GMAC),
    SPI_ID_IACC             = SPI_ID(INT_ID_IACC),
    SPI_ID_G2D              = SPI_ID(INT_ID_G2D),
    SPI_ID_LCD1             = SPI_ID(INT_ID_LCD1),
    SPI_ID_VIP1             = SPI_ID(INT_ID_VIP1),
    SPI_ID_LVDS             = SPI_ID(INT_ID_LVDS),
    SPI_ID_VPP1             = SPI_ID(INT_ID_VPP1),
    SPI_ID_UART3            = SPI_ID(INT_ID_UART3),
    SPI_ID_CBUS0            = SPI_ID(INT_ID_CBUS0),
    SPI_ID_CBUS1            = SPI_ID(INT_ID_CBUS1),
    SPI_ID_UART4            = SPI_ID(INT_ID_UART4),
    SPI_ID_GMAC_PMT         = SPI_ID(INT_ID_GMAC_PMT),
    SPI_ID_UART5            = SPI_ID(INT_ID_UART5),
    SPI_ID_MEDIAE0          = SPI_ID(INT_ID_MEDIAE0),
    SPI_ID_MEDIAE1          = SPI_ID(INT_ID_MEDIAE1),
    SPI_ID_TIMER6           = SPI_ID(INT_ID_TIMER6),
    SPI_ID_TIMER7           = SPI_ID(INT_ID_TIMER7),
    SPI_ID_TIMER8           = SPI_ID(INT_ID_TIMER8),
    SPI_ID_TIMER9           = SPI_ID(INT_ID_TIMER9),
    SPI_ID_TIMER10          = SPI_ID(INT_ID_TIMER10),
    SPI_ID_TIMER11          = SPI_ID(INT_ID_TIMER11),
    SPI_ID_DMACN_USP0       = SPI_ID(INT_ID_DMACN_USP0),
    SPI_ID_DMAC_NAND        = SPI_ID(INT_ID_DMAC_NAND),
    SPI_ID_CPU_PMU1         = SPI_ID(INT_ID_CPU_PMU1),
    SPI_ID_CPU_AXIERR       = SPI_ID(INT_ID_CPU_AXIERR),
    SPI_ID_DMACN_VIP1       = SPI_ID(INT_ID_DMACN_VIP1),
    SPI_ID_AFECVDVIP0       = SPI_ID(INT_ID_AFECVDVIP0),
    SPI_ID_DMAC0_SEC        = SPI_ID(INT_ID_DMAC0_SEC),
    SPI_ID_DMAC2_SEC        = SPI_ID(INT_ID_DMAC2_SEC),
    SPI_ID_DMAC3_SEC        = SPI_ID(INT_ID_DMAC3_SEC),
    SPI_ID_DMAC4_SEC        = SPI_ID(INT_ID_DMAC4_SEC),
    SPI_ID_LCD0_FRONT       = SPI_ID(INT_ID_LCD0_FRONT),
    SPI_ID_LCD1_FRONT       = SPI_ID(INT_ID_LCD1_FRONT),
    SPI_ID_M3_CTINTISR0     = SPI_ID(INT_ID_M3_CTINTISR0),
    SPI_ID_M3_CTINTISR1     = SPI_ID(INT_ID_M3_CTINTISR1),
    SPI_ID_CPU_CTIIRQ0      = SPI_ID(INT_ID_CPU_CTIIRQ0),
    SPI_ID_CPU_CTIIRQ1      = SPI_ID(INT_ID_CPU_CTIIRQ1),
    SPI_ID_DCU              = SPI_ID(INT_ID_DCU),
    SPI_ID_NOCFIFO          = SPI_ID(INT_ID_NOCFIFO),
    SPI_ID_SDIO67           = SPI_ID(INT_ID_SDIO67),
    SPI_ID_DMAC4            = SPI_ID(INT_ID_DMAC4),
    SPI_ID_UART6            = SPI_ID(INT_ID_UART6),
    SPI_ID_USP3             = SPI_ID(INT_ID_USP3),
    SPI_ID_AUDMSCM_INTR     = SPI_ID(INT_ID_NOC_AUDMSCM_INTR),
    SPI_ID_BTM_INTR         = SPI_ID(INT_ID_NOC_BTM_INTR),
    SPI_ID_CPUM_INTR        = SPI_ID(INT_ID_NOC_CPUM_INTR),
    SPI_ID_DDRM_INTR        = SPI_ID(INT_ID_NOC_DDRM_INTR),
    SPI_ID_GNSSM_INTR       = SPI_ID(INT_ID_NOC_GNSSM_INTR),
    SPI_ID_GPUM_INTR        = SPI_ID(INT_ID_NOC_GPUM_INTR),
    SPI_ID_MEDIAM_INTR      = SPI_ID(INT_ID_NOC_MEDIAM_INTR),
    SPI_ID_RTCM_INTR        = SPI_ID(INT_ID_NOC_RTCM_INTR),
    SPI_ID_VDIFM_INTR       = SPI_ID(INT_ID_NOC_VDIFM_INTR),








    SPI_ID_IPC_TRGT0_INIT1_INTR1   = SPI_ID(INT_ID_IPC_TRGT0_INIT1_INTR1),
    SPI_ID_IPC_TRGT0_INIT2_INTR1   = SPI_ID(INT_ID_IPC_TRGT0_INIT2_INTR1),
    SPI_ID_IPC_TRGT0_INIT3_INTR1   = SPI_ID(INT_ID_IPC_TRGT0_INIT3_INTR1),
    SPI_ID_IPC_TRGT0_INIT1_INTR2   = SPI_ID(INT_ID_IPC_TRGT0_INIT1_INTR2),
    SPI_ID_IPC_TRGT0_INIT2_INTR2   = SPI_ID(INT_ID_IPC_TRGT0_INIT2_INTR2),
    SPI_ID_IPC_TRGT0_INIT3_INTR2   = SPI_ID(INT_ID_IPC_TRGT0_INIT3_INTR2),
    SPI_ID_IPC_TRGT1_INIT0_INTR1   = SPI_ID(INT_ID_IPC_TRGT1_INIT0_INTR1),
    SPI_ID_IPC_TRGT1_INIT2_INTR1   = SPI_ID(INT_ID_IPC_TRGT1_INIT2_INTR1),
    SPI_ID_IPC_TRGT1_INIT3_INTR1   = SPI_ID(INT_ID_IPC_TRGT1_INIT3_INTR1),
    SPI_ID_IPC_TRGT1_INIT0_INTR2   = SPI_ID(INT_ID_IPC_TRGT1_INIT0_INTR2),
    SPI_ID_IPC_TRGT1_INIT2_INTR2   = SPI_ID(INT_ID_IPC_TRGT1_INIT2_INTR2),
    SPI_ID_IPC_TRGT1_INIT3_INTR2   = SPI_ID(INT_ID_IPC_TRGT1_INIT3_INTR2)};


/**
 * ID of 'spurious' interrupt
 *
 * This ID is returned when acknowledging an interrupt, if no interrupt is pending.
 * This is also returned when acknowledging an interrupt that has become masked due
 * to priority change but was already pending (sent to the CPU).
 */
#define GIC_SPURIOUS_INTERRUPT 1023


/**
 * bitwise selectors for target CPU
 */
enum target_e {
    TARGET_CPU0 = 0x1, ///< target to CPU0
    TARGET_CPU1 = 0x2, ///< target to CPU1
    TARGET_CPU2 = 0x4, ///< target to CPU2
    TARGET_CPU3 = 0x8  ///< target to CPU3
};


/**
 * Initialize the Interrupt Distributor
 *
 * Only one CPU should call this routine
 *
 * @retval false The distributor is not present and/or correct
 * @retval true  Initialization successful
 *
 */
bool gic_distributor_init(void);


/**
 * Initialize CPU Interface
 *
 * There is one CPU Interface per CPU, therefore all CPUs must call this routine
 *
 * @return No return value
 *
 */
void gic_cpu_interface_init(void);


/**
 * Type definition of the generic gic interrupt handler
 */
typedef void (gic_handler_t)(unsigned int gic_id);

/**
 * Enable an interrupt and install the handler
 *
 * @param gic_id    the GIC interrupt ID to be enabled(could be any SGI/PPI/SPI)
 * @param handler   the interrupt handler for the ID
 */
void gic_irq_enable(unsigned int gic_id, gic_handler_t handler);


/**
 * Disable an interrupt and remove the handler
 *
 * @param gic_id    the GIC interrupt ID to be disabled
 */
void gic_irq_disable(unsigned int gic_id);


/**
 * Type definition of the specific sgi interrupt handler
 */
typedef void (sgi_handler_t)(sgi_id_t id, unsigned int src);

/**
 * Enable a SGI and install the SGI handler
 *
 * If a SGI has both a gic_handler and a sgi_handler set,
 * the sgi_handler has higher priority.
 *
 * @param id        the SGI ID to be enabled
 * @param handler   the interrupt handler for the ID
 */
void gic_sgi_enable(sgi_id_t id, sgi_handler_t handler);

/**
 * Broadcast a SGI to other cores
 *
 * @param id        the SGI ID to be sent
 */
void sgi_broadcast_to_others(sgi_id_t id);


/**
 * Add a target for an interrupt
 *
 * @param gic_id
 *          the gic ID whose targets to change
 * @param cpu_id
 *          the cpu_id to be added into targets
 *
 */
void gic_add_target(unsigned int gic_id, unsigned int cpu_id);

/**
 * Remove a target for an interrupt
 *
 * @param gic_id
 *          the gic ID whose targets to change
 * @param cpu_id
 *          the cpu_id to be removed from targets
 *
 */
void gic_remove_target(unsigned int gic_id, unsigned int cpu_id);

/**
 * Get targets for an interrupt
 *
 * @param gic_id
 *          the gic ID whose targets to get
 *
 * @return A bitwise OR of @ref target_e
 *
 */
unsigned char gic_get_targets(unsigned int gic_id);

/**
 * Set targets for an interrupt
 *
 * @param gic_id
 *          the gic ID whose targets to be set
 * @param target_list
 *          A bitwise OR of @ref target_e
 *
 * @return No return value
 */
void gic_set_targets(unsigned int gic_id, unsigned int target_list);

/**
 * Get priority for an interrupt
 *
 * @param gic_id
 *          the gic ID whose priority to get
 *
 * @return The priority of the gic ID
 */
unsigned char gic_get_priority(unsigned int gic_id);

/**
 * Set priority for an interrupt
 *
 * @param gic_id
 *          the gic ID whose priority to be set
 * @param priority
 *          the new priority to set
 *
 * @return The old priority of the gic ID
 */
unsigned char gic_set_priority(unsigned int gic_id, unsigned char priority);

/**
 * Get priority_mask of cpu interface
 *
 * @return The priority_mask
 */
unsigned char gic_get_priority_mask(void);

/**
 * Set priority_mask of cpu interface
 *
 * @param priority_mask
 *          the new priority_mask to set
 *
 * @return The old priority_mask
 */
unsigned char gic_set_priority_mask(unsigned char priority_mask);

/**
 * Get highest priority pending interrupt id from cpu interface
 *
 * Note: highest priority pending register only effect after the corresponding interrupt is enabled in distributor
 *
 * @return the highest priority pending interrupt id
 */
unsigned int gic_get_highest_priority_pending_id(void);

/**
 * Get running priority
 *
 * @return the running priority
 */
unsigned char gic_get_running_priority(void);

/**
 * Gheck if a SPI is pending on distributor interface
 *
 * @param gic_id
 *          the gic ID to check. The gic_id must >= 32
 *
 * @return True if there is a pending SPI for the gic_id
 */
bool gic_is_spi_pending(unsigned int gic_id);

/**
 * Gheck if any SPI is pending on distributor interface
 *
 * @return True if there is a pending SPI
 */
bool gic_has_pending_spi(void);

/**
 * Get the smallest pending SPI id on distributor interface
 *
 * @retval The smallest pending SPI id
 * @retval GIC_SPURIOUS_INTERRUPT if there is no SPI pending
 */
unsigned int gic_get_smallest_pending_spi(void);

/**
 * Print status of all pending SPIs on distributor interface
 */
void gic_print_spi_status(void);

/**
 * Print all pending SPIs on distributor interface
 */
void gic_print_all_pending_spi(void);

////////////////////////////////////////////////////////////////////////////////

/**
 * Gheck if a gic ID is pending on distributor interface
 *
 * Note: pending register will be enabled only if the target register for the corresponding interrupt is non-zero
 *
 * @param gic_id
 *          the gic ID to check
 *
 * @return True if there is a pending interrupt for the gic_id
 */
bool gic_is_id_pending(unsigned int gic_id);

/**
 * Get the smallest pending id on distributor interface
 *
 * Note: pending register will be enabled only if the target register for the corresponding interrupt is non-zero
 *
 * @retval The smallest pending id
 * @retval GIC_SPURIOUS_INTERRUPT if there is no interrupt pending
 */
unsigned int gic_get_smallest_pending_id(void);

/**
 * Print all pending IDs on distributor interface
 *
 * Note: pending register will be enabled only if the target register for the corresponding interrupt is non-zero
 *
 */
void gic_print_all_pending_id(void);

#ifdef __cplusplus
    }
#endif

#endif // _SOC_GIC_H_
