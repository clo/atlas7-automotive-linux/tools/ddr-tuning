/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef __CACHE_H__
#define __CACHE_H__

void flush_dcache_all(void);
void invalidate_dcache_all(void);
void invalidate_icache_all(void);

void enable_i_cache( void );
void disable_i_cache( void );
void enable_d_cache( void );
void disable_d_cache( void );

#endif // __CACHE_H__

