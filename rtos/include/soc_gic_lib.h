/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#ifndef _SOC_GIC_LIB_H_
#define _SOC_GIC_LIB_H_

#include "soc_gic.h"

#ifdef __ARMCC_VERSION /* { */
#define PACKED_GCC
#define PACKED_ARMCC            __packed
#else /* __ARMCC_VERSION }{ */
#define PACKED_GCC              __attribute(( packed ))
#define PACKED_ARMCC
#endif /* } */

/// Total number of implemented interrupts in GIC
#define GIC_NUM_INTERRUPTS (GIC_NUM_SGI + GIC_NUM_PPI + GIC_NUM_SPI)


/**
 * Target list filter for Software Interrupt register
 */
enum targetlist_filter_e {
    SGI_USE_TARGET_LIST = 0x0, ///< Interrupt sent to the CPUs listed in CPU Target List
    SGI_ALL_BUT_SELF    = 0x1, ///< CPU Target list ignored, interrupt is sent to all but requesting CPU
    SGI_SELF            = 0x2  ///< CPU Target list ignored, interrupt is sent to requesting CPU only
};


/**
 * Number of priority bits
 *
 * It's the prority bits implemented in the GIC.
 * - priority[7:(8-GIC_PRIORITY_BITS)] is used as priority
 * - priority[(GIC_PRIORITY_BITS-1):0] is reserved
 */
#define GIC_PRIORITY_BITS 5

/**
 * Some special priority values
 *
 * The CPU interface sends an interrupt to the CPU if its priority (set in
 * the distributor) is strictly higher than the mask set in the Priority Mask Register.
 * One consequence of the strict comparison is that a Pending interrupt with the lowest
 * possible priority will never causes the assertion of an interrupt request to CPUs.
 *
 */
#define PRIORITY_HIGHEST       0x0  ///<
/**
 * Lowest priority level
 *
 * An interrupt with this priority level will never pass through GIC to CPU
 */
#define PRIORITY_LOWEST        ((1<<GIC_PRIORITY_BITS)-1)
#define PRIORITY_ALLOW_ALL     0xFF  ///< special value. used to set priority mask to allow all priority to pass through


// ==============================================================
// GIC ICD/ICC offset from periph_base
// ==============================================================
#define MP_ICD_OFFSET        0x1000  ///<  Interrupt Controller Distributor offset from PERIPH_BASE
#define MP_ICC_OFFSET_A9     0x0100  ///<  Interrupt Controller CPU interface offset from PERIPH_BASE, for Cortex-A9
#define MP_ICC_OFFSET_A7     0x2000  ///<  Interrupt Controller CPU interface offset from PERIPH_BASE, for Cortex-A7 (see ARM Cortex-A7 MPCore Technical Reference Manual, 8.2.1, GIC memory-map).
#define MP_ICC_OFFSET        MP_ICC_OFFSET_A7

// ==============================================================
// GIC registers definitions
// ==============================================================

// Number of 32 bit registers necessary in order to allocate enough space
// for 1020 irqs (1 bit each) for the following:
// - Enable Set / Enable Clear
// - Pending Set / Pending Clear
// - Active Bit
#define GIC_NUM_REGISTERS 32

// for 1020 irqs (8 bit each) for the priority field
#define GIC_PRIORITY_REGISTERS 1024

// Number of byte registers necessary in order to allocate enough space
// for 1020 irqs (8 bit each) for the CPU targets field
#define GIC_TARGET_REGISTERS 1024

// Number of 4 byte registers necessary in order to allocate enough space
// for 1020 irqs (2 bit each) for the configuration field
#define GIC_CONFIG_REGISTERS 64


// Number of 4 byte registers necessary in order to allocate enough space
// for 224 irqs (1 bit each) for the configuration field
#define GIC_SPI_STATUS_REGISTERS 7



// Inform armcc to enable anonymous union/struct
// ---------------------------------------------
#ifdef __ARMCC_VERSION /* { */
#pragma anon_unions
#endif /* __ARMCC_VERSION } */


// Interface Distributor registers
// ====================================================================

/**
 * Interrupt Distributor -- Distributor Control Register
 */
typedef union {
    struct {
        // LSB first
        unsigned enable_secure      : 1;
        unsigned enable_nonsecure   : 1;
        unsigned reserved           :30;
    };
    uint32_t all;
} ICDDCR;

// Access structure for set/clear set of registers
// -----------------------------------------------
struct set_and_clear_regs
{
    volatile uint32_t set[GIC_NUM_REGISTERS], clear[GIC_NUM_REGISTERS];
};

/**
 * Interrupt Distributor -- Interrupt Priority Register(Byte)
 */
typedef PACKED_ARMCC union {
    PACKED_ARMCC struct {
        // LSB first
        unsigned reserved           : (8-GIC_PRIORITY_BITS);
        unsigned priority           : GIC_PRIORITY_BITS;
    } PACKED_GCC;
    uint8_t all;
} PACKED_GCC ICDIPR;

/**
 * Interrupt Distributor -- Software Generate Interrupt Register
 */
typedef struct {
    // LSB first
    unsigned sgi_id             : 4;
    unsigned reserved           :11;
    unsigned nsatt              : 1;
    unsigned targetlist         : 8;
    unsigned targetlist_filter  : 2;
    unsigned reserved1          : 6;
} ICDSGIR;


/**
 * Interrupt Distributor interface access structure
 */
typedef struct
{
    /* 0x000 */  volatile/*FIXME*/ ICDDCR control; ///< Interrupt Distributor Control Register (R/W)

    /* 0x004 */  volatile const uint32_t controller_type; /* Interrupt Controller Type Register (RO) */

    /* 0x008 */  const uint32_t reserved1[62]; // Reserved
    // FIXME: 0x080 should add ICDISRn definition

    /* 0x100 */  struct set_and_clear_regs enable; ///< Interrupt Set/Clear Enable Registers (R/W)

    /* 0x200 */  struct set_and_clear_regs pending; ///< Interrupt Set/Clear Pending Registers (R/W)

    /* 0x300 */  volatile const uint32_t active[GIC_NUM_REGISTERS]; ///< Interrupt Active Bit Registers (RO)

    /* 0x380 */  const uint32_t reserved2[32]; // Reserved

    /* 0x400 */  volatile ICDIPR priority[GIC_PRIORITY_REGISTERS]; ///< Interrupt Priority Registers (R/W)

    /* 0x800 */  volatile uint8_t target[GIC_TARGET_REGISTERS]; ///< Interrupt CPU Target Registers (R/W)

    /* 0xC00 */  volatile uint32_t configuration[GIC_CONFIG_REGISTERS]; ///< Interrupt Configuration Registers (R/W)

    /* 0xD00 */  volatile const uint32_t ppi_status; ///< PPI Status Register (RO)

    /* 0xD04 */  volatile const uint32_t spi_status[GIC_SPI_STATUS_REGISTERS]; ///< SPI Status Register (RO)

    /* 0xD00 */  const uint32_t reserved3[127-GIC_SPI_STATUS_REGISTERS]; // Reserved

    /* 0xF00 */  volatile ICDSGIR software_interrupt; ///< Software Interrupt Register (WO)

    /* 0xF04 */  const uint32_t reserved4[55]; // Reserved

    /* 0xFE0 */  volatile const uint32_t peripheral_id[4]; ///< Peripheral Identification Registers (RO)

    /* 0xFF0 */  volatile const uint32_t primecell_id[4]; ///< PrimeCell Identification Registers (RO)

} distributor_interface;



// CPU Interface registers
// * These registers are banked for each CPU
// =============================================================================

/**
 * CPU interface -- Interrupt Control Register
 */
typedef struct {
    // LSB first
    unsigned enable             : 1;
    unsigned reserved           :31;
} ICCICR;

/**
 * CPU interface -- Interrupt Priority Mask Register
 */
typedef struct {
    // LSB first
    unsigned reserved           : (8-GIC_PRIORITY_BITS);
    unsigned priority           : GIC_PRIORITY_BITS;
    unsigned reserved1          :24;
} ICCPMR;

/**
 * CPU interface -- Binary Point Register
 */
typedef struct {
    // LSB first
    unsigned binary_point       : 3;
    unsigned reserved           :29;
} ICCBPR;
/// Binary point bit values
enum binary_point_e {
    BINARY_COMPARE_ALL     = 0x2, ///< All priority bits are compared for pre-emption
    BINARY_COMPARE_7_to_4  = 0x3, ///< Only bits [7:4] of the priority are compared for pre-emption
    BINARY_COMPARE_7_to_5  = 0x4, ///< Only bits [7:5] of the priority are compared for pre-emption
    BINARY_COMPARE_7_to_6  = 0x5, ///< Only bits [7:6] of the priority are compared for pre-emption
    BINARY_COMPARE_7       = 0x6, ///< Only bit [7] of the priority is compared for pre-emption
    BINARY_NO_PREEMPTION   = 0x7  ///< No pre-emprion is performed
};


/**
 * CPU interface -- Interrupt Acknowledge Register
 */
typedef struct {
    // LSB first
    unsigned id                 :10;
    unsigned src_cpuid          : 3;
    unsigned reserved           :19;
} ICCIAR;

/**
 * CPU interface -- End Of Interrupt Register
 */
typedef ICCIAR ICCEOIR;

/**
 * CPU interface -- Running Priority Register
 */
typedef ICCPMR ICCRPR;

/**
 * CPU interface -- Highest Pending Interrupt Register
 */
typedef ICCIAR ICCHPIR;


/**
 * CPU Interface register access structure
 */
typedef struct
{
    /* 0x00 */  volatile ICCICR control; ///< Control Register (R/W)

    /* 0x04 */  volatile ICCPMR priority_mask; ///< Priority Mask Register (R/W)

    /* 0x08 */  volatile ICCBPR binary_point; ///< Binary Point Register (R/W)

    /* 0x0C */  volatile const ICCIAR interrupt_ack; ///< Interrupt Acknowledge Register (RO)

    /* 0x10 */  volatile ICCEOIR end_of_interrupt; ///< End of Interrupt Register (W)

    /* 0x14 */  volatile const ICCRPR running_priority; ///< Running Priority Register (RO)

    /* 0x18 */  volatile const ICCHPIR highest_pending; ///< Highest Pending Interrupt Register (RO)

} cpu_interface;


// ===============================================================
// functions
// ===============================================================


/**
 * Send an SGI to a bitmask of CPUs
 *
 * @param target_list
 *          A bitwise OR of @ref target_e
 * @param id
 *          SGI ID to be asserted
 *
 * @return No return value
 */
void sgi_send(unsigned int target_list, sgi_id_t id);


/**
 * Send an SGI to the requesting CPU
 *
 * @param id SGI ID to be asserted
 *
 * @return No return value
 *
 */
void sgi_send_to_self(sgi_id_t id);


#endif
