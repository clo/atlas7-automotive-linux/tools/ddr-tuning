/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    chip_utils.h
//
// Abstract: 
//   
// 
// Notes:
//    
//
/*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#ifndef __CHIP_UTILS_H__
#define __CHIP_UTILS_H__

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#include "ioctop.h"
#include "soc_fc_gpio_vdifm.h"

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define WRITE_REG(a, d) (*(volatile unsigned long*)(a) = (unsigned long)(d))
#define READ_REG(a)     (*(volatile unsigned long*)(a))
#define SET_IO_REG(ADDR,VAL)  (*(volatile unsigned*) (ADDR) = (VAL))
// RD_MOD_WR: address, and-value, or-value
#define RD_MOD_WR(a,and_value,or_value) WRITE_REG(a, (READ_REG(a) & and_value) | or_value)
// poll: address, mask, value, limit, error
#define POLL_REG(a,m,v,l,e) {                                                   \
                                unsigned int wd=l;                              \
                                while ( (READ_REG(a)&m) != (v&m) ) {            \
                                  if (--wd == 0) {                              \
                                    return(e);                                  \
                                  }                                             \
                                }                                               \
                        }

#define RAM_OFFSET_DWORD(x) (*(volatile unsigned int  *) ((x) & 0xFFFFFFFC))
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define  PADOPT_COUNT__GP__GPIO_3  1
// padopt: 
//   0 = x_gpio_3
#define set_funcsel__gp__gpio_3(padopt) \
    ( ((padopt) == 0) ? (SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_CLR, SW_TOP_FUNC_SEL_16_REG_CLR__GPIO_3__MASK), \
                         SET_IO_REG(SW_TOP_FUNC_SEL_16_REG_SET, ((0 << SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__SHIFT) & SW_TOP_FUNC_SEL_16_REG_SET__GPIO_3__MASK)), \
                         1) \
    : 0)

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define DDR_ENABLE_LDO_WA

// DDR setup parameters
struct stDdrSetup {
   int targetFreq;
   unsigned int freq_x16;
   unsigned long xtalFreq;
   unsigned long apbFreq;
   long clkPuOffset;
   long clkPdOffset;
   long clkDelay;
   long acPuOffset;
   long acPdOffset;
   long Atlas7DataRout;
   long Atlas7DataOdt;
   long DramDataRout;
   long DramDataOdt;
};

enum eDdrParam {
   e_targetFreq,     /*  0 */
   e_xtalFreq,       /*  1 */
   e_apbFreq,        /*  2 */
   e_clkDelay,       /*  3 */
   e_clkPuOffset,    /*  4 */
   e_clkPdOffset,    /*  5 */
   e_acPuOffset,     /*  6 */
   e_acPdOffset,     /*  7 */
   e_Atlas7DataRout, /*  8 */
   e_Atlas7DataOdt,  /*  9 */
   e_DramDataRout,   /* 10 */
   e_DramDataOdt,    /* 11 */
   e_tREFI,          /* 12 */
   e_tRFC,           /* 13 */
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void idle(int iter);
int Reg_Polling(unsigned int laddress, unsigned int lmask, unsigned int lval, int ilimit);
void BlinkLed(unsigned int lcycles);
int g2d_render(unsigned long* pSrc, unsigned long* pTgt, unsigned long lWidthInDW, unsigned long lHeight);
int ddr_freq_set(unsigned int target_ddr_freq, unsigned int xtal_freq);
int ddr_bist_dram(unsigned int row_bits, unsigned int addr, unsigned int size, unsigned int loops);
void dump_ddrphy_regs();
void dump_upctl_regs(void);

void setDdrDebugPrint(unsigned long ddrDebugPrint);
void initDdrSetupParameters();
void setDdrParameter(enum eDdrParam pIdx,long value);
void printDdrParameters();
void winCheck(char *name,unsigned int addr,unsigned int mask,int startbit,int byte);
int runDdrInit ();

int ddr_test(unsigned int i0, unsigned int i1, unsigned int i2, unsigned int i3, unsigned int i4);
int ddr_bist_loopback(void);
int ddr_range_check(unsigned int addr, unsigned int size, unsigned int loop, unsigned int op, unsigned int wait_mask);
int ddr_memvex(unsigned int addr, unsigned int size, unsigned int op, unsigned int timeout);
void wait_us(unsigned int n );
void wait_100ns(unsigned int n);
int ddr_self_refresh_enter(void);

void rtcwrite(unsigned long laddress, unsigned long ldata);
unsigned long rtcread ( unsigned long laddress);

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#endif //__CHIP_UTILS_H__

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
