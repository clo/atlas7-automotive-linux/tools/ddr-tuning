/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    debug.h
//
// Abstract: 
//    just function declarations.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _DEBUG_H_
#define _DEBUG_H_


extern DWORD g_dwTimerTicks;

void DbgInit(int baudrate);
void DbgPutString(const char *str);
void DbgPutHex(DWORD u_value, DWORD dwByteSize);
void DbgPutDec(unsigned num);
char * DbgGetString(void);
char DbgGetChar(void);




DWORD DumpStatckUseage(void);
void DumpBuffer(BYTE *pbuffer,DWORD size);
void RawDumpBuffer(BYTE *pBuffer,DWORD size);
void _DebugMsg (char *sz, ...);
void _debug_print_data_byte(char * str, PBYTE buff, DWORD size);

#define UPPERDIV(Dividend, Divisor)         (((Dividend) + (Divisor) -1) / (Divisor))

#define DebugMsg _DebugMsg 
#define debug_print_data_byte _debug_print_data_byte


#define DiagPrintf _DebugMsg 


#endif //_DEBUG_H_

