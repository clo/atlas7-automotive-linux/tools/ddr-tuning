/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */
/*****************************************************************************/
// Module Name: 
//    otp.h
//
// Abstract: 
//    MACROs for otp.c.
// 
// Notes:
//    
//
/*****************************************************************************/

#ifndef _RINGOSC_H_
#define _RINGOSC_H_

#include "soc.h"
#include "otp.h"
#include "dvm.h"


/***************************************************************************************\
| DVM definitions
\***************************************************************************************/
#define DVM_COUNT_NUM	    20
#define DVM_TYPE_NUM	    16

 
#define DVM_CPU_READ_EN_BIT 7
 
//#define OSC_XIN_CLK_RATIO   5.2 
#define OSC_XIN_CLK_RATIO   5 
#define OSC_XIN_CLK_MARGIN  5


/***************************************************************************************\
| DVM functions
\***************************************************************************************/

int ringosc_test () ;
int ringosc_one () ;


//////////////////end function
#endif //_RINGOSC_H_

