/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */


#ifndef __DDRBIST_H__
#define __DDRBIST_H__



/* DDR_BIST */
/* ==================================================================== */

/* control register */
/* control register */
#define DDR_BIST_CONTROL          0x10828000

/* DDR_BIST_CONTROL.ACTIVE - write '1' to activate the bist. self clear to '0' when bist operation is done. */
#define DDR_BIST_CONTROL__ACTIVE__SHIFT       0
#define DDR_BIST_CONTROL__ACTIVE__WIDTH       1
#define DDR_BIST_CONTROL__ACTIVE__MASK        0x00000001
#define DDR_BIST_CONTROL__ACTIVE__INV_MASK    0xFFFFFFFE
#define DDR_BIST_CONTROL__ACTIVE__HW_DEFAULT  0x0

/* DDR_BIST_CONTROL.CYCLIC - if set, bist will re-start immediatly after it finishes, until this bit is cleared or until error occured and CONTROL.CYCLIC_STOP_ERR=1. */
#define DDR_BIST_CONTROL__CYCLIC__SHIFT       1
#define DDR_BIST_CONTROL__CYCLIC__WIDTH       1
#define DDR_BIST_CONTROL__CYCLIC__MASK        0x00000002
#define DDR_BIST_CONTROL__CYCLIC__INV_MASK    0xFFFFFFFD
#define DDR_BIST_CONTROL__CYCLIC__HW_DEFAULT  0x0

/* DDR_BIST_CONTROL.CYCLIC_STOP_ERR - if CONTROL.CYCLIC=1 and CONTROL.CYCLIC_STOP_ERR=1, bist will stop at the end of the first loop in which error is encountered. */
#define DDR_BIST_CONTROL__CYCLIC_STOP_ERR__SHIFT       2
#define DDR_BIST_CONTROL__CYCLIC_STOP_ERR__WIDTH       1
#define DDR_BIST_CONTROL__CYCLIC_STOP_ERR__MASK        0x00000004
#define DDR_BIST_CONTROL__CYCLIC_STOP_ERR__INV_MASK    0xFFFFFFFB
#define DDR_BIST_CONTROL__CYCLIC_STOP_ERR__HW_DEFAULT  0x0

/* DDR_BIST_CONTROL.LOOP_CNT - number of loops finished so far. */
#define DDR_BIST_CONTROL__LOOP_CNT__SHIFT       8
#define DDR_BIST_CONTROL__LOOP_CNT__WIDTH       8
#define DDR_BIST_CONTROL__LOOP_CNT__MASK        0x0000FF00
#define DDR_BIST_CONTROL__LOOP_CNT__INV_MASK    0xFFFF00FF
#define DDR_BIST_CONTROL__LOOP_CNT__HW_DEFAULT  0x0

/* configuration register */
/* configuration register */
#define DDR_BIST_CONFIG           0x10828004

/* DDR_BIST_CONFIG.WRITE - should the bist operation include write to dram (1) or not (0). CONFIG.WRITE and CONFIG.READ may not be both 0. */
#define DDR_BIST_CONFIG__WRITE__SHIFT       0
#define DDR_BIST_CONFIG__WRITE__WIDTH       1
#define DDR_BIST_CONFIG__WRITE__MASK        0x00000001
#define DDR_BIST_CONFIG__WRITE__INV_MASK    0xFFFFFFFE
#define DDR_BIST_CONFIG__WRITE__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.READ - should the bist operation include read from dram (1) or not (0). CONFIG.WRITE and CONFIG.READ may not be both 0. */
#define DDR_BIST_CONFIG__READ__SHIFT       1
#define DDR_BIST_CONFIG__READ__WIDTH       1
#define DDR_BIST_CONFIG__READ__MASK        0x00000002
#define DDR_BIST_CONFIG__READ__INV_MASK    0xFFFFFFFD
#define DDR_BIST_CONFIG__READ__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.RW - should the read be done simultaneously with the write (1) or after the write is done (0). may be configured '1' only when both write and read are performed (CONFIG.WRITE and CONFIG.READ are both 1). */
#define DDR_BIST_CONFIG__RW__SHIFT       2
#define DDR_BIST_CONFIG__RW__WIDTH       1
#define DDR_BIST_CONFIG__RW__MASK        0x00000004
#define DDR_BIST_CONFIG__RW__INV_MASK    0xFFFFFFFB
#define DDR_BIST_CONFIG__RW__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.WR_MASK - if set, the written data is masked with MASK* parameters. i.e. in each chunk of 128 bits, bits marked with '1' in MASK* parameters are taken from the data to be written, and bits marked with '0' in MASK* parameters are taken from CONFIG.WR_MASK_DATA. */
#define DDR_BIST_CONFIG__WR_MASK__SHIFT       4
#define DDR_BIST_CONFIG__WR_MASK__WIDTH       1
#define DDR_BIST_CONFIG__WR_MASK__MASK        0x00000010
#define DDR_BIST_CONFIG__WR_MASK__INV_MASK    0xFFFFFFEF
#define DDR_BIST_CONFIG__WR_MASK__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.RD_MASK - if set, check only bits marked 1 in MASK* parameters. */
#define DDR_BIST_CONFIG__RD_MASK__SHIFT       5
#define DDR_BIST_CONFIG__RD_MASK__WIDTH       1
#define DDR_BIST_CONFIG__RD_MASK__MASK        0x00000020
#define DDR_BIST_CONFIG__RD_MASK__INV_MASK    0xFFFFFFDF
#define DDR_BIST_CONFIG__RD_MASK__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.WR_MASK_DATA - if CONFIG.WR_MASK=1, all bits marked '0' in MASK* parameters are written with CONFIG.WR_MASK_DATA value. */
#define DDR_BIST_CONFIG__WR_MASK_DATA__SHIFT       6
#define DDR_BIST_CONFIG__WR_MASK_DATA__WIDTH       1
#define DDR_BIST_CONFIG__WR_MASK_DATA__MASK        0x00000040
#define DDR_BIST_CONFIG__WR_MASK_DATA__INV_MASK    0xFFFFFFBF
#define DDR_BIST_CONFIG__WR_MASK_DATA__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.BURST_LEN - axi burst length (awlen/arlen) will be 2^CONFIG.BURST_LEN data transfers (each of 64 bits). */
/* 0:4 : for 1,2,4,8,16 transfers of 64 bits each. */
#define DDR_BIST_CONFIG__BURST_LEN__SHIFT       8
#define DDR_BIST_CONFIG__BURST_LEN__WIDTH       3
#define DDR_BIST_CONFIG__BURST_LEN__MASK        0x00000700
#define DDR_BIST_CONFIG__BURST_LEN__INV_MASK    0xFFFFF8FF
#define DDR_BIST_CONFIG__BURST_LEN__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.DATA_TYPE - data type to be read/written */
/* 0 : RAM: write cyclicly the 128 bits in DATA* parameters. in terms of linear address this is {DATA1H,DATA1L,DATA0H,DATA0L}. in terms of DDR physical address each such 128 bits are done in burst of 8 accesses of 16 bits in this order:
 {DATA0L[ 7: 0], DATA0H[ 7: 0]}
 {DATA0L[15: 8], DATA0H[15: 8]}
 {DATA0L[23:16], DATA0H[23:16]}
 {DATA0L[31:24], DATA0H[31:24]}
 {DATA1L[ 7: 0], DATA1H[ 7: 0]}
 {DATA1L[15: 8], DATA1H[15: 8]}
 {DATA1L[23:16], DATA1H[23:16]}
 {DATA1L[31:24], DATA1H[31:24]} */
/* 1 : LFSR: the first 64 bits will be {DATA0H,DATA0L}, and then each consecutive word will be lfsr function on the previous word, on each 32 bits independently. lfsr function is: next_data = {data[30:0], data[31]^data[28]}; */
/* 2:3 : RESERVED */
#define DDR_BIST_CONFIG__DATA_TYPE__SHIFT       12
#define DDR_BIST_CONFIG__DATA_TYPE__WIDTH       2
#define DDR_BIST_CONFIG__DATA_TYPE__MASK        0x00003000
#define DDR_BIST_CONFIG__DATA_TYPE__INV_MASK    0xFFFFCFFF
#define DDR_BIST_CONFIG__DATA_TYPE__HW_DEFAULT  0x0

/* DDR_BIST_CONFIG.RW_MAX_GAP - for interleaved wr-rd mode (CONFIG.RW=1), determines the maximum gap (minus 1) allowed between write and read in terms of axi requests. */
/* 0:0xfe : for max gap of 1..0xff axi requests. */
#define DDR_BIST_CONFIG__RW_MAX_GAP__SHIFT       16
#define DDR_BIST_CONFIG__RW_MAX_GAP__WIDTH       8
#define DDR_BIST_CONFIG__RW_MAX_GAP__MASK        0x00FF0000
#define DDR_BIST_CONFIG__RW_MAX_GAP__INV_MASK    0xFF00FFFF
#define DDR_BIST_CONFIG__RW_MAX_GAP__HW_DEFAULT  0x0

/* address */
/* address */
#define DDR_BIST_ADDRESS          0x10828008

/* DDR_BIST_ADDRESS.ADDRESS - start address of the transaction (bits[31:0] represent byte address). must be aligned to axi burst length. e.g. if CONFIG.BURST_LEN=3, each burst is 2^3=8 transfers of 64 bits = 64B, then ADDRESS[5:0] must be zeroes. */
#define DDR_BIST_ADDRESS__ADDRESS__SHIFT       3
#define DDR_BIST_ADDRESS__ADDRESS__WIDTH       29
#define DDR_BIST_ADDRESS__ADDRESS__MASK        0xFFFFFFF8
#define DDR_BIST_ADDRESS__ADDRESS__INV_MASK    0x00000007
#define DDR_BIST_ADDRESS__ADDRESS__HW_DEFAULT  0x0

/* size */
/* size */
#define DDR_BIST_SIZE             0x1082800C

/* DDR_BIST_SIZE.SIZE - size of the transaction (bits[31:0] represent size in bytes). must be positive and multiple of axi burst length. e.g. if CONFIG.BURST_LEN=3, each burst is 2^3=8 transfers of 64 bits = 64B, then SIZE[5:0] must be zeroes. ADDRESS+SIZE should be <= actual DRAM size. */
#define DDR_BIST_SIZE__SIZE__SHIFT       3
#define DDR_BIST_SIZE__SIZE__WIDTH       29
#define DDR_BIST_SIZE__SIZE__MASK        0xFFFFFFF8
#define DDR_BIST_SIZE__SIZE__INV_MASK    0x00000007
#define DDR_BIST_SIZE__SIZE__HW_DEFAULT  0x0

/* status register */
/* status register. each bit in this register is set upon the relevant error event, and reset only at the beginning of bist operation. in case of cyclic run (CONTROL.CYCLIC=1) the error bit is reset only at the beginning of the first loop, i.e. error indications are saved as long as the cyclic bist is running. */
#define DDR_BIST_STATUS           0x10828010

/* DDR_BIST_STATUS.BIST_ERROR - error occured in comparison of read data. */
#define DDR_BIST_STATUS__BIST_ERROR__SHIFT       0
#define DDR_BIST_STATUS__BIST_ERROR__WIDTH       1
#define DDR_BIST_STATUS__BIST_ERROR__MASK        0x00000001
#define DDR_BIST_STATUS__BIST_ERROR__INV_MASK    0xFFFFFFFE
#define DDR_BIST_STATUS__BIST_ERROR__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.WR_PEND_OVR - overflow of counter counting the pending write requests. */
#define DDR_BIST_STATUS__WR_PEND_OVR__SHIFT       8
#define DDR_BIST_STATUS__WR_PEND_OVR__WIDTH       1
#define DDR_BIST_STATUS__WR_PEND_OVR__MASK        0x00000100
#define DDR_BIST_STATUS__WR_PEND_OVR__INV_MASK    0xFFFFFEFF
#define DDR_BIST_STATUS__WR_PEND_OVR__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.WR_PEND_UND - underflow of counter counting the pending write requests. */
#define DDR_BIST_STATUS__WR_PEND_UND__SHIFT       9
#define DDR_BIST_STATUS__WR_PEND_UND__WIDTH       1
#define DDR_BIST_STATUS__WR_PEND_UND__MASK        0x00000200
#define DDR_BIST_STATUS__WR_PEND_UND__INV_MASK    0xFFFFFDFF
#define DDR_BIST_STATUS__WR_PEND_UND__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.RD_PEND_OVR - overflow of counter counting the pending read requests. */
#define DDR_BIST_STATUS__RD_PEND_OVR__SHIFT       10
#define DDR_BIST_STATUS__RD_PEND_OVR__WIDTH       1
#define DDR_BIST_STATUS__RD_PEND_OVR__MASK        0x00000400
#define DDR_BIST_STATUS__RD_PEND_OVR__INV_MASK    0xFFFFFBFF
#define DDR_BIST_STATUS__RD_PEND_OVR__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.RD_PEND_UND - underflow of counter counting the pending read requests. */
#define DDR_BIST_STATUS__RD_PEND_UND__SHIFT       11
#define DDR_BIST_STATUS__RD_PEND_UND__WIDTH       1
#define DDR_BIST_STATUS__RD_PEND_UND__MASK        0x00000800
#define DDR_BIST_STATUS__RD_PEND_UND__INV_MASK    0xFFFFF7FF
#define DDR_BIST_STATUS__RD_PEND_UND__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.RW_GAP_OVR - overflow of counter counting the gap between write done (axi b) and read requests (axi ar). */
#define DDR_BIST_STATUS__RW_GAP_OVR__SHIFT       12
#define DDR_BIST_STATUS__RW_GAP_OVR__WIDTH       1
#define DDR_BIST_STATUS__RW_GAP_OVR__MASK        0x00001000
#define DDR_BIST_STATUS__RW_GAP_OVR__INV_MASK    0xFFFFEFFF
#define DDR_BIST_STATUS__RW_GAP_OVR__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.RW_GAP_UND - underflow of counter counting the gap between write done (axi b) and read requests (axi ar). */
#define DDR_BIST_STATUS__RW_GAP_UND__SHIFT       13
#define DDR_BIST_STATUS__RW_GAP_UND__WIDTH       1
#define DDR_BIST_STATUS__RW_GAP_UND__MASK        0x00002000
#define DDR_BIST_STATUS__RW_GAP_UND__INV_MASK    0xFFFFDFFF
#define DDR_BIST_STATUS__RW_GAP_UND__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.WR_GAP_OVR - overflow of counter counting the gap between write requests (axi aw) and read requests (axi ar). */
#define DDR_BIST_STATUS__WR_GAP_OVR__SHIFT       14
#define DDR_BIST_STATUS__WR_GAP_OVR__WIDTH       1
#define DDR_BIST_STATUS__WR_GAP_OVR__MASK        0x00004000
#define DDR_BIST_STATUS__WR_GAP_OVR__INV_MASK    0xFFFFBFFF
#define DDR_BIST_STATUS__WR_GAP_OVR__HW_DEFAULT  0x0

/* DDR_BIST_STATUS.WR_GAP_UND - underflow of counter counting the gap between write requests (axi aw) and read requests (axi ar). */
#define DDR_BIST_STATUS__WR_GAP_UND__SHIFT       15
#define DDR_BIST_STATUS__WR_GAP_UND__WIDTH       1
#define DDR_BIST_STATUS__WR_GAP_UND__MASK        0x00008000
#define DDR_BIST_STATUS__WR_GAP_UND__INV_MASK    0xFFFF7FFF
#define DDR_BIST_STATUS__WR_GAP_UND__HW_DEFAULT  0x0

/* state register */
/* state register. upon bist completion all fields of this register should be 0. */
#define DDR_BIST_STATE            0x10828014

/* DDR_BIST_STATE.FSM_STATE - current state of fsm. */
/* 0 : IDLE. bist is not running. in cyclic mode (CONTROL.CYCLIC=1) fsm will spend one clock cycle in this state between loops. */
/* 3 : RW. both read and write are performed simultaneously. this is the only active state in case CONFIG.RW=1. */
/* 1 : WR. writing to DRAM. */
/* 2 : RD. reading from DRAM. */
#define DDR_BIST_STATE__FSM_STATE__SHIFT       0
#define DDR_BIST_STATE__FSM_STATE__WIDTH       2
#define DDR_BIST_STATE__FSM_STATE__MASK        0x00000003
#define DDR_BIST_STATE__FSM_STATE__INV_MASK    0xFFFFFFFC
#define DDR_BIST_STATE__FSM_STATE__HW_DEFAULT  0x0

/* DDR_BIST_STATE.WR_PENDING - current pending write transactions. */
#define DDR_BIST_STATE__WR_PENDING__SHIFT       8
#define DDR_BIST_STATE__WR_PENDING__WIDTH       8
#define DDR_BIST_STATE__WR_PENDING__MASK        0x0000FF00
#define DDR_BIST_STATE__WR_PENDING__INV_MASK    0xFFFF00FF
#define DDR_BIST_STATE__WR_PENDING__HW_DEFAULT  0x0

/* DDR_BIST_STATE.RD_PENDING - current pending read transactions. */
#define DDR_BIST_STATE__RD_PENDING__SHIFT       16
#define DDR_BIST_STATE__RD_PENDING__WIDTH       8
#define DDR_BIST_STATE__RD_PENDING__MASK        0x00FF0000
#define DDR_BIST_STATE__RD_PENDING__INV_MASK    0xFF00FFFF
#define DDR_BIST_STATE__RD_PENDING__HW_DEFAULT  0x0

/* DDR_BIST_STATE.RW_GAP - current gap between write and read transactions. */
#define DDR_BIST_STATE__RW_GAP__SHIFT       24
#define DDR_BIST_STATE__RW_GAP__WIDTH       8
#define DDR_BIST_STATE__RW_GAP__MASK        0xFF000000
#define DDR_BIST_STATE__RW_GAP__INV_MASK    0x00FFFFFF
#define DDR_BIST_STATE__RW_GAP__HW_DEFAULT  0x0

/* error count */
/* error count */
#define DDR_BIST_ERROR_CNT        0x10828018

/* DDR_BIST_ERROR_CNT.COUNT - number of erroneous words (of 64 bits) in current bist operation. */
#define DDR_BIST_ERROR_CNT__COUNT__SHIFT       0
#define DDR_BIST_ERROR_CNT__COUNT__WIDTH       32
#define DDR_BIST_ERROR_CNT__COUNT__MASK        0xFFFFFFFF
#define DDR_BIST_ERROR_CNT__COUNT__INV_MASK    0x00000000
#define DDR_BIST_ERROR_CNT__COUNT__HW_DEFAULT  0x0

/* first error address */
/* first error address */
#define DDR_BIST_FIRST_ERROR_ADDR 0x1082801C

/* DDR_BIST_FIRST_ERROR_ADDR.ADDR - DRAM byte address of first failure. */
#define DDR_BIST_FIRST_ERROR_ADDR__ADDR__SHIFT       0
#define DDR_BIST_FIRST_ERROR_ADDR__ADDR__WIDTH       32
#define DDR_BIST_FIRST_ERROR_ADDR__ADDR__MASK        0xFFFFFFFF
#define DDR_BIST_FIRST_ERROR_ADDR__ADDR__INV_MASK    0x00000000
#define DDR_BIST_FIRST_ERROR_ADDR__ADDR__HW_DEFAULT  0x0

/* first error expected data low */
/* first error address */
#define DDR_BIST_FIRST_ERROR_EXP_DATAL 0x10828020

/* DDR_BIST_FIRST_ERROR_EXP_DATAL.DATA - 32 lsbits of expected data in the first failure. */
#define DDR_BIST_FIRST_ERROR_EXP_DATAL__DATA__SHIFT       0
#define DDR_BIST_FIRST_ERROR_EXP_DATAL__DATA__WIDTH       32
#define DDR_BIST_FIRST_ERROR_EXP_DATAL__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_FIRST_ERROR_EXP_DATAL__DATA__INV_MASK    0x00000000
#define DDR_BIST_FIRST_ERROR_EXP_DATAL__DATA__HW_DEFAULT  0x0

/* first error expected data high */
/* first error address */
#define DDR_BIST_FIRST_ERROR_EXP_DATAH 0x10828024

/* DDR_BIST_FIRST_ERROR_EXP_DATAH.DATA - 32 msbits of expected data in the first failure. */
#define DDR_BIST_FIRST_ERROR_EXP_DATAH__DATA__SHIFT       0
#define DDR_BIST_FIRST_ERROR_EXP_DATAH__DATA__WIDTH       32
#define DDR_BIST_FIRST_ERROR_EXP_DATAH__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_FIRST_ERROR_EXP_DATAH__DATA__INV_MASK    0x00000000
#define DDR_BIST_FIRST_ERROR_EXP_DATAH__DATA__HW_DEFAULT  0x0

/* first error read data low */
/* first error address */
#define DDR_BIST_FIRST_ERROR_RD_DATAL 0x10828028

/* DDR_BIST_FIRST_ERROR_RD_DATAL.DATA - 32 lsbits of the data read in the first failure. */
#define DDR_BIST_FIRST_ERROR_RD_DATAL__DATA__SHIFT       0
#define DDR_BIST_FIRST_ERROR_RD_DATAL__DATA__WIDTH       32
#define DDR_BIST_FIRST_ERROR_RD_DATAL__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_FIRST_ERROR_RD_DATAL__DATA__INV_MASK    0x00000000
#define DDR_BIST_FIRST_ERROR_RD_DATAL__DATA__HW_DEFAULT  0x0

/* first error read data high */
/* first error address */
#define DDR_BIST_FIRST_ERROR_RD_DATAH 0x1082802C

/* DDR_BIST_FIRST_ERROR_RD_DATAH.DATA - 32 msbits of the data read in the first failure. */
#define DDR_BIST_FIRST_ERROR_RD_DATAH__DATA__SHIFT       0
#define DDR_BIST_FIRST_ERROR_RD_DATAH__DATA__WIDTH       32
#define DDR_BIST_FIRST_ERROR_RD_DATAH__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_FIRST_ERROR_RD_DATAH__DATA__INV_MASK    0x00000000
#define DDR_BIST_FIRST_ERROR_RD_DATAH__DATA__HW_DEFAULT  0x0

/* data0-low register */
/* data0-low register */
#define DDR_BIST_DATA0L           0x10828040

/* DDR_BIST_DATA0L.DATA - 32 lsbits of data0 */
#define DDR_BIST_DATA0L__DATA__SHIFT       0
#define DDR_BIST_DATA0L__DATA__WIDTH       32
#define DDR_BIST_DATA0L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_DATA0L__DATA__INV_MASK    0x00000000
#define DDR_BIST_DATA0L__DATA__HW_DEFAULT  0x0

/* data0-high register */
/* data0-high register */
#define DDR_BIST_DATA0H           0x10828044

/* DDR_BIST_DATA0H.DATA - 32 msbits of data0 */
#define DDR_BIST_DATA0H__DATA__SHIFT       0
#define DDR_BIST_DATA0H__DATA__WIDTH       32
#define DDR_BIST_DATA0H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_DATA0H__DATA__INV_MASK    0x00000000
#define DDR_BIST_DATA0H__DATA__HW_DEFAULT  0x0

/* data1-low register */
/* data1-low register */
#define DDR_BIST_DATA1L           0x10828048

/* DDR_BIST_DATA1L.DATA - 32 lsbits of data1 */
#define DDR_BIST_DATA1L__DATA__SHIFT       0
#define DDR_BIST_DATA1L__DATA__WIDTH       32
#define DDR_BIST_DATA1L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_DATA1L__DATA__INV_MASK    0x00000000
#define DDR_BIST_DATA1L__DATA__HW_DEFAULT  0x0

/* data1-high register */
/* data1-high register */
#define DDR_BIST_DATA1H           0x1082804C

/* DDR_BIST_DATA1H.DATA - 32 msbits of data1 */
#define DDR_BIST_DATA1H__DATA__SHIFT       0
#define DDR_BIST_DATA1H__DATA__WIDTH       32
#define DDR_BIST_DATA1H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_DATA1H__DATA__INV_MASK    0x00000000
#define DDR_BIST_DATA1H__DATA__HW_DEFAULT  0x0

/* mask0-low register */
/* mask0-low register */
#define DDR_BIST_MASK0L           0x10828050

/* DDR_BIST_MASK0L.DATA - 32 lsbits of mask0 */
#define DDR_BIST_MASK0L__DATA__SHIFT       0
#define DDR_BIST_MASK0L__DATA__WIDTH       32
#define DDR_BIST_MASK0L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_MASK0L__DATA__INV_MASK    0x00000000
#define DDR_BIST_MASK0L__DATA__HW_DEFAULT  0x0

/* mask0-high register */
/* mask0-high register */
#define DDR_BIST_MASK0H           0x10828054

/* DDR_BIST_MASK0H.DATA - 32 msbits of mask0 */
#define DDR_BIST_MASK0H__DATA__SHIFT       0
#define DDR_BIST_MASK0H__DATA__WIDTH       32
#define DDR_BIST_MASK0H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_MASK0H__DATA__INV_MASK    0x00000000
#define DDR_BIST_MASK0H__DATA__HW_DEFAULT  0x0

/* mask1-low register */
/* mask1-low register */
#define DDR_BIST_MASK1L           0x10828058

/* DDR_BIST_MASK1L.DATA - 32 lsbits of mask1 */
#define DDR_BIST_MASK1L__DATA__SHIFT       0
#define DDR_BIST_MASK1L__DATA__WIDTH       32
#define DDR_BIST_MASK1L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_MASK1L__DATA__INV_MASK    0x00000000
#define DDR_BIST_MASK1L__DATA__HW_DEFAULT  0x0

/* mask1-high register */
/* mask1-high register */
#define DDR_BIST_MASK1H           0x1082805C

/* DDR_BIST_MASK1H.DATA - 32 msbits of mask1 */
#define DDR_BIST_MASK1H__DATA__SHIFT       0
#define DDR_BIST_MASK1H__DATA__WIDTH       32
#define DDR_BIST_MASK1H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_MASK1H__DATA__INV_MASK    0x00000000
#define DDR_BIST_MASK1H__DATA__HW_DEFAULT  0x0

/* error0-low register */
/* error0-low register */
#define DDR_BIST_ERROR0L          0x10828060

/* DDR_BIST_ERROR0L.DATA - 32 lsbits of error0. the 128 bits in {ERROR1H, ERROR1L, ERROR0H, ERROR0L} represent in which bits of each chunk of 128-bits error occured since the beginning of the bist loop. */
#define DDR_BIST_ERROR0L__DATA__SHIFT       0
#define DDR_BIST_ERROR0L__DATA__WIDTH       32
#define DDR_BIST_ERROR0L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_ERROR0L__DATA__INV_MASK    0x00000000
#define DDR_BIST_ERROR0L__DATA__HW_DEFAULT  0x0

/* error0-high register */
/* error0-high register */
#define DDR_BIST_ERROR0H          0x10828064

/* DDR_BIST_ERROR0H.DATA - 32 msbits of error0. the 128 bits in {ERROR1H, ERROR1L, ERROR0H, ERROR0L} represent in which bits of each chunk of 128-bits error occured since the beginning of the bist loop. */
#define DDR_BIST_ERROR0H__DATA__SHIFT       0
#define DDR_BIST_ERROR0H__DATA__WIDTH       32
#define DDR_BIST_ERROR0H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_ERROR0H__DATA__INV_MASK    0x00000000
#define DDR_BIST_ERROR0H__DATA__HW_DEFAULT  0x0

/* error1-low register */
/* error1-low register */
#define DDR_BIST_ERROR1L          0x10828068

/* DDR_BIST_ERROR1L.DATA - 32 lsbits of error1. the 128 bits in {ERROR1H, ERROR1L, ERROR0H, ERROR0L} represent in which bits of each chunk of 128-bits error occured since the beginning of the bist loop. */
#define DDR_BIST_ERROR1L__DATA__SHIFT       0
#define DDR_BIST_ERROR1L__DATA__WIDTH       32
#define DDR_BIST_ERROR1L__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_ERROR1L__DATA__INV_MASK    0x00000000
#define DDR_BIST_ERROR1L__DATA__HW_DEFAULT  0x0

/* error1-high register */
/* error1-high register */
#define DDR_BIST_ERROR1H          0x1082806C

/* DDR_BIST_ERROR1H.DATA - 32 msbits of error1. the 128 bits in {ERROR1H, ERROR1L, ERROR0H, ERROR0L} represent in which bits of each chunk of 128-bits error occured since the beginning of the bist loop. */
#define DDR_BIST_ERROR1H__DATA__SHIFT       0
#define DDR_BIST_ERROR1H__DATA__WIDTH       32
#define DDR_BIST_ERROR1H__DATA__MASK        0xFFFFFFFF
#define DDR_BIST_ERROR1H__DATA__INV_MASK    0x00000000
#define DDR_BIST_ERROR1H__DATA__HW_DEFAULT  0x0


#endif  // __DDRBIST_H__
