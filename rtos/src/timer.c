/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/*****************************************************************************/
// Module Name: 
//    timer.c
//
// Abstract: 
//    timer driver
// 
// Notes:
//    
//
/*****************************************************************************/
 

//#include "soc.h"
#include "clkc.h"
#include "chip_utils.h"
#include "boot.h"
#include "clkrstio.h"

#define TIMER_FRE  (1*1000*1000)

static DWORD g_TimeOutValue;
static DWORD g_TimerStartPoint;

#define TIMER_64_LATCH           0x1
#define TIMER_64_LOAD           0x2


void OSTimerInit(DWORD sys_clk, DWORD dwLow)
{
    WRITE_REG( CLKC_ROOT_CLK_EN0_SET, 1 << CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__SHIFT );
#ifdef A7_STEP_B
    WRITE_REG( CLKC_MISC1_LEAF_CLK_EN_SET, 1 << CLKC_MISC1_LEAF_CLK_EN_SET__TIMER_IO_CLKEN__SHIFT );
#else
    WRITE_REG( CLKC_LEAF_CLK_EN1_SET, 1 << CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__SHIFT);
#endif
    //ResetModule(MODULE_TIMER);
    rTIMER_64COUNTER_CTRL = (sys_clk/TIMER_FRE - 1) << 16;    //Set DIV
    rTIMER_64COUNTER_LOAD_LO = dwLow;
    rTIMER_64COUNTER_LOAD_HI = 0;
    rTIMER_64COUNTER_CTRL |= TIMER_64_LOAD;
}

DWORD OSTGetCurrentTick (void)
{
    rTIMER_64COUNTER_CTRL |= TIMER_64_LATCH;   //Latch
    
    return (rTIMER_64COUNTER_RLATCHED_LO);
}

void usWait(DWORD usNum)
{
    
    DWORD target = OSTGetCurrentTick() + usNum+1;
    
    while (OSTGetCurrentTick() < target);    
}


void SetTimeOut(DWORD us)   //us
{
    g_TimerStartPoint = OSTGetCurrentTick();
    g_TimeOutValue = g_TimerStartPoint+us+1;    
}

DWORD GetSpendTime(void)   //us
{ 
    return OSTGetCurrentTick()-g_TimerStartPoint;
}


BOOL IsTimeOut()
{
   return !(OSTGetCurrentTick() < g_TimeOutValue);
}

