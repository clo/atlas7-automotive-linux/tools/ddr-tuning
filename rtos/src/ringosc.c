/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/* otp.c for scratchpad  application */

#include "types.h"
#include "uart1.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
#ifndef A7_STEP_B
#include "soc_gic.h"
#else
#include "soc_gic_b.h"
#endif
#include "chip_utils.h"
#include "ringosc.h"
#include "otp.h"

       
 const unsigned int dvm_regs[] = {
          CPU_ACCESS_DVM_CONTROL , 0x0, 0x7f,
          COUNT_EN_CYCLE_XIN_NUM , 0x0, 0xffffffff,
          COUNT_EN_DVM_UNIT_MASK , 0x0, 0xffffffff,       
          DVM_VERSION         , 0x0, 0xffffffff       
       };
 
 const unsigned int dvm_count_regs[] = {
             DVM_COUNT_0         , 0x0, 
             DVM_COUNT_1         , 0x0, 
             DVM_COUNT_2         , 0x0, 
             DVM_COUNT_3         , 0x0, 
             DVM_COUNT_4         , 0x0, 
             DVM_COUNT_5         , 0x0, 
             DVM_COUNT_6         , 0x0, 
             DVM_COUNT_7         , 0x0, 
             DVM_COUNT_8         , 0x0, 
             DVM_COUNT_9         , 0x0, 
             DVM_COUNT_10         , 0x0, 
             DVM_COUNT_11         , 0x0, 
             DVM_COUNT_12         , 0x0, 
             DVM_COUNT_13         , 0x0, 
             DVM_COUNT_14         , 0x0, 
             DVM_COUNT_15         , 0x0, 
             DVM_COUNT_16         , 0x0, 
             DVM_COUNT_17         , 0x0, 
             DVM_COUNT_18         , 0x0, 
             DVM_COUNT_19         , 0x0
       };
 unsigned long dvm_count[] = {
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0, 
              0x0
       };
 #if 0
  char* dvm_description[] = {
              "AUDSCM0", 
              "AUDSCM1", 
              "CGUM0", 
              "CGUM1", 
              "MEDIAM0", 
              "MEDIAM1",
              "DDRM0", 
              "DDRM1",              
              "CPUM0", 
              "CPUM1", 
              "GPUM0", 
              "GPUM1",               
              "VDIFM0", 
              "VDIFM1", 
               "GNSSM0", 
              "GNSSM1", 
              "RTCM0", 
              "RTCM1", 
              "BTM0", 
              "BTM1" 
       };
  char* dvm_type[] = {
               "Ptype", 
               "Ntype", 
               "LVT50", 
               "RVT40",
               "LVT40",               
               "RVT50",               
               "RVT50Quiet",               
               "AllRo",               
               "HVT50",               
               "ULP50",
               "ULP40",      
               "NOR3",
               "NAND4",                  
                  "RVT50",   
               "RVT50",   
               "RVT50"               
        };

 #endif 
 
int ringosc_test ()
{    
   int i;
   unsigned long dvm_ring_osc_sel;  
   unsigned long dvm_clk_sel;    
   unsigned long dvm_count_read_val;
   unsigned long norm_dvm_count_value;
   unsigned long regStat;
      
   dvm_ring_osc_sel=3; //divide by 8

   printf("\r\n hello world 1! \r\n");
   //WRITE_REG(0x18620244 /*CLKC_LEAF_CLK_EN0_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x1862024C /*CLKC_LEAF_CLK_EN0_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN0_STATUS: 0x%x\r\n", regStat );   

   WRITE_REG(0x186204A0 /*CLKC_LEAF_CLK_EN1_SET*/ ,0xFFFFFFFF);
   regStat = Read_Byte(0x186204A8 /*CLKC_LEAF_CLK_EN1_STATUS*/);       
   printf("CLKC_LEAF_CLK_EN1_STATUS: 0x%x\r\n", regStat );

   WRITE_REG(0x186204B8 /*CLKC_LEAF_CLK_EN2_SET*/ ,0xFFFFFFFF);
   regStat = Read_Byte(0x186204C0 /*CLKC_LEAF_CLK_EN2_STATUS*/);       
   printf("CLKC_LEAF_CLK_EN2_STATUS: 0x%x\r\n", regStat );   

   WRITE_REG(0x186204D0 /*CLKC_LEAF_CLK_EN3_SET*/ ,0xFFFFFFFF);
   regStat = Read_Byte(0x186204D8 /*CLKC_LEAF_CLK_EN3_STATUS*/);       
   printf("CLKC_LEAF_CLK_EN3_STATUS: 0x%x\r\n", regStat );   

   //WRITE_REG(0x186204E8 /*CLKC_LEAF_CLK_EN4_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x186204F0 /*CLKC_LEAF_CLK_EN4_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN4_STATUS: 0x%x\r\n", regStat );   

   //WRITE_REG(0x18620500 /*CLKC_LEAF_CLK_EN5_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x18620508 /*CLKC_LEAF_CLK_EN5_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN5_STATUS: 0x%x\r\n", regStat );   

   //WRITE_REG(0x18620518 /*CLKC_LEAF_CLK_EN6_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x18620520 /*CLKC_LEAF_CLK_EN6_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN6_STATUS: 0x%x\r\n", regStat );

   //WRITE_REG(0x18620530 /*CLKC_LEAF_CLK_EN7_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x18620538 /*CLKC_LEAF_CLK_EN7_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN7_STATUS: 0x%x\r\n", regStat );   

   //WRITE_REG(0x18620548 /*CLKC_LEAF_CLK_EN8_SET*/ ,0xFFFFFFFF);
   //regStat = Read_Byte(0x18620550 /*CLKC_LEAF_CLK_EN8_STATUS*/);       
   //printf("CLKC_LEAF_CLK_EN8_STATUS: 0x%x\r\n", regStat );   

   return 1;
   for (dvm_clk_sel=0; dvm_clk_sel<DVM_TYPE_NUM; dvm_clk_sel++) 
   {
      if ((dvm_clk_sel==7) ||(dvm_clk_sel==10)) continue;   
      WRITE_REG(CPU_ACCESS_DVM_CONTROL,(dvm_ring_osc_sel << 5) + (dvm_clk_sel << 1));

      // XIN = 26 MHz, 10416*38ns = 0.4 msec

      // WRITE_REG(COUNT_EN_CYCLE_XIN_NUM ,0x1FBD0); //5 msec
      //WRITE_REG(COUNT_EN_CYCLE_XIN_NUM ,0x28B0); //0.4 msec 
      // XIN = 150 MHz, 60000*6.6667ns = 0.4 msec
      WRITE_REG(COUNT_EN_CYCLE_XIN_NUM, 0x0000EA60); //0.4 msec 
      WRITE_REG(COUNT_EN_DVM_UNIT_MASK, 0x000FFFFF);
      WRITE_REG(CPU_ACCESS_DVM_CONTROL, READ_REG(CPU_ACCESS_DVM_CONTROL) | 0x1);
 
      while ((READ_REG(CPU_ACCESS_DVM_CONTROL) & (1 << DVM_CPU_READ_EN_BIT)) != (1 << DVM_CPU_READ_EN_BIT))
      {
         for (i=0; i<DVM_COUNT_NUM; i++) 
         {
            dvm_count_read_val = READ_REG(dvm_count_regs[i*2]);
      
            if(dvm_clk_sel)
            { norm_dvm_count_value = dvm_count_read_val - dvm_count[i]; }
            else
            { norm_dvm_count_value = dvm_count_read_val; }

            printf("\rdvm_count_loc%d_type%d=%d\r\n",i,dvm_clk_sel,norm_dvm_count_value);
            
            /* if (i==0 && dvm_clk_sel==3) { j=j+norm_dvm_count_value; } */

            dvm_count[i] = dvm_count[i] + norm_dvm_count_value;
         } // for loop
      } // while loop
   } // for loop
   
   return 1;
} // ringosc_test

 
  int ringosc_one ()
 {    
       int i,j=0;
        unsigned long dvm_ring_osc_sel;  
       unsigned long dvm_clk_sel;    
       unsigned long dvm_count_read_val;
       unsigned long norm_dvm_count_value;
       //unsigned long regStat;
      
    dvm_ring_osc_sel=3; //divide by 8

   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x18620244 /*CLKC_LEAF_CLK_EN0_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x1862024C /*CLKC_LEAF_CLK_EN0_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN0_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x186204A0 /*CLKC_LEAF_CLK_EN1_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x186204A8 /*CLKC_LEAF_CLK_EN1_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN1_STATUS: 0x%x\r\n", regStat );
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x186204B8 /*CLKC_LEAF_CLK_EN2_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x186204C0 /*CLKC_LEAF_CLK_EN2_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN2_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x186204D0 /*CLKC_LEAF_CLK_EN3_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x186204D8 /*CLKC_LEAF_CLK_EN3_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN3_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x186204E8 /*CLKC_LEAF_CLK_EN4_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x186204F0 /*CLKC_LEAF_CLK_EN4_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN4_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x18620500 /*CLKC_LEAF_CLK_EN5_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x18620508 /*CLKC_LEAF_CLK_EN5_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN5_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x18620518 /*CLKC_LEAF_CLK_EN6_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x18620520 /*CLKC_LEAF_CLK_EN6_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN6_STATUS: 0x%x\r\n", regStat );
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x18620530 /*CLKC_LEAF_CLK_EN7_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x18620538 /*CLKC_LEAF_CLK_EN7_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN7_STATUS: 0x%x\r\n", regStat );   
//   printf("\r\n hello world 1! \r\n");
//   WRITE_REG(0x18620548 /*CLKC_LEAF_CLK_EN8_SET*/ ,0xFFFFFFFF);
//   //regStat = Read_Byte(0x18620550 /*CLKC_LEAF_CLK_EN8_STATUS*/);       
//   //printf("\r CLKC_LEAF_CLK_EN8_STATUS: 0x%x\r\n", regStat );   


   printf("\r\n hello world 1! \r\n");
    for (dvm_clk_sel=0; dvm_clk_sel<DVM_TYPE_NUM; dvm_clk_sel++) {
      if ((dvm_clk_sel==7) ||(dvm_clk_sel==10)) continue;   
      WRITE_REG(CPU_ACCESS_DVM_CONTROL,(dvm_ring_osc_sel << 5) + (dvm_clk_sel << 1));

       // XIN = 26 MHz, 10416*38ns = 0.4 msec

      // WRITE_REG(COUNT_EN_CYCLE_XIN_NUM ,0x1FBD0); //5 msec
      //WRITE_REG(COUNT_EN_CYCLE_XIN_NUM ,0x28B0); //0.4 msec 
   
   // XIN = 150 MHz, 60000*6.6667ns = 0.4 msec
        WRITE_REG(COUNT_EN_CYCLE_XIN_NUM ,0xEA60); //0.4 msec 
        WRITE_REG( COUNT_EN_DVM_UNIT_MASK ,0xFFFFF);
    
         WRITE_REG (CPU_ACCESS_DVM_CONTROL , READ_REG(CPU_ACCESS_DVM_CONTROL) | 0x1);
 
        while ((READ_REG(CPU_ACCESS_DVM_CONTROL) & (1 << DVM_CPU_READ_EN_BIT)) != (1 << DVM_CPU_READ_EN_BIT))
    
        for (i=0; i<DVM_COUNT_NUM; i++) {
         dvm_count_read_val = READ_REG(dvm_count_regs[i*2]);
      
         if(dvm_clk_sel)
            norm_dvm_count_value = dvm_count_read_val - dvm_count[i];
         else
            norm_dvm_count_value = dvm_count_read_val;

         
         //printf("\rdvm_count_loc%d_type%d=%d\r\n",
         //         i,dvm_clk_sel,norm_dvm_count_value);
         if (i==0 && dvm_clk_sel==3) {
            j=j+norm_dvm_count_value;
         }
         dvm_count[i] = dvm_count[i] + norm_dvm_count_value;

          }   
        }
    return j;
    }

 

