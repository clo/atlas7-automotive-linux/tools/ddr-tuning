/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "types.h"
#include "uart1.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
#include "task.h"

/* Dimensions the buffer into which input characters are placed. */
#define cmdMAX_INPUT_SIZE	60

/* Dimensions the buffer into which string outputs can be placed. */
#define cmdMAX_OUTPUT_SIZE	configCOMMAND_INT_MAX_OUTPUT_SIZE

#if SUPPORT_HISTORY
/******************************************************************************
** history queue
*/
#define MAX_HISTORY_LENGHT 10

/* history link add an item to the next when command is executed.
** going back in time is going to the previous direction.
*/

typedef struct _sHistoryItem
{
    char* command;
    struct _sHistoryItem* next;
    struct _sHistoryItem* previous;
} sHistoryItem;

static sHistoryItem* historyRoot = NULL;
static sHistoryItem* historyPtr = NULL;


static void addHistoryItem( char* cmdStr )
{
    static int uHistoryLength = 0;
    // create new item
    sHistoryItem* newItem = (sHistoryItem*)pvPortMalloc( sizeof( sHistoryItem ) );
    newItem->command = (char*)pvPortMalloc( strlen( cmdStr ) + 1 );
    // copy string
    sprintf( newItem->command, "%s", cmdStr );
    newItem->next = NULL;
    newItem->previous = historyRoot;
 
    // add to link list
    if( NULL != historyRoot )
    {
        historyRoot->next = newItem;
    }
  
    historyRoot = newItem;
    
    // Clean history tail
    uHistoryLength++;
    if( uHistoryLength > MAX_HISTORY_LENGHT )
    {
        /* find the last item and remove it */
        sHistoryItem* pItem = historyRoot;
        while( pItem != NULL && pItem->previous != NULL )
        {
            pItem = pItem->previous;
        }
        if( pItem != NULL )
        {
            pItem = pItem->next;
            vPortFree( pItem->previous->command );
            vPortFree( pItem->previous );
            pItem->previous = NULL;
        }
        uHistoryLength--;
    }
            
}

static void echoHistoryItemToPrompt( sHistoryItem* item, int previousItmeLength )
{
    int cmdLength = ( ( item != NULL ) ? strlen( item->command ) : 0 );
    int i;
    
    while( previousItmeLength > 0 )
    {
        InjectChar( '\b' );
        previousItmeLength--;
    }
    
    for( i = 0; i < cmdLength; i++)
    {
        InjectChar( item->command[i] );
    }
}
static void historyPrevious( int previousItmeLength )
{
    if( NULL == historyPtr )
    {
        // first time history is called.
        historyPtr = historyRoot;
    }
    else
    {
        historyPtr = historyPtr->previous;
    }
    
    echoHistoryItemToPrompt( historyPtr, previousItmeLength );
}      

static void historyNext( int previousItmeLength )
{
    if( NULL == historyPtr )
    {
        // first time history is called.
        // no next item.
        return;
    }
    else
    {
        historyPtr = historyPtr->next;
    }
    
    echoHistoryItemToPrompt( historyPtr, previousItmeLength );
}

void printHistory()
{
    sHistoryItem* ptr = historyRoot;
    int count = 0;
    
    while( NULL != ptr )
    {
        printf("\r\n %d: %s", count, ptr->command );
        count++;
        ptr = ptr->previous;
    }
    printf("\r\n--.\r\n");
}
#else
void printHistory()
{
    printf("\r\nHistory not now supported in CLI!\r\n");
}
#endif // SUPPORT_HISTORY

/*
 * Task that provides the input and output for the FreeRTOS+CLI command
 * interpreter.  
 */
void vCommandInterpreterTask( void *pvParameters )
{
    signed char cInChar, cInputIndex = 0;
    char* pcInputString;
    char* pcOutputString;
    portBASE_TYPE xMoreDataToFollow;

    pcInputString = (char*)pvPortMalloc( cmdMAX_INPUT_SIZE );
    memset( pcInputString, 0x00, cmdMAX_INPUT_SIZE );
    pcOutputString = FreeRTOS_CLIGetOutputBuffer();
    pcOutputString[0] = '\0';
    
	/* Just to prevent compiler warnings. */
	( void ) pvParameters;

   #if SUPPORT_DDR   
      vTaskDelay( 50 ); // For stable operation for DDR BENCH support at 9600 BPS
   #endif // SUPPORT_DDR   

    printf("Welcome to CLI.\r\nEnter 'help' for list of commands.\r\n");
    printf( ">> " );

	for( ;; )
	{
	    cInChar = uartReadByte();

	    /* Newline characters are taken as the end of the command
	    string. */
	    if( ( cInChar == '\r' || cInChar == '\n' ) )
	    {
            /* echo */
            uartWriteByte( cInChar);

	        if( cInputIndex > 0 )
	        { 
		        /* Process the input string received prior to the newline. */
		        do
		        {
			        /* Pass the string to FreeRTOS+CLI. */
			        xMoreDataToFollow = FreeRTOS_CLIProcessCommand( pcInputString, pcOutputString, cmdMAX_OUTPUT_SIZE );

			        /* Send the output generated by the command's implementation. */
			        printf("%s", pcOutputString );

		        } while( xMoreDataToFollow != pdFALSE ); /* Until the command does not generate any more output. */
#if SUPPORT_HISTORY
      	        // add new item to history
	            addHistoryItem( pcInputString );
	            // clear history pointer
	            historyPtr = NULL;
#endif
            }

	        /* All the strings generated by the command processing
	        have been sent.  Clear the input string ready to receive
	        the next command. */
	        memset( pcInputString, 0x00, cmdMAX_INPUT_SIZE );
	        pcOutputString[0] = '\0';
	        cInputIndex = 0;
	        cInChar = 0;
	        
		    /* Transmit a spacer, just to make the command console
		    easier to read. */
		    printf( "\r\n>> " );
	    }
	    else
	    {
		    if( cInChar == '\r' || cInChar == '\n' || cInChar == '\0' )
		    {
			    /* Ignore the character.  Newlines are used to
			    detect the end of the input string. */
		    }
		    else if( cInChar == '\b' || cInChar == 127 )
		    {
                /* delete the curent char: go back one char, print empty char, go back one char */
                uartWriteByte( cInChar);
                uartWriteByte( ' ');
                uartWriteByte( cInChar);
                
			    /* Backspace was pressed.  Erase the last character
			    in the string - if any. */
			    if( cInputIndex > 0 )
			    {
				    cInputIndex--;
				    pcInputString[ cInputIndex ] = '\0';
			    }
		    }
#if SUPPORT_HISTORY
            /* up arrow is a sequance of 3 caracters: '<ESC>[A' ( <esc> = 27 ) 
             this is TerqaTerm implementation, it amy not work on other terminals. */
		    else if( cInChar == 27 ) 
		    {
		        cInChar = uartReadByte();
		        if( cInChar == '[' )
		        {
		            cInChar = uartReadByte();
		            if( cInChar == 'A' )
		            { 
			            /* up arrow pressed. go to previous history */
			            historyPrevious( cInputIndex );			    
		            }
		            else if( cInChar == 'B' )
		            {
			            /* down arrow pressed. go to next history */
			            historyNext( cInputIndex );			    
		            }
                }
		    }
#endif
		    else
		    {
                /* echo */
                uartWriteByte( cInChar);
			    /* A character was entered.  Add it to the string
			    entered so far.  When a \n is entered the complete
			    string will be passed to the command interpreter. */
			    if( cInputIndex < cmdMAX_INPUT_SIZE )
			    {
				    pcInputString[ cInputIndex ] = cInChar;
				    cInputIndex++ ;                   
			    }
		    }
	    }
    }
}
