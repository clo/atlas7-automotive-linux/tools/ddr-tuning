/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/*****************************************************************************/
// Module Name: 
//    uart1_queue.c
//
// Abstract: 
//    UART 1 driver with queue support for RTOS
// 
// Notes:
//    
//
/*****************************************************************************/

#include "uart1.h"
#include "clkrstio.h"
#ifndef A7_STEP_B
#include "soc_gic.h"
#else
#include "soc_gic_b.h"
#endif
#include "boot.h"

#include "clkc.h"
#include "chip_utils.h"

/* Free RTOS  */
#include "FreeRTOS.h"
#include "queue.h"


// for debugging
//#define DEBUG_MODE

static QueueHandle_t   TxQueue = NULL;
static QueueHandle_t   RxQueue = NULL;

/* if TX queue is full task will be blocked on adding
   change the queue size if needed. */
#define TX_QUEUE_SIZE   100
#define RX_QUEUE_SIZE   200

static void gic_UART_handler(unsigned int gic_id);
#define UART_SAMPLE_DIV_LIMIT 	0x41

DWORD CalBaudRate(DWORD   BaudRate, DWORD IOClk)
{
    int ioclkDiv = 0, sampleDiv = 0, delta = 0, tmp = 0, i = 0, div = 0;
    delta = IOClk;

    if (0 == BaudRate)
        return FALSE;

    for (i = 16;i < UART_SAMPLE_DIV_LIMIT;i++)
    {
        div = (IOClk + (BaudRate * i) / 2) / (BaudRate * i); // round
        tmp = IOClk - div * BaudRate * i;
        tmp = (tmp > 0 ? tmp : (-tmp));

        if (delta > tmp)
        {
            delta = tmp;
            ioclkDiv = div - 1; // io clk div in doc
            sampleDiv = i - 1; // sample div in doc
        }

        if (!tmp) break;
    }

    //DebugMsg("BR %d, IO clk %dM, Value 0x%x\r\n", BaudRate, IOClk/1000000, ioclkDiv | (sampleDiv << 16)); 

    return ioclkDiv | (sampleDiv << 16);

}

void uartInit(DWORD u_baudrate, DWORD ioclk)
{

    WRITE_REG( CLKC_ROOT_CLK_EN0_SET, 1 << CLKC_ROOT_CLK_EN0_SET__GNSSM_IO__SHIFT );
#ifdef A7_STEP_B
    WRITE_REG( CLKC_GNSS_LEAF_CLK_EN_SET, 1 << CLKC_GNSS_LEAF_CLK_EN_SET__UART1_IO_CLKEN__SHIFT );
#else
    WRITE_REG( CLKC_LEAF_CLK_EN3_SET, 1 << CLKC_LEAF_CLK_EN3_SET__UART1_IO_CLKEN__SHIFT);
#endif

    //ResetModule(MODULE_UART1);
    rIOC_TOP_FUNC_SEL_19_REG_CLR = (0x7 << 16) | (0x7 << 20);
    rIOC_TOP_FUNC_SEL_19_REG_SET = (0x1 << 16) | (0x1 << 20);


    UART1_DIVISOR = CalBaudRate(u_baudrate, ioclk);

    usWait(10);   

    UART1_LINE_CTRL = 0x00000003;   //8 data bits, 1 stop bit, no parity bit

    UART1_TX_DMA_IO_CTRL = UART1_TX_IO_MODE;
    UART1_RX_DMA_IO_CTRL = UART1_RX_IO_MODE;
    UART1_TX_DMA_IO_LEN = 0;
    UART1_RX_DMA_IO_LEN = 0;

    UART1_TXFIFO_CTRL = 0x08 ;
    UART1_RXFIFO_CTRL = 0x10 ;

    UART1_TXFIFO_LEVEL_CHK = 0x06 | (0x04 << 10) | (0x02 << 20);
    UART1_RXFIFO_LEVEL_CHK = 0x02 | (0x04 << 10) | (0x06 << 20);

    // FIFO Reset
    UART1_TXFIFO_OP = UART1_TXFIFO_RESET;
    UART1_RXFIFO_OP = UART1_RXFIFO_RESET;

    // FIFO Start
    UART1_TXFIFO_OP = UART1_TXFIFO_START;
    UART1_RXFIFO_OP = UART1_RXFIFO_START;

    usWait(10);

    UART1_TXRX_ENA_REG = UART1_RX_EN | UART1_TX_EN;

    // clear all pending uart interrupts
    UART1_INT_STATUS = UART_INT_MASK_ALL;

}


void uartGicInit()
{
    int rxfifo_threshold, txfifo_threshold;

    // uart 1 work in i/O mode only, no dma mode.

    /* wait for FIFO to empty */
    while( 0 == (UART1_TXFIFO_STATUS & UART1_TXFIFO_EMPTY) );
    
    /* reconfigure UART */
    txfifo_threshold=0x01;
    rxfifo_threshold=0x01;
    
    UART1_TXFIFO_CTRL = txfifo_threshold ;
    UART1_RXFIFO_CTRL = rxfifo_threshold ;
    
    UART1_TXRX_ENA_REG = UART1_RX_EN | UART1_TX_EN;
    UART1_INT_ENABLE_SET = UART_INT_RX_DONE_MASK | UART_INT_TX_THLD_MASK;

    UART1_MODE1 = 0x23; // even bit parity, 8 bit data.
    
    // Fifo Reset
    UART1_TXFIFO_OP = UART1_TXFIFO_RESET;
    UART1_RXFIFO_OP = UART1_RXFIFO_RESET;
    
    // Fifo Start
    UART1_TXFIFO_OP = UART1_TXFIFO_START;
    UART1_RXFIFO_OP = UART1_RXFIFO_START;
  
    // clear all pending uart interrupts
    UART1_INT_STATUS = UART_INT_MASK_ALL;
   

    TxQueue = xQueueCreate( TX_QUEUE_SIZE, sizeof( char ) );
    configASSERT( TxQueue );
    RxQueue = xQueueCreate( RX_QUEUE_SIZE, sizeof( char ) );
    configASSERT( RxQueue );

    //UART1_TXRX_ENA_REG = UART1_RX_EN;// | UART1_TX_EN;
    UART1_INT_STATUS = UART_INT_MASK_ALL;
    INT_ENABLE0( INT_PENDING_UART1 );
    gic_irq_enable(SPI_ID_UART1, gic_UART_handler);

}

#ifndef DEBUG_MODE
BaseType_t uartFillTxFIFO( void )
{
    char ch;
    BaseType_t xTaskWokenByReceive;

    while( 0 == ( UART1_TXFIFO_STATUS & UART1_TXFIFO_FULL )                              &&
           pdTRUE == xQueueReceiveFromISR( TxQueue, ( void * ) &ch, &xTaskWokenByReceive)  )
    {
        // send the new char
        UART1_TXFIFO_DATA = (unsigned long)ch;
    }
    return xTaskWokenByReceive;
}

// non blocking implementation.
void uartWriteByte(BYTE ch)
{
    // queue the byte for future proccessing.
    BaseType_t status;
    do {
        status = xQueueSend( TxQueue, (void*)&ch, 1 );
    } while ( errQUEUE_FULL == status );
    configASSERT( status == pdTRUE );

    uartFillTxFIFO();
}
#else // DEBUG_MODE
// non blocking implementation.
void uartWriteByte(BYTE ch)
{
    dumpChar( ch );
}
#endif

// blocking impelemntation
BYTE uartReadByte (void)
{
    char ch;
    BaseType_t status;
    do {
        status = xQueueReceive( RxQueue, (void*)&ch, 1000  );
    }
    while( status != pdPASS) ;
    configASSERT( status == pdPASS );

    return ch;
}
/**************************************************************************************************
    Poll data length in rx buf. 
    
    If no data in rx buf, return FALSE.
    Else set data length in *v_byteNum and return TRUE.
*/
BOOL uartIsDataInReadBuf(DWORD *v_byteNum)
{
    if(0 != (UART1_RXFIFO_STATUS & UART1_RXFIFO_EMPTY))
    {
        return FALSE;
    } 
    
    if(0 != (UART1_RXFIFO_STATUS & UART1_RXFIFO_FULL))
    {
        *v_byteNum = 32;  //max Rx fifo size
    }
    else
    {
        *v_byteNum = (UART1_RXFIFO_STATUS) & 0x1F;  //bit4:0       
    }
        
    return TRUE;
}




static void gic_UART_handler(unsigned int gic_id)
{
    char ch;
    uint32_t iStatus;
    BaseType_t xHigherPriorityTaskWokenByPost = pdFALSE;
    BaseType_t xTaskWokenByReceive = pdFALSE;

    if (gic_id == SPI_ID_UART1)
    {
        iStatus = UART1_INT_STATUS;
        if(iStatus & UART_INT_TX_THLD_MASK) 
        {
            UART1_INT_STATUS = UART_INT_TX_THLD_MASK;
#ifndef DEBUG_MODE 
            /* if TX queue is not empty fill HW FIFO */
            xTaskWokenByReceive = uartFillTxFIFO( );
#endif        
        }
        if(iStatus & UART_INT_RX_DONE_MASK) 
        {
            UART1_INT_STATUS = UART_INT_RX_DONE_MASK;
            ch = (UART1_RXFIFO_DATA);
            xQueueSendFromISR( RxQueue, (void*)&ch, &xHigherPriorityTaskWokenByPost);
        }

    	// Now the buffer is empty we can switch context if necessary.
	    if( xHigherPriorityTaskWokenByPost || xTaskWokenByReceive )
	    {
		    // Actual macro used here is port specific.
		    portYIELD_FROM_ISR( pdTRUE );
	    } 
        INT_ENABLE0( INT_PENDING_UART1 );
    }

}

/*******************************************
**  dumpChar
*/
void dumpChar( char ch )
{
    /* wait untill FIFO is empty */
	while(0 == (UART1_TXFIFO_STATUS & UART1_TXFIFO_EMPTY));
    /* add the char to the FIFO */
    UART1_TXFIFO_DATA = (unsigned long)( ch );
}
/************************************************
** dump the string onto the uart.
*/
void dumpString( char* str )
{
    while( *str != '\0' )
    {
        dumpChar( *str );
        str++;
    } 
}

/******************************************
** Send Closing Sequence 
** this procedure signals the UART image 
** loader that the load is completed.
*/
void SendClosingSequence( void )
{
    int i;
    char cClosingSequence[] = { 0xA1, 0xB2, 0xC3, 0x22, 
                                0x00, 0x00, 0x00, 0x00, 
                                0x00, 0x00, 0x00, 0x00, 
                                0xA1, 0xB2, 0xC3, 0x44, 
                                0xA5, 0xA5, 0xA5, 0xA5 };

    for( i = 0; i < sizeof( cClosingSequence ); i++ )
    {
        dumpChar( cClosingSequence[i] );
    }
    
    /* wait for char to be sent */
    while(0 == (UART1_TXFIFO_STATUS & UART1_TXFIFO_EMPTY));
}

void InjectChar( char ch )
{
    BaseType_t status;
    status = xQueueSend( RxQueue, (void*)&ch, 1 );
    configASSERT( status == pdTRUE );
}


