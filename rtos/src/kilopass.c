/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/* test implement for kilopass mist test -- ported from simulation */
#include <stdio.h>
#include <sys/time.h>
#include "ds_otp.h"
#include "otp.h"

#define BLANK_TESTMODE			1
#define TESTDEC_TESTMODE		2
#define WRTEST_TESTMODE			4

#define BLANK_TESTMODE_DONE_MASK	0x200
#define TESTDEC_TESTMODE_DONE_MASK	0x400
#define WRTEST_TESTMODE_DONE_MASK	0x600
#define CMD_DONE_MASK			0x040

#define TEST_ERR_MASK			0x1

#define TEST_CMD			0x00070000
#define DONECLR_CMD			0x80000000

#define BR_STAT_ERR_MASK		0x3C00

/* read write reg marcro -- actually same with similar reg
 marcros, make them a copy here to make sure module only exports
 function interfaces for external in case code changing.
 linux mmap address don't use volatile */

#define Wait_polling(Address, Mask, Expected, Polling)\
long long ipolling = Polling;\
   do{ipolling--;} while((((Address) & (Mask)) != (Expected)) && (ipolling > 0) )

#define WaitI(Address, Mask, Expected) do{;}while((((Address)) & (Mask)) != (Expected))

static int test_mode(unsigned int reg_base,
	unsigned int mode, unsigned int mask)
{
	unsigned long RegRes = 0x0;
	/* initiate test mode */
	WriteReg((reg_base + rOTP_TESTMODE), mode);
	WriteReg(reg_base + rOTP_CMD, TEST_CMD);
	/* wait for test done and check status */
	Wait_polling(reg_base + rOTP_STATUS, CMD_DONE_MASK,CMD_DONE_MASK, 100000000);
	RegRes = ReadReg(reg_base + rOTP_STATUS);
	printf("\r rOTP_STATUS: 0x%x\r\n", RegRes );

	RegRes = ReadReg(reg_base + rOTP_STATUS);
	printf("\r rOTP_STATUS (0x%x): 0x%x\r\n", reg_base + rOTP_STATUS, RegRes );
	
	RegRes = ReadReg(reg_base + rOTP_STATUS);
	printf("\r rOTP_STATUS : 0x%x\r\n", RegRes );

	int modePF = ((ReadReg(reg_base + rOTP_STATUS) & mask) != mask);
	if (modePF) {
		printf("mode:%d failed\n", mode);
		return 1;
	}
	modePF = ((ReadReg(reg_base + rOTP_STATUS) & TEST_ERR_MASK) == TEST_ERR_MASK);
	if (modePF) {
		printf("mode:%d done with errors:%x\n",
			mode, ReadReg(reg_base + rOTP_STATUS));
		return 1;
	}

	/* clear testmode command */
	WriteReg(reg_base + rOTP_CMD, 0x0);
	/* clear done bit */
	WriteReg(reg_base + rOTP_CMD, DONECLR_CMD);

	return 0;
}

/* test file -- return 0 for succeed and non-zero for fail */
int kilopass_test(int run_option)
{
	int error = 0;
	int run_error = 0;
	//unsigned int reg_base = (unsigned int)v_addr;
	unsigned int reg_base = 0x18260000;

	printf("kilopass test run option: %d\n", run_option);
	/* wait for boot-sequence to finish
	also make sure we're in ready-state */
	Wait_polling(reg_base + rOTP_BR_STATUS, 0x3, 0x3, 1000);
	printf("reg_base + rOTP_BR_STATUS: 0x%x\n", reg_base + rOTP_BR_STATUS);
	/* disable CC path, enable extra-OTP-access path */
	WriteReg(reg_base + rOTP_BR_CONFIG, 0x0);
	printf("reg_base + rOTP_BR_CONFIG: 0x%x\n", reg_base + rOTP_BR_CONFIG);
	/* write arbitrary timing parameter */
	WriteReg(reg_base + rOTP_CTRL_T_RW, 0xf);
	printf("reg_base + rOTP_CTRL_T_RW: 0x%x\n", reg_base + rOTP_CTRL_T_RW);

	/* make sure no error occurred checked by eye */
	error = ReadReg(reg_base + rOTP_BR_STATUS) & BR_STAT_ERR_MASK;
	if (error != 0x0) {
		printf("error %x occurred\n", error);
		return 1;
	}
	
if ((run_option & 1) == 1)
{
	printf("kilopass test run option: BLANK_TESTMODE\n");
	if (test_mode(reg_base,
		BLANK_TESTMODE, BLANK_TESTMODE_DONE_MASK))
		run_error = 1;
}
if (((run_option >> 1 ) & 1) == 1)
{
	printf("kilopass test run option: TESTDEC_TESTMODE\n");
	if (test_mode(reg_base,
		TESTDEC_TESTMODE, TESTDEC_TESTMODE_DONE_MASK))
		run_error = run_error + 2;
}
if (((run_option >> 2 ) & 1) == 1)
{

	printf("kilopass test run option: WRTEST_TESTMODE\n");
	if (test_mode(reg_base,
		WRTEST_TESTMODE, WRTEST_TESTMODE_DONE_MASK))
		run_error = run_error + 4;
}
	/* Re-enable CC path */
	WriteReg(reg_base + rOTP_BR_CONFIG, 0x1);

	return run_error;
}

