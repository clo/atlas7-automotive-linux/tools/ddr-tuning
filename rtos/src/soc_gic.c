/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "soc_gic_lib.h"
#include "FreeRTOSConfig.h"
#include "types.h"
#include "std_funcs.h"

#ifndef __ARMCC_VERSION
#define __memory_changed()
#endif /* __ARMCC_VERSION */

/********************************************************
** Razp: make it compile
*/
#define _DSB() 
#define get_cpuid() (0)

//#define DEBUG 1


/* for size considuration, unneeded GIC features are removed.
** add these features by changing the value to 1.
*/
#define FULL_FEATURE_GIC 0

int getCpuPeripheralAddress(void);

// Interrupt Distributor memory mapped registers
static distributor_interface * di = (distributor_interface *)NULL;
//static distributor_interface * const di = (distributor_interface *)MPID_BASE;

// CPU Interface memory mapped registers - banked for each CPU
static cpu_interface * ci = NULL;
//static cpu_interface * const ci = (cpu_interface *)MPIC_BASE;

// Array of IRQ handlers
static gic_handler_t * task_irq_handler[GIC_NUM_INTERRUPTS] ;
static sgi_handler_t * task_sgi_handler[GIC_NUM_SGI]  ;

//static bool gic_enabled(void)
//{
//    return (di->control & 0x01);
//}
//

static void gic_init_addr(void)
{
    unsigned int periph_base = getCpuPeripheralAddress();

    if (di == NULL) {
        di = (distributor_interface*)(periph_base + MP_ICD_OFFSET);
        ci = (cpu_interface*)(periph_base + MP_ICC_OFFSET);
//#ifdef DEBUG
//        printf("CPU %d: gic_init_addr(): periph_base = 0x%08X, di = 0x%08X, ci = 0x%08X\r\n", get_cpuid(), periph_base, (unsigned)di, (unsigned)ci);
//#endif
    }

#if defined( MP_SUPPORT ) || CACHE_MODE != 0
    AddPageTable( periph_base, periph_base, 1, TTB_SDEVICE | TTB_RW | TTB_NON_EXE | TTB_SECTION );
    _DMB();
#endif /* defined( MP_SUPPORT ) || CACHE_MODE != 0 */
}

bool gic_distributor_init(void)
{
    unsigned int i;

    //sim_step(700);
    // init addr
    gic_init_addr();

    //sim_step(702);
    // Check the ARM Legacy Identification Register Field values
    if( di->peripheral_id[0] != 0x90 ||
       (di->peripheral_id[1] != 0xB3 /* ARM Cortex-A9 */ && di->peripheral_id[1] != 0xB4 /* ARM Cortex-A7 */) ||
       (di->peripheral_id[2] != 0x1B /* ARM Cortex-A9 */ && di->peripheral_id[2] != 0x2B /* ARM Cortex-A7 */) ||
        di->peripheral_id[3] != 0x00 ||
        di->primecell_id[0] != 0x0D ||
        di->primecell_id[1] != 0xF0 ||
        di->primecell_id[2] != 0x05 ||
        di->primecell_id[3] != 0xB1 )
    {
        return false;
    }
    //sim_step(703);
#ifdef DEBUG
    printf("CPU %d: gic_distributor_init called\r\n", get_cpuid());
#endif

    //sim_step(704);
    // Disable the whole controller
    di->control.all = 0;

    //sim_step(705);
    // Disable all interrupts
    for (i = 1; i < GIC_NUM_INTERRUPTS / 32; i++)
    {// enable.clear[0] is banked, will be inited in gic_cpu_interface_init
        di->enable.clear[i] = (uint32_t)-1;
    }

    //sim_step(706);
    // Clear all pending interrupts
    for (i = 1; i < GIC_NUM_INTERRUPTS / 32; i++)
    {// pending.clear[0] is banked, will be inited in gic_cpu_interface_init
        di->pending.clear[i] = (uint32_t)-1;
    }

    //sim_step(707);
    // Reset interrupt priorities
    for (i = 32; i < GIC_NUM_INTERRUPTS; i++)
    {// priority[0-31] is banked, will be inited in gic_cpu_interface_init
        di->priority[i].all = 0;
    }

    //sim_step(708);
    // Reset interrupt targets
    for (i = 32; i < GIC_NUM_INTERRUPTS; i++)
    {// target[0-31] are read-only
        gic_set_targets(i, 0);
    }

    //sim_step(709);
    // Set interrupt configuration (level high sensitive, 1-N)
    for (i = 2; i < GIC_NUM_INTERRUPTS / 16; i++)
    {// configuration[0-1] are read-only
        di->configuration[i] = 0x55555555;
    }

// moved into gic_cpu_interface_init, because irq_handler[] are private to each core now.
//    // Clear task_irq_handler
//    for (i = 0; i < GIC_NUM_INTERRUPTS; i++) {
//        task_irq_handler[i] = NULL;
//    }
//
//    // Clear task_sgi_handler
//    for (i = 0; i < GIC_NUM_SGI; i++) {
//        task_sgi_handler[i] = NULL;
//    }

    //sim_step(710);
    // Finally, enable the interrupt controller
    di->control.enable_secure = 1;

    //sim_step(712);
    return true;
}

void gic_cpu_interface_init(void)
{
    unsigned int i;

    //sim_step(800);
    // init addr
    gic_init_addr();

    //sim_step(802);
#ifdef DEBUG
    printf("CPU %d: gic_cpu_interface_init called\r\n", get_cpuid());
#endif

    //sim_step(804);
    // Clear up the bits of the distributor which are actually CPU-specific
    di->enable.clear[0] = (uint32_t)-1;

    // note: pending reg for SGI is read-only, so there is no effect to write pending SGI reg.
    //       The only way to clear pending SGI reg are read ack reg and then write eoi reg.
    //di->pending.clear[0] = (uint32_t)-1;

    //sim_step(808);
    for (i = 0; i < 32; i++) {
        di->priority[i].all = 0;
    }

    //sim_step(812);
    // Allow any interrupts. Set priority mask to lowest priority(larget possible number)
    gic_set_priority_mask(PRIORITY_ALLOW_ALL);

    //sim_step(814);
    // forbit pre-emption
    ci->binary_point.binary_point = BINARY_NO_PREEMPTION;

    //sim_step(816);
    // Clear any pending interrupts
    //   Note: ci must be enabled, if you want to clear pending interrupts
    do {
        ICCIAR iar;
        //sim_step(818);
        //sim_value_hex(di->pending.clear[0]);

        iar = ci->interrupt_ack;

        if (iar.id == GIC_SPURIOUS_INTERRUPT) {
            //sim_step(820);
            break;
        }

        //sim_step(822);
        ci->end_of_interrupt = iar;
#ifdef DEBUG
        printf("in\r\n", get_cpuid());
#endif
    } while (true);
#ifdef DEBUG
        printf("out\r\n", get_cpuid());
#endif

    //sim_step(824);

    // Clear task_irq_handler
    for (i = 0; i < GIC_NUM_INTERRUPTS; i++) {
        task_irq_handler[i] = NULL;
    }

    //sim_step(826);
    // Clear task_sgi_handler
    for (i = 0; i < GIC_NUM_SGI; i++) {
        task_sgi_handler[i] = NULL;
    }
    //sim_step(828);
    // Enable the CPU Interface
    ci->control.enable = 1;

    //sim_step(830);
}

/**
 * Enable (enable=1) or disable (enable=0) an interrupt
 *
 * @param[in] interrupt The ID of the interrupt
 * @param[in] enable    Enable or disable (boolean)
 *
 * @return No return value
 *
 */
/**
 * Enable/Disable macros (used as a parameter in interrupt_enable() )
 */
#define ENABLE        1 /**< Interrupt enable. Used with interrupt_enable() primitive. */
#define DISABLE       0 /**< Interrupt disable. Used with interrupt_enable() primitive. */
static void interrupt_enable(unsigned int interrupt, unsigned int enable)
{
    unsigned int word;

    word = interrupt / 32;
    interrupt %= 32;
    interrupt = 1 << interrupt;

    if (enable)
    {
        di->enable.set[word] = interrupt;
    }
    else
    {
        di->enable.clear[word] = interrupt;
    }
}

/**
 * Install a specific interrupt's handler
 *
 * @param[in] interrupt The ID of the interrupt
 * @param[in] handler   The handling routine
 *
 * @return No return value
 *
 */
static void interrupt_handler_set(unsigned int interrupt, gic_handler_t handler)
{
//#ifdef DEBUG
//    printf("CPU %d: interrupt_handler_set called, int_id = %d, handler = 0x%08X\r\n", get_cpuid(), interrupt, handler);
//#endif
    if (interrupt >= GIC_NUM_INTERRUPTS) {
        return;
    }
    task_irq_handler[interrupt] = handler;

    _DSB();
}

/**
 * Install a specific handler for a SGI
 *
 * @param id The SGI ID
 * @param handler   The handling routine
 *
 * @return No return value
 *
 */
static void sgi_handler_set(sgi_id_t id, sgi_handler_t handler)
{
//#ifdef DEBUG
//    printf("CPU %d: sgi_handler_set called, int_id = %d, handler = 0x%08X\r\n", get_cpuid(), id, handler);
//#endif
    if (id >= GIC_NUM_SGI) {
        return;
    }
    task_sgi_handler[id] = handler;

    _DSB();
}
#if FULL_FEATURE_GIC
void sgi_broadcast_to_others(sgi_id_t id)
{
    ICDSGIR sgir;

    _DSB();

    sgir = di->software_interrupt;

    sgir.sgi_id = id;
    sgir.targetlist_filter = SGI_ALL_BUT_SELF;

    di->software_interrupt = sgir;

    __memory_changed();      // Compiler must store all registers back to memory
    _DSB();
}
#endif
#if FULL_FEATURE_GIC
void sgi_send_to_self(sgi_id_t id)
{
    ICDSGIR sgir;

    _DSB();

    sgir = di->software_interrupt;

    sgir.sgi_id = id;
    sgir.targetlist_filter = SGI_SELF;

    di->software_interrupt = sgir;

    __memory_changed();      // Compiler must store all registers back to memory
    _DSB();
}
#endif
#if FULL_FEATURE_GIC

void sgi_send(unsigned int target_list, sgi_id_t id)
{
    ICDSGIR sgir;

    _DSB();

    sgir = di->software_interrupt;

    sgir.sgi_id = id;
    sgir.targetlist_filter = SGI_USE_TARGET_LIST;
    sgir.targetlist = target_list;

    di->software_interrupt = sgir;

    __memory_changed();      // Compiler must store all registers back to memory
    _DSB();
}
#endif



void vApplicationIRQHandler( uint32_t IccIarValue )
{
    unsigned int source, interrupt;

    //sim_step(99900);

    // init addr
    //gic_init_addr();

#ifdef MP_SUPPORT
    //enable_dcache(); //FIXME: to be removed // when task handler is shared, if dcache is not enabled, cpu1 could not read out the correct handler
#endif

    {
        //*(uint32_t*) (& raw_interrupt) = IccIarValue;
        // Get the highest priority interrupt
        
        source = ((IccIarValue >> 10 ) & 0x000007) ;
        interrupt = (IccIarValue & 0x000003ff);

#ifdef DEBUG
        printf("Irq_id = %d\r\n", interrupt);
#endif

        if (interrupt == GIC_SPURIOUS_INTERRUPT) {
            //sim_step(99910);
            return;
        }

        // Call the handler function
        if (interrupt < GIC_NUM_SGI && task_sgi_handler[interrupt] != NULL) 
        {
            task_sgi_handler[interrupt]((sgi_id_t)interrupt, source);
        } 
        else 
        {
            if (task_irq_handler[interrupt] != NULL) 
            {
                task_irq_handler[interrupt](interrupt);
            }
        }
        
    }
}

//void gic_secondary_cpu_init_for_wakeup_interrupt(sgi_id_t id)
//{
//    // init addr
//    gic_init_addr();
//
//    // set to highest priority
//    gic_set_priority(PRIORITY_HIGHEST);
//
//    // sgi is always enabled, no need to set the enable bit
//
//    // ensure the interrupt could pass through
//    gic_set_priority_mask(PRIORITY_ALLOW_ALL);
//
//    // enable cpu interface
//    ci->control.enable = 1;
//
//    // if we clear the interrupt before we enable irq, the handler will be unnecessary
//    //gic_sgi_enable(id, secondary_cpus_wakeup_handler);
//}

////////////////////////////////////////////////////////////////////////////////
// user interface
//

void gic_irq_enable(unsigned int gic_id, gic_handler_t handler)
{
#ifdef DEBUG
    printf("CPU %d: gic_irq_enable called, gic_id = %d, handler = 0x%X\r\n", get_cpuid(), gic_id, handler);
#endif

    if (gic_id >= SPI_ID_BASE) {
        gic_add_target(gic_id, get_cpuid());
    }
    interrupt_handler_set(gic_id, handler);
    interrupt_enable(gic_id, ENABLE);
}

void gic_irq_disable(unsigned int gic_id)
{
#ifdef DEBUG
    printf("CPU %d: gic_irq_disable called, gic_id = %d\r\n", get_cpuid(), gic_id);
#endif

    interrupt_enable(gic_id, DISABLE);
    if (gic_id >= SPI_ID_BASE) {
        gic_remove_target(gic_id, get_cpuid());
    }
    interrupt_handler_set(gic_id, (gic_handler_t *)NULL);
    if (gic_id < GIC_NUM_SGI) {
        sgi_handler_set((sgi_id_t)gic_id, (sgi_handler_t *)NULL);
    }
}

void gic_sgi_enable(sgi_id_t id, sgi_handler_t handler)
{
    if (id < GIC_NUM_SGI) {
        sgi_handler_set(id, handler);
        interrupt_enable(id, ENABLE);
    }
}

void gic_add_target(unsigned int gic_id, unsigned int cpu_id)
{
    di->target[gic_id] |= 1 << cpu_id;
}


void gic_remove_target(unsigned int gic_id, unsigned int cpu_id)
{
    di->target[gic_id] &= ~(1 << cpu_id);
}

unsigned char gic_get_targets(unsigned int gic_id)
{
    return di->target[gic_id];
}


void gic_set_targets(unsigned int gic_id, unsigned int target_list)
{
    di->target[gic_id] = target_list;
}

unsigned char gic_get_priority(unsigned int gic_id)
{
    return di->priority[gic_id].priority;
}

#if FULL_FEATURE_GIC

unsigned char gic_set_priority(unsigned int gic_id, unsigned char priority)
{
    unsigned char old_priority;

    old_priority = gic_get_priority(gic_id);

    di->priority[gic_id].priority = priority;

    return old_priority;
}
#endif


unsigned char gic_get_priority_mask(void)
{
    return ci->priority_mask.priority;
}


unsigned char gic_set_priority_mask(unsigned char priority_mask)
{
    unsigned char old_priority_mask;

    old_priority_mask = gic_get_priority_mask();

    ci->priority_mask.priority = priority_mask;

    return old_priority_mask;
}

#if FULL_FEATURE_GIC

unsigned int gic_get_highest_priority_pending_id(void)
{
    return ci->highest_pending.id;
}
#endif
#if FULL_FEATURE_GIC

unsigned char gic_get_running_priority(void)
{
    return ci->running_priority.priority;
}

#endif
#if FULL_FEATURE_GIC

bool gic_is_spi_pending(unsigned int gic_id)
{
    int word;
    int bit;
    unsigned int status_wd;
    unsigned int spi_id;

    if (gic_id < SPI_ID_BASE) {
        return false;
    }

    spi_id = gic_id - SPI_ID_BASE;
    word = spi_id / 32;
    bit = 1 << (spi_id % 32);
    status_wd = di->spi_status[word];
    return ((status_wd & bit) != 0);
}
#endif
#if FULL_FEATURE_GIC

bool gic_has_pending_spi(void)
{
    int id;
    for (id=SPI_ID_BASE; id<GIC_NUM_INTERRUPTS; id++) {
        if ( gic_is_spi_pending(id) ) {
            return true;
        }
    }
    return false;
}
#endif
#if FULL_FEATURE_GIC

unsigned int gic_get_smallest_pending_spi(void)
{
    int id;
    for (id=SPI_ID_BASE; id<GIC_NUM_INTERRUPTS; id++) {
        if ( gic_is_spi_pending(id) ) {
            return id;
        }
    }
    return GIC_SPURIOUS_INTERRUPT;
}
#endif
#if FULL_FEATURE_GIC

void gic_print_spi_status(void)
{
    int i;
    for (i=0; i<(GIC_NUM_SPI+31)/32; i++) {
        printf("CPU %d: gic: spi_status[%0d] = 0x%08X\r\n", get_cpuid(), i, di->spi_status[i]);
    }
}
#endif
#if FULL_FEATURE_GIC

void gic_print_all_pending_spi(void)
{
    int id;
    for (id=SPI_ID_BASE; id<GIC_NUM_INTERRUPTS; id++) {
        if ( gic_is_spi_pending(id) ) {
            printf("CPU %d: gic: pending SPI found, gic_id = %0d\r\n", get_cpuid(), id);
        }
    }
}
#endif
#if FULL_FEATURE_GIC

//////////////////////////////////////////////////////////////////
bool gic_is_id_pending(unsigned int gic_id)
{
    int word;
    int bit;
    unsigned int pending_wd;
    bool is_id_pending;

    word = gic_id / 32;
    bit = 1 << (gic_id % 32);
    pending_wd = di->pending.set[word];
    is_id_pending = ((pending_wd & bit) != 0);
    return is_id_pending;
}
#endif
#if FULL_FEATURE_GIC

unsigned int gic_get_smallest_pending_id(void)
{
    int id;
    for (id=0; id<GIC_NUM_INTERRUPTS; id++) {
        if ( gic_is_id_pending(id) ) {
            return id;
        }
    }
    return GIC_SPURIOUS_INTERRUPT;
}
#endif

#if FULL_FEATURE_GIC
void gic_print_all_pending_id(void)
{
    int id;
    for (id=0; id<GIC_NUM_INTERRUPTS; id++) {
        if ( gic_is_id_pending(id) ) {
            printf("gic: pending id = %0d\r\n", id);
        }
    }
}
#endif



