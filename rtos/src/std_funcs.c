/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

#include "types.h"
//#include "debug.h"
#include "FreeRTOS.h"
#include "std_funcs.h"

/****************************************************************
** simple implementqation of library functions 
**  strncpy()
*/
char *
strncpy(char *dest, const char *src, size_t n)
{
    size_t i;
    
    for (i = 0; i < n && src[i] != '\0'; i++)
    {
        dest[i] = src[i];
    }
    for ( ; i < n; i++)
    {
        dest[i] = '\0';
    }
    return dest;
}
/*********************************************************************
**  strcpy 
*/
char *
strcpy(char *dest, const char *src)
{
    return strncpy( dest, src, strlen(src) +1  );
}


/**************************************************************************
** strncmp() 
*/
int strncmp (const char * const s1, const char * const s2, const size_t num)
{
    const unsigned char * const us1 = (const unsigned char *) s1;
    const unsigned char * const us2 = (const unsigned char *) s2;
    size_t i=0;
    for(i = (size_t)0; i < num; ++i)
    {
        if(us1[i] < us2[i])
           return -1;
        else if(us1[i] > us2[i])
           return 1;
        else if( '\0' == us1[i]) /* null byte -- end of string */
           return 0;
    }
 
    return 0;
}
#define max(a,b) ((a>b)?a:b)
/**************************************************************************
** strcmp() 
*/
int strcmp (const char * const s1, const char * const s2)
{
    size_t num = max( strlen(s1), strlen(s2) );
    return strncmp( s1, s2, num +1);
}

/**************************************************************************
** strlen() 
*/

size_t strlen(const char* str )
{
    unsigned int count = 0;
    while( str[count] != '\0' )
    {
        count++;
    }
    return count;
}

/**************************************************************************
** strncat() 
*/
char*
strncat(char *dest, const char *src, size_t n)
{
    size_t i;
    size_t dest_len = strlen(dest);

    for (i = 0 ; i < n && src[i] != '\0' ; i++)
    {
        dest[dest_len + i] = src[i];
    }
    dest[dest_len + i] = '\0';

    return dest;
}

/**********************************************************************
** memset()
*/
void *memset(void *dst, int fill, unsigned long length)
{
    char *src = dst;

    /* Truncate c to 8 bits */
    fill = (fill & 0xFF);

    /* Simple, byte oriented memset or the rest of count. */
    while (length--)
    {
        *src++ = fill;
    }
    return dst;
}

/**********************************************************************
** memcpy()
*/
void *memcpy(void *out, const void *in, DWORD n)
{
    int i;
    char* d = out;
    const char* s = in;

    for (i = 0; i < n; i++) {
        d[i] = s[i];
    }

    return out;
}
