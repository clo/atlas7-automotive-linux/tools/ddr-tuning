/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/* otp.c for scratchpad  application */

#include "FreeRTOSConfig.h"
#if SUPPORT_OTP

#include "types.h"
#include "uart1.h"
#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "std_funcs.h"
#ifndef A7_STEP_B
#include "soc_gic.h"
#else
#include "soc_gic_b.h"
#endif
#include "otp.h"
#include "chip_utils.h"
#include "gpio_pw_funcs.h"

unsigned long PeByteData1 = 0xAAAAAAAA;
unsigned long PeByteData2 = 0x55555555;

  unsigned long *pGpioData[16] = {
(unsigned long *) 0x13300230U,
(unsigned long *) 0x13300234U,
(unsigned long *) 0x13300238U,
(unsigned long *) 0x1330023CU,
(unsigned long *) 0x13300240U,
(unsigned long *) 0x13300244U,
(unsigned long *) 0x13300248U,
(unsigned long *) 0x1330024CU,
(unsigned long *) 0x13300250U,
(unsigned long *) 0x13300254U,
(unsigned long *) 0x13300258U,
(unsigned long *) 0x1330025CU,
(unsigned long *) 0x13300260U,
(unsigned long *) 0x13300264U,
(unsigned long *) 0x13300268U,
(unsigned long *) 0x13300270U,
};

static void pw_set_gpio (unsigned long *gp_reg, int val, int dir)
{
  *gp_reg = (*gp_reg & 0x9f) | ((val&1) << 6) | ((dir&1) << 5);
  return;
}

 int otp_test (int non_blank_read) {
	 
	 int err			 = 0;
	 int start_aib_addr  = 0x0;
	 int num_words		 = 0x20;
	 
 //Check AIB	
	 if (non_blank_read) Write_Aib(start_aib_addr, num_words);
	 err = Read_Aib(start_aib_addr, num_words);
	 return (err);
 }


 void  Write_Aib
 (
  unsigned int start_address,  
  unsigned int num_words) {
 
	 unsigned int i;
	 unsigned int write_pefix = 0x1 << 17 ;
	 unsigned long WriteByte = 0;
 
	for( i = 0 ; i < num_words ; i++) {
		
		if ((i%2) == 0) WriteByte = PeByteData1;
		else WriteByte = PeByteData2;

		 printf("\rProgramming address 0x%x with value 0x%x\r\n", (start_address+i), WriteByte);
		 WriteReg(SEC_DXH_HOST_AIB_WDATA_REG , WriteByte);
		 WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , write_pefix + ((start_address + i) << 2));
		 Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		 Wait(SEC_DXH_AIB_FUSE_PROG_COMPLETED , 0x1 ,0x1);
	} 
 
 }
 unsigned int Write_OTP
 (
  unsigned int address,  
  unsigned long WriteByte) {
 
	 unsigned int write_pefix = 0x1 << 17 ;
 
		 printf("\rProgramming address 0x%x with value 0x%x\r\n", address, WriteByte);
		 WriteReg(SEC_DXH_HOST_AIB_WDATA_REG , WriteByte);
		 WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , write_pefix + ( address  << 2));
		 Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		 Wait(SEC_DXH_AIB_FUSE_PROG_COMPLETED , 0x1 ,0x1);
		 return 0;
 }
 
 unsigned int Read_Aib	
(
  unsigned int start_address,  
  unsigned int num_words ) {
 
	 unsigned int i;
	 unsigned int read_pefix = 0x1 << 16 ;
	 unsigned long regStat = 0x0;

	for( i=0 ; i < num_words ; i++) {
		
		WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , read_pefix + start_address + (i<<2) );
		Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		regStat = ReadReg(SEC_DXH_HOST_AIB_RDATA_REG);
		printf("\r byte(0x%X): 0x%X\r\n", start_address + i , regStat );
	} 

	return 0;
  	}

 unsigned int Read_Aib_char	
(
  unsigned int StartAddr,  
  unsigned int EndAddr) 
{
 
	 unsigned int regStat = 0;
	 unsigned int read_pefix = 0x1 << 16 ;
	 unsigned long error = 0;
	 unsigned int ErrorCounter = 0;
	 unsigned int i = 0;
	 unsigned long ExpectedResult = 0;

//clear work indicators
pw_set_gpio(pGpioData[1], 0, 1); 
pw_set_gpio(pGpioData[2], 0, 1); 

pw_set_gpio(pGpioData[1], 1, 1); //Start work indicator

	for (i = StartAddr ;i < EndAddr; i++)
	{	
		if ((i%2)==0) ExpectedResult = PeByteData1;
		else ExpectedResult = PeByteData2;
		error = 0;

		WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , read_pefix + ((StartAddr + i) << 2));
		Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		regStat = ReadReg(SEC_DXH_HOST_AIB_RDATA_REG);
		if (regStat == ExpectedResult) error = 0;
		else error = 1;
		
		if (error == 1) {
		ErrorCounter = ErrorCounter + 1;
	        printf("\rError: %d, Address 0x%x read error, Expected: 0x%x, Received:0x%x, ErrorCounter %d\r\n", 			error, i,ExpectedResult, regStat, ErrorCounter);
		}
}
		pw_set_gpio(pGpioData[2], 1, 1); //End work indicator	
		printf("\rErrorConuter: %d\r\n", ErrorCounter);
		
		if(ErrorCounter){
			pw_set_gpio(pGpioData[0], 0, 1);
			pw_set_gpio(pGpioData[3], 0, 1);
		}
		else
		{
			pw_set_gpio(pGpioData[0], 1, 1);
			pw_set_gpio(pGpioData[3], 1, 1);
		}

	
	return 0;
  	}
 
unsigned int Read_OTP_Verify	
(
  unsigned int OtpReadAddr,  
  unsigned long ExpectedOtpValue) 
{
 
	 unsigned int regStat = 0;
	 unsigned int read_pefix = 0x1 << 16 ;
	 unsigned long error = 0;

		WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , read_pefix + (OtpReadAddr << 2));
		Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		regStat = ReadReg(SEC_DXH_HOST_AIB_RDATA_REG);
		if (regStat == ExpectedOtpValue) error = 0;
		else error = 1;
		printf("\rByte: 0x%x (0x%x, %d)\r\n",OtpReadAddr, regStat,ExpectedOtpValue, error );
	
	return error;
}

unsigned int Read_OTP_Value	
(
  unsigned int OtpReadAddr,  
  unsigned long *OtpReadValue) 
{
 
	 unsigned int regStat = 0;
	 unsigned int read_pefix = 0x1 << 16 ;
	 unsigned long error = 0;

		WriteReg(SEC_DXH_HOST_AIB_ADDR_REG , read_pefix + (OtpReadAddr << 2));
		Wait(SEC_DXH_AIB_FUSE_ACK ,0x1,0x1);
		regStat = ReadReg(SEC_DXH_HOST_AIB_RDATA_REG);
		error = 1;
		*OtpReadValue = regStat;
		printf("\rByte(0x%X): 0x%X, 0x%X\r\n",OtpReadAddr, regStat, *OtpReadValue);
			
	return error;
}
 #endif // SUPPORT_OTP
 


