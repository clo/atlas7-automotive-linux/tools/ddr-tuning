/*
 * Copyright (c) 2016, The Linux Foundation. All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 and only version 2 as published by the Free Software
 * Foundation. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more details. 
 */

/* main.c for scratchpad  application */

#include "types.h"
#include "uart1.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "std_funcs.h"
#include "clkrstio.h"
#ifndef A7_STEP_B
#include "soc_gic.h"
#else
#include "soc_gic_b.h"
#endif
#include "boot.h"

int getCpuPeripheralAddress(void);
#define HELLO_WORLD_TASK 0
#if HELLO_WORLD_TASK
static void HelloWorldTask( void *pvParameters );
#endif


#define INJECT_COMMANDS_TASK 0
#if INJECT_COMMANDS_TASK
static void InjectTask( void *pvParameters );
#endif

#if SUPPORT_ATE_2_DUT
void Ate2DutTask( void *pvParameters );
#endif

void vRegisterCLICommands( void );
void vRegisterDemoCommands( void );
void vCommandInterpreterTask( void *pvParameters );
#define main_CLI_TASK_PRIORITY			( tskIDLE_PRIORITY+2 )

__attribute__(( used )) uint32_t ulICCIAR = 0;//portICCIAR_INTERRUPT_ACKNOWLEDGE_REGISTER_ADDRESS;
__attribute__(( used )) uint32_t ulICCEOIR = 0;//portICCEOIR_END_OF_INTERRUPT_REGISTER_ADDRESS;
__attribute__(( used )) uint32_t ulICCPMR	= 0;//portICCPMR_PRIORITY_MASK_REGISTER_ADDRESS;
__attribute__(( used )) const uint32_t ulMaxAPIPriorityMask = ( configMAX_API_CALL_INTERRUPT_PRIORITY << portPRIORITY_SHIFT );
__attribute__(( used )) uint32_t ulCpuPeripheralAddress = 0;
/*
 * The Xilinx projects use a BSP that do not allow the start up code to be
 * altered easily.  Therefore the vector table used by FreeRTOS is defined in
 * FreeRTOS_asm_vectors.S, which is part of this project.  Switch to use the
 * FreeRTOS vector table.
 */
extern void vPortInstallFreeRTOSVectorTable( void );

void prvSetupHardware(void)
{   
    if( (rRESET_LATCH_REGISTER & ( 1 << 9 )) == 0 ) // Not in ATE mode
    {

        volatile unsigned long * rCLKC_IO_SELECT = (unsigned long *)0x186201C4;
        *rCLKC_IO_SELECT = 0x3;
     
       /* assuming ROM already initilize all most needed HW, nothing to do here 
        OSTimerInit( configPERIPHERAL_CLOCK_HZ, 0 ); 
      */
        #if SUPPORT_DDR 
           uartInit( 9600UL, 150000000UL );  // Run at 9600 bps for stable DDR bench testing!
        #else
           uartInit( 115200UL, 150000000UL );
        #endif
    }
    else
    {
        uartInit( 115200UL, (26000000UL) / 8  ); // ATE mode:  all clocks in fixed bypass mode at freq of 26MHz / 8 
    }
    

    ulCpuPeripheralAddress = getCpuPeripheralAddress();
    ulICCIAR = portICCIAR_INTERRUPT_ACKNOWLEDGE_REGISTER_ADDRESS;
    ulICCEOIR = portICCEOIR_END_OF_INTERRUPT_REGISTER_ADDRESS;
    ulICCPMR	= portICCPMR_PRIORITY_MASK_REGISTER_ADDRESS;

	/* The Xilinx projects use a BSP that do not allow the start up code to be
	altered easily.  Therefore the vector table used by FreeRTOS is defined in
	FreeRTOS_asm_vectors.S, which is part of this project.  Switch to use the
	FreeRTOS vector table. */
	vPortInstallFreeRTOSVectorTable();

    gic_distributor_init();
    gic_cpu_interface_init();

    /* Note that UART interrupt, or any interrupt may initiate task switch,
        interrupt should not accure before  vTaskStartScheduler() */
    uartGicInit(); 

}


/*************************************************************************
***    main start here
*************************************************************************/
int main()
{   
    /* send "Closing Sequance" to uart, to indicate that upload was completed */
    SendClosingSequence();
    dumpString("\r\n" __DATE__ " - " __TIME__"\r\nStarting FreeRTOS\r\n" );

    /* configure HW */
    prvSetupHardware();

    /* starting CLI task */
#if HELLO_WORLD_TASK
    xTaskCreate( HelloWorldTask, "Hello World", configMINIMAL_STACK_SIZE, NULL, main_CLI_TASK_PRIORITY, NULL );
#endif    
#if INJECT_COMMANDS_TASK
    xTaskCreate( InjectTask, "Inject", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
#endif    
#if SUPPORT_ATE_2_DUT
    xTaskCreate( Ate2DutTask, "Ate2Dut", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL );
#endif
    xTaskCreate( vCommandInterpreterTask, "CLI", configMINIMAL_STACK_SIZE, NULL, main_CLI_TASK_PRIORITY, NULL ); 
    /* register commands */
    vRegisterCLICommands(); 
    
    //vRegisterDemoCommands(); 

    vTaskStartScheduler();

    /* If all is well, the scheduler will now be running, and the following
	line will never be reached.  If the following line does execute, then
	there was either insufficient FreeRTOS heap memory available for the idle
	and/or timer tasks to be created, or vTaskStartScheduler() was called from
	User mode.  See the memory management section on the FreeRTOS web site for
	more details on the FreeRTOS heap http://www.freertos.org/a00111.html.  The
	mode from which main() is called is set in the C start up code and must be
	a privileged mode (not user mode). */
    for( ;; ) ;    


	return 0;
}


#if HELLO_WORLD_TASK

static void HelloWorldTask( void *pvParameters )
{
    portTickType xNextWakeTime;

    /* Remove warning about unused parameters. */
    ( void ) pvParameters;

    /* Initialise xNextWakeTime - this only needs to be done once. */
    xNextWakeTime = xTaskGetTickCount();

    for( ;; )
    {
        printf( "Hello World\r\n" );
        vTaskDelayUntil( &xNextWakeTime, 1000 );
    }
}

#endif


#if INJECT_COMMANDS_TASK
static void InjectCommand( char* cmd )
{
    while( *cmd != '\0' )
    {
        InjectChar( *cmd );        
        cmd++;
    }
    InjectChar( '\r' );
    InjectChar( '\n' );
}


static void InjectTask( void *pvParameters )
{
    /* Remove warning about unused parameters. */
    ( void ) pvParameters;
    
    vTaskDelay( 1 );
    InjectCommand( "help" );
    vTaskDelay( 1 );
    InjectCommand( "aaa" );
    
    for( ;; )
        vTaskDelay( 100000 );

}

#endif

