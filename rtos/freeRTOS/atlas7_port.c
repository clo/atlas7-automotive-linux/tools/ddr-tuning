/* port for CSR Atlas 7 */

#include<stdint.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "types.h"
#include "uart1.h"

#include "soc.h"
#ifndef A7_STEP_B
#include "soc_gic.h"
#else
#include "soc_gic_b.h"
#endif
#include "clkc.h"
#include "chip_utils.h"
#include "std_funcs.h"

extern uint32_t ulICCPMR;
#define WRITE_PRIORITY_MASK_REGISTER(v)                     (*( ( volatile uint32_t * )ulICCPMR)) = v
#define READ_PRIORITY_MASK_REGISTER()                       (*( ( volatile uint32_t * )ulICCPMR))

/* A variable is used to keep track of the critical section nesting.  This
variable has to be stored as part of the task context and must be initialised to
a non zero value to ensure interrupts don't inadvertently become unmasked before
the scheduler starts.  As it is stored as part of the task context it will
automatically be set to 0 when the first task is started. */
volatile uint32_t ulCriticalNesting = 9999UL;


/* A critical section is exited when the critical section nesting count reaches
this value. */
#define portNO_CRITICAL_NESTING			( ( uint32_t ) 0 )

/* Masks all bits in the APSR other than the mode bits. */
#define portAPSR_MODE_BITS_MASK			( 0x1F )

/* The value of the mode bits in the APSR when the CPU is executing in user
mode. */
#define portAPSR_USER_MODE				( 0x10 )

/* In all GICs 255 can be written to the priority mask register to unmask all
(but the lowest) interrupt priority. */
#define portUNMASK_VALUE				( 0xFFUL )


/* Saved as part of the task context.  If ulPortTaskHasFPUContext is non-zero
    then a floating point context must be saved and restored for the task. */
uint32_t ulPortTaskHasFPUContext = 0;

/* Set to 1 to pend a context switch from an ISR. */
uint32_t ulPortYieldRequired = 0;

/* Counts the interrupt nesting depth.  A context switch is only performed if
    if the nesting depth is 0. */
uint32_t ulPortInterruptNesting = 0UL;


void vPortRestoreTaskContext(void);

/*-----------------------------------------------------------*/

/* The critical section macros only mask interrupts up to an application
determined priority level.  Sometimes it is necessary to turn interrupt off in
the CPU itself before modifying certain hardware registers. */
#define portCPU_IRQ_DISABLE()	__asm__ __volatile__("CPSID if");

#define portCPU_IRQ_ENABLE()	__asm__ __volatile__("CPSIE if");


/* Macro to unmask all interrupt priorities. */
#define portCLEAR_INTERRUPT_MASK()									\
{																	\
	portCPU_IRQ_DISABLE();											\
	WRITE_PRIORITY_MASK_REGISTER( portUNMASK_VALUE );   			\
	__asm(	"DSB		\n"											\
			"ISB		\n" );										\
	portCPU_IRQ_ENABLE();											\
}

/*****************************************************************************
** ASSERT()
**  no dump message to consol and never continue. 
*/
void vAssertCalled( const char * str, const char * pcFile, unsigned long ulLine  )
{ 
    char string[150] = {'\0'};
    sprintf(string, "Assertion Fail ( %s ) in %s:%d\n", str, pcFile, (int)ulLine);
    
    
    gic_irq_disable(SPI_ID_UART1);
    
    dumpString( string );

    /* this is the end! */
    for(;;) ;
}
void vPortValidateInterruptPriority( void )
{

}
/*****************************************************************************************************/
void FreeRTOS_Tick_Handler()
{
	/* Set interrupt mask before altering scheduler structures.   The tick
	handler runs at the lowest priority, so interrupts cannot already be masked,
	so there is no need to save and restore the current mask value.  It is
	necessary to turn off interrupts in the CPU itself while the ICCPMR is being
	updated. */

	portCPU_IRQ_DISABLE();
	WRITE_PRIORITY_MASK_REGISTER( ( uint32_t ) ( configMAX_API_CALL_INTERRUPT_PRIORITY << portPRIORITY_SHIFT ) );
	__asm(	"dsb		\n"
			"isb		\n" );
	portCPU_IRQ_ENABLE();

	/* Increment the RTOS tick. */
	if( xTaskIncrementTick() != pdFALSE )
	{
		ulPortYieldRequired = pdTRUE;
	}

	/* Ensure all interrupt priorities are active again. */
	portCLEAR_INTERRUPT_MASK();      
	/* Clear Tick Interrupt */
    rTIMER_INTR_STATUS = 0x1;
}

void gic_Tick_Handler( unsigned int gic_id)
{
    FreeRTOS_Tick_Handler();
}


/* function that generate a periodic interrupt at the required frequency: ~1kHz */
#define TIMER_LOOP_EN          0x4
#define TIMER_INT_EN           0x2
#define TIMER_CNT_EN           0x1

void vConfigureTickInterrupt( void )
{
    /* using TIMER 0 in this implimentation */
    WRITE_REG( CLKC_ROOT_CLK_EN0_SET, 1 << CLKC_ROOT_CLK_EN0_SET__AUDMSCM_IO__SHIFT );
#ifdef A7_STEP_B
    WRITE_REG( CLKC_MISC1_LEAF_CLK_EN_SET, 1 << CLKC_MISC1_LEAF_CLK_EN_SET__TIMER_IO_CLKEN__SHIFT );
#else
    WRITE_REG( CLKC_LEAF_CLK_EN1_SET, 1 << CLKC_LEAF_CLK_EN1_SET__TIMER_IO_CLKEN__SHIFT);
#endif
    rTIMER_MATCH_0 = 300;        /* Approximate delay between programming interrupt and triggering this interrupt. */
    rTIMER_COUNTER_0 = 0;
    
    /* timer will count in loop, creating an interrupt every loop */
    rTIMER_32COUNTER_0_CTRL = ( 75 << 16) | TIMER_LOOP_EN | TIMER_INT_EN | TIMER_CNT_EN;

    /*  Install the tick Interrupt */
    gic_irq_enable( SPI_ID_TIMER0, gic_Tick_Handler );
}


/************************************************************************
** interrupt masking
*/

void vPortClearInterruptMask( uint32_t ulNewMaskValue )
{
	if( ulNewMaskValue == pdFALSE )
	{
		portCLEAR_INTERRUPT_MASK();
	}
}
/*-----------------------------------------------------------*/

uint32_t ulPortSetInterruptMask( void )
{
uint32_t ulReturn;

	/* Interrupt in the CPU must be turned off while the ICCPMR is being
	updated. */
	portCPU_IRQ_DISABLE();
	if( READ_PRIORITY_MASK_REGISTER() == ( uint32_t ) ( configMAX_API_CALL_INTERRUPT_PRIORITY << portPRIORITY_SHIFT ) )
	{
		/* Interrupts were already masked. */
		ulReturn = pdTRUE;
	}
	else
	{
		ulReturn = pdFALSE;
		WRITE_PRIORITY_MASK_REGISTER( ( uint32_t ) ( configMAX_API_CALL_INTERRUPT_PRIORITY << portPRIORITY_SHIFT ) );
		__asm(	"dsb		\n"
				"isb		\n" );
	}
	portCPU_IRQ_ENABLE();

	return ulReturn;
}


/** critical sections **/

void vPortEnterCritical( void )
{
	/* Mask interrupts up to the max syscall interrupt priority. */
	ulPortSetInterruptMask();

	/* Now interrupts are disabled ulCriticalNesting can be accessed
	directly.  Increment ulCriticalNesting to keep a count of how many times
	portENTER_CRITICAL() has been called. */
	ulCriticalNesting++;
}
/*-----------------------------------------------------------*/

void vPortExitCritical( void )
{
	if( ulCriticalNesting > portNO_CRITICAL_NESTING )
	{
		/* Decrement the nesting count as the critical section is being
		exited. */
		ulCriticalNesting--;

		/* If the nesting level has reached zero then all interrupt
		priorities must be re-enabled. */
		if( ulCriticalNesting == portNO_CRITICAL_NESTING )
		{
			/* Critical nesting has reached zero so all interrupt priorities
			should be unmasked. */
			portCLEAR_INTERRUPT_MASK();
		}
	}
}


/*-----------------------------------------------------------*/

/* Tasks are not created with a floating point context, but can be given a
floating point context after they have been created.  A variable is stored as
part of the tasks context that holds portNO_FLOATING_POINT_CONTEXT if the task
does not have an FPU context, or any other value if the task does have an FPU
context. */
#define portNO_FLOATING_POINT_CONTEXT	( ( StackType_t ) 0 )

/* Constants required to setup the initial task context. */
/* TODO: verify tese values */
#define portINITIAL_SPSR				( ( StackType_t ) 0x1f ) /* System mode, ARM mode, IRQ enabled FIQ enabled. */
#define portTHUMB_MODE_BIT				( ( StackType_t ) 0x20 )
#define portINTERRUPT_ENABLE_BIT		( 0x80UL )
#define portTHUMB_MODE_ADDRESS			( 0x01UL )
/*
 * See header file for description.
 */
StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters )
{
	/* Setup the initial stack of the task.  The stack is set exactly as
	expected by the portRESTORE_CONTEXT() macro.

	The fist real value on the stack is the status register, which is set for
	system mode, with interrupts enabled.  A few NULLs are added first to ensure
	GDB does not try decoding a non-existent return address. */
	*pxTopOfStack = ( StackType_t ) NULL;
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) NULL;
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) NULL;
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) portINITIAL_SPSR;

	if( ( ( uint32_t ) pxCode & portTHUMB_MODE_ADDRESS ) != 0x00UL )
	{
		/* The task will start in THUMB mode. */
		*pxTopOfStack |= portTHUMB_MODE_BIT;
	}

	pxTopOfStack--;

	/* Next the return address, which in this case is the start of the task. */
	*pxTopOfStack = ( StackType_t ) pxCode;
	pxTopOfStack--;

	/* Next all the registers other than the stack pointer. */
	*pxTopOfStack = ( StackType_t ) 0x00000000;	/* R14 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x12121212;	/* R12 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x11111111;	/* R11 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x10101010;	/* R10 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x09090909;	/* R9 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x08080808;	/* R8 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x07070707;	/* R7 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x06060606;	/* R6 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x05050505;	/* R5 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x04040404;	/* R4 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x03030303;	/* R3 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x02020202;	/* R2 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) 0x01010101;	/* R1 */
	pxTopOfStack--;
	*pxTopOfStack = ( StackType_t ) pvParameters; /* R0 */
	pxTopOfStack--;

	/* The task will start with a critical nesting count of 0 as interrupts are
	enabled. */
	*pxTopOfStack = portNO_CRITICAL_NESTING;
	pxTopOfStack--;

	/* The task will start without a floating point context.  A task that uses
	the floating point hardware must call vPortTaskUsesFPU() before executing
	any floating point instructions. */
	*pxTopOfStack = portNO_FLOATING_POINT_CONTEXT;

	return pxTopOfStack;
}
/*-----------------------------------------------------------*/
void vPortTaskUsesFPU( void )
{
	/* A task is registering the fact that it needs an FPU context. 
	Set the FPU flag (saved as part of the task context. */
	ulPortTaskHasFPUContext = pdTRUE;

	/* Initialise the floating point status register. */
	__asm volatile (
		"MOV     R0, #0         \n\t"
		"FMXR    FPSCR, R0      \n\t"
	);
}

/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* Not implemented in ports where there is nothing to return to.
	Artificially force an assert. */
	configASSERT( ulCriticalNesting == 1000UL );
}

BaseType_t xPortStartScheduler( void )
{
    uint32_t ulAPSR;

	/* Only continue if the CPU is not in User mode.  The CPU must be in a
	Privileged mode for the scheduler to start. */
	__asm volatile ( "MRS %0, APSR" : "=r" ( ulAPSR ) );
	ulAPSR &= portAPSR_MODE_BITS_MASK;
	configASSERT( ulAPSR != portAPSR_USER_MODE );

	if( ulAPSR != portAPSR_USER_MODE )
	{
		/* Interrupts are turned off in the CPU itself to ensure tick does
		not execute	while the scheduler is being started.  Interrupts are
		automatically turned back on in the CPU when the first task starts
		executing. */
		portCPU_IRQ_DISABLE();

		/* Start the timer that generates the tick ISR. */
		configSETUP_TICK_INTERRUPT();

		/* Start the first task executing. */
		vPortRestoreTaskContext();
	}

	/* Will only get here if xTaskStartScheduler() was called with the CPU in
	a non-privileged mode or the binary point register was not set to its lowest
	possible value. */
	return 0;
}

#include "uart1.h"

/*************************************************************/
#define OS_DEBUG 0
void vApplicationIdleHook( void )
{
#if OS_DEBUG 
    dumpString("idle ");
#endif
}


static uint32_t tmr = 0;
void vApplicationTickHook()
{
    tmr++;
#if OS_DEBUG
    dumpString("t ");
#endif
}

void vInitialiseRunTimeStats()
{
    tmr = 0;
}
uint32_t ulGetRunTimeCounterValue()
{
#if OS_DEBUG
    dumpString("s ");
#endif
    return tmr;
}

/* for 4 bit value */
static char toAscii( int val )
{
    switch( val )
    {
    case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:case 8:case 9:
        return (val + '0');
    case 0xa: return 'a';
    case 0xb: return 'b';
    case 0xc: return 'c';
    case 0xd: return 'd';
    case 0xe: return 'e';
    case 0xf: return 'f';
    }
    return '-';
}            

static void dumpHaxVal( unsigned int val )
{
    dumpChar( '0' );
    dumpChar( 'x' );
    dumpChar( toAscii((val >> 28 ) & 0xf ) );
    dumpChar( toAscii((val >> 24 ) & 0xf ) );
    dumpChar( toAscii((val >> 20 ) & 0xf ) );
    dumpChar( toAscii((val >> 16 ) & 0xf ) );
    dumpChar( toAscii((val >> 12 ) & 0xf ) );
    dumpChar( toAscii((val >> 8  ) & 0xf ) );
    dumpChar( toAscii((val >> 4  ) & 0xf ) );
    dumpChar( toAscii((val >> 0  ) & 0xf ) );
}

void FIQInterrupt( void ) {}
void DataAbortInterrupt( unsigned int lr_abt, unsigned int DFSR, unsigned int DFAR ) 
{
    lr_abt -= 8; // point to the cause instruction.
    dumpString( "DataAbortInterrupt at address " );
    dumpHaxVal( lr_abt );
    dumpString( " DFSR: " );
    dumpHaxVal( DFSR );
    dumpString( " DFAR " );
    dumpHaxVal( DFAR );
    dumpString( "\r\n" );
}

